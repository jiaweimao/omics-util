module omics.util {
    uses omics.util.ms.TitleParser;
    uses omics.util.protein.database.ProteinHeaderParser;

    requires transitive jcommander;
    requires transitive org.slf4j;
    requires transitive it.unimi.dsi.fastutil;
    requires transitive com.google.common;

    requires java.desktop;
    requires transitive jsr305;

    exports omics.util;
    exports omics.util.bean;
    exports omics.util.chem;
    exports omics.util.cv;
    exports omics.util.interfaces;
    exports omics.util.io;
    exports omics.util.io.csv;
    exports omics.util.io.xml;
    exports omics.util.math;
    exports omics.util.ms;
    exports omics.util.ms.peaklist;
    exports omics.util.ms.peaklist.filter;
    exports omics.util.ms.peaklist.impl;
    exports omics.util.ms.peaklist.sim;
    exports omics.util.ms.peaklist.sim.impl;
    exports omics.util.ms.peaklist.transform;
    exports omics.util.ms.util;
    exports omics.util.protein;
    exports omics.util.protein.database;
    exports omics.util.protein.database.util;
    exports omics.util.protein.database.uniprot;
    exports omics.util.protein.digest;
    exports omics.util.protein.digest.suffix;
    exports omics.util.protein.mod;
    exports omics.util.protein.ms;
    exports omics.util.utils;
    exports omics.util.protein.variants;

    opens omics.util to jcommander;

    provides omics.util.ms.TitleParser
            with omics.util.ms.util.ProteomeDiscovererTitleParser,
                    omics.util.ms.util.DelegatingRegexTitleParser;
    provides omics.util.protein.database.ProteinHeaderParser
            with omics.util.protein.database.uniprot.UniProtHeaderParser,
                    omics.util.protein.database.uniprot.UniProtKBFastaHeaderParser,
                    omics.util.protein.database.util.NCBIHeaderParser;
}