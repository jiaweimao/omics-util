package omics.util;

import java.util.Objects;

/**
 * default implementation of {@link MetaKey}
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 04 Oct 2018, 3:54 PM
 */
class DefaultMetaKey implements MetaKey
{
    private String key;

    public DefaultMetaKey(String key)
    {
        this.key = key;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultMetaKey that = (DefaultMetaKey) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(key);
    }

    @Override
    public String getId()
    {
        return key;
    }
}
