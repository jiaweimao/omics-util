package omics.util;

import omics.util.bean.BooleanProperty;
import omics.util.bean.DoubleProperty;
import omics.util.bean.Property;
import omics.util.bean.StringProperty;

/**
 * Task with information, it is designed for heavy tasks.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 25 Jan 2018, 11:45 PM
 */
public abstract class InfoTask<V>
{
    private final Property<V> value = new Property<>("value");
    private final DoubleProperty totalWork = new DoubleProperty("totalWork", -1);
    private final DoubleProperty workDone = new DoubleProperty("workDone", -1);
    private final DoubleProperty progress = new DoubleProperty("progress", -1);
    private final Property<Throwable> exception = new Property<>("exception");
    private final StringProperty message = new StringProperty("message", "");
    private final StringProperty title = new StringProperty("title", "");
    private final BooleanProperty stopped = new BooleanProperty("stopped", false);

    public InfoTask() { }

    /**
     * stop the task explicitly
     */
    public final void stop()
    {
        this.stopped.set(true);
    }

    /**
     * @return true if the task is stopped.
     */
    public final boolean isStopped()
    {
        return stopped.get();
    }

    public final BooleanProperty stoppedProperty()
    {
        return stopped;
    }

    /**
     * An optional title that should be associated with this Worker.
     * This may be something such as "Modifying Images".
     *
     * @return the current title
     */
    public final String getTitle() { return title.get(); }

    /**
     * Updates the <code>title</code> property.
     *
     * @param title the new title
     */
    public final void updateTitle(String title)
    {
        this.title.set(title);
    }

    public final StringProperty titleProperty()
    {
        return title;
    }

    /**
     * Gets a message associated with the current state of this Worker. This may
     * be something such as "Processing image 1 of 3", for example.
     *
     * @return the current message
     */
    public final String getMessage() { return message.get(); }

    /**
     * Updates the <code>message</code> property.
     *
     * @param message the new message
     */
    public final void updateMessage(String message)
    {
        this.message.set(message);
    }

    public final StringProperty messageProperty()
    {
        return message;
    }

    /**
     * Updates the <code>workDone</code>, <code>totalWork</code>,
     * and <code>progress</code> properties.
     *
     * @param workDone A value from Long.MIN_VALUE up to max. If the value is greater
     *                 than max, then it will be clamped at max.
     *                 If the value passed is negative then the resulting percent
     *                 done will be -1 (thus, indeterminate).
     * @param max      A value from Long.MIN_VALUE to Long.MAX_VALUE.
     * @see #updateProgress(double, double)
     */
    public final void updateProgress(long workDone, long max)
    {
        updateProgress((double) workDone, (double) max);
    }

    /**
     * Updates the <code>workDone</code>, <code>totalWork</code>,
     * and <code>progress</code> properties.
     *
     * @param workDone A value from Double.MIN_VALUE up to max. If the value is greater
     *                 than max, then it will be clamped at max.
     *                 If the value passed is negative, or Infinity, or NaN,
     *                 then the resulting percentDone will be -1 (thus, indeterminate).
     * @param max      A value from Double.MIN_VALUE to Double.MAX_VALUE. Infinity and NaN are treated as -1.
     */
    public final void updateProgress(double workDone, double max)
    {
        // Adjust Infinity / NaN to be -1 for both workDone and max.
        if (Double.isInfinite(workDone) || Double.isNaN(workDone) || workDone < 0) {
            workDone = -1;
        }

        if (Double.isInfinite(max) || Double.isNaN(max) || max < 0) {
            max = -1;
        }

        // Clamp the workDone if necessary so as not to exceed max
        if (workDone > max)
            workDone = max;

        setTotalWork(max);
        setWorkDone(workDone);

        if (workDone == -1)
            setProgress(-1);
        else
            setProgress(workDone / max);
    }

    /**
     * Indicates the current progress of this Worker in terms of percent complete.
     * A value between zero and one indicates progress toward completion. A value
     * of -1 means that the current progress cannot be determined (that is, it is
     * indeterminate). This property may or may not change from its default value
     * of -1 depending on the specific Worker implementation.
     *
     * @return the current progress
     */
    public final double getProgress()
    {
        return progress.get();
    }

    private void setProgress(double value)
    {
        this.progress.set(value);
    }

    public final DoubleProperty progressProperty()
    {
        return progress;
    }

    /**
     * Indicates the current amount of work that has been completed. Zero or a
     * positive value indicate progress toward completion. This variables value
     * may or may not change from its default value depending on the specific
     * Worker implementation. A value of -1 means that the current amount of work
     * done cannot be determined (ie: it is indeterminate). The value of
     * this property is always less than or equal to totalWork.
     *
     * @return the amount of work done
     */
    public final double getWorkDone() { return workDone.get(); }

    private void setWorkDone(double value)
    {
        this.workDone.set(value);
    }

    public final DoubleProperty workDoneProperty()
    {
        return workDone;
    }

    /**
     * Indicates a maximum value for the task property.
     *
     * @return the total work to be done
     */
    public final double getTotalWork()
    {
        return totalWork.get();
    }

    /**
     * setter of the total work
     *
     * @param value total work value.
     */
    private void setTotalWork(double value)
    {
        this.totalWork.set(value);
    }

    public final DoubleProperty totalWorkProperty()
    {
        return totalWork;
    }

    /**
     * Indicates the exception which occurred while the Task was running, if any.
     * If this property value is {@code null}, there is no known exception, even if
     * the status is FAILED. If this property is not {@code null}, it will most
     * likely contain an exception that describes the cause of failure.
     *
     * @return the exception, if one occurred
     */
    public final Throwable getException() { return exception.get(); }

    public final void setException(Throwable value)
    {
        this.exception.set(value);
    }

    public final Property<Throwable> exceptionProperty()
    {
        return exception;
    }

    /**
     * Specifies the value, or result, of this Worker. This is set upon entering
     * the SUCCEEDED state, and cleared (set to null) if the Worker is reinitialized
     * (that is, if the Worker is a reusable Worker and is reset or restarted).
     *
     * @return the current value of this Worker
     */
    public final V getValue() { return value.get(); }

    /**
     * Updates the <code>value</code> property.
     *
     * @param value the new value
     */
    public final void updateValue(V value)
    {
        this.value.set(value);
    }

    public final Property<V> valueProperty()
    {
        return value;
    }
}
