package omics.util;

import omics.util.interfaces.Identifiable;

/**
 * Object which can be used as a key can implement this interface.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 04 Oct 2018, 3:53 PM
 */
public interface MetaKey extends Identifiable
{
    /**
     * {@inheritDoc}
     */
    int hashCode();
}
