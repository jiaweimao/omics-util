package omics.util;

/**
 * OmicsException, which extends IOException for easy use.
 *
 * @author JiaweiMao 2017.03.19
 * @since 1.1.0
 */
public class OmicsException extends Exception {

    /**
     * <p>
     * Constructor for OmicsException.
     * </p>
     *
     * @param msg a {@link java.lang.String} object.
     */
    public OmicsException(String msg) {
        super(msg);
    }

    /**
     * <p>
     * Constructor for MSDKException.
     * </p>
     *
     * @param exception a {@link java.lang.Throwable} object.
     */
    public OmicsException(Throwable exception) {
        super(exception);
    }
}
