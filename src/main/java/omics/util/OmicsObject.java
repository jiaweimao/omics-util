package omics.util;

import omics.util.interfaces.Identifiable;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * This abstract class provides customization facilities, the keys should implement hashCode().
 *
 * @author JiaweiMao
 * @version 4.3.0
 * @since 19 Oct 2017, 1:19 PM
 */
public abstract class OmicsObject
{
    /**
     * Map containing user refinement parameters.
     */
    private HashMap<MetaKey, Object> urMetas = null;

    /**
     * add a parameter.
     *
     * @param key   parameter key, should be hashable.
     * @param value parameter value.
     */
    public void addMeta(MetaKey key, Object value)
    {
        if (urMetas == null)
            createMetaMap();

        urMetas.put(key, value);
    }

    /**
     * add a parameter.
     *
     * @param key   parameter kye,
     * @param value parameter value.
     */
    public void addMeta(String key, Object value)
    {
        addMeta(new DefaultMetaKey(key), value);
    }

    /**
     * add all parameter in the map
     *
     * @param pms parameter map.
     */
    public void addMetas(Map<MetaKey, Object> pms)
    {
        requireNonNull(pms);

        for (Map.Entry<MetaKey, Object> entry : pms.entrySet()) {
            addMeta(entry.getKey(), entry.getValue());
        }
    }

    /**
     * @return a copy of all parameter keys, or an empty Set if there is no parameters.
     */
    public Set<MetaKey> getMetaKeys()
    {
        if (urMetas == null)
            return new HashSet<>();
        return new HashSet<>(urMetas.keySet());
    }

    /**
     * @return the parameter key list with titles ordered.
     */
    public List<MetaKey> getMetaKeyList()
    {
        if (urMetas == null)
            return new ArrayList<>();
        List<MetaKey> keyList = new ArrayList<>(urMetas.keySet());
        keyList.sort(Comparator.comparing(Identifiable::getId));

        return keyList;
    }

    /**
     * Removes a parameter.
     *
     * @param key the key of the parameter
     */
    public void removeMeta(MetaKey key)
    {
        if (urMetas != null)
            urMetas.remove(key);
    }

    /**
     * Creates the parameters map unless done by another thread already.
     */
    private synchronized void createMetaMap()
    {
        if (urMetas == null)
            urMetas = new HashMap<>(1);
    }

    /**
     * Returns the parameter of given key.
     *
     * @param metaKey the desired parameter key.
     * @return parameter value.
     */
    public Optional<Object> getMeta(MetaKey metaKey)
    {
        if (urMetas == null)
            return Optional.empty();
        return Optional.ofNullable(urMetas.get(metaKey));
    }

    public Optional<Object> getMeta(String key)
    {
        return getMeta(new DefaultMetaKey(key));
    }

    /**
     * Returns the parameter value
     *
     * @param metaKey the desired parameter
     * @return the value stored, {@link OptionalInt#empty()} for absent.
     */
    public OptionalInt getMetaInt(MetaKey metaKey)
    {
        Object value = getMeta(metaKey).orElse(null);
        if (!(value instanceof Integer))
            return OptionalInt.empty();

        return OptionalInt.of((Integer) value);
    }

    /**
     * Return int parameter of given key, if the parameter is not present, return {@link OptionalInt#empty()}
     *
     * @param key parameter key
     * @return the value stored, {@link OptionalInt#empty()} for absent.
     */
    public OptionalInt getMetaInt(String key)
    {
        return getMetaInt(new DefaultMetaKey(key));
    }

    public OptionalLong getMetaLong(String key)
    {
        return getMetaLong(new DefaultMetaKey(key));
    }

    /**
     * Returns the parameter value
     *
     * @param metaKey the desired parameter
     * @return the value stored, null for absent.
     */
    public OptionalLong getMetaLong(MetaKey metaKey)
    {
        Object value = getMeta(metaKey).orElse(null);
        if (!(value instanceof Long))
            return OptionalLong.empty();

        return OptionalLong.of((Long) value);
    }

    public Optional<Boolean> getMetaBool(MetaKey key)
    {
        Object value = getMeta(key).orElse(null);
        if (!(value instanceof Boolean))
            return Optional.empty();
        return Optional.of((Boolean) value);
    }

    public Optional<Boolean> getMetaBool(String key)
    {
        return getMetaBool(new DefaultMetaKey(key));
    }

    /**
     * Returns the parameter value
     *
     * @param metaKey the desired parameter
     * @return the value stored.
     */
    public OptionalDouble getMetaDouble(MetaKey metaKey)
    {
        Object value = getMeta(metaKey).orElse(null);
        if (!(value instanceof Double))
            return OptionalDouble.empty();

        return OptionalDouble.of((Double) value);
    }

    public OptionalDouble getMetaDouble(String key)
    {
        return getMetaDouble(new DefaultMetaKey(key));
    }

    /**
     * Returns the parameter value, for other types, its toString() method is called. empty for absent.
     *
     * @param metaKey the desired parameter
     * @return the value stored in string.
     */
    public String getMetaString(MetaKey metaKey)
    {
        Object value = getMeta(metaKey).orElse("");
        return value.toString();
    }

    /**
     * Return the parameter value, for other types, its toString() method is called. empty for absent.
     *
     * @param key parameter key
     * @return the toString() value of given parameter key.
     */
    public String getMetaString(String key)
    {
        return getMetaString(new DefaultMetaKey(key));
    }

    /**
     * copy all Metas from source OmicsObject to this.
     *
     * @param omicsObject source OmicsObject.
     */
    public void addMetas(OmicsObject omicsObject)
    {
        if (omicsObject.urMetas == null)
            return;

        for (Map.Entry<MetaKey, Object> entry : omicsObject.urMetas.entrySet()) {
            addMeta(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Clears the loaded parameters.
     */
    public void clearMetas()
    {
        if (urMetas != null)
            urMetas.clear();
        urMetas = null;
    }
}
