package omics.util;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 10:18 PM
 */
public class OmicsRuntimeException extends RuntimeException {

    /**
     * <p>
     * Constructor for OmicsRuntimeException.
     * </p>
     *
     * @param msg a {@link String} object.
     */
    public OmicsRuntimeException(String msg) {
        super(msg);
    }

    /**
     * <p>
     * Constructor for OmicsRuntimeException.
     * </p>
     *
     * @param exception a {@link Throwable} object.
     */
    public OmicsRuntimeException(Throwable exception) {
        super(exception);
    }
}
