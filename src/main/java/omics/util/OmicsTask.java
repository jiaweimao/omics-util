package omics.util;

import omics.util.interfaces.Task;

/**
 * This abstract class is designed for any class need to execute some tasks and update information about
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Oct 2018, 6:34 PM
 */
public abstract class OmicsTask<V> extends InfoTask<V> implements Task {}
