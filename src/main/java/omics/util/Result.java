package omics.util;

/**
 * Running result for a task.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 24 Feb 2020, 10:15 AM
 */
public interface Result<V>
{
    class OK<V> implements omics.util.Result<V>
    {
        private V result;

        public OK(V result)
        {
            this.result = result;
        }

        @Override
        public String getMessage()
        {
            return null;
        }

        @Override
        public V getResult()
        {
            return result;
        }

        @Override
        public boolean isOk()
        {
            return true;
        }

        @Override
        public boolean isError()
        {
            return false;
        }

        @Override
        public boolean isCanceled()
        {
            return false;
        }
    }

    class Error<V> implements Result<V>
    {
        private String msg;

        public Error(String msg)
        {
            this.msg = msg;
        }

        @Override
        public String getMessage()
        {
            return msg;
        }

        @Override
        public V getResult()
        {
            return null;
        }

        @Override
        public boolean isOk()
        {
            return false;
        }

        @Override
        public boolean isError()
        {
            return true;
        }

        @Override
        public boolean isCanceled()
        {
            return false;
        }
    }

    class Cancel<V> implements Result<V>
    {
        private String msg;

        public Cancel(String msg)
        {
            this.msg = msg;
        }

        @Override
        public String getMessage()
        {
            return msg;
        }

        @Override
        public V getResult()
        {
            return null;
        }

        @Override
        public boolean isOk()
        {
            return false;
        }

        @Override
        public boolean isError()
        {
            return false;
        }

        @Override
        public boolean isCanceled()
        {
            return true;
        }
    }

    static Result<Void> ok()
    {
        return new OK<>(null);
    }

    /**
     * Running ok.
     *
     * @param result result to result.
     */
    static <T> Result<T> ok(T result)
    {
        return new OK<>(result);
    }

    static <T> Result<T> error(String msg)
    {
        return new Error<>(msg);
    }

    static <T> Result<T> cancel()
    {
        return new Cancel<>("The task is canceled");
    }

    static <T> Result<T> cancel(String msg)
    {
        return new Cancel<>(msg);
    }

    /**
     * @return message of the task.
     */
    String getMessage();

    /**
     * @return the running result, only work for OK.
     */
    V getResult();

    /**
     * @return true if run successfully.
     */
    boolean isOk();

    /**
     * @return true if run with error.
     */
    boolean isError();

    /**
     * @return true if the task is canceled.
     */
    boolean isCanceled();
}
