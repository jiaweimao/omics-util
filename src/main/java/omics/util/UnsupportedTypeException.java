package omics.util;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 30 Jul 2018, 1:13 PM
 */
public class UnsupportedTypeException extends OmicsRuntimeException {

    public UnsupportedTypeException(String msg) {
        super(msg);
    }
}
