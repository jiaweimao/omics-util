package omics.util.bean;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Feb 2019, 3:39 PM
 */
public class BooleanProperty extends Property<Boolean>
{
    public BooleanProperty(String name, Boolean initialValue)
    {
        super(name, initialValue);
    }
}
