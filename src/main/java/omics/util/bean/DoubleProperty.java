package omics.util.bean;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Feb 2019, 2:39 PM
 */
public class DoubleProperty extends Property<Double>
{
    public DoubleProperty(String name)
    {
        super(name);
    }

    public DoubleProperty(String name, double initialValue)
    {
        super(name, initialValue);
    }
}
