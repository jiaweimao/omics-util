package omics.util.bean;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * This class implements a wrapper of an arbitrary object.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Feb 2019, 3:24 PM
 */
public class Property<T>
{
    private static final String DEFAULT_NAME = "";

    private T value;
    private final String name;

    private final PropertyChangeSupport iPCS = new PropertyChangeSupport(this);


    public Property()
    {
        this(DEFAULT_NAME);
    }

    /**
     * The constructor of {@code Property}
     *
     * @param initialValue the initial value of the wrapped value
     */
    public Property(T initialValue)
    {
        this(DEFAULT_NAME, initialValue);
    }

    /**
     * The constructor of {@code Property}
     *
     * @param name the name of this {@code Property}
     */
    public Property(String name)
    {
        this.name = (name == null) ? DEFAULT_NAME : name;
    }

    /**
     * The constructor of {@code Property}
     *
     * @param name         the name of this {@code Property}
     * @param initialValue the initial value of the wrapped value
     */
    public Property(String name, T initialValue)
    {
        this.name = (name == null) ? DEFAULT_NAME : name;
        this.value = initialValue;
    }

    /**
     * Returns the name of this property. If the property does not have a name,
     * this method returns an empty {@code String}.
     *
     * @return the name or an empty {@code String}
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return property value
     */
    public T get()
    {
        return value;
    }

    public void set(T value)
    {
        T oldValue = this.value;
        this.value = value;

        iPCS.firePropertyChange(name, oldValue, value);
    }

    /**
     * add a {@link PropertyChangeListener} to listen to the change of this property
     *
     * @param listener a {@link PropertyChangeListener}
     */
    public void addListener(PropertyChangeListener listener)
    {
        iPCS.addPropertyChangeListener(listener);
    }

    /**
     * remove a {@link PropertyChangeListener}
     *
     * @param listener a {@link PropertyChangeListener}
     */
    public void removeListener(PropertyChangeListener listener)
    {
        iPCS.removePropertyChangeListener(listener);
    }
}
