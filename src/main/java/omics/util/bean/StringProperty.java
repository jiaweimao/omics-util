package omics.util.bean;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Feb 2019, 3:39 PM
 */
public class StringProperty extends Property<String>
{
    public StringProperty()
    {
        super();
    }

    public StringProperty(String name, String initialValue)
    {
        super(name, initialValue);
    }
}
