package omics.util.chem;

import java.util.Objects;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;


/**
 * An atom or isotope in PeriodicTable.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 06 May 2017, 4:36 PM
 */
public class Atom implements Comparable<Atom>
{
    private final AtomicSymbol symbol;
    private final int nucleonCount;
    private final int neutronCount;
    private final double mass;
    private final double abundance;
    private boolean defaultIsotope = false;

    Atom(AtomicSymbol symbol, int nucleonCount, int neutronCount, double mass, double abundance)
    {
        requireNonNull(symbol);
        checkArgument(mass > 0);
        checkArgument(abundance >= 0);
        checkArgument(abundance <= 1);

        this.symbol = symbol;
        this.nucleonCount = nucleonCount;
        this.neutronCount = neutronCount;
        this.mass = mass;
        this.abundance = abundance;
    }

    static int staticHash(AtomicSymbol symbol, int neutronCount)
    {
        return Objects.hash(symbol, neutronCount);
    }

    public AtomicSymbol getSymbol()
    {
        return symbol;
    }

    /**
     * @return the nucleon (核子) count of this isotope.
     */
    public int getNucleonCount()
    {
        return nucleonCount;
    }

    /**
     * @return neutron (中子) count of this isotope.
     */
    public int getNeutronCount()
    {
        return neutronCount;
    }

    /**
     * @return isotope mass
     */
    public double getMass()
    {
        return mass;
    }

    /**
     * @return average mass of this atom.
     */
    public double getAverageMass()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        if (!defaultIsotope)
            return mass;

        return periodicTable.getElement(symbol).getAverageMass();
    }

    /**
     * @return abundance of this isotope.
     */
    public double getAbundance()
    {
        return abundance;
    }

    /**
     * @return true if this is the default mono isotope
     */
    public boolean isDefaultIsotope()
    {
        return defaultIsotope;
    }

    /**
     * Set this Atom as default isotope.
     */
    public void setDefaultIsotope(boolean defaultIsotope)
    {
        this.defaultIsotope = defaultIsotope;
    }

    @Override
    public String toString()
    {
        if (!isDefaultIsotope())
            return symbol.toString() + "[" + nucleonCount + "]";
        else
            return symbol.toString();
    }

    @Override
    public int hashCode()
    {
        return staticHash(symbol, nucleonCount);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Atom atom = (Atom) o;

        return symbol == atom.symbol && nucleonCount == atom.nucleonCount;
    }

    @Override
    public int compareTo(Atom o)
    {
        return Double.compare(mass, o.mass);
    }
}
