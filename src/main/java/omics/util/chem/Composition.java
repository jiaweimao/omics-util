package omics.util.chem;

import com.google.common.collect.ImmutableSet;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntMaps;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

import java.io.Serializable;
import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Composition is a collection of atoms and charge, can represent any chemical formula.
 * The mass of the same Composition may slightly differ as the round off effect.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 15 Mar 2017, 4:10 PM
 */
public class Composition extends Mass
{
    public static final Composition H = Composition.parseComposition("H");
    public static final Composition OH = Composition.parseComposition("OH");
    public static final Composition CO = Composition.parseComposition("CO");
    public static final Composition NH = Composition.parseComposition("NH");
    public static final Composition H2O = Composition.parseComposition("H2O");
    public static final Composition NH3 = Composition.parseComposition("NH3");
    public static final Composition H3PO4 = Composition.parseComposition("H3PO4");
    public static final Composition EMPTY = new Composition("", new Object2IntOpenHashMap<>(), 0);

    protected final int charge;
    protected final Object2IntMap<Atom> atomCounterMap;
    private final String formula;
    private double mass = 0;
    private int nominalMass = 0;

    /**
     * Construct a new mass that contains the <code>charge</code> and the
     * atoms in the atomCounterMap.
     *
     * @param atomCounterMap the atoms to include in the mass
     * @param charge         the charge of the mass
     */
    public Composition(Object2IntMap<Atom> atomCounterMap, int charge)
    {
        this(Builder.makeFormula(atomCounterMap, charge),
                Object2IntMaps.unmodifiable(new Object2IntOpenHashMap<>(atomCounterMap)), charge);
    }

    protected Composition(String formula, Object2IntMap<Atom> atomCountMap, int charge)
    {
        this.charge = charge;
        this.atomCounterMap = atomCountMap;
        this.formula = formula;

        for (Atom atom : atomCountMap.keySet()) {

            mass += (atom.getMass() * atomCountMap.getInt(atom));
        }

        // the number of charge brought by electron should be considered.
        mass -= (this.charge * PeriodicTable.ELECTRON_MASS);
    }

    /**
     * Constructs a mass that is the sum of the <code>compositions</code>
     *
     * @param compositions the Compositions to sum
     */
    public Composition(Composition... compositions)
    {
        this.charge = sumCharges(compositions);

        atomCounterMap = new Object2IntOpenHashMap<>();

        for (Composition composition : compositions) {

            for (Atom atom : composition.atomCounterMap.keySet()) {

                addAtoms(atom, composition.atomCounterMap.getInt(atom));
            }
        }

        if (compositions.length != 1) {
            formula = Builder.makeFormula(atomCounterMap, charge);
        } else {
            formula = compositions[0].getFormula();
        }

        mass -= (charge * PeriodicTable.ELECTRON_MASS);
    }

    /**
     * Creates a Composition by parsing the given string. For the format that is
     * allowed see AtomicCompositionParser
     *
     * @param s the string to parse
     * @return the new Composition
     * @see AtomicCompositionParser
     */
    public static Composition parseComposition(String s)
    {
        Object2IntMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = AtomicCompositionParser.getInstance().parse(s, atomCounterMap);

        return new Composition(s, atomCounterMap, charge);
    }

    /**
     * Sum charges of all Composition.
     *
     * @param compositions all mass
     * @return summary of charges.
     */
    private int sumCharges(Composition[] compositions)
    {
        int charge = 0;
        for (Composition composition : compositions) {

            charge += composition.getCharge();
        }

        return charge;
    }

    /**
     * Sub composition from this composition
     *
     * @param composition composition to sub
     * @return a new compsotion which is the delta of this the the other composition.
     */
    public Composition subtract(Composition composition)
    {
        requireNonNull(composition);

        int z = charge - composition.getCharge();
        Object2IntMap<Atom> countMap = new Object2IntOpenHashMap<>(atomCounterMap);
        for (Object2IntMap.Entry<Atom> entry : composition.atomCounterMap.object2IntEntrySet()) {
            Atom atom = entry.getKey();
            int sub = countMap.getOrDefault(atom, 0) - entry.getIntValue();
            if (sub == 0) {
                countMap.removeInt(atom);
            } else {
                countMap.put(atom, sub);
            }
        }

        return new Composition(countMap, z);
    }

    /**
     * Returns a immutable set containing all the Atoms in this mass.
     */
    public Set<Atom> getAtoms()
    {
        return ImmutableSet.copyOf(atomCounterMap.keySet());
    }

    /**
     * Returns the monoisotopic molecular mass of this mass.
     *
     * <h2>Roundoff error and error propagation</h2>
     * At rare occasion, masses of different instances with exact
     * same mass may slightly differ.
     * <p>
     * This is due to the fact that as our mass is a Map of unsorted
     * atoms the same atoms traversing order is not guaranteed.
     * <p>
     * As each instance mass is calculated by traversing atomic masses and that
     * mass are unexactly represented in the standard binary floating point,
     * these roundoff errors<br>
     * can propagate through the calculation in non-intuitive ways and may lead
     * to tiny different results.
     *
     * @return the monoisotopic molecular mass of this mass
     */
    @Override
    public double getMolecularMass()
    {
        return mass;
    }

    /**
     * Calculates the average mass of this Composition.
     *
     * @return the average mass
     */
    public double getAverageMass()
    {
        double avgMass = 0;
        for (Object2IntMap.Entry<Atom> entry : atomCounterMap.object2IntEntrySet()) {
            avgMass += entry.getKey().getAverageMass() * entry.getIntValue();
        }

        avgMass -= charge * PeriodicTable.ELECTRON_MASS;

        return avgMass;
    }

    /**
     * Get the delta mass [absolute mass-monoisotopic mass]
     *
     * @return the delta mass
     */
    public double getIsotopeDelta()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();
        double nonIsotopeMass = 0;
        for (Object2IntMap.Entry<Atom> entry : atomCounterMap.object2IntEntrySet()) {
            nonIsotopeMass += periodicTable.getAtom(entry.getKey().getSymbol()).getMass() * entry.getIntValue();
        }

        return mass - nonIsotopeMass;
    }

    /**
     * Return the formula for this mass.
     *
     * @see AtomicCompositionParser
     */
    public String getFormula()
    {
        return formula;
    }

    @Override
    public double getMassDefect()
    {
        int count = 0;
        for (Map.Entry<Atom, Integer> entry : atomCounterMap.object2IntEntrySet()) {
            count += entry.getKey().getNucleonCount() * entry.getValue();
        }

        return getMolecularMass() - count;
    }

    /**
     * ref: 1.	S. Kim, P. A. Pevzner, MS-GF+ makes progress towards a universal database search tool for proteomics. Nature Communications 5,  (2014).
     */
    @Override
    public int getNominalMass()
    {
        if (nominalMass == 0) {
            for (Object2IntMap.Entry<Atom> atomEntry : atomCounterMap.object2IntEntrySet()) {
                nominalMass += atomEntry.getKey().getNucleonCount() * atomEntry.getIntValue();
            }
        }
        return nominalMass;
    }

    /**
     * Return the charge for this mass.
     */
    public int getCharge()
    {
        return charge;
    }

    /**
     * Counts the number of times <code>atom</code> occurs in this Composition
     *
     * @param atom the atom
     */
    public int getCount(Atom atom)
    {
        return atomCounterMap.containsKey(atom) ? atomCounterMap.getInt(atom) : 0;
    }

    /**
     * @return the number of atoms in this Composition
     */
    public int size()
    {
        int count = 0;

        for (int counter : atomCounterMap.values()) {

            count += counter;
        }

        return count;
    }

    /**
     * Returns true if this mass contains no atoms and charge, false otherwise.
     */
    public boolean isEmpty()
    {
        return atomCounterMap.isEmpty() && charge == 0;
    }

    private void addAtoms(Atom atom, int count)
    {
        if (count == 0)
            return;

        requireNonNull(atom);

        mass += atom.getMass() * count;

        atomCounterMap.put(atom, atomCounterMap.getInt(atom) + count);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (!(o instanceof Composition))
            return false;

        Composition that = (Composition) o;

        return charge == that.charge && atomCounterMap.equals(that.atomCounterMap);

    }

    @Override
    public int hashCode()
    {
        int result = charge;
        result = 31 * result + atomCounterMap.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return getFormula();
    }

    public static final class Builder
    {
        private static final IsotopeComparatorBySymbolAndA atomComparator = new IsotopeComparatorBySymbolAndA();
        private final PeriodicTable periodicTable = PeriodicTable.getInstance();
        private final Object2IntMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        private int charge = 0;

        /**
         * Construct a new Builder
         */
        public Builder() { }

        /**
         * Construct a new Builder and add the <code>atom</code>
         *
         * @param atom the first atom to add to the builder
         */
        public Builder(AtomicSymbol atom)
        {
            add(atom, 1);
        }

        /**
         * Construct a new Builder and add the <code>atom</code>
         * <code>count</code> times
         *
         * @param atom  the first atom to add to the builder
         * @param count the number of times to add the atom
         */
        public Builder(AtomicSymbol atom, int count)
        {
            add(atom, count);
        }

        /**
         * Make the atomic formula given an <code>atomCounterMap</code> and
         * <code>charge</code>
         *
         * @param atomCounterMap map containing the counts for the atoms
         * @param charge         the charge
         * @return the formula
         */
        public static String makeFormula(Object2IntMap<Atom> atomCounterMap, int charge)
        {
            // sort by symbol + A (number of mass)
            List<Atom> sortedIsotopes = new ArrayList<>(atomCounterMap.keySet());
            sortedIsotopes.sort(atomComparator);

            StringBuilder sbFormula = new StringBuilder();

            /* add isotopic mass */
            for (Atom atom : sortedIsotopes) {
                int count = atomCounterMap.getInt(atom);
                if (count == 0)
                    continue;

                sbFormula.append(atom.toString());

                if (count != 1) {
                    sbFormula.append(count);
                }
            }

            if (charge != 0) {
                sbFormula.append('(');

                if (charge != 1 && charge != -1) {
                    sbFormula.append((charge > 0) ? charge + "+" : -charge + "-");
                } else {
                    sbFormula.append((charge > 0) ? "+" : "-");
                }
                sbFormula.append(')');

            }

            return sbFormula.toString();
        }

        /**
         * Set the charge.
         *
         * @return this builder
         */
        public Builder charge(int charge)
        {
            this.charge = charge;
            return this;
        }

        /**
         * Add an atom for the specified AtomicSymbol
         *
         * @param atom the symbol of the atom to add
         * @return this builder
         */
        public Builder add(AtomicSymbol atom)
        {
            requireNonNull(atom);

            add(periodicTable.getAtom(atom), 1);

            return this;
        }

        /**
         * Add <code>count</code> atoms for the specified AtomicSymbol
         *
         * @param atom  the symbol of the atom to add
         * @param count the number of times that the atom is to be added
         * @return this builder
         */
        public Builder add(AtomicSymbol atom, int count)
        {
            requireNonNull(atom);

            add(periodicTable.getAtom(atom), count);

            return this;
        }

        /**
         * Add an atom for the specified symbol and isotope
         *
         * @param symbol  the symbol of the atom to add
         * @param isotope the isotope number
         * @return this builder
         */
        public Builder addIsotope(AtomicSymbol symbol, int isotope)
        {
            requireNonNull(symbol);

            add(periodicTable.getAtom(symbol, isotope), 1);

            return this;
        }

        /**
         * Add an atom <code>count<code/> times for the specified symbol and
         * isotope
         *
         * @param symbol  the symbol of the atom to add
         * @param isotope the isotope number
         * @param count   the number of times to add the atom
         * @return this builder
         */
        public Builder addIsotope(AtomicSymbol symbol, int isotope, int count)
        {
            requireNonNull(symbol);

            add(periodicTable.getAtom(symbol, isotope), count);

            return this;
        }

        /**
         * Add the <code>atom</code> <code>count</code> times.
         *
         * @param atom  the atom to add
         * @param count the number of atoms to add
         * @return this builder
         */
        public Builder add(Atom atom, int count)
        {
            if (count == 0)
                return this;
            requireNonNull(atom);
            atomCounterMap.put(atom, atomCounterMap.getInt(atom) + count);

            return this;
        }

        /**
         * Build the mass
         *
         * @return the new Composition
         */
        public Composition build()
        {
            return new Composition(atomCounterMap, charge);
        }

        /**
         * Return the atomic mass string defined in
         * AtomicCompositionParser
         *
         * @return the formula
         * @see AtomicCompositionParser
         */
        @Override
        public String toString()
        {
            return makeFormula(atomCounterMap, charge);
        }

        /**
         * Add all of the atoms in <code>mass</code> to this
         *
         * @param composition the mass
         * @return this builder
         */
        public Builder add(Composition composition)
        {
            for (Atom atom : composition.atomCounterMap.keySet()) {
                atomCounterMap.put(atom, atomCounterMap.getInt(atom) + composition.atomCounterMap.getInt(atom));
            }

            return this;
        }

        /**
         * add given {@link Composition} of given count
         *
         * @param composition a {@link Composition}
         * @param count       count of the {@link Composition} to add
         */
        public Builder add(Composition composition, int count)
        {
            if (composition.isEmpty() || count == 0)
                return this;

            for (Atom atom : composition.atomCounterMap.keySet()) {
                atomCounterMap.put(atom, atomCounterMap.getInt(atom) + composition.atomCounterMap.getInt(atom) * count);
            }

            return this;
        }

        /**
         * Returns true if there are no atoms in this builder and the charge is 0.
         *
         * @return true if there are no atoms in this builder and the charge is 0
         */
        public boolean isEmpty()
        {
            return atomCounterMap.isEmpty() && charge == 0;
        }

        /**
         * Sorts Atoms alphabetically and by neutron count
         */
        private static class IsotopeComparatorBySymbolAndA implements Comparator<Atom>, Serializable
        {
            public int compare(Atom o1, Atom o2)
            {
                int comp = o1.getSymbol().getSymbol().compareTo(o2.getSymbol().getSymbol());
                if (comp == 0) {
                    comp = o1.getNucleonCount() - o2.getNucleonCount();
                }
                return comp;
            }
        }
    }
}
