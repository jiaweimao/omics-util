package omics.util.chem;

import java.util.ArrayList;
import java.util.List;

/**
 * All mass data get from www.unimod.org
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 12:12 PM
 */
public class Element implements Symbol, Comparable<Element>
{
    private final AtomicSymbol symbol;
    private final String name;
    private final int protonCount;
    private final double avgMass;
    private final List<Atom> atomList;

    public Element(AtomicSymbol symbol, String name, int protonCount, double avgMass, List<Atom> atomList)
    {
        this.symbol = symbol;
        this.name = name;
        this.protonCount = protonCount;
        this.avgMass = avgMass;
        this.atomList = atomList;
    }

    /**
     * @return ElementSymbol
     */
    public AtomicSymbol getElementSymbol()
    {
        return symbol;
    }

    /**
     * @return Element name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return proton count.
     */
    public int getProtonCount()
    {
        return protonCount;
    }

    /**
     * @return Element average mass.
     */
    public double getAverageMass()
    {
        return avgMass;
    }

    /**
     * @return Isotope list of this element.
     */
    public List<Atom> getIsotopeList()
    {
        return atomList;
    }

    /**
     * Return all Atoms with abundance larger than the threshold.
     *
     * @param abundance abundance threshold
     * @return list of Atom.
     */
    public List<Atom> getIsotopeList(double abundance)
    {
        List<Atom> list = new ArrayList<>();
        for (Atom atom : atomList) {
            if (atom.getAbundance() > abundance) {
                list.add(atom);
            }
        }
        return list;
    }

    @Override
    public int compareTo(Element o)
    {
        return Integer.compare(protonCount, o.protonCount);
    }

    @Override
    public String getSymbol()
    {
        return symbol.toString();
    }
}
