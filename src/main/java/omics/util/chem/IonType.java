package omics.util.chem;


/**
 * Ion types.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Mar 2017, 3:27 PM
 */
public enum IonType
{
    /**
     * Identifier for a peptide fragment ion.
     */
    PEPTIDE_FRAGMENT_ION,
    /**
     * A tag fragment ion
     */
    TAG_FRAGMENT_ION,
    /**
     * Identifier for an MH ion. The number of H is not represented here.
     */
    PRECURSOR_ION,
    /**
     * Identifier for an immonium ion.
     */
    IMMONIUM_ION,
    /**
     * Identifier for a reporter ion.
     */
    REPORTER_ION,
    /**
     * Identifier for a glycan.
     */
    GLYCAN_ION,
    /**
     * Identifier for an elementary ion.
     */
    ELEMENTARY_ION,
    /**
     * Identifier for a related ion.
     */
    RELATED_ION,
    /**
     * Identifier for an unknown ion.
     */
    UNKNOWN;
}
