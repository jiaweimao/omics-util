package omics.util.chem;


/**
 * Object with mass and formula.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 08 Mar 2017, 4:15 PM
 */
public abstract class Mass implements Weighable, Comparable<Mass>
{
    /**
     * @return the formula of the molecular.
     */
    public abstract String getFormula();

    /**
     * The amount by which the mass of all atomic nuclei in this mass differs from the sum of
     * the masses of its constituent protons, neutrons and electrons.
     * <p>
     * This is the mass equivalent of the energy released in the formation of the nucleus.
     */
    public abstract double getMassDefect();

    /**
     * @return the nominal mass in Daltons of this object.
     */
    public abstract int getNominalMass();

    @Override
    public int compareTo(Mass o)
    {
        return Double.compare(getMolecularMass(), o.getMolecularMass());
    }

    @Override
    public String toString()
    {
        return String.format("[%.2f]", getMolecularMass());
    }
}
