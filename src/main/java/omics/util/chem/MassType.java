package omics.util.chem;

/**
 * The Enumeration serves to type the Mass deconvolution that should be used.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 4:54 PM
 */
public enum MassType
{
    MONOISOTOPIC, AVERAGE;
}
