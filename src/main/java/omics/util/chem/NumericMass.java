package omics.util.chem;

/**
 * A simple Mass Object with a double as mass.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 12:10 PM
 */
public class NumericMass extends Mass {

    public static final Mass ZERO = new NumericMass(0.0);

    public static final double INTEGER_MASS_SCALER = 0.999497;

    private final double mass;

    public NumericMass(double mass) {
        this.mass = mass;
    }

    @Override
    public double getMolecularMass() {
        return mass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        NumericMass that = (NumericMass) o;

        return Double.compare(that.mass, mass) == 0;
    }

    @Override
    public int hashCode() {
        long temp = mass != +0.0d ? Double.doubleToLongBits(mass) : 0L;
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        return Double.toString(mass);
    }

    @Override
    public String getFormula() {
        return Double.toString(mass);
    }

    @Override
    public double getMassDefect() {
        return 0;
    }

    @Override
    public int getNominalMass() {
        return (int) Math.round(mass * INTEGER_MASS_SCALER);
    }
}
