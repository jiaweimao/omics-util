package omics.util.chem;

import omics.util.io.xml.XMLUtils;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.*;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * All atom mass get from
 * https://www.webelements.com/
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 06 Mar 2017, 12:13 PM
 */
public class PeriodicTable
{
    /**
     * these mass get from https://en.wikipedia.org
     */
    public static final double ELECTRON_MASS = 0.00054857990946;
    public static final double PROTON_MASS = 1.007276466879;
    public static final double NEUTRON_MASS = 1.00866491588;
    public static final double C12C13MassDiff = 1.0033548378;
    public static final double C12C14MassDiff = 2.003241988;
    public static final double ISOTOPE_DIFF = 1.00286864; // J. Cox, M. Mann. Nature Biotechnology 26, 1367-1372 (2008).

    public static final double H_MASS;
    public static final double H2_MASS;
    public static final double O_MASS;
    public static final double C_MASS;
    public static final double C13_MASS;
    public static final double C14_MASS;
    public static final double N_MASS;
    public static final double Na_MASS;
    public static final double P_MASS;
    public static final double S_MASS;
    public static final double K_MASS;

    public static final double NH_MASS;
    public static final double NH2_MASS;
    public static final double NH3_MASS;
    public static final double H2O_MASS;
    public static final double CO_MASS;

    public static final Atom H;
    public static final Atom H2;
    public static final Atom O;
    public static final Atom C;
    public static final Atom C13;
    public static final Atom C14;
    public static final Atom N;
    public static final Atom Na;
    public static final Atom K;
    public static final Atom P;
    public static final Atom S;
    private static final PeriodicTable instance;

    static {
        instance = new PeriodicTable();

        H = instance.getAtom(AtomicSymbol.H);
        H2 = instance.getAtom(AtomicSymbol.H, 2);
        O = instance.getAtom(AtomicSymbol.O);
        C = instance.getAtom(AtomicSymbol.C);
        C13 = instance.getAtom(AtomicSymbol.C, 13);
        C14 = instance.getAtom(AtomicSymbol.C, 14);
        N = instance.getAtom(AtomicSymbol.N);
        Na = instance.getAtom(AtomicSymbol.Na);
        P = instance.getAtom(AtomicSymbol.P);
        S = instance.getAtom(AtomicSymbol.S);
        K = instance.getAtom(AtomicSymbol.K);

        H_MASS = H.getMass();
        H2_MASS = H2.getMass();
        O_MASS = O.getMass();
        C_MASS = C.getMass();
        C13_MASS = C13.getMass();
        C14_MASS = C14.getMass();
        N_MASS = N.getMass();
        Na_MASS = Na.getMass();
        P_MASS = P.getMass();
        S_MASS = S.getMass();
        K_MASS = K.getMass();

        NH_MASS = H_MASS + N_MASS;
        NH2_MASS = N_MASS + 2 * H_MASS;
        NH3_MASS = N_MASS + 3 * H_MASS;

        H2O_MASS = O_MASS + 2 * H_MASS;
        CO_MASS = O_MASS + C_MASS;
    }

    private final Map<Integer, Atom> isotopeMap = new HashMap<>();
    private final Map<AtomicSymbol, Atom> defaultIsotopeMap = new HashMap<>();
    private final Map<AtomicSymbol, Element> elementMap = new HashMap<>();
    private final List<Atom> atomList = new ArrayList<>();

    /**
     * Construct a PeriodicTable by reading the elements.xml file
     */
    private PeriodicTable()
    {
        try {
            InputStream inputStream = PeriodicTable.class.getClassLoader().getResourceAsStream("elements.xml");
            XMLStreamReader reader = XMLUtils.createXMLStreamReader(inputStream);

            AtomicSymbol symbol = null;
            String name = "";
            int protonCount = -1;
            double avgMass = 0.0;// not use

            int nucleonCount;
            int neutronCount;
            double mass;
            double abundance;

            List<Atom> isotopeList = null;
            Atom monoIsotope = null;
            double maxAbundance = -1;

            while (reader.hasNext()) {

                int event = reader.next();
                if (event == XMLStreamConstants.START_ELEMENT) {

                    String localName = reader.getLocalName();
                    if (localName.equals("element")) {
                        isotopeList = new ArrayList<>();

                        symbol = AtomicSymbol.valueOf(reader.getAttributeValue(null, "symbol"));
                        name = reader.getAttributeValue(null, "name");
                        protonCount = Integer.parseInt(reader.getAttributeValue(null, "atomic_number"));
                        avgMass = Double.parseDouble(reader.getAttributeValue(null, "avg_mass"));
                    } else if (localName.equals("isotope")) {
                        nucleonCount = Integer.parseInt(reader.getAttributeValue(null, "id"));
                        neutronCount = Integer.parseInt(reader.getAttributeValue(null, "neutron_count"));

                        mass = Double.parseDouble(reader.getAttributeValue(null, "mass"));
                        abundance = Double.parseDouble(reader.getAttributeValue(null, "frequency"));

                        int hash = Atom.staticHash(symbol, nucleonCount);

                        Atom atom = new Atom(symbol, nucleonCount, neutronCount, mass, abundance);
                        atomList.add(atom);
                        isotopeMap.put(hash, atom);
                        isotopeList.add(atom);

                        // some radio element do not have default isotope
                        if (abundance > 0 && abundance > maxAbundance) {
                            monoIsotope = atom;
                            maxAbundance = abundance;
                        }
                    }

                } else if (event == XMLStreamConstants.END_ELEMENT) {
                    String localName = reader.getLocalName();
                    if (localName.equals("element")) {
                        if (monoIsotope != null) {
                            monoIsotope.setDefaultIsotope(true);
                            defaultIsotopeMap.put(symbol, monoIsotope);
                        }

                        Element element = new Element(symbol, name, protonCount, avgMass, isotopeList);
                        elementMap.put(symbol, element);

                        maxAbundance = -1;
                        monoIsotope = null;
                    }
                }
            }

            reader.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public static PeriodicTable getInstance()
    {
        return instance;
    }

    /**
     * Return the atom for the atomicSymbol with the number of neutrons given by massNumber
     *
     * @param atomicSymbol the element symbol
     * @param massNumber   the number of nucleon Count
     */
    public Atom getAtom(AtomicSymbol atomicSymbol, int massNumber)
    {
        Atom atom = isotopeMap.get(Atom.staticHash(atomicSymbol, massNumber));
        if (atom != null) {
            return atom;
        }

        throw new IllegalArgumentException(massNumber +
                " is not a valid argument for getting the isotope for the symbol " + atomicSymbol);
    }

    /**
     * Return the default isotope for the <code>atomicSymbol</code></br>
     * <b>NOTE: Some element do not contain default isotope.</b>
     *
     * @param atomicSymbol the atomic symbol
     * @return default isotope of given {@link AtomicSymbol}, if all abundance is 0, which means no default isotope for the element, return null.
     */
    public Atom getAtom(AtomicSymbol atomicSymbol)
    {
        requireNonNull(atomicSymbol);

        return defaultIsotopeMap.get(atomicSymbol);
    }

    /**
     * Parse the symbol and return the corresponding Atom. For example,
     * <ul>
     * <li>if symbol {@code C} given then an instance of {@code Atom
     * C[12]} returned</li>
     * <li>if symbol {@code C[13]} given then an instance of {@code
     * Atom C[13]} instance returned</li>
     * </ul>
     * <p/>
     * <h2>Atom definition</h2> An atom is defined by a symbol with an optional
     * explicit mass number (nucleon number)<br>
     * Example: hydrogen atom could be described like "H", "H[1]"
     *
     * @param symbol the symbol
     * @return the atom for the symbol
     * @throws IllegalArgumentException if bad element format
     */
    public Atom getAtom(String symbol)
    {
        int index = symbol.indexOf('[');

        try {
            if (index == -1) {

                return getAtom(AtomicSymbol.valueOf(symbol));
            } else {

                return getAtom(AtomicSymbol.valueOf(symbol.substring(0, index)),
                        Integer.parseInt(symbol.substring(index + 1, symbol.length() - 1)));
            }
        } catch (IllegalArgumentException e) {

            throw new IllegalArgumentException(symbol + ": invalid symbol", e);
        }
    }

    /**
     * return the Element of given symbol.
     *
     * @param atomicSymbol a {@link AtomicSymbol} object.
     * @return Element of given symbol.
     */
    public Element getElement(AtomicSymbol atomicSymbol)
    {
        requireNonNull(atomicSymbol);

        return elementMap.get(atomicSymbol);
    }

    /**
     * @return all {@link Atom} in the PeriodicTable
     */
    public List<Atom> getAtomList()
    {
        return Collections.unmodifiableList(atomList);
    }

    /**
     * Returns the {@link Atom}s of given element.
     *
     * @param symbol element symbol
     */
    public List<Atom> getAtoms(AtomicSymbol symbol)
    {
        requireNonNull(symbol);

        return elementMap.get(symbol).getIsotopeList();
    }
}
