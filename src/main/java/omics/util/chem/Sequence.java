package omics.util.chem;

/**
 * Interface for sequence of symbol, such as protein, peptide etc.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 3:54 PM
 */
public interface Sequence<S extends Symbol>
{
    /**
     * Returns the symbol at the specified position in this symbol sequence.
     *
     * @param index index of the symbol to return (0-based)
     * @throws IndexOutOfBoundsException if the index is out of range ( <tt>index &lt; 0 || index &gt;= size()</tt>)
     */
    S getSymbol(int index);

    /**
     * @return String containing the Symbols
     */
    String toSymbolString();

    /**
     * Returns the number of symbols in this symbol sequence. If this symbol
     * sequence contains more than <tt>Integer.MAX_VALUE</tt> symbols, returns
     * <tt>Integer.MAX_VALUE</tt>.
     *
     * @return the number of symbols in this symbol sequence
     */
    int size();
}


