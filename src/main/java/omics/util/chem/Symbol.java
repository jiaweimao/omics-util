package omics.util.chem;

/**
 * For items that have name or symbol, such as Amino acid and atoms
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 15 Mar 2017, 4:29 PM
 */
public interface Symbol
{
    /**
     * @return a short name of the object.
     */
    String getSymbol();
}
