package omics.util.chem;


/**
 * The {@code Weighable} interface should be implemented by any object that has
 * a molecular mass measured.
 * <p>
 * The molecular mass of a substance is the mass of one molecule of that
 * substance. In unified atomic mass unit(s) (u) or dalton (Da), is equals to
 * 1/12 the mass of one isotope of carbon 12.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 15 Mar 2017, 4:28 PM
 */
public interface Weighable
{
    /**
     * Return the molecular mass
     */
    double getMolecularMass();
}
