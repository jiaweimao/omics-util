package omics.util.cv;

import omics.util.interfaces.Identifiable;

import static java.util.Objects.requireNonNull;

/**
 * Information about an ontology or CV source and a short 'lookup' tag to refer to.
 *
 * @author JiaweiMao
 * @version 1.3.0
 * @since 25 Aug 2018, 3:57 PM
 */
public enum Cv implements Identifiable
{
    /**
     * the PSI-MS cv.
     */
    PSI_MS("MS", "Proteomics Standards Initiative Mass Spectrometry Ontology", "4.1.28", "https://raw.githubusercontent.com/HUPO-PSI/psi-ms-CV/master/psi-ms.obo"),
    /**
     * the unit ontology
     */
    UNIT_ONTOLOGY("UO", "Unit Ontology", "09:04:2014", "https://raw.githubusercontent.com/bio-ontology-research-group/unit-ontology/master/unit.obo"),
    /**
     * unimod cv
     */
    UNIMOD("UNIMOD", "UNIMOD", "2018:10:25", "http://www.unimod.org/obo/unimod.obo");

    /**
     * The short label to be used as a reference tag with which to refer to this particular Controlled Vocabulary source description. required
     */
    private String id;
    /**
     * The usual name for the resource (e.g. The PSI-MS Controlled Vocabulary). required
     */
    private String fullName;
    /**
     * The version of the CV from which the referred-to terms are drawn.
     */
    private String version;
    /**
     * The URI for the resource. required
     */
    private String uri;

    Cv(String id, String fullName, String version, String uri)
    {
        requireNonNull(id);
        requireNonNull(fullName);
        requireNonNull(uri);

        this.id = id;
        this.fullName = fullName;
        this.version = version;
        this.uri = uri;
    }

    /**
     * @return id of this cv
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return name of this cv
     */
    public String getFullName()
    {
        return fullName;
    }

    /**
     * @return version of this cv
     */
    public String getVersion()
    {
        return version;
    }

    public String getUri()
    {
        return uri;
    }
}
