package omics.util.cv;

import java.io.Serializable;
import java.util.Objects;

/**
 * CvParam, CvTerm with value.
 *
 * @author JiaweiMao
 * @version 2.4.0
 * @since 27 Jul 2018, 10:29 AM
 */
public class CvParam implements Serializable
{
    private CvTerm cvTerm;
    /**
     * The value for the given term.
     */
    private String value;

    /**
     * Create a new CV param.
     *
     * @param cvTerm {@link CvTerm}
     * @param value  value of the param
     */
    public CvParam(CvTerm cvTerm, String value)
    {
        Objects.requireNonNull(cvTerm);

        this.cvTerm = cvTerm;
        this.value = value;
    }

    public CvTerm getCvTerm()
    {

        return cvTerm;
    }

    public void setCvTerm(CvTerm cvTerm)
    {

        this.cvTerm = cvTerm;
    }

    /**
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Sets the value
     *
     * @param value value to set
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CvParam cvParam = (CvParam) o;
        return cvTerm.equals(cvParam.cvTerm) &&
                value.equals(cvParam.value);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(cvTerm, value);
    }

}
