package omics.util.cv;

import omics.util.MetaKey;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * psi ms cv.
 *
 * @author JiaweiMao
 * @version 2.6.0
 * @since 02 May 2018, 12:06 PM
 */
public class CvTerm implements MetaKey
{
    /**
     * This counts distinct sequences hitting the protein without regard to a minimal confidence threshold.
     */
    public static final CvTerm DISTINCT_PEPTIDE_SEQUENCES = new CvTerm("MS:1001097", "distinct peptide sequences");
    /**
     * This counts the number of distinct peptide sequences. Multiple charge states and multiple modification states do
     * NOT count as multiple sequences. The definition of 'confident' must be qualified elsewhere.
     */
    public static final CvTerm CONFIDENT_DISTINCT_PEPTIDE_SEQUENCES = new CvTerm("MS:1001098", "confident distinct peptide sequences");
    /**
     * The rank of the protein in a list sorted by the search engine.
     */
    public static final CvTerm PROTEIN_RANK = new CvTerm("MS:1001301", "protein rank");

    public static final CvTerm A_H2O_DPR = new CvTerm("MS:1001148", "param: a ion-H2O");
    public static final CvTerm A_NH3_DPR = new CvTerm("MS:1001146", "param: a ion-NH3");
    public static final CvTerm B_NH3_DPR = new CvTerm("MS:1001149", "param: b ion-NH3");
    public static final CvTerm B_H2O_DPR = new CvTerm("MS:1001150", "param: b ion-H2O");
    public static final CvTerm Y_NH3_DPR = new CvTerm("MS:1001151", "param: y ion-NH3");
    public static final CvTerm Y_H2O_DPR = new CvTerm("MS:1001152", "param: y ion-H2O");
    public static final CvTerm Y = new CvTerm("MS:1001262", "param: y ion");
    public static final CvTerm NEUTRAL_LOSS = new CvTerm("MS:1000336", "neutral loss");

    /**
     * The ontology, required
     */
    private Cv cv;
    private final String accession;
    private final String name;
    private Cv unitCv;
    private String unitAccession;
    private String unitName;

    /**
     * Constructor.
     *
     * @param cv            {@link Cv} this term belong to.
     * @param accession     accession number
     * @param name          term name
     * @param unitCv        unic {@link Cv}
     * @param unitAccession unit accession number
     * @param unitName      unit name
     */
    public CvTerm(Cv cv, String accession, String name, Cv unitCv, String unitAccession, String unitName)
    {
        requireNonNull(cv);
        requireNonNull(accession);
        requireNonNull(name);

        this.cv = cv;
        this.accession = accession;
        this.name = name;
        this.unitCv = unitCv;
        this.unitAccession = unitAccession;
        this.unitName = unitName;
    }

    public CvTerm(Cv cv, String accession, String name)
    {
        this(cv, accession, name, null, null, null);
    }

    /**
     * Constructor, with default {@link Cv#PSI_MS}
     *
     * @param accession accession
     * @param name      name
     */
    public CvTerm(String accession, String name)
    {
        this(Cv.PSI_MS, accession, name);
    }

    /**
     * @return {@link Cv} of this {@link CvTerm}, default to be {@link Cv#PSI_MS}
     */
    public Cv getCv()
    {
        return cv;
    }

    public void setCv(Cv cv)
    {
        this.cv = cv;
    }

    /**
     * @return the cv reference, which is the id of the {@link Cv}.
     */
    public String getCvRef()
    {
        return cv.getId();
    }

    /**
     * @return the accession of the unit.
     */
    public String getUnitAccession()
    {
        return unitAccession;
    }

    public void setUnitAccession(String unitAccession)
    {
        this.unitAccession = unitAccession;
    }

    public String getUnitName()
    {
        return unitName;
    }

    public void setUnitName(String unitName)
    {
        this.unitName = unitName;
    }

    public Cv getUnitCv()
    {

        return unitCv;
    }

    public void setUnitCv(Cv unitCv)
    {
        this.unitCv = unitCv;
    }

    /**
     * @return the unit cv ref, which is the id of the unit cv.
     */
    public String getUnitCvRef()
    {
        return unitCv.getId();
    }

    /**
     * @return psi-ms cv accession.
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * @return PSI-MS cv term name
     */
    public String getName()
    {
        return name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CvTerm cvTerm = (CvTerm) o;
        return cv == cvTerm.cv &&
                accession.equals(cvTerm.accession) &&
                name.equals(cvTerm.name) &&
                unitCv == cvTerm.unitCv &&
                Objects.equals(unitAccession, cvTerm.unitAccession) &&
                Objects.equals(unitName, cvTerm.unitName);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(cv, accession, name, unitCv, unitAccession, unitName);
    }

    @Override
    public String getId()
    {
        return accession;
    }
}
