package omics.util.cv;

import java.util.Objects;

/**
 * Uncontrolled user parameters (essentially allowing free text). Before using these, one should verify whether
 * there is an appropriate CV term available, and if so, use the CV term instead
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 7:34 PM
 */
public class UserParam {

    protected String name;
    protected String type;
    protected String value;
    protected String unitAccession;
    protected String unitName;
    protected String unitCvRef;

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the value property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the unitAccession property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUnitAccession() {
        return unitAccession;
    }

    /**
     * Sets the value of the unitAccession property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUnitAccession(String value) {
        this.unitAccession = value;
    }

    /**
     * Gets the value of the unitName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * Sets the value of the unitName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUnitName(String value) {
        this.unitName = value;
    }

    /**
     * Gets the value of the unitCvRef property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUnitCvRef() {
        return unitCvRef;
    }

    /**
     * Sets the value of the unitCvRef property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUnitCvRef(String value) {
        this.unitCvRef = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserParam userParam = (UserParam) o;
        return Objects.equals(name, userParam.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
