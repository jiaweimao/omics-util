package omics.util.exception;

/**
 * Argument with dimension not match.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Jul 2020, 10:27 AM
 */
public class DimensionNotMatchException extends IllegalArgumentException
{
    private final int expectedDimension;
    private final int dimension;

    public DimensionNotMatchException(int expectedDimension, int dimension)
    {
        super("Dimension not match: " + expectedDimension + "!=" + dimension);
        this.expectedDimension = expectedDimension;
        this.dimension = dimension;
    }

    public DimensionNotMatchException(String message, Throwable cause, int expectedDimension, int dimension)
    {
        super(message, cause);
        this.expectedDimension = expectedDimension;
        this.dimension = dimension;
    }

    public int getExpectedDimension()
    {
        return expectedDimension;
    }

    public int getDimension()
    {
        return dimension;
    }
}
