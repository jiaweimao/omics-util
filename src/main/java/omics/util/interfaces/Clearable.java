package omics.util.interfaces;

/**
 * Common interface for objects that can be cleaned.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Jan 2019, 10:17 AM
 */
public interface Clearable
{
    /**
     * do clean action
     */
    void clear();
}
