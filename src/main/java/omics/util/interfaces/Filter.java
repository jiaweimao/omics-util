package omics.util.interfaces;

import javax.annotation.Nonnull;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * Filter interface.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 May 2018, 11:12 AM
 */
public interface Filter<T> extends Predicate<T>
{
    /**
     * Return true if given object pass the filter, false otherwise.
     *
     * @param filterable any object to be filtered.
     * @return true if given object pass the filter.
     */
    boolean test(T filterable);

    default Filter<T> and(Filter<? super T> other)
    {
        requireNonNull(other);
        return (t) -> test(t) && other.test(t);
    }

    default Filter<T> or(Filter<? super T> other)
    {
        requireNonNull(other);
        return (t) -> test(t) || other.test(t);
    }

    @Nonnull
    default Filter<T> negate()
    {
        return (t) -> !test(t);
    }
}
