package omics.util.interfaces;

/**
 * Identifiable gives object a unique identifier within the scope.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 22 Jan 2018, 8:54 PM
 */
public interface Identifiable
{
    /**
     * An id is an unambiguous string that is unique within the scope (i.e. a document, a set of related
     * documents, or a repository) of its use, the uniqueness of the id is responsible by user.
     *
     * @return a unique identifier of this object.
     */
    String getId();
}
