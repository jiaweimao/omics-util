package omics.util.interfaces;

/**
 * For any object with a double value.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Mar 2018, 8:15 AM
 */
public interface Measurable
{
    double getValue();
}
