package omics.util.interfaces;


/**
 * Interface for a task.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 31 Mar 2017, 8:09 AM
 */
public interface Task
{
    /**
     * Performs a task, most of the time it is a time consuming task such as IO.
     *
     * @throws Exception For any error.
     */
    void go() throws Exception;
}
