package omics.util.io;

import java.io.OutputStream;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Aug 2018, 10:55 AM
 */
public class CountingOutputStream extends ProxyOutputStream
{
    /**
     * The count of bytes that have passed.
     */
    private long count = 0;

    /**
     * Constructs a new CountingOutputStream.
     *
     * @param out the OutputStream to write to
     */
    public CountingOutputStream(final OutputStream out)
    {
        super(out);
    }

    /**
     * Updates the count with the number of bytes that are being written.
     *
     * @param n number of bytes to be written to the stream
     * @since 2.0
     */
    @Override
    protected synchronized void beforeWrite(final int n)
    {
        count += n;
    }

    /**
     * The number of bytes that have passed through this stream.
     * <p>
     * NOTE: From v1.3 this method throws an ArithmeticException if the
     * count is greater than can be expressed by an <code>int</code>.
     * See {@link #getByteCount()} for a method using a <code>long</code>.
     *
     * @return the number of bytes accumulated
     * @throws ArithmeticException if the byte count is too large
     */
    public int getCount()
    {
        final long result = getByteCount();
        if (result > Integer.MAX_VALUE) {
            throw new ArithmeticException("The byte count " + result + " is too large to be converted to an int");
        }
        return (int) result;
    }

    /**
     * Set the byte count back to 0.
     * <p>
     * NOTE: From v1.3 this method throws an ArithmeticException if the
     * count is greater than can be expressed by an <code>int</code>.
     * See {@link #resetByteCount()} for a method using a <code>long</code>.
     *
     * @return the count previous to resetting
     * @throws ArithmeticException if the byte count is too large
     */
    public int resetCount()
    {
        final long result = resetByteCount();
        if (result > Integer.MAX_VALUE) {
            throw new ArithmeticException("The byte count " + result + " is too large to be converted to an int");
        }
        return (int) result;
    }

    /**
     * The number of bytes that have passed through this stream.
     * <p>
     * NOTE: This method is an alternative for <code>getCount()</code>.
     * It was added because that method returns an integer which will
     * result in incorrect count for files over 2GB.
     *
     * @return the number of bytes accumulated
     */
    public synchronized long getByteCount()
    {
        return this.count;
    }

    /**
     * Set the byte count back to 0.
     * <p>
     * NOTE: This method is an alternative for <code>resetCount()</code>.
     * It was added because that method returns an integer which will
     * result in incorrect count for files over 2GB.
     *
     * @return the count previous to resetting
     */
    public synchronized long resetByteCount()
    {
        final long tmp = this.count;
        this.count = 0;
        return tmp;
    }
}
