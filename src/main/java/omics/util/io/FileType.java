package omics.util.io;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Common used File types.
 *
 * @author JiaweiMao
 * @version 2.9.0
 * @since 16 Mar 2017, 10:05 PM
 */
public class FileType implements Predicate<Path>
{
    public static final FileType CSV = new FileType(".csv", "*.csv");
    public static final FileType DAT = new FileType(".dat", "*.dat, *.DAT");
    public static final FileType DTA = new FileType(".dta", "*.dta");
    public static final FileType FASTA = new FileType(".fasta", "*.fasta, *.FASTA");
    public static final FileType GIF = new FileType(".gif", "*.gif, *.GIF");
    public static final FileType JPEG = new FileType(".jpeg", "*.jpeg, *.JPEG");
    public static final FileType JPG = new FileType(".jpg", "*.jpg, *.JPG");
    public static final FileType MGF = new FileType(".mgf", "*.mgf");
    public static final FileType MS2 = new FileType(".ms2", "*.ms2");
    public static final FileType MZDATA = new FileType(".mzData", "*.mzDATA");
    public static final FileType MZIdentML = new FileType(".mzid", "*.mzid");
    public static final FileType PEPXML = new FileType(".pepXML", "*.pepXML");
    public static final FileType MZXML = new FileType(".mzXML", "*.mzXML");
    public static final FileType MZML = new FileType(".mzML", "*.mzML");
    public static final FileType PKL = new FileType(".pkl", "*.pkl");
    public static final FileType PDF = new FileType(".pdf", "*.pdf, *.PDF");
    public static final FileType PNG = new FileType(".png", "*.png, *.PNG");
    public static final FileType SVG = new FileType(".svg", "*.svg, *.SVG");
    public static final FileType TSV = new FileType(".tsv", "*.tsv");
    public static final FileType TXT = new FileType(".txt", "*.txt");
    public static final FileType TIFF = new FileType(".tiff", "*.tiff, *.TIFF");
    public static final FileType XML = new FileType(".xml", "*.xml");
    public static final FileType XLS = new FileType(".xls", "*.xls, *.XLS");
    public static final FileType XLSX = new FileType(".xlsx", "*.xlsx, *.XLSX");

    /**
     * Create a new {@link FileType}
     *
     * @param ext         file extension
     * @param description file type description
     */
    public static FileType getFileType(String ext, String description)
    {
        return new FileType(ext, description);
    }

    private String extension;
    private String extensionDescription;

    public FileType(String extension, String extensionDescription)
    {
        this.extension = extension;
        this.extensionDescription = extensionDescription;
    }

    /**
     * @return extension of this file type
     */
    public String getExtension()
    {
        return extension;
    }

    /**
     * @return file type description
     */
    public String getDescription()
    {
        return extensionDescription;
    }

    /**
     * Return the FilenameFilter of this file extension.
     *
     * @param caseSensitive true if case sensitive.
     * @return FilenameFilter for this FileType
     */
    public FilenameFilter getFilter(boolean caseSensitive)
    {
        if (caseSensitive) {
            return ((dir, name) -> name.endsWith(extension));
        } else
            return (dir, name) -> name.toLowerCase().endsWith(extension.toLowerCase());
    }

    /**
     * @return the {@link FilenameFilter} with case insensitive
     */
    public FilenameFilter getFilter()
    {
        return getFilter(false);
    }

    /**
     * List files in the given directory of this {@link FileType}
     *
     * @param dir directory
     * @return file array, empty array for non.
     */
    public File[] listFiles(File dir)
    {
        File[] files = dir.listFiles(getFilter());
        if (files == null)
            return new File[0];
        return files;
    }

    /**
     * Return a {@link PathMatcher} of this file type.
     * The matching is case sensitive, are implementation-dependent and therefore not specified.
     *
     * @return the {@link PathMatcher} to match files with the extension.
     */
    public PathMatcher getPathMatcher()
    {
        return FileSystems.getDefault().getPathMatcher("glob:*" + getExtension());
    }

    /**
     * List all files in the directory of this type.
     *
     * @param dir a directory
     * @return files in the directory of this type.
     */
    public Path[] listPaths(Path dir)
    {
        checkArgument(Files.isDirectory(dir), "The path is not a directory: " + dir);

        List<Path> resultList = new ArrayList<>();
        try (DirectoryStream<Path> paths = Files.newDirectoryStream(dir, '*' + extension)) {
            for (Path path : paths) {
                resultList.add(path);
            }
        } catch (IOException ignored) {
        }
        return resultList.toArray(new Path[0]);
    }


    /**
     * if <code>path</code> is a file with the type, it is added to the <code>resultList</code>,
     * <p>
     * else if it is a directory, all files of this type in it are added to the resultList.
     *
     * @param path       file or directory
     * @param resultList {@link Path} list to return.
     * @return files of this type in the directory.
     */
    public List<Path> listPaths(Path path, List<Path> resultList)
    {
        checkNotNull(path);

        if (resultList == null)
            resultList = new ArrayList<>();

        if (Files.isDirectory(path)) {
            Path[] paths = listPaths(path);
            resultList.addAll(Arrays.asList(paths));
        } else if (test(path)) resultList.add(path);

        return resultList;
    }

    /**
     * list files in the given directory of given FileTYpe
     *
     * @param file     directory
     * @param fileType {@link FileType}
     * @return Files array, if non present, return an empty array.
     */
    public static File[] listFiles(File file, FileType fileType)
    {
        File[] files = file.listFiles(fileType.getFilter());
        if (files == null)
            files = new File[0];
        return files;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof FileType)) return false;
        FileType fileType = (FileType) o;
        return Objects.equals(extension, fileType.extension);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(extension);
    }

    /**
     * Return true if the path math this file extension.
     *
     * @param path a {@link Path}
     * @return true if the file extension match this FileType
     */
    @Override
    public boolean test(Path path)
    {
        String s = path.getFileName().toString();
        return s.toLowerCase().endsWith(extension.toLowerCase());
    }
}
