package omics.util.io;

import java.io.IOException;
import java.util.Iterator;

/**
 * A reader that can be iterated over.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 08 Mar 2017, 2:39 PM
 */
public interface IterativeReader<T> extends Iterator<T>, AutoCloseable
{
    /**
     * The empty IterativeReader.
     */
    IterativeReader<Object> EMPTY_READER = new IterativeReader<Object>()
    {
        @Override
        public boolean hasNext()
        {
            return false;
        }

        @Override
        public Object next()
        {
            return null;
        }

        @Override
        public void close()
        {
            // Do nothing because this reader is empty
        }
    };

    /**
     * Closes the reader and releases any system resources associated with it. Once the reader has been closed, further
     * hasNext() or next() invocations will throw an IOException. Closing a previously closed reader has no effect.
     *
     * @throws IOException If an I/O error occurs
     */
    void close() throws IOException;
}
