package omics.util.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;


/**
 * A simple reader which keep last read line.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 10:23 AM
 */
public class LineReader implements AutoCloseable
{
    private final BufferedReader reader;
    private String last = null;

    /**
     * Constructor
     *
     * @param reader A Reader
     */
    public LineReader(Reader reader)
    {
        if (reader instanceof BufferedReader) {
            this.reader = (BufferedReader) reader;
        } else {
            this.reader = new BufferedReader(reader);
        }
    }

    /**
     * Return next line of the file.
     *
     * @return A new line
     * @throws IOException If an I/O error occurs.
     */
    public String readLine() throws IOException
    {
        String line;
        if (last == null) {
            line = reader.readLine();
        } else {
            line = last;
            last = null;
        }

        return line;
    }

    /**
     * Return the next line, but do not affect {@link #readLine()} index.
     * continuously call peek() will return the same line.
     *
     * @return current line.
     * @throws IOException If an I/O error occurs.
     */
    public String peek() throws IOException
    {
        last = readLine();
        return last;
    }

    public void close() throws IOException
    {
        reader.close();
    }
}
