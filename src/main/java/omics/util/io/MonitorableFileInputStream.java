package omics.util.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Mar 2017, 4:28 PM
 */
public class MonitorableFileInputStream extends MonitorableInputStream
{
    /**
     * The maximum readable size.
     */
    private long iMaximum;

    /**
     * The current amount of bytes read from the file.
     */
    private long iCurrent;

    private double scale;

    /**
     * This constructor takes a file to load the FileInputStream from.
     *
     * @param aFile File to connect the input stream to.
     * @throws IOException when the file could not be raed.
     */
    public MonitorableFileInputStream(Path aFile) throws IOException
    {
        super(Files.newInputStream(aFile));
        long length = Files.size(aFile);
        int factor = 0;
        long max = Integer.MAX_VALUE;
        while (length >= max) {
            length /= 1024;
            factor++;
        }
        this.iMaximum = Files.size(aFile);
        this.scale = Math.pow(1024, factor);
        this.iCurrent = 0;
    }

    /**
     * This method reports on the maximum scale for the monitor.
     *
     * @return int with the maximum for the monitor.
     */
    public int getMaximum()
    {
        return (int) (this.iMaximum / scale);
    }

    /**
     * This method returns the progress (as measured by taking the value of iMaximum, minus the current progress as
     * reported by the cache).
     *
     * @return int with the progress.
     */
    public int getProgress()
    {
        return (int) ((this.iCurrent) / scale);
    }

    /**
     * {@inheritDoc}
     */
    public int read(byte b[]) throws IOException
    {
        int result = super.read(b);
        iCurrent += result;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public int read() throws IOException
    {
        int result = super.read();
        iCurrent++;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public int read(byte b[], int off, int len) throws IOException
    {
        int result = super.read(b, off, len);
        iCurrent += result;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public long skip(long n) throws IOException
    {
        long result = super.skip(n);
        iCurrent += n;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void reset() throws IOException
    {
        super.reset();
        iCurrent = 0L;
    }
}
