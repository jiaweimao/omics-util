package omics.util.io;

import omics.util.utils.SystemUtils;

import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * For the io of parameter file.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Feb 2020, 3:20 PM
 */
public interface ParameterFile<T>
{
    String SERIALIZATION_FOLDER = SystemUtils.USER_HOME + "/.omics";
    /**
     * Configuration folder, add custom settings.
     */
    String CONFIG_FOLDER = "conf";

    default List<T> initialize(ClassLoader classLoader) throws IOException, XMLStreamException
    {
        List<T> list = new ArrayList<>();
        String fileName = getSerializeFileName();
        Path path = Paths.get(SERIALIZATION_FOLDER, fileName);
        if (Files.exists(path)) {
            try {
                List<T> serializedList = read(path);
                list.addAll(serializedList);
            } catch (IOException | XMLStreamException e) {
                Files.delete(path);
                List<T> embeddedList = read(classLoader.getResourceAsStream(fileName));
                list.addAll(embeddedList);
            }
        } else {
            List<T> embeddedList = read(classLoader.getResourceAsStream(fileName));
            list.addAll(embeddedList);
        }
        Path customPath = Paths.get(CONFIG_FOLDER, fileName);
        if (Files.exists(customPath)) {
            List<T> customList = read(customPath);
            list.addAll(customList);
        }
        return list;
    }

    /**
     * @return all items in this parameter file
     */
    List<T> getItemList();

    /**
     * @return number of elements in the parameter file
     */
    int size();

    /**
     * Read parameters from the inputstream.
     *
     * @param inputStream an {@link InputStream} from parameter file
     */
    List<T> read(InputStream inputStream) throws XMLStreamException, IOException;

    /**
     * Write parameters to the outputstream
     *
     * @param outputStream an {@link OutputStream} to write parameters.
     */
    void write(OutputStream outputStream) throws XMLStreamException, IOException;

    /**
     * add a new element
     *
     * @param element element to add
     * @return true if add successfully
     */
    boolean add(T element);

    /**
     * @return name of the serialization file
     */
    String getSerializeFileName();

    /**
     * Delte the cached user defined parameter file
     *
     * @return true if delete successfully
     */
    default boolean deleteSerializeFile()
    {
        Path path = Paths.get(SERIALIZATION_FOLDER, getSerializeFileName());
        if (Files.notExists(path))
            return true;
        try {
            Files.delete(path);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Read parameters from given file
     *
     * @param path file path
     */
    default List<T> read(String path) throws IOException, XMLStreamException
    {
        return read(Paths.get(path));
    }

    /**
     * Read parameters from given file
     *
     * @param path file {@link Path}
     */
    default List<T> read(Path path) throws IOException, XMLStreamException
    {
        return read(new BufferedInputStream(Files.newInputStream(path)));
    }

    /**
     * Write parameters to the file path.
     *
     * @param absolutePath absolute file path.
     */
    default void write(String absolutePath) throws IOException, XMLStreamException
    {
        write(Paths.get(absolutePath));
    }

    /**
     * Write parameters to the path.
     *
     * @param path target {@link Path}
     */
    default void write(Path path) throws IOException, XMLStreamException
    {
        write(new BufferedOutputStream(Files.newOutputStream(path)));
    }

    /**
     * write items to the default serialize location
     */
    default void write()
    {
        Path path = Paths.get(SERIALIZATION_FOLDER, getSerializeFileName());
        try {
            Path parent = path.getParent();
            if (Files.notExists(parent))
                Files.createDirectories(parent);
            write(path);
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
