package omics.util.io.xml;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static omics.util.utils.ObjectUtils.checkArgument;


/**
 * XML Utilities.
 *
 * @author JiaweiMao
 * @version 1.10.1
 * @since 21 Mar 2017, 9:40 AM
 */
public class XMLUtils
{
    private static final Pattern xmlHeader = Pattern.compile(".*<\\?xml.+\\?>.*", Pattern.DOTALL);
    private static final Pattern xmlEnc = Pattern.compile(".*encoding\\s*=\\s*[\"']([A-Za-z]([A-Za-z0-9._]|[-])*)" +
            "[\"'](.*)", Pattern.DOTALL);

    /**
     * Create a {@link XMLStreamWriter}.
     *
     * @param targetFile target xml file path.
     * @return a {@link XMLStreamWriter} instance.
     */
    public static XMLStreamWriter createXMLStreamWriter(String targetFile)
    {
        XMLOutputFactory xof = XMLOutputFactory.newFactory();
        XMLStreamWriter writer = null;
        try {
            writer = xof.createXMLStreamWriter(new BufferedWriter(new FileWriter(targetFile)));
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
        return writer;
    }

    /**
     * Create an {@link IndentXMLStreamWriter} to output formatted xml writer.
     *
     * @param targetFile target file path.
     * @return a {@link XMLStreamWriter} instance.
     */
    public static XMLStreamWriter createIndentXMLStreamWriter(Path targetFile)
    {
        IndentXMLStreamWriter writer = null;
        try {
            writer = new IndentXMLStreamWriter(targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer;
    }

    /**
     * Create an {@link IndentXMLStreamWriter} to output formatted xml writer.
     *
     * @param targetFile target file path.
     * @return a {@link XMLStreamWriter} instance.
     */
    public static XMLStreamWriter createIndentXMLStreamWriter(String targetFile)
    {
        IndentXMLStreamWriter writer = null;
        try {
            writer = new IndentXMLStreamWriter(Paths.get(targetFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer;
    }

    /**
     * Create an {@link IndentXMLStreamWriter} to output formatted xml writer.
     *
     * @param outputStream target stream.
     * @return a {@link XMLStreamWriter} instance.
     */
    public static XMLStreamWriter createIndentXMLStreamWriter(OutputStream outputStream)
    {
        IndentXMLStreamWriter writer = null;
        try {
            writer = new IndentXMLStreamWriter(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer;
    }

    /**
     * Create a {@link XMLStreamReader} of given file
     *
     * @param file file path
     * @return {@link XMLStreamReader}
     */
    public static XMLStreamReader createXMLStreamReader(String file)
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        xmlInputFactory.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        XMLStreamReader reader = null;
        try {
            reader = xmlInputFactory.createXMLStreamReader(new BufferedReader(new FileReader(file)));
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return reader;
    }

    /**
     * Create a {@link XMLStreamReader} of given file
     *
     * @param path file path
     * @return {@link XMLStreamReader}
     */
    public static XMLStreamReader createXMLStreamReader(Path path)
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        xmlInputFactory.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        XMLStreamReader reader = null;
        try {
            reader = xmlInputFactory.createXMLStreamReader(Files.newBufferedReader(path));
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
        return reader;
    }

    /**
     * Create a {@link XMLStreamReader} of given {@link InputStream}
     *
     * @param inputStream an {@link InputStream}
     * @return {@link XMLStreamReader} for this {@link InputStream}
     */
    public static XMLStreamReader createXMLStreamReader(InputStream inputStream)
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        xmlInputFactory.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        XMLStreamReader reader = null;
        try {
            if (inputStream instanceof BufferedInputStream) {
                reader = xmlInputFactory.createXMLStreamReader(inputStream);
            } else {
                reader = xmlInputFactory.createXMLStreamReader(new BufferedInputStream(inputStream));
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return reader;
    }

    /**
     * detect the xml file encoding
     *
     * @param aFile         a file
     * @param maxReadLength max number of byte to read to detect the encoding
     * @return String representation of the encoding
     */
    public static String detectFileEncoding(File aFile, int maxReadLength) throws IOException
    {
        checkArgument(aFile.exists(), "The file is not present: " + aFile.getName());

        // read a bit of the input file and check if it contains a XML header
        InputStream in = new FileInputStream(aFile);
        int length = in.available();

        // read a maximum of maxReadLength bytes
        byte[] bytes;
        if (length > maxReadLength) {
            bytes = new byte[maxReadLength];
        } else {
            bytes = new byte[length];
        }
        // fill the byte buffer
        in.read(bytes);
        in.close();
        // convert the bytes to String using ASCII
        String fileStart = new String(bytes, "ASCII");

        // first check if there is a XML header
        Matcher mHead = xmlHeader.matcher(fileStart);
        if (!mHead.matches()) {
            return null;
        }
        Matcher mEnc = xmlEnc.matcher(fileStart);
        if (!mEnc.matches()) {
            return null;
        }
        if (mEnc.groupCount() < 1) {
            return null;
        }
        return mEnc.group(1);
    }

    /**
     * This method reads up to maxReadLength bytes from the file specified by fileLocation and will try to
     * detect the character encoding.
     * This simple detection is assuming a file according to XML 1.1 specs, where
     * the encoding should be provided in the XML header/prolog. It will only try
     * to parse this information from the read bytes.
     *
     * @param fileLocation  The location of the file to check.
     * @param maxReadLength The maximum number of bytes to read from the file.
     * @return A String representing the Charset detected for the provided file or null if no character encoding could
     * be determined.
     * @throws IOException If the specified location could not be opened for reading.
     */
    public static String detectFileEncoding(URL fileLocation, int maxReadLength) throws IOException
    {
        // read a bit of the input file and check if it contains a XML header
        InputStream in = fileLocation.openStream();
        int length = in.available();

        // read a maximum of maxReadLength bytes
        byte[] bytes;
        if (length > maxReadLength) {
            bytes = new byte[maxReadLength];
        } else {
            bytes = new byte[length];
        }
        // fill the byte buffer
        in.read(bytes);
        in.close();
        // convert the bytes to String using ASCII
        String fileStart = new String(bytes, "ASCII");

        // first check if there is a XML header
        Matcher mHead = xmlHeader.matcher(fileStart);
        if (!mHead.matches()) {
            return null;
        }
        Matcher mEnc = xmlEnc.matcher(fileStart);
        if (!mEnc.matches()) {
            return null;
        }
        if (mEnc.groupCount() < 1) {
            return null;
        }
        return mEnc.group(1);
    }

    /**
     * return true if the <code>reader</code> is pointing to the start of the element <code>tag</code>
     *
     * @param reader {@link XMLStreamReader}
     * @param tag    element tag
     */
    public static boolean isStartElement(XMLStreamReader reader, String tag)
    {
        return reader.getEventType() == XMLStreamConstants.START_ELEMENT && reader.getLocalName().equals(tag);
    }

    /**
     * return true if the <code>reader</code> current point to the end of the Element with the <code>tag</code>
     *
     * @param reader {@link XMLStreamReader}
     * @param tag    element tag
     */
    public static boolean isEndElement(XMLStreamReader reader, String tag)
    {
        return reader.getEventType() == XMLStreamConstants.END_ELEMENT && reader.getLocalName().equals(tag);
    }

    /**
     * return true if the {@link XMLEvent} is an endElement of given tag name
     *
     * @param event {@link XMLEvent}
     * @param tag   element tag
     */
    public static boolean isEndElement(XMLEvent event, String tag)
    {
        return event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(tag);
    }

    /**
     * return true if the {@link XMLEvent} is a startElement of give local name
     *
     * @param event {@link XMLEvent} to test
     * @param tag   element local name
     */
    public static boolean isStartElement(XMLEvent event, String tag)
    {
        return event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(tag);
    }

    /**
     * Seek to the next start tag.
     *
     * @param reader  xml reader.
     * @param tagName the start element tag.
     * @return true if find the start element.
     * @throws XMLStreamException
     */
    public static boolean toStartElement(XMLStreamReader reader, String tagName) throws XMLStreamException
    {
        while (reader.hasNext()) {

            int event = reader.next();
            if (event == XMLStreamConstants.START_ELEMENT && reader.getLocalName().equals(tagName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return the value of given attribute name.
     *
     * @param xmlSR {@link XMLStreamReader}
     * @param name  attribute name
     * @return attribute name, null for absent.
     */
    public static String attr(XMLStreamReader xmlSR, String name)
    {
        return xmlSR.getAttributeValue(null, name);
    }

    /**
     * Return the value of given attribute name.
     *
     * @param xmlSR {@link XMLStreamReader}
     * @param name  attribute name
     * @return attribute value
     * @throws XMLStreamException for attribute absent.
     */
    public static String mandatoryAttr(XMLStreamReader xmlSR, String name) throws XMLStreamException
    {
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue == null) {
            throw new XMLStreamException(String.format("%s miss attr %s", xmlSR.getLocalName(), name));
        }
        return attributeValue;
    }

    /**
     * Return the value of given attribute.
     *
     * @param xmlSR a {@link XMLStreamReader}
     * @param name  attribute name
     * @return attribute boolean value, null for absent.
     */
    public static Boolean booleanAttr(XMLStreamReader xmlSR, String name)
    {
        Boolean result = null;
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue != null)
            result = Boolean.parseBoolean(attributeValue);

        return result;
    }

    /**
     * Return the value of given attribute.
     *
     * @param xmlSR a {@link XMLStreamReader}
     * @param name  attribute name
     * @return attribute boolean value.
     */
    public static Boolean mandatoryBooleanAttr(XMLStreamReader xmlSR, String name) throws XMLStreamException
    {
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue == null)
            throw new XMLStreamException(String.format("%s miss attr %s", xmlSR.getLocalName(), name));

        return Boolean.parseBoolean(attributeValue);
    }

    /**
     * Return the value of given attribute.
     *
     * @param xmlSR a {@link XMLStreamReader}
     * @param name  attribute name
     * @return attribute double value, null for absent.
     */
    public static Double doubleAttr(XMLStreamReader xmlSR, String name)
    {
        Double value = null;
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue != null)
            value = Double.parseDouble(attributeValue);
        return value;
    }

    /**
     * Return the value of given attribute name.
     *
     * @param xmlSR {@link XMLStreamReader}
     * @param name  attribute name
     * @return attribute value
     * @throws XMLStreamException for attribute absent.
     */
    public static Double mandatoryDoubleAttr(XMLStreamReader xmlSR, String name) throws XMLStreamException
    {
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue == null) {
            throw new XMLStreamException(String.format("%s miss attr %s", xmlSR.getLocalName(), name));
        }
        return Double.parseDouble(attributeValue);
    }

    /**
     * Return the value of given attribute that must exist.
     *
     * @return integer attribute value, null for absent.
     */
    public static Integer intAttr(XMLStreamReader xmlSR, String name)
    {
        Integer value = null;
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue != null)
            value = Integer.parseInt(attributeValue);
        return value;
    }

    /**
     * Return the value of given attribute that must exist.
     *
     * @return integer attribute value, null for absent.
     */
    public static Integer mandatoryIntAttr(XMLStreamReader xmlSR, String name) throws XMLStreamException
    {
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue == null) {
            throw new XMLStreamException(String.format("%s miss attr %s", xmlSR.getLocalName(), name));
        }
        return Integer.parseInt(attributeValue);
    }

    /**
     * Return the value of given attributes.
     *
     * @return Float attribute value, null for absent
     */
    public static Float floatAttr(XMLStreamReader xmlSR, String name)
    {
        Float value = null;
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue != null) {
            value = Float.parseFloat(attributeValue);
        }
        return value;
    }

    /**
     * Return the value of given attributes.
     *
     * @return Float attribute value, null for absent
     */
    public static Float mandatoryFloatAttr(XMLStreamReader xmlSR, String name) throws XMLStreamException
    {
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue == null) {
            throw new XMLStreamException(String.format("%s miss attr %s", xmlSR.getLocalName(), name));
        }
        return Float.parseFloat(attributeValue);
    }

    /**
     * Return the value of given attributes.
     *
     * @return Long attribute value, null for absent
     */
    public static Long longAttr(XMLStreamReader xmlSR, String name)
    {
        Long value = null;
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue != null) {
            value = Long.parseLong(attributeValue);
        }
        return value;
    }


    /**
     * Return the value of given attributes.
     *
     * @return Long attribute value
     */
    public static Long mandatoryLongAttr(XMLStreamReader xmlSR, String name) throws XMLStreamException
    {
        String attributeValue = xmlSR.getAttributeValue(null, name);
        if (attributeValue == null) {
            throw new XMLStreamException(String.format("%s miss attr %s", xmlSR.getLocalName(), name));
        }
        return Long.parseLong(attributeValue);
    }

    /**
     * Seek to the next start tag
     *
     * @param reader  the xml reader
     * @param tagName the start element name to return
     * @return optional scan start element
     */
    public static Optional<StartElement> toStartElement(XMLEventReader reader, String tagName) throws XMLStreamException
    {
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                StartElement se = event.asStartElement();

                if (se.getName().getLocalPart().equals(tagName)) {
                    return Optional.of(se);
                }
            }
        }

        return Optional.empty();
    }

    /**
     * seek to the next start tag.
     *
     * @param tagName    element tag name
     * @param altTagName alternative tag name
     */
    public static Optional<StartElement> toStartElement(XMLEventReader reader, String tagName, String altTagName) throws XMLStreamException
    {
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                StartElement se = event.asStartElement();
                String name = se.getName().getLocalPart();
                if (name.equals(tagName) || name.equals(altTagName))
                    return Optional.of(se);
            }
        }

        return Optional.empty();
    }

    /**
     * Seek to the next start element.
     *
     * @param reader xml reader
     */
    public static void nextStartElement(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == XMLStreamConstants.START_ELEMENT)
                return;
        }
    }

    /**
     * Return the next start element only if match tagName.
     *
     * @param reader  the xml reader
     * @param tagName the start element name to return
     * @return optional StartElement
     */
    public static Optional<StartElement> nextStartElementIf(XMLEventReader reader,
                                                            String tagName) throws XMLStreamException
    {
        while (reader.hasNext()) {

            XMLEvent event = reader.peek();

            // test the next start element for tagName and return anyway
            if (event.isStartElement()) {

                StartElement se = event.asStartElement();

                if (se.getName().getLocalPart().equals(tagName)) {

                    // consume this start element
                    reader.nextEvent();

                    return Optional.of(se);
                } else {

                    return Optional.empty();
                }
            }
            // consume all non-start elements
            else {

                reader.nextEvent();
            }
        }

        return Optional.empty();
    }

    /**
     * Find the next Characters event and return.
     *
     * @param reader xml reader
     * @return next Character event if it is present.
     */
    public static Optional<Characters> seekNextCharacters(XMLEventReader reader) throws XMLStreamException
    {
        while (reader.hasNext()) {

            XMLEvent event = reader.nextEvent();

            if (event.isCharacters()) {

                Characters chars = event.asCharacters();

                if (reader.peek().isCharacters()) {

                    throw new XMLStreamException("Please set property 'javax.xml.stream.isCoalescingproperty' of " +
                            "XMLInputFactory to true !");
                }

                if (!chars.isWhiteSpace()) {

                    return Optional.of(chars);
                }
            }
        }

        return Optional.empty();
    }

    /**
     * Return the next event if it is a Character.
     *
     * @param reader xml reader
     * @return Optional Characters event.
     * @throws XMLStreamException any io exception.
     */
    public static Optional<Characters> nextCharacters(XMLEventReader reader) throws XMLStreamException
    {
        if (reader.hasNext()) {

            XMLEvent event = reader.nextEvent();
            if (event.isCharacters()) {
                Characters chars = event.asCharacters();

                if (reader.peek().isCharacters()) {
                    throw new XMLStreamException("Please set property 'javax.xml.stream.isCoalescingproperty' of " +
                            "XMLInputFactory to true !");
                }

                if (!chars.isWhiteSpace()) {
                    return Optional.of(chars);
                }
            }
        }

        return Optional.empty();
    }

    public static void nextCharacters(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == XMLStreamConstants.CHARACTERS)
                return;
        }
    }

    /**
     * Return the attribute value of given name, null for absent.
     *
     * @param startElement  {@link StartElement}
     * @param attributeName attribute name
     * @return attribute value, null for absent.
     */
    public static String attr(StartElement startElement, String attributeName)
    {
        Attribute attribute = startElement.getAttributeByName(QName.valueOf(attributeName));

        if (attribute != null) {
            return attribute.getValue();
        }

        return null;
    }

    public static Optional<String> getOptionalAttribute(XMLStreamReader reader, String attributeName)
    {
        String value = reader.getAttributeValue(null, attributeName);
        if (value != null) {
            return Optional.of(value);
        }
        return Optional.empty();
    }

    public static String mandatoryAttr(StartElement startElement, String attributeName) throws XMLStreamException
    {
        String optAttribute = attr(startElement, attributeName);
        if (optAttribute != null) {
            return optAttribute;
        }
        throw new XMLStreamException("mandatory attribute " + attributeName + " is missing from start element " +
                startElement.getName().getLocalPart() + "!");
    }

    public static Integer mandatoryIntAttr(StartElement startElement, String attributeName) throws XMLStreamException
    {
        String optAttribute = attr(startElement, attributeName);
        if (optAttribute != null)
            return Integer.parseInt(optAttribute);

        throw new XMLStreamException("mandatory attribute " + attributeName + " is missing from start element " +
                startElement.getName().getLocalPart() + "!");
    }

    public static Double mandatoryDoubleAttr(StartElement startElement, String attributeName) throws XMLStreamException
    {
        String optAttribute = attr(startElement, attributeName);
        if (optAttribute != null)
            return Double.parseDouble(optAttribute);

        throw new XMLStreamException("mandatory attribute " + attributeName + " is missing from start element " +
                startElement.getName().getLocalPart() + "!");
    }

    public static String mandatoryAttr(StartElement startElement, String attributeName, String...
            alternateNames) throws XMLStreamException
    {
        String optAttribute = attr(startElement, attributeName);
        if (optAttribute != null) {
            return optAttribute;
        } else {
            for (String name : alternateNames) {
                optAttribute = attr(startElement, name);
                if (optAttribute != null)
                    return optAttribute;
            }
        }

        throw new XMLStreamException("mandatory attribute " + attributeName + " is missing from start element " +
                startElement.getName().getLocalPart() + "!");
    }

    public static String getMandatoryAttribute(XMLStreamReader reader, String attributeName,
                                               String... alternateNames) throws XMLStreamException
    {
        Optional<String> optionalAttribute = getOptionalAttribute(reader, attributeName);
        if (optionalAttribute.isPresent()) {
            return optionalAttribute.get();
        } else {
            for (String name : alternateNames) {
                optionalAttribute = getOptionalAttribute(reader, name);
                if (optionalAttribute.isPresent()) return optionalAttribute.get();
            }
        }

        throw new XMLStreamException("mandatory attribute " + attributeName + " is missing from start element " + reader.getLocalName() + "!");
    }

    public static <E extends XMLEvent> E getMandatoryXMLEvent(Optional<E> mandatoryEvent,
                                                              String message) throws XMLStreamException
    {
        if (mandatoryEvent.isPresent()) {

            return mandatoryEvent.get();
        }

        throw new XMLStreamException(message + ": XMLEvent is missing!");
    }

    /**
     * Return every events until reaching start element named <code>tagName</code>
     *
     * @param reader  the xml event reader
     * @param tagName the tag name
     * @return absent if start element named <code>tagName</code> or end of file
     * @throws XMLStreamException
     */
    private static Optional<XMLEvent> nextEventUntilStartElement(XMLEventReader reader, String tagName) throws
            XMLStreamException
    {
        if (reader.hasNext()) {

            XMLEvent event = reader.peek();

            if (event.isStartElement()) {

                StartElement se = event.asStartElement();

                if (se.getName().getLocalPart().equals(tagName)) {

                    return Optional.empty();
                }
            }

            return Optional.of(reader.nextEvent());
        }

        // no more events
        return Optional.empty();
    }

    public static void writeAllEventsUntilStartElement(XMLEventReader reader, String tagName, Writer writer)
            throws XMLStreamException
    {
        while (reader.hasNext()) {

            Optional<XMLEvent> optEvent = XMLUtils.nextEventUntilStartElement(reader, tagName);

            if (optEvent.isPresent()) {

                optEvent.get().writeAsEncodedUnicode(writer);
            } else {

                break;
            }
        }
    }
}
