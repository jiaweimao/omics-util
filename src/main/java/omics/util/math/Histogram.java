package omics.util.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Nov 2018, 9:31 AM
 */
public class Histogram
{
    private ArrayList<Bin> bins;
    private final double[] values;
    private final double start;
    private final double end;
    private final double binSize;

    private final Bin leftExtreme;
    private final Bin rightExtreme;

    /**
     * construct a histogram
     *
     * @param values  values to distribute
     * @param start   start of the histogram
     * @param end     end of the histogram
     * @param binSize size of bin
     */
    public Histogram(double[] values, double start, double end, double binSize)
    {
        Objects.requireNonNull(values);
        checkArgument(values.length > 0, "The values is empty");
        checkArgument(start < end, "start should less than end");
        checkArgument(binSize > 0, "bin size should &gt; 0");

        this.values = Arrays.copyOf(values, values.length);
        this.start = start;
        this.end = end;
        this.binSize = binSize;

        this.leftExtreme = new Bin(Double.NEGATIVE_INFINITY, start);
        this.rightExtreme = new Bin(end, Double.POSITIVE_INFINITY);
        init();
    }

    private void init()
    {
        int binCount = (int) Math.ceil((end - start) / binSize);
        this.bins = new ArrayList<>(binCount);
        double[] leftBounds = new double[binCount];
        double[] rightBounds = new double[binCount];

        for (int i = 0; i < binCount; i++) {
            leftBounds[i] = start + i * binSize;
            rightBounds[i] = start + (i + 1) * binSize;
            this.bins.add(new Bin(leftBounds[i], rightBounds[i], i));
        }

        for (double value : values) {
            int index = Arrays.binarySearch(leftBounds, value);
            if (index < 0)
                index = -index - 1;

            if (index == 0) { // left most
                if (value == leftBounds[0]) {
                    bins.get(0).increase();
                } else {
                    leftExtreme.increase();
                }
            } else if (index == leftBounds.length) { // right most
                if (value <= rightBounds[rightBounds.length - 1]) {
                    bins.get(rightBounds.length - 1).increase();
                } else {
                    rightExtreme.increase();
                }
            } else { // the index of the first element greater than the key
                if (value == leftBounds[index]) {
                    bins.get(index).increase();
                } else {
                    bins.get(index - 1).increase();
                }
            }
        }
    }

    /**
     * @return bins of this Histogram
     */
    public ArrayList<Bin> getBins()
    {
        return bins;
    }

    /**
     * @return the {@link Bin} with all values smaller than the start
     */
    public Bin getLeftExtreme()
    {
        return leftExtreme;
    }

    /**
     * @return the {@link Bin} with all values greater than the end.
     */
    public Bin getRightExtreme()
    {
        return rightExtreme;
    }

    public double getStart()
    {
        return start;
    }

    public double getEnd()
    {
        return end;
    }

    public double getBinSize()
    {
        return binSize;
    }

    public static class Bin
    {
        private final double leftBound;
        private final double rightBound;
        private int index;
        private int count = 0;

        /**
         * Construct a bin
         *
         * @param leftBound  bin left bound, inclusive
         * @param rightBound bin right bound, exclusive, but for the last bin inclusive.
         */
        public Bin(double leftBound, double rightBound)
        {
            this.leftBound = leftBound;
            this.rightBound = rightBound;
        }

        public Bin(double leftBound, double rightBound, int index)
        {
            this.leftBound = leftBound;
            this.rightBound = rightBound;
            this.index = index;
        }

        /**
         * increase the count of this bin by 1
         */
        public void increase()
        {
            this.count++;
        }

        /**
         * increase the count of this bin by given number
         *
         * @param count increased value
         */
        public void increase(int count)
        {
            this.count += count;
        }

        /**
         * @return count of this bin
         */
        public int getCount()
        {
            return count;
        }

        public double getLeftBound()
        {
            return leftBound;
        }

        public double getRightBound()
        {
            return rightBound;
        }

        public int getIndex()
        {
            return index;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        @Override
        public String toString()
        {
            return "Bin{" +
                    "leftBound=" + leftBound +
                    ", rightBound=" + rightBound +
                    ", index=" + index +
                    ", count=" + count +
                    '}';
        }
    }

}
