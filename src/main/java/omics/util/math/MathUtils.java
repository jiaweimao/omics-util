package omics.util.math;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import omics.util.utils.ArrayUtils;
import omics.util.utils.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.*;

/**
 * Math utilities.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 20 Mar 2017, 3:36 PM
 */
public class MathUtils
{
    private MathUtils() { }

    /**
     * Utility method for computing the factorial n! of a number n. NOTE: the
     * resulting factorial is only valid in the scope of the primitive type
     * 'long', so the largest factorial is 20!.
     *
     * @param n The integer number of which to compute the factorial.
     * @return The factorial number n!.
     */
    public static long factorial(int n)
    {
        // check that we have a number we can actually calculate the factorial for.
        if (n < 0 || n > 20) {
            throw new IllegalArgumentException("The factorial can only "
                    + "be calculated for numbers n with 0 <= n <= 20 (currently n = " + n + "). Please try to " +
                    "consider less modifications.");
        }
        // handle the trivial cases
        if (n == 0 || n == 1) {
            return 1;
        }
        // calculate the factorial
        long f = 1;
        for (int i = 2; i <= n; i++) {
            f *= i;
        }
        return f;
    }

    /**
     * The input is an array with the same size of box count, and the array value is the number of balls.
     * we need to get a ball from each of the box, with all combinations. such as array:
     * [1, 2, 3]
     * <p>
     * It is mean the 1st box has 1 ball, the 2ed box has 2 balls, the 3rd box has 3 ball, possible combinations are:
     * 1 * 2 * 3 = 6
     * <p>
     * [0, 0, 0], [0, 0, 1], [0, 0, 2], [0, 1, 0], [0, 1, 1], [0, 1, 2]
     * <p>
     * the values of the combinations can be treated as the index of the ball in the box.
     *
     * @param input input array.
     * @return all combinations.
     */
    public static List<int[]> combination(int[] input)
    {
        List<int[]> resultList = new ArrayList<>();
        int size = input.length;

        // check argument, values less than 0 is not allowed.
        if (size < 1)
            return resultList;

        for (int value : input) {
            if (value <= 0)
                return resultList;
        }

        List<IntList> results = new ArrayList<>();
        int first = input[0];
        for (int i = 0; i < first; i++) {
            IntList list = new IntArrayList();
            list.add(i);
            results.add(list);
        }

        if (size > 1) {
            for (int boxId = 1; boxId < size; boxId++) {
                List<IntList> tmpList = new ArrayList<>();
                for (int ballId = 0; ballId < input[boxId]; ballId++) {
                    for (IntList list : results) {
                        IntList newList = new IntArrayList(list);
                        newList.add(ballId);
                        tmpList.add(newList);
                    }
                }

                results = tmpList;
            }
        }

        for (IntList list : results) {
            resultList.add(list.toIntArray());
        }
        return resultList;
    }


    /**
     * Calculation all combinations of choose K element from input. Duplicate value is not allowed.
     * This algorithm adapt from
     * http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
     * <p>
     * for array [1, 2, 3]:
     * permutation(array, 2) could generate [1, 2], [1, 3], [2, 3]
     *
     * @param input input data, duplicate value is not allowed.
     * @param k     K value
     * @return all combinations.
     */
    public static List<int[]> permutation(int[] input, int k)
    {
        ObjectUtils.checkArgument(k > 0, "K should > 0");

        List<int[]> resultList = new ArrayList<>();
        int[] data = new int[k];
        permutation(input, data, 0, input.length - 1, 0, k, resultList);
        return resultList;
    }

    /**
     * Generate all combinations of given input data.
     *
     * @param input      input data.
     * @param data       temporary array to store current combination.
     * @param start      start index in input
     * @param end        end index in input
     * @param index      current index in data
     * @param k          size of a combination
     * @param resultList List to store the result permutations.
     */
    private static void permutation(int[] input, int[] data, int start, int end, int index, int k,
            List<int[]> resultList)
    {
        if (index == k) {
            resultList.add(Arrays.copyOf(data, k));
            return;
        }

        // replace index with all possible elements. The condition "end-i+1 >= r-index" makes sure that including
        // one element at index will make a combination with remaining elements at remaining positions
        for (int i = start; i <= end && end - i + 1 >= k - index; i++) {
            data[index] = input[i];
            permutation(input, data, i + 1, end, index + 1, k, resultList);
        }
    }

    /**
     * Calculation all combinations of choose K element from input. Duplicate values are allowed.
     * This algorithm adopt from
     * http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
     * <p>
     * int arr[] = {1, 2, 2, 4, 5};
     * int k = 3;
     * List<int[]> list = MathUtils.permutationDup(arr, k);
     * assertEquals(7, list.size());
     * assertArrayEquals(new int[]{1, 2, 2}, list.get(0));
     * assertArrayEquals(new int[]{1, 2, 4}, list.get(1));
     * assertArrayEquals(new int[]{1, 2, 5}, list.get(2));
     * assertArrayEquals(new int[]{1, 4, 5}, list.get(3));
     * assertArrayEquals(new int[]{2, 2, 4}, list.get(4));
     * assertArrayEquals(new int[]{2, 2, 5}, list.get(5));
     * assertArrayEquals(new int[]{2, 4, 5}, list.get(6));
     *
     * @param input input data, duplicate values are allowed.
     * @param k     K value, should > 0.
     * @return all combinations.
     */
    public static List<int[]> permutationDup(int[] input, int k)
    {
        ObjectUtils.checkArgument(k > 0, "K should > 0");
        ObjectUtils.checkArgument(k <= input.length, "The K should <= the input length.");

        List<int[]> resultList = new ArrayList<>();
        int[] data = new int[k];
        Arrays.sort(input);

        permutationDup(input, data, 0, input.length - 1, 0, k, resultList);
        return resultList;
    }

    /**
     * Generate all combinations of given input data, duplicate values are allowed.
     *
     * @param input      input data, should in ascending order.
     * @param data       temporary array to store current combination.
     * @param start      start index in input
     * @param end        end index in input
     * @param index      current index in data
     * @param k          size of a combination
     * @param resultList List to store the result permutations.
     */
    private static void permutationDup(int[] input, int[] data, int start, int end, int index, int k, List<int[]>
            resultList)
    {
        if (index == k) {
            resultList.add(Arrays.copyOf(data, k));
            return;
        }

        // replace index with all possible elements. The condition "end-i+1 >= r-index" makes sure that including
        // one element at index will make a combination with remaining elements at remaining positions
        for (int i = start; i <= end && end - i + 1 >= k - index; i++) {
            data[index] = input[i];
            permutationDup(input, data, i + 1, end, index + 1, k, resultList);

            while (i < end && input[i] == input[i + 1])
                i++;
        }
    }

    /**
     * Return true if two double value equals within given accuracy.
     *
     * @param aValue a double value
     * @param bValue a double value
     * @param aTol   comparison accuracy
     * @return true if the two double value equals within the tol
     */
    public static boolean equal(double aValue, double bValue, double aTol)
    {
        return Math.abs(aValue - bValue) < aTol;
    }

    /**
     * Return sum of values in array.
     * <p>
     * If there are no values in the dataset, then 0 is returned. If any of the values are {@link Double#NaN},
     * then {@link Double#NaN} is returned.
     *
     * @param values an Double array
     * @return sum.
     */
    public static double sum(Double[] values)
    {
        double total = 0;
        for (Double value : values) {
            total += value;
        }
        return total;
    }

    /**
     * Return the sum of the array.
     *
     * @param values double array
     * @return sum of all values.
     */
    public static double sum(double[] values)
    {
        requireNonNull(values);
        double sum = 0;
        for (double value : values) {
            sum += value;
        }
        return sum;
    }

    /**
     * Return the sum of an collection
     *
     * @param values dataset
     * @return sum of datas in the dataset
     */
    public static double sum(Iterable<Double> values)
    {
        requireNonNull(values);

        double sum = 0;
        for (Double value : values) {
            sum += value;
        }
        return sum;
    }

    /**
     * The weighted sum of the entries in the specified input array.
     * Uses the formula,
     * <pre>
     *  weighted sum = &Sigma;(values[i] * weights[i])
     * </pre>
     *
     * @param values  the input array
     * @param weights the weight array
     * @return weighted sum
     */
    public static double sum(final double[] values, final double[] weights)
    {
        checkNotNull(values);
        checkNotNull(weights);
        checkEqualLength(values, weights);
        ArrayUtils.checkArray(weights, 0, weights.length);

        double sum = 0.;
        for (int i = 0; i < values.length; i++) {
            sum += values[i] * weights[i];
        }
        return sum;
    }

    /**
     * Return the mean of the values.
     *
     * @param values an Double array.
     * @return mean of the array values.
     */
    public static double mean(Double[] values)
    {
        return sum(values) / values.length;
    }

    /**
     * Return the mean value of a given dataset
     *
     * @param values dataset
     * @return mean value
     */
    public static double mean(double[] values)
    {
        return sum(values) / values.length;
    }

    public static double mean(Collection<Double> values)
    {
        checkArgument(values != null && !values.isEmpty());
        double sum = sum(values);
        return sum / values.size();
    }

    /**
     * Return the relative standard deviation (RSD) of given double array.
     *
     * @param values a {@link Double} array.
     * @return relative standard deviation of the double array.
     */
    public static double rsd(double[] values)
    {
        double mean = mean(values);
        double variance = variance(values, mean, 0, values.length, true);
        return Math.sqrt(variance) / mean;
    }

    public static double rsd(Collection<Double> values)
    {
        double mean = mean(values);
        double variance = variance(values, mean, true);
        return Math.sqrt(variance) / mean;
    }

    /**
     * Return the standard deviation of given sample data.
     *
     * @param values values
     * @return standard deviation.
     */
    public static double standardDeviation(final double[] values)
    {
        return Math.sqrt(variance(values));
    }

    /**
     * return the standard deviation of a sample dataset.
     *
     * @param values data values
     * @param mean   mean value
     * @return standard deviation
     */
    public static double standardDeviation(final double[] values, double mean)
    {
        double variance = variance(values, mean);
        return Math.sqrt(variance);
    }

    public static double standardDeviation(Collection<Double> values)
    {
        return Math.sqrt(variance(values));
    }

    /**
     * Calculate the variance of the double array.
     *
     * @param values double array
     * @return variance of the data.
     */
    public static double variance(final double[] values)
    {
        double mean = mean(values);
        return variance(values, mean, 0, values.length, true);
    }

    /**
     * Calculate the variance of the double array.
     *
     * @param values double array
     * @param bias   Whether or not bias correction is applied when computing the value of the statistics.
     * @return variance of the data.
     */
    public static double variance(final double[] values, boolean bias)
    {
        double mean = mean(values);
        return variance(values, mean, 0, values.length, bias);
    }

    /**
     * Calculate the variance of the double array.
     *
     * @param values double array
     * @param mean   mean
     * @return variance of the data.
     */
    public static double variance(final double[] values, double mean)
    {
        return variance(values, mean, 0, values.length, true);
    }

    /**
     * Return variance of a sample dataset
     *
     * @param values data set
     * @return variance of the data.
     */
    public static double variance(final Collection<Double> values)
    {
        return variance(values, true);
    }

    /**
     * Return variance of the data
     *
     * @param values data set
     * @return variance of the data.
     */
    public static double variance(final Collection<Double> values, boolean isBias)
    {
        double mean = mean(values);
        return variance(values, mean, isBias);
    }

    /**
     * Return variance of the dataset.
     *
     * @param values data set
     * @param mean   data set mean
     * @param isBias true calculate sample variance
     * @return variance of the data.
     */
    public static double variance(final Collection<Double> values, final double mean, boolean isBias)
    {
        if (values.isEmpty())
            return Double.NaN;
        int len = values.size();
        if (len == 1) {
            return 0.;
        } else {
            double sum = 0.;
            double dev;
            for (Double value : values) {
                dev = value - mean;
                sum += dev * dev;
            }
            if (isBias) {
                return sum / (len - 1.);
            } else
                return sum / len;
        }
    }

    /**
     * Calculate the variance.
     *
     * @param values double array
     * @param mean   mean value
     * @param begin  start index
     * @param length data number
     * @param isBias Whether or not bias correction is applied when computing the value of the statistics.
     * @return variance of the data, return {@link Double#NaN} if the data is empty.
     */
    public static double variance(final double[] values, final double mean, final int begin, final int length, final boolean isBias)
    {
        double var = Double.NaN;
        if (length == 1) {
            var = 0.;
        } else if (length > 1) {
            double sum = 0.;
            double dev;
            for (int i = begin; i < begin + length; i++) {
                dev = values[i] - mean;
                sum += dev * dev;
            }
            if (isBias) {
                var = sum / (length - 1.0);
            } else
                var = sum / (length);
        }
        return var;
    }


    /**
     * Rounds a double value to the wanted number of decimal places.
     *
     * @param value         the value to scale
     * @param decimalPlaces the number of decimal places
     * @return a scaled double to the indicated decimal places
     */
    public static double round(double value, int decimalPlaces)
    {
        BigDecimal bd = new BigDecimal(value).setScale(decimalPlaces, RoundingMode.HALF_EVEN);
        return bd.doubleValue();
    }

}
