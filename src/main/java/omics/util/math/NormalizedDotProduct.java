package omics.util.math;

import it.unimi.dsi.fastutil.doubles.DoubleList;


/**
 * Static methods to calculate the normalized dot product.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 6:24 PM
 */
public class NormalizedDotProduct
{
    private NormalizedDotProduct() { }

    public static double cosim(DoubleList vX, DoubleList vY)
    {
        if (vX.isEmpty()) return Double.NaN;

        double aDotB = 0;
        double distA = 0;
        double distB = 0;

        for (int i = 0; i < vX.size(); i++) {

            double x = vX.getDouble(i);
            double y = vY.getDouble(i);

            aDotB += x * y;

            distA += Math.pow(x, 2);
            distB += Math.pow(y, 2);
        }

        distA = Math.sqrt(distA);
        distB = Math.sqrt(distB);

        return aDotB / (distA * distB);
    }

    public static double cosim(double[] vX, double[] vY)
    {
        if (vX.length < 1) return Double.NaN;

        double aDotB = 0;
        double distA = 0;
        double distB = 0;

        for (int i = 0; i < vX.length; i++) {

            double x = vX[i];
            double y = vY[i];

            aDotB += x * y;

            distA += Math.pow(x, 2);
            distB += Math.pow(y, 2);
        }

        distA = Math.sqrt(distA);
        distB = Math.sqrt(distB);

        return aDotB / (distA * distB);
    }
}
