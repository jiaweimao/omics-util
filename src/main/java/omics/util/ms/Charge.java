package omics.util.ms;

import omics.util.OmicsRuntimeException;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * charge of spectrum.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Jan 2017, 2:36 PM
 */
public class Charge {

    static final Pattern PAT_CHARGE = Pattern.compile("(\\d+)([+-]?)");

    private Polarity polarity;
    private int value;

    /**
     * Constructor.
     *
     * @param value    charge value.
     * @param polarity {@link Polarity}
     */
    public Charge(int value, Polarity polarity) {
        this.polarity = polarity;
        this.value = value;
    }

    public static Charge parse(String value) {
        Matcher matcher = PAT_CHARGE.matcher(value);
        if (!matcher.matches()) {
            throw new OmicsRuntimeException("Wrong charge format: " + value);
        }
        return new Charge(Integer.parseInt(matcher.group(1)), Polarity.from(matcher.group(2)));
    }

    /**
     * @return polarity of the charge.
     */
    public Polarity getPolarity() {
        return polarity;
    }

    public int getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Charge)) return false;
        Charge charge = (Charge) o;
        return value == charge.value &&
                polarity == charge.polarity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(polarity, value);
    }

    @Override
    public String toString() {
        switch (polarity) {
            case NEUTRAL:
                return "0";
            case POSITIVE:
                return value + "+";
            case NEGATIVE:
                return value + "-";
            default:
                return String.valueOf(value);
        }
    }
}
