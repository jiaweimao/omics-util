package omics.util.ms;

import omics.util.interfaces.Identifiable;
import omics.util.ms.peaklist.*;
import omics.util.ms.peaklist.impl.MzSymbolAnnotation;

import java.util.Optional;

import static omics.util.utils.ObjectUtils.checkArgument;


/**
 * Mass spectrum Chromatogram object.
 *
 * @author JiaweiMao
 * @version 1.10.0
 * @since 05 Apr 2017, 9:18 PM
 */
public class Chromatogram extends Spectrum<PeakAnnotation> implements Identifiable
{
    private ChromatogramType mChromatogramType;
    private SeparationType mSeparationType;
    private Double mz;
    private Double area;
    private String id;
    private Integer index;

    public Chromatogram()
    {
        this(null);
    }

    public Chromatogram(ChromatogramType chromatogramType)
    {
        this(chromatogramType, null);
    }


    public Chromatogram(ChromatogramType chromatogramType, SeparationType separationType)
    {
        this(chromatogramType, separationType, null);
    }

    public Chromatogram(ChromatogramType chromatogramType, SeparationType separationType, Double mz)
    {
        this(chromatogramType, separationType, 0, mz);
    }

    public Chromatogram(ChromatogramType chromatogramType, SeparationType separationType, int size, Double mz)
    {
        super(size, Precision.DOUBLE);

        this.mChromatogramType = chromatogramType;
        this.mSeparationType = separationType;
        this.mz = mz;
    }

    protected Chromatogram(Chromatogram src, PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor)
    {
        super(src, peakProcessor);
        this.mChromatogramType = src.mChromatogramType;
        this.mSeparationType = src.mSeparationType;
        this.mz = src.mz;
        this.area = src.area;
    }

    protected Chromatogram(Chromatogram src, PeakProcessorChain<PeakAnnotation> peakProcessorChain)
    {
        super(src, peakProcessorChain);
        this.mChromatogramType = src.mChromatogramType;
        this.mSeparationType = src.mSeparationType;
        this.mz = src.mz;
        this.area = src.area;
    }

    protected Chromatogram(Chromatogram src, SpectrumProcessor<PeakAnnotation, PeakAnnotation> spectrumProcessor)
    {
        super(src, spectrumProcessor);
        this.mChromatogramType = src.mChromatogramType;
        this.mSeparationType = src.mSeparationType;
        this.mz = src.mz;
        this.area = src.area;
    }

    @Override
    public Chromatogram copy(SpectrumProcessor<PeakAnnotation, PeakAnnotation> spectrumProcessor)
    {
        return new Chromatogram(this, spectrumProcessor);
    }

    @Override
    public PeakList<PeakAnnotation> copy(PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor)
    {
        return new Chromatogram(this, peakProcessor);
    }

    @Override
    public PeakList<PeakAnnotation> copy(PeakProcessorChain<PeakAnnotation> peakProcessorChain)
    {
        return new Chromatogram(this, peakProcessorChain);
    }

    public Integer getIndex()
    {
        return index;
    }

    public void setIndex(Integer index)
    {
        this.index = index;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * add data
     *
     * @param rtValues        retention time values
     * @param intensityValues intensity values
     * @param mzValues        mz values
     */
    public void addSorted(double[] rtValues, double[] intensityValues, double[] mzValues)
    {
        for (int i = 0; i < rtValues.length; i++) {
            add(rtValues[i], intensityValues[i], new MzSymbolAnnotation(mzValues[i]));
        }
    }

    /**
     * @return retention times.
     */
    public double[] getRetentionTimes()
    {
        return getXs();
    }

    /**
     * Returns the retention time of this Chromatogram (the time where the most intense peak locates)
     *
     * @return rt value.
     */
    public double getRT()
    {
        checkArgument(size() > 0, "This Chromatogram size should larger than 0!");

        return getBasePeakX();
    }

    /**
     * Returns the area of this Chromatogram.
     *
     * @return Chromatogram area.
     */
    public double getArea()
    {
        if (area == null)
            calcArea();
        return area;
    }

    private void calcArea()
    {
        area = 0.0;
        if (size() == 0)
            return;

        double rtDelta, inStart, inEnd;
        for (int i = 0; i < size() - 1; i++) {

            rtDelta = getX(i + 1) - getX(i);
            inStart = getY(i);
            inEnd = getY(i + 1);
            area += (rtDelta * (inStart + inEnd) / 2);
        }
    }

    /**
     * @return intensity values.
     */
    public double[] getIntensityValues()
    {
        return getYs();
    }


    /**
     * Returns the type of the chromatogram.
     *
     * @return Chromatogram type
     */
    public Optional<ChromatogramType> getChromatogramType()
    {
        return Optional.ofNullable(mChromatogramType);
    }

    /**
     * Returns the separation type used for separation of molecules.
     *
     * @return the separation type.
     */
    public Optional<SeparationType> getSeparationType()
    {
        return Optional.ofNullable(mSeparationType);
    }

    public void setChromatogramType(ChromatogramType iChromatogramType)
    {
        this.mChromatogramType = iChromatogramType;
    }

    public void setSeparationType(SeparationType iSeparationType)
    {
        this.mSeparationType = iSeparationType;
    }

    public void setArea(Double area)
    {
        this.area = area;
    }

    /**
     * Returns the m/z value of this chromatogram, null for absent.
     */
    public Double getMz()
    {
        return mz;
    }

    /**
     * Sets the m/z value of the chromatogram
     *
     * @param newMz a {@link java.lang.Double} object.
     */
    public void setMz(Double newMz)
    {
        this.mz = newMz;
    }
}
