package omics.util.ms;

/**
 * Represents the type of chromatogram.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 20 Apr 2017, 12:55 PM
 */
public enum ChromatogramType
{
    /**
     * Total ion current (TIC) chromatogram. This is the summed intensity of all
     * ions in each spectrum.
     */
    TIC("MS:1000235", "total ion current chromatogram"),

    /**
     * Base peak chromatogram (BPC). This is the intensity of the most intense
     * ion in each spectrum.
     */
    BPC("MS:1000628", "basepeak chromatogram"),
    /**
     * Single ion current (SIC) chromatogram. Only a narrow range of m/z values
     * are detected in the analysis and a chromatogram similar to the XIC is
     * generated.
     */
    SIC("MS:1000627", "selected ion current chromatogram"),

    /**
     * Chromatogram created by creating an array of the measurements of a selectively monitored ion at each time point.
     */
    SIM("MS:1001472", "selected ion monitoring chromatogram"),

    /**
     * Multiple Reaction Monitoring (MRM) or Selected Reaction Monitoring (SRM).
     * A specific product ion of a specific parent ion is detected.
     */
    MRM_SRM("MS:1001473", "selected reaction monitoring chromatogram");

    ChromatogramType(String accession, String name)
    {
        this.accession = accession;
        this.name = name;
    }

    private String accession;
    private String name;

    public String getAccession()
    {
        return accession;
    }

    public String getName()
    {
        return name;
    }
}
