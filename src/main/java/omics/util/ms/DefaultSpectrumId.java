package omics.util.ms;

import java.util.Optional;

/**
 * default {@link SpectrumId} implementation.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 14 Aug 2019, 4:09 PM
 */
class DefaultSpectrumId implements SpectrumId
{
    protected final String id;
    protected final MsDataId msDataID;
    protected final int index;

    public DefaultSpectrumId(String id, MsDataId msDataID, int index)
    {
        this.id = id;
        this.msDataID = msDataID;
        this.index = index;
    }

    /**
     * @return an unique identifier
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return index of this spectrum, -1 if not set.
     */
    public int getIndex()
    {
        return index;
    }


    @Override
    public MsDataId getMsDataId()
    {
        return msDataID;
    }

    @Override
    public Optional<Double> getCollisionEnergy()
    {
        return Optional.empty();
    }

    @Override
    public Optional<ScanNumber> getParentScanNumber()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Double> getIsolateWindowTargetMz()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Double> getIsolateWindowLowerOffset()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Double> getIsolateWindowUpperOffset()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Double> getIonInjectTime()
    {
        return Optional.empty();
    }

    @Override
    public Optional<ScanType> getScanType()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Polarity> getPolarity()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Double> getScanLowerMz()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Double> getScanUpperMz()
    {
        return Optional.empty();
    }

    @Override
    public SpectrumId copy()
    {
        return new DefaultSpectrumId(id, msDataID, index);
    }
}
