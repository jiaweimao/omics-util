package omics.util.ms;

import java.util.Optional;

/**
 * Fragmentation method used for dissociation or fragmentation.
 * <p>
 * See also K. K. Murray, R. K. Boyd, M. N. Eberlin, G. J. Langley, L. Li and Y.
 * Naito, Definitions of Terms Relating to Mass Spectrometry (IUPAC
 * Recommendations 2013) Pure Appl. Chem. 2013, 85, 1515-1609.
 *
 * @author JiaweiMao
 * @version 1.3.0
 */
public enum Dissociation
{
    /**
     * A special case of infrared multiphoton dissociation wherein excitation of the reactant ion is caused by
     * absorption of infrared photons radiating from heated blackbody surroundings, which are usually the walls
     * of a vacuum chamber. See also infrared multiphoton dissociation.
     */
    BIRD("MS:1000242", "BIRD", "blackbody infrared radiative dissociation"),
    /**
     * Collision-induced dissociation.
     * The dissociation of an ion after collisional excitation. The term collisional-activated dissociation is not recommended.
     */
    CID("MS:1000133", "CID", "collision-induced dissociation"),

    /**
     * Higher-energy C-trap dissociation.
     */
    HCD("MS:1000422", "HCD", "beam-type collision-induced dissociation"),

    /**
     * A process in which a multiply protonated molecules interacts with a low energy electrons.
     * Capture of the electron leads the liberation of energy and a reduction in charge state of
     * the ion with the production of the (M + nH) (n-1)+ odd electron ion, which readily fragments.
     */
    ECD("MS:1000250", "ECD", "electron capture dissociation"),

    /**
     * Electron-transfer dissociation.
     * <p>
     * A process to fragment ions in a mass spectrometer by inducing fragmentation of cations (e.g. peptides or proteins)
     * by transferring electrons to them."
     */
    ETD("MS:1000598", "ETD", "electron transfer dissociation"),

    /**
     * Electron-transfer and higher-energy collision dissociation.
     */
    ETHCD("MS:1002631", "ETHCD", "Electron-Transfer/Higher-Energy Collision Dissociation (EThcD)"),

    /**
     * The ionization of material in a solid sample by bombarding it with ionic or neutral atoms formed as a result
     * of the fission of a suitable nuclide, typically 252Cf. Synonymous with fission fragment ionization.
     */
    PD("MS:1000134", "PD", "plasma desorption"),

    /**
     * A technique specific to reflectron time-of-flight mass spectrometers where product ions of metastable
     * transitions or collision-induced dissociations generated in the drift tube prior to entering the reflectron
     * are m/z separated to yield product ion spectra.
     */
    PSD("MS:1000135", "PSD", "post-source decay"),

    /**
     * A process that involves precursor ion activation at high Q, a time delay to allow the precursor to fragment,
     * then a rapid pulse to low Q where all fragment ions are trapped. The product ions can then be scanned out
     * of the ion trap and detected.
     */
    PQD("MS:1000599", "PQD", "pulsed q dissociation"),

    /**
     * Fragmentation that results from the collision of an ion with a surface.
     */
    SID("MS:1000136", "SID", "surface-induced dissociation"),

    /**
     * A process wherein the reactant ion is dissociated as a result of absorption of one or more photons.
     */
    MPD("MS:1000435", "MPD", "photodissociation"),

    /**
     * Multiphoton ionization where the reactant ion dissociates as a result of the absorption of multiple infrared photons.
     */
    IRMPD("MS:1000262", "IRMPD", "infrared multiphoton dissociation"),

    /**
     * A technique associated with Fourier transform ion cyclotron resonance (FT-ICR) mass spectrometry to carry out
     * ion/neutral reactions such as low-energy collision-induced dissociation. A radio-frequency electric field
     * of slightly off-resonance to the cyclotron frequency of the reactant ion cyclically accelerates and decelerates
     * the reactant ion that is confined in the Penning ion trap. The ion's orbit does not exceed the dimensions of
     * ion trap while the ion undergoes an ion/neutral species process that produces a high average translational energy
     * for an extended time.
     */
    SORI("MS:1000282", "SORI", "sustained off-resonance irradiation"),

    ANY("MS:1000044", "dissociation method", "Fragmentation method used for dissociation or fragmentation");

    /**
     * Return the {@link Dissociation} of given short name
     *
     * @param shortName {@link Dissociation} name
     * @return {@link Dissociation} of the name, null for absent
     */
    public static Dissociation fromName(String shortName)
    {
        return Dissociation.valueOf(shortName);
    }

    /**
     * Return the {@link Dissociation} of given PSI-MS accession
     *
     * @param acc {@link Dissociation} PSI-MS accession
     * @return {@link Dissociation} of the accession
     */
    public static Optional<Dissociation> fromAccession(String acc)
    {
        String s = acc.toUpperCase();
        for (Dissociation dissociation : values()) {
            if (dissociation.accession.equals(s))
                return Optional.of(dissociation);
        }

        return Optional.empty();
    }

    private String accession;
    private String name;
    private String fullName;

    Dissociation(String accession, String name, String fullName)
    {
        this.accession = accession;
        this.name = name;
        this.fullName = fullName;
    }

    /**
     * @return PSI-MS accession of this dissociation method.
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * @return name of this dissociation method.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return full name of this dissociation method.
     */
    public String getFullName()
    {
        return fullName;
    }
}
