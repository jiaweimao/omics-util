package omics.util.ms;

import omics.util.ms.peaklist.*;

/**
 * This class store information and peaks for an Isotope Pattern.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Nov 2018, 8:12 PM
 */
public class IsotopePattern extends Spectrum<PeakAnnotation> {

    private int charge;

    public IsotopePattern(IsotopePattern src, PeakProcessorChain<PeakAnnotation> peakProcessorChain) {
        super(src, peakProcessorChain);
        this.charge = src.charge;
    }

    public IsotopePattern(IsotopePattern src, PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor) {
        super(src, peakProcessor);
        this.charge = src.charge;

    }

    public IsotopePattern(IsotopePattern src, SpectrumProcessor<PeakAnnotation, PeakAnnotation> spectrumProcessor) {
        super(src, spectrumProcessor);
        this.charge = src.charge;
    }

    @Override
    public PeakList<PeakAnnotation> copy(PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor) {
        return new IsotopePattern(this, peakProcessor);
    }

    @Override
    public PeakList<PeakAnnotation> copy(SpectrumProcessor<PeakAnnotation, PeakAnnotation> spectrumProcessor) {
        return new IsotopePattern(this, spectrumProcessor);
    }

    @Override
    public PeakList<PeakAnnotation> copy(PeakProcessorChain<PeakAnnotation> peakProcessorChain) {
        return new IsotopePattern(this, peakProcessorChain);
    }

    /**
     * @return peak charge of this IsotopePattern
     */
    public int getCharge() {
        return charge;
    }

    /**
     * setter of the charge of peaks in this {@link IsotopePattern}
     */
    public void setCharge(int charge) {
        this.charge = charge;
    }
}
