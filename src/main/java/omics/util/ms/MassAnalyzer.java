package omics.util.ms;

/**
 * Mass analyzer separates the ions according to their mass-to-charge ratio.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 02 Dec 2019, 2:19 PM
 */
public enum MassAnalyzer
{
    /**
     * A mass spectrometer based on the principle of ion cyclotron resonance in which an ion in a magnetic field moves
     * in a circular orbit at a frequency characteristic of its m/z value. Ions are coherently excited to a larger
     * radius orbit using a pulse of radio frequency energy and their image charge is detected on receiver plates as a
     * time domain signal. Fourier transformation of the time domain signal results in a frequency domain signal which
     * is converted to a mass spectrum based in the inverse relationship between frequency and m/z.
     */
    FT_ICR,
    /**
     * A mass spectrometer that consists of four parallel rods whose centers form the corners of a square and whose
     * opposing poles are connected. The voltage applied to the rods is a superposition of a static potential and a
     * sinusoidal radio frequency potential. The motion of an ion in the x and y dimensions is described by the Matthieu
     * equation whose solutions show that ions in a particular m/z range can be transmitted along the z axis.
     */
    Quadrupole,
    /**
     * Instrument that separates ions by m/z in a field-free region after acceleration to a fixed acceleration energy
     */
    TOF,
    /**
     * ion trap,
     * A device for spatially confining ions using electric and magnetic fields alone or in combination.
     */
    IT,
    /**
     * An ion trapping device that consists of an outer barrel-like electrode and a coaxial inner spindle-like electrode
     * that form an electrostatic field with quadro-logarithmic potential distribution. The frequency of harmonic
     * oscillations of the orbitally trapped ions along the axis of the electrostatic field is independent of the ion
     * velocity and is inversely proportional to the square root of m/z so that the trap can be used as a mass analyzer.
     */
    Orbitrap;

    public static MassAnalyzer fromPSIAccession(String accession)
    {
        String s = accession.toUpperCase();
        switch (s) {
            case "MS:1000079":
                return FT_ICR;
            case "MS:1000081":
                return Quadrupole;
            case "MS:1000084":
                return TOF;
            case "MS:1000264":
                return IT;
            case "MS:1000484":
                return Orbitrap;
            default:
                throw new IllegalArgumentException("Unknown acceesion: " + accession);
        }
    }
}
