package omics.util.ms;

import java.util.Optional;

/**
 * Spectrum ID to match all fields of MzML.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 15 Aug 2019, 8:57 AM
 */
class MoreSpectrumId extends DefaultSpectrumId
{
    private final Double collisionEnergy;
    private final ScanNumber precursorScan;

    private final Double isolateWindowTargetMz;
    private final Double isolateWindowLowerOffset;
    private final Double isolateWindowUpperOffset;

    private final Double ionInjectTime;
    private final ScanType scanType;
    private final Polarity polarity;
    private final Double scanningLowerMz;
    private final Double scanningUpperMz;

    public MoreSpectrumId(String id, MsDataId msDataID, int index, Double collisionEnergy,
                          ScanNumber precursorScan, Double isolateWindowTargetMz, Double isolateWindowLowerOffset,
                          Double isolateWindowUpperOffset, Double ionInjectTime, ScanType scanType, Polarity polarity,
                          Double scanningLowerMz, Double scanningUpperMz)
    {
        super(id, msDataID, index);
        this.collisionEnergy = collisionEnergy;
        this.precursorScan = precursorScan;
        this.isolateWindowTargetMz = isolateWindowTargetMz;
        this.isolateWindowLowerOffset = isolateWindowLowerOffset;
        this.isolateWindowUpperOffset = isolateWindowUpperOffset;
        this.ionInjectTime = ionInjectTime;
        this.scanType = scanType;
        this.polarity = polarity;
        this.scanningLowerMz = scanningLowerMz;
        this.scanningUpperMz = scanningUpperMz;
    }

    @Override
    public SpectrumId copy()
    {
        return new MoreSpectrumId(id, msDataID, index, collisionEnergy, precursorScan, isolateWindowTargetMz,
                isolateWindowLowerOffset, isolateWindowUpperOffset, ionInjectTime, scanType,
                polarity, scanningLowerMz, scanningUpperMz);
    }

    /**
     * @return {@link ScanType} of the spectrum
     */
    public Optional<ScanType> getScanType()
    {
        return Optional.ofNullable(scanType);
    }

    /**
     * The primary or reference m/z about which the isolation window is defined.
     *
     * @return isolation window target m/z, null if not set.
     */
    public Optional<Double> getIsolateWindowTargetMz()
    {
        return Optional.ofNullable(isolateWindowTargetMz);
    }

    /**
     * the extent of the isolation window in m/z below the isolation window target m/z. The lower
     * and upper may be asymmetric about the target m/z.
     *
     * @return isolation window lower offset
     */
    public Optional<Double> getIsolateWindowLowerOffset()
    {
        return Optional.ofNullable(isolateWindowLowerOffset);
    }

    /**
     * The extent of the isolation window in m/z above the isolation window target m/z.
     * The lower and upper offsets may be asymmetric about the target m/z.
     *
     * @return isolation window upper offset
     */
    @Override
    public Optional<Double> getIsolateWindowUpperOffset()
    {
        return Optional.ofNullable(isolateWindowUpperOffset);
    }

    /**
     * Returns the ion injection time in ms, for ion trap experiments. Shorter
     * time indicates larger amount of ions (trap fills faster).
     * TimeUnit: millisecond.
     *
     * @return ion injection time, null for absent.
     */
    public Optional<Double> getIonInjectTime()
    {
        return Optional.ofNullable(ionInjectTime);
    }

    /**
     * @return scanning lower mz of the spectrum
     */
    public Optional<Double> getScanLowerMz()
    {
        return Optional.ofNullable(scanningLowerMz);
    }

    /**
     * @return scanning upper mz of the spectrum
     */
    public Optional<Double> getScanUpperMz()
    {
        return Optional.ofNullable(scanningUpperMz);
    }

    @Override
    public Optional<Double> getCollisionEnergy()
    {
        return Optional.ofNullable(collisionEnergy);
    }

    @Override
    public Optional<Polarity> getPolarity()
    {
        return Optional.ofNullable(polarity);
    }

    /**
     * Returns the scan number of the parent spectrum.
     *
     * @return the parent scan number
     */
    public Optional<ScanNumber> getParentScanNumber()
    {
        return Optional.ofNullable(precursorScan);
    }
}
