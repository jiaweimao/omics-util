package omics.util.ms;

import omics.util.interfaces.Copyable;
import omics.util.interfaces.Identifiable;

import javax.annotation.Nonnull;
import java.nio.file.Path;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Identifier for a MS data.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 14 Aug 2019, 3:39 PM
 */
public class MsDataId implements Identifiable, Copyable<MsDataId>
{
    /**
     * Create a {@link MsDataId} from path and filetype
     *
     * @param path     {@link Path} of the data file
     * @param fileType {@link MsFileType} of the data file.
     * @return {@link MsDataId} of the data file.
     */
    public static MsDataId of(Path path, MsFileType fileType)
    {
        String name = path.getFileName().toString();
        return new MsDataId(name, fileType, name, path.toString());
    }

    /**
     * Create a {@link MsDataId}
     *
     * @param id         identifier
     * @param msFileType {@link MsFileType}
     * @param name       file name
     * @param path       file path
     * @return {@link MsDataId} instance.
     */
    public static MsDataId of(String id, MsFileType msFileType, String name, String path)
    {
        return new MsDataId(id, msFileType, name, path);
    }

    public static MsDataId of(String id)
    {
        return of(id, MsFileType.UNKNOWN, null, null);
    }

    /**
     * Return a {@link MsDataId} instance.
     *
     * @param id       identifier
     * @param fileType {@link MsFileType}
     * @return {@link MsDataId}
     */
    public static MsDataId of(String id, MsFileType fileType)
    {
        return new MsDataId(id, fileType, null, null);
    }

    private String id;
    private MsFileType msFileType;
    private String name;
    private String path;

    /**
     * Constructor.
     *
     * @param id         unique identifier for the MS data.
     * @param msFileType {@link MsFileType} of the MS data.
     * @param name       name
     * @param path       location of the data file.
     */
    protected MsDataId(@Nonnull String id, @Nonnull MsFileType msFileType, String name, String path)
    {
        requireNonNull(id);
        requireNonNull(msFileType);

        this.id = id;
        this.msFileType = msFileType;
        this.name = name;
        this.path = path;
    }

    @Override
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the {@link MsFileType} of the MS data.
     */
    public MsFileType getMSFileType()
    {
        return msFileType;
    }

    /**
     * @return the name of the MS data.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return path of the data file, or null if it is not set.
     */
    public String getPath()
    {
        return path;
    }

    @Override
    public String toString()
    {
        return "MSDataID{" + "id='" + id + '\'' +
                ", msFileType=" + msFileType +
                ", name='" + name + '\'' +
                ", path=" + path +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MsDataId msDataID = (MsDataId) o;
        return id.equals(msDataID.id) &&
                msFileType == msDataID.msFileType;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, msFileType);
    }

    @Override
    public MsDataId copy()
    {
        return new MsDataId(this.id, this.msFileType, this.name, this.path);
    }
}
