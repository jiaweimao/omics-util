package omics.util.ms;

/**
 * Enum of supported raw data file formats
 *
 * @author JiaweiMao
 * @version 1.3.0
 * @since 22 Jan 2018, 9:16 PM
 */
public enum MsFileType
{
    /**
     * Peak list file format of the Andromeda search engine. [ PSI:PI ]
     */
    APL("MS:1002996", "Andromeda:apl file format", ".apl"),
    DTA("MS:1000613", "DTA format", ".dta"),
    /**
     * mgf format.
     * See http://www.matrixscience.com/help/data_file_help.html
     */
    MGF("MS:1001062", "Mascot MGF format", ".mgf"),
    MS2("MS:1001466", "MS2 format", ".ms2"),
    /**
     * mzML format. See Martens L, Chambers M, Sturm M, Kessner D, Levander F,
     * Shofstahl J, Tang WH, Römpp A, Neumann S, Pizarro AD, Montecchi-Palazzi
     * L, Tasman N, Coleman M, Reisinger F, Souda P, Hermjakob H, Binz PA,
     * Deutsch EW (2011) mzML-a community standard for mass spectrometry data,
     * Mol Cell Proteomics 10(1):R110.000133. doi:10.1074/mcp.R110.000133
     */
    MZML("MS:1000584", "mzML format", ".mzML"),
    /**
     * mzXML format, now deprecated in favor of mzML. See Pedrioli PG, Eng JK,
     * Hubley R, Vogelzang M, Deutsch EW, Raught B, Pratt B, Nilsson E,
     * Angeletti RH, Apweiler R, Cheung K, Costello CE, Hermjakob H, Huang S,
     * Julian RK, Kapp E, McComb ME, Oliver SG, Omenn G, Paton NW, Simpson R,
     * Smith R, Taylor CF, Zhu W, Aebersold R (2004) A common open
     * representation of mass spectrometry data and its application to
     * proteomics research, Nat. Biotechnol. 22(11):1459–66. doi:10.1038/nbt1031
     */
    MZXML("MS:1000566", "ISB mzXML format", ".mzXML"),
    /**
     * mzData format, now deprecated in favor of mzML. See Orchard S,
     * Montechi-Palazzi L, Deutsch EW, Binz PA, Jones AR, Paton N, Pizarro A,
     * Creasy DM, Wojcik J, Hermjakob H (2007) Five years of progress in the
     * Standardization of Proteomics Data 4(th) Annual Spring Workshop of the
     * HUPO-Proteomics Standards Initiative April 23–25, 2007 Ecole Nationale
     * Supérieure (ENS), Lyon, France, Proteomics 7(19):3436–40.
     * doi:10.1002/pmic.200700658
     */
    MZDATA("MS:1000564", "PSI mzData format", ".mzData"),
    /**
     * Micromass PKL file format. [ PSI:MS ]
     */
    PKL("MS:1000565", "Micromass PKL format", ".pkl"),
    /**
     * Native RAW format of Thermo Fisher Scientific MS instruments.
     */
    THERMO_RAW("MS:1000563", "Thermo RAW format", ".raw"),
    /**
     * Native RAW format of Waters MS instruments.
     */
    WATERS_RAW("MS:1000526", "Waters raw format", ".raw"),
    /**
     * Applied Biosystems WIFF file format. [ PSI:MS ]
     */
    ABI_WIFF("MS:1000562", "ABI WIFF format", ".wiff"),
    /**
     * Unknown or unsupported format.
     */
    UNKNOWN("", "", "");

    private final String accession;
    private final String name;
    private final String[] extensions;

    MsFileType(String accession, String name, String extension)
    {
        this(accession, name, new String[]{extension});
    }

    MsFileType(String accession, String name, String[] extensions)
    {
        this.accession = accession;
        this.name = name;
        this.extensions = extensions;
    }

    /**
     * return the file type of given accession
     *
     * @param accession PSI-MS accession
     */
    public static MsFileType fromAccession(String accession)
    {
        for (MsFileType fileType : MsFileType.values()) {
            if (fileType.accession.equals(accession))
                return fileType;
        }
        return UNKNOWN;
    }

    /**
     * Return the {@link MsFileType} of given file name
     *
     * @param fileName file name
     * @return {@link MsFileType} of the name
     */
    public static MsFileType fromFileName(String fileName)
    {
        String name = fileName.toLowerCase();
        for (MsFileType fileType : MsFileType.values()) {
            for (String ext : fileType.getExtensions()) {
                if (name.endsWith(ext.toLowerCase()))
                    return fileType;
            }
        }
        return UNKNOWN;
    }

    /**
     * @return PSI-MS accession
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * @return name of this MSFileType
     */
    public String getName()
    {
        return name;
    }

    public String[] getExtensions()
    {
        return extensions;
    }
}
