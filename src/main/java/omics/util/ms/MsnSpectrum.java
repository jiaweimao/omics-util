package omics.util.ms;

import omics.util.ms.peaklist.*;
import omics.util.utils.NumberFormatFactory;
import omics.util.utils.StringUtils;

import java.io.IOException;
import java.io.Writer;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

/**
 * MS Spectrum implementation.
 *
 * @author JiaweiMao
 * @version 1.7.0
 * @since 14 Aug 2019, 4:07 PM
 */
public class MsnSpectrum extends Spectrum<PeakAnnotation>
{
    private String title = "";
    private ScanNumberList scanNumbers = new ScanNumberList();
    private RetentionTimeList retentionTimes = new RetentionTimeList();
    private Dissociation dissociation = null;
    private SpectrumId spectrumID = null;
    private SpectrumType spectrumType = SpectrumType.CENTROIDED;

    /**
     * Default constructor
     */
    public MsnSpectrum()
    {
        this(0, Precision.DOUBLE_FLOAT);
    }

    /**
     * Construct a MsnSpectrum that stores the peak list at the given precision
     *
     * @param precision the precision that the peak list is stored at
     */
    public MsnSpectrum(Precision precision)
    {
        this(0, precision);
    }

    /**
     * Construct a MsnSpectrum that stores the peak list at the given precision
     * and has an initial capacity equal to <code>initialCapacity</code>
     *
     * @param initialCapacity the initial capacity of the peak list
     * @param precision       the precision that the peak list is stored at
     */
    public MsnSpectrum(int initialCapacity, Precision precision)
    {
        super(initialCapacity, precision);
    }

    public MsnSpectrum(int initialCapacity, Precision precision, SpectrumId spectrumID)
    {
        super(initialCapacity, precision);
        this.spectrumID = spectrumID;
    }

    /**
     * Construct a MsnSpectrum that has a constant intensity. The precision
     * needs to be either PeakList.Precision.DOUBLE_CONSTANT or
     * PeakList.Precision.FLOAT_CONSTANT. The peak lists initial capacity is set
     * to <code>initialCapacity</code>. The spectrumID is of tye {@link DefaultSpectrumId}
     *
     * @param initialCapacity   the initial capacity of the peak list
     * @param constantIntensity the constant intensity if
     * @param precision         the precision that the peak list is stored at
     */
    public MsnSpectrum(int initialCapacity, double constantIntensity, Precision precision)
    {
        super(initialCapacity, constantIntensity, precision);
    }

    /**
     * Copy constructor.
     *
     * @param src               source {@link MsnSpectrum}
     * @param spectrumProcessor {@link SpectrumProcessor}
     */
    protected MsnSpectrum(MsnSpectrum src, SpectrumProcessor<PeakAnnotation, PeakAnnotation> spectrumProcessor)
    {
        super(src, spectrumProcessor);
        this.spectrumID = src.spectrumID;
        this.spectrumType = src.spectrumType;
        this.title = src.title;
    }

    /**
     * Copy constructor
     *
     * @param src the MsnSpectrum to copy
     */
    protected MsnSpectrum(MsnSpectrum src, PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor)
    {
        super(src, peakProcessor);
        this.spectrumID = src.spectrumID;
        this.spectrumType = src.spectrumType;
        this.title = src.title;
    }

    /**
     * Copy constructor
     *
     * @param src the MsnSpectrum to copy
     */
    protected MsnSpectrum(MsnSpectrum src, PeakProcessorChain<PeakAnnotation> peakProcessorChain)
    {
        super(src, peakProcessorChain);
        this.spectrumID = src.spectrumID;
        this.spectrumType = src.spectrumType;
        this.title = src.title;
    }

    @Override
    public MsnSpectrum copy(PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor)
    {
        return new MsnSpectrum(this, peakProcessor);
    }

    @Override
    public MsnSpectrum copy(PeakProcessorChain<PeakAnnotation> peakProcessorChain)
    {
        return new MsnSpectrum(this, peakProcessorChain);
    }

    public MsnSpectrum copy(SpectrumProcessor<PeakAnnotation, PeakAnnotation> spectrumProcessor)
    {
        return new MsnSpectrum(this, spectrumProcessor);
    }

    /**
     * determine the {@link SpectrumType}
     */
    public void determineSpectrumType()
    {
        this.spectrumType = SpectrumType.CENTROIDED;
        if (size() > 0) {
            ArrayList<Double> diff = new ArrayList<>();
            double preMz = getMz(0);
            for (int i = 1; i < size(); i++) {
                if (getIntensity(i) == 0)
                    continue;
                double curMz = getMz(i);
                diff.add((curMz - preMz) / curMz * 1e6);
                preMz = curMz;
            }
            Collections.sort(diff);
            if (diff.size() > 0 && diff.get(diff.size() / 2) < 50)
                this.spectrumType = SpectrumType.PROFILE;
        }
    }

    public Optional<Dissociation> getDissociation()
    {
        return Optional.ofNullable(dissociation);
    }

    /**
     * Set the {@link Dissociation} of the spectrum.
     *
     * @param dissociation {@link Dissociation} instance.
     */
    public void setDissociation(Dissociation dissociation)
    {
        this.dissociation = dissociation;
    }

    /**
     * add a scan number
     *
     * @param scanNumber {@link ScanNumber} instance.
     */
    public void addScanNumber(ScanNumber scanNumber)
    {
        this.scanNumbers.add(scanNumber);
    }

    /**
     * add list of {@link ScanNumber}, duplicate one will be ignored.
     *
     * @param scanNumberList {@link ScanNumberList}
     */
    public void addScanNumber(ScanNumberList scanNumberList)
    {
        for (ScanNumber scanNumber : scanNumberList) {
            if (!scanNumbers.contains(scanNumber)) {
                scanNumbers.add(scanNumber);
            }
        }
    }

    /**
     * add retentiontime.
     *
     * @param retentionTime {@link RetentionTime} instance.
     */
    public void addRetentionTime(RetentionTime retentionTime)
    {
        this.retentionTimes.add(retentionTime);
    }

    public void addRetentionTime(RetentionTimeList retentionTimes)
    {
        this.retentionTimes.addAll(retentionTimes);
    }

    /**
     * @return the first retention time.
     */
    public RetentionTime getRetentionTime()
    {
        return this.retentionTimes.getFirst();
    }

    /**
     * @return true if the spectrum has retention time information.
     */
    public boolean hasRetentionTime()
    {
        return !retentionTimes.isEmpty();
    }

    /**
     * @return {@link ScanNumberList} of the spectrum
     */
    public ScanNumberList getScanNumberList()
    {
        return scanNumbers;
    }

    /**
     * For most of time, only one scan number is present for a spectrum, use this method is more brief.
     *
     * @return the first ScanNumber
     */
    public ScanNumber getScanNumber()
    {
        return scanNumbers.getFirst();
    }

    /**
     * @return {@link RetentionTimeList} of the spectrum
     */
    public RetentionTimeList getRetentionTimeList()
    {
        return retentionTimes;
    }

    /**
     * @return spectrum title.
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * set the spectrum title
     *
     * @param title spectrum title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Updates the SpectrumType of this mass spectrum.
     *
     * @param spectrumType a {@link SpectrumType}
     */
    public void setSpectrumType(SpectrumType spectrumType)
    {
        this.spectrumType = spectrumType;
    }

    /**
     * @return set the {@link SpectrumType} of this spectrum.
     */
    public SpectrumType getSpectrumType()
    {
        return spectrumType;
    }

    /**
     * @return {@link SpectrumId} of this spectrum, if not set, return a {@link DefaultSpectrumId}
     */
    public SpectrumId getId()
    {
        return spectrumID;
    }

    /**
     * @param spectrumID setter of the {@link SpectrumId}
     */
    public void setId(SpectrumId spectrumID)
    {
        this.spectrumID = spectrumID;
    }

    /**
     * Return the mz value at given index
     *
     * @param i index
     * @return mz value
     */
    public double getMz(int i)
    {
        return getX(i);
    }

    /**
     * return intensity value at given index
     *
     * @param index index
     * @return intensity value
     */
    public double getIntensity(int index)
    {
        return getY(index);
    }

    /**
     * write this spectrum to a file in mgf format with mz in 4 digits and intensity in 2 digits.
     *
     * @param out output mgf file path
     */
    public void write2Mgf(Writer out)
    {
        try {
            write2Mgf(out, NumberFormatFactory.MASS_PRECISION, NumberFormatFactory.DIGIT2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * write this spectrum to a file in mgf format.
     *
     * @param writer   output file path
     * @param mzFormat {@link NumberFormat} to format m/z values
     * @param inFormat {@link NumberFormat} to format intensity values.
     */
    public void write2Mgf(Writer writer, NumberFormat mzFormat, NumberFormat inFormat) throws IOException
    {
        writer.write("BEGIN IONS\n");

        if (StringUtils.isNotEmpty(title)) {
            writer.write("TITLE=" + title);
            writer.write("\n");
        }

        Peak precursor = getPrecursor();
        writer.write("PEPMASS=" + precursor.getMz() + " " + precursor.getIntensity() + "\n");

        // write charges
        int[] charges = precursor.getCharges();
        if (charges.length > 0) {
            writer.write("CHARGE=");

            boolean first = true;
            for (int charge : charges) {
                if (first) {
                    first = false;
                } else {
                    writer.write(", ");
                }
                writer.write(String.valueOf(charge));
            }
            writer.write('\n');
        }

        // write scans
        if (!scanNumbers.isEmpty()) {

            writer.write("SCANS=");
            boolean first = true;
            for (ScanNumber sn : scanNumbers) {
                if (first) {
                    first = false;
                } else {
                    writer.write(",");
                }

                writer.write(sn.toString());
            }
            writer.write('\n');
        }

        // RTs
        if (!retentionTimes.isEmpty()) {
            writer.write("RTINSECONDS=");
            boolean first = true;
            for (RetentionTime rt : retentionTimes) {
                if (first) {
                    first = false;
                } else {
                    writer.write(",");
                }

                writer.write(rt.toString());
            }
            writer.write('\n');
        }

        Optional<Object> seq = getMeta("SEQ");
        if (seq.isPresent()) {
            writer.write("SEQ=");
            writer.write(seq.get().toString());
            writer.write("\n");
        }

        for (int i = 0; i < size(); i++) {

            double mz = getX(i);
            double intensity = getY(i);

            writer.write(mzFormat.format(mz));
            writer.write(' ');
            writer.write(inFormat.format(intensity));
            writer.write("\n");
        }

        writer.write("END IONS\n");
    }
}
