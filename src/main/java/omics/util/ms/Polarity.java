package omics.util.ms;

/**
 * Polarity of charge.
 *
 * @author JiaweiMao 2017.03.16
 * @since 1.1.0
 */
public enum Polarity
{
    POSITIVE, NEGATIVE, NEUTRAL;

    /**
     * Get the charge with sign(+/-)
     *
     * @param polarity the Polarity
     * @param charge   the charge value
     * @return the charge with sign
     */
    public static int getCharge(Polarity polarity, int charge)
    {
        int unsignedCharge = Math.abs(charge);

        switch (polarity) {
            case POSITIVE:
                return unsignedCharge;
            case NEGATIVE:
                return -1 * unsignedCharge;
            default:
                throw new IllegalArgumentException("Polarity cannot be " + polarity);
        }
    }

    /**
     * Retrun the Polarity of given charge value.
     *
     * @param charge charge value.
     * @return {@link Polarity} of the charge.
     */
    public static Polarity from(int charge)
    {
        if (charge > 0)
            return POSITIVE;
        else if (charge < 0)
            return NEGATIVE;
        else
            return NEUTRAL;
    }

    public static Polarity from(String sign)
    {
        switch (sign) {
            case "+":
                return POSITIVE;
            case "-":
                return NEGATIVE;
            default:
                return NEUTRAL;
        }
    }
}
