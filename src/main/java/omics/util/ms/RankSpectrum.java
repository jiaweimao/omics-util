package omics.util.ms;

import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import omics.util.utils.ArrayIndexSorter;
import omics.util.utils.DoubleDescendingComparator;

/**
 * Spectrum with peak ranks.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 28 Mar 2019, 1:44 PM
 */
public class RankSpectrum extends MsnSpectrum
{
    public static RankSpectrum from(MsnSpectrum spectrum)
    {
        return new RankSpectrum(spectrum);
    }

    private int[] ranks;

    /**
     * Construct a spectrum with rank.
     *
     * @param spectrum the {@link MsnSpectrum}
     */
    private RankSpectrum(MsnSpectrum spectrum)
    {
        super(spectrum, new IdentityPeakProcessor<>());

        double[] ys = spectrum.getYs();
        ranks = new int[ys.length];
        Integer[] indexes = ArrayIndexSorter.sort(new DoubleDescendingComparator(ys));
        for (int i = 0; i < indexes.length; i++) {
            ranks[indexes[i]] = i + 1;
        }
    }

    /**
     * Return the rank of peak at given index. For the peaks with the same intensity, their original order is retained.
     *
     * @param index index of the peak
     * @return rank the the peak, 1-based order.
     */
    public int getRank(int index)
    {
        return ranks[index];
    }
}
