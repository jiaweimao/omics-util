package omics.util.ms;


import java.text.NumberFormat;

/**
 * A retention time can either be a discrete time number or a time interval.
 * This interface serves to hide this complexity from code for is not relevant.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 4:58 PM
 */
public interface RetentionTime
{
    /**
     * Constructor for a retention time.
     *
     * @param time the time for this retention time
     * @param unit the unit for the <code>time</code>
     */
    static RetentionTime discrete(final double time, final TimeUnit unit)
    {
        return new RetentionTimeDiscrete(time, unit);
    }

    /**
     * RetentionTime.
     *
     * @param time time in second.
     */
    static RetentionTime discrete(final double time)
    {
        return new RetentionTimeDiscrete(time);
    }

    /**
     * Create a Retention time from startTime to endTime.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param unit      the time unit for <code>startTime</code> and <code>endTime</code>
     */
    static RetentionTime interval(double startTime, double endTime, TimeUnit unit)
    {
        return new RetentionTimeInterval(startTime, endTime, unit);
    }

    /**
     * Create a Retention time from startTime to endTime.
     *
     * @param startTime the start time in second
     * @param endTime   the end time in second
     */
    static RetentionTime interval(double startTime, double endTime)
    {
        return new RetentionTimeInterval(startTime, endTime);
    }

    /**
     * @return retention time the units of the time is seconds
     */
    double getTime();

    /**
     * @return the smallest retention time, the units of the time is seconds
     */
    double getMinTime();

    /**
     * @return the largest retention time, the units of the time is seconds
     */
    double getMaxTime();

    /**
     * @return a copy of this RetentionTime
     */
    RetentionTime copy();

    /**
     * @param retentionTime a RetentionTime
     * @return true if the retentionTime is in the scope of this RetentionTime.
     */
    boolean contains(RetentionTime retentionTime);

    /**
     * toString with given format
     *
     * @param format {@link NumberFormat} to format time value.
     */
    String toString(NumberFormat format);
}
