package omics.util.ms;

import java.text.NumberFormat;
import java.util.Objects;

/**
 * The retention time is the time it takes for a particular analyte to pass through
 * the liquid chromatography system (from the column inlet to the detector) under set conditions.
 * <p>
 * Retention times are stored in seconds.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 4:58 PM
 */
class RetentionTimeDiscrete implements RetentionTime
{
    private final double time;

    /**
     * Constructor for a retention time. The <code>time</code> is converted to
     * seconds.
     *
     * @param time the time for this retention time
     * @param unit the unit for the <code>time</code>
     */
    protected RetentionTimeDiscrete(final double time, final TimeUnit unit)
    {
        this.time = unit.convert(time, TimeUnit.SECOND);
    }

    /**
     * Constructor, with second time
     *
     * @param time time with TimeUnit.SECOND
     */
    protected RetentionTimeDiscrete(final double time)
    {
        this.time = time;
    }

    @Override
    public double getTime()
    {
        return time;
    }

    @Override
    public double getMinTime()
    {
        return time;
    }

    @Override
    public double getMaxTime()
    {
        return time;
    }

    @Override
    public RetentionTimeDiscrete copy()
    {
        return new RetentionTimeDiscrete(this.time);
    }

    @Override
    public boolean contains(RetentionTime retentionTime)
    {
        return time == retentionTime.getMinTime() && time == retentionTime.getMaxTime();
    }

    @Override
    public String toString(NumberFormat format)
    {
        return format.format(time);
    }

    @Override
    public String toString()
    {
        return Double.toString(time);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof RetentionTimeDiscrete)) return false;
        RetentionTimeDiscrete that = (RetentionTimeDiscrete) o;
        return Double.compare(that.time, time) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(time);
    }
}
