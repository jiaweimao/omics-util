package omics.util.ms;


import java.text.NumberFormat;

/**
 * A retention time that stores a time interval. The time method returns the
 * time in the middle of the interval.
 * <p>
 * All times are stored in seconds.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 4:59 PM
 */
class RetentionTimeInterval implements RetentionTime
{
    private final double startTime, endTime;

    /**
     * Constructor for a RetentionTimeInterval from startTime to endTime. Both
     * times are converted to seconds
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param unit      the time unit for <code>startTime</code> and <code>endTime</code>
     */
    RetentionTimeInterval(double startTime, double endTime, TimeUnit unit)
    {
        if (startTime > endTime)
            throw new IllegalArgumentException("Start time has to be <= than the end time start time was " + startTime
                    + " and end time was " + endTime);

        this.startTime = unit.convert(startTime, TimeUnit.SECOND);
        this.endTime = unit.convert(endTime, TimeUnit.SECOND);
    }

    RetentionTimeInterval(double startTime, double endTime)
    {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * @return the time in the middle of the interval in seconds
     */
    @Override
    public double getTime()
    {
        return startTime + (endTime - startTime) / 2;
    }

    @Override
    public double getMinTime()
    {
        return startTime;
    }

    @Override
    public double getMaxTime()
    {
        return endTime;
    }

    @Override
    public RetentionTimeInterval copy()
    {
        return new RetentionTimeInterval(startTime, endTime);
    }

    @Override
    public boolean contains(RetentionTime retentionTime)
    {
        return retentionTime.getMinTime() >= startTime && retentionTime.getMaxTime() <= endTime;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        RetentionTimeInterval that = (RetentionTimeInterval) o;

        return Double.compare(that.endTime, endTime) == 0 && Double.compare(that.startTime, startTime) == 0;
    }

    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = startTime != +0.0d ? Double.doubleToLongBits(startTime) : 0L;
        result = (int) (temp ^ (temp >>> 32));
        temp = endTime != +0.0d ? Double.doubleToLongBits(endTime) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString()
    {
        return startTime + "-" + endTime;
    }

    @Override
    public String toString(NumberFormat format)
    {
        return format.format(startTime) + "-" + format.format(endTime);
    }
}
