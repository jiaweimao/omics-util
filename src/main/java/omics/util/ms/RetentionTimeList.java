package omics.util.ms;

import omics.util.interfaces.Copyable;
import omics.util.utils.StringUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringJoiner;


/**
 * lsit of {@link RetentionTime}
 *
 * @author JiaweiMao
 * @version 1.3.0
 * @since 29 Jan 2017, 4:57 PM
 */
public class RetentionTimeList extends ArrayList<RetentionTime> implements Copyable<RetentionTimeList>
{
    /**
     * Return {@link RetentionTimeList} from a string with format:
     * StartTime-EndTime;Time;
     *
     * @param rts formatted retention time.
     * @return {@link RetentionTimeList} from the formatted string.
     */
    public static RetentionTimeList from(String rts)
    {
        RetentionTimeList retentionTimes = new RetentionTimeList();
        for (String rt : rts.split(StringUtils.SEP1)) {
            int index = rt.indexOf('-');
            if (index > 0) {
                double left = Double.parseDouble(rt.substring(0, index));
                double right = Double.parseDouble(rt.substring(index + 1));
                retentionTimes.add(RetentionTime.interval(left, right));
            } else {
                retentionTimes.add(RetentionTime.discrete(Double.parseDouble(rt)));
            }
        }
        return retentionTimes;
    }

    /**
     * Constructor with default capacity 1.
     */
    public RetentionTimeList()
    {
        this(1);
    }

    public RetentionTimeList(int initialCapacity)
    {
        super(initialCapacity);
    }

    public RetentionTimeList(RetentionTime... retentionTimes)
    {
        Collections.addAll(this, retentionTimes);
    }

    /**
     * @return a deep copy of {@link RetentionTimeList}
     */
    @Override
    public RetentionTimeList copy()
    {
        RetentionTimeList list = new RetentionTimeList(this.size());
        for (RetentionTime retentionTime : this) {
            list.add(retentionTime.copy());
        }

        return list;
    }

    /**
     * @return the first RetentionTime object.
     */
    public RetentionTime getFirst()
    {
        return get(0);
    }

    /**
     * @return the last RetentionTime object.
     */
    public RetentionTime getLast()
    {
        return get(size() - 1);
    }

    @Override
    public boolean contains(Object o)
    {
        if (!(o instanceof RetentionTime))
            return false;

        RetentionTime rt = (RetentionTime) o;
        for (RetentionTime retentionTime : this) {
            if (retentionTime.contains(rt))
                return true;
        }
        return false;
    }

    @Override
    public String toString()
    {
        StringJoiner joiner = new StringJoiner(StringUtils.SEP1);
        for (RetentionTime time : this) {
            joiner.add(time.toString());
        }
        return joiner.toString();
    }

    public String toString(NumberFormat numberFormat)
    {
        StringJoiner joiner = new StringJoiner(StringUtils.SEP1);
        for (RetentionTime time : this) {
            if (time instanceof RetentionTimeDiscrete) {
                joiner.add(numberFormat.format(time.getTime()));
            } else {
                joiner.add(numberFormat.format(time.getMinTime()) + "-" + numberFormat.format(time.getMaxTime()));
            }
        }
        return joiner.toString();
    }

}
