package omics.util.ms;


/**
 * A scan number can either be a discrete number or a number interval.
 * <p/>
 * This interface serves to hide this complexity from code for which is not
 * relevant.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 19 Mar 2017, 1:57 PM
 */
public interface ScanNumber extends Comparable<ScanNumber>
{
    /**
     * Return a ScanNumber
     *
     * @param scan scan number
     * @return a scanNumber
     */
    static ScanNumber discrete(int scan)
    {
        return new ScanNumberDiscrete(scan);
    }

    /**
     * Return a scanNumber in given range
     *
     * @param minScanNumber start scan number
     * @param maxScanNumber end scan number
     * @return scanNumber in a range
     */
    static ScanNumber interval(int minScanNumber, int maxScanNumber)
    {
        return new ScanNumberInterval(minScanNumber, maxScanNumber);
    }

    /**
     * @return the scan number
     */
    int getValue();

    /**
     * @return the smallest scan number
     */
    int getMinScanNumber();

    /**
     * @return the largest scan number
     */
    int getMaxScanNumber();

    /**
     * Return true if this scan number contains the <code>scanNumber</code>,
     * false otherwise
     *
     * @param scanNumber the scan number to test
     */
    boolean contains(ScanNumber scanNumber);

    /**
     * Compare ScanNumber, for {@link ScanNumberDiscrete}, compare its value,
     * for {@link ScanNumberInterval}, compare their start scan.
     */
    int compareTo(ScanNumber o);

}
