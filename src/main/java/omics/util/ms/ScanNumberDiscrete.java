package omics.util.ms;

/**
 * ScanNumber of a single value.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 12 Oct 2017, 9:02 AM
 */
class ScanNumberDiscrete implements ScanNumber
{
    private final int scanNumber;

    ScanNumberDiscrete(int scanNumber)
    {
        this.scanNumber = scanNumber;
    }

    @Override
    public int getValue()
    {
        return scanNumber;
    }

    @Override
    public int getMinScanNumber()
    {
        return scanNumber;
    }

    @Override
    public int getMaxScanNumber()
    {
        return scanNumber;
    }

    @Override
    public boolean contains(ScanNumber scanNumber)
    {
        return this.scanNumber == scanNumber.getMinScanNumber() && this.scanNumber == scanNumber.getMaxScanNumber();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ScanNumberDiscrete scanId = (ScanNumberDiscrete) o;

        return scanNumber == scanId.scanNumber;
    }

    @Override
    public int hashCode()
    {
        return scanNumber;
    }

    @Override
    public String toString()
    {
        return String.valueOf(scanNumber);
    }

    @Override
    public int compareTo(ScanNumber o)
    {
        return Integer.compare(scanNumber, o.getValue());
    }
}
