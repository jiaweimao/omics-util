package omics.util.ms;


import java.util.Objects;

/**
 * ScanNumber between a min and a max value.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 19 Mar 2017, 2:03 PM
 */
class ScanNumberInterval implements ScanNumber
{
    private final int minScanNumber, maxScanNumber;

    ScanNumberInterval(int minScanNumber, int maxScanNumber)
    {
        if (minScanNumber > maxScanNumber)
            throw new IllegalArgumentException("Min scan number has to be <= than max scan number.  minScanNumber was "
                    + minScanNumber + " and maxScanNumber was " + maxScanNumber);

        this.minScanNumber = minScanNumber;
        this.maxScanNumber = maxScanNumber;
    }

    /**
     * @return average value of <code>minScanNumber</code> and <code>maxScanNumber</code>
     */
    @Override
    public int getValue()
    {
        return (maxScanNumber + minScanNumber) / 2;
    }

    @Override
    public int getMinScanNumber()
    {
        return minScanNumber;
    }

    @Override
    public int getMaxScanNumber()
    {
        return maxScanNumber;
    }

    @Override
    public boolean contains(ScanNumber scanNumber)
    {
        return scanNumber.getMinScanNumber() >= minScanNumber && scanNumber.getMaxScanNumber() <= maxScanNumber;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ScanNumberInterval)) return false;
        ScanNumberInterval that = (ScanNumberInterval) o;
        return minScanNumber == that.minScanNumber &&
                maxScanNumber == that.maxScanNumber;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(minScanNumber, maxScanNumber);
    }

    @Override
    public String toString()
    {
        return minScanNumber + "-" + maxScanNumber;
    }

    @Override
    public int compareTo(ScanNumber o)
    {
        return Integer.compare(minScanNumber, o.getMinScanNumber());
    }
}
