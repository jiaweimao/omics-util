package omics.util.ms;

import omics.util.interfaces.Copyable;
import omics.util.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.StringJoiner;

/**
 * List of ScanNumber
 *
 * @author JiaweiMao
 * @version 1.2.1
 * @since 23 Jan 2017, 2:08 PM
 */
public class ScanNumberList extends ArrayList<ScanNumber> implements Copyable<ScanNumberList>
{
    /**
     * Return a {@link ScanNumberList} from its string representation.
     *
     * @param attr scanNumberList string, different scan number separate by ;
     * @return a {@link ScanNumberList}
     */
    public static ScanNumberList from(String attr)
    {
        ScanNumberList scanNumbers = new ScanNumberList();
        String[] values = attr.split(StringUtils.SEP1);
        for (String value : values) {
            int index = value.indexOf('-');
            if (index > 0) {
                int left = Integer.parseInt(value.substring(0, index));
                int right = Integer.parseInt(value.substring(index + 1));
                scanNumbers.add(ScanNumber.interval(left, right));
            } else {
                scanNumbers.add(ScanNumber.discrete(Integer.parseInt(value)));
            }
        }
        return scanNumbers;
    }

    /**
     * Construct a ScanNumberList that contains a list of discrete <code>scanNumbers</code>
     *
     * @param scanNumbers the initial content of this ScanNumberList
     */
    public static ScanNumberList from(int... scanNumbers)
    {
        ScanNumberList list = new ScanNumberList(scanNumbers.length);
        for (int scanNumber : scanNumbers) {
            list.add(ScanNumber.discrete(scanNumber));
        }
        return list;
    }

    /**
     * Construct an empty ScanNumberList with default capacity 1.
     */
    public ScanNumberList()
    {
        this(1);
    }

    /**
     * Construct an empty ScanNumberList.
     */
    public ScanNumberList(int capacity)
    {
        super(capacity);
    }

    /**
     * Construct a ScanNumberList that contains the ScanNumber's in
     * <code>scanNumbers</code>
     *
     * @param scanNumbers the array whose elements are to be placed in this ScanNumberList
     */
    public ScanNumberList(ScanNumber... scanNumbers)
    {
        Collections.addAll(this, scanNumbers);
    }

    /**
     * Return the first scan number in this ScanNumberList
     *
     * @return the first scan number in this ScanNumberList
     * @throws IndexOutOfBoundsException if this ScanNumberList is empty
     */
    public ScanNumber getFirst()
    {
        return get(0);
    }

    /**
     * Return the last scan number in this ScanNumberList
     *
     * @return the last scan number in this ScanNumberList
     * @throws IndexOutOfBoundsException if this ScanNumberList is empty
     */
    public ScanNumber getLast()
    {
        return get(size() - 1);
    }

    /**
     * @return a shadow copy of this {@link ScanNumberList}
     */
    @Override
    public ScanNumberList copy()
    {
        ScanNumberList list = new ScanNumberList(this.size());
        list.addAll(this);
        return list;
    }

    @Override
    public boolean contains(Object o)
    {
        if (!(o instanceof ScanNumber))
            return false;

        ScanNumber nbr = (ScanNumber) o;
        for (ScanNumber scanNumber : this) {
            if (scanNumber.contains(nbr))
                return true;
        }

        return false;
    }

    @Override
    public String toString()
    {
        StringJoiner joiner = new StringJoiner(StringUtils.SEP1);
        for (ScanNumber scanNumber : this) {
            joiner.add(scanNumber.toString());
        }
        return joiner.toString();
    }
}
