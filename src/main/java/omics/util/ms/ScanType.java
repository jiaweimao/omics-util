package omics.util.ms;

/**
 * Represents the type of MS scan.
 * <p>
 * See also K. K. Murray, R. K. Boyd, M. N. Eberlin, G. J. Langley, L. Li and Y.
 * Naito, Definitions of Terms Relating to Mass Spectrometry (IUPAC
 * Recommendations 2013) Pure Appl. Chem. 2013, 85, 1515-1609.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 14 Aug 2019, 4:27 PM
 */
public enum ScanType
{
    /**
     * Full MS scan.
     */
    FULLMS,

    /**
     * Fragmentation (tandem MS, or MS/MS) scan.
     */
    MSMS,

    /**
     * id: MS:1000205
     * name: selected ion monitoring
     * def: "The operation of a mass spectrometer in which the intensities of several specific m/z values are recorded rather than the entire mass spectrum." [PSI:MS]
     * synonym: "MIM" RELATED []
     * This scan isolates a narrow range of m/z values before scanning, thus increasing the sensitivity in this range.
     */
    SIM,
    /**
     * id: MS:1000206
     * name: selected reaction monitoring
     * def: "Data acquired from specific product ions corresponding to m/z selected precursor ions recorded via multiple stages of mass spectrometry. Selected reaction monitoring can be performed in time or in space." [PSI:MS]
     * synonym: "MRM" RELATED []
     */
    SRM,

    /**
     * Data Independent Acquisition (DIA) scan.
     */
    DIA,
    /**
     * Special scan mode, where data with improved resolution is acquired. This is typically achieved
     * by scanning a more narrow m/z window or scanning with a lower scan rate."
     */
    zoom;
}
