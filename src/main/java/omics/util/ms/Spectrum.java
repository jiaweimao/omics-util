package omics.util.ms;

import omics.util.OmicsObject;
import omics.util.ms.peaklist.*;
import omics.util.ms.peaklist.impl.*;
import omics.util.utils.ObjectUtils;

import java.util.*;
import java.util.function.Function;

/**
 * A mass spectrum. It may represent a single scan in raw MS data, a chromatogram, a calculated isotope pattern,
 * a predicated fragmentation spectrum of a molecule, etc.
 *
 * @author JiaweiMao
 * @version 1.6.0
 * @since 17 Apr 2017, 1:12 PM
 */
public abstract class Spectrum<A extends PeakAnnotation> extends OmicsObject implements PeakList<A>
{
    /**
     * PeakList stored in this peaklist
     */
    private final PeakList<A> peakList;
    /**
     * the spectrum level
     */
    private int msLevel = 0;

    /**
     * Copy constructor
     *
     * @param src           the Spectrum to copy
     * @param peakProcessor Processor for the spectrum
     */
    protected Spectrum(Spectrum<A> src, PeakProcessor<A, A> peakProcessor)
    {
        peakList = src.peakList.copy(peakProcessor);
        msLevel = src.msLevel;

        this.addMetas(src);
    }

    /**
     * Copy constructor
     *
     * @param src                the Spectrum to copy
     * @param peakProcessorChain ProcessorChain for the spectrum
     */
    protected Spectrum(Spectrum<A> src, PeakProcessorChain<A> peakProcessorChain)
    {
        peakList = src.peakList.copy(peakProcessorChain);
        msLevel = src.msLevel;
        addMetas(src);
    }

    protected Spectrum(Spectrum<A> src, SpectrumProcessor<A, A> spectrumProcessor)
    {
        peakList = src.peakList.copy(spectrumProcessor);
        msLevel = src.msLevel;
        addMetas(src);
    }

    /**
     * Construct a Spectrum that has an initial capacity of
     * <code>initialCapacity</code> and a precision of <code>precision</code>
     *
     * @param initialCapacity the initial capacity
     * @param precision       the precision
     */
    public Spectrum(int initialCapacity, Precision precision)
    {
        switch (precision) {
            case DOUBLE:
                peakList = new DoublePeakList<>(initialCapacity);
                break;
            case FLOAT:
                peakList = new FloatPeakList<>(initialCapacity);
                break;
            case DOUBLE_FLOAT:
                peakList = new DoubleFloatPeakList<>(initialCapacity);
                break;
            case DOUBLE_CONSTANT:
                peakList = new DoubleConstantPeakList<>(1.0, initialCapacity);
                break;
            case FLOAT_CONSTANT:
                peakList = new FloatConstantPeakList<>(1.0, initialCapacity);
                break;
            default:
                throw new IllegalArgumentException("Cannot create a peak list for precision " + precision);
        }
    }

    /**
     * Construct a Spectrum that has a constant intensity. The precision needs
     * to be either PeakList.Precision.DOUBLE_CONSTANT or
     * PeakList.Precision.FLOAT_CONSTANT. The peak lists initial capacity is set
     * to <code>initialCapacity</code>
     *
     * @param initialCapacity   the initial capacity of the peak list
     * @param constantIntensity the constant intensity if
     * @param precision         the precision that the peak list is stored at
     */
    public Spectrum(int initialCapacity, double constantIntensity, Precision precision)
    {
        switch (precision) {
            case DOUBLE_CONSTANT:
                peakList = new DoubleConstantPeakList<>(constantIntensity, initialCapacity);
                break;
            case FLOAT_CONSTANT:
                peakList = new FloatConstantPeakList<>(constantIntensity, initialCapacity);
                break;
            default:
                throw new IllegalArgumentException(
                        "A constant intensity can only be set for DOUBLE_CONSTANT or FLOAT_CONSTANT precision.  Use " +
                                "new AbstractSpectrum(int initialCapacity, M spectrumMetaData, Precision precision)");
        }
    }

    /**
     * @return cast this spectrum to {@link MsnSpectrum}
     */
    public MsnSpectrum asMsnSpectrum()
    {
        return (MsnSpectrum) this;
    }

    /**
     * @return cast this spectrum to {@link Chromatogram}
     */
    public Chromatogram asChromatogram()
    {
        return (Chromatogram) this;
    }

    /**
     * @return true if this is a {@link MsnSpectrum} object.
     */
    public boolean isMsnSpectrum()
    {
        return this instanceof MsnSpectrum;
    }

    /**
     * @return true if this is a {@link Chromatogram} object.
     */
    public boolean isChromatogram()
    {
        return this instanceof Chromatogram;
    }

    @Override
    public void addPeaks(PeakList<A> peakList)
    {
        this.peakList.addPeaks(peakList);
    }

    @Override
    public <T extends PeakAnnotation> void addPeaksNoAnnotations(PeakList<T> peakList)
    {
        this.peakList.addPeaksNoAnnotations(peakList);
    }

    @Override
    public <T extends PeakAnnotation> void addPeaks(PeakList<T> peakList,
            Function<List<T>, List<A>> annotationConverter)
    {
        this.peakList.addPeaks(peakList, annotationConverter);
    }

    @Override
    public void addSorted(double[] xValues, double[] yValues)
    {
        peakList.addSorted(xValues, yValues);
    }

    @Override
    public void addSorted(double[] xValues, double[] yValues, int length)
    {
        peakList.addSorted(xValues, yValues, length);
    }

    @Override
    public int add(double x, double y)
    {
        return peakList.add(x, y);
    }

    @Override
    public int add(double x, double y, A annotation)
    {
        return peakList.add(x, y, annotation);
    }

    public int add(double x, double y, Collection<? extends A> annotations)
    {
        return peakList.add(x, y, annotations);
    }

    public int size()
    {
        return peakList.size();
    }

    public boolean isEmpty()
    {
        return peakList.isEmpty();
    }

    public double getX(int index)
    {
        return peakList.getX(index);
    }

    public double getY(int index)
    {
        return peakList.getY(index);
    }

    @Override
    public void clear()
    {
        peakList.clear();
    }

    @Override
    public double getTotalIonCurrent()
    {
        return peakList.getTotalIonCurrent();
    }

    @Override
    public int getClosestIndex(double x)
    {
        return peakList.getClosestIndex(x);
    }

    @Override
    public int indexOf(double xKey)
    {
        return peakList.indexOf(xKey);
    }

    @Override
    public int indexOf(int fromIndex, int toIndex, double xKey)
    {
        return peakList.indexOf(fromIndex, toIndex, xKey);
    }

    @Override
    public int[] indexOf(double minX, double maxX)
    {
        return peakList.indexOf(minX, maxX);
    }

    /**
     * Returns indexes of mzs in the specified region.
     *
     * @param mass m/z value to detect
     * @param tol  {@link Tolerance} used to match
     */
    public int[] indexOf(double mass, Tolerance tol)
    {
        return indexOf(tol.getMin(mass), tol.getMax(mass));
    }

    @Override
    public int getMostIntenseIndex(double minX, double maxX)
    {
        return peakList.getMostIntenseIndex(minX, maxX);
    }

    /**
     * Returns the index of the peak that is most intense in the specified region. Where minX <= region <= maxX.
     *
     * @param mz  mz value
     * @param tol {@link Tolerance} to match peaks
     * @return the index of the most intense peak or -1 if there is no such peak
     */
    public int getMostIntenseIndex(double mz, Tolerance tol)
    {
        return getMostIntenseIndex(tol.getMin(mz), tol.getMax(mz));
    }

    @Override
    public int getMostIntenseIndex()
    {
        return peakList.getMostIntenseIndex();
    }

    public double getBasePeakX()
    {
        return peakList.getBasePeakX();
    }

    public double getBasePeakY()
    {
        return peakList.getBasePeakY();
    }

    public double[] getXs(double[] dest)
    {
        return peakList.getXs(dest);
    }

    public double[] getXs(double[] dest, int destPos)
    {
        return peakList.getXs(dest, destPos);
    }

    @Override
    public double[] getXs(int srcPos, double[] dest, int destPos, int length)
    {
        return peakList.getXs(srcPos, dest, destPos, length);
    }

    public double[] getXs()
    {
        return getXs(null);
    }

    public double[] getYs()
    {
        return getYs(null);
    }

    public double[] getYs(double[] dest)
    {
        return peakList.getYs(dest);
    }

    @Override
    public double[] getYs(double[] dest, int destPos)
    {
        return peakList.getYs(dest, destPos);
    }

    @Override
    public double[] getYs(int srcPos, double[] dest, int destPos, int length)
    {
        return peakList.getYs(srcPos, dest, destPos, length);
    }

    @Override
    public Precision getPrecision()
    {
        return peakList.getPrecision();
    }

    @Override
    public void setYAt(double y, int index)
    {
        peakList.setYAt(y, index);
    }

    @Override
    public void trimToSize()
    {
        peakList.trimToSize();
    }

    @Override
    public void ensureCapacity(int minCapacity)
    {
        peakList.ensureCapacity(minCapacity);
    }

    @Override
    public void addAnnotation(int index, A annotation)
    {
        ObjectUtils.checkElementIndex(index, size());

        peakList.addAnnotation(index, annotation);
    }

    @Override
    public void addAnnotations(int index, Collection<A> annotations)
    {
        ObjectUtils.checkElementIndex(index, size());

        peakList.addAnnotations(index, annotations);
    }

    @Override
    public boolean removeAnnotation(A annotation, int index)
    {
        return peakList.removeAnnotation(annotation, index);
    }

    @Override
    public void clearAnnotationsAt(int index)
    {
        peakList.clearAnnotationsAt(index);
    }

    @Override
    public void clearAnnotations()
    {
        peakList.clearAnnotations();
    }

    @Override
    public boolean hasAnnotations()
    {
        return peakList.hasAnnotations();
    }

    @Override
    public int[] getAnnotationIndexes()
    {
        return peakList.getAnnotationIndexes();
    }

    @Override
    public boolean hasAnnotationsAt(int index)
    {
        return peakList.hasAnnotationsAt(index);
    }

    @Override
    public List<A> getAnnotations(int index)
    {
        return peakList.getAnnotations(index);
    }

    /**
     * Return a description for annotations of given index.
     *
     * @param index index of the peak.
     * @return a description for annotations of the peak at given index.
     */
    public String getAnnotationsText(int index, String sep)
    {
        StringJoiner joiner = new StringJoiner(sep);
        for (PeakAnnotation annotation : peakList.getAnnotations(index)) {
            joiner.add(annotation.getSymbol());
        }
        return joiner.toString();
    }

    /**
     * Return a description for annotations of given index with default separator ';'
     *
     * @param index index of the peak.
     * @return a description for annotations of the peak at given index.
     */
    public String getAnnotationsText(int index)
    {
        return getAnnotationsText(index, ";");
    }

    @Override
    public A getFirstAnnotation(int index)
    {
        return peakList.getFirstAnnotation(index);
    }

    @Override
    public void sortAnnotations(Comparator<A> comparator)
    {
        peakList.sortAnnotations(comparator);
    }

    @Override
    public Peak getPrecursor()
    {
        return peakList.getPrecursor();
    }

    /**
     * @return precursor charge
     */
    public int getPrecursorCharge()
    {
        return peakList.getPrecursor().getCharge();
    }

    /**
     * @return precursor mz
     */
    public double getPrecursorMz()
    {
        return peakList.getPrecursor().getMz();
    }

    /**
     * @return precursor mass
     */
    public double getPrecursorMass()
    {
        return peakList.getPrecursor().getMass();
    }

    @Override
    public void setPrecursor(Peak precursor)
    {
        peakList.setPrecursor(precursor);
    }

    /**
     * @return the ms level that this peak list was measured at, default to be 0.
     */
    public int getMsLevel()
    {
        return msLevel;
    }

    /**
     * Set the ms level that this peak list was measured at
     *
     * @param msLevel the ms level
     */
    public void setMsLevel(int msLevel)
    {
        this.msLevel = msLevel;
    }

    /**
     * if 90% of the intensity is below precursor mz, the precursor should be 1+
     */
    public void setChargeIfSingleCharged()
    {
        Peak precursor = getPrecursor();
        if (precursor == null || precursor.getCharge() != 0)
            return;

        double ticBlowPrecursor = 0;
        double precursorMz = precursor.getMz();
        for (int i = 0; i < size(); i++) {
            if (getX(i) < precursorMz)
                ticBlowPrecursor += getY(i);
        }
        if (ticBlowPrecursor / getTotalIonCurrent() > 0.9)
            getPrecursor().setCharge(1);
    }

    @Override
    public PeakCursor<A> cursor()
    {
        return peakList.cursor();
    }

    @Override
    public void apply(SpectrumProcessor<A, A> spectrumProcessor)
    {
        peakList.apply(spectrumProcessor);
    }

    @Override
    public void apply(PeakProcessor<A, A> peakProcessor)
    {
        peakList.apply(peakProcessor);
    }

    @Override
    public void apply(PeakProcessorChain<A> peakProcessorChain)
    {
        peakList.apply(peakProcessorChain);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Spectrum<?> spectrum = (Spectrum<?>) o;
        return msLevel == spectrum.msLevel &&
                Objects.equals(peakList, spectrum.peakList);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(peakList, msLevel);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            builder.append(getX(i) + " " + getY(i) + "\n");
        }

        return builder.toString();
    }
}
