package omics.util.ms;

import omics.util.interfaces.Filter;

/**
 * Interface for filter of spectrum.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 May 2018, 1:47 PM
 */
public interface SpectrumFilter extends Filter<MsnSpectrum> {}
