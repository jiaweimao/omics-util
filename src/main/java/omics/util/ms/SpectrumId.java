package omics.util.ms;

import omics.util.interfaces.Copyable;
import omics.util.interfaces.Identifiable;

import java.util.Optional;

/**
 * Identifier for a spectrum.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 14 Aug 2019, 3:27 PM
 */
public interface SpectrumId extends Identifiable, Copyable<SpectrumId>
{
    /**
     * builder instance with less fields, such as spectrum from mgf files.
     */
    class Builder
    {
        protected String id;
        protected MsDataId msDataID;
        protected int index = -1;

        private Double collisionEnergy = null;
        private ScanNumber precursorScan = null;
        private Double windowTargetMz = null;
        private Double windowLowerOffset = null;
        private Double windowUpperOffset = null;
        private Double ionInjectTime = null;
        private ScanType scanType = null;
        private Polarity polarity = null;
        private Double scanLowerMz = null;
        private Double scanUpperMz = null;

        public Builder() { }

        public Builder(SpectrumId spectrumID)
        {
            this.id = spectrumID.getId();
            this.msDataID = spectrumID.getMsDataId();
            this.index = spectrumID.getIndex();

            this.collisionEnergy = spectrumID.getCollisionEnergy().orElse(null);
            this.precursorScan = spectrumID.getParentScanNumber().orElse(null);
            this.windowTargetMz = spectrumID.getIsolateWindowTargetMz().orElse(null);
            this.windowLowerOffset = spectrumID.getIsolateWindowLowerOffset().orElse(null);
            this.windowUpperOffset = spectrumID.getIsolateWindowUpperOffset().orElse(null);
            this.ionInjectTime = spectrumID.getIonInjectTime().orElse(null);
            this.scanType = spectrumID.getScanType().orElse(null);
            this.polarity = spectrumID.getPolarity().orElse(null);
            this.scanLowerMz = spectrumID.getScanLowerMz().orElse(null);
            this.scanUpperMz = spectrumID.getScanUpperMz().orElse(null);
        }

        /**
         * set identifier of the spectrum
         *
         * @param id identifier
         */
        public Builder id(String id)
        {
            this.id = id;
            return this;
        }

        /**
         * set the msDataID
         *
         * @param msDataID {@link MsDataId} instance.
         */
        public Builder msDataId(MsDataId msDataID)
        {
            this.msDataID = msDataID;
            return this;
        }

        /**
         * set the index of the spectrum
         *
         * @param index spectrum index
         */
        public Builder index(int index)
        {
            this.index = index;
            return this;
        }

        /**
         * set the collision energy
         *
         * @param collisionEnergy collision energy
         */
        public Builder collisionEnergy(double collisionEnergy)
        {
            this.collisionEnergy = collisionEnergy;
            return this;
        }

        /**
         * set the {@link ScanNumber} of the precursor.
         *
         * @param scanNumber {@link ScanNumber} instance
         */
        public Builder parentScanNumber(ScanNumber scanNumber)
        {
            this.precursorScan = scanNumber;
            return this;
        }

        /**
         * set the isolate window target mz
         *
         * @param mz m/z value
         */
        public Builder isolateTargetMz(double mz)
        {
            this.windowTargetMz = mz;
            return this;
        }

        /**
         * set the isolate window lower offset
         *
         * @param mz m/z value
         */
        public Builder isolateLowerOffset(double mz)
        {
            this.windowLowerOffset = mz;
            return this;
        }

        /**
         * set the isolate window upper offset.
         *
         * @param mz m/z value
         */
        public Builder isolateUpperOffset(double mz)
        {
            this.windowUpperOffset = mz;
            return this;
        }

        /**
         * set the ion injection time.
         *
         * @param ionInjectTime ion injection time
         */
        public Builder ionInjectTime(double ionInjectTime)
        {
            this.ionInjectTime = ionInjectTime;
            return this;
        }

        /**
         * set the {@link ScanType}
         *
         * @param scanType {@link ScanType} instance.
         */
        public Builder scanType(ScanType scanType)
        {
            this.scanType = scanType;
            return this;
        }

        public Builder polarity(Polarity polarity)
        {
            this.polarity = polarity;
            return this;
        }

        public Builder scanLowerMz(double mz)
        {
            this.scanLowerMz = mz;
            return this;
        }

        public Builder scanUpperMz(double mz)
        {
            this.scanUpperMz = mz;
            return this;
        }

        public SpectrumId build()
        {
            if (collisionEnergy == null
                    && precursorScan == null
                    && windowTargetMz == null
                    && windowUpperOffset == null
                    && windowLowerOffset == null
                    && ionInjectTime == null
                    && scanType == null
                    && polarity == null
                    && scanLowerMz == null
                    && scanUpperMz == null)
                return new DefaultSpectrumId(id, msDataID, index);
            else
                return new MoreSpectrumId(id, msDataID, index, collisionEnergy, precursorScan,
                        windowTargetMz, windowLowerOffset, windowUpperOffset,
                        ionInjectTime, scanType, polarity, scanLowerMz, scanUpperMz);
        }
    }

    static SpectrumId of(String id, MsDataId dataID)
    {
        return new DefaultSpectrumId(id, dataID, -1);
    }

    static SpectrumId of(MsDataId dataID)
    {
        return new DefaultSpectrumId(null, dataID, -1);
    }

    static SpectrumId of(String id, MsDataId dataID, int index)
    {
        return new DefaultSpectrumId(id, dataID, index);
    }

    /**
     * @return index of the spectrum, -1 if it is not set.
     */
    int getIndex();

    /**
     * @return identifier, null if not set.
     */
    @Override
    String getId();

    /**
     * @return the {@link MsDataId} this spectrum belong to.
     */
    MsDataId getMsDataId();

    /**
     * @return the collision energy.
     */
    Optional<Double> getCollisionEnergy();

    /**
     * @return parent scan number.
     */
    Optional<ScanNumber> getParentScanNumber();

    /**
     * The primary or reference m/z about which the isolation window is defined.
     *
     * @return isolation window target m/z.
     */
    Optional<Double> getIsolateWindowTargetMz();

    /**
     * the extent of the isolation window in m/z below the isolation window target m/z. The lower
     * and upper may be asymmetric about the target m/z.
     *
     * @return isolation window lower offset
     */
    Optional<Double> getIsolateWindowLowerOffset();

    /**
     * The extent of the isolation window in m/z above the isolation window target m/z.
     * The lower and upper offsets may be asymmetric about the target m/z.
     *
     * @return isolation window upper offset
     */
    Optional<Double> getIsolateWindowUpperOffset();

    /**
     * Returns the ion injection time in ms, for ion trap experiments. Shorter
     * time indicates larger amount of ions (trap fills faster).
     * TimeUnit: millisecond.
     *
     * @return ion injection time, null for absent.
     */
    Optional<Double> getIonInjectTime();

    /**
     * @return {@link ScanType} of the spectrum
     */
    Optional<ScanType> getScanType();

    /**
     * @return the {@link Polarity} of this spectrum.
     */
    Optional<Polarity> getPolarity();

    /**
     * @return scanning lower mz of the spectrum
     */
    Optional<Double> getScanLowerMz();

    /**
     * @return scanning upper mz of the spectrum
     */
    Optional<Double> getScanUpperMz();
}
