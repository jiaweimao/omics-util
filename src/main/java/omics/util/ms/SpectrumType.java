package omics.util.ms;

/**
 * Way in which the spectrum is represented, either with regularly spaced data points or with a list of centroided peaks.
 * <p>
 * For exact definition of the different spectra types, see
 * Deutsch, E. W. (2012). File Formats Commonly Used in Mass Spectrometry Proteomics.
 * Molecular &amp; Cellular Proteomics, 11(12), 1612–1621. doi:10.1074/mcp.R112.019695
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Apr 2017, 2:28 PM
 */
public enum SpectrumType
{
    /**
     * A profile mass spectrum is created when data is recorded with ion current (counts per second) on one axis
     * and mass/charge ratio on another axis.
     */
    PROFILE,
    /**
     * Processing of profile data to produce spectra that contains discrete peaks of zero width.
     * Often used to reduce the size of dataset.
     */
    CENTROIDED
}
