package omics.util.ms;

import omics.util.utils.Pair;

import java.text.NumberFormat;

/**
 * Retention time unit.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 7:24 PM
 */
public enum TimeUnit
{
    /**
     * SECOND unit
     */
    SECOND,
    /**
     * MINUTE unit
     */
    MINUTE,
    /**
     * Hour unit
     */
    HOUR;

    /**
     * Convert time in second to a suitable.
     *
     * @param time time in second
     * @return suitable time values and its unit.
     */
    public static Pair<Double, TimeUnit> convert(double time)
    {
        if (time > 3600) {
            return Pair.create(time / 3600, TimeUnit.HOUR);
        } else if (time > 60) {
            return Pair.create(time / 60, TimeUnit.MINUTE);
        } else {
            return Pair.create(time, TimeUnit.SECOND);
        }
    }

    /**
     * Convert time in second to a suitable unit.
     *
     * @param time time in second
     * @return suitable time values and its unit.
     */
    public static String formatTime(double time, NumberFormat format)
    {
        if (time > 3600) {
            return format.format(time / 3600) + " hours";
        } else if (time > 60) {
            return format.format(time / 60) + " minutes";
        } else {
            return format.format(time) + " seconds";
        }
    }

    /**
     * Convert the time of this TimeUnit other to TimeUnit.
     *
     * @param time time value
     * @param to   target time unit
     * @return time value of target time unit.
     */
    public double convert(double time, TimeUnit to)
    {
        if (this == to)
            return time;

        switch (this) {
            case SECOND:
                return convertFromSeconds(time, to);
            case MINUTE:
                return convertFromMinutes(time, to);
            case HOUR:
                return convertFromHours(time, to);
            default:
                throw new IllegalStateException("Cannot convert from " + this + " to " + to);
        }
    }

    /**
     * Convert time in seconds to other time unit value
     *
     * @param time time value
     * @param to   given time unit
     * @return the time value in time unit.
     */
    private double convertFromSeconds(double time, TimeUnit to)
    {
        switch (to) {
            case SECOND:
                return time;
            case MINUTE:
                return time / 60;
            case HOUR:
                return time / 60 / 60;
            default:
                throw new IllegalStateException("Cannot convert from " + this + " to " + to);
        }
    }

    /**
     * Convert time in minutes to given TimeUnit
     *
     * @param time time value
     * @param to   given time unit
     * @return time value in time unit.
     */
    private double convertFromMinutes(double time, TimeUnit to)
    {
        switch (to) {
            case SECOND:
                return time * 60;
            case MINUTE:
                return time;
            case HOUR:
                return time / 60;
            default:
                throw new IllegalStateException("Cannot convert from " + this + " to " + to);
        }
    }

    /**
     * Convert time of hours into other unit
     *
     * @param time time in hours
     * @param to   given time unit
     * @return time in given time unit.
     */
    private double convertFromHours(double time, TimeUnit to)
    {
        switch (to) {
            case MINUTE:
                return time * 60;
            case SECOND:
                return time * 60 * 60;
            case HOUR:
                return time;
            default:
                throw new IllegalStateException("Cannot convert from " + this + " to " + to);
        }
    }
}
