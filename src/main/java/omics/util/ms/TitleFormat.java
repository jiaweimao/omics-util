package omics.util.ms;

/**
 * spectrum title format.
 *
 * @author JiaweiMao
 * @version 1.3.0
 * @since 08 Oct 2018, 3:18 PM
 */
public enum TitleFormat
{
    /**
     * Native format defined by scanId=xsd:nonNegativeInteger. [ PSI:PI ]
     */
    AGILENT_MASSHUNTER("MS:1001508", "Agilent MassHunter nativeID format\n"),
    /**
     * Native format defined by query=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    MASCOT_QUERY("MS:1001528", "Mascot query number"),
    /**
     * Native format defined by controllerType=xsd:nonNegativeInteger controllerNumber=xsd:positiveInteger scan=xsd:positiveInteger. [ PSI:MS ]
     */
    THERMO("MS:1000768", "Thermo nativeID format"),
    /**
     * Native format defined by frame=xsd:nonNegativeInteger scan=xsd:nonNegativeInteger frameType=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    UIMF("MS:1002532", "UIMF nativeID format"),
    /**
     * Native format defined by function=xsd:positiveInteger process=xsd:nonNegativeInteger scan=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    WATERS("MS:1000769", "Waters nativeID format"),
    /**
     * Native format defined by sample=xsd:nonNegativeInteger period=xsd:nonNegativeInteger cycle=xsd:nonNegativeInteger experiment=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    WIFF("MS:1000770", "WIFF nativeID format"),
    /**
     * Native format defined by scan=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    BRUKER_AGILENT_YEP("MS:1000771", "Bruker/Agilent YEP nativeID format"),
    /**
     * Native format defined by scan=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    BRUKER_BAF("MS:1000772", "Bruker BAF nativeID format"),
    /**
     * Native format defined by file=xsd:IDREF. [ PSI:MS ]
     */
    BRUKER_FID("MS:1000773", "Bruker FID nativeID format"),
    /**
     * Native identifier (UUID). [ PSI:MS ]
     */
    BRUKER_CONTAINER("MS:1002303", "Bruker Container nativeID format"),
    /**
     * Native format defined by frame=xsd:nonNegativeInteger scan=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    BRUKET_TDF("MS:1002818", "Bruker TDF nativeID format"),
    /**
     * Native format defined by declaration=xsd:nonNegativeInteger collection=xsd:nonNegativeInteger scan=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    BRUKER_U2("MS:1000823", "Bruker U2 nativeID format"),
    /**
     * Native format defined by index=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    MULTIPLE_PEAK_LIST("MS:1000774", "multiple peak list nativeID format"),
    /**
     * Native format defined by file=xsd:IDREF. [ PSI:MS ]
     */
    SINGLE_PEAK_LIST("MS:1000775", "single peak list nativeID format"),
    /**
     * Native format defined by file=xsd:IDREF. [ PSI:MS ]
     */
    SCIEX_T2D("MS:1001559", "SCIEX TOF/TOF T2D nativeID format"),
    /**
     * Native format defined by jobRun=xsd:nonNegativeInteger spotLabel=xsd:string spectrum=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    SCIEX("MS:1001480", "SCIEX TOF/TOF nativeID format"),
    /**
     * Scaffold native ID format. [ PSI:MS ]
     */
    SCAFFOLD("MS:1001562", "Scaffold nativeID format"),
    /**
     * Native format defined by scan=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    SCAN_ONLY("MS:1000776", "scan number only nativeID format"),
    /**
     * Native format defined by spectrum=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    SPECTRUM_ID("MS:1000777", "spectrum identifier nativeID format"),
    /**
     * No nativeID format indicates that the file tagged with this term does not contain spectra that can have a nativeID format. [ PSI:MS ]
     */
    NO("MS:1000824", "no nativeID format"),
    /**
     * Native format defined by source=xsd:string start=xsd:nonNegativeInteger end=xsd:nonNegativeInteger. [ PSI:MS ]
     */
    SHIMADZU_BIOTECH("MS:1000929", "Shimadzu Biotech nativeID format"),
    /**
     * Native format defined by databasekey=xsd:long. [ PSI:MS ]
     */
    SPECTRUM_PROTEINSCAPE("MS:1001531", "spectrum from ProteinScape database nativeID format"),
    /**
     * Native format defined by databasekey=xsd:string. [ PSI:MS ]
     */
    SPECTRUM_DB_STR("MS:1001532", "spectrum from database string nativeID format"),
    /**
     * Native format defined by databasekey=xsd:long. [ PSI:MS ]
     */
    SPECTRUM_DB_INT("MS:1001526", "spectrum from database integer nativeID format"),
    /**
     * Describes how the native spectrum identifiers are formated. [ PSI:MS ] root node
     */
    CUSTOM("MS:1000767", "native spectrum identifier format");

    private String accession;
    private String title;

    TitleFormat(String accession, String title)
    {
        this.accession = accession;
        this.title = title;
    }

    public String getAccession()
    {
        return accession;
    }

    public String getTitle()
    {
        return title;
    }

    /**
     * Return the title format of given PSI-MS accession
     *
     * @param accession accession
     */
    public static TitleFormat format4Acc(String accession)
    {
        for (TitleFormat format : TitleFormat.values()) {
            if (format.accession.equals(accession))
                return format;
        }
        return TitleFormat.CUSTOM;
    }

    public static TitleFormat getTitleFormat(MsFileType msFileType)
    {
        if (msFileType == MsFileType.MGF || msFileType == MsFileType.DTA || msFileType == MsFileType.PKL
                || msFileType == MsFileType.MS2)
            return TitleFormat.MULTIPLE_PEAK_LIST;
        else if (msFileType == MsFileType.MZXML) {
            return TitleFormat.SCAN_ONLY;
        } else if (msFileType == MsFileType.MZDATA) {
            return TitleFormat.SPECTRUM_ID;
        }
        return CUSTOM;
    }
}
