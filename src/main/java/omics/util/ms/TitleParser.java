package omics.util.ms;


/**
 * This interface has to be implemented by parsers that can extract information from the TITLE line of a spectrum.
 * <p>
 * The META-INF -> services and class annotation should not be changed.
 *
 * @author JiaweiMao
 * @version 1.1.1
 * @since 08 Oct 2017, 8:37 PM
 */
public interface TitleParser
{
    boolean parse(String title, MsnSpectrum spectrum);
}
