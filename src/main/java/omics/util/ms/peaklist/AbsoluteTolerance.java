package omics.util.ms.peaklist;

import java.util.Objects;

/**
 * Implementation of Tolerance where the + and - is an absolute value
 * <p>
 * Scaling to get around issues with representing some numbers as doubles. For
 * example on windows 125.3 - 125.4 = 0.10000000000000853 instead of the expected 0.1.
 * <p>
 * If the allowed error is +-0.1 then withinTolerance(125.3, 125.4) would return
 * false when it should return true
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 02 May 2017, 2:45 PM
 */
class AbsoluteTolerance extends Tolerance
{
    private final int scale = 100000;

    private final int scaledMinusError;
    private final int scaledPlusError;

    /**
     * Construct with the same minus and plus error.
     *
     * @param error error value.
     */
    protected AbsoluteTolerance(double error)
    {
        this(error, error);
    }

    protected AbsoluteTolerance(double minusError, double plusError)
    {
        super(minusError, plusError);
        this.scaledMinusError = (int) (minusError * scale);
        this.scaledPlusError = (int) (plusError * scale);
    }

    @Override
    public double getMinusErrorDa(double mass)
    {
        return minusError;
    }

    @Override
    public double getPlusErrorDa(double mass)
    {
        return plusError;
    }

    @Override
    public double getErrorWindow(double mass)
    {
        return minusError + plusError;
    }

    @Override
    public double getError(double expectedMr, double actualMr)
    {
        return Math.abs(actualMr - expectedMr);
    }

    @Override
    public Location check(double expected, double actual)
    {
        int scaledActual = (int) (actual * scale);

        int min = (int) (expected * scale) - scaledMinusError;
        int max = (int) (expected * scale) + scaledPlusError;

        if (scaledActual < min)
            return Location.SMALLER;
        else if (scaledActual > max)
            return Location.LARGER;
        else
            return Location.WITHIN;
    }

    public double getMin(double mz)
    {
        return mz - minusError;
    }

    public double getMax(double mz)
    {
        return mz + plusError;
    }

    @Override
    public boolean isAbsolute()
    {
        return true;
    }

    @Override
    public String toString()
    {
        if (minusError == plusError)
            return minusError + " Da";
        return "(" + minusError + ", " + plusError + ")" + " Da";
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbsoluteTolerance that = (AbsoluteTolerance) o;
        return Double.compare(that.minusError, minusError) == 0 &&
                Double.compare(that.plusError, plusError) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(minusError, plusError);
    }
}
