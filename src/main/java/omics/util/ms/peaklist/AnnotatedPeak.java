package omics.util.ms.peaklist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * A peak that has a list of peak annotations.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 2:11 PM
 */
public class AnnotatedPeak<A extends PeakAnnotation> extends Peak
{
    private final List<A> annotations;

    /**
     * Default constructor. Default values are:
     *
     * <pre>
     *     mz = 0.0
     *     intensity = 0.0
     *     charge = 0
     *     annotations = empty list
     * </pre>
     */
    public AnnotatedPeak()
    {
        super();
        annotations = new ArrayList<>(1);
    }

    /**
     * Constructs a AnnotatedPeak.
     *
     * @param mz          the peak m/z
     * @param intensity   the peak
     * @param charge      the peak charge
     * @param annotations the annotations
     */
    public AnnotatedPeak(double mz, double intensity, int charge, A... annotations)
    {
        super(mz, intensity, charge);
        this.annotations = new ArrayList<>(annotations.length);
        Collections.addAll(this.annotations, annotations);
    }

    /**
     * Copy constructor.
     *
     * @param src the AnnotatedPeak to copy
     */
    public AnnotatedPeak(AnnotatedPeak<A> src)
    {
        super(src);

        annotations = new ArrayList<>();
        for (A annotation : src.annotations) {
            annotations.add((A) annotation.copy());
        }
    }

    /**
     * Add <code>annotation</code> to this AnnotatedPeak
     *
     * @param annotation the annotation to add
     * @return true if the annotation was added, false otherwise
     */
    public boolean add(A annotation)
    {
        return annotations.add(annotation);
    }

    /**
     * Remove <code>annotation</code> from this AnnotatedPeak
     *
     * @param annotation the annotation to remove
     * @return true if the annotation was removed, false otherwise
     */
    public boolean remove(A annotation)
    {
        return annotations.remove(annotation);
    }

    /**
     * Removes the annotation at the specified position in this peak. Returns
     * the annotation that was removed.
     *
     * @param index the index of the annotation to be removed
     * @return the annotation previously at the specified position *
     * @throws IndexOutOfBoundsException if the index is out of range ( <tt>index &lt; 0 || index &gt;=
     *                                   getAnnotationCount()</tt>)
     */
    public A remove(int index)
    {
        return annotations.remove(index);
    }

    /**
     * Returns the annotation at the specified position in this peak.
     *
     * @param index index of the annotation to return
     * @return the annotation at the specified position in this peak
     * @throws IndexOutOfBoundsException if the index is out of range ( <tt>index &lt; 0 || index &gt;=
     *                                   getAnnotationCount()</tt>)
     */
    public A getAnnotation(int index)
    {
        return annotations.get(index);
    }

    /**
     * Returns the number of annotations in this peak. If this peak contains
     * more than <tt>Integer.MAX_VALUE</tt> annotation, returns
     * <tt>Integer.MAX_VALUE</tt>.
     *
     * @return the number of annotations in this list
     */
    public int getAnnotationCount()
    {
        return annotations.size();
    }

    /**
     * Returns true this peak has annotations, false otherwise.
     */
    public boolean hasAnnotation()
    {
        return !annotations.isEmpty();
    }

    /**
     * Returns an unmodifiable list containing this peaks annotations
     */
    public List<A> getAnnotations()
    {
        return Collections.unmodifiableList(annotations);
    }

    /**
     * Use the <code>comparator</code> to sort this peaks annotations
     *
     * @param comparator the comparator that is used to sort the annotations
     */
    public void sortAnnotations(Comparator<A> comparator)
    {
        annotations.sort(comparator);
    }

    @Override
    public AnnotatedPeak<A> copy()
    {
        return new AnnotatedPeak<>(this);
    }

    @Override
    public String toString()
    {
        return "AnnotatedPeak{mz=" + getMz() + ", charge=" + getCharge() + ", intensity=" + getIntensity()
                + ", annotations=" + annotations + "}";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        AnnotatedPeak that = (AnnotatedPeak) o;
        return annotations.equals(that.annotations);
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        result = 31 * result + annotations.hashCode();
        return result;
    }
}
