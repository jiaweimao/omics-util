package omics.util.ms.peaklist;

import omics.util.interfaces.Copyable;
import omics.util.protein.ms.Ion;

import java.util.Arrays;
import java.util.Objects;

import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * A peak in a mass spectrum with mz, charge and intensity.
 *
 * @author JiaweiMao
 * @version 1.0.2
 * @since 16 Mar 2017, 2:11 PM
 */
public class Peak implements Copyable<Peak>, Comparable<Peak>
{

    private double mz = 0.0;
    private double intensity = 0.0;
    private int[] chargeList = new int[0];
    private double mass = 0.0;

    /**
     * Default constructor. Default values are:
     * <pre>
     *     mz = 0.0
     *     intensity = 0.0
     *     charge = 0
     * </pre>
     */
    public Peak() { }

    public Peak(double mz, double intensity)
    {
        this.mz = mz;
        this.intensity = intensity;
    }

    /**
     * Constructs a Peak.
     *
     * @param mz        peak m/z
     * @param intensity peak intensity
     * @param charge    peak charges
     */
    public Peak(double mz, double intensity, int... charge)
    {
        setValues(mz, intensity, charge);
    }

    /**
     * Copy constructor
     *
     * @param peak the peak to copy
     */
    public Peak(Peak peak)
    {
        mz = peak.mz;
        mass = peak.mass;
        intensity = peak.intensity;
        chargeList = Arrays.copyOf(peak.chargeList, peak.chargeList.length);
    }

    /**
     * Factory method to construct a Peak that has an intensity of 0.
     *
     * @param mz     the peaks m/z
     * @param charge the peaks charge
     * @return the new Peak
     */
    public static Peak noIntensity(double mz, int... charge)
    {
        return new Peak(mz, 0, charge);
    }

    /**
     * Set this peaks values
     *
     * @param mz        the peak m/z
     * @param intensity the peak
     * @param charge    the peak charge
     */
    public void setValues(double mz, double intensity, int... charge)
    {
        checkArgument(mz >= 0.0, "mz value should not less than 0");
        checkArgument(intensity >= 0.0, "intensity value should not less than 0");
        checkArgument(!Double.isNaN(mz), "m/z cannot be NaN");
        checkArgument(!Double.isNaN(intensity), "intensity cannot be NaN");

        setMzAndCharge(mz, charge);
        this.intensity = intensity;
    }

    /**
     * Return the m/z of the peak
     *
     * @return the peak mz
     */
    public double getMz()
    {
        return mz;
    }


    /**
     * Returns mass of this peak
     */
    public double getMass()
    {
        if (chargeList.length == 0)
            throw new IllegalStateException("The mass is undefined because the peak does not have any charge");

        return mass;
    }

    /**
     * Return the default charge of this peak
     *
     * @return the charge
     */
    public int getCharge()
    {
        return chargeList.length == 0 ? 0 : chargeList[0];
    }

    /**
     * @return an copy of charge array
     */
    public int[] getCharges()
    {
        int[] list = new int[chargeList.length];
        System.arraycopy(chargeList, 0, list, 0, chargeList.length);
        return list;
    }

    /**
     * Return the peak intensity
     */
    public double getIntensity()
    {
        return intensity;
    }

    /**
     * Set the intensity of this peak
     *
     * @param intensity the intensity to set
     */
    public void setIntensity(double intensity)
    {
        this.intensity = intensity;
    }

    /**
     * Set the m/z and chargeList of this peak
     *
     * @param mz     the new m/z
     * @param charge the new chargeList
     */
    public void setMzAndCharge(double mz, int... charge)
    {
        if (charge.length == 1 && charge[0] == 0)
            charge = new int[0];

        // check the charge, avoid 0
        for (int i = charge.length - 1; i > 0; i--) {
            int z = charge[i];
            checkArgument(z != 0, "Charge cannot be 0. An unknown charge is specified by a empty array (new int[0])");
            charge[i] = z;
        }

        if (chargeList == null || chargeList.length != charge.length) {
            chargeList = new int[charge.length];
        }

        System.arraycopy(charge, 0, chargeList, 0, charge.length);

        if (chargeList.length >= 1) {
            mass = Ion.calcNeutralMass(mz, chargeList[0]);
        } else {
            mass = 0;
        }

        this.mz = mz;
    }

    public void setCharge(int charge)
    {
        checkArgument(charge != 0);
        if (chargeList.length != 1)
            this.chargeList = new int[1];
        chargeList[0] = charge;
        this.mass = Ion.calcNeutralMass(mz, charge);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Peak)) return false;
        Peak peak = (Peak) o;
        return Double.compare(peak.mz, mz) == 0 &&
                Double.compare(peak.intensity, intensity) == 0 &&
                Arrays.equals(chargeList, peak.chargeList);
    }

    @Override
    public int hashCode()
    {
        int result = Objects.hash(mz, intensity);
        result = 31 * result + Arrays.hashCode(chargeList);
        return result;
    }

    @Override
    public int compareTo(Peak o)
    {
        int cmp = Double.compare(getCharge(), o.getCharge());
        if (cmp != 0) {
            return cmp;
        }
        cmp = Double.compare(mz, o.mz);
        if (cmp != 0) {
            return cmp;
        }
        return Double.compare(intensity, o.intensity);
    }

    @Override
    public Peak copy()
    {
        return new Peak(this);
    }

    @Override
    public String toString()
    {
        return "Peak{" + "mz=" + mz + ", charge=" +
                Arrays.toString(chargeList) + ", intensity=" + intensity + '}';
    }
}
