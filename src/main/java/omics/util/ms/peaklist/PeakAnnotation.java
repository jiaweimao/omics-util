package omics.util.ms.peaklist;

import omics.util.chem.Symbol;
import omics.util.interfaces.Copyable;
import omics.util.protein.ms.Ion;
import omics.util.utils.NumberFormatFactory;

import java.text.NumberFormat;


/**
 * Tagging interface for an annotation of a peak in a PeakList
 *
 * @author JiaweiMao
 * @version 2.1.0
 * @since 18 Mar 2017, 1:16 PM
 */
public interface PeakAnnotation extends Copyable<PeakAnnotation>, Symbol
{
    NumberFormat MZ_FORMAT = NumberFormatFactory.getInstance();

    /**
     * @return the fragment ion type
     */
    Ion getIon();
}
