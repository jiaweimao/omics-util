package omics.util.ms.peaklist;

import java.util.List;


/**
 * A cursor for traversing a PeakList.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Mar 2017, 3:19 PM
 */
public interface PeakCursor<A extends PeakAnnotation>
{
    /**
     * Returns the number of peaks in this cursor
     */
    int size();

    /**
     * Returns <tt>true</tt> if this cursor contains no peaks.
     */
    boolean isEmpty();

    /**
     * Reset this cursor, namely change the index to -1.
     */
    void resetCursor();

    /**
     * Moves the cursor to the next peak if there are more peaks. If this method returns false the cursor
     * is currently pointing to the last peak in the peak list
     *
     * @return true if the cursor was moved, false otherwise
     */
    boolean next();

    /**
     * Moves the cursor to the previous peak if there are any previous peaks. If this method returns false
     * the cursor is currently pointing to the first peak in the peak list
     *
     * @return true if the cursor was moved, false otherwise
     */
    boolean previous();

    /**
     * Moves the cursor to the next peak only if current peak.mz &lt;= <code>mz</code>. Should be careful
     * when used with AbsoluteTolerance, as the Scaling effect of AbsoluteTolerance, two mz maybe justed
     * within tolerance, but it is exceed the mz threshold, as it is exceed 5 decimal points.
     *
     * @param mz the threshold
     * @return true if there are more peaks, false otherwise
     */
    boolean next(double mz);

    /**
     * Returns the intensity of the peak that this cursor is currently pointing at
     */
    double currIntensity();

    /**
     * Returns the annotations of the peak that this cursor is currently pointing at
     */
    List<A> currAnnotations();

    /**
     * Returns the m/z of the peak that this cursor is currently pointing at
     */
    double currMz();

    /**
     * Returns true if there are n more peaks in the peak list
     *
     * @param n the number of peaks removed from the current peak
     * @return true if there are n more peaks in the peak list
     */
    boolean canPeek(int n);

    /**
     * Returns the intensity of the peak that is n positions moved from the
     * peak that this cursor is currently pointing at
     *
     * @param n the number of peaks removed from the current peak
     */
    double peekIntensity(int n);

    /**
     * Returns the m/z of the peak that is n positions removed from the peak
     * that this cursor is currently pointing at
     *
     * @param n the number of peaks removed from the current peak
     * @return the m/z of the peak that is n positions removed from the peak that this cursor is currently pointing at
     */
    double peekMz(int n);

    /**
     * Returns the m/z of the last peak
     */
    double lastMz();

    /**
     * Returns the intensity of the last peak
     */
    double lastIntensity();

    /**
     * Returns the m/z of the peak at index
     *
     * @param index the index of the peak
     * @return the m/z of the peak at index
     */
    double mz(int index);

    /**
     * Returns the intensity of the peak at index
     *
     * @param index the index of the peak
     * @return the intensity of the peak at index
     */
    double intensity(int index);

    /**
     * Move the cursor to the m/z that is closest to <code>mz</code>. If
     * <code>mz</code> is exactly in the middle of two peaks the cursor is moved
     * to the peak with the smaller mz
     *
     * @param mz the m/z value to which the cursor is to be moved
     */
    void moveToClosest(double mz);

    /**
     * Move the cursor right before <code>mz</code> (peak.mz &lt;
     * <code>mz</code>), the index could be -1 if the mz value <= the first mz.
     *
     * @param mz the m/z value to which the cursor is to be moved before
     */
    void moveBefore(double mz);

    /**
     * Move the cursor right after <code>mz</code> (peak.mz &gt; <code>mz</code>), if the the <code>mz</code>
     * if larger than the last mz, cursor is size-1.
     *
     * @param mz the m/z value to which the cursor is to be moved past
     */
    void movePast(double mz);

    /**
     * Returns the index of the peak which is closest to mz. If mz is exactly in
     * the middle of two peaks the index of the peak with the smaller mz is returned.
     *
     * @param mz the m/z for which the closest index is to be found
     * @return the index of the peak which is closest to mz
     */
    int getClosestIndex(double mz);
}
