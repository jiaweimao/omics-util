package omics.util.ms.peaklist;

import omics.util.ms.peaklist.impl.UnsortedPeakListException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;


/**
 * PeakList is a object with 2-dimension double value, which is used to store spectrum data.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 18 Jan 2017, 8:59 PM
 */
public interface PeakList<A extends PeakAnnotation>
{
    /**
     * Add peaks and annotations in <code>peakList</code> to this peak list.
     *
     * @param peakList containing peaks and annotations which are to be added to this peak list
     */
    void addPeaks(PeakList<A> peakList);

    /**
     * Add peaks (mz, intensity pairs) and optional annotations in <code>peakList</code> to this peak list
     *
     * @param peakList            containing peaks which are to be added to this peak list
     * @param annotationConverter convert annotation T to A (annotation will not be added if Optional<A> is absent)
     */
    <T extends PeakAnnotation> void addPeaks(PeakList<T> peakList, Function<List<T>, List<A>> annotationConverter);

    /**
     * Add peaks (without annotations) in <code>peakList</code> to this peak list.
     *
     * @param peakList containing peaks which are to be added to this peak list
     */
    <T extends PeakAnnotation> void addPeaksNoAnnotations(PeakList<T> peakList);

    /**
     * Add the pairs of mz and intensities to this peak list. The mzs array has to be sorted.
     * If the m/z array is not sorted, an UnsortedPeakListException will be thrown.
     *
     * @param xValues the x values (e.g. mz values)
     * @param yValues the y values (e.g. intensity values)
     * @throws ArrayIndexOutOfBoundsException if length is < 0 or larger than the x or y array
     * @throws UnsortedPeakListException      if the x values are not sorted
     */
    void addSorted(double[] xValues, double[] yValues);

    /**
     * Add the pairs of mz and intensities to this peak list. The mzs array has to be sorted.
     * If the m/z array is not sorted an UnsortedPeakListException will be thrown.
     *
     * @param xValues the mz values
     * @param yValues the intensities
     * @param length  the number of pairs to add
     * @throws ArrayIndexOutOfBoundsException if length is < 0 or larger than the mz or intensity array
     * @throws UnsortedPeakListException      if the mzs are not sorted
     */
    void addSorted(double[] xValues, double[] yValues, int length);

    /**
     * Add a mz, intensity pair to the peak list
     *
     * @param x the x (e.g. mz) to add
     * @param y the y (e.g. intensity) to add
     * @return the index at which the peak was added
     */
    int add(double x, double y);

    /**
     * Add a x (e.g. mz), y (e.g. intensity) pair and associated annotations to the peak list
     *
     * @param x          the x to add
     * @param y          the y to add
     * @param annotation the peaks annotation
     * @return the index at which the peak was added
     */
    int add(double x, double y, A annotation);

    /**
     * Add a mz, intensity pair and associated annotations to the peak list
     *
     * @param x           the mz to add
     * @param y           the intensity to add
     * @param annotations the peaks annotations
     * @return the index at which the peak was added
     */
    int add(double x, double y, Collection<? extends A> annotations);

    /**
     * @return the number of peaks in this peak list
     */
    int size();

    /**
     * @return <tt>true</tt> if this peak list contains no peaks
     */
    boolean isEmpty();

    /**
     * Return the X value at index
     *
     * @param index the index
     * @return the x of the peak at index
     */
    double getX(int index);

    /**
     * Returns the y value of the peak at index
     *
     * @param index the index
     */
    double getY(int index);

    /**
     * Replace the intensity at index with intensity
     *
     * @param y     the new y value
     * @param index the index for which the y is to be set
     * @throws IndexOutOfBoundsException if the index is out of range ( <tt>index &lt; 0 || index &gt;= size()</tt>)
     */
    void setYAt(double y, int index);

    /**
     * Clears the peak list, the size is set to 0 and objects used to store
     * peaks are unreferenced to allow garbage collection
     */
    void clear();

    /**
     * @return Returns the sum of all y values (total ion current or TIC)
     */
    double getTotalIonCurrent();

    /**
     * Returns the index of the peak that is closest to the supplied x. If x
     * is exactly equidistant between two peaks the index of the smaller peak is returned
     *
     * @param x the x value
     */
    int getClosestIndex(double x);

    /**
     * Returns the index of the first occurrence of the specified xKey in this peak list, or
     * (-(<i>insertion point</i>) - 1) if this peak list does not contain the exact xKey. The <i>insertion point</i>
     * is defined as the point at which the key would be inserted into the peak list: the index of the first xKey
     * greater than the key, or <tt>size()</tt> if all x's in the peak list are less than the specified key.
     * Note that this guarantees that the return value will be &gt;= 0 if and only if the key is found.
     *
     * @param xKey the search key
     * @return index of the search key, if it is contained in the peak list; otherwise, <tt>(-(<i>insertion point</i>) -
     * 1)</tt>. The <i>insertion point</i> is defined as the point at which the key would be inserted into the peak
     * list: the index of the first xKey greater than the key, or <tt>size()</tt> if all m/z's in the peak list are
     * less than the specified key. Note that this guarantees that the return value will be &gt;= 0 if and only if the
     * key is found.
     */
    int indexOf(double xKey);

    /**
     * Returns the index of the first occurrence of the specified xKey in the specified range within this peak list,
     * or (-(<i>insertion point</i>) - 1) if this peak list does not contain the exact xKey. The
     * <i>insertion point</i> is defined as the point at which the key would be inserted into the peak list:
     * the index of the first xKey greater than the key, or <tt>size()</tt> if all m/z's in the peak list are less
     * than the specified key. Note that this guarantees that the return value will be &gt;= 0 if and only if the key is found.
     *
     * @param fromIndex the index of the first peak (inclusive) to be searched
     * @param toIndex   the index of the last peak (exclusive) to be searched
     * @param xKey      the search key
     * @return index of the search key, if it is contained in the peak list; otherwise, <tt>(-(<i>insertion point</i>) -
     * 1)</tt>. The <i>insertion point</i> is defined as the point at which the key would be inserted into the peak
     * list: the index of the first xKey greater than the key, or <tt>size()</tt> if all m/z's in the peak list are
     * less than the specified key. Note that this guarantees that the return value will be &gt;= 0 if and only if the
     * key is found.
     */
    int indexOf(int fromIndex, int toIndex, double xKey);

    /**
     * Returns the index of the peak that is most intense in the specified region. Where minX <= region <= maxX.
     *
     * @param minX the minX
     * @param maxX the maxX
     * @return the index of the most intense peak or -1 if there is no such peak
     */
    int getMostIntenseIndex(double minX, double maxX);

    /**
     * Returns indexes of mzs in the specified region. where minX <= region <= maxX.
     *
     * @param minX min mz
     * @param maxX max mz
     */
    int[] indexOf(double minX, double maxX);

    /**
     * Returns the index of the most intense peak in the peak list or -1 if there are no peaks in this peak list.
     *
     * @return the index of the most intense peak in the peak list
     */
    int getMostIntenseIndex();

    /**
     * @return the m/z of the most intense peak
     */
    double getBasePeakX();

    /**
     * @return the intensity of the most intense peak
     */
    double getBasePeakY();

    /**
     * @return x values
     */
    double[] getXs();

    /**
     * Copies an the xs from this peak list, beginning at 0, to the destination array. The number of xs copied is equal
     * to the size of this peak list. The xs at positions 0 through size() in this peak list are copied into positions
     * 0 through size(), respectively, of the destination array.
     * <p/>
     * If <code>dest</code> is <code>null</code>, then a new array with the correct length is created.
     * <p/>
     * Otherwise, if any of the following is true, an <code>IndexOutOfBoundsException</code> is thrown and the
     * destination is not modified:
     * <ul>
     * <li><code>size()</code> is greater than <code>dest.length</code>, the length of the destination array.
     * </ul>
     * <p/>
     *
     * @param dest the destination array.
     * @return dest or the newly created array if dest was null
     * @throws IndexOutOfBoundsException if copying would cause access of data outside array bounds.
     */
    double[] getXs(double[] dest);

    /**
     * Copies an the mzs from this peak list, beginning at 0, to the specified
     * position of the destination array. The number of mzs copied is equal to
     * the size of this peak list. The mzs at positions 0 through size() in this
     * peak list are copied into positions <code>destPos</code> through
     * <code>destPos+length-1</code>, respectively, of the destination array.
     * <p/>
     * If <code>dest</code> is <code>null</code>, then a new array with the
     * correct length is created.
     * <p/>
     * Otherwise, if any of the following is true, an
     * <code>IndexOutOfBoundsException</code> is thrown and the destination is
     * not modified:
     * <ul>
     * <li>The <code>destPos</code> argument is negative.
     * <li><code>destPos+size()</code> is greater than <code>dest.length</code>,
     * the length of the destination array.
     * </ul>
     * <p/>
     *
     * @param dest    the destination array.
     * @param destPos starting position in the destination data.
     * @return dest or the newly created array if dest was null
     * @throws IndexOutOfBoundsException if copying would cause access of data outside array bounds.
     */
    double[] getXs(double[] dest, int destPos);

    /**
     * Copies an the mzs from this peak list, beginning at the specified
     * position, to the specified position of the destination array. A
     * sub-sequence of mzs are copied from this peak list to the destination
     * array referenced by <code>dest</code>. The number of mzs copied is equal
     * to the <code>length</code> argument. The mzs at positions
     * <code>srcPos</code> through <code>srcPos+length-1</code> in this peak
     * list are copied into positions <code>destPos</code> through
     * <code>destPos+length-1</code>, respectively, of the destination array.
     * <p/>
     * If <code>dest</code> is <code>null</code>, then a new array with the
     * correct length is created.
     * <p/>
     * Otherwise, if any of the following is true, an
     * <code>IndexOutOfBoundsException</code> is thrown and the destination is
     * not modified:
     * <ul>
     * <li>The <code>srcPos</code> argument is negative.
     * <li>The <code>destPos</code> argument is negative.
     * <li>The <code>length</code> argument is negative.
     * <li><code>srcPos+length</code> is greater than <code>size()</code>, the
     * size of this.
     * <li><code>destPos+length</code> is greater than <code>dest.length</code>,
     * the length of the destination array.
     * </ul>
     * <p/>
     *
     * @param srcPos  starting position in the mz.
     * @param dest    the destination array.
     * @param destPos starting position in the destination data.
     * @param length  the number of array elements to be copied.
     * @return dest or the newly created array if dest was null
     * @throws IndexOutOfBoundsException if copying would cause access of data outside array bounds.
     */
    double[] getXs(int srcPos, double[] dest, int destPos, int length);

    /**
     * Returns intensity values
     *
     * @return an array of double.
     */
    double[] getYs();

    /**
     * Copies an the intensities from this peak list, beginning at 0, to the
     * destination array. The number of intensities copied is equal to the size
     * of this peak list. The intensities at positions 0 through size() in this
     * peak list are copied into positions 0 through size(), respectively, of
     * the destination array.
     * <p/>
     * If <code>dest</code> is <code>null</code>, then a new array with the
     * correct length is created.
     * <p/>
     * Otherwise, if any of the following is true, an
     * <code>IndexOutOfBoundsException</code> is thrown and the destination is
     * not modified:
     * <ul>
     * <li><code>size()</code> is greater than <code>dest.length</code>, the
     * length of the destination array.
     * </ul>
     * <p/>
     *
     * @param dest the destination array.
     * @return dest or the newly created array if dest was null
     * @throws IndexOutOfBoundsException if copying would cause access of data outside array bounds.
     */
    double[] getYs(double[] dest);

    /**
     * Copies an the intensities from this peak list, beginning at 0, to the
     * specified position of the destination array. The number of intensities
     * copied is equal to the size of this peak list. The intensities at
     * positions 0 through size() in this peak list are copied into positions
     * <code>destPos</code> through <code>destPos+length-1</code>, respectively,
     * of the destination array.
     * <p/>
     * If <code>dest</code> is <code>null</code>, then a new array with the
     * correct length is created.
     * <p/>
     * Otherwise, if any of the following is true, an
     * <code>IndexOutOfBoundsException</code> is thrown and the destination is
     * not modified:
     * <ul>
     * <li>The <code>destPos</code> argument is negative.
     * <li><code>destPos+size()</code> is greater than <code>dest.length</code>,
     * the length of the destination array.
     * </ul>
     * <p/>
     *
     * @param dest    the destination array.
     * @param destPos starting position in the destination data.
     * @return dest or the newly created array if dest was null
     * @throws IndexOutOfBoundsException if copying would cause access of data outside array bounds.
     */
    double[] getYs(double[] dest, int destPos);

    /**
     * Copies an the intensities from this peak list, beginning at the specified
     * position, to the specified position of the destination array. A
     * sub-sequence of intensities are copied from this peak list to the
     * destination array referenced by <code>dest</code>. The number of
     * intensities copied is equal to the <code>length</code> argument. The
     * intensities at positions <code>srcPos</code> through
     * <code>srcPos+length-1</code> in this peak list are copied into positions
     * <code>destPos</code> through <code>destPos+length-1</code>, respectively,
     * of the destination array.
     * <p/>
     * If <code>dest</code> is <code>null</code>, then a new array with the
     * correct length is created.
     * <p/>
     * Otherwise, if any of the following is true, an
     * <code>IndexOutOfBoundsException</code> is thrown and the destination is
     * not modified:
     * <ul>
     * <li>The <code>srcPos</code> argument is negative.
     * <li>The <code>destPos</code> argument is negative.
     * <li>The <code>length</code> argument is negative.
     * <li><code>srcPos+length</code> is greater than <code>size()</code>, the
     * size of this.
     * <li><code>destPos+length</code> is greater than <code>dest.length</code>,
     * the length of the destination array.
     * </ul>
     * <p/>
     *
     * @param srcPos  starting position in the mz.
     * @param dest    the destination array.
     * @param destPos starting position in the destination data.
     * @param length  the number of array elements to be copied.
     * @return dest or the newly created array if dest was null
     * @throws IndexOutOfBoundsException if copying would cause access of data outside array bounds.
     */
    double[] getYs(int srcPos, double[] dest, int destPos, int length);

    /**
     * Trims the capacity of this <tt>PeakList</tt> instance to be the list's current size.
     * <p>
     * An application can use this operation to minimize the storage of a <tt>PeakList</tt> instance.
     */
    void trimToSize();

    /**
     * Increases the capacity of this <tt>PeakList</tt> instance, if necessary, to ensure that
     * it can hold at least the number of peaks specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    void ensureCapacity(int minCapacity);

    /**
     * @return the precision that this PeakList uses to store the mz and intensity values
     */
    Precision getPrecision();

    /**
     * Add an annotation to the peak at index
     *
     * @param index      the index of the peak
     * @param annotation the annotation to add
     */
    void addAnnotation(int index, A annotation);

    /**
     * Set the annotations for the peak at index
     *
     * @param index       the index of the peak
     * @param annotations the list of annotations
     */
    void addAnnotations(int index, Collection<A> annotations);

    /**
     * Remove the annotation from the peak at index
     *
     * @param annotation the annotation to remove
     * @param index      the index of the peak
     * @return <tt>true</tt> if an annotation was removed as a result of this call
     */
    boolean removeAnnotation(A annotation, int index);

    /**
     * Remove all annotations for the peak at index
     *
     * @param index the index of the peak
     */
    void clearAnnotationsAt(int index);

    /**
     * Remove all annotations from this PeakList
     */
    void clearAnnotations();

    /**
     * Returns true if the spectrum has annotations
     */
    boolean hasAnnotations();

    /**
     * Returns an array containing the sorted indexes of the peaks that are annotated.
     * <p/>
     * If this peak list has no annotated peaks an clear array is returned
     */
    int[] getAnnotationIndexes();

    /**
     * Returns true if the spectrum has annotations at peak index
     *
     * @param index the index of the peak
     */
    boolean hasAnnotationsAt(int index);

    /**
     * Returns an unmodifiable list containing the annotations for the peak at index, empty list for absent.
     *
     * @param index the index of the peak
     */
    List<A> getAnnotations(int index);

    /**
     * Return the first annotation in the list of annotations for the peak at index.
     * If the specified index does not have any annotations null is returned.
     *
     * @param index the index of the peak
     * @return the first annotation in the list of annotations, or null if the specified index has no annotations
     */
    A getFirstAnnotation(int index);

    /**
     * Sort the annotations for each annotated peak using the <code>comparator</code>
     *
     * @param comparator the comparator that is to be used to sort the annotations
     */
    void sortAnnotations(Comparator<A> comparator);

    /**
     * Return PeakList copy processed with PeakProcessor
     *
     * @param peakProcessor a PeakProcessor
     * @return PeakList after processed with PeakProcessor
     */
    PeakList<A> copy(PeakProcessor<A, A> peakProcessor);

    /**
     * Return PeakList copy processed with {@link SpectrumProcessor}
     *
     * @param spectrumProcessor a {@link SpectrumProcessor}
     * @return {@link PeakList} after processed with {@link SpectrumProcessor}
     */
    PeakList<A> copy(SpectrumProcessor<A, A> spectrumProcessor);

    /**
     * Return PeakList copy processed with PeakProcessorChain
     *
     * @param peakProcessorChain List of PeakProcessor
     * @return PeakList after processed with PeakProcessorChain
     */
    PeakList<A> copy(PeakProcessorChain<A> peakProcessorChain);

    /**
     * Returns the precursor peak of this peak list
     */
    Peak getPrecursor();

    /**
     * Set the precursor peak for this peak list
     */
    void setPrecursor(Peak precursor);

    /**
     * Returns a cursor over the peaks in this peak list in proper sequence.
     */
    PeakCursor<A> cursor();

    /**
     * Apply the spectrumProcessor to the peaks in this peakList.
     *
     * @param spectrumProcessor the {@link SpectrumProcessor}
     */
    void apply(SpectrumProcessor<A, A> spectrumProcessor);

    /**
     * Apply the peakProcessor to the peaks in this PeakList
     *
     * @param peakProcessor the peak processor
     */
    void apply(PeakProcessor<A, A> peakProcessor);

    /**
     * Apply the peakProcessorChain to the peaks in this PeakList
     *
     * @param peakProcessorChain the peak processor list
     */
    void apply(PeakProcessorChain<A> peakProcessorChain);
}
