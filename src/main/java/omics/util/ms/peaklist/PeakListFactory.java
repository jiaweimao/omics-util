package omics.util.ms.peaklist;

import omics.util.ms.peaklist.impl.*;

/**
 * Factory class for PeakList.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Oct 2017, 7:05 PM
 */
public class PeakListFactory {

    private PeakListFactory() {
        throw new AssertionError("cannot instantiate this class");
    }

    public static <A extends PeakAnnotation> PeakList<A> newPeakList(Precision precision) {
        return newPeakList(precision, 0);
    }

    public static <A extends PeakAnnotation> PeakList<A> newPeakList(Precision precision, int initialCapacity) {
        PeakList<A> peakList;

        switch (precision) {

            case DOUBLE:
                peakList = new DoublePeakList<>(initialCapacity);
                break;
            case FLOAT:
                peakList = new FloatPeakList<>(initialCapacity);
                break;
            case DOUBLE_FLOAT:
                peakList = new DoubleFloatPeakList<>(initialCapacity);
                break;
            case DOUBLE_CONSTANT:
                peakList = new DoubleConstantPeakList<>(1, initialCapacity);
                break;
            case FLOAT_CONSTANT:
                peakList = new FloatConstantPeakList<>(1, initialCapacity);
                break;
            default:
                throw new IllegalStateException("Cannot create peak list with precision " + precision);
        }

        return peakList;
    }

    public static <A extends PeakAnnotation> PeakList<A> newPeakList(Precision precision, double[] mzs,
            double[] intensities) {
        return newPeakList(precision, mzs, intensities, mzs.length);
    }

    public static <A extends PeakAnnotation> PeakList<A> newPeakList(Precision precision, double[] mzs,
            double[] intensities, int length) {
        PeakList<A> peakList = newPeakList(precision, length);

        peakList.addSorted(mzs, intensities, length);

        return peakList;
    }
}
