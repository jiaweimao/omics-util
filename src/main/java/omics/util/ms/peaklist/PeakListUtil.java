package omics.util.ms.peaklist;

import omics.util.ms.peaklist.impl.DoublePeakList;

import java.util.ArrayList;

/**
 * @author JiaweiMao
 * @version 1.2.0
 * @since 13 May 2017, 4:41 PM
 */
public class PeakListUtil
{
    /**
     * parse a string to peak list
     *
     * @param peaks peak list split with ':' or ';'
     * @return peak list.
     */
    public static <X extends PeakAnnotation> PeakList<X> parse(String peaks)
    {
        PeakList<X> peakList = new DoublePeakList<>();
        String[] array = peaks.split("[;:]");
        for (int i = 0; i < array.length / 2; i++) {
            double mz = Double.parseDouble(array[i * 2]);
            double in = Double.parseDouble(array[i * 2 + 1]);
            peakList.add(mz, in);
        }
        return peakList;
    }

    /**
     * parse a string to peak list.
     * mz and intensity split by space,
     * and evey peak is splitted by "\n"
     */
    public static <X extends PeakAnnotation> PeakList<X> parsePeakList(String values)
    {
        String[] split = values.split("\n");
        PeakList<X> pl = new DoublePeakList<>(split.length);
        for (String s : split) {
            String[] peaks = s.split("\\s+");
            pl.add(Double.parseDouble(peaks[0]), Double.parseDouble(peaks[1]));
        }

        return pl;
    }

    /**
     * Split a PeakList with given window size.
     *
     * @param rawPeaks original peak list
     * @param aWindow  window size
     * @return splited list of PeakList
     */
    public static <A extends PeakAnnotation> ArrayList<PeakList<A>> split(PeakList<A> rawPeaks, double aWindow)
    {
        ArrayList<PeakList<A>> spectra = new ArrayList<>();
        double minMz = rawPeaks.getX(0);
        double maxMz = rawPeaks.getX(rawPeaks.size() - 1);

        PeakCursor<A> cursor = rawPeaks.cursor();

        while (minMz < maxMz) {
            double tempMax = minMz + aWindow;

            PeakList<A> temp = new DoublePeakList<>();
            while (cursor.next()) {
                double mz = cursor.currMz();
                if (mz <= tempMax) {
                    double intensity = cursor.currIntensity();
                    temp.add(mz, intensity);
                } else {
                    cursor.previous();
                    break;
                }
            }

            if (!temp.isEmpty()) {
                spectra.add(temp);
            }
            minMz = tempMax;
        }
        return spectra;
    }

    /**
     * Calculate the vector length of peak list intensities.
     *
     * @param intensityList list of intensities.
     * @param size          peak list size.
     * @return vector length.
     */
    public static double calcLength(double[] intensityList, int size)
    {
        double dist = 0;

        for (int i = 0; i < size; i++) {

            double x = intensityList[i];

            dist += x * x;
        }

        return Math.sqrt(dist);
    }

    public static double calcLength(float[] intensityList, int size)
    {
        double dist = 0;

        for (int i = 0; i < size; i++) {

            double x = intensityList[i];

            dist += x * x;
        }

        return Math.sqrt(dist);
    }
}
