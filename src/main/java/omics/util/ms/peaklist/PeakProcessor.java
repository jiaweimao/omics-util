package omics.util.ms.peaklist;

/**
 * A peak sink that is one element in a peak processor chain.
 * <p>
 * The peak processor is used to create a processor chain to process a PeakList.
 * For example, to extract the top 100 peaks from a PeakList and then normalize
 * to the second most intense peak a processor chain is created as follows:
 *
 * <code>
 * NPeaksFilter topPeaksFilter = new TopPeakFilter(100);
 * <p>
 * NthPeakNormalizer scaleToPeakProcessor = new NthPeakNormalizer(2);
 * <p>
 * PeakProcessorChain processorChain = new PeakProcessorChain().add(topPeaksFilter).add(scaleToPeakProcessor);
 * //To process in place
 * peakList.apply(processorChain);
 * <p>
 * //To create a copy that is processed
 * peakList.copy(processorChain);
 * </code>
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Mar 2017, 9:31 PM
 */
public interface PeakProcessor<IN extends PeakAnnotation, OUT extends PeakAnnotation>
        extends PeakSource<OUT>, PeakSink<IN> {}
