package omics.util.ms.peaklist;

import it.unimi.dsi.fastutil.doubles.DoubleList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * A list of peak processors.
 * * <p>
 * * <b>Note that this implementation is not synchronized.</b> If multiple threads
 * * access a PeakProcessorChain instance concurrently the process method must be
 * * synchronized externally.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 06 May 2017, 4:32 PM
 */
public class PeakProcessorChain<A extends PeakAnnotation> {

    private final List<PeakProcessor<A, A>> list = new ArrayList<>();
    private PeakSink<A> sink;

    public PeakProcessorChain() { }

    public PeakProcessorChain(PeakProcessor<A, A> peakProcessor) {
        requireNonNull(peakProcessor);
        list.add(peakProcessor);
    }

    public PeakProcessorChain<A> add(PeakProcessor<A, A> peakProcessor) {
        requireNonNull(peakProcessor);

        list.add(peakProcessor);
        return this;
    }

    public void process(PeakList<A> peakList, PeakSink<A> peakSink) {
        requireNonNull(peakList);
        requireNonNull(peakSink);

        sink = peakSink;
        PeakSink<A> firstSink = initialize();

        firstSink.start(peakList.size());
        for (int i = 0; i < peakList.size(); i++) {
            firstSink.processPeak(peakList.getX(i), peakList.getY(i), peakList.getAnnotations(i));
        }
        firstSink.end();
    }

    public void process(DoubleList mzList, DoubleList intensityList, Int2ObjectMap<List<A>> annotationMap,
            PeakSink<A> peakSink) {
        requireNonNull(mzList);
        requireNonNull(intensityList);
        requireNonNull(annotationMap);
        requireNonNull(peakSink);

        sink = peakSink;

        PeakSink<A> firstSink = initialize();

        int size = Math.min(mzList.size(), intensityList.size());
        firstSink.start(size);
        for (int i = 0; i < size; i++) {

            List<A> annotations = annotationMap.get(i);
            if (annotations == null)
                annotations = Collections.emptyList();

            firstSink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), annotations);
        }
        firstSink.end();
    }

    /**
     * Process a peak list
     *
     * @param mzArray        mz array
     * @param intensityArray intensity array
     * @param annotationMap  annotations of the peak list
     * @param peakSink       a PeakSink
     */
    public void process(Number[] mzArray, Number[] intensityArray, Int2ObjectMap<List<A>> annotationMap,
            PeakSink<A> peakSink) {
        requireNonNull(mzArray);
        requireNonNull(intensityArray);
        requireNonNull(peakSink);
        requireNonNull(annotationMap);

        sink = peakSink;
        PeakSink<A> firstSink = initialize();
        int size = Math.min(mzArray.length, intensityArray.length);
        firstSink.start(size);
        for (int i = 0; i < size; i++) {
            List<A> annotations = annotationMap.get(i);
            if (annotations == null) {
                annotations = Collections.emptyList();
            }
            firstSink.processPeak(mzArray[i].doubleValue(), intensityArray[i].doubleValue(), annotations);
        }
        firstSink.end();
    }

    /**
     * initialize the PeakSink in a chain, the first PeakProcessor's sink is the second PeakProcessor...
     *
     * @return the First PeakSink
     */
    private PeakSink<A> initialize() {
        for (int i = 0; i < list.size() - 1; i++) {
            list.get(i).sink(list.get(i + 1));
        }

        PeakSink<A> firstSink;
        if (!list.isEmpty()) {
            list.get(list.size() - 1).sink(this.sink);
            firstSink = list.get(0);
        } else {
            firstSink = this.sink;
        }

        return firstSink;
    }

    /**
     * Returns true if this PeakProcessorChain contains no PeakProcessor.
     */
    public boolean isEmpty() {
        return list.isEmpty();
    }

    /**
     * @return {@link PeakProcessor} in this chain.
     */
    public List<PeakProcessor<A, A>> getProcessorList() {
        return list;
    }
}
