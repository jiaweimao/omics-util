package omics.util.ms.peaklist;

import java.util.List;


/**
 * Interface to be implemented by any class that is a sink for peaks
 * <p>
 * Before a PeakList is processed, start is called, then processPeak is called
 * for every peak and finally the end method is called.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Mar 2017, 9:32 PM
 */
public interface PeakSink<A extends PeakAnnotation>
{
    /**
     * Called before a new PeakList is processed.
     * <p>
     * The size parameter is used to give an indication of the number of peaks
     * that will be passed to this PeakSinks. The size does not have to be
     * correct.
     *
     * @param size the number of peaks that will be processed.
     */
    void start(int size);

    /**
     * Process the peak, the order for adding the peaks does not matter.
     *
     * @param mz          the peak's m/z
     * @param intensity   the peak's intensity
     * @param annotations the peak's annotations
     */
    void processPeak(double mz, double intensity, List<A> annotations);

    /**
     * Called after a PeakList is processed
     */
    void end();
}
