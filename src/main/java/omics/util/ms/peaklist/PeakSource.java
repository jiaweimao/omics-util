package omics.util.ms.peaklist;

/**
 * Any object with peaks.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Mar 2017, 9:33 PM
 */
public interface PeakSource<A extends PeakAnnotation>
{
    /**
     * Set the next element in the peak processor chain. The sink is where the
     * results of the peak processing are pushed
     *
     * @param sink the next element in the peak processor chain
     */
    <PS extends PeakSink<A>> PS sink(PS sink);
}
