package omics.util.ms.peaklist;

import java.util.Objects;

/**
 * Tolerance in parts per million.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 02 May 2017, 2:58 PM
 */
class PpmTolerance extends Tolerance
{
    /**
     * Construct with the same minus and plus error.
     *
     * @param errorPPM error value in ppm.
     */
    protected PpmTolerance(double errorPPM)
    {
        this(errorPPM, errorPPM);
    }

    /**
     * Constructor.
     *
     * @param minusErrorPPM the minus error in ppm
     * @param plusErrorPPM  the plus error in ppm
     */
    protected PpmTolerance(double minusErrorPPM, double plusErrorPPM)
    {
        super(minusErrorPPM, plusErrorPPM);
    }

    @Override
    public double getError(double expectedMr, double actualMr)
    {
        double delta = actualMr - expectedMr;
        if (actualMr > expectedMr) {
            return (delta / expectedMr) * MILLION;
        } else {
            return -(delta / expectedMr) * MILLION;
        }
    }

    @Override
    public Location check(double expected, double actual)
    {
        double minus = calcError(expected, minusError);
        double plus = calcError(expected, plusError);

        double min = expected - minus;
        double max = expected + plus;

        if (actual < min)
            return Location.SMALLER;
        else if (actual > max)
            return Location.LARGER;
        else
            return Location.WITHIN;
    }

    @Override
    public double getMin(double mz)
    {
        double error = calcError(mz, minusError);
        return mz - error;
    }

    @Override
    public double getMax(double mz)
    {
        double error = calcError(mz, plusError);
        return mz + error;
    }

    @Override
    public boolean isAbsolute()
    {
        return false;
    }

    @Override
    public double getMinusErrorDa(double mass)
    {
        return calcError(mass, minusError);
    }

    @Override
    public double getPlusErrorDa(double mass)
    {
        return calcError(mass, plusError);
    }

    @Override
    public double getErrorWindow(double mass)
    {
        return mass * (minusError + plusError) / MILLION;
    }

    /**
     * calculate the absolute error tolerance
     *
     * @param expectedMass the expected mass
     * @return the absolute error
     */
    private double calcError(double expectedMass, double errorPPM)
    {
        return expectedMass * (errorPPM / MILLION);
    }

    @Override
    public String toString()
    {
        if (minusError == plusError) {
            return minusError + " ppm";
        }

        return "(" + minusError + ", " + plusError + ")" + " ppm";
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PpmTolerance that = (PpmTolerance) o;
        return Double.compare(that.minusError, minusError) == 0 &&
                Double.compare(that.plusError, plusError) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(minusError, plusError);
    }
}
