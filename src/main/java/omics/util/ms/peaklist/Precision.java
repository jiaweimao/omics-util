package omics.util.ms.peaklist;


/**
 * Precision used to store peaks.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 8:24 PM
 */
public enum Precision
{
    /**
     * PeakList that has both m/z and intensities as double
     */
    DOUBLE,
    /**
     * PeakList that has both m/z and intensities as float
     */
    FLOAT,
    /**
     * PeakList that has m/z as double and intensities as float
     */
    DOUBLE_FLOAT,
    /**
     * PeakList that has m/z as double and the intensities as a constant value
     */
    DOUBLE_CONSTANT,
    /**
     * PeakList that has m/z as float and the intensities as a constant value
     */
    FLOAT_CONSTANT
}
