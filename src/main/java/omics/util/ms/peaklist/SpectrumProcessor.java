package omics.util.ms.peaklist;

/**
 * An additional Precursor is added.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Sep 2018, 8:25 AM
 */
public abstract class SpectrumProcessor<IN extends PeakAnnotation, OUT extends PeakAnnotation> implements PeakProcessor<IN, OUT> {

    protected Peak precursor;
    protected PeakSink<OUT> sink;

    /**
     * @param precursor spectrum precursor.
     */
    public void setPrecursor(Peak precursor) {
        this.precursor = precursor;
    }

    public Peak getPrecursor() {
        return precursor;
    }

    @Override
    public void start(int size) {
        sink.start(size);
    }

    @Override
    public void end() {
        sink.end();
    }

    @Override
    public <PS extends PeakSink<OUT>> PS sink(PS sink) {
        this.sink = sink;
        return sink;
    }
}
