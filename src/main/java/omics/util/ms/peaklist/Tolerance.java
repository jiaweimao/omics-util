package omics.util.ms.peaklist;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import omics.util.chem.Weighable;

import java.util.Iterator;

import static java.util.Objects.requireNonNull;

/**
 * Class for specifying a tolerance.
 *
 * @author JiaweiMao
 * @version 1.5.0
 * @since 21 Mar 2017, 1:49 PM
 */
public abstract class Tolerance
{
    protected static final Double MILLION = 1000000.0;

    /**
     * Return Tolerance in Da.
     *
     * @param da error in Da
     * @return an Tolerance with unit in Da.
     */
    public static Tolerance abs(double da)
    {
        return new AbsoluteTolerance(da);
    }

    /**
     * Return Tolerance in Da.
     *
     * @param minus minus error in Da
     * @param plus  plus error in Da
     * @return Tolerance with unit in Da
     */
    public static Tolerance abs(double minus, double plus)
    {
        return new AbsoluteTolerance(minus, plus);
    }

    /**
     * Return tolerance in ppm.
     *
     * @param errorPpm error value in ppm.
     */
    public static Tolerance ppm(double errorPpm)
    {
        return new PpmTolerance(errorPpm);
    }

    /**
     * return error in ppm.
     *
     * @param minusPpm minus error
     * @param plusPpm  plus error
     * @return Tolerance with ppm unit..
     */
    public static Tolerance ppm(double minusPpm, double plusPpm)
    {
        return new PpmTolerance(minusPpm, plusPpm);
    }

    /**
     * Error in ppm.
     *
     * @param expectedMr expected mass
     * @param actualMr   actual mass
     * @return the ppm mass delta between expected and actual Mr in ppm
     */
    public static double getErrorInPpm(double expectedMr, double actualMr)
    {
        double delta = actualMr - expectedMr;
        if (actualMr > expectedMr) {
            return (delta / expectedMr) * MILLION;
        } else {
            return -(delta / expectedMr) * MILLION;
        }
    }

    /**
     * Parse tolerance in formatted string, such as 10 ppm, 0.05 Da, or asymmetric tolerance, "(0.3, 0.5) Da"
     *
     * @param formatStr formatted tolerance string
     * @return {@link Tolerance} of the formatted string.
     */
    public static Tolerance from(String formatStr)
    {
        requireNonNull(formatStr);

        String str = formatStr.trim().toLowerCase();
        boolean isAbs;
        if (str.endsWith("da")) {
            isAbs = true;
            str = str.substring(0, str.length() - 2);
        } else if (str.endsWith("ppm")) {
            isAbs = false;
            str = str.substring(0, str.length() - 3);
        } else {
            throw new IllegalArgumentException(String.format("Unrecognized tolerance %s, only accept 'da' or 'ppm' unit", formatStr));
        }

        if (str.contains(",")) {
            Iterator<String> it = Splitter.on(CharMatcher.anyOf("(,)"))
                    .trimResults()
                    .omitEmptyStrings()
                    .split(str).iterator();

            assert it.hasNext();
            String val1 = it.next();
            assert it.hasNext();
            String val2 = it.next();

            double leftValue = Double.parseDouble(val1);
            double rightValue = Double.parseDouble(val2);

            if (isAbs)
                return abs(leftValue, rightValue);
            else
                return ppm(leftValue, rightValue);
        } else {
            double value = Double.parseDouble(str);
            if (isAbs)
                return abs(value);
            else
                return ppm(value);
        }
    }

    protected final double minusError;
    protected final double plusError;

    /**
     * Constructor tolerance with same minus and plus error.
     *
     * @param error error value
     */
    public Tolerance(double error)
    {
        this(error, error);
    }

    public Tolerance(double minusError, double plusError)
    {
        this.minusError = minusError;
        this.plusError = plusError;
    }

    /**
     * Returns true if the <code>actual</code> is within tolerance of the <code>expected</code>
     *
     * @param expected the expected m/z
     * @param actual   the actual m/z
     * @return true if the <code>actual</code> is within tolerance of the <code>expected</code>
     */
    public boolean inTolerance(double expected, double actual)
    {
        return check(expected, actual) == Location.WITHIN;
    }

    /**
     * Returns true if the <code>actual</code> is within tolerance of the <code>expected</code>
     *
     * @param expected the expected mass
     * @param actual   the actual mass
     * @return true if the <code>actual</code> is within tolerance of the <code>expected</code>
     */
    public boolean inTolerance(Weighable expected, Weighable actual)
    {
        return check(expected.getMolecularMass(), actual.getMolecularMass()) == Location.WITHIN;
    }

    /**
     * Get the error between actual value and expected value.
     * For absolute tolerance, it is absolute delta in unit Da;
     * For ppm tolerance, it is absolute delta in ppm.
     *
     * @param expectedMr expected mr value
     * @param actualMr   actual mr value
     * @return the difference between actual value and expected value in absolute.
     */
    public abstract double getError(double expectedMr, double actualMr);

    /**
     * To check the relation of actual and expected value
     *
     * @param expected the expected value
     * @param actual   the actual value
     * @return Location.SMALLER if actual is smaller than expected, Location.WITHIN if actual is within tolerance with
     * expected, Location.LARGER if actual is LARGER than the expected.
     */
    public abstract Location check(double expected, double actual);

    /**
     * Return true if the <code>actual</code> is larger than <code>expected</code>.
     *
     * @param expected expected value
     * @param actual   actual value
     * @return true if actual value if larger than expected with this tolerance.
     */
    public boolean isLarger(double expected, double actual)
    {
        return check(expected, actual) == Location.LARGER;
    }

    /**
     * @return the minus error with the original unit, for {@link PpmTolerance}, it is
     * error in ppm, for {@link AbsoluteTolerance}, it is error in Da.
     */
    public double getMinusError()
    {
        return minusError;
    }

    /**
     * @return plus error with the original unit.
     */
    public double getPlusError()
    {
        return plusError;
    }

    /**
     * return the absolute minus error of given mass, it is the same with {@link #getMinusError()}for {@link AbsoluteTolerance}, but
     * different for {@link PpmTolerance}.
     * <p>
     * For ppm tolerance, error was convert to da with the reference mass.
     *
     * @param mass mass
     */
    public abstract double getMinusErrorDa(double mass);

    /**
     * return the absolute plus error of given mass, it is the same with {@link #getMinusError()}for {@link AbsoluteTolerance}, but
     * different for {@link PpmTolerance}
     * <p>
     * For ppm tolerance, error was convert to da with the reference mass.
     *
     * @param mass mass
     */
    public abstract double getPlusErrorDa(double mass);

    /**
     * Return the absolute error window of given mass, which is the sum of
     * minus error and plus error.
     *
     * @param mass mass
     * @return error window for this mass.
     */
    public abstract double getErrorWindow(double mass);

    /**
     * Return the minimum value of given mass in tolerance. it is the same with {@link #getMinusError()}for {@link AbsoluteTolerance}, but
     * different for {@link PpmTolerance}
     *
     * @param mass the mass value
     * @return the min value.
     */
    public abstract double getMin(double mass);

    /**
     * Return the maximum value of given mass in tolerance.
     *
     * @param mass the mass value
     * @return the max value.
     */
    public abstract double getMax(double mass);

    /**
     * @return true if the tolerance is evaluate in absolute mode.
     */
    public abstract boolean isAbsolute();

    public enum Location
    {
        SMALLER, WITHIN, LARGER
    }
}
