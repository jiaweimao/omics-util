package omics.util.ms.peaklist.filter;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleConsumer;

import static omics.util.utils.ObjectUtils.checkArgument;


/**
 * Class to bin a spectrum. Peaks will still be stored as a PeakList, but the mz
 * values will be regularly set as the centers of the bins.
 * <p>
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 2:39 PM
 */
public class BinnedSpectrumFilter<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A>
{

    private final double startMz, endMz, binWidth;
    private final IntensityMode intensityMode;

    private final DoubleArrayList intensityBuffer;

    private final HighestIntensityProcedure highestIntensityProcedure = new HighestIntensityProcedure();
    private final SumIntensityProcedure sumIntensityProcedure = new SumIntensityProcedure();
    private final AverageIntensityProcedure averageIntensityProcedure = new AverageIntensityProcedure();

    /**
     * Constructor class for binned spectrum. Binned mz values will be at
     * startMz + binWidth/2 + i*binWidth, i>0 && i < (endMz-startMz)/binWidth
     *
     * @param startMz       start mz of the first bin
     * @param endMz         end mz of the last bin
     * @param binWidth      binWidth
     * @param intensityMode Mode for the calculation of centroid intensity - IntensityMode.HIGHEST : set to highest
     *                      intensity in bin - IntensityMode.SUM : set to summed intensities in bin - IntensityMode.AVG
     *                      : set to average intensity in bin
     */
    public BinnedSpectrumFilter(double startMz, double endMz, double binWidth, IntensityMode intensityMode)
    {
        checkArgument(binWidth > 0);
        checkArgument(endMz > startMz);

        this.startMz = startMz;
        this.endMz = endMz;
        this.binWidth = binWidth;
        this.intensityMode = intensityMode;

        intensityBuffer = new DoubleArrayList();
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList,
            Int2ObjectMap<List<A>> annotationMap)
    {
        int maxIndex = (int) Math.floor((endMz - startMz) / binWidth);

        List<List<A>> annotations = new ArrayList<>(maxIndex);
        int i = 0;
        double halfBinWidth = binWidth / 2.0;
        for (int j = 0; j < maxIndex; j++) {

            double minMz = startMz + j * binWidth;
            double maxMz = startMz + j * binWidth + 2 * halfBinWidth;
            double binnedMz = startMz + halfBinWidth + j * binWidth;

            annotations.add(j, new ArrayList<>());
            intensityBuffer.clear();
            while (i < mzList.size() && mzList.getDouble(i) < maxMz) {
                if (mzList.getDouble(i) >= minMz) {
                    intensityBuffer.add(intensityList.getDouble(i));
                    List<A> peakAnnots = annotations.get(j);
                    if (annotationMap.containsKey(i)) {
                        peakAnnots.addAll(annotationMap.get(i));
                    }
                }
                i++;
            }

            double intensity = getBinIntensity();
            if (intensity > 0) {
                sink.processPeak(binnedMz, intensity, annotations.get(j));
            }
        }
    }

    protected double getBinIntensity()
    {
        ResetableProcedure procedure;
        switch (intensityMode) {
            case HIGHEST:
                procedure = highestIntensityProcedure;
                break;
            case SUM:
                procedure = sumIntensityProcedure;
                break;
            case AVG:
                procedure = averageIntensityProcedure;
                break;
            default:
                throw new IllegalStateException("Cannot calc centroid intensity for " + intensityMode);
        }

        procedure.reset();
        intensityBuffer.forEach(procedure);
        return procedure.getResult();
    }

    private interface ResetableProcedure extends DoubleConsumer
    {
        void reset();

        double getResult();
    }

    /**
     * Calculate the max value of all values passed.
     */
    private static class HighestIntensityProcedure implements ResetableProcedure
    {
        private double max = 0;

        public void reset()
        {
            max = 0;
        }

        @Override
        public double getResult()
        {
            return max;
        }


        @Override
        public void accept(double value)
        {
            max = Math.max(max, value);
        }
    }

    /**
     * Calculate the sum of all values passed.
     */
    private static class SumIntensityProcedure implements ResetableProcedure
    {
        private double sum = 0;

        @Override
        public double getResult()
        {
            return sum;
        }

        public void reset()
        {
            sum = 0;
        }

        @Override
        public void accept(double value)
        {
            sum += value;
        }
    }

    /**
     * Calculate the average value of all values passed.
     */
    private static class AverageIntensityProcedure implements ResetableProcedure
    {
        private int count = 0;
        private double sum = 0;

        @Override
        public void reset()
        {
            count = 0;
            sum = 0;
        }

        @Override
        public double getResult()
        {
            double average = sum;
            if (count > 0) {
                average = sum / count;
            }
            return average;
        }

        @Override
        public void accept(double value)
        {
            sum += value;
            count++;
        }
    }
}
