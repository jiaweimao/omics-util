package omics.util.ms.peaklist.filter;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleComparators;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;

import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;


/**
 * Class to select all peaks that are local maxima and replace their
 * mass/intensity values by the centroid values calculated within a
 * neighborhood. It is assumed that the mass/intensity signal is smooth, i.e.
 * that the signal is concave around the peak. The intensity is set to the value at the apex of the centroid.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Mar 2017, 9:53 PM
 */
public class CentroidFilter<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A>
{
    /**
     * Maximum distance between consecutive mz values in same peak group
     */
    private final double mzMaxDiff;
    private final IntensityMode intensityMode;
    /**
     * Internal parameters used in the algorithm.</br>
     * iMin, iMax : lower and upper bound of sliding window indices</br>
     * iLowBound, iUpBound : start and end indices of centroid mz values
     */
    private int iMin, iMax, iLowBound, iUpBound;

    /**
     * @param mzMaxDiff     Maximum distance between consecutive mz values in same peak group
     * @param intensityMode Mode for the calculation of centroid intensity - IntensityMode.HIGHEST : set to highest
     *                      intensity in peak - IntensityMode.SUM : set to summed intensities in peak -
     *                      IntensityMode.AVG : set to average intensity in peak
     */
    public CentroidFilter(double mzMaxDiff, IntensityMode intensityMode)
    {
        checkArgument(mzMaxDiff > 0);
        requireNonNull(intensityMode);

        this.intensityMode = intensityMode;
        this.mzMaxDiff = mzMaxDiff;
    }

    @Override
    public String toString()
    {
        return "CentroidFilter {" +
                "mzMaxDiff=" + mzMaxDiff +
                ", intensityMode=" + intensityMode +
                '}';
    }

    public static double calcMzSamplingDist(DoubleArrayList mzList)
    {
        double minDiff = 1000000.0;
        double diff;

        mzList.sort(DoubleComparators.NATURAL_COMPARATOR);
        // Calc minimal difference between 2 consecutive mz values
        for (int i = 1; i < mzList.size(); i++) {
            diff = mzList.getDouble(i) - mzList.getDouble(i - 1);
            if (diff < minDiff) {
                minDiff = diff;
            }
        }

        return minDiff;
    }

    /**
     * Finds lower and upper index bound for peaks belonging to centroid at
     * index.
     */
    private void calcBounds(DoubleArrayList mzList, DoubleArrayList intensityList, int index)
    {
        iLowBound = Math.max(index, 0);
        // find lower bound for centroid, i.e. the point where the intensity
        // becomes 0 or starts raising again.
        while (iLowBound > 0 && mzList.getDouble(iLowBound) - mzList.getDouble(iLowBound - 1) < mzMaxDiff) {
            if (intensityList.getDouble(iLowBound - 1) <= 0
                    || intensityList.getDouble(iLowBound) < intensityList.getDouble(iLowBound - 1)) {
                break;
            }
            iLowBound--;
        }
        // find upper bound for centroid, i.e. the point where the intensity
        // becomes 0 or starts raising again.
        int size = mzList.size();
        iUpBound = index;
        while (iUpBound < size - 1 && mzList.getDouble(iUpBound + 1) - mzList.getDouble(iUpBound) < mzMaxDiff) {
            if (intensityList.getDouble(iUpBound + 1) <= 0 || intensityList.getDouble(iUpBound) < intensityList
                    .getDouble(iUpBound + 1)) {
                break;
            }
            iUpBound++;
        }
    }

    /**
     * Checks whether peak at index is local maximum within +/- mzMaxDiff
     */
    private boolean isMaximum(DoubleArrayList mzList, DoubleArrayList intensityList, int index)
    {
        final double mz = mzList.getDouble(index);
        final double h = intensityList.getDouble(index);

        // adjust left border of sliding window
        while (iMin < mzList.size() && mzList.getDouble(iMin) < mz - mzMaxDiff)
            iMin++;
        // adjust right border of sliding window
        while (iMax < mzList.size() && mzList.getDouble(iMax) <= mz + mzMaxDiff)
            iMax++;
        iMax--;

        // pick N highest peak within window
        boolean isMax = true;
        for (int i = iMin; i <= iMax; i++) {
            if (intensityList.getDouble(i) > h) {
                isMax = false;
                break;
            }
        }

        return isMax;
    }

    /**
     * Given the index-bounds of a centroid, the centroid mass and intensity is
     * calculated. The intensity is set to the value at the apex of the
     * centroid.
     */
    protected double calcCentroidMz(DoubleArrayList mzList, DoubleArrayList intensityList,
            PeakIntensityInfo neighborhood)
    {
        double htot = 0.0;
        double m = 0.0;

        for (int j = iLowBound; j <= iUpBound; j++) {
            double h = intensityList.getDouble(j);
            htot += intensityList.getDouble(j);
            m += h * mzList.getDouble(j);
        }

        if (htot > 0) {
            m = m / htot;
        } else {
            m = m / (iUpBound - iLowBound + 1);
        }

        neighborhood.peakCount(iUpBound - iLowBound + 1);
        neighborhood.totalIntensity(htot);

        return m;
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList,
            Int2ObjectMap<List<A>> annotationMap)
    {

        iMin = iMax = 0;

        // collect informations on peaks reduced into centroid
        PeakIntensityInfo neighborhood = new PeakIntensityInfo();

        for (int i = 0; i < mzList.size(); i++) {
            if (isMaximum(mzList, intensityList, i) && intensityList.getDouble(i) > 0.0) {

                calcBounds(mzList, intensityList, i);
                double centroidMz = calcCentroidMz(mzList, intensityList, neighborhood);

                neighborhood.highestIntensity(intensityList.getDouble(i));

                List<A> annotations;

                if (annotationMap.containsKey(i))
                    annotations = annotationMap.get(i);
                else
                    annotations = Collections.emptyList();

                sink.processPeak(centroidMz, getCentroidIntensity(neighborhood), annotations);
            }
        }
    }

    protected double getCentroidIntensity(PeakIntensityInfo peakInfo)
    {
        switch (intensityMode) {
            case HIGHEST:
                return peakInfo.highestIntensity();
            case SUM:
                return peakInfo.totalIntensity();
            case AVG:
                return peakInfo.totalIntensity() / peakInfo.peakCount();
            default:
                throw new IllegalStateException("Cannot calc centroid intensity for " + intensityMode);
        }
    }

    public static class PeakIntensityInfo
    {
        private double highestIntensity;
        private double totalIntensity;
        private int peakCount;

        public double highestIntensity()
        {
            return highestIntensity;
        }

        public void highestIntensity(double highestIntensity)
        {
            this.highestIntensity = highestIntensity;
        }

        public double totalIntensity()
        {
            return totalIntensity;
        }

        public void totalIntensity(double totalIntensity)
        {
            this.totalIntensity = totalIntensity;
        }

        public int peakCount()
        {
            return peakCount;
        }

        public void peakCount(int peakCount)
        {
            this.peakCount = peakCount;
        }
    }
}
