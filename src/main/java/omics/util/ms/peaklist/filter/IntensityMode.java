package omics.util.ms.peaklist.filter;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2013, 9:19 PM
 */
public enum IntensityMode {

    HIGHEST, SUM, AVG
}
