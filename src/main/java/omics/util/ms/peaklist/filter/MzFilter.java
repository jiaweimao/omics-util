package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;
import omics.util.utils.IntervalList;

import java.util.List;


/**
 * Filter out mz values in given tolerance.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 29 Oct 2013, 11:56 PM
 */
public class MzFilter<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {
    /**
     * Object to store intervals
     */
    private final IntervalList intervalList;
    private final Tolerance tolerance;

    public MzFilter(Tolerance tolerance) {
        intervalList = new IntervalList();
        this.tolerance = tolerance;
    }

    public void addMz(double mz) {
        intervalList.addInterval(tolerance.getMin(mz), tolerance.getMax(mz));
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {

        if (!intervalList.contains(mz)) {

            sink.processPeak(mz, intensity, annotations);
        }
    }
}
