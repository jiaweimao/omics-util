package omics.util.ms.peaklist.filter;

import com.google.common.collect.Range;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;
import omics.util.utils.IntervalList;

import java.util.List;


/**
 * Filter peaks in spectrum of given range.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 10:03 PM
 */
public class MzRangeFilter<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A>
{
    /**
     * Object to store intervals
     */
    private final IntervalList intervalList;
    /**
     * flag indicating whether to include or exclude the peaks lying in the intervals
     */
    private final boolean include;

    public MzRangeFilter(double startMz, double endMz, boolean include)
    {
        intervalList = new IntervalList();
        intervalList.addInterval(startMz, endMz);
        this.include = include;
    }

    /**
     * Constructor with given peak list, all values in the list will be removed from the spectrum
     *
     * @param intervalList list of peaks.
     */
    public MzRangeFilter(IntervalList intervalList)
    {
        this(intervalList, false);
    }

    /**
     * @param intervals list of mz intervals
     * @param include   true, if peak in intervals are included, false if they are excluded
     */
    public MzRangeFilter(IntervalList intervals, boolean include)
    {
        this.include = include;
        this.intervalList = intervals;
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations)
    {
        if (intervalList.contains(mz) == include)
            sink.processPeak(mz, intensity, annotations);
    }

    @Override
    public String toString()
    {
        int rangeCount = intervalList.getRangeCount();
        Range<Double>[] intervals = intervalList.getIntervals();
        StringBuilder builder = new StringBuilder();
        builder.append("MzRangeFilter {");
        if (include) {
            builder.append("include: ");
        } else {
            builder.append("exclude: ");
        }

        builder.append(intervals[0].toString());
        if (rangeCount > 1)
            builder.append("...");

        builder.append("}");

        return builder.toString();
    }
}
