package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;

import java.util.List;

/**
 * Extracts the top (most intense) <code>numberOfPeaks</code> peaks that are
 * pushed to this PeakProcessor.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 7:11 PM
 */
public class NPeaksFilter<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {

    private final TopNList<A> topNList;

    public NPeaksFilter(int numberOfPeaks) {
        topNList = new TopNList<>(numberOfPeaks);
    }

    @Override
    public void start(int size) {
        topNList.clear();

        sink.start(topNList.getNumberOfPeaksToRetain());
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        topNList.add(mz, intensity, annotations);
    }

    @Override
    public void end() {
        topNList.resetCursor();
        while (topNList.next()) {

            sink.processPeak(topNList.currMz(), topNList.currIntensity(), topNList.currAnnotations());
        }

        sink.end();
    }

    @Override
    public String toString() {
        return "NPeaksFilter {" + topNList.getNumberOfPeaksToRetain() + '}';
    }
}
