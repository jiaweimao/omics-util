package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;

import java.util.ArrayList;
import java.util.List;


/**
 * Filter that retains the top k peaks per fixed mz range.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Feb 2017, 8:27 PM
 */
public class NPeaksPerBinFilter<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {
    /**
     * the range of the mz window
     */
    private final double range;
    /**
     * number of peaks to retain in every range
     */
    private final int k;
    private final List<TopNList<A>> topNLists = new ArrayList<>();
    /**
     * The max mz of current range
     */
    private double max;
    private TopNList<A> currTopNList;

    /**
     * Construct with number of peaks per range, and the range width.
     *
     * @param k        the number of peaks per mzWindow
     * @param mzWindow the mzWindow
     */
    public NPeaksPerBinFilter(int k, double mzWindow) {
        this.k = k;
        this.range = mzWindow;
    }

    @Override
    public void start(int size) {
        topNLists.clear();
        currTopNList = new TopNList<>(k);

        max = range;

        sink.start(size);
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        if (mz >= max) {

            if (!currTopNList.isEmpty()) {

                topNLists.add(currTopNList);
                currTopNList = new TopNList<>(k);
            }

            while (max < mz)
                max += range;
        }

        currTopNList.add(mz, intensity, annotations);
    }

    @Override
    public void end() {
        if (!currTopNList.isEmpty())
            topNLists.add(currTopNList);

        for (TopNList<A> filter : topNLists) {

            filter.resetCursor();

            while (filter.next()) {

                sink.processPeak(filter.currMz(), filter.currIntensity(), filter.currAnnotations());
            }
        }

        sink.end();
    }

    @Override
    public String toString() {
        return "NPeaksPerBinFilter {" + k + "/" + range + "}";
    }
}
