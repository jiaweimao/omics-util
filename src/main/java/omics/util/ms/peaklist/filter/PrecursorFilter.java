package omics.util.ms.peaklist.filter;


import omics.util.ms.peaklist.PeakAnnotation;

/**
 * remove the precursor related peak from the spectrum.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2018, 10:03 PM
 */
public class PrecursorFilter extends MzRangeFilter<PeakAnnotation>
{
    private static final double PRECURSOR_MASS_WINDOW = 18.0;

    public PrecursorFilter(double startMz, double endMz, boolean include)
    {
        super(startMz, endMz, include);
    }

    public PrecursorFilter(double precursor)
    {
        this(precursor - PRECURSOR_MASS_WINDOW, precursor + PRECURSOR_MASS_WINDOW, false);
    }
}
