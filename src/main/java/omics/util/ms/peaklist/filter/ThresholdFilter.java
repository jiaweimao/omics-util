package omics.util.ms.peaklist.filter;

import com.google.common.collect.EvictingQueue;
import omics.util.math.MathUtils;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;
import omics.util.utils.ArrayUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Peak processor that removes any peaks that have an intensity that is &lt; threshold.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 9:36 PM
 */
public class ThresholdFilter<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {

    // The absolute threshold
    private final double threshold;
    private final Inequality inequality;

    public ThresholdFilter(double threshold) {
        this(threshold, Inequality.GREATER_EQUALS);
    }

    public ThresholdFilter(double threshold, Inequality inequality) {
        this.threshold = threshold;
        this.inequality = inequality;
    }

    /**
     * Calculate the intensity threshold with the rsd value:
     * <ol>
     * <li>Sort the peak intensity in ascending order
     * <li>Calculate the rsd of the first 10 intensities
     * <li>If the rsd > 0.05, the threshold is the average of the 10 intensities
     * <li>If the rsd < 0.05, we add a new intensity and delete the smallest
     * one, and repeat step 2.
     * </ol>
     *
     * @param aList peak list
     * @return intensity threshold
     */
    public static double calcThreshold(PeakList<PeakAnnotation> aList) {
        double[] ins = aList.getYs();
        Arrays.sort(ins);

        double mean = 0.0;

        EvictingQueue<Double> queue = EvictingQueue.create(10);
        for (double in : ins) {
            if (queue.size() == 10) {
                double rsd = MathUtils.rsd(ArrayUtils.toDoubleArray(queue));
                if (rsd > 0.05) {
                    break;
                }
                queue.add(in);
            }

            queue.add(in);
        }

        return mean;
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        boolean match;

        if (inequality == Inequality.GREATER)
            match = intensity > threshold;
        else if (inequality == Inequality.GREATER_EQUALS)
            match = intensity >= threshold;
        else if (inequality == Inequality.LESS) {
            match = intensity < threshold;
        } else if (inequality == Inequality.LESS_EQUALS) {
            match = intensity <= threshold;
        } else
            throw new IllegalStateException("Cannot test for " + inequality);

        if (match)
            sink.processPeak(mz, intensity, annotations);
    }

    @Override
    public String toString() {
        return "ThresholdFilter{" +
                "threshold=" + threshold +
                ", inequality=" + inequality +
                '}';
    }

    public enum Inequality {
        LESS, LESS_EQUALS, GREATER, GREATER_EQUALS
    }
}
