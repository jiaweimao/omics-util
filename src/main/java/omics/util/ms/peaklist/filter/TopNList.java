package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.PeakAnnotation;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;


/**
 * List that retains the <code>size</code> peaks that have the largest intensities.
 * <p>
 * The stored peaks can be accessed using the cursor methods.
 * <p>
 * TopNList list = new TopNList(3);
 * list.resetCursor();
 * while(list.hasNext()) {
 * double mz = list.currMz();
 * double intensity = list.currIntensity();
 * List annotations = list.currAnnotations();
 * }
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Mar 2017, 10:13 PM
 */
public class TopNList<A extends PeakAnnotation> {

    private final int numberOfPeaksToRetain;
    private final Cursor cursor = new Cursor();
    private final Comparator<Entry> valueComparator = (e1, e2) -> Double.compare(e2.getIntensity(), e1.getIntensity());
    private final Comparator<Entry> keyComparator = Comparator.comparingDouble(Entry::getMz);
    protected LinkedList<Entry<A>> list = new LinkedList<>();

    /**
     * Constructs a TopNList that retains the top <code>size</code> peaks.
     *
     * @param numberOfPeaksToRetain the number of peaks to retain
     */
    public TopNList(int numberOfPeaksToRetain) {
        this.numberOfPeaksToRetain = numberOfPeaksToRetain;
    }

    /**
     * Add a new peak
     *
     * @param mz          the peaks m/z
     * @param intensity   the peaks intensity
     * @param annotations the peaks annotations
     */
    public void add(double mz, double intensity, List<A> annotations) {
        if (list.size() < numberOfPeaksToRetain) {
            list.add(new Entry<>(mz, intensity, annotations));

            if (list.size() == numberOfPeaksToRetain)
                list.sort(valueComparator);
        } else {

            Entry last = list.getLast();
            if (last.getIntensity() < intensity) {

                Entry<A> current = list.removeLast();
                current.setValues(mz, intensity, annotations);

                ListIterator<Entry<A>> it = list.listIterator();
                boolean added = false;
                while (it.hasNext()) {
                    Entry entry = it.next();

                    if (entry.getIntensity() < intensity) {

                        it.previous();

                        added = true;
                        it.add(current);
                        break;
                    }
                }
                if (!added)
                    list.add(current);
            }
        }
    }

    /**
     * @return the number of peaks that will be retained
     */
    public int getNumberOfPeaksToRetain() {
        return numberOfPeaksToRetain;
    }

    /**
     * Remove all peaks
     */
    public void clear() {
        list.clear();
    }

    /**
     * @return <tt>true</tt> if this contains no peaks
     */
    public boolean isEmpty() {
        return list.isEmpty();
    }

    /**
     * Reset the cursor
     */
    public void resetCursor() {
        list.sort(keyComparator);

        cursor.reset();
    }

    /**
     * Advance the cursor to the next peak
     *
     * @return true if the cursor was advanced
     */
    public boolean next() {
        return cursor.next();
    }

    /**
     * Return the m/z of the current peak
     *
     * @return the m/z of the current peak
     */
    public double currMz() {
        return cursor.getCurrMz();
    }

    /**
     * Return the intensity of the current peak
     *
     * @return the intensity of the current peak
     */
    public double currIntensity() {
        return cursor.getCurrIntensity();
    }

    /**
     * Return the current annotation
     *
     * @return the current annotation
     */
    public List<A> currAnnotations() {
        return cursor.getCurrAnnotations();
    }

    /**
     * Returns the intensity of the last peak
     *
     * @return the intensity of the last peak
     */
    public double getLastIntensity() {
        return list.getLast().getIntensity();
    }

    private static class Entry<A> {
        private double mz, intensity;
        private List<A> annotations;

        public Entry(double mz, double value, List<A> annotations) {
            setValues(mz, value, annotations);
        }

        double getMz() {
            return mz;
        }

        double getIntensity() {
            return intensity;
        }

        List<A> getAnnotations() {
            return annotations;
        }

        final void setValues(double mz, double intensity, List<A> annotations) {
            this.mz = mz;
            this.intensity = intensity;
            this.annotations = annotations;
        }

        @Override
        public String toString() {
            return MessageFormat.format("m/z = {0}, intensity = {1}", mz, intensity);
        }
    }

    private class Cursor {
        private ListIterator<Entry<A>> iterator;
        private Entry<A> current;

        void reset() {
            current = null;
            iterator = list.listIterator();
        }

        boolean next() {
            if (iterator.hasNext()) {

                current = iterator.next();
                return true;
            }
            return false;
        }

        double getCurrMz() {
            return current.getMz();
        }

        double getCurrIntensity() {
            return current.getIntensity();
        }

        List<A> getCurrAnnotations() {
            return current.getAnnotations();
        }
    }
}
