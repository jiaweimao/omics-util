package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakSink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * abstract implementation of sink which merge peaks with same mz.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2018, 3:25 PM
 */
public abstract class AbstractMergePeakSink<A extends PeakAnnotation, L extends AbstractPeakList<A>>
        implements PeakSink<A> {

    protected final Map<Integer, List<A>> tmpAnnotationMap = new HashMap<>();
    protected final L peakList;
    protected int tmpSize = 0;
    protected double tmpTotalIonCurrent = 0;

    /**
     * Construct with the destinate peak list.
     *
     * @param peakList peaklist stored the final peak list.
     */
    protected AbstractMergePeakSink(L peakList) {
        this.peakList = peakList;
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        if (tmpSize > 0 && mz == getX(tmpSize - 1)) { // if current mz equals to the previous peak

            addY(intensity, tmpSize - 1);

            if (!annotations.isEmpty()) {
                List<A> currentAnnotations = tmpAnnotationMap.get(tmpSize - 1);
                if (currentAnnotations == null)
                    tmpAnnotationMap.put(tmpSize - 1, new ArrayList<>(annotations));
                else
                    currentAnnotations.addAll(annotations);
            }
        } else {

            if (!(tmpSize == 0 || mz >= getX(tmpSize - 1))) {

                throw new UnsortedPeakListException(tmpSize);
            }

            addToArray(mz, intensity, tmpSize);

            if (!annotations.isEmpty()) {

                tmpAnnotationMap.put(tmpSize, new ArrayList<>(annotations));
            }

            tmpSize++;
        }
        tmpTotalIonCurrent += intensity;
    }

    /**
     * add intensity value to peak at given index
     *
     * @param intensity intensity
     * @param index     peak index
     */
    protected abstract void addY(double intensity, int index);

    @Override
    public void end() {
        peakList.size = tmpSize;
        peakList.totalIonCurrent = tmpTotalIonCurrent;
        setArrays();

        peakList.annotationMap.clear();
        for (Integer id : tmpAnnotationMap.keySet()) {
            peakList.annotationMap.putAll(id, tmpAnnotationMap.get(id));
        }
    }

    protected abstract void setArrays();

    /**
     * return mz value at given index.
     *
     * @param index peak index
     * @return mz value
     */
    protected abstract double getX(int index);

    /**
     * add a peak to given index.
     *
     * @param mz        peak mz
     * @param intensity peak intensity
     * @param tmpSize   index
     */
    protected abstract void addToArray(double mz, double intensity, int tmpSize);
}
