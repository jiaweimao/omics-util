package omics.util.ms.peaklist.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import omics.util.ms.peaklist.*;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.*;


/**
 * Abstract class for PeakList.
 *
 * @author JiaweiMao
 * @version 1.0.2
 * @since 06 Oct 2017, 7:06 PM
 */
public abstract class AbstractPeakList<A extends PeakAnnotation> implements PeakList<A>
{
    final ListMultimap<Integer, A> annotationMap = ArrayListMultimap.create();
    /**
     * Total ion current of this peak list.
     */
    protected double totalIonCurrent = 0.0;
    protected int size = 0;
    protected int loadFactor = 100;
    private Peak precursor;

    protected AbstractPeakList()
    {
        precursor = new Peak();
    }

    /**
     * Copy constructor, src peak list information except peaks were copied.
     *
     * @param src source {@link PeakList}
     */
    protected AbstractPeakList(AbstractPeakList<A> src)
    {
        Peak srcPrecursor = src.precursor;
        if (srcPrecursor != null) {
            precursor = srcPrecursor.copy();
        }
        this.totalIonCurrent = 0;
        this.size = 0;
    }

    /**
     * Create a peak list from a string
     *
     * @param str the string
     * @return the new peakList
     */
    public static <A extends PeakAnnotation> PeakList<A> valueOf(String str, Precision precision)
    {
        PeakList<A> peakList = PeakListFactory.newPeakList(precision, 100);
        valueOf(str, peakList);
        return peakList;
    }

    /**
     * Create a peak list from a string
     *
     * @param str the string
     */
    protected static void valueOf(String str, PeakList peakList)
    {
        Pattern pat = Pattern.compile(
                "([+-]?\\d+\\.?\\d*(?:[eE][-+]?\\d+)?)\\s+(?:Da\\s*)?(\\([+-]?\\d+\\.?\\d*(?:[eE][-+]?\\d+)?\\))?");

        Matcher matcher = pat.matcher(str);

        while (matcher.find()) {
            double mz = Double.parseDouble(matcher.group(1));
            double intensity = 0;
            if (matcher.group(2) != null) {
                intensity = Double.parseDouble(matcher.group(2).substring(1, matcher.group(2).length() - 1));
            }

            peakList.add(mz, intensity);
        }
    }

    /**
     * Checks if the given index is in range. If not, throws an appropriate
     * runtime exception. This method does *not* check if the index is negative:
     * It is always used immediately prior to an array access, which throws an
     * ArrayIndexOutOfBoundsException if index is negative.
     *
     * @param index the index
     */
    protected void rangeCheck(int index)
    {
        if (index >= size)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }

    @Override
    public int add(double x, double y)
    {
        grow();

        int index;
        if (size > 0 && x < getX(size - 1)) {
            index = doInsert(x, y);
        } else {
            index = doAppend(x, y);
        }

        return index;
    }

    @Override
    public int add(double x, double y, A annotation)
    {
        requireNonNull(annotation);

        int index = add(x, y);
        addAnnotation(index, annotation);

        return index;
    }

    @Override
    public int add(double x, double y, Collection<? extends A> annotations)
    {
        requireNonNull(annotations);

        int index = add(x, y);
        for (A annotation : annotations)
            addAnnotation(index, annotation);

        return index;
    }

    /**
     * Shift all annotations that have an index that is >= insertionIndex by 1
     *
     * @param insertionIndex the index where the peak insert occurred
     */
    protected void shiftAnnotations(int insertionIndex)
    {
        Integer[] keys = annotationMap.keySet().toArray(new Integer[0]);
        Arrays.sort(keys, Comparator.naturalOrder());

        for (int id = keys.length - 1; id >= 0; id--) {
            Integer key = keys[id];
            if (key >= insertionIndex)
                annotationMap.putAll(key + 1, annotationMap.removeAll(key));
        }
    }

    /**
     * Insert the peak so that the m/z are still sorted
     *
     * @param mz        the peak m/z
     * @param intensity the peak intensity
     * @return the index at which the peak was added
     */
    protected abstract int doInsert(double mz, double intensity);

    /**
     * Append the peak to the end of the peak list
     *
     * @param mz        the peak m/z
     * @param intensity the peak intensity
     */
    protected abstract int doAppend(double mz, double intensity);

    /**
     * Grow the backing if the array is full.
     */
    protected void grow()
    {
        ensureCapacity(((size / loadFactor) + 1) * loadFactor);
    }

    @Override
    public void addSorted(double[] xValues, double[] yValues)
    {
        requireNonNull(xValues);
        requireNonNull(yValues);

        if (xValues.length != yValues.length)
            throw new IllegalStateException(
                    "The mz and intensity arrays need to have the same length, the mz array has a length of "
                            + xValues.length + " and the intensity array a length of " + yValues.length);

        addSorted(xValues, yValues, xValues.length);
    }

    @Override
    public void addSorted(double[] xValues, double[] yValues, int length)
    {
        if (length == 0)
            return;

        checkNotNull(xValues, "mzs cannot be null");
        checkNotNull(yValues, "intensities cannot be null");
        checkPositionIndex(length, xValues.length);
        checkPositionIndex(length, yValues.length);

        PeakCursor<A> arrayCursor = new ArrayCursor<>(xValues, yValues, length);
        PeakCursor<A> listCursor = cursor();

        PeakListMerger<A> merger = new PeakListMerger<>();
        merger.sink(newMergeSink());
        merger.merge(listCursor, arrayCursor);
    }

    @Override
    public void addPeaks(PeakList<A> peakList)
    {
        requireNonNull(peakList);
        if (peakList.isEmpty())
            return;

        PeakListMerger<A> merger = new PeakListMerger<>();
        merger.sink(newMergeSink());
        merger.merge(peakList.cursor(), cursor());
    }

    @Override
    public <T extends PeakAnnotation> void addPeaksNoAnnotations(PeakList<T> peakList)
    {
        addPeaks(peakList, new RemoveAnnotationFunction<>());
    }

    @Override
    public <T extends PeakAnnotation> void addPeaks(PeakList<T> peakList,
                                                    Function<List<T>, List<A>> annotationConverter)
    {
        requireNonNull(peakList);

        PeakListMerger<A> merger = new PeakListMerger<>();
        merger.sink(newMergeSink());
        merger.merge(new WrapperPeakCursor<>(peakList.cursor(), annotationConverter), cursor());
    }

    abstract PeakSink<A> newMergeSink();

    @Override
    public int getClosestIndex(double x)
    {
        if (size == 0) {
            return -1;
        }

        int index = indexOf(0, size, x);

        if (index < 0)
            index = -1 * (index + 1);

        if (index == size) {
            return index - 1;
        } else if (index == 0) {
            return index;
        }

        double ds = x - getX(index - 1);
        double dl = getX(index) - x;

        return ds <= dl ? index - 1 : index;
    }

    @Override
    public int indexOf(double xKey)
    {
        return indexOf(0, size, xKey);
    }

    @Override
    public int[] indexOf(double minX, double maxX)
    {
        IntList indexes = new IntArrayList();
        int startId = indexOf(minX);
        if (startId < 0) {
            startId = -startId - 1;
        }
        for (int i = startId; i < size; i++) {
            if (getX(i) > maxX)
                break;
            indexes.add(i);
        }
        return indexes.toIntArray();
    }

    /**
     * {@inheritDoc}
     */
    public int size()
    {
        return size;
    }

    @Override
    public boolean isEmpty()
    {
        return size == 0;
    }

    /**
     * Clears the peak list, the size is set to 0 and the backing arrays are set
     * to an clear array
     */
    @Override
    public void clear()
    {
        size = 0;
        totalIonCurrent = 0;
        trimToSize();

        annotationMap.clear();
    }

    /**
     * Returns the current load factor, which is the number of elements that are
     * added to the mz and intensity arrays when they are full
     *
     * @return the current load factor
     */
    public int getLoadFactor()
    {
        return loadFactor;
    }

    /**
     * Set the number of elements that are added to the mz and intensity arrays
     * when the size of this instance exceeds the capacity of these arrays
     *
     * @param loadFactor the new load factor
     */
    public void setLoadFactor(int loadFactor)
    {
        this.loadFactor = loadFactor;
    }

    /**
     * Returns the sum of all intensities
     *
     * @return the sum of all intensities
     */
    @Override
    public double getTotalIonCurrent()
    {
        return totalIonCurrent;
    }

    @Override
    public boolean hasAnnotations()
    {
        return !annotationMap.isEmpty();
    }

    @Override
    public int[] getAnnotationIndexes()
    {
        int[] indexes = new int[annotationMap.keySet().size()];

        int c = 0;
        for (int index : annotationMap.keySet()) {
            indexes[c++] = index;
        }
        Arrays.sort(indexes);

        return indexes;
    }

    @Override
    public boolean hasAnnotationsAt(int index)
    {
        return annotationMap.containsKey(index);
    }

    @Override
    public List<A> getAnnotations(int index)
    {
        if (annotationMap.containsKey(index)) {
            return Collections.unmodifiableList(annotationMap.get(index));
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public A getFirstAnnotation(int index)
    {
        List<A> annotationList = annotationMap.get(index);
        if (annotationList.isEmpty())
            return null;

        return annotationList.get(0);
    }

    @Override
    public void addAnnotation(int index, A annotation)
    {
        requireNonNull(annotation);
        checkElementIndex(index, size());

        annotationMap.put(index, annotation);
    }

    @Override
    public void addAnnotations(int index, Collection<A> annotations)
    {
        checkElementIndex(index, size());

        annotationMap.putAll(index, annotations);
    }

    @Override
    public boolean removeAnnotation(A annotation, int index)
    {
        List<A> annotations = annotationMap.get(index);
        if (annotations.isEmpty())
            annotationMap.removeAll(index);

        return annotations.remove(annotation);
    }

    @Override
    public void clearAnnotationsAt(int index)
    {
        annotationMap.removeAll(index);
    }

    @Override
    public void clearAnnotations()
    {
        annotationMap.clear();
    }

    @Override
    public void sortAnnotations(Comparator<A> comparator)
    {
        for (Integer key : annotationMap.keySet()) {
            List<A> as = annotationMap.get(key);
            as.sort(comparator);
        }
    }

    @Override
    public Peak getPrecursor()
    {
        return precursor;
    }

    @Override
    public void setPrecursor(Peak precursor)
    {
        requireNonNull(precursor);

        this.precursor = precursor;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (!(o instanceof AbstractPeakList))
            return false;

        AbstractPeakList that = (AbstractPeakList) o;

        return size == that.size && Double.compare(that.totalIonCurrent, totalIonCurrent) == 0
                && annotationMap.equals(that.annotationMap)
                && !(precursor != null ? !precursor.equals(that.precursor) : that.precursor != null);
    }

    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = totalIonCurrent != +0.0d ? Double.doubleToLongBits(totalIonCurrent) : 0L;
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + size;
        result = 31 * result + annotationMap.hashCode();
        result = 31 * result + (precursor != null ? precursor.hashCode() : 0);
        return result;
    }

    @Override
    public void apply(PeakProcessor<A, A> peakProcessor)
    {
        requireNonNull(peakProcessor);

        apply(this, this, peakProcessor);
    }

    @Override
    public void apply(SpectrumProcessor<A, A> spectrumProcessor)
    {
        requireNonNull(spectrumProcessor);

        apply(this, this, spectrumProcessor);
    }


    /**
     * apply a {@link SpectrumProcessor} to the src peak list, push peaks to dest peak list.
     *
     * @param src               source {@link PeakList}
     * @param dest              destination {@link PeakList}
     * @param spectrumProcessor a {@link SpectrumProcessor}
     */
    protected void apply(PeakList<A> src, PeakList<A> dest, SpectrumProcessor<A, A> spectrumProcessor)
    {
        requireNonNull(spectrumProcessor);

        PeakCollectorSink<A> sink = new PeakCollectorSink<>(dest);
        sink.start(src.size());

        spectrumProcessor.sink(sink);
        spectrumProcessor.setPrecursor(src.getPrecursor());

        int peakCount = src.size();
        spectrumProcessor.start(peakCount);
        for (int i = 0; i < peakCount; i++) {
            spectrumProcessor.processPeak(src.getX(i), src.getY(i), src.getAnnotations(i));
        }
        spectrumProcessor.end();
        dest.trimToSize();
    }

    /**
     * when peak processor is processing this peak list, the result is stored in a temp list,
     * when the end() is called, the dest peak list is cleared, the the result is pulled into the dest peak list.
     *
     * @param src           the source peak list
     * @param dest          the destination peak list
     * @param peakProcessor peak processor applied
     */
    protected void apply(PeakList<A> src, PeakList<A> dest, PeakProcessor<A, A> peakProcessor)
    {
        requireNonNull(peakProcessor);

        PeakCollectorSink<A> peakSink = new PeakCollectorSink<>(dest);
        peakSink.start(src.size());

        peakProcessor.sink(peakSink);

        int peakCount = src.size();
        peakProcessor.start(peakCount);
        for (int i = 0; i < peakCount; i++) {
            peakProcessor.processPeak(src.getX(i), src.getY(i), src.getAnnotations(i));
        }
        peakProcessor.end();
        dest.trimToSize();
    }

    @Override
    public void apply(PeakProcessorChain<A> peakProcessorChain)
    {
        requireNonNull(peakProcessorChain);

        apply(this, this, peakProcessorChain);
    }

    protected void apply(PeakList<A> org, PeakList<A> dest, PeakProcessorChain<A> peakProcessorChain)
    {
        requireNonNull(peakProcessorChain);
        if (peakProcessorChain.isEmpty())
            return;

        PeakCollectorSink<A> peakSink = new PeakCollectorSink<>(dest);

        peakProcessorChain.process(org, peakSink);
        trimToSize();
    }

    @Override
    public double getBasePeakX()
    {
        return getX(getMostIntenseIndex());
    }

    @Override
    public double getBasePeakY()
    {
        return getY(getMostIntenseIndex());
    }

    /**
     * Finds the index that has of the peak that has an mz which is equal to or
     * larger than mz
     *
     * @param mz           the mz to test
     * @param defaultIndex the index to return if there is no peak in this peak lis that has a mz that is >= mz
     * @return the index that has an mz that is equal to or larger than mz or defaultIndex if no such index exists
     */
    protected int indexEqualOrLarger(double mz, int defaultIndex, double[] mzList)
    {
        int index = getClosestIndex(mz);

        if (index == -1) {

            return defaultIndex;
        } else if (mzList[index] < mz) {

            return index + 1;
        } else {

            return index;
        }
    }

    /**
     * Finds the index of the peak that has an mz which is equal to or larger than mz
     *
     * @param mz           the mz to test
     * @param defaultIndex the index to return if there is no peak in this peak lis that has a mz that is >= mz
     * @return the index that has an mz that is equal to or larger than mz or defaultIndex if no such index exists
     */
    protected int indexEqualOrLarger(float mz, int defaultIndex, float[] mzList)
    {
        int index = getClosestIndex(mz);

        if (index == -1) {

            return defaultIndex;
        } else if (mzList[index] < mz) {

            return index + 1;
        } else {

            return index;
        }
    }

    @Override
    public String toString()
    {
        return getClass().getSimpleName() + "{" + "precursor=" + precursor + ", size=" + size + '}';
    }

    public PeakCursor<A> cursor()
    {
        return new PeakListCursor<>(this);
    }

    static class RemoveAnnotationFunction<IN extends PeakAnnotation, OUT extends PeakAnnotation>
            implements Function<List<IN>, List<OUT>>
    {
        private final List<OUT> emptyList = Collections.emptyList();

        @Override
        public List<OUT> apply(List<IN> input)
        {
            return emptyList;
        }
    }
}
