package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakProcessor;
import omics.util.ms.peaklist.PeakSink;

/**
 * Implementation of PeakProcessor that takes care of the reference to the next
 * element in the peak processor chain
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Mar 2017, 9:34 PM
 */
public abstract class AbstractPeakProcessor<IN extends PeakAnnotation, OUT extends PeakAnnotation>
        implements PeakProcessor<IN, OUT>
{
    /**
     * The next element in the peak processor chain
     */
    protected PeakSink<OUT> sink;

    @Override
    public <PS extends PeakSink<OUT>> PS sink(PS sink)
    {
        this.sink = sink;
        return sink;
    }

    @Override
    public void start(int size)
    {
        sink.start(size);
    }

    @Override
    public void end()
    {
        sink.end();
    }
}
