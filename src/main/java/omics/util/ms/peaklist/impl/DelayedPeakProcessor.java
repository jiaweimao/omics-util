package omics.util.ms.peaklist.impl;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import omics.util.ms.peaklist.PeakAnnotation;

import java.util.List;

/**
 * A peak processor that caches all the peaks that are pushed to it by the
 * processPeak method.
 * <p/>
 * This can be used by subclasses that need all the peaks before the processing
 * can begin, for example see the NthPeakNormalizer.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 03 Apr 2017, 3:15 PM
 */
public abstract class DelayedPeakProcessor<IN extends PeakAnnotation, OUT extends PeakAnnotation>
        extends AbstractPeakProcessor<IN, OUT>
{
    // The cached m/z's
    private final DoubleArrayList mzList = new DoubleArrayList();
    // The cached intensities
    private final DoubleArrayList intensityList = new DoubleArrayList();
    // The cached annotations
    private final Int2ObjectMap<List<IN>> annotationMap = new Int2ObjectOpenHashMap<>();

    @Override
    public void processPeak(double mz, double intensity, List<IN> annotations)
    {
        mzList.add(mz);
        intensityList.add(intensity);
        if (!annotations.isEmpty()) {
            annotationMap.put(mzList.size() - 1, annotations);
        }
    }

    @Override
    public void start(int size)
    {
        mzList.clear();
        intensityList.clear();

        mzList.ensureCapacity(size);
        intensityList.ensureCapacity(size);
        annotationMap.clear();

        sink.start(size);
    }

    @Override
    public void end()
    {
        if (!mzList.isEmpty())
            processCached(mzList, intensityList, annotationMap);

        sink.end();
    }

    /**
     * This method is called when all the peaks have been cached
     *
     * @param mzList        the list of all peak m/z
     * @param intensityList the list of all peak intensities
     * @param annotationMap peak annotations mapped by the peak index
     */
    protected abstract void processCached(DoubleArrayList mzList, DoubleArrayList intensityList,
            Int2ObjectMap<List<IN>> annotationMap);
}
