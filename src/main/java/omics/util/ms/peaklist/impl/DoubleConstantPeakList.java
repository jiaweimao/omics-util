package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.*;
import omics.util.utils.ArrayUtils;

import java.util.Arrays;

/**
 * PeakList that stores the m/z values as doubles and the intensities as a
 * constant double value.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Oct 2017, 7:06 PM
 */
public class DoubleConstantPeakList<A extends PeakAnnotation> extends AbstractPeakList<A>
{
    private final double intensity;
    private double[] mzList;

    /**
     * Create an clear peak list
     *
     * @param intensity the intensity of all peaks in this PeakList
     */
    public DoubleConstantPeakList(double intensity)
    {
        this(intensity, 0);
    }

    /**
     * Create a peak list that has a capacity set to initialCapacity
     *
     * @param intensity       the intensity of all peaks in this PeakList
     * @param initialCapacity the initial capacity of the peak list
     */
    public DoubleConstantPeakList(double intensity, int initialCapacity)
    {
        this.intensity = intensity;
        mzList = new double[initialCapacity];
    }

    private DoubleConstantPeakList(DoubleConstantPeakList<A> src)
    {
        super(src);

        this.intensity = src.intensity;
        mzList = new double[size];
    }

    protected DoubleConstantPeakList(DoubleConstantPeakList<A> src, PeakProcessor<A, A> peakProcessor)
    {
        this(src);

        apply(src, this, peakProcessor);

    }

    protected DoubleConstantPeakList(DoubleConstantPeakList<A> src, PeakProcessorChain<A> peakProcessorChain)
    {
        this(src);

        apply(src, this, peakProcessorChain);
    }

    protected DoubleConstantPeakList(DoubleConstantPeakList<A> src, SpectrumProcessor<A, A> spectrumProcessor)
    {
        this(src);

        apply(src, this, spectrumProcessor);
    }

    public double getIntensity()
    {
        return intensity;
    }

    @Override
    public double getTotalIonCurrent()
    {
        return intensity * size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getX(int index)
    {
        rangeCheck(index);

        return mzList[index];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getY(int index)
    {
        rangeCheck(index);

        return intensity;
    }

    /**
     * Add a mz to this peak list. The intensity value is ignored
     *
     * @param mz            the mz to add
     * @param voidIntensity the intensity to add
     */
    @Override
    public int doAppend(double mz, double voidIntensity)
    {
        if (size > 0 && mz == mzList[size - 1]) {

            return size - 1;
        }
        if (size > 0 && mz < mzList[size - 1]) {

            throw new IllegalStateException("the added peak is not sorted.  Adding " + mz + " to " + mzList[size - 1]);
        }

        mzList[size] = mz;
        totalIonCurrent += intensity;

        size++;
        return size - 1;
    }

    @Override
    protected int doInsert(double mz, double intensity)
    {
        int indexOf = indexOf(0, size, mz);
        if (indexOf < 0) {

            indexOf = -1 * (indexOf + 1);

            ArrayUtils.insert(mz, indexOf, mzList, size);
            size++;
            shiftAnnotations(indexOf);
        }

        totalIonCurrent += this.intensity;
        return indexOf;
    }

    @Override
    protected PeakSink<A> newMergeSink()
    {
        return new MergePeakSink<>(this);
    }

    /**
     * Add a mz to this peak list.
     *
     * @param mz the mz to add
     */
    public void add(double mz)
    {
        add(mz, 0);
    }

    /**
     * Trims the capacity of this <tt>DoublePeakList</tt> instance to be the
     * list's current size. An application can use this operation to minimize
     * the storage of a <tt>DoublePeakList</tt> instance.
     */
    @Override
    public void trimToSize()
    {
        mzList = ArrayUtils.trim(mzList, size);
    }

    @Override
    public int indexOf(int fromIndex, int toIndex, double xKey)
    {
        if (size == 0) {

            return -1;
        }

        return Arrays.binarySearch(mzList, Math.max(0, fromIndex), Math.min(size, toIndex), xKey);
    }

    /**
     * Returns the index of the most intense peak in the range minMz to maxMz.
     * If there are no peaks in this range -1 returned.
     *
     * @param minX the minMz
     * @param maxX the maxMz
     * @return the index of the most intense peak in the range minMz to maxMz. If there are no peaks in this range -1
     * returned
     */
    @Override
    public int getMostIntenseIndex(double minX, double maxX)
    {
        if (minX > maxX)
            throw new IllegalArgumentException(
                    "Min mz needs to be smaller than max mz.  minMz = " + minX + " maxMz " + maxX);

        final int index = indexEqualOrLarger(minX, -1, mzList);
        return index < size ? index : -1;
    }

    @Override
    public int getMostIntenseIndex()
    {
        return size == 0 ? -1 : 0;
    }


    @Override
    public double[] getXs()
    {
        return getXs(null);
    }

    /**
     * Increases the capacity of this <tt>DoublePeakList</tt> instance, if
     * necessary, to ensure that it can hold at least the number of peaks
     * specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    @Override
    public void ensureCapacity(int minCapacity)
    {
        if (mzList.length < minCapacity) {

            int remaining = minCapacity - mzList.length;

            mzList = ArrayUtils.grow(mzList, remaining);
        }
    }

    @Override
    public Precision getPrecision()
    {
        return Precision.DOUBLE_CONSTANT;
    }

    @Override
    public DoubleConstantPeakList<A> copy(PeakProcessor<A, A> peakProcessor)
    {
        return new DoubleConstantPeakList<>(this, peakProcessor);
    }

    @Override
    public DoubleConstantPeakList<A> copy(PeakProcessorChain<A> peakProcessorChain)
    {
        return new DoubleConstantPeakList<>(this, peakProcessorChain);
    }

    @Override
    public PeakList<A> copy(SpectrumProcessor<A, A> spectrumProcessor)
    {
        return new DoubleConstantPeakList<>(this, spectrumProcessor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getXs(double[] dest)
    {
        return ArrayUtils.copyArray(mzList, 0, dest, 0, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getXs(double[] dest, int destPos)
    {
        return ArrayUtils.copyArray(mzList, 0, dest, destPos, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getXs(int srcPos, double[] dest, int destPos, int length)
    {
        return ArrayUtils.copyArray(mzList, srcPos, dest, destPos, length);
    }

    @Override
    public double[] getYs()
    {
        return getYs(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getYs(double[] dest)
    {
        return getYs(0, dest, 0, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getYs(double[] dest, int destPos)
    {
        return getYs(0, dest, destPos, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getYs(int srcPos, double[] dest, int destPos, int length)
    {
        if (dest == null)
            dest = new double[destPos + length];

        Arrays.fill(dest, destPos, destPos + length, intensity);

        return dest;
    }

    /**
     * Replace the intensity at index with intensity
     *
     * @param y     the new intensity
     * @param index the index for which the intensity is to be set
     * @throws IndexOutOfBoundsException if the index is out of range ( <tt>index &lt; 0 || index &gt;= size()</tt>)
     */
    @Override
    public void setYAt(double y, int index)
    {
        throw new UnsupportedOperationException("DoubleConstantPeakList has a constant intensity which is immutable");
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        @SuppressWarnings("rawtypes")
        DoubleConstantPeakList that = (DoubleConstantPeakList) o;

        return Double.compare(that.intensity, intensity) == 0
                && ArrayUtils.arrayEquals(mzList, that.mzList, size);
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        long temp;

        result = 31 * result + ArrayUtils.arrayHashCode(mzList, size);
        temp = intensity != +0.0d ? Double.doubleToLongBits(intensity) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /**
     * PeakSink that is used to merge two peak lists
     */
    private static class MergePeakSink<A extends PeakAnnotation>
            extends AbstractMergePeakSink<A, DoubleConstantPeakList<A>>
    {

        private double[] tmpMzList;

        private MergePeakSink(DoubleConstantPeakList<A> peakList)
        {
            super(peakList);
        }

        @Override
        protected void addY(double intensity, int index)
        {
            // Constant peak lists do not have any intensity
        }

        @Override
        public void start(int size)
        {
            tmpMzList = new double[size];
        }

        protected void addToArray(double mz, double intensity, int index)
        {
            tmpMzList[index] = mz;
        }

        @Override
        protected double getX(int index)
        {
            return tmpMzList[index];
        }

        protected void setArrays()
        {
            peakList.mzList = tmpMzList;
            peakList.totalIonCurrent = peakList.intensity * peakList.size();
        }
    }
}
