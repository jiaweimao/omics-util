package omics.util.ms.peaklist.impl;

import omics.util.utils.ArrayUtils;
import omics.util.ms.peaklist.*;

import java.util.Arrays;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Oct 2017, 7:07 PM
 */
public class DoubleFloatPeakList<A extends PeakAnnotation> extends AbstractPeakList<A> {

    private double[] mzList;
    private float[] intensityList;

    /**
     * Create an clear peak list
     */
    public DoubleFloatPeakList() {
        this(0);
    }

    /**
     * Create a peak list that has a capacity set to initialCapacity
     *
     * @param initialCapacity the initial capacity of the peak list
     */
    public DoubleFloatPeakList(int initialCapacity) {
        mzList = new double[initialCapacity];
        intensityList = new float[initialCapacity];
    }

    private DoubleFloatPeakList(DoubleFloatPeakList<A> src) {
        super(src);

        mzList = new double[src.size];
        intensityList = new float[src.size];
    }

    public DoubleFloatPeakList(DoubleFloatPeakList<A> src, PeakProcessor<A, A> peakProcessor) {
        this(src);

        apply(src, this, peakProcessor);
    }

    public DoubleFloatPeakList(DoubleFloatPeakList<A> src, PeakProcessorChain<A> peakProcessorChain) {
        this(src);

        apply(src, this, peakProcessorChain);
    }

    public DoubleFloatPeakList(DoubleFloatPeakList<A> src, SpectrumProcessor<A, A> spectrumProcessor) {
        this(src);

        apply(src, this, spectrumProcessor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getX(int index) {
        rangeCheck(index);

        return mzList[index];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getY(int index) {
        rangeCheck(index);

        return intensityList[index];
    }

    /**
     * Add a mz, intensity pair to the peak list
     *
     * @param mz        the mz to add
     * @param intensity the intensity to add
     */
    @Override
    public int doAppend(double mz, double intensity) {
        if (size > 0 && mz == mzList[size - 1]) {

            intensityList[size - 1] = intensityList[size - 1] + (float) intensity;
        } else if (size > 0 && mz < mzList[size - 1]) {

            throw new IllegalStateException("the added peak is not sorted.  Adding " + mz + " to " + mzList[size - 1]);
        } else {

            grow();
            mzList[size] = mz;
            intensityList[size] = (float) intensity;
            size++;
        }

        totalIonCurrent += (float) intensity;
        return size - 1;
    }

    @Override
    protected int doInsert(double mz, double intensity) {
        int indexOf = indexOf(0, size, mz);
        if (indexOf >= 0) {

            intensityList[indexOf] += intensity;
        } else {

            indexOf = -1 * (indexOf + 1);

            ArrayUtils.insert(mz, indexOf, mzList, size);

            ArrayUtils.insert((float) intensity, indexOf, intensityList, size);
            size++;
            shiftAnnotations(indexOf);
        }

        totalIonCurrent += intensity;
        return indexOf;
    }

    @Override
    protected PeakSink<A> newMergeSink() {
        return new MergePeakSink<>(this);
    }

    /**
     * Trims the capacity of this <tt>DoublePeakList</tt> instance to be the
     * list's current size. An application can use this operation to minimize
     * the storage of a <tt>DoublePeakList</tt> instance.
     */
    @Override
    public void trimToSize() {
        mzList = ArrayUtils.trim(mzList, size);

        intensityList = ArrayUtils.trim(intensityList, size);
    }

    @Override
    public int indexOf(int fromIndex, int toIndex, double xKey) {
        if (size == 0) {

            return -1;
        }

        return Arrays.binarySearch(mzList, Math.max(0, fromIndex), Math.min(size, toIndex), xKey);
    }

    /**
     * Returns the index of the most intense peak in the range minMz to maxMz.
     * If there are no peaks in this range -1 returned.
     *
     * @param minX the minMz
     * @param maxX the maxMz
     * @return the index of the most intense peak in the range minMz to maxMz. If there are no peaks in this range -1
     * returned
     */
    @Override
    public int getMostIntenseIndex(double minX, double maxX) {
        if (minX > maxX)
            throw new IllegalArgumentException(
                    "Min mz needs to be smaller than max mz.  minMz = " + minX + " maxMz " + maxX);

        if (isEmpty())
            return -1;

        return findMostIntenseIndex(indexEqualOrLarger(minX, 0, mzList), maxX);
    }

    @Override
    public int getMostIntenseIndex() {
        return findMostIntenseIndex(0, Double.MAX_VALUE);
    }

    @Override
    public double[] getXs() {
        return getXs(null);
    }

    private int findMostIntenseIndex(int startIndex, double maxMz) {
        double maxIntensity = 0;
        int maxIndex = -1;
        for (int i = startIndex; i < size && mzList[i] <= maxMz; i++) {

            double intensity = intensityList[i];
            if (intensity > maxIntensity) {

                maxIntensity = intensity;
                maxIndex = i;
            }
        }

        return maxIndex;
    }

    /**
     * Increases the capacity of this <tt>DoublePeakList</tt> instance, if
     * necessary, to ensure that it can hold at least the number of peaks
     * specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    @Override
    public void ensureCapacity(int minCapacity) {
        if (mzList.length < minCapacity) {

            int remaining = minCapacity - mzList.length;

            mzList = ArrayUtils.grow(mzList, remaining);

            intensityList = ArrayUtils.grow(intensityList, remaining);
        }
    }

    @Override
    public Precision getPrecision() {
        return Precision.DOUBLE_FLOAT;
    }

    @Override
    public DoubleFloatPeakList<A> copy(PeakProcessor<A, A> peakProcessor) {
        return new DoubleFloatPeakList<>(this, peakProcessor);
    }

    @Override
    public DoubleFloatPeakList<A> copy(PeakProcessorChain<A> peakProcessorChain) {
        return new DoubleFloatPeakList<>(this, peakProcessorChain);
    }

    @Override
    public PeakList<A> copy(SpectrumProcessor<A, A> spectrumProcessor) {
        return new DoubleFloatPeakList<>(this, spectrumProcessor);
    }

    /**
     * {@inheritDoc}
     */
    public double[] getXs(double[] dest) {
        return ArrayUtils.copyArray(mzList, 0, dest, 0, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getXs(double[] dest, int destPos) {
        return ArrayUtils.copyArray(mzList, 0, dest, destPos, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getXs(int srcPos, double[] dest, int destPos, int length) {
        return ArrayUtils.copyArray(mzList, srcPos, dest, destPos, length);
    }

    @Override
    public double[] getYs() {
        return getYs(null);
    }

    /**
     * {@inheritDoc}
     */
    public double[] getYs(double[] dest) {
        return getYs(0, dest, 0, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getYs(double[] dest, int destPos) {
        return getYs(0, dest, destPos, size);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double[] getYs(int srcPos, double[] dest, int destPos, int length) {
        return ArrayUtils.copyArray(intensityList, srcPos, dest, destPos, length);
    }

    /**
     * Replace the intensity at index with intensity
     *
     * @param y     the new intensity
     * @param index the index for which the intensity is to be set
     * @throws IndexOutOfBoundsException if the index is out of range ( <tt>index &lt; 0 || index &gt;= size()</tt>)
     */
    @Override
    public void setYAt(double y, int index) {
        rangeCheck(index);

        double origIntensity = intensityList[index];

        intensityList[index] = (float) y;

        totalIonCurrent = totalIonCurrent - origIntensity + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        @SuppressWarnings("rawtypes")
        DoubleFloatPeakList peakList = (DoubleFloatPeakList) o;

        return size == peakList.size && Double.compare(peakList.totalIonCurrent, totalIonCurrent) == 0
                && ArrayUtils.arrayEquals(intensityList, peakList.intensityList, size)
                && ArrayUtils.arrayEquals(mzList, peakList.mzList, size);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();

        result = 31 * result + ArrayUtils.arrayHashCode(mzList, size);
        result = 31 * result + ArrayUtils.arrayHashCode(intensityList, size);
        return result;
    }

    /**
     * PeakSink that is used to merge two peak lists
     */
    private static class MergePeakSink<A extends PeakAnnotation> extends AbstractMergePeakSink<A, DoubleFloatPeakList<A>> {
        private double[] tmpMzList;
        private float[] tmpIntensityList;

        private MergePeakSink(DoubleFloatPeakList<A> peakList) {
            super(peakList);
        }

        @Override
        protected void addY(double intensity, int index) {
            tmpIntensityList[index] = tmpIntensityList[index] + (float) intensity;
        }

        @Override
        public void start(int size) {
            tmpMzList = new double[size];
            tmpIntensityList = new float[size];
        }

        protected void addToArray(double mz, double intensity, int index) {
            tmpMzList[index] = mz;
            tmpIntensityList[index] = (float) intensity;
        }

        @Override
        protected double getX(int index) {
            return tmpMzList[index];
        }

        protected void setArrays() {
            peakList.mzList = tmpMzList;
            peakList.intensityList = tmpIntensityList;
        }
    }
}
