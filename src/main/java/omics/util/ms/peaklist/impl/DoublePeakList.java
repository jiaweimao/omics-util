package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.*;
import omics.util.utils.ArrayUtils;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/**
 * {@link PeakList} with double mz and intensity values.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Oct 2017, 7:11 PM
 */
public class DoublePeakList<A extends PeakAnnotation> extends AbstractPeakList<A>
{
    private double[] xList;
    private double[] yList;

    /**
     * Create an clear peak list
     */
    public DoublePeakList()
    {
        this(0);
    }

    /**
     * Create a peak list that has a capacity set to initialCapacity
     *
     * @param initialCapacity the initial capacity of the peak list
     */
    public DoublePeakList(int initialCapacity)
    {
        xList = new double[initialCapacity];
        yList = new double[initialCapacity];
    }

    private DoublePeakList(DoublePeakList<A> src)
    {
        super(src);

        size = 0;
        xList = new double[src.size];
        yList = new double[src.size];
    }

    public DoublePeakList(DoublePeakList<A> src, PeakProcessor<A, A> peakProcessor)
    {
        this(src);

        apply(src, this, peakProcessor);
    }

    public DoublePeakList(DoublePeakList<A> src, PeakProcessorChain<A> peakProcessorChain)
    {
        this(src);

        apply(src, this, peakProcessorChain);
    }

    public DoublePeakList(DoublePeakList<A> src, SpectrumProcessor<A, A> spectrumProcessor)
    {
        this(src);

        apply(src, this, spectrumProcessor);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public double getX(int index)
    {
        rangeCheck(index);

        return xList[index];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getY(int index)
    {
        rangeCheck(index);

        return yList[index];
    }

    /**
     * Add a mz, intensity pair to the peak list
     *
     * @param mz        the mz to add
     * @param intensity the intensity to add
     */
    @Override
    public int doAppend(double mz, double intensity)
    {
        if (size > 0 && mz == xList[size - 1]) {

            yList[size - 1] = yList[size - 1] + intensity;
        } else if (size > 0 && mz < xList[size - 1]) {

            throw new IllegalStateException("the added peak is not sorted.  Adding " + mz + " to " + xList[size - 1]);
        } else {

            grow();
            xList[size] = mz;
            yList[size] = intensity;
            size++;
        }

        totalIonCurrent += intensity;
        return size - 1;
    }

    @Override
    protected int doInsert(double mz, double intensity)
    {
        int indexOf = indexOf(0, size, mz);
        if (indexOf >= 0) {
            yList[indexOf] += intensity;
        } else {

            indexOf = -1 * (indexOf + 1);

            ArrayUtils.insert(mz, indexOf, xList, size);
            ArrayUtils.insert(intensity, indexOf, yList, size);
            size++;
            shiftAnnotations(indexOf);
        }

        totalIonCurrent += intensity;
        return indexOf;
    }

    @Override
    PeakSink<A> newMergeSink()
    {
        return new MergePeakSink<>(this);
    }

    /**
     * Trims the capacity of this <tt>DoublePeakList</tt> instance to be the
     * list's current size. An application can use this operation to minimize
     * the storage of a <tt>DoublePeakList</tt> instance.
     */
    public void trimToSize()
    {
        xList = ArrayUtils.trim(xList, size);
        yList = ArrayUtils.trim(yList, size);
    }

    @Override
    public int indexOf(int fromIndex, int toIndex, double xKey)
    {
        if (size == 0) {
            return -1;
        }

        return Arrays.binarySearch(xList, Math.max(0, fromIndex), Math.min(size, toIndex), xKey);
    }

    /**
     * Returns the index of the most intense peak in the range minMz to maxMz.
     * If there are no peaks in this range -1 returned.
     *
     * @param minX the minMz
     * @param maxX the maxMz
     * @return the index of the most intense peak in the range minMz to maxMz. If there are no peaks in this range -1
     * returned
     */
    @Override
    public int getMostIntenseIndex(double minX, double maxX)
    {
        if (minX > maxX)
            throw new IllegalArgumentException("Min mz needs to be smaller than max mz.  minMz = " + minX + " maxMz " + maxX);

        if (isEmpty())
            return -1;

        return findMostIntenseIndex(indexEqualOrLarger(minX, 0, xList), maxX);
    }

    @Override
    public int getMostIntenseIndex()
    {
        return findMostIntenseIndex(0, Double.MAX_VALUE);
    }

    public double[] getXs()
    {
        return getXs(null);
    }

    private int findMostIntenseIndex(int startIndex, double maxMz)
    {
        double maxIntensity = 0;
        int maxIndex = -1;
        for (int i = startIndex; i < size && xList[i] <= maxMz; i++) {

            double intensity = yList[i];
            if (intensity > maxIntensity) {

                maxIntensity = intensity;
                maxIndex = i;
            }
        }

        return maxIndex;
    }

    /**
     * Increases the capacity of this <tt>DoublePeakList</tt> instance, if
     * necessary, to ensure that it can hold at least the number of peaks
     * specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    @Override
    public void ensureCapacity(int minCapacity)
    {
        if (xList.length < minCapacity) {

            int remaining = minCapacity - xList.length;

            xList = ArrayUtils.grow(xList, remaining);

            yList = ArrayUtils.grow(yList, remaining);
        }
    }

    @Override
    public Precision getPrecision()
    {
        return Precision.DOUBLE;
    }

    @Override
    public PeakList<A> copy(SpectrumProcessor<A, A> spectrumProcessor)
    {
        return new DoublePeakList<>(this, spectrumProcessor);
    }

    @Override
    public DoublePeakList<A> copy(PeakProcessor<A, A> peakProcessor)
    {
        return new DoublePeakList<>(this, peakProcessor);
    }

    @Override
    public DoublePeakList<A> copy(PeakProcessorChain<A> peakProcessorChain)
    {
        return new DoublePeakList<>(this, peakProcessorChain);
    }


    public double[] getXs(double[] dest)
    {
        return ArrayUtils.copyArray(xList, 0, dest, 0, size);
    }


    @Override
    public double[] getXs(double[] dest, int destPos)
    {
        return ArrayUtils.copyArray(xList, 0, dest, destPos, size);
    }


    @Override
    public double[] getXs(int srcPos, double[] dest, int destPos, int length)
    {
        return ArrayUtils.copyArray(xList, srcPos, dest, destPos, length);
    }

    public double[] getYs()
    {
        return getYs(null);
    }


    public double[] getYs(double[] dest)
    {
        return getYs(0, dest, 0, size);
    }


    @Override
    public double[] getYs(double[] dest, int destPos)
    {
        return getYs(0, dest, destPos, size);
    }


    @Override
    public double[] getYs(int srcPos, double[] dest, int destPos, int length)
    {
        return ArrayUtils.copyArray(yList, srcPos, dest, destPos, length);
    }

    /**
     * Replace the intensity at index with intensity
     *
     * @param y     the new intensity
     * @param index the index for which the intensity is to be set
     * @throws IndexOutOfBoundsException if the index is out of range ( <tt>index &lt; 0 || index &gt;= size()</tt>)
     */
    @Override
    public void setYAt(double y, int index)
    {
        rangeCheck(index);

        double origIntensity = yList[index];

        yList[index] = y;

        totalIonCurrent = totalIonCurrent - origIntensity + y;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        @SuppressWarnings("rawtypes")
        DoublePeakList peakList = (DoublePeakList) o;

        return size == peakList.size && Double.compare(peakList.totalIonCurrent, totalIonCurrent) == 0
                && ArrayUtils.arrayEquals(yList, peakList.yList, size)
                && ArrayUtils.arrayEquals(xList, peakList.xList, size);
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();

        result = 31 * result + ArrayUtils.arrayHashCode(xList, size);
        result = 31 * result + ArrayUtils.arrayHashCode(yList, size);
        return result;
    }

    public String toString()
    {
        StringJoiner joiner = new StringJoiner("\n");
        for (int i = 0; i < this.size(); i++) {
            StringJoiner aj = new StringJoiner(";");
            aj.add(String.valueOf(getX(i)));
            aj.add(String.valueOf(getY(i)));
            if (hasAnnotationsAt(i)) {
                List<A> annotations = getAnnotations(i);
                aj.add(annotations.toString());
            }
            joiner.add(aj.toString());
        }
        return joiner.toString();
    }

    /**
     * PeakSink that is used to merge two peak lists
     */
    private static class MergePeakSink<A extends PeakAnnotation> extends AbstractMergePeakSink<A, DoublePeakList<A>>
    {
        private double[] tmpMzList;
        private double[] tmpIntensityList;

        private MergePeakSink(DoublePeakList<A> peakList)
        {
            super(peakList);
        }

        @Override
        protected void addY(double intensity, int index)
        {
            tmpIntensityList[index] = tmpIntensityList[index] + intensity;
        }

        @Override
        public void start(int size)
        {
            tmpMzList = new double[size];
            tmpIntensityList = new double[size];
        }

        protected void addToArray(double mz, double intensity, int index)
        {
            tmpMzList[index] = mz;
            tmpIntensityList[index] = intensity;
        }

        @Override
        protected double getX(int index)
        {
            return tmpMzList[index];
        }

        protected void setArrays()
        {
            peakList.xList = tmpMzList;
            peakList.yList = tmpIntensityList;
        }
    }
}
