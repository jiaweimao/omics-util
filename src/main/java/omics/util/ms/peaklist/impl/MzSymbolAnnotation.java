package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.protein.ms.Ion;

/**
 * An annotation of peak with symbol.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 21 May 2018, 8:02 PM
 */
public class MzSymbolAnnotation implements PeakAnnotation
{
    private double mz;
    private int charge;
    private String symbol;
    private Ion ion;

    public MzSymbolAnnotation(MzSymbolAnnotation src)
    {
        this.mz = src.mz;
        this.charge = src.charge;
        this.symbol = src.symbol;
        this.ion = src.ion;
    }

    public MzSymbolAnnotation(double mz)
    {
        this(mz, 1);
    }

    /**
     * Create a new {@link MzSymbolAnnotation}
     *
     * @param mz     peak mz
     * @param charge peak charge
     */
    public MzSymbolAnnotation(double mz, int charge)
    {
        this(mz, charge, MZ_FORMAT.format(mz));
    }

    /**
     * Create a new {@link MzSymbolAnnotation} with default {@link Ion#mz}
     *
     * @param mz     peak mz
     * @param charge peak charge
     * @param symbol annotation symbol
     */
    public MzSymbolAnnotation(double mz, int charge, String symbol)
    {
        this(mz, charge, symbol, Ion.mz);
    }

    /**
     * Create a new {@link MzSymbolAnnotation}
     *
     * @param mz              peak mz
     * @param charge          peak charge
     * @param symbol          symbol
     * @param ion {@link Ion}
     */
    public MzSymbolAnnotation(double mz, int charge, String symbol, Ion ion)
    {
        this.mz = mz;
        this.charge = charge;
        this.symbol = symbol;
        this.ion = ion;
    }

    /**
     * @return m/z value
     */
    public double getMz()
    {
        return mz;
    }

    /**
     * @return charge of the peak.
     */
    public int getCharge()
    {
        return charge;
    }

    @Override
    public String getSymbol()
    {
        return symbol;
    }

    public Ion getIon()
    {
        return ion;
    }

    @Override
    public PeakAnnotation copy()
    {
        return new MzSymbolAnnotation(this);
    }
}
