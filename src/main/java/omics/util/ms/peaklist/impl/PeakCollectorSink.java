package omics.util.ms.peaklist.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.PeakSink;

import java.util.List;

/**
 * Sink that adds peaks to a peak list.
 * This class use a temp cache of the data, and push all data to the destination peak list after
 * the end() is called.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 06 May 2017, 6:33 PM
 */
public class PeakCollectorSink<A extends PeakAnnotation> implements PeakSink<A>
{
    private final DoubleArrayList mzList = new DoubleArrayList();
    private final DoubleArrayList intensityList = new DoubleArrayList();
    private final ListMultimap<Integer, A> annotationMap = ArrayListMultimap.create();

    private PeakList<A> dest;

    public PeakCollectorSink() { }

    public PeakCollectorSink(PeakList<A> dest)
    {
        this.dest = dest;
    }

    public void setPeakList(PeakList<A> dest)
    {
        this.dest = dest;
    }

    @Override
    public void start(int size)
    {
        mzList.clear();
        mzList.ensureCapacity(size);

        intensityList.clear();
        intensityList.ensureCapacity(size);
        annotationMap.clear();
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations)
    {
        mzList.add(mz);
        intensityList.add(intensity);

        if (annotations != null && !annotations.isEmpty()) {
            annotationMap.putAll(mzList.size() - 1, annotations);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void end()
    {
        dest.clear();
        for (int i = 0; i < mzList.size(); i++) {
            int index = dest.add(mzList.getDouble(i), intensityList.getDouble(i));
            if (annotationMap.containsKey(i)) {
                List<A> as = annotationMap.get(i);
                for (A annotation : as) {
                    dest.addAnnotation(index, (A) annotation.copy());
                }
            }
        }
    }
}
