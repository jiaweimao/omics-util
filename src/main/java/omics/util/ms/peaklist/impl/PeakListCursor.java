package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakCursor;
import omics.util.ms.peaklist.PeakList;

import java.util.List;

import static omics.util.utils.ObjectUtils.checkElementIndex;

/**
 * A PeakList Cursor implementation.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2018, 4:13 PM
 */
public class PeakListCursor<A extends PeakAnnotation> implements PeakCursor<A>
{
    private final PeakList<A> peakList;
    private final int size;
    private int cursorIndex = -1;

    public PeakListCursor(PeakList<A> peakList)
    {
        this.peakList = peakList;
        this.size = peakList.size();
    }

    @Override
    public int size()
    {
        return size;
    }

    @Override
    public boolean isEmpty()
    {
        return size == 0;
    }

    @Override
    public void resetCursor()
    {
        cursorIndex = -1;
    }

    @Override
    public boolean next()
    {
        if (cursorIndex < size - 1) {

            cursorIndex++;

            return true;
        } else {

            return false;
        }
    }

    @Override
    public boolean previous()
    {
        cursorIndex = Math.max(cursorIndex - 1, -1);
        return cursorIndex > -1;
    }

    @Override
    public List<A> currAnnotations()
    {
        checkElementIndex(cursorIndex, size);

        return peakList.getAnnotations(cursorIndex);
    }

    @Override
    public boolean canPeek(int n)
    {
        return cursorIndex + n < size;
    }

    /**
     * Move the cursor to the m/z that is closest to <code>mz</code>
     *
     * @param mz the m/z value to which the cursor is to be moved
     */
    @Override
    public void moveToClosest(double mz)
    {
        cursorIndex = getClosestIndex(mz);
    }

    @Override
    public void moveBefore(double mz)
    {
        cursorIndex = getClosestIndex(mz);

        if (currMz() >= mz) {

            previous();
        }
    }

    @Override
    public void movePast(double mz)
    {
        if (cursorIndex == -1) {

            cursorIndex++;
        }

        while (cursorIndex < size && mz(cursorIndex) <= mz) {

            cursorIndex++;
        }
    }

    @Override
    public int getClosestIndex(double mz)
    {
        return peakList.getClosestIndex(mz);
    }

    @Override
    public String toString()
    {
        return currMz() + ", " + currIntensity();
    }

    @Override
    public boolean next(double mz)
    {
        if (cursorIndex == -1 || peakList.getX(cursorIndex) <= mz) {

            cursorIndex++;
        }

        boolean more = cursorIndex < size;

        cursorIndex = Math.min(cursorIndex, size - 1);

        return more;
    }

    @Override
    public double currIntensity()
    {
        return intensity(cursorIndex);
    }

    @Override
    public double currMz()
    {
        return mz(cursorIndex);
    }

    @Override
    public double peekMz(int n)
    {
        return mz(cursorIndex + n);
    }

    @Override
    public double lastMz()
    {
        return mz(size - 1);
    }

    @Override
    public double lastIntensity()
    {
        return intensity(size - 1);
    }

    @Override
    public double peekIntensity(int n)
    {
        return intensity(cursorIndex + n);
    }

    @Override
    public double mz(int index)
    {
        checkElementIndex(index, size);

        return peakList.getX(index);
    }

    @Override
    public double intensity(int index)
    {
        checkElementIndex(index, size);

        return peakList.getY(index);
    }
}
