package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

import static java.util.Objects.requireNonNull;

/**
 * PeakListMerger is used to merge multiple peak list.
 * <p>
 * This PeakListMerger cannot combine the mzs with same value.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Oct 2017, 7:16 PM
 */
public class PeakListMerger<A extends PeakAnnotation> implements PeakSource<A>
{
    private PeakSink<A> sink;
    private final PeakCursorComparator comparator = new PeakCursorComparator();

    @Override
    public <PS extends PeakSink<A>> PS sink(PS sink)
    {
        requireNonNull(sink);

        this.sink = sink;
        return sink;
    }

    public void merge(Collection<? extends PeakList<A>> peakLists)
    {
        requireNonNull(peakLists);
        requireNonNull(sink, "Sink is null, use sink() to set a sink to collect the merged peaks");

        PeakCursor[] peakCursors = new PeakCursor[peakLists.size()];

        int totalSize = 0;
        int size = 0;
        for (PeakList peakList : peakLists) {

            if (!peakList.isEmpty()) { // ignore empty peak list

                totalSize += peakList.size();
                final PeakCursor cursor = peakList.cursor();
                cursor.next(); // PeakCursor is prepared
                peakCursors[size++] = cursor;
            }
        }

        doMerge(peakCursors, totalSize, size);
    }

    public void merge(PeakCursor<A> peakCursor1, PeakCursor<A> peakCursor2)
    {
        requireNonNull(peakCursor1);
        requireNonNull(peakCursor2);
        requireNonNull(sink, "Sink is null, use sink() to set a sink to collect the merged peaks");

        int totalSize = peakCursor1.size() + peakCursor2.size();
        if (!peakCursor1.isEmpty() && !peakCursor2.isEmpty()) {

            peakCursor1.next();
            peakCursor2.next();
            doMerge(new PeakCursor[]{peakCursor1, peakCursor2}, totalSize, 2);
        } else if (!peakCursor1.isEmpty()) {
            copy(peakCursor1, totalSize);
        } else if (!peakCursor2.isEmpty()) {
            copy(peakCursor2, totalSize);
        }
    }

    /**
     * Copy all peak list of the PeakCursor to the Sink.
     *
     * @param cursor    source PeakCursor
     * @param totalSize total number of peaks.
     */
    private void copy(PeakCursor<A> cursor, int totalSize)
    {
        sink.start(totalSize);

        while (cursor.next()) {
            sink.processPeak(cursor.currMz(), cursor.currIntensity(), cursor.currAnnotations());
        }

        sink.end();
    }

    /**
     * Merge all PeakCursors.
     *
     * @param peakCursors All PeakCursor to merge
     * @param totalSize   number of peaks in all PeakCursors
     * @param size        number of PeakCursor
     */
    private void doMerge(PeakCursor[] peakCursors, int totalSize, int size)
    {
        Arrays.sort(peakCursors, 0, size, comparator);
        sink.start(totalSize);

        while (size > 0) {
            PeakCursor current = peakCursors[0]; // the PeakCursor with the smallest mz
            sink.processPeak(current.currMz(), current.currIntensity(), current.currAnnotations());

            if (current.next()) {
                if (size > 1 && current.currMz() > peakCursors[1].currMz()) {
                    int index = Arrays.binarySearch(peakCursors, 1, size, current, comparator);
                    if (index < 0) {
                        index = -1 * (index + 1);
                    }

                    System.arraycopy(peakCursors, 1, peakCursors, 0, index - 1);
                    peakCursors[index - 1] = current;
                }
            } else {
                if (size > 1)
                    System.arraycopy(peakCursors, 1, peakCursors, 0, size - 1);
                size--;
            }
        }

        sink.end();
    }

    private static class PeakCursorComparator implements Comparator<PeakCursor>
    {
        @Override
        public int compare(PeakCursor o1, PeakCursor o2)
        {
            return Double.compare(o1.currMz(), o2.currMz());
        }
    }
}
