package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.PeakSink;
import omics.util.ms.Spectrum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2018, 3:38 PM
 */
public class SpectrumMergePeakSink<A extends PeakAnnotation, S extends Spectrum<A>> implements PeakSink<A> {

    protected final Map<Integer, List<A>> tmpAnnotationMap = new HashMap<>();
    private PeakList<A> peakList;
    private Spectrum<A> spectrum;
    protected int tmpSize = 0;
    protected double tmpTotalIonCurrent = 0;

    protected SpectrumMergePeakSink(S spectrum) {
        this.spectrum = spectrum;
    }

    @Override
    public void start(int size) {

    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        if (tmpSize > 0 && mz == getX(tmpSize - 1)) { // if current mz equals to the previous peak

            addY(intensity, tmpSize - 1);

            if (!annotations.isEmpty()) {
                List<A> currentAnnotations = tmpAnnotationMap.get(tmpSize - 1);
                if (currentAnnotations == null)
                    tmpAnnotationMap.put(tmpSize - 1, new ArrayList<>(annotations));
                else
                    currentAnnotations.addAll(annotations);
            }
        } else {

            if (!(tmpSize == 0 || mz >= getX(tmpSize - 1))) {

                throw new UnsortedPeakListException(tmpSize);
            }

            addToArray(mz, intensity, tmpSize);

            if (!annotations.isEmpty()) {

                tmpAnnotationMap.put(tmpSize, new ArrayList<>(annotations));
            }

            tmpSize++;
        }
        tmpTotalIonCurrent += intensity;
    }

    /**
     * add intensity value to peak at given index
     *
     * @param intensity intensity
     * @param index     peak index
     */
    protected void addY(double intensity, int index) {
        peakList.setYAt(peakList.getY(index) + intensity, index);
    }

    @Override
    public void end() {
//        peakList.size = tmpSize;
//        peakList.totalIonCurrent = tmpTotalIonCurrent;
//        setArrays();
//
//        peakList.annotationMap.clear();
//        for (Integer id : tmpAnnotationMap.keySet()) {
//            peakList.annotationMap.putAll(id, tmpAnnotationMap.get(id));
//        }
    }

    protected void setArrays() {

    }

    /**
     * return mz value at given index.
     *
     * @param index peak index
     * @return mz value
     */
    protected double getX(int index) {
        return 0;
    }

    /**
     * add a peak to given index.
     *
     * @param mz        peak mz
     * @param intensity peak intensity
     * @param tmpSize   index
     */
    protected void addToArray(double mz, double intensity, int tmpSize) {

    }
}
