package omics.util.ms.peaklist.impl;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 6:32 PM
 */
public class UnsortedPeakListException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "mz values are not sorted at index ";

    private final int peakIndex;

    public UnsortedPeakListException(int peakIndex) {
        super(DEFAULT_MESSAGE + peakIndex);
        this.peakIndex = peakIndex;
    }

    public UnsortedPeakListException(String message, int peakIndex) {
        super(message + " - " + DEFAULT_MESSAGE + peakIndex);
        this.peakIndex = peakIndex;
    }

    public UnsortedPeakListException(String message, UnsortedPeakListException cause) {
        super(message, cause);
        this.peakIndex = cause.peakIndex;
    }

    public int getPeakIndex() {
        return peakIndex;
    }
}
