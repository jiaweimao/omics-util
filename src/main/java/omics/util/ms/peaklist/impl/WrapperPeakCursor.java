package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakCursor;

import java.util.List;
import java.util.function.Function;

/**
 * A wrapper for a peak cursor that transforms annotations using a Function.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2018, 4:16 PM
 */
public class WrapperPeakCursor<IN extends PeakAnnotation, A extends PeakAnnotation> implements PeakCursor<A> {
    private final PeakCursor<IN> cursor;
    private final Function<List<IN>, List<A>> annotationFunction;

    public WrapperPeakCursor(PeakCursor<IN> cursor, Function<List<IN>, List<A>> annotationFunction) {
        this.cursor = cursor;
        this.annotationFunction = annotationFunction;
    }

    @Override
    public int size() {
        return cursor.size();
    }

    @Override
    public boolean isEmpty() {
        return cursor.isEmpty();
    }

    @Override
    public void resetCursor() {
        cursor.resetCursor();
    }

    @Override
    public boolean next() {
        return cursor.next();
    }

    @Override
    public boolean previous() {
        return cursor.previous();
    }

    @Override
    public boolean next(double mz) {
        return cursor.next(mz);
    }

    @Override
    public double currIntensity() {
        return cursor.currIntensity();
    }

    /**
     * Returns an empty annotation list
     *
     * @return an empty annotation list
     */
    public List<A> currAnnotations() {
        return annotationFunction.apply(cursor.currAnnotations());
    }

    @Override
    public double currMz() {
        return cursor.currMz();
    }

    @Override
    public boolean canPeek(int n) {
        return cursor.canPeek(n);
    }

    @Override
    public double peekIntensity(int n) {
        return cursor.peekIntensity(n);
    }

    @Override
    public double peekMz(int n) {
        return cursor.peekMz(n);
    }

    @Override
    public double lastMz() {
        return cursor.lastMz();
    }

    @Override
    public double lastIntensity() {
        return cursor.lastIntensity();
    }

    @Override
    public double mz(int index) {
        return cursor.mz(index);
    }

    @Override
    public double intensity(int index) {
        return cursor.intensity(index);
    }

    @Override
    public void moveToClosest(double mz) {
        cursor.moveToClosest(mz);
    }

    @Override
    public void moveBefore(double mz) {
        cursor.moveBefore(mz);
    }

    @Override
    public void movePast(double mz) {
        cursor.movePast(mz);
    }

    @Override
    public int getClosestIndex(double mz) {
        return cursor.getClosestIndex(mz);
    }
}
