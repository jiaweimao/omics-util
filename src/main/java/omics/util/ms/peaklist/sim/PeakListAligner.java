package omics.util.ms.peaklist.sim;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.impl.PairEntry;

import java.util.ArrayList;
import java.util.List;


/**
 * abstract class to be extended by classes that can align two PeakLists.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 20 Mar 2017, 9:07 AM
 */
public abstract class PeakListAligner<X extends PeakAnnotation, Y extends PeakAnnotation>
{
    private Tolerance iTol;

    public PeakListAligner(Tolerance iTol)
    {
        this.iTol = iTol;
    }

    /**
     * set the tolerance to match peak.
     *
     * @param tol {@link Tolerance} instance.
     */
    public void setTolerance(Tolerance tol)
    {
        this.iTol = tol;
    }

    /**
     * Return all peak list
     *
     * @param xPeakList peaklist to match
     * @param yPeakList peaklist to match
     * @return all matched peak pairs
     */
    protected List<PairEntry> getMatchEntries(PeakList<X> xPeakList, PeakList<Y> yPeakList)
    {
        List<PairEntry> matchEntryList = new ArrayList<>();
        int start = 0;
        for (int xId = 0; xId < xPeakList.size(); xId++) {

            boolean match = false;
            for (int yId = start; yId < yPeakList.size(); yId++) {

                double xMz = xPeakList.getX(xId);
                double yMz = yPeakList.getX(yId);

                Tolerance.Location check = iTol.check(xMz, yMz);
                if (check == Tolerance.Location.WITHIN) {
                    matchEntryList.add(new PairEntry(xId, yId, xMz, yMz, xPeakList.getY(xId), yPeakList.getY(yId)));
                    if (!match) {
                        match = true;
                        start = yId;
                    }
                } else if (check == Tolerance.Location.LARGER)
                    break;
            }
        }
        return matchEntryList;
    }

    /**
     * Align peakListX with peakListY. The aligned peak pair indexes are put in the map.
     *
     * @param xPeakList the first peak list
     * @param yPeakList the second peak list
     * @return match peak pair indexes.
     */
    public abstract Int2IntMap align(PeakList<X> xPeakList, PeakList<Y> yPeakList);
}
