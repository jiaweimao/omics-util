package omics.util.ms.peaklist.sim;

import omics.util.ms.peaklist.PeakAnnotation;

/**
 * Interface to be implemented by any class that processes peak pairs and acts
 * as an element in a peak pair processor chain.
 *
 * @author JiaweiMao 2017.03.20
 * @since 1.0.0
 */
public interface PeakPairProcessor<X extends PeakAnnotation, Y extends PeakAnnotation>
        extends PeakPairSink<X, Y>, PeakPairSource<X, Y> {}
