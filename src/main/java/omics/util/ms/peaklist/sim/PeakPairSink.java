package omics.util.ms.peaklist.sim;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;


/**
 * Interface to be implemented by any class that is a sink for peak pairs.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 6:34 PM
 */
public interface PeakPairSink<X extends PeakAnnotation, Y extends PeakAnnotation> {
    /**
     * Called before a new PeakList pair is processed
     *
     * @param xPeakList the first member of the peak pair that is going to be processed
     * @param yPeakList the second member of the peak pair that is going to be processed
     */
    void begin(PeakList<X> xPeakList, PeakList<Y> yPeakList);

    /**
     * Pushes the next peak pair to this sink
     *
     * @param xIndex index of the peak in x peak list.
     * @param yIndex index of the peak in y peak list.
     */
    void processPeakPair(int xIndex, int yIndex);

    /**
     * Called when all the peak pairs in the PeakList pair have been processed
     */
    void end();
}
