package omics.util.ms.peaklist.sim;

import omics.util.ms.peaklist.PeakAnnotation;

/**
 * A peak pair source that can push peak pairs to a sink.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 6:35 PM
 */
public interface PeakPairSource<X extends PeakAnnotation, Y extends PeakAnnotation> {

    void sink(PeakPairSink<X, Y> sink);
}
