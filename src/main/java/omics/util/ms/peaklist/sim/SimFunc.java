package omics.util.ms.peaklist.sim;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;

/**
 * Interface for any class that can calculate the similarity between two PeakLists.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 6:35 PM
 */
public interface SimFunc<X extends PeakAnnotation, Y extends PeakAnnotation> {
    /**
     * Calculate the similarity score of the two vectors.
     *
     * @param plX the fist peak list
     * @param plY the second peak list
     * @return the similarity score or NaN if the score cannot be calculated
     */
    double calcSimilarity(PeakList<X> plX, PeakList<Y> plY);

    /**
     * @return the total number peaks in plX and plY from the last call to calcSimilarity
     */
    int getTotalPeakCount();

    /**
     * Return the best score that can be returned by calls to calcSimilarity.
     * For similarity functions for which the best score has no upper limit
     * Double.POSITIVE_INFINITY is returned and where the best score has no
     * lower limit Double.NEGATIVE_INFINITY is returned.
     *
     * @return the best score that can be returned by calls to calcSimilarity
     */
    double getBestScore();

    /**
     * Return the worst the score that can be returned by calls to
     * calcSimilarity. For similarity functions for which the worst score has no
     * upper limit Double.POSITIVE_INFINITY is returned and where the worst
     * score has no lower limit Double.NEGATIVE_INFINITY is returned.
     *
     * @return the worst score that can be returned by calls to calcSimilarity
     */
    double getWorstScore();
}
