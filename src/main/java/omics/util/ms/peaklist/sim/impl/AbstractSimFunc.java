package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;
import omics.util.ms.peaklist.sim.SimFunc;


/**
 * Abstract similarity function. This class takes care of vectorizing the PeakList pairs.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 06 Mar 2017, 6:36 PM
 */
public abstract class AbstractSimFunc<X extends PeakAnnotation, Y extends PeakAnnotation> implements SimFunc<X, Y> {

    private final PeakListAligner<X, Y> iAligner;

    /**
     * Creates a AbstractSimFunc that uses a DefaultPeakListAligner and clear
     * processor chain.
     *
     * @param tolerance the tolerance to use when aligning the two PeakLists
     */
    protected AbstractSimFunc(Tolerance tolerance) {
        this(new PreferCloserMzAligner<>(tolerance));
    }

    /**
     * Creates a AbstractSimFunc that uses the supplied PeakListAligner and
     * PeakPairProcessor chain.
     *
     * @param peakListAligner the object that is used to align the PeakList pairs
     */
    protected AbstractSimFunc(PeakListAligner<X, Y> peakListAligner) {
        this.iAligner = peakListAligner;
    }

    /**
     * Aligns the two PeakLists and then processes the aligned peak tupplets
     * using the PeakPairProcessor chain.
     *
     * @param peakListX the first peak list
     * @param peakListY the second peak list
     */
    public Int2IntMap vectorize(PeakList<X> peakListX, PeakList<Y> peakListY) {
        return iAligner.align(peakListX, peakListY);
    }
}
