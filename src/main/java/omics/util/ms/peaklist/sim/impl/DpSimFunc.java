package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;


/**
 * Calculates the similarity between two PeakLists using the dot product (dp).
 * http://www.mvps.org/DirectX/articles/math/dot/index.htm
 * <p>
 * A · B = A1B1 + ... + AnBn
 * <p>
 * if the lengths of the vectors is one (1.0), the dot product equals the Normalized dot product.
 * <p>
 * A · B = |A| * |B| * cos(Θ)
 * A · B = 1 * 1 * cos(Θ)
 * A · B = cos(Θ)
 * <p>
 * <p>
 * A minimum peak count can be set so that NaN is returned for peak list pairs
 * that do not have enough peaks to calculate a reliable dp.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 6:36 PM
 */
public class DpSimFunc<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractSimFunc<X, Y> {
    private final int minPeakCount;
    private int peakCount = 0;

    /**
     * Construct a NdpSimFunc that uses the default peak list aligner.
     *
     * @param minPeakCount the minimum number of peaks that are required in order to calculate the similarity
     * @param tolerance    the tolerance to use when aligning the two PeakLists
     */
    public DpSimFunc(int minPeakCount, Tolerance tolerance) {
        super(tolerance);
        this.minPeakCount = minPeakCount;
    }

    /**
     * Construct a NdpSimFunc that uses the supplied PeakListAligner and
     * PeakPairProcessor chain to process and align the peak lists.
     *
     * @param minPeakCount    the minimum number of peaks that are required in order to calculate the ndp
     * @param peakListAligner the object that is used to allign the PeakList pairs
     */
    public DpSimFunc(int minPeakCount, PeakListAligner<X, Y> peakListAligner) {
        super(peakListAligner);
        this.minPeakCount = minPeakCount;
    }

    @Override
    public double calcSimilarity(PeakList<X> plX, PeakList<Y> plY) {
        peakCount = plX.size() + plY.size();
        Int2IntMap map = vectorize(plX, plY);

        double aDotB = 0;
        for (int key : map.keySet()) {
            aDotB += plX.getY(key) * plY.getY(map.get(key));
        }

        return peakCount < minPeakCount ? Double.NaN : aDotB;
    }

    @Override
    public int getTotalPeakCount() {
        return peakCount;
    }

    @Override
    public double getBestScore() {
        return 1;
    }

    @Override
    public double getWorstScore() {
        return 0;
    }

}
