package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import omics.util.math.MathUtils;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;
import omics.util.protein.ms.Ion;
import omics.util.protein.ms.PeptideFragmentAnnotation;

import java.util.List;

/**
 * Calculate the Hyper score, ref:
 * Kong, A. T., Leprevost, F. V, Avtonomov, D. M., Mellacheruvu, D. & Nesvizhskii, A. I.
 * MSFragger: ultrafast and comprehensive peptide identification in mass spectrometry–based proteomics. Nat. Methods 14, 513–520 (2017).
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 15 Sep 2018, 9:09 PM
 */
public class HyperScore<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractSimFunc<X, Y>
{

    public HyperScore(Tolerance tolerance)
    {
        super(tolerance);
    }

    public HyperScore(PeakListAligner<X, Y> peakListAligner)
    {
        super(peakListAligner);
    }

    /**
     * Calculate the hyper score
     *
     * @param plX the fist peak list, the experimental peaks list
     * @param plY the second peak list, the theoretical peak list, for hyper score, only b,y are supported, it is means
     *            the <code>plY</code> should be theoretical peak list of b and y ions.
     * @return hyper score.
     */
    @Override
    public double calcSimilarity(PeakList<X> plX, PeakList<Y> plY)
    {
        Int2IntMap matchPeaks = vectorize(plX, plY);

        double bIntensitySum = .0;
        double yIntensitySum = .0;
        int matchedB = 0;
        int matchedY = 0;
        for (int key : matchPeaks.keySet()) {
            int i = matchPeaks.get(key);

            List<Y> annotations = plY.getAnnotations(i);
            for (Y anno : annotations) {
                if (anno instanceof PeptideFragmentAnnotation) {
                    PeptideFragmentAnnotation a = (PeptideFragmentAnnotation) anno;
                    if (a.getIon() == Ion.b) {
                        matchedB++;
                        bIntensitySum += plX.getY(key);
                    } else if (a.getIon() == Ion.y) {
                        matchedY++;
                        yIntensitySum += plX.getY(key);
                    }
                }
            }
        }
        double eHyper = MathUtils.factorial(matchedB) * MathUtils.factorial(matchedY) * bIntensitySum * yIntensitySum;

        return Math.log10(eHyper);
    }

    @Override
    public int getTotalPeakCount()
    {
        return 0;
    }

    @Override
    public double getBestScore()
    {
        return Double.MAX_VALUE;
    }

    @Override
    public double getWorstScore()
    {
        return 0;
    }
}
