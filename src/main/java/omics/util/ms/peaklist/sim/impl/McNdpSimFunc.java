package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import omics.util.math.NormalizedDotProduct;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;

/**
 * Calculates PeakList similarity using the Mean Centered Normalized Dot Product similarity function
 * <p>
 * Olson et.al.. Evaluating reproducibility and similarity of mass and intensity data in complex spectra--applications
 * to tubulin. Journal of the American Society for Mass Spectrometry, 19(3), 367-74.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 6:37 PM
 */
public class McNdpSimFunc<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractSimFunc<X, Y> {

    private final DoubleArrayList vectorX = new DoubleArrayList();
    private final DoubleArrayList vectorY = new DoubleArrayList();
    private final int minPeakCount;
    private double totalX = 0;
    private double totalY = 0;
    private int peakCountX = 0;
    private int peakCountY = 0;

    /**
     * Construct a McNdpSimFunc that uses the default peak list aligner.
     *
     * @param minPeakCount the minimum number of peaks that are required in order to calculate the similarity
     * @param tolerance    the tolerance to use when aligning the two PeakLists
     */
    public McNdpSimFunc(int minPeakCount, Tolerance tolerance) {
        super(tolerance);
        this.minPeakCount = minPeakCount;
    }

    /**
     * Construct a McNdpSimFunc that uses the supplied PeakListAligner and PeakPairProcessor chain to process
     * and align the peak lists.
     *
     * @param minPeakCount    the minimum number of peaks that are required in order to calculate the ndp
     * @param peakListAligner the object that is used to allign the PeakList pairs
     */
    public McNdpSimFunc(int minPeakCount, PeakListAligner<X, Y> peakListAligner) {
        super(peakListAligner);
        this.minPeakCount = minPeakCount;
    }

    @Override
    public double calcSimilarity(PeakList<X> plX, PeakList<Y> plY) {
        peakCountX = plX.size();
        peakCountY = plY.size();

        totalX = plX.getTotalIonCurrent();
        totalY = plY.getTotalIonCurrent();

        vectorize(plX, plY);

        if (peakCountX + peakCountY < minPeakCount) return Double.NaN;

        meanCenter(vectorX, totalX / peakCountX);
        meanCenter(vectorY, totalY / peakCountY);

        return NormalizedDotProduct.cosim(vectorX, vectorY);
    }

    private void meanCenter(DoubleArrayList vector, double mean) {
        for (int i = 0; i < vector.size(); i++) {
            double v = vector.getDouble(i);

            if (v > 0) {

                vector.set(i, v - mean);
            }
        }
    }

    @Override
    public int getTotalPeakCount() {
        return peakCountX + peakCountY;
    }


    @Override
    public double getBestScore() {
        return 1;
    }

    @Override
    public double getWorstScore() {
        return 0;
    }
}
