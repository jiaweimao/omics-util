package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;

/**
 * Calculates the similarity between two PeakLists using the normalized dot product (ndp).
 * <p>
 * A minimum peak count can be set so that NaN is returned for peak list pairs
 * that do not have enough peaks to calculate a reliable ndp.
 * <p>
 * This class also calculates the dot bias. The dot bias can be retrieved by
 * calling getDotBias
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 6:38 PM
 */
public class NdpSimFunc<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractSimFunc<X, Y>
{

    private final int minPeakCount;
    private int peakCount = 0;
    private double dotBias;

    /**
     * Construct a NdpSimFunc that uses the default peak list aligner.
     *
     * @param minPeakCount the minimum number of peaks that are required in order to calculate the similarity
     * @param tolerance    the tolerance to use when aligning the two PeakLists
     */
    public NdpSimFunc(int minPeakCount, Tolerance tolerance)
    {
        super(tolerance);
        this.minPeakCount = minPeakCount;
    }

    /**
     * Construct a NdpSimFunc that uses the supplied PeakListAligner and
     * PeakPairProcessor chain to process and align the peak lists.
     *
     * @param minPeakCount    the minimum number of peaks that are required in order to calculate the ndp
     * @param peakListAligner the object that is used to allign the PeakList pairs
     */
    public NdpSimFunc(int minPeakCount, PeakListAligner<X, Y> peakListAligner)
    {
        super(peakListAligner);
        this.minPeakCount = minPeakCount;
    }

    @Override
    public double calcSimilarity(PeakList<X> plX, PeakList<Y> plY)
    {
        double aDotB = 0;
        double aDotBsquare = 0;
        double distA = 0;
        double distB = 0;

        peakCount = plX.size() + plY.size();

        Int2IntMap map = vectorize(plX, plY);

        for (int i = 0; i < plX.size(); i++)
            distA += Math.pow(plX.getY(i), 2);
        for (int i = 0; i < plY.size(); i++)
            distB += Math.pow(plY.getY(i), 2);

        for (int xId : map.keySet()) {
            double xIn = plX.getY(xId);
            double yIn = plY.getY(map.get(xId));
            aDotB += xIn * yIn;
            aDotBsquare += Math.pow(xIn, 2) * Math.pow(yIn, 2);

        }

        distA = Math.sqrt(distA);
        distB = Math.sqrt(distB);

        dotBias = Math.sqrt(aDotBsquare) / aDotB;

        return peakCount < minPeakCount ? Double.NaN : aDotB / (distA * distB);
    }

    @Override
    public int getTotalPeakCount()
    {
        return peakCount;
    }

    /**
     * Returns the dot bias for the last calculated ndp
     *
     * @return the dot bias for the last calculated ndp
     */
    public double getDotBias()
    {
        return dotBias;
    }

    @Override
    public double getBestScore()
    {
        return 1;
    }

    @Override
    public double getWorstScore()
    {
        return -1;
    }
}
