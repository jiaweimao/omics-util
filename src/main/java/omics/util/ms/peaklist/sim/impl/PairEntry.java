package omics.util.ms.peaklist.sim.impl;


import java.util.Comparator;

/**
 * A pair of match peaks
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 13 Mar 2017, 3:59 PM
 */
public class PairEntry
{
    private final int xIndex;
    private final int yIndex;
    private final double xMz;
    private final double yMz;
    private final double xIntensity;
    private final double yIntensity;

    /**
     * Create a pair entry
     *
     * @param xIndex     index in peak list x
     * @param yIndex     index in peak list y
     * @param xMz        mz in peak list x
     * @param yMz        mz in peak list y
     * @param xIntensity intensity in peak list x
     * @param yIntensity intensity in peak list y
     */
    public PairEntry(int xIndex, int yIndex, double xMz, double yMz, double xIntensity, double yIntensity)
    {
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        this.xMz = xMz;
        this.yMz = yMz;
        this.xIntensity = xIntensity;
        this.yIntensity = yIntensity;
    }

    /**
     * @return the index of the peak found in the first peak list.
     */
    public int xIndex()
    {
        return xIndex;
    }

    /**
     * @return the index of the peak found in the second peak list.
     */
    public int yIndex()
    {
        return yIndex;
    }

    public double xMz()
    {
        return xMz;
    }

    public double yMz()
    {
        return yMz;
    }

    public double xIntensity()
    {
        return xIntensity;
    }

    public double yIntensity()
    {
        return yIntensity;
    }

    @Override
    public String toString()
    {
        return "PairEntry{" +
                "xIndex=" + xIndex +
                ", yIndex=" + yIndex +
                ", xMz=" + xMz +
                ", yMz=" + yMz +
                ", xIntensity=" + xIntensity +
                ", yIntensity=" + yIntensity +
                '}';
    }

    public static class IntensityComparator implements Comparator<PairEntry>
    {
        @Override
        public int compare(PairEntry o1, PairEntry o2)
        {
            return Double.compare(o2.xIntensity + o2.yIntensity, o1.xIntensity + o1.yIntensity);
        }
    }
}
