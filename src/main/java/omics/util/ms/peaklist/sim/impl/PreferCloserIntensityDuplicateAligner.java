package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;

import java.util.Comparator;
import java.util.List;

/**
 * Aligns two peak lists.
 * If a peak in PeakList can match many peaks in another PeakList, choose the one with nearest intensity.
 * <p>
 * If a peak in <code>yPeakList</code> can match multiple peaks in <code>xPeakList</code>, all matches are retained.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 Feb 2018, 4:40 PM
 */
public class PreferCloserIntensityDuplicateAligner<X extends PeakAnnotation, Y extends PeakAnnotation> extends PeakListAligner<X, Y> {
    public PreferCloserIntensityDuplicateAligner(Tolerance iTol) {
        super(iTol);
    }

    @Override
    public Int2IntMap align(PeakList<X> xPeakList, PeakList<Y> yPeakList) {
        List<PairEntry> matchEntries = getMatchEntries(xPeakList, yPeakList);
        matchEntries.sort(Comparator.comparingDouble(o -> Math.abs(o.xIntensity() - o.yIntensity())));

        Int2IntMap map = new Int2IntOpenHashMap();
        IntSet xSet = new IntOpenHashSet();
        for (PairEntry matchEntry : matchEntries) {
            if (xSet.contains(matchEntry.xIndex()))
                continue;

            int xId = matchEntry.xIndex();
            int yId = matchEntry.yIndex();

            map.put(xId, yId);

            xSet.add(xId);
        }

        return map;
    }
}
