package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;

import java.util.Comparator;
import java.util.List;


/**
 * Peak list aligner which choose peak pair with near mz if multiple matches exist.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 May 2017, 9:11 AM
 */
public class PreferCloserMzAligner<X extends PeakAnnotation, Y extends PeakAnnotation> extends PeakListAligner<X, Y> {
    public PreferCloserMzAligner(Tolerance iTol) {
        super(iTol);
    }

    @Override
    public Int2IntMap align(PeakList<X> xPeakList, PeakList<Y> yPeakList) {
        List<PairEntry> matchEntries = getMatchEntries(xPeakList, yPeakList);
        matchEntries.sort(Comparator.comparingDouble(o -> Math.abs(o.xMz() - o.yMz())));

        Int2IntMap map = new Int2IntOpenHashMap();
        IntSet xSet = new IntOpenHashSet();
        IntSet ySet = new IntOpenHashSet();
        for (PairEntry matchEntry : matchEntries) {
            if (xSet.contains(matchEntry.xIndex()) || ySet.contains(matchEntry.yIndex()))
                continue;

            int xId = matchEntry.xIndex();
            int yId = matchEntry.yIndex();
            map.put(xId, yId);

            xSet.add(xId);
            ySet.add(yId);
        }

        return map;
    }
}
