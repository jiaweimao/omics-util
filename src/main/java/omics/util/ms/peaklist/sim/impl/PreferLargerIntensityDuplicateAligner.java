package omics.util.ms.peaklist.sim.impl;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;

import java.util.List;

/**
 * Aligns two peak lists.
 * If a peak in PeakList can match many peak in another PeakList, choose the one with highest intensity.
 * <p>
 * peaks in yPeakList can match multiple peaks in xPeakList.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 Feb 2018, 3:49 PM
 */
public class PreferLargerIntensityDuplicateAligner<X extends PeakAnnotation, Y extends PeakAnnotation> extends PeakListAligner<X, Y>
{
    public PreferLargerIntensityDuplicateAligner(Tolerance iTol)
    {
        super(iTol);
    }

    @Override
    public Int2IntMap align(PeakList<X> xPeakList, PeakList<Y> yPeakList)
    {
        List<PairEntry> matchEntries = getMatchEntries(xPeakList, yPeakList);
        matchEntries.sort((o1, o2) -> Double.compare(o2.xIntensity() + o2.yIntensity(), o1.xIntensity() + o1.yIntensity()));

        Int2IntMap map = new Int2IntOpenHashMap();
        IntSet xSet = new IntOpenHashSet();
        for (PairEntry matchEntry : matchEntries) {
            if (xSet.contains(matchEntry.xIndex()))
                continue;

            int xId = matchEntry.xIndex();
            int yId = matchEntry.yIndex();

            map.put(xId, yId);

            xSet.add(xId);
        }

        return map;
    }
}
