package omics.util.ms.peaklist.transform;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;

import java.util.List;


/**
 * Data processor that do no change to peak.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 6:40 PM
 */
public class IdentityPeakProcessor<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        sink.processPeak(mz, intensity, annotations);
    }
}
