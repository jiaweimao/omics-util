package omics.util.ms.peaklist.transform;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;

import java.util.Collections;
import java.util.List;


/**
 * Normalize the spectrum peaks with total ion current.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 6:40 PM
 */
public class IonCurrentNormalizer<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A>
{
    private double sum = 0;

    @Override
    public void start(int size)
    {
        super.start(size);

        sum = 0;
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations)
    {
        sum += intensity;
        super.processPeak(mz, intensity, annotations);
    }

    /**
     * @return the sum of all peak intensities.
     */
    public double sum()
    {
        return sum;
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap<List<A>>
            annotationMap)
    {
        for (int i = 0; i < mzList.size(); i++) {

            List<A> annotations;
            if (annotationMap.containsKey(i))
                annotations = annotationMap.get(i);
            else
                annotations = Collections.emptyList();

            sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i) / sum, annotations);
        }
    }
}
