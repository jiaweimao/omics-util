package omics.util.ms.peaklist.transform;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;

import java.util.List;

/**
 * Shift the peaks' mz in the peak list.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 6:41 PM
 */
public class MzShiftTransformer<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {
    /**
     * The value added to the current mz value.
     */
    private double shift;

    /**
     * @param shift shift mz value.
     */
    public MzShiftTransformer(double shift) {
        this.shift = shift;
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        sink.processPeak(mz + shift, intensity, annotations);
    }

    @Override
    public String toString() {
        return "MzShiftTransformer {" + "shiftMz=" + shift + '}';
    }
}
