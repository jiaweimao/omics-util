package omics.util.ms.peaklist.transform;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.filter.TopNList;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;

import java.util.Collections;
import java.util.List;


/**
 * Each peak intensity is divided by the summed up intensity of the k most intense peaks and multiplied by a
 * normalization factor (targetIntensity)
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Mar 2017, 6:41 PM
 */
public class NHighestPeaksNormalizer<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A> {

    private final double targetIntensity;
    protected TopNList<A> topNList;

    /**
     * Constructor.
     *
     * @param k               number of top peaks
     * @param targetIntensity target intensity after normalize
     */
    public NHighestPeaksNormalizer(int k, double targetIntensity) {
        topNList = new TopNList<>(k);
        this.targetIntensity = targetIntensity;
    }

    @Override
    public void start(int size) {
        super.start(size);
        topNList.clear();
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        super.processPeak(mz, intensity, annotations);

        topNList.add(mz, intensity, annotations);
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList,
            Int2ObjectMap<List<A>> annotationMap) {
        double sum = 0;
        topNList.resetCursor();

        while (topNList.next()) {

            sum += topNList.currIntensity();
        }

        double scale = targetIntensity / sum;

        for (int i = 0; i < mzList.size(); i++) {

            List<A> annotations;
            if (annotationMap.containsKey(i))
                annotations = annotationMap.get(i);
            else
                annotations = Collections.emptyList();
            sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i) * scale, annotations);
        }
    }

    @Override
    public String toString() {
        return "NHighestPeaksNormalizer {" + topNList.getNumberOfPeaksToRetain() + "," + targetIntensity + '}';
    }
}
