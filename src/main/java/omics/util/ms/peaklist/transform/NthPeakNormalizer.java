package omics.util.ms.peaklist.transform;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.filter.TopNList;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;

import java.util.Collections;
import java.util.List;


/**
 * Rescales the intensity to the nth most intense peak in the spectrum
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 10:11 PM
 */
public class NthPeakNormalizer<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A> {

    private final TopNList<A> topNList;

    /**
     * Creates a NthPeakNormalizer that scales the intensities to the
     * <code>nthPeak</code> most intense peak.
     *
     * @param nthPeak the peak to scale to
     */
    public NthPeakNormalizer(int nthPeak) {
        topNList = new TopNList<>(nthPeak);
    }

    @Override
    public void start(int size) {
        super.start(size);
        topNList.clear();
    }

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        super.processPeak(mz, intensity, annotations);

        topNList.add(mz, intensity, annotations);
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList,
            Int2ObjectMap<List<A>> annotationMap) {
        double baseHeight = topNList.getLastIntensity();

        for (int i = 0; i < mzList.size(); i++) {

            List<A> annotations;
            if (annotationMap.containsKey(i))
                annotations = annotationMap.get(i);
            else
                annotations = Collections.emptyList();

            sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i) / baseHeight, annotations);
        }
    }

    @Override
    public String toString() {
        return "NthPeakNormalizer {" + topNList.getNumberOfPeaksToRetain() + "}";
    }
}
