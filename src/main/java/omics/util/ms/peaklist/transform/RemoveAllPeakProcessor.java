package omics.util.ms.peaklist.transform;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;

import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2018, 7:11 PM
 */
public class RemoveAllPeakProcessor<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) { }
}
