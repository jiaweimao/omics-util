package omics.util.ms.peaklist.transform;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;

import java.util.List;


/**
 * Performs a square root transform on peak intensities.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 9:58 PM
 */
public class SqrtTransformer<A extends PeakAnnotation> extends AbstractPeakProcessor<A, A> {

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations) {
        double transformed = intensity <= 0 ? 0 : Math.sqrt(intensity);

        sink.processPeak(mz, transformed, annotations);
    }

    @Override
    public String toString() {
        return "SqrtTransformer";
    }
}
