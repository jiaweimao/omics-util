package omics.util.ms.util;

import omics.util.ms.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A title parser that delegates
 *
 * @author JiaweiMao
 * @version 1.1.0
 */
public class DelegatingRegexTitleParser implements TitleParser
{
    private final List<TitleParser> titleParsers = new ArrayList<>();
    private TitleParser defaultTitleParser;

    public DelegatingRegexTitleParser()
    {
        titleParsers.add(new RegexScanNumTitleParser(Pattern.compile("scan\\s+(\\d+).*")));
        titleParsers.add(new RegexScanNumTitleParser(Pattern.compile("[Ss]can=(\\d+)"))); // Thermo nativeID format
        titleParsers.add(new TitleParser1());
        titleParsers.add(new TitleParser2());
        titleParsers.add(new TitleParser3());
        titleParsers.add(new TitleParser4());

        defaultTitleParser = titleParsers.get(0);
    }

    @Override
    public boolean parse(String title, MsnSpectrum spectrum)
    {
        if (defaultTitleParser.parse(title, spectrum)) return true;

        for (TitleParser titleParser : titleParsers) {
            if (titleParser.parse(title, spectrum)) {

                defaultTitleParser = titleParser;
                return true;
            }
        }

        return false;
    }

    /**
     * title generate by msconvert with default setting.
     */
    private static class TitleParser1 implements TitleParser
    {
        private final Pattern pattern = Pattern.compile("^([^\\.]+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");

        @Override
        public boolean parse(String title, MsnSpectrum spectrum)
        {
            Matcher matcher = pattern.matcher(title);

            if (matcher.find()) {
                spectrum.addScanNumber(ScanNumber.discrete(Integer.parseInt(matcher.group(2))));
                return true;
            }
            return false;
        }
    }

    private static class TitleParser2 implements TitleParser
    {
        private final Pattern pattern = Pattern.compile("[Ss]can\\s[Nn]umber\\s?(\\d+)");

        @Override
        public boolean parse(String title, MsnSpectrum spectrum)
        {
            Matcher matcher = pattern.matcher(title);

            if (matcher.find()) {
                spectrum.addScanNumber(ScanNumber.discrete(Integer.parseInt(matcher.group(1))));
                return true;
            }
            return false;
        }
    }

    private static class TitleParser3 implements TitleParser
    {
        private final Pattern pattern = Pattern.compile("[Ss]can\\s*(\\d+)");

        @Override
        public boolean parse(String title, MsnSpectrum spectrum)
        {
            Matcher matcher = pattern.matcher(title);

            if (matcher.find()) {
                spectrum.addScanNumber(ScanNumber.discrete(Integer.parseInt(matcher.group(1))));
                return true;
            }

            return false;
        }
    }

    private static class TitleParser4 implements TitleParser
    {
        private final Pattern scanNumberPattern = Pattern.compile("survey:S(\\d+)");
        private final Pattern retentionTimePattern = Pattern.compile("start:(\\d+\\.?\\d+)[,\\s]end:(\\d+\\.?\\d+)");
        private final Pattern precursorIntensityPattern = Pattern.compile("parent_area:(\\d+\\.?\\d+)");

        @Override
        public boolean parse(String title, MsnSpectrum spectrum)
        {
            Matcher scanNumberMatcher = scanNumberPattern.matcher(title);
            Matcher retentionTimeMatcher = retentionTimePattern.matcher(title);
            Matcher precursorIntensityMatcher = precursorIntensityPattern.matcher(title);

            if (scanNumberMatcher.find() && retentionTimeMatcher.find() /*&& precursorIntensityMatcher.find()*/) {

                spectrum.addScanNumber(ScanNumber.discrete(Integer.parseInt(scanNumberMatcher.group(1))));

                double rtStart = Double.parseDouble(retentionTimeMatcher.group(1));
                double rtEnd = Double.parseDouble(retentionTimeMatcher.group(2));

                if (rtStart == rtEnd)
                    spectrum.addRetentionTime(RetentionTime.discrete(rtStart));
                else
                    spectrum.addRetentionTime(RetentionTime.interval(rtStart, rtEnd, TimeUnit.SECOND));

                spectrum.getPrecursor().setIntensity(Double.parseDouble(precursorIntensityMatcher.group(1)));
                return true;
            } else {

                return false;
            }
        }
    }
}
