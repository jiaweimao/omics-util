package omics.util.ms.util;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.ScanNumber;
import omics.util.ms.TitleParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;


/**
 * Implementation of a title parser that uses a regex pattern to extract the scan number from the title
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 09 Oct 2017, 7:56 AM
 */
public class RegexScanNumTitleParser implements TitleParser
{
    private static final Pattern CAPT_GROUP_PATTERN = Pattern.compile("(?<!\\\\)\\([^)]+\\)");

    private final Pattern pat;

    public RegexScanNumTitleParser(Pattern pat)
    {
        requireNonNull(pat);

        int count = countCapturingGroup(pat);

        checkArgument(count != 0, "regexp " + pat.pattern() + " misses to capture scan number!");
        checkArgument(count == 1, "regexp " + pat.pattern() + " contains too many capture groups " +
                "(need only one to get scan number)!");

        this.pat = pat;
    }

    private static int countCapturingGroup(Pattern pat)
    {
        Matcher matcher = CAPT_GROUP_PATTERN.matcher(pat.pattern());

        int count = 0;
        while (matcher.find()) {

            count++;
        }

        return count;
    }

    @Override
    public boolean parse(String title, MsnSpectrum spectrum)
    {
        Matcher matcher = pat.matcher(title);
        if (matcher.find()) {
            spectrum.addScanNumber(ScanNumber.discrete(Integer.parseInt(matcher.group(1))));
            return true;
        }
        return false;
    }
}
