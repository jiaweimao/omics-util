package omics.util.ms.util;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.ScanNumberList;
import omics.util.ms.TitleParser;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.ServiceLoader;

/**
 * Extract scan number from spectrum title.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 15 Aug 2019, 8:51 AM
 */
public class ScanTitleParser
{
    private List<TitleParser> parserList;
    private TitleParser currentParser;

    public ScanTitleParser()
    {
        parserList = new ArrayList<>();

        ServiceLoader<TitleParser> load = ServiceLoader.load(TitleParser.class);
        for (TitleParser parser : load) {
            parserList.add(parser);
        }

        if (parserList.isEmpty()) {
            throw new IllegalStateException("No TitleParser implementation class found: org.openide.util.Lookup has " +
                    "not properly worked (hint: check that Annotation Compiler Processing is enable)");
        } else {
            currentParser = parserList.get(0);
        }
    }

    public boolean parse(String title, MsnSpectrum spectrum)
    {
        boolean parsed = currentParser.parse(title, spectrum);
        if (!parsed) {
            for (TitleParser titleParser : parserList) {
                if (titleParser.parse(title, spectrum)) {
                    parsed = true;
                    currentParser = titleParser;
                    break;
                }
            }
        }
        return parsed;
    }

    /**
     * parse the title and return the scan number.
     *
     * @param title spectrum title
     * @return scan number of -1 if parse failed.
     */
    public OptionalInt getScan(String title)
    {
        MsnSpectrum spectrum = new MsnSpectrum();
        parse(title, spectrum);

        ScanNumberList scanNumbers = spectrum.getScanNumberList();
        if (scanNumbers.isEmpty())
            return OptionalInt.empty();

        return OptionalInt.of(scanNumbers.getFirst().getValue());
    }
}
