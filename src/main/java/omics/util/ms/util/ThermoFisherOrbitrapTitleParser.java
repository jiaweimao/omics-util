package omics.util.ms.util;

import omics.util.ms.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Title parse for data obtained from Savitski et. al. (2011). Confident phosphorylation site localization using the
 * Mascot Delta Score. Molecular &amp; cellular proteomics : MCP
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Oct 2017, 9:22 PM
 */
public class ThermoFisherOrbitrapTitleParser implements TitleParser
{
    private final Pattern pattern = Pattern.compile("msmsid:F(\\d+),quan:(\\d+\\.?\\d*),start:(\\d+\\.?\\d*),end:" +
            "(\\d+\\.?\\d*),survey:S(\\d+),parent:(\\d+\\.?\\d*),parent_area:(\\d+\\.?\\d*),Qstart:(\\d+\\.?\\d*)," +
            "Qend:(\\d+\\.?\\d*),AnalTime:(\\d+),Activation:(\\w*)");

    @Override
    public boolean parse(String title, MsnSpectrum spectrum)
    {
        Matcher matcher = pattern.matcher(title);
        if (matcher.matches()) {

            spectrum.addScanNumber(ScanNumber.discrete(Integer.parseInt(matcher.group(5))));
            spectrum.setDissociation(Dissociation.fromName(matcher.group(11)));

            double timeFrom = Double.parseDouble(matcher.group(3));
            double timeTo = Double.parseDouble(matcher.group(4));

            if (timeFrom == timeTo) {
                spectrum.addRetentionTime(RetentionTime.discrete(timeFrom, TimeUnit.HOUR));
            } else {
                spectrum.addRetentionTime(RetentionTime.interval(timeFrom, timeTo, TimeUnit.HOUR));
            }
            return true;
        }

        return false;
    }
}
