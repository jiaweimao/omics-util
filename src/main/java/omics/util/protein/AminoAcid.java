package omics.util.protein;

import omics.util.chem.Composition;
import omics.util.chem.Mass;
import omics.util.chem.NumericMass;
import omics.util.chem.Symbol;

import java.util.HashMap;
import java.util.Objects;


/**
 * Class represents the 21 Proteinogenic amino acids and 3 B, Z and X to
 * represent amino acids for which there is ambiguity.
 * <p>
 * All amino acid mass comes from unimod: http://www.unimod.org/
 * <p>
 * The molecular mass and mass can be retrieved for amino acids that are
 * unambiguous. Both the mass and mass are for amino acids that are
 * missing the n-term H and c-term OH.
 *
 * @author JiaweiMao
 * @version 2.6.0
 * @since 15 Mar 2017, 8:44 PM
 */
public class AminoAcid extends Mass implements Symbol
{
    public static final AminoAcid A = new AminoAcid('A', "Ala", "Alanine", "C3H5NO", new String[]{"GCT", "GCC", "GCA", "GCG"});
    public static final AminoAcid C = new AminoAcid('C', "Cys", "Cysteine", "C3H5NOS", new String[]{"TGT", "TGC"});
    public static final AminoAcid D = new AminoAcid('D', "Asp", "Aspartic acid", "C4H5NO3", new String[]{"GAT", "GAC"});
    public static final AminoAcid E = new AminoAcid('E', "Glu", "Glutamic acid", "C5H7NO3", new String[]{"GAA", "GAG"});
    public static final AminoAcid F = new AminoAcid('F', "Phe", "Phenylalanine", "C9H9NO", new String[]{"TTT", "TTC"});
    public static final AminoAcid G = new AminoAcid('G', "Gly", "Glycine", "C2H3NO", new String[]{"GGT", "GGC", "GGA", "GGG"});
    public static final AminoAcid H = new AminoAcid('H', "His", "Histidine", "C6H7N3O", new String[]{"CAT", "CAC"});
    public static final AminoAcid I = new AminoAcid('I', "Ile", "Isoleucine", "C6H11NO", new String[]{"ATT", "ATC", "ATA"});
    public static final AminoAcid K = new AminoAcid('K', "Lys", "Lysine", "C6H12N2O", new String[]{"AAA", "AAG"});
    public static final AminoAcid L = new AminoAcid('L', "Leu", "Leucine", "C6H11ON", new String[]{"TTA", "TTG", "CTT", "CTC", "CTA", "CTG"});
    public static final AminoAcid M = new AminoAcid('M', "Met", "Methionine", "C5H9NOS", new String[]{"ATG"});
    public static final AminoAcid N = new AminoAcid('N', "Asn", "Asparagine", "C4H6N2O2", new String[]{"AAT", "AAC"});
    public static final AminoAcid P = new AminoAcid('P', "Pro", "Proline", "C5H7NO", new String[]{"CCT", "CCC", "CCA", "CCG"});
    public static final AminoAcid Q = new AminoAcid('Q', "Gln", "Glutamine", "C5H8N2O2", new String[]{"CAA", "CAG"});
    public static final AminoAcid R = new AminoAcid('R', "Arg", "Arginine", "C6H12N4O", new String[]{"CGT", "CGC", "CGA", "CGG", "AGA", "AGG"});
    public static final AminoAcid S = new AminoAcid('S', "Ser", "Serine", "C3H5NO2", new String[]{"AGT", "AGC"});
    public static final AminoAcid T = new AminoAcid('T', "Thr", "Threonine", "C4H7NO2", new String[]{"ACT", "ACC", "ACA", "ACG"});
    public static final AminoAcid V = new AminoAcid('V', "Val", "Valine", "C5H9NO", new String[]{"GTT", "GTC", "GTA", "GTG"});
    public static final AminoAcid W = new AminoAcid('W', "Trp", "Tryptophan", "C11H10N2O", new String[]{"TGG"});
    public static final AminoAcid Y = new AminoAcid('Y', "Tyr", "Tyrosine", "C9H9NO2", new String[]{"TAT", "TAC"});

    public static final AminoAcid N_TERM = new AminoAcid('_', "N-term", "N-term", "H");
    public static final AminoAcid C_TERM = new AminoAcid('_', "C-term", "C-term", "HO");

    public static final AminoAcid J = new AminoAcid('J', "I/L", "Isoleucine or Leucine", "C6H11ON");
    public static final AminoAcid O = new AminoAcid('O', "Pyl", "Pyrrolysine", "C12H19O2N3", new String[]{"TAG"});
    public static final AminoAcid U = new AminoAcid('U', "Sec", "Selenocysteine", "C3H5NOSe", new String[]{"TGA"});
    /**
     * Z is an average between E and Q
     */
    public static final AminoAcid Z = new AminoAcid('Z', "Glx", "Glu or Gln", new NumericMass(128.5505855));

    /**
     * B is an average between N and D
     */
    public static final AminoAcid B = new AminoAcid('B', "Asx", "Asparagine or aspartic acid", new NumericMass(114.534935));

    public static final AminoAcid X = new AminoAcid('X', "Xaa", "X", new NumericMass(110));

    private static final HashMap<Character, AminoAcid> stdMap = new HashMap<>(20);
    private static final AminoAcid[] stdAA;

    static {
        stdAA = new AminoAcid[]{A, C, D, E, F, G, H, I, K, L, M, N, P, Q, R, S, T, V, W, Y};
        for (AminoAcid aa : stdAA) {
            stdMap.put(aa.oneLetterCode, aa);
        }
    }

    /**
     * Create a custom amino acid.
     *
     * @param residue     amino acid residue
     * @param name        amino acid name
     * @param composition amino acid mass
     */
    public static AminoAcid getAminoAcid(char residue, String name, Composition composition)
    {
        AminoAcid stdAA = getAminoAcid(residue);
        if (stdAA != null && stdAA.getComposition().equals(composition))
            return stdAA;

        return new AminoAcid(residue, name, composition);
    }

    private final char oneLetterCode;
    private final String threeLetterCode;
    private final String name;
    private final Mass composition;
    private final String[] standardGeneticCode;
    private final double mass;
    private final int nominalMass;

    /**
     * Constructor.
     *
     * @param oneLetterCode       one letter code
     * @param threeLetterCode     three letter code
     * @param name                amino acid name
     * @param composition         amino acid composition
     * @param standardGeneticCode genetic code for amino acid.
     */
    public AminoAcid(char oneLetterCode, String threeLetterCode, String name, Mass composition, String[] standardGeneticCode)
    {
        this.oneLetterCode = oneLetterCode;
        this.threeLetterCode = threeLetterCode;
        this.name = name;
        this.composition = composition;
        this.standardGeneticCode = standardGeneticCode;
        this.mass = composition.getMolecularMass();
        this.nominalMass = composition.getNominalMass();
    }

    public AminoAcid(char oneLetterCode, String threeLetterCode, String name, Mass composition)
    {
        this(oneLetterCode, threeLetterCode, name, composition, null);
    }

    /**
     * Constructor.
     *
     * @param oneLetterCode a unique char for this amino acid
     * @param name          amino acid name
     * @param composition   amino acid amss
     */
    public AminoAcid(char oneLetterCode, String name, Mass composition)
    {
        this(oneLetterCode, null, name, composition);
    }

    /**
     * Constructor.
     *
     * @param oneLetterCode       one letter code
     * @param threeLetterCode     three letter code
     * @param name                amino acid name
     * @param formula             amino acid composition formula
     * @param standardGeneticCode genetic code for amino acid.
     */
    public AminoAcid(char oneLetterCode, String threeLetterCode, String name, String formula, String[] standardGeneticCode)
    {
        this(oneLetterCode, threeLetterCode, name, Composition.parseComposition(formula), standardGeneticCode);
    }

    /**
     * Constructor.
     *
     * @param oneLetterCode   one letter code
     * @param threeLetterCode three letter code
     * @param name            amino acid name
     * @param formula         amino acid composition
     */
    public AminoAcid(char oneLetterCode, String threeLetterCode, String name, String formula)
    {
        this(oneLetterCode, threeLetterCode, name, formula, null);
    }

    /**
     * Returns the amino acid corresponding to the single letter code given, null if not
     * implemented.
     *
     * @param aa the single letter code of the amino acid, from 'A' to 'Z'
     * @return the corresponding amino acid, standard amino acids or ambiguous amino acid.
     */
    public static AminoAcid getAminoAcid(char aa)
    {
        switch (aa) {
            case 'A':
                return AminoAcid.A;
            case 'C':
                return AminoAcid.C;
            case 'D':
                return AminoAcid.D;
            case 'E':
                return AminoAcid.E;
            case 'F':
                return AminoAcid.F;
            case 'G':
                return AminoAcid.G;
            case 'H':
                return AminoAcid.H;
            case 'I':
                return AminoAcid.I;
            case 'K':
                return AminoAcid.K;
            case 'L':
                return AminoAcid.L;
            case 'M':
                return AminoAcid.M;
            case 'N':
                return AminoAcid.N;
            case 'P':
                return AminoAcid.P;
            case 'Q':
                return AminoAcid.Q;
            case 'R':
                return AminoAcid.R;
            case 'S':
                return AminoAcid.S;
            case 'T':
                return AminoAcid.T;
            case 'V':
                return AminoAcid.V;
            case 'W':
                return AminoAcid.W;
            case 'Y':
                return AminoAcid.Y;
            case 'B':
                return AminoAcid.B;
            case 'Z':
                return AminoAcid.Z;
            case 'X':
                return AminoAcid.X;
            case 'U':
                return AminoAcid.U;
            case 'J':
                return AminoAcid.J;
            case 'O':
                return AminoAcid.O;
            default:
                throw new IllegalArgumentException("No amino acid found for letter " + aa + ".");
        }
    }

    /**
     * @param aa amino acid single letter code.
     * @return true if the amino acid of given char is a standard amino acid.
     */
    public static boolean isStandardAminoAcid(char aa)
    {
        return stdMap.containsKey(aa);
    }

    /**
     * Return the amino acid of given name
     *
     * @param name amino acid name, or N-term/C-term
     */
    public static AminoAcid getAminoAcid(String name)
    {
        switch (name) {
            case "N-term":
                return AminoAcid.N_TERM;
            case "C-term":
                return AminoAcid.C_TERM;
            default:
                return AminoAcid.getAminoAcid(name.charAt(0));
        }
    }


    /**
     * @param geneticCode generic code
     * @return the AminoAcid of given genetic code
     */
    public static AminoAcid getAminoAcid4GeneticCode(String geneticCode)
    {
        if (geneticCode.equals("TTT") || geneticCode.equals("TTC")) {
            return F;
        } else if (geneticCode.equals("TTA") || geneticCode.equals("TTG") || geneticCode.equals("CTT") || geneticCode.equals("CTC") || geneticCode.equals("CTA") || geneticCode.equals("CTG")) {
            return L;
        } else if (geneticCode.equals("ATT") || geneticCode.equals("ATC") || geneticCode.equals("ATA")) {
            return I;
        } else if (geneticCode.equals("ATG")) {
            return M;
        } else if (geneticCode.startsWith("GT")) {
            return V;
        } else if (geneticCode.startsWith("TC")) {
            return S;
        } else if (geneticCode.startsWith("CC")) {
            return P;
        } else if (geneticCode.startsWith("AC")) {
            return T;
        } else if (geneticCode.startsWith("GC")) {
            return A;
        } else if (geneticCode.equals("TAT") || geneticCode.equals("TAC")) {
            return Y;
        } else if (geneticCode.equals("CAT") || geneticCode.equals("CAC")) {
            return H;
        } else if (geneticCode.equals("CAA") || geneticCode.equals("CAG")) {
            return Q;
        } else if (geneticCode.equals("AAT") || geneticCode.equals("AAC")) {
            return N;
        } else if (geneticCode.equals("AAA") || geneticCode.equals("AAG")) {
            return K;
        } else if (geneticCode.equals("GAT") || geneticCode.equals("GAC")) {
            return D;
        } else if (geneticCode.equals("GAA") || geneticCode.equals("GAG")) {
            return E;
        } else if (geneticCode.equals("TGT") || geneticCode.equals("TGC")) {
            return C;
        } else if (geneticCode.equals("TGG")) {
            return W;
        } else if (geneticCode.startsWith("CG")) {
            return R;
        } else if (geneticCode.equals("AGT") || geneticCode.equals("AGC")) {
            return S;
        } else if (geneticCode.equals("AGA") || geneticCode.equals("AGG")) {
            return R;
        } else if (geneticCode.startsWith("GG")) {
            return G;
        } else if (geneticCode.equals("TAG")) {
            return O;
        } else if (geneticCode.equals("TGA")) {
            return U;
        }
        return null;
    }

    /**
     * @return all standard amino acid
     */
    public static AminoAcid[] getStandardAminoAcids()
    {
        return stdAA;
    }

    /**
     * @return all amino acids
     */
    public static AminoAcid[] getAllAminoAcids()
    {
        return new AminoAcid[]{
                A, R, N, D, C, E, Q, G, H, I,
                L, K, M, F, P, S, T, W, Y, V,
                B, X, Z, U, O, J};
    }

    private double probability = 0.05;

    /**
     * @return the probability of the amino acid in a database.
     */
    public double getProbability()
    {
        return probability;
    }

    public void setProbability(double probability)
    {
        this.probability = probability;
    }

    /**
     * create an amino acid which is this amino acid with fixed modification
     *
     * @param peptideMod a {@link PeptideMod}
     */
    public AminoAcid getFixedModAA(PeptideMod peptideMod)
    {
        Composition composition = peptideMod.getComposition();
        if (composition.isEmpty()) {
            return this;
        }

        return new AminoAcid(oneLetterCode, name, new Composition(getComposition(), composition));
    }

    /**
     * @return three letter short code of the amino acid.
     */
    public String getShortName()
    {
        return threeLetterCode;
    }

    /**
     * @return amino acid full name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Return the average mass, for {@link Composition}, the average mass is calculated based on atoms;
     * for {@link omics.util.chem.NumericMass}, just return the Mass.
     *
     * @return the average mass
     */
    public double getAverageMass()
    {
        if (composition instanceof Composition)
            return ((Composition) composition).getAverageMass();

        return composition.getMolecularMass();
    }

    /**
     * Returns the mass of this amino acid when it is part of a polymer. When an
     * amino acid is part of a polymer the n-term 'H' and c-term 'OH' are
     * removed.
     *
     * @return the mass of this amino acid residue (-H2O)
     */
    public double getMolecularMass()
    {
        return mass;
    }

    /**
     * @return {@link Mass} object of this AminoAcid.
     */
    public Mass getMass()
    {
        return composition;
    }

    /**
     * @return the Composition of this amino acid.
     */
    public Composition getComposition()
    {
        if (composition instanceof Composition)
            return (Composition) composition;
        else
            throw new NullPointerException("This amino acid does not have a defined composition.");
    }

    /**
     * @return single letter code of this amino acid.
     */
    public char getOneLetterCode()
    {
        return oneLetterCode;
    }

    /**
     * @return the standard amino acid single letter code.
     */
    public char getStandardResidue()
    {
        return oneLetterCode;
    }

    @Override
    public String getSymbol()
    {
        return String.valueOf(oneLetterCode);
    }

    /**
     * @return the standard genetic triplets of this amino acid, null if not present.
     */
    public String[] getGeneticCode()
    {
        return standardGeneticCode;
    }

    /**
     * @return true if this amino acid is a standard amino acid.
     */
    public boolean isUnambiguous()
    {
        return stdMap.containsKey(oneLetterCode);
    }

    @Override
    public String getFormula()
    {
        return composition.getFormula();
    }

    @Override
    public double getMassDefect()
    {
        return composition.getMassDefect();
    }

    @Override
    public int getNominalMass()
    {
        return nominalMass;
    }

    /**
     * @return true if this amino acid is modified by a variable modification.
     */
    public boolean isModified()
    {
        return false;
    }

    /**
     * cast this amino acid to {@link ModifiedAminoAcid} if it is a {@link ModifiedAminoAcid} instance.
     */
    public ModifiedAminoAcid asModifiedAminoAcid()
    {
        if (this instanceof ModifiedAminoAcid) {
            return (ModifiedAminoAcid) this;
        }
        throw new UnsupportedOperationException("This amino acid is not ModAA instance.");
    }

    /**
     * @return true if the amino acid is modified by amino acid specific modification.
     */
    public boolean isResidueModified()
    {
        return false;
    }

    /**
     * @return true if the amino acid is modified by terminal specific modification.
     */
    public boolean isTerminalModified()
    {
        return false;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AminoAcid aminoAcid = (AminoAcid) o;
        return oneLetterCode == aminoAcid.oneLetterCode &&
                Objects.equals(composition, aminoAcid.composition);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(oneLetterCode, composition);
    }

    @Override
    public String toString()
    {
        return String.valueOf(oneLetterCode);
    }
}
