package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleComparators;
import it.unimi.dsi.fastutil.doubles.DoubleList;
import omics.util.OmicsObject;
import omics.util.chem.Composition;
import omics.util.chem.PeriodicTable;
import omics.util.chem.Sequence;
import omics.util.chem.Weighable;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.ModificationList;
import omics.util.protein.mod.Pos;
import omics.util.protein.ms.FragmentType;
import omics.util.protein.ms.PeptideIon;
import omics.util.utils.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.*;

/**
 * Amino acid array.
 *
 * @author JiaweiMao
 * @version 1.2.2
 * @since 03 Jul 2019, 2:31 PM
 */
public class AminoAcidArray extends OmicsObject implements Sequence<AminoAcid>, Weighable
{
    /**
     * convert AminoAcidArray string to peptide
     *
     * @param seq   a amino acid array string.
     * @param aaSet {@link AminoAcidSet} setting of the sequence
     * @return {@link Peptide}
     */
    public static Peptide getPeptide(String seq, AminoAcidSet aaSet)
    {
        List<AminoAcid> residues = new ArrayList<>(seq.length());
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

        for (int i = 0; i < seq.length(); i++) {
            char aa = seq.charAt(i);
            AminoAcid aminoAcid = aaSet.getAminoAcid(aa);

            if (aminoAcid instanceof ModifiedAminoAcid) {
                ModifiedAminoAcid modAA = aminoAcid.asModifiedAminoAcid();
                AminoAcid stdAA = modAA.getTargetAminoAcid();
                if (stdAA.getComposition().equals(AminoAcid.getAminoAcid(stdAA.getStandardResidue()).getComposition())) {
                    residues.add(stdAA);
                } else { // amino acid with fix modification.
                    Pair<Modification, Pos> fixedMod = aaSet.getFixedModification(stdAA.getOneLetterCode());
                    residues.add(AminoAcid.getAminoAcid(stdAA.getStandardResidue()));
                    Modification mod = fixedMod.getFirst();
                    Pos pos = fixedMod.getSecond();
                    if (i == 0 && pos.isNTerm()) {
                        termModMap.put(ModAttachment.N_TERM, mod);
                    } else if (i == seq.length() - 1 && pos.isCTerm()) {
                        termModMap.put(ModAttachment.C_TERM, mod);
                    } else {
                        sideChainModMap.put(i, mod);
                    }
                }

                PeptideMod peptideMod = modAA.getPeptideMod();
                Modification aMod = peptideMod.getModification();
                Pos pos = peptideMod.getPosition();
                if (pos.isNTerm() && i == 0) {
                    termModMap.put(ModAttachment.N_TERM, aMod);
                } else if (pos.isCTerm() && i == seq.length() - 1) {
                    termModMap.put(ModAttachment.C_TERM, aMod);
                } else {
                    sideChainModMap.put(i, aMod);
                }
            } else {
                if (aminoAcid.getComposition().equals(AminoAcid.getAminoAcid(aminoAcid.getStandardResidue()).getComposition())) {
                    residues.add(aminoAcid);
                } else { // fixed modified amino acid again
                    Pair<Modification, Pos> fixedMod = aaSet.getFixedModification(aminoAcid.getOneLetterCode());
                    residues.add(AminoAcid.getAminoAcid(aminoAcid.getStandardResidue()));
                    Modification mod = fixedMod.getFirst();
                    Pos pos = fixedMod.getSecond();
                    if (i == 0 && pos.isNTerm()) {
                        termModMap.put(ModAttachment.N_TERM, mod);
                    } else if (i == seq.length() - 1 && pos.isCTerm()) {
                        termModMap.put(ModAttachment.C_TERM, mod);
                    } else {
                        sideChainModMap.put(i, mod);
                    }
                }
            }
        }

        return new Peptide(residues, sideChainModMap, termModMap);
    }


    private final AminoAcid[] aminoAcids;
    private Composition composition;
    private boolean isModified;

    /**
     * Constructs an AminoAcidArray by copying the residues.
     *
     * @param residues the residues, this array cannot be empty
     */
    public AminoAcidArray(AminoAcid[] residues)
    {
        requireNonNull(residues);

        int size = residues.length;
        checkState(size > 0, "Peptides cannot be empty");

        aminoAcids = new AminoAcid[size];
        System.arraycopy(residues, 0, aminoAcids, 0, residues.length);
        init();
    }

    /**
     * Constructor that copies the residues from src. The new
     * AminoAcidArray begins at the specified <code>fromInclusive</code> and
     * extends to the residue at index <code>toExclusive - 1</code>.
     * <p/>
     * Thus the length of the new AminoAcidArray is <code>toExclusive-fromInclusive</code>.
     *
     * @param src           the AminoAcidSequence from which to copy the sequence and modifications
     * @param fromInclusive the beginning index, inclusive.
     * @param toExclusive   the ending index, exclusive.
     */
    public AminoAcidArray(final AminoAcidArray src, final int fromInclusive, final int toExclusive)
    {
        checkNotNull(src);

        final int size = toExclusive - fromInclusive;
        checkState(size > 0, "Peptides cannot be empty");
        aminoAcids = new AminoAcid[size];

        checkElementIndex(fromInclusive, src.size());
        checkPositionIndex(toExclusive, src.size());

        System.arraycopy(src.aminoAcids, fromInclusive, aminoAcids, 0, aminoAcids.length);
        init();
    }

    /**
     * Construct an AminoAcidSequence by copying the residues
     *
     * @param firstResidue the first residue
     * @param residues     the rest of the residues
     */
    public AminoAcidArray(AminoAcid firstResidue, AminoAcid... residues)
    {
        aminoAcids = new AminoAcid[residues.length + 1];
        aminoAcids[0] = firstResidue;
        System.arraycopy(residues, 0, aminoAcids, 1, residues.length);
        init();
    }

    /**
     * Copy constructor.
     */
    public AminoAcidArray(AminoAcidArray aminoAcidArray)
    {
        this.aminoAcids = Arrays.copyOf(aminoAcidArray.aminoAcids, aminoAcidArray.size());
        init();
    }

    /**
     * Constructor.
     *
     * @param seq   peptide sequence, every char represents an amino acid or a modified amino acid.
     * @param aaSet {@link AminoAcidSet}
     */
    public AminoAcidArray(String seq, AminoAcidSet aaSet)
    {
        checkNotNull(seq);

        aminoAcids = new AminoAcid[seq.length()];
        for (int i = 0; i < seq.length(); i++) {
            char c = seq.charAt(i);
            AminoAcid aa = aaSet.getAminoAcid(c);
            checkNotNull(aa);

            aminoAcids[i] = aa;
        }
        init();
    }

    /**
     * Create an {@link AminoAcidArray} from {@link Peptide} instance.
     *
     * @param peptide a {@link Peptide} instance.
     * @param aaSet   {@link AminoAcidSet}
     */
    public AminoAcidArray(Peptide peptide, AminoAcidSet aaSet)
    {
        aminoAcids = new AminoAcid[peptide.size()];

        for (int i = 0; i < peptide.size(); i++) {
            AminoAcid aminoAcid = peptide.getSymbol(i);
            ModificationList mods = peptide.getModificationsAt(i, ModAttachment.all);
            if (mods.isEmpty()) {
                aminoAcids[i] = aminoAcid;
            } else {
                AminoAcid[] aas;
                if (i == 0) {
                    aas = aaSet.getAminoAcids(Pos.PROTEIN_N_TERM, aminoAcid.getOneLetterCode());
                } else if (i == (peptide.size() - 1)) {
                    aas = aaSet.getAminoAcids(Pos.PROTEIN_C_TERM, aminoAcid.getOneLetterCode());
                } else {
                    aas = aaSet.getAminoAcids(Pos.ANY_WHERE, aminoAcid.getOneLetterCode());
                }

                boolean match = false;
                double modMass = mods.getMolecularMass() + aminoAcid.getMolecularMass();
                for (AminoAcid aa : aas) {
                    if (Math.abs(modMass - aa.getMolecularMass()) < 0.01) {
                        aminoAcids[i] = aa;
                        match = true;
                        break;
                    }
                }
                if (!match) {
                    throw new IllegalArgumentException("No modified amino acid for " + aminoAcid + " and mass " + modMass);
                }
            }
        }

        init();
    }

    private void init()
    {
        isModified = false;
        Composition.Builder builder = new Composition.Builder();
        for (AminoAcid aminoAcid : aminoAcids) {
            builder.add(aminoAcid.getComposition());
            if (aminoAcid.isModified()) {
                isModified = true;
            }
        }
        this.composition = builder.build();
    }

    /**
     * @return true if this peptide contains variable modifications.
     */
    public boolean isModified()
    {
        return isModified;
    }

    /**
     * @return the {@link Composition} of this {@link AminoAcidArray}
     */
    public Composition getComposition()
    {
        return composition;
    }

    /**
     * @return the nominal mass of this amino acid array, which is the sum of nominal masses of all amino acids.
     */
    public int getNominalMass()
    {
        return composition.getNominalMass();
    }

    /**
     * @return the mass of peptide with this amino acid array, which is the sum of residues' mass and H2O.
     */
    public double getPeptideMass()
    {
        return composition.getMolecularMass() + PeriodicTable.H2O_MASS;
    }

    @Override
    public AminoAcid getSymbol(int index)
    {
        return aminoAcids[index];
    }

    /**
     * Return the Peptide mass array, the length is peptide.size() - 1, which means the last residue is not included.
     *
     * @param fragmentType {@link FragmentType}
     */
    public double[] getMassArray(FragmentType fragmentType)
    {
        double[] array;
        double mass = 0.0;
        if (fragmentType == FragmentType.FORWARD) {
            array = new double[aminoAcids.length - 1];
            for (int i = 0; i < array.length; i++) {
                mass += getSymbol(i).getMolecularMass();
                array[i] = mass;
            }
        } else if (fragmentType == FragmentType.REVERSE) {
            array = new double[aminoAcids.length - 1];
            for (int i = 0; i < array.length; i++) {
                mass += getSymbol(array.length - i).getMolecularMass();
                array[i] = mass;
            }
        } else if (fragmentType == FragmentType.INTERNAL) {
            DoubleList list = new DoubleArrayList();
            for (int i = 1; i < aminoAcids.length - 2; i++) {
                for (int j = i + 2; j < aminoAcids.length; j++) {
                    double m = 0;
                    for (int k = i; k < j; k++) {
                        m += aminoAcids[k].getMolecularMass();
                    }
                    list.add(m);
                }
            }
            list.sort(DoubleComparators.NATURAL_COMPARATOR);
            array = list.toDoubleArray();
        } else {
            throw new IllegalArgumentException("Not support " + fragmentType);
        }
        return array;
    }

    /**
     * Return the Peptide mass array, the length is peptide.size() - 1, which means the last residue is not included.
     *
     * @param fragmentType {@link FragmentType}
     */
    public int[] getNominalMassArray(FragmentType fragmentType)
    {
        int[] array = new int[aminoAcids.length - 1];
        int mass = 0;
        if (fragmentType == FragmentType.FORWARD) {
            for (int i = 0; i < array.length; i++) {
                mass += getSymbol(i).getNominalMass();
                array[i] = mass;
            }
        } else {
            for (int i = 0; i < array.length; i++) {
                mass += getSymbol(array.length - i).getNominalMass();
                array[i] = mass;
            }
        }
        return array;
    }

    /**
     * Return the mz values of this peptide of given {@link PeptideIon}, the length is size() - 1,
     * the last residue is not included.
     *
     * @param peptideIon a {@link PeptideIon} instance
     */
    public double[] getMzArray(PeptideIon peptideIon)
    {
        double[] array = new double[aminoAcids.length - 1];
        double mass = 0f;
        if (peptideIon.isForwardIon()) {
            for (int i = 0; i < array.length; i++) {
                mass += getSymbol(i).getMolecularMass();
                array[i] = peptideIon.calcMz(mass);
            }
        } else {
            for (int i = 0; i < array.length; i++) {
                mass += getSymbol(array.length - i).getMolecularMass();
                array[i] = peptideIon.calcMz(mass);
            }
        }
        return array;
    }

    @Override
    public String toSymbolString()
    {
        StringBuilder buff = new StringBuilder();
        for (AminoAcid aa : aminoAcids)
            buff.append(aa.getOneLetterCode());

        return buff.toString();
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            sb.append(aminoAcids[i]);
        }
        return sb.toString();
    }

    @Override
    public int size()
    {
        return aminoAcids.length;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AminoAcidArray that = (AminoAcidArray) o;
        return Arrays.equals(aminoAcids, that.aminoAcids);
    }

    private volatile int hash;

    @Override
    public int hashCode()
    {
        int result = hash;
        if (result == 0) {
            result = Arrays.hashCode(aminoAcids);
            hash = result;
        }

        return result;
    }

    /**
     * Return the mass of this {@link AminoAcidArray}, which is the sum of all amino acid residues' mass.
     */
    @Override
    public double getMolecularMass()
    {
        return composition.getMolecularMass();
    }
}
