package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import omics.util.chem.Composition;
import omics.util.chem.Mass;
import omics.util.chem.Sequence;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.ModificationList;
import omics.util.utils.Counter;

import java.util.*;

import static omics.util.utils.ObjectUtils.*;

/**
 * This class holds the amino acid sequence along with the modifications to the amino acids.
 * <p/>
 * Modifications can be attached to the side chains of any amino acid residue or
 * the sequence n-term or c-term.
 * <p/>
 * Each modification site can have 0 or more Modifications.
 * All site and index in this class is 0-based.
 *
 * @author JiaweiMao
 * @version 1.2.1
 * @since 16 Mar 2017, 4:51 PM
 */
public abstract class AminoAcidSequence implements Sequence<AminoAcid>
{
    private final AminoAcid[] aminoAcids;
    private final boolean hasAmbiguousAA;
    private ListMultimap<Integer, Modification> sideChainModMap;
    private ListMultimap<ModAttachment, Modification> termModMap;
    private int hash = 0;

    /**
     * Construct an AminoAcidSequence by copying the residues
     *
     * @param firstResidue the first residue
     * @param residues     the rest of the residues
     */
    AminoAcidSequence(AminoAcid firstResidue, AminoAcid... residues)
    {
        aminoAcids = new AminoAcid[residues.length + 1];
        aminoAcids[0] = firstResidue;
        System.arraycopy(residues, 0, aminoAcids, 1, residues.length);

        hasAmbiguousAA = hasAmbiguousAA();
    }

    /**
     * Constructs an AminoAcidSequence by copying the residues.
     *
     * @param residues the residues, this array cannot be empty
     */
    AminoAcidSequence(AminoAcid[] residues)
    {
        checkNotNull(residues);
        int size = residues.length;
        checkState(size > 0, "Peptides cannot be empty");

        aminoAcids = new AminoAcid[size];
        System.arraycopy(residues, 0, aminoAcids, 0, residues.length);

        hasAmbiguousAA = hasAmbiguousAA();
    }

    /**
     * Constructor that copies the residues and modifications from src. The new
     * AminoAcidSequence begins at the specified <code>fromInclusive</code> and
     * extends to the residue at index <code>toExclusive - 1</code>.
     * <p/>
     * Thus the length of the new AminoAcidSequence is <code>toExclusive-fromInclusive</code>.
     *
     * @param src           the AminoAcidSequence from which to copy the sequence and modifications
     * @param fromInclusive the beginning index, inclusive.
     * @param toExclusive   the ending index, exclusive.
     */
    AminoAcidSequence(final AminoAcidSequence src, final int fromInclusive, final int toExclusive)
    {
        checkNotNull(src);
        final int size = toExclusive - fromInclusive;
        checkState(size > 0, "Peptides cannot be empty");
        aminoAcids = new AminoAcid[size];

        checkElementIndex(fromInclusive, src.size());
        checkPositionIndex(toExclusive, src.size());

        System.arraycopy(src.aminoAcids, fromInclusive, aminoAcids, 0, aminoAcids.length);

        this.sideChainModMap = copySideChainModifications(fromInclusive, toExclusive, src.sideChainModMap);
        this.termModMap = copyTermModifications(src, fromInclusive, toExclusive);
        hasAmbiguousAA = hasAmbiguousAA();
    }

    /**
     * Constructor that copies the source AminoAcidSequence and replaces any
     * amino acids in oldAAs with newAA.
     *
     * @param src    the AminoAcidSequence from which to copy the sequence and modifications
     * @param oldAAs set containing the old amino acids
     * @param newAA  the new amino acid
     */
    AminoAcidSequence(final AminoAcidSequence src, Set<AminoAcid> oldAAs, AminoAcid newAA)
    {
        checkNotNull(src);
        checkNotNull(newAA);
        checkNotNull(oldAAs);

        final int size = src.size();
        checkState(size > 0, "Peptides cannot be empty");
        aminoAcids = new AminoAcid[size];

        System.arraycopy(src.aminoAcids, 0, aminoAcids, 0, aminoAcids.length);
        for (int i = 0; i < aminoAcids.length; i++) {

            AminoAcid aa = aminoAcids[i];
            if (oldAAs.contains(aa))
                aminoAcids[i] = newAA;
        }

        this.sideChainModMap = copySideChainModifications(0, size, src.sideChainModMap);
        this.termModMap = copyTermModifications(src, 0, size);
        hasAmbiguousAA = hasAmbiguousAA();
    }

    /**
     * Constructs an AminoAcidSequence from the list of residues with the
     * modifications contained in side chain and term mod map.
     *
     * @param residues        the residues, this list must have at least one amino acid
     * @param sideChainModMap the side chain modifications, this map can be empty
     * @param termModMap      the terminal modifications, this map can be empty
     */
    AminoAcidSequence(List<AminoAcid> residues, ListMultimap<Integer, Modification> sideChainModMap,
            ListMultimap<ModAttachment, Modification> termModMap)
    {
        final int size = residues.size();
        checkState(size > 0, "Peptides cannot be empty");
        aminoAcids = new AminoAcid[size];

        checkNotNull(sideChainModMap);
        checkNotNull(termModMap);
        checkArgument(!termModMap.containsKey(ModAttachment.SIDE_CHAIN), "termModMap cannot contain side chain modifications");

        residues.toArray(aminoAcids);

        for (int key : sideChainModMap.keySet()) {
            checkElementIndex(key, size());
        }

        if (!sideChainModMap.isEmpty()) {

            this.sideChainModMap = ArrayListMultimap.create();
            for (int index : sideChainModMap.keySet()) {
                this.sideChainModMap.putAll(index, ImmutableList.copyOf(sideChainModMap.get(index)));
            }
        }
        if (!termModMap.isEmpty()) {
            this.termModMap = ArrayListMultimap.create();
            for (ModAttachment modAttachment : termModMap.keySet()) {
                this.termModMap.putAll(modAttachment, ImmutableList.copyOf(termModMap.get(modAttachment)));
            }
        }

        hasAmbiguousAA = hasAmbiguousAA();
    }

    private ListMultimap<ModAttachment, Modification> copyTermModifications(
            AminoAcidSequence src, int fromInclusive, int toExclusive)
    {
        if (src.termModMap == null)
            return null;

        ListMultimap<ModAttachment, Modification> newTermModMap = ArrayListMultimap.create();
        if (fromInclusive == 0 && src.termModMap.containsKey(ModAttachment.N_TERM)) {
            newTermModMap.putAll(ModAttachment.N_TERM, src.termModMap.get(ModAttachment.N_TERM));
        }
        if (toExclusive == src.size() && src.termModMap.containsKey(ModAttachment.C_TERM)) {
            newTermModMap.putAll(ModAttachment.C_TERM, src.termModMap.get(ModAttachment.C_TERM));
        }

        return newTermModMap.isEmpty() ? null : newTermModMap;
    }

    /**
     * Copy side chain modification.
     *
     * @param fromInclusive      from index inclusive
     * @param toExclusive        to index exclusive
     * @param srcSideChainModMap source side chain modification map.
     * @return Modification in the range [fromInclusive, toExclusive)
     */
    private ListMultimap<Integer, Modification> copySideChainModifications(
            final int fromInclusive, final int toExclusive, ListMultimap<Integer, Modification> srcSideChainModMap)
    {
        if (srcSideChainModMap == null)
            return null;

        final ListMultimap<Integer, Modification> newSideChainModMap = ArrayListMultimap.create();

        for (Integer index : srcSideChainModMap.keySet()) {
            if (index >= fromInclusive && index < toExclusive) {
                newSideChainModMap.putAll(index - fromInclusive, srcSideChainModMap.get(index));
            }
        }

        return newSideChainModMap.isEmpty() ? null : newSideChainModMap;
    }

    /**
     * @return true if this Sequence has ambiguous amino acid.
     */
    private boolean hasAmbiguousAA()
    {
        for (AminoAcid aminoAcid : aminoAcids) {
            if (!aminoAcid.isUnambiguous())
                return true;
        }

        return false;
    }

    /**
     * @return true if this AminoAcidSequence has modifications
     */
    public boolean hasModifications()
    {
        return (sideChainModMap != null && !sideChainModMap.isEmpty()) || (termModMap != null && !termModMap.isEmpty());
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();

        if (termModMap != null && (termModMap.containsKey(ModAttachment.N_TERM))) {
            appendMods(sb, termModMap.get(ModAttachment.N_TERM));
            sb.append('_');
        }

        for (int i = 0; i < size(); i++) {
            sb.append(aminoAcids[i]);

            if (sideChainModMap != null && sideChainModMap.containsKey(i)) {

                appendMods(sb, sideChainModMap.get(i));
            }
        }

        if (termModMap != null && (termModMap.containsKey(ModAttachment.C_TERM))) {
            sb.append('_');
            appendMods(sb, termModMap.get(ModAttachment.C_TERM));
        }

        return sb.toString();
    }

    /**
     * Used by toString
     *
     * @param sb       the StringBuilder
     * @param modLists the list of mod lists
     */
    @SafeVarargs
    private final void appendMods(StringBuilder sb, List<Modification>... modLists)
    {
        if (modLists.length == 0)
            return;

        sb.append('(');
        for (List<Modification> mods : modLists) {
            if (mods == null)
                mods = Collections.emptyList();
            for (Modification mod : mods) {
                sb.append(mod.getTitle());
                sb.append(", ");
            }
        }
        int length = sb.length();
        sb.delete(length - 2, length);
        sb.append(')');
    }

    /**
     * Returns true if this and <code>sequence</code> have the same AA sequence
     * even if the two sequences have different modifications.
     *
     * @param sequence the sequence to check against
     * @return true if this and <code>sequence</code> have the same AA sequence
     */
    public boolean hasSameSequence(AminoAcidSequence sequence)
    {
        return Arrays.equals(aminoAcids, sequence.aminoAcids);
    }

    /**
     * Returns true if this and <code>sequence</code> have the same AA sequence.
     *
     * @param sequence the sequence to check against
     * @return true if this and <code>sequence</code> have the same AA sequence
     */
    public boolean hasSameSequence(Sequence<AminoAcid> sequence)
    {
        if (size() != sequence.size())
            return false;

        int seqSize = sequence.size();
        for (int i = 0; i < seqSize; i++) {

            AminoAcid thisAA = getSymbol(i);
            AminoAcid thatAA = sequence.getSymbol(i);
            if (!thisAA.equals(thatAA))
                return false;
        }

        return true;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        AminoAcidSequence peptide = (AminoAcidSequence) o;

        return Arrays.equals(aminoAcids, peptide.aminoAcids)
                && Objects.equals(sideChainModMap, peptide.sideChainModMap)
                && Objects.equals(termModMap, peptide.termModMap);
    }

    @Override
    public int hashCode()
    {
        int result = hash;

        if (result == 0) {

            result = Arrays.hashCode(aminoAcids);
            result = 31 * result + (sideChainModMap != null ? sideChainModMap.hashCode() : 0);
            result = 31 * result + (termModMap != null ? termModMap.hashCode() : 0);
        }

        this.hash = result;

        return result;
    }

    /**
     * @return the number of modifications that this peptide has
     */
    public int getModificationCount()
    {
        final Counter count = new Counter();

        if (sideChainModMap != null) {
            count.increment(sideChainModMap.size());
        }

        if (termModMap != null) {
            count.increment(termModMap.size());
        }

        return count.count();
    }

    /**
     * Returns the number of modifications that are attached to the given attachment
     *
     * @param attachment where the modification is attached
     */
    public int getModificationCount(ModAttachment attachment)
    {
        switch (attachment) {
            case SIDE_CHAIN:
                return countSideChainModifications();
            case N_TERM:
            case C_TERM:
                return (termModMap != null) ? termModMap.get(attachment).size() : 0;
            default:
                throw new IllegalStateException("cannot get modification count for " + attachment);
        }
    }

    /**
     * @return number of modifications at amino acid sequence side chain.
     */
    private int countSideChainModifications()
    {
        if (sideChainModMap == null)
            return 0;

        final Counter counter = new Counter();
        counter.increment(sideChainModMap.size());
        return counter.count();
    }

    /**
     * @return a set containing all the amino acids that are ambiguous
     */
    public Set<AminoAcid> getAmbiguousAminoAcids()
    {
        if (!hasAmbiguousAA)
            return Collections.emptySet();

        Set<AminoAcid> ambiguousAAs = new HashSet<>();
        for (int i = 0; i < size(); i++) {

            if (!aminoAcids[i].isUnambiguous())
                ambiguousAAs.add(aminoAcids[i]);
        }
        return ambiguousAAs;
    }

    /**
     * Returns true if this AminoAcidSequence has amino acids that are ambiguous
     * such as X or B.
     *
     * @return true if there are ambiguous amino acids false otherwise
     */
    public boolean hasAmbiguousAminoAcids()
    {
        return hasAmbiguousAA;
    }

    /**
     * @return a string containing the amino acids and ignores the modifications
     */
    @Override
    public String toSymbolString()
    {
        StringBuilder buff = new StringBuilder();

        for (AminoAcid aa : aminoAcids)
            buff.append(aa.getSymbol());

        return buff.toString();
    }

    /**
     * @return the number of residues that have a modification
     */
    public int getModifiedResidueCount()
    {
        int count = (sideChainModMap != null) ? sideChainModMap.keySet().size() : 0;
        if (termModMap != null) {
            if (sideChainModMap == null || !sideChainModMap.containsKey(0))
                count += termModMap.containsKey(ModAttachment.N_TERM) ? 1 : 0;
            if (sideChainModMap == null || !sideChainModMap.containsKey(size() - 1))
                count += termModMap.containsKey(ModAttachment.C_TERM) ? 1 : 0;
        }

        return count;
    }

    /**
     * Returns a ModificationList containing all the modifications for the given
     * attachments.
     * <p>
     * <p>
     * For example given the peptide <b>(n-term)_PE(m1)PS(m2, m3)IDE_(c-term1,
     * c-term2)</b>:
     *
     * <pre>
     * getModifications(EnumSet.of(ModAttachment.N_TERM) will return {n-term}
     * getModifications(EnumSet.of(ModAttachment.C_TERM) will return {c-term1, c-term2}
     * getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN) will return {m1, m2, m3}
     * getModifications(EnumSet.of(ModAttachment.all) will return {n-term, m1, m2, m3, c-term1, c-term2}
     * </pre>
     *
     * @param attachments the ModAttachments to include
     * @return a ModificationList containing all the modifications for the given attachments
     */
    public ModificationList getModifications(Set<ModAttachment> attachments)
    {
        final ModificationList foundMods = new ModificationList();

        if (termModMap != null) {
            for (ModAttachment attachment : attachments) {
                if (termModMap.containsKey(attachment))
                    foundMods.addAll(termModMap.get(attachment));
            }
        }

        if (sideChainModMap != null && attachments.contains(ModAttachment.SIDE_CHAIN)) {
            foundMods.addAll(sideChainModMap.values());
        }

        return foundMods;
    }

    /**
     * Return the index of given modification.
     * for N-term, -1 is return
     * for C-term, peptide length is return.
     *
     * @param modId modification name
     * @return the index of given modification
     */
    public int[] getModificationIndexes(String modId)
    {
        IntList indexes = new IntArrayList();
        List<Modification> mods;
        if (termModMap != null) {
            mods = termModMap.get(ModAttachment.N_TERM);
            if (!mods.isEmpty()) {
                for (Modification mod : mods) {
                    if (mod.getTitle().equals(modId)) {
                        indexes.add(-1);
                        break;
                    }
                }
            }
        }

        if (sideChainModMap != null) {
            for (Integer index : sideChainModMap.keySet()) {
                mods = sideChainModMap.get(index);
                for (Modification mod : mods) {
                    if (mod.getTitle().equals(modId)) {
                        indexes.add(index.intValue());
                        break;
                    }
                }
            }
        }

        if (termModMap != null) {
            mods = termModMap.get(ModAttachment.C_TERM);
            if (!mods.isEmpty()) {
                for (Modification mod : mods) {
                    if (mod.getTitle().equals(modId)) {
                        indexes.add(aminoAcids.length);
                        break;
                    }
                }
            }
        }
        return indexes.toIntArray();
    }

    /**
     * Returns a ModificationList containing all the modifications for the given
     * index and attachments.
     * <p>
     * <p>
     * For example given the peptide <b>(n-term)_PE(m1)PS(m2, m3)IDE_(c-term1,
     * c-term2)</b>:
     *
     * <pre>
     * getModifications(0, EnumSet.of(ModAttachment.N_TERM) will return {n-term}
     * getModifications(6, EnumSet.of(ModAttachment.N_TERM) will return {}
     * getModifications(6, EnumSet.of(ModAttachment.C_TERM) will return {c-term1, c-term2}
     * getModifications(3, EnumSet.of(ModAttachment.SIDE_CHAIN) will return {m2, m3}
     * </pre>
     *
     * @param attachments the ModAttachments to include
     * @return a ModificationList containing all the modifications for the given attachments
     */
    public ModificationList getModificationsAt(int index, Set<ModAttachment> attachments)
    {
        checkElementIndex(index, size());

        ModificationList foundMods = null;
        if (sideChainModMap != null && attachments.contains(ModAttachment.SIDE_CHAIN)
                && sideChainModMap.containsKey(index)) {

            foundMods = createAndAddModifications(null, sideChainModMap.get(index));
        }

        if (termModMap != null) {
            if (index <= 0 && attachments.contains(ModAttachment.N_TERM)) {
                List<Modification> modificationList = termModMap.get(ModAttachment.N_TERM);
                if (!modificationList.isEmpty())
                    foundMods = createAndAddModifications(foundMods, modificationList);

            } else if (index >= size() - 1 && attachments.contains(ModAttachment.C_TERM)) {
                List<Modification> modificationList = termModMap.get(ModAttachment.C_TERM);
                if (!modificationList.isEmpty())
                    foundMods = createAndAddModifications(foundMods, modificationList);
            }
        }

        return foundMods == null ? ModificationList.EMPTY_MOD_LIST : foundMods;
    }

    private ModificationList createAndAddModifications(ModificationList foundMods, List<Modification> mods)
    {
        if (foundMods == null)
            foundMods = new ModificationList();

        foundMods.addAll(mods);
        return foundMods;
    }

    /**
     * Returns true if the residue at <code>index</code>(0-based) has a modification.
     *
     * @param index the index
     */
    public boolean hasModificationAt(int index)
    {
        final boolean hasSideChainMod = sideChainModMap != null && sideChainModMap.containsKey(index);
        if (index == 0) {
            return hasSideChainMod || (termModMap != null && termModMap.containsKey(ModAttachment.N_TERM));
        } else if (index == aminoAcids.length - 1) {
            return hasSideChainMod || (termModMap != null && termModMap.containsKey(ModAttachment.C_TERM));
        } else {
            return hasSideChainMod;
        }
    }

    /**
     * Returns true if the residue at <code>index</code> has a modification
     * attached by <code>modAttachment</code>.
     *
     * @param index          the index
     * @param modAttachments set of mod attachments to check
     * @return true if the residue at <code>index</code> has a modification
     */
    public boolean hasModificationAt(int index, Set<ModAttachment> modAttachments)
    {
        boolean hasMods = false;
        for (ModAttachment modAttachment : modAttachments) {
            switch (modAttachment) {
                case SIDE_CHAIN:
                    hasMods = hasMods || sideChainModMap != null && sideChainModMap.containsKey(index);
                    break;
                case N_TERM:
                    hasMods = hasMods || index == 0 && termModMap.containsKey(ModAttachment.N_TERM);
                    break;
                case C_TERM:
                    hasMods = hasMods || index == aminoAcids.length - 1 && termModMap.containsKey(ModAttachment.C_TERM);
                    break;
                default:
                    throw new IllegalStateException(modAttachment + " is an unknown ModAttachment");
            }
        }

        return hasMods;
    }

    /**
     * Returns true if there are modifications attached via the given <code>modAttachment</code>
     *
     * @param modAttachment the mod attachments to check
     */
    public boolean hasModificationAt(ModAttachment modAttachment)
    {
        switch (modAttachment) {
            case SIDE_CHAIN:
                return sideChainModMap != null && !sideChainModMap.isEmpty();
            case N_TERM:
            case C_TERM:
                return termModMap != null && termModMap.containsKey(modAttachment);
            default:

                throw new IllegalStateException(modAttachment + " is an unknown ModAttachment");
        }
    }

    /**
     * Returns true if there are modifications attached via any of the
     * ModAttachments in <code>modAttachments</code>
     *
     * @param modAttachments the mod attachments to check
     * @return true if there are modifications attached via the given <code>modAttachment</code>
     */
    public boolean hasModificationAt(Set<ModAttachment> modAttachments)
    {
        for (ModAttachment modAttachment : modAttachments) {
            if (hasModificationAt(modAttachment)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return the number of residues in this amino acid sequence
     */
    @Override
    public int size()
    {
        return aminoAcids.length;
    }

    /**
     * @return a sorted Array containing the indexes of the modified amino acids
     */
    public int[] getModificationIndexes(Set<ModAttachment> modAttachments)
    {
        final IntSet indexSet = new IntOpenHashSet();

        if (modAttachments.contains(ModAttachment.N_TERM) && hasModificationAt(ModAttachment.N_TERM)) {
            indexSet.add(0);
        }
        if (modAttachments.contains(ModAttachment.C_TERM) && hasModificationAt(ModAttachment.C_TERM)) {
            indexSet.add(size() - 1);
        }
        if (modAttachments.contains(ModAttachment.SIDE_CHAIN) && sideChainModMap != null) {
            Set<Integer> keys = sideChainModMap.keySet();
            indexSet.addAll(keys);
        }

        int[] indexes = indexSet.toIntArray();
        Arrays.sort(indexes);
        return indexes;
    }

    /**
     * Returns an array containing the indexes of where <code>symbol</code> is
     * found in this sequence.
     * <p/>
     * For example:
     * <p/>
     * the indexes of S in PSPSK is {1, 3}
     *
     * @param symbol the symbol for which the indexes are to be returned
     * @return an array containing the indexes of where <code>symbol</code> is found in this sequence
     */
    public int[] getSymbolIndexes(AminoAcid symbol)
    {
        IntList indices = new IntArrayList(size());

        for (int i = 0; i < aminoAcids.length; i++) {

            if (aminoAcids[i] == symbol)
                indices.add(i);
        }

        return indices.toIntArray();
    }

    @Override
    public AminoAcid getSymbol(int index)
    {
        return aminoAcids[index];
    }

    /**
     * Sum the mass of all average
     *
     * @return average mass
     */
    double calculateAverageMassSum()
    {
        double mass = 0;
        // sum all amino acids
        for (int i = 0; i < size(); i++) {
            mass += aminoAcids[i].getAverageMass();
        }

        // sum all side chain modification
        if (sideChainModMap != null) {
            for (Modification mod : sideChainModMap.values()) {
                Mass mass1 = mod.getMass();
                if (mass1 instanceof Composition) {
                    Composition com = (Composition) mass1;
                    mass += com.getAverageMass();
                } else {
                    mass += mass1.getMolecularMass();
                }
            }
        }

        // sum term modification
        if (termModMap != null) {
            for (Modification mod : termModMap.values()) {
                Mass mass1 = mod.getMass();
                if (mass1 instanceof Composition) {
                    Composition com = (Composition) mass1;
                    mass += com.getAverageMass();
                } else {
                    mass += mass1.getMolecularMass();
                }
            }
        }

        return mass;
    }

    /**
     * @return the sum of all residue masses, including modifications.
     */
    public double calculateMonomerMassSum()
    {
        double mass = 0;
        // sum all amino acids
        for (int i = 0; i < size(); i++)
            mass += aminoAcids[i].getMolecularMass();

        // sum all side chain modification
        if (sideChainModMap != null) {
            for (Modification mod : sideChainModMap.values()) {
                mass += mod.getMolecularMass();
            }
        }

        // sum term modification
        if (termModMap != null) {
            for (Modification mod : termModMap.values()) {
                mass += mod.getMolecularMass();
            }
        }

        return mass;
    }

    public double calculateMonomerMassDefect()
    {
        double massDefect = 0;
        for (int i = 0; i < size(); i++) {
            massDefect += aminoAcids[i].getComposition().getMassDefect();
        }

        if (sideChainModMap != null) {
            for (Modification modification : sideChainModMap.values()) {
                massDefect += modification.getMass().getMassDefect();
            }
        }

        if (termModMap != null) {

            for (Modification modification : termModMap.values()) {
                massDefect += modification.getMass().getMassDefect();
            }
        }

        return massDefect;
    }

    /**
     * To test if this peptide contains any of the AminoAcid in <code>aminoAcidSet</code>.
     *
     * @param aminoAcidSet a AminoAcid Set.
     * @return true if this peptide contains any of the AminoAcid in the given Amino Acid Set.
     */
    public boolean contains(Set<AminoAcid> aminoAcidSet)
    {
        for (int i = 0; i < size(); i++) {
            if (aminoAcidSet.contains(getSymbol(i)))
                return true;
        }

        return false;
    }

    /**
     * Counts sums the number of times each of the amino acid in <code>aminoAcidSet</code> is contained in this Peptide.
     * <p/>
     * For example PEPSIDE.countAminoAcidsIn((P, I)) returns 3
     *
     * @param aminoAcidSet the amino acids to check
     */
    public int countAminoAcidsIn(Set<AminoAcid> aminoAcidSet)
    {
        int count = 0;
        for (int i = 0; i < size(); i++) {
            if (aminoAcidSet.contains(getSymbol(i)))
                count += 1;
        }

        return count;
    }

    /**
     * @return <tt>true</tt> if this AminoAcidSequence contains no residues
     */
    public boolean isEmpty()
    {
        return aminoAcids.length == 0;
    }
}
