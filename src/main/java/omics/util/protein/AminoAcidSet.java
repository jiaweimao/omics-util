package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import it.unimi.dsi.fastutil.chars.*;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import omics.util.protein.database.FastaSequence;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.PTM;
import omics.util.protein.mod.Pos;
import omics.util.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * A factory class to instantiate a set of amino acids
 *
 * @author sangtaekim
 * @author JiaweiMao
 * @version 2.0.2
 */
public class AminoAcidSet implements Iterable<AminoAcid>
{
    private static final Logger logger = LoggerFactory.getLogger(AminoAcidSet.class);

    private static final AminoAcid[] EMPTY_AA_ARRAY = new AminoAcid[0];

    /**
     * location map, map from parent location its children locations.
     */
    private static HashMap<Pos, Pos[]> locMap;

    static {
        locMap = new HashMap<>();
        locMap.put(Pos.ANY_WHERE, new Pos[]{Pos.ANY_WHERE, Pos.ANY_N_TERM, Pos.ANY_C_TERM, Pos.PROTEIN_N_TERM, Pos.PROTEIN_C_TERM});
        locMap.put(Pos.ANY_N_TERM, new Pos[]{Pos.ANY_N_TERM, Pos.PROTEIN_N_TERM});
        locMap.put(Pos.ANY_C_TERM, new Pos[]{Pos.ANY_C_TERM, Pos.PROTEIN_C_TERM});
        locMap.put(Pos.PROTEIN_N_TERM, new Pos[]{Pos.PROTEIN_N_TERM});
        locMap.put(Pos.PROTEIN_C_TERM, new Pos[]{Pos.PROTEIN_C_TERM});
    }

    /**
     * standard amino acid set.
     */
    private static AminoAcidSet standardAASet = null;

    /**
     * for fast indexing
     * residue -> aa (residue must be unique), including modified AAs
     */
    private Char2ObjectMap<AminoAcid> residueMap;
    /**
     * aa -> index (mass in ascending order), for all amino acids, including modified amino acids.
     */
    private Object2IntMap<AminoAcid> aa2index;

    /**
     * nominalMass -> array of amino acids
     */
    private HashMap<Pos, Int2ObjectOpenHashMap<AminoAcid[]>> nominalMass2aa;

    /**
     * std residue -> array of amino acids, unmodified amino acid is always in the front of the array.
     */
    private HashMap<Pos, Char2ObjectOpenHashMap<AminoAcid[]>> stdResidueAAArrayMap;

    /**
     * the amino acids with the heaviest and lighest mass
     */
    private AminoAcid heaviestAA;

    /**
     * array of all amino acids, sort in mass ascending order
     */
    private AminoAcid[] allAminoAcidArr;
    /**
     * true if this contains any (fixed or variable) modification specific to N-terminus
     */
    private boolean containsNTermMod;
    /**
     * true if this contains any (fixed or variable) modification specific to C-terminus
     */
    private boolean containsCTermMod;
    private boolean containsPhosphorylation;
    /**
     * true if this contains iTRAQ modification
     */
    private boolean containsITRAQ;
    /**
     * true if this contains TMT modification
     */
    private boolean containsTMT;
    // for enzyme
    private int neighborAACutCredit = 0;
    private int neighborAACutPenalty = 0;
    private int peptideCutCredit = 0;
    private int peptideCutPenalty = 0;
    /**
     * probability of the cleavage of current Enzyme, which is the sum of the ratios all cleavable
     * amino acids in the fasta database.
     */
    private double probCleavageSites = 0;
    /**
     * this map hold all {@link AminoAcid} of this set, including fixed and variable modified amino acids.
     */
    private ArrayListMultimap<Pos, AminoAcid> aaListMap = ArrayListMultimap.create();
    private HashMap<Character, Pair<Modification, Pos>> fixedModMap = new HashMap<>();
    private Table<AminoAcid, Modification, AminoAcid> aaPTMTable = HashBasedTable.create();

    /**
     * all modifications apply to this AminoAcidSet.
     */
    private List<PeptideMod> modifications;

    private AminoAcidSet() { }

    /**
     * Return the {@link Modification} of given amino acid char if it is fixed modified, return null otherwise.
     *
     * @param aa modified amino acid one letter code
     * @return {@link Modification} and corresponding {@link Pos} of given modified amino acid.
     */
    public Pair<Modification, Pos> getFixedModification(char aa)
    {
        return fixedModMap.get(aa);
    }

    /**
     * Returns the list of amino acids specific to the position.
     *
     * @return list of intermediate amino acids.
     */
    public List<AminoAcid> getAAList(Pos location)
    {

        return aaListMap.get(location);
    }

    /**
     * Returns the iterator of anywhere amino acids
     */
    @Override
    public Iterator<AminoAcid> iterator()
    {
        return aaListMap.get(Pos.ANY_WHERE).iterator();
    }

    /**
     * Returns the number of amino acids at given position.
     *
     * @param location amino acid location
     */
    public int size(Pos location)
    {
        return aaListMap.get(location).size();
    }

    /**
     * @return the types of amino acids
     */
    public int size()
    {
        return allAminoAcidArr.length;
    }

    /**
     * Retrieve an array of amino acids given the specific standard residue, modified {@link AminoAcid} are included
     *
     * @param location          amino acid location
     * @param standardAAResidue the standard residue to look up
     * @return the array of amino acids or an empty array otherwise
     */
    public AminoAcid[] getAminoAcids(Pos location, char standardAAResidue)
    {
        AminoAcid[] matches = stdResidueAAArrayMap.get(location).get(standardAAResidue);
        if (matches != null)
            return matches;
        else
            return EMPTY_AA_ARRAY;
    }

    /**
     * Retrieve an array of amino acids given the specific nominal mass.
     *
     * @param location    amino acid location
     * @param nominalMass nominal mass to look up
     * @return the array of amino acids or an empty array otherwise
     */
    public AminoAcid[] getAminoAcids(Pos location, int nominalMass)
    {

        AminoAcid[] matches = nominalMass2aa.get(location).get(nominalMass);
        if (matches != null)
            return matches;
        return EMPTY_AA_ARRAY;
    }

    /**
     * Retrieve an array of amino acids given the specific nominal mass.
     *
     * @param nominalMass the mass to look up
     * @return the array of amino acids or an empty list otherwise
     */
    public AminoAcid[] getAminoAcids(int nominalMass)
    {
        return getAminoAcids(Pos.ANY_WHERE, nominalMass);
    }

    /**
     * Checks whether a residue belongs to this amino acid set,
     * such as 'S' for standard Ser and 's' for variable modified Ser.
     *
     * @param residue a residue, which the char of Amino acid, standard or modified.
     * @return true if residue belongs to the amino acid set
     */
    public boolean contains(char residue)
    {

        return residueMap.containsKey(residue);
    }

    /**
     * Returns a list of all residues without mods, no duplication.
     */
    public CharList getResiduesWithoutMods()
    {
        CharSet residueSet = new CharOpenHashSet();
        for (Char2ObjectMap.Entry<AminoAcid> entry : residueMap.char2ObjectEntrySet()) {
            char residue = entry.getValue().getStandardResidue();
            residueSet.add(residue);
        }
        CharArrayList list = new CharArrayList(residueSet);
        list.sort(CharComparators.NATURAL_COMPARATOR);
        return list;
    }

    /**
     * @return Returns residues in natural order, including modified residues, fixed modifications do not change the set.
     */
    public CharList getResidueList()
    {
        CharArrayList list = new CharArrayList(residueMap.keySet());
        list.sort(CharComparators.NATURAL_COMPARATOR);
        return list;
    }

    /**
     * Get the unmodified amino acid of the residue.
     *
     * @param residue the amino acid mass. Use upper case for standard aa (convention).
     *                this method is case sensitive.
     * @return the amino acid object. null if no aa corresponding to the residue
     */
    public AminoAcid getAminoAcid(Pos location, char residue)
    {

        AminoAcid[] aaArr = getAminoAcids(location, residue);
        for (AminoAcid aa : aaArr)
            if (!aa.isModified())
                return aa;
        return null;
    }

    /**
     * Get the amino acid mass of the residue.
     *
     * @param residue the amino acid mass. Use upper case for standard aa (convention).
     *                this method is case sensitive.
     * @return the amino acid object. null if no aa corresponding to the residue
     */
    public AminoAcid getAminoAcid(char residue)
    {
        return residueMap.get(residue);
    }

    /**
     * Return all amino acids, including modified amino acids.
     *
     * @return an array of all amino acids
     */
    public AminoAcid[] getAminoAcids()
    {
        return this.allAminoAcidArr;
    }

    /**
     * Get the amino acid corresponding to the index, the index is the order of the
     * amino acid in amino acid array in ascending mass order
     *
     * @param index amino acid index
     * @return amino acid object
     */
    public AminoAcid getAminoAcid(int index)
    {
        return allAminoAcidArr[index];
    }

    /**
     * Get the index of the aa, the index is the order of Amino acid with mass in ascending order in all amino acids.
     *
     * @param aa amino acid
     * @return the index of aa. -1 if aa does not belong to this amino acid set
     */
    public int getIndex(AminoAcid aa)
    {
        return aa2index.getOrDefault(aa, -1);
    }

    /**
     * @return the nominal mass of the amino acid with largest mass
     */
    public int getMaxNominalMass()
    {
        return this.heaviestAA.getNominalMass();
    }

    /**
     * @return {@link AminoAcid} with largest mass
     */
    public AminoAcid getHeaviestAA()
    {
        return this.heaviestAA;
    }

    /**
     * @return true if this contains any (fixed or variable) modification specific to N-terminus
     */
    public boolean hasNTermMod()
    {
        return this.containsNTermMod;
    }

    /**
     * @return true if contains any (fixed or variable) modification specific to C-terminus
     */
    public boolean hasCTermMod()
    {

        return this.containsCTermMod;
    }

    /**
     * @return true if this contains phosphorylation
     */
    public boolean containsPhosphorylation()
    {
        return this.containsPhosphorylation;
    }

    /**
     * @return true if contains the iTRAQ modification.
     */
    public boolean containsITRAQ()
    {

        return this.containsITRAQ;
    }

    public boolean containsTMT()
    {
        return this.containsTMT;
    }

    /**
     * @return the residue with maximum char value
     */
    public char getMaxResidue()
    {

        return nextResidue;
    }

    public void registerEnzyme(Enzyme enzyme1, Enzyme enzyme2)
    {
        checkNotNull(enzyme1, "Enzyme is null");
        checkNotNull(enzyme2, "Enzyme is null");

        if (enzyme1.getCleavageEfficiency() <= 0 || enzyme1.getNeighborCleavageEfficiency() <= 0 ||
                enzyme2.getCleavageEfficiency() <= 0 || enzyme2.getNeighborCleavageEfficiency() <= 0)
            return;
        probCleavageSites = 0;
        for (Character residue : enzyme1.getResidueSet()) {
            AminoAcid aa = getAminoAcid(residue);
            if (aa == null) {
                throw new IllegalArgumentException("No amino acid of '" + residue + "' for enzyme " + enzyme1.getName());
            }
            probCleavageSites += aa.getProbability();
        }
        for (Character residue : enzyme2.getResidueSet()) {
            AminoAcid aa = getAminoAcid(residue);
            if (aa == null) {
                throw new IllegalArgumentException("No amino acid of '" + residue + "' for enzyme " + enzyme1.getName());
            }
            probCleavageSites += aa.getProbability();
        }

        if (probCleavageSites == 0 || probCleavageSites == 1) {
            throw new IllegalArgumentException("Probability of enzyme residues must be in range (0,1)!");
        }

        double cleavageEfficiency = Math.min(enzyme1.getCleavageEfficiency(), enzyme2.getCleavageEfficiency());
        double neighborEfficiency = Math.min(enzyme1.getNeighborCleavageEfficiency(), enzyme2.getNeighborCleavageEfficiency());

        peptideCutCredit = (int) Math.round(Math.log(cleavageEfficiency / probCleavageSites));
        peptideCutPenalty = (int) Math.round(Math.log((1 - cleavageEfficiency) / (1 - probCleavageSites)));
        neighborAACutCredit = (int) Math.round(Math.log(neighborEfficiency / probCleavageSites));
        neighborAACutPenalty = (int) Math.round(Math.log((1 - neighborEfficiency) / (1 - probCleavageSites)));
    }

    /**
     * update the enzyme specific probability values.
     */
    public void registerEnzyme(Enzyme enzyme)
    {
        checkNotNull(enzyme, "The enzyme is null.");

        if (enzyme.getCleavageEfficiency() <= 0 || enzyme.getNeighborCleavageEfficiency() <= 0) {
            return;
        }

        probCleavageSites = 0;
        for (char residue : enzyme.getResidueSet()) {
            AminoAcid aa = this.getAminoAcid(residue);
            if (aa == null) {
                throw new IllegalArgumentException("No amino acid of '" + residue + "' for enzyme " + enzyme.getName());
            }
            probCleavageSites += aa.getProbability();
        }

        if (probCleavageSites == 0 || probCleavageSites == 1) {
            throw new IllegalArgumentException("Probability of enzyme residues must be in range (0,1)!");
        }

        double subCleavageEfficiency = enzyme.getCleavageEfficiency();
        double preCleavageEfficiency = enzyme.getNeighborCleavageEfficiency();

        peptideCutCredit = (int) Math.round(Math.log(subCleavageEfficiency / probCleavageSites));
        peptideCutPenalty = (int) Math.round(Math.log((1 - subCleavageEfficiency) / (1 - probCleavageSites)));
        neighborAACutCredit = (int) Math.round(Math.log(preCleavageEfficiency / probCleavageSites));
        neighborAACutPenalty = (int) Math.round(Math.log((1 - preCleavageEfficiency) / (1 - probCleavageSites)));
    }

    /**
     * @return the credit of the neighbor amino acids of the peptide to be enzyme cleavable.
     */
    public int getNeighborCutCredit()
    {
        return neighborAACutCredit;
    }

    /**
     * @return the penalty of the neighbor amino acids not to be enzyme cleavable.
     */
    public int getNeighborCutPenalty()
    {
        return neighborAACutPenalty;
    }

    /**
     * Return the credit score for enzyme cleavage, with cleavage efficiency larger than 0.9 for trypsin of human uniprot database, it is about 2
     *
     * @return the credit for enzyme cleavage.
     */
    public int getEnzymeCutCredit()
    {
        return peptideCutCredit;
    }

    /**
     * Return the penalty score for enzyme cleavage.
     * <p>
     * for uniprot human database, of different trypsin cleavage efficiency, the score value:
     * <ul>
     *     <li>1-10^-1, -2</li>
     *     <li>1-10^-2, -4</li>
     *     <li>1-10^-3, -7</li>
     *     <li>1-10^-4, -9</li>
     *     <li>1-10^-5, -11</li>
     *     <li>1-10^-6, -14</li>
     * </ul>
     *
     * @return the penalty for non enzyme cleavage.
     */
    public int getEnzymeCutPenalty()
    {
        return peptideCutPenalty;
    }

    /**
     * Probability of the cleavage of current Enzyme, which is the sum of the ratios all cleavable
     * amino acids in the fasta database.
     *
     * @return ratios sum of all cleavable amino acids in the database.
     */
    public double getCleavageProbability()
    {
        return probCleavageSites;
    }

    /**
     * print the Amino acid set
     */
    public void printAASet()
    {
        for (Pos location : Pos.getCommonsPositions()) {
            List<AminoAcid> aaList = this.getAAList(location);
            System.out.println(location + "\t" + aaList.size());
            System.out.println("Residue\tChar\tCharInInt\tNominal Mass\tMass\tProbability");
            for (AminoAcid aa : aaList)
                System.out.println(aa.getSymbol() + (aa.isModified() ? "*" : "") + "\t" + aa.getOneLetterCode() + "\t"
                        + (int) aa.getOneLetterCode() + "\t" + aa.getNominalMass() + "\t" + aa.getMass() + "\t" + aa.getProbability());
        }
    }

    /**
     * Add an {@link AminoAcid} to given {@link Pos}
     */
    private void addAminoAcid(AminoAcid aa, Pos location)
    {

        for (Pos loc : locMap.get(location)) {
            aaListMap.put(loc, aa);
        }
    }


    /**
     * @return {@link PeptideMod} applied to this amino acid set, return null if these is no modification applied.
     */
    public List<PeptideMod> getModifications()
    {
        return modifications;
    }

    /**
     * apply given modification list to AminoAcidSet.
     */
    private void applyModifications(List<PeptideMod> mods)
    {
        checkNotNull(mods);
        if (mods.isEmpty())
            return;
        this.modifications = mods;

        // partition modification instances into different types
        ArrayListMultimap<Pos, PeptideMod> fixedMods = ArrayListMultimap.create();
        ArrayListMultimap<Pos, PeptideMod> variableMods = ArrayListMultimap.create();
        for (PeptideMod mod : mods) {
            if (mod.isFixed())
                fixedMods.put(mod.getPosition(), mod);
            else
                variableMods.put(mod.getPosition(), mod);
        }

        // the order is important, to avoid
        Pos[] posArr = new Pos[]{
                Pos.ANY_WHERE,
                Pos.ANY_N_TERM,
                Pos.ANY_C_TERM,
                Pos.PROTEIN_N_TERM,
                Pos.PROTEIN_C_TERM
        };

        for (Pos pos : posArr) {
            applyFixedMods(fixedMods.get(pos), pos);
        }

        for (Pos pos : posArr) {
            applyVariableMods(variableMods.get(pos), pos);
        }

        for (PeptideMod mod : mods) {
            Pos location = mod.getPosition();

            if (!containsNTermMod && location.isNTerm())
                this.containsNTermMod = true;
            if (!containsCTermMod && location.isCTerm())
                this.containsCTermMod = true;

            if (mod.getModificationName().toLowerCase().startsWith("phospho"))
                this.containsPhosphorylation = true;
            if (mod.getModificationName().toLowerCase().startsWith("itraq"))
                this.containsITRAQ = true;
            if (mod.getModificationName().toLowerCase().startsWith("tmt"))
                this.containsTMT = true;
        }
    }


    /**
     * Apply fixed modifications at given Location to this {@link AminoAcidSet}, update the <code>aaListMap</code>
     *
     * @param fixedMods fixed modification
     * @param location  {@link Pos}
     */
    private void applyFixedMods(List<PeptideMod> fixedMods, Pos location)
    {

        for (PeptideMod mod : fixedMods) {
            // residue-specific
            AminoAcid targetAA = mod.getTargetAminoAcid();
            ArrayList<AminoAcid> newAAList = new ArrayList<>();
            if (targetAA == AminoAcid.X) {
                if (location == Pos.ANY_WHERE) {
                    for (AminoAcid aa : aaListMap.get(location)) { // any where for any amino acid
                        AminoAcid fixedModAA = aa.getFixedModAA(mod);
                        newAAList.add(fixedModAA);

                        fixedModMap.put(fixedModAA.getOneLetterCode(), Pair.create(mod.getModification(), location));
                        aaPTMTable.put(aa, mod.getModification(), fixedModAA);
                    }
                } else {
                    for (AminoAcid aa : aaListMap.get(location)) { // only for some location.
                        ModifiedAminoAcid modAA = getModifiedAminoAcid(aa, mod);
                        newAAList.add(modAA);

                        fixedModMap.put(modAA.getOneLetterCode(), Pair.create(mod.getModification(), location));
                        aaPTMTable.put(aa, mod.getModification(), modAA);
                    }
                }
            } else {
                for (AminoAcid aa : aaListMap.get(location)) {
                    if (aa.getStandardResidue() != targetAA.getStandardResidue()) {
                        newAAList.add(aa);
                    } else {
                        if (location == Pos.ANY_WHERE) {
                            AminoAcid fixedModAA = aa.getFixedModAA(mod);
                            newAAList.add(fixedModAA);    // replace with a new amino acid

                            fixedModMap.put(fixedModAA.getOneLetterCode(), Pair.create(mod.getModification(), location));
                            aaPTMTable.put(aa, mod.getModification(), fixedModAA);
                        } else {
                            ModifiedAminoAcid modifiedAminoAcid = getModifiedAminoAcid(aa, mod);
                            newAAList.add(modifiedAminoAcid);

                            fixedModMap.put(modifiedAminoAcid.getOneLetterCode(), Pair.create(mod.getModification(), location));
                            aaPTMTable.put(aa, mod.getModification(), modifiedAminoAcid);
                        }
                    }
                }
            }

            for (Pos loc : locMap.get(location)) {
                aaListMap.replaceValues(loc, newAAList);
            }
        }
    }

    /**
     * apply variable modifications at given location to this {@link AminoAcidSet}, update the <code>aaListMap</code>
     */
    private void applyVariableMods(List<PeptideMod> variableMods, Pos location)
    {

        for (Pos pos : locMap.get(location)) {
            ArrayList<AminoAcid> newAAList = new ArrayList<>();
            for (AminoAcid targetAA : aaListMap.get(pos)) {
                for (PeptideMod mod : variableMods) {
                    AminoAcid residue = mod.getTargetAminoAcid();
                    if (residue == AminoAcid.X) { // any amino acid, only in terminal
                        if (targetAA.isTerminalModified()) // here only one modification is allowed.
                            continue;

                        ModifiedAminoAcid modAA = getModifiedAminoAcid(targetAA, mod);
                        newAAList.add(modAA);

                        aaPTMTable.put(targetAA, mod.getModification(), modAA);
                    } else {
                        if (residue.getStandardResidue() == targetAA.getStandardResidue()) {
                            if (targetAA.isResidueModified())
                                continue;

                            ModifiedAminoAcid modAA = getModifiedAminoAcid(targetAA, mod);
                            newAAList.add(modAA);

                            aaPTMTable.put(targetAA, mod.getModification(), modAA);
                        }
                    }
                }
            }

            aaListMap.putAll(pos, newAAList);
        }
    }


    /**
     * Return the modified {@link AminoAcid} of given standard {@link AminoAcid} and {@link Modification}
     *
     * @param standardAA   a standard {@link AminoAcid}
     * @param modification a {@link Modification}
     */
    public AminoAcid getAminoAcid(AminoAcid standardAA, Modification modification)
    {

        return aaPTMTable.get(standardAA, modification);
    }

    /**
     * Return the modified {@link AminoAcid} of given standard amino acid, fixed modification and variable {@link Modification}
     *
     * @param stdAA    standard {@link AminoAcid}
     * @param fixed    fixed {@link Modification}
     * @param variable variable {@link Modification}
     */
    public AminoAcid getAminoAcid(AminoAcid stdAA, Modification fixed, Modification variable)
    {

        AminoAcid aminoAcid = aaPTMTable.get(stdAA, fixed);
        return aaPTMTable.get(aminoAcid, variable);
    }

    /**
     * process all amino acids, and create all the related information maps.
     */
    private void build()
    {
        // update the allAminoAcidArr
        HashSet<AminoAcid> allAASet = new HashSet<>(); // only letter code and composition
        for (Pos location : aaListMap.keySet()) {
            allAASet.addAll(aaListMap.get(location));
        }
        this.allAminoAcidArr = allAASet.toArray(new AminoAcid[0]);
        Arrays.sort(allAminoAcidArr);

        this.heaviestAA = allAminoAcidArr[allAminoAcidArr.length - 1];

        // assign index
        aa2index = new Object2IntOpenHashMap<>(allAminoAcidArr.length);
        residueMap = new Char2ObjectOpenHashMap<>(allAminoAcidArr.length);
        for (int i = 0; i < allAminoAcidArr.length; i++) {
            AminoAcid aa = allAminoAcidArr[i];
            aa2index.put(aa, i);
            assert (residueMap.get(aa.getOneLetterCode()) == null) : aa.getOneLetterCode() + " already exists!";
            residueMap.put(aa.getOneLetterCode(), aa);
        }

        stdResidueAAArrayMap = new HashMap<>();
        nominalMass2aa = new HashMap<>();
        for (Pos location : Pos.getCommonsPositions()) {
            stdResidueAAArrayMap.put(location, new Char2ObjectOpenHashMap<>());
            nominalMass2aa.put(location, new Int2ObjectOpenHashMap<>());
        }

        for (Pos location : Pos.getCommonsPositions()) {
            Int2ObjectMap<ArrayList<AminoAcid>> mass2aaList = new Int2ObjectOpenHashMap<>(); // nominal mass to amino acid list
            Char2ObjectMap<LinkedList<AminoAcid>> stdResidue2aaList = new Char2ObjectOpenHashMap<>();

            for (AminoAcid aa : aaListMap.get(location)) {
                int thisMass = aa.getNominalMass();

                ArrayList<AminoAcid> aaListofMass = mass2aaList.computeIfAbsent(thisMass, k -> new ArrayList<>());
                aaListofMass.add(aa);

                char stdResidue = aa.getStandardResidue();
                LinkedList<AminoAcid> aaList = stdResidue2aaList.get(stdResidue);
                if (aaList == null)
                    aaList = new LinkedList<>();
                if (!aa.isModified())
                    aaList.addFirst(aa);    // unmodified residue is at first
                else
                    aaList.addLast(aa);
                stdResidue2aaList.put(stdResidue, aaList);
            }

            // convert the array back to real arrays
            Int2ObjectOpenHashMap<AminoAcid[]> mass2aaArray = new Int2ObjectOpenHashMap<>(mass2aaList.size());
            for (int mass : mass2aaList.keySet()) {
                mass2aaArray.put(mass, mass2aaList.get(mass).toArray(new AminoAcid[0]));
            }

            Char2ObjectOpenHashMap<AminoAcid[]> stdResidue2aaArray = new Char2ObjectOpenHashMap<>(stdResidue2aaList.size());
            for (char residue : stdResidue2aaList.keySet()) {
                stdResidue2aaArray.put(residue, stdResidue2aaList.get(residue).toArray(new AminoAcid[0]));
            }

            this.nominalMass2aa.put(location, mass2aaArray);
            this.stdResidueAAArrayMap.put(location, stdResidue2aaArray);
        }
    }


    /**
     * @return amino acid set contains all standard amino acids.
     */
    public static AminoAcidSet getStandardAminoAcidSet()
    {
        if (standardAASet == null) {
            standardAASet = new AminoAcidSet();
            for (AminoAcid aa : AminoAcid.getStandardAminoAcids()) {
                standardAASet.addAminoAcid(aa, Pos.ANY_WHERE);
            }
            standardAASet.build();
        }
        return standardAASet;
    }

    private static AminoAcidSet stdAASetWithCarbamidomethylatedCys = null;

    /**
     * @return standard {@link AminoAcidSet} with Carbamidomethyl of Cys as fixed modification.
     */
    public static AminoAcidSet getCarbamidomethylCysAminoAcidSet()
    {
        if (stdAASetWithCarbamidomethylatedCys == null) {
            stdAASetWithCarbamidomethylatedCys = AminoAcidSet.getAminoAcidSet(Collections.singletonList(
                    PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C, Pos.ANY_WHERE, true)));
        }

        return stdAASetWithCarbamidomethylatedCys;
    }

    /**
     * Return the {@link AminoAcidSet} of given modifications
     *
     * @param mods modifications
     * @return an {@link AminoAcidSet}
     */
    public static AminoAcidSet getAminoAcidSet(List<PeptideMod> mods)
    {
        AminoAcidSet aaSet = new AminoAcidSet();
        for (AminoAcid aa : getStandardAminoAcidSet()) {
            aaSet.addAminoAcid(aa, Pos.ANY_WHERE);
        }

        aaSet.applyModifications(mods);
        aaSet.build();

        return aaSet;
    }

    /**
     * Return a {@link AminoAcidSet} of given modifications.
     *
     * @param mods {@link PeptideMod} array.
     * @return {@link AminoAcidSet} with the {@link PeptideMod} array.
     */
    public static AminoAcidSet getAminoAcidSet(PeptideMod... mods)
    {
        return getAminoAcidSet(Arrays.asList(mods));
    }

    /**
     * add custom amino acids, and apply mods to amino acids.
     *
     * @param mods             modifications
     * @param customAminoAcids custom amino acids
     */
    public static AminoAcidSet getAminoAcidSet(List<PeptideMod> mods, List<AminoAcid> customAminoAcids)
    {
        AminoAcidSet aaSet = new AminoAcidSet();
        for (AminoAcid aa : getStandardAminoAcidSet()) {
            aaSet.addAminoAcid(aa, Pos.ANY_WHERE);
        }

        for (AminoAcid aa : customAminoAcids) {
            aaSet.addAminoAcid(aa, Pos.ANY_WHERE);
        }

        aaSet.applyModifications(mods);
        aaSet.build();

        return aaSet;
    }

    /**
     * stored all modified amino acid
     */
    private ArrayListMultimap<AminoAcid, ModifiedAminoAcid> modAAMap = ArrayListMultimap.create();

    /**
     * Return a {@link ModifiedAminoAcid} of given target Amino acid
     *
     * @param targetAA {@link AminoAcid} to modified
     * @param mod      {@link PeptideMod} instance.
     */
    private ModifiedAminoAcid getModifiedAminoAcid(AminoAcid targetAA, PeptideMod mod)
    {

        // test if it is a new Modification
        List<ModifiedAminoAcid> modAAs = modAAMap.get(targetAA);
        for (ModifiedAminoAcid modAA : modAAs) {
            if (modAA.getModification() == mod.getModification())
                return modAA;
        }

        char modResidue = getModifiedResidue(targetAA.getStandardResidue());
        ModifiedAminoAcid modAA = new ModifiedAminoAcid(mod, targetAA, modResidue);
        modAAMap.put(targetAA, modAA);
        return modAA;
    }

    /**
     * set of symbols used for residues, helper for assign symbols for new modified amino acid.
     */
    private CharSet modResidueSet = new CharOpenHashSet();
    /**
     * next char for modified residue.
     */
    private char nextResidue = 128;

    /**
     * Returns a new residue for modified amino acid, for different modification at the same residue,
     * different char is used to distinguish them.
     * <p>
     * If the lower case is not occupied, use it fist.
     *
     * @param unmodResidue unmodified amino acid residue (namely standard amino acid residue)
     */
    char getModifiedResidue(char unmodResidue)
    {
        checkArgument(Character.isUpperCase(unmodResidue), "Only upper case standard amino acid is allowed");

        // first, test if lower case letter is available
        char lowerCaseR = Character.toLowerCase(unmodResidue);
        if (modResidueSet.add(lowerCaseR)) {
            return lowerCaseR;
        }

        // if not, use char value >= 128
        char symbol = this.nextResidue;
        nextResidue++;
        if (nextResidue >= Character.MAX_VALUE) {
            logger.error("Too many modifications!");
            throw new IllegalArgumentException("Too many modifications.");
        }
        return symbol;
    }


    /**
     * Set the probabilities of amino acids at given database.
     *
     * @param fastaSequence {@link FastaSequence} object.
     */
    public void setAminoAcidProbabilities(FastaSequence fastaSequence)
    {
        boolean success = true;
        int totalAACount = fastaSequence.getResidueCount();
        for (AminoAcid aa : allAminoAcidArr) {
            int count = fastaSequence.getResidueCount(aa.getStandardResidue());
            if (count == 0 && AminoAcid.isStandardAminoAcid(aa.getStandardResidue())) {
                success = false;
                break;
            }
            aa.setProbability(count / (double) totalAACount);
        }
        for (int i = 0; i < 128; i++) {
            if (!contains((char) i) && fastaSequence.getResidueCount((char) i) > 0) {
                logger.warn("Warning: Sequence database contains {} '{}', which is not an amino acid.", fastaSequence.getResidueCount((char) i), (char) i);
            }
        }

        if (!success) {
            logger.warn("Warning: database does not contain all standard amino acids. " +
                    "Probability 0.05 will be used for all amino acids.");
            for (AminoAcid aa : getAminoAcids())
                aa.setProbability(0.05f);
        }
    }
}
