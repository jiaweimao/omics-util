package omics.util.protein;

import omics.util.chem.Composition;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.Pos;
import omics.util.protein.mod.Specificity;

import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * class for modified amino acid.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 21 Jun 2019, 8:49 PM
 */
public class ModifiedAminoAcid extends AminoAcid
{
    private final PeptideMod peptideMod;
    private final AminoAcid targetAminoAcid;
    private final char standardResidue;
    private boolean isNTermMod = false;
    private boolean isCTermMod = false;
    private boolean isResidueMod = false;
    private boolean isTerminal = false;
    private boolean isFixed = false;

    /**
     * Constructor.
     *
     * @param mod     {@link PeptideMod}
     * @param residue a unique char for this modified amino acid.
     */
    public ModifiedAminoAcid(PeptideMod mod, AminoAcid targetAminoAcid, char residue)
    {
        super(residue, initName(targetAminoAcid, mod), new Composition((Composition) mod.getModification().getMass(), targetAminoAcid.getComposition()));
        this.peptideMod = mod;
        this.targetAminoAcid = targetAminoAcid;
        this.standardResidue = targetAminoAcid.getStandardResidue();

        setProbability(targetAminoAcid.getProbability());

        Specificity specificity = peptideMod.getSpecificity();
        if (mod.isFixed()) {
            isFixed = true;
        } else {
            if (specificity.getPosition().isNTerm())
                isNTermMod = true;
            else if (specificity.getPosition().isCTerm())
                isCTermMod = true;

            if (isCTermMod || isNTermMod)
                isTerminal = true;
            else
                isResidueMod = true;

            if (isResidueMod) {
                checkArgument(specificity.getAminoAcid() != AminoAcid.X);
            }
        }
    }

    private static String initName(AminoAcid targetAminoAcid, PeptideMod mod)
    {
        Specificity specificity = mod.getSpecificity();
        Pos position = specificity.getPosition();
        if (position.isNTerm())
            return "(" + mod.getModificationName() + ")_" + targetAminoAcid.getOneLetterCode();
        else if (position.isCTerm())
            return targetAminoAcid.getOneLetterCode() + "_(" + mod.getModificationName() + ")";
        else
            return targetAminoAcid.getOneLetterCode() + "(" + mod.getModificationName() + ")";
    }

    /**
     * @return the {@link Modification} of this modified amino acid.
     */
    public Modification getModification()
    {
        return peptideMod.getModification();
    }

    /**
     * @return the {@link PeptideMod} at this amino acid
     */
    public PeptideMod getPeptideMod()
    {
        return peptideMod;
    }

    /**
     * @return the standard target amino acid
     */
    public AminoAcid getTargetAminoAcid()
    {
        return targetAminoAcid;
    }

    @Override
    public boolean isModified()
    {
        return !isFixed;
    }

    /**
     * @return true if the amino acid is modified by N-terminal specific modification.
     */
    public boolean isNTermModified()
    {
        return isNTermMod;
    }

    /**
     * @return true if the amino acid is modified by C-terminal specific modification.
     */
    public boolean isCTermModified()
    {
        return isCTermMod;
    }

    @Override
    public char getStandardResidue()
    {

        return standardResidue;
    }

    public boolean isResidueModified()
    {
        return isResidueMod;
    }

    public boolean isTerminalModified()
    {
        return isTerminal;
    }

    /**
     * @return true if this amino acid is modified by fixed modification.
     */
    public boolean isFixed()
    {
        return isFixed;
    }

    @Override
    public String getSymbol()
    {
        return getName();
    }

    @Override
    public String toString()
    {

        return getName();
    }
}
