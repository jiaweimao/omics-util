package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import omics.util.chem.Composition;
import omics.util.chem.Weighable;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.ModificationList;
import omics.util.protein.mod.ModificationResolver;
import omics.util.protein.ms.FragmentType;
import omics.util.protein.ms.Ion;
import omics.util.utils.Pair;

import java.util.*;

import static omics.util.utils.ObjectUtils.*;

/**
 * Class represent peptide object.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 06 Oct 2017, 4:11 PM
 */
public class Peptide extends AminoAcidSequence implements Weighable
{
    private final double mass;

    /**
     * Construct an Peptide by copying the residues.
     *
     * @param firstResidue the first residue
     * @param residues     the rest of the residues
     */
    public Peptide(AminoAcid firstResidue, AminoAcid... residues)
    {
        super(firstResidue, residues);

        mass = calculateMass();
    }

    /**
     * Constructs an Peptide by copying the residues.
     *
     * @param residues the residues, the array cannot be empty
     */
    public Peptide(List<AminoAcid> residues)
    {
        super(residues.toArray(new AminoAcid[0]));

        mass = calculateMass();
    }

    /**
     * Copy constructor.
     *
     * @param src the peptide to copy.
     */
    public Peptide(AminoAcidSequence src)
    {
        super(src, 0, src.size());

        mass = calculateMass();
    }

    /**
     * Constructs a Peptide by copying the residues and modifications from
     * <code>src</code>. The new Peptide begins at the specified
     * <code>beginIndex</code> and extends to the residue at index
     * <code>endIndex - 1</code>.
     *
     * @param src        the AminoAcidSequence from which to copy the sequence and modifications
     * @param beginIndex beginIndex the beginning index, inclusive.
     * @param endIndex   the ending index, exclusive.
     */
    public Peptide(AminoAcidSequence src, int beginIndex, int endIndex)
    {
        super(src, beginIndex, endIndex);

        mass = calculateMass();
    }

    /**
     * Constructs an Peptide from the list of residues with the modifications
     * contained in side chain and term mod map.
     *
     * @param residues        the residues, this list must have at least one amino acid
     * @param sideChainModMap the side chain modifications, this map can be empty
     * @param termModMap      the terminal modifications, this map can be empty
     */
    public Peptide(List<AminoAcid> residues, ListMultimap<Integer, Modification> sideChainModMap,
            ListMultimap<ModAttachment, Modification> termModMap)
    {
        super(residues, sideChainModMap, termModMap);

        mass = calculateMass();
    }

    public Peptide(Peptide peptide, Set<AminoAcid> oldAAs, AminoAcid newAa)
    {
        super(peptide, oldAAs, newAa);

        mass = calculateMass();
    }

    /**
     * remove amino acid that is not standard amino acid.
     *
     * @param raw peptide sequence
     * @return peptide sequence after remove non standard amino acid.
     */
    public static String clearSequence(String raw)
    {
        char[] array = new char[raw.length()];
        int i = 0;
        for (char aa : raw.toCharArray()) {
            if (AminoAcid.isStandardAminoAcid(aa)) {
                array[i] = aa;
                i++;
            }
        }
        return new String(array, 0, i);
    }

    /**
     * Parse s to build a Peptide.
     * <p/>
     * The expected format is a string of amino acids. Each amino acid can have
     * an optional list of modifications. The modifications can either be
     * formulas as defined by Composition, or PSI-MS Names found in Uni Mod.
     *
     * <pre>
     * PEPTIDE              : the peptide PEPTIDE
     * PEPTM(O)IDE          : the peptide PEPTIDE with an oxidation on M
     * PEPTM(O, Phospho)IDE : the peptide PEPTIDE with an oxidation and phosphorylation on M
     * (Acetyl)_PEPTIDE_(O) : the peptide PEPTIDE with a n-term Acetylation and c_term oxidation modification
     * </pre>
     *
     * @param s the string
     * @return the Peptide
     * @throws PeptideParseException if s is not a valid string
     */
    public static Peptide parse(String s)
    {
        return PeptideParser.getInstance().parsePeptide(s);
    }

    /**
     * Parse <code>s</code> to build a Peptide using the modResolver to convert
     * modification strings to Modification
     *
     * @param s           the string
     * @param modResolver Function to convert modification string to Modification
     * @return the Peptide
     * @throws PeptideParseException if s is not a valid string
     */
    public static Peptide parse(String s, ModificationResolver modResolver)
    {
        return PeptideParser.getInstance().parsePeptide(s, modResolver);
    }

    /**
     * return the peptide form which does not contains given ptms.
     *
     * @param aPeptide a peptide
     * @param mods     inspected Modifications.
     * @return a non modified version of the peptide
     */
    public static Peptide removeMod(Peptide aPeptide, Set<Modification> mods)
    {
        Builder builder = new Builder(aPeptide.toSymbolString());

        if (aPeptide.hasModificationAt(ModAttachment.N_TERM)) {
            ModificationList modList = aPeptide.getModifications(EnumSet.of(ModAttachment.N_TERM));
            modList.stream().filter(mod -> !mods.contains(mod)).forEach(mod -> builder.addModification(ModAttachment
                    .N_TERM, mod));
        }

        for (int i = 0; i < aPeptide.size(); i++) {
            if (aPeptide.hasModificationAt(i, EnumSet.of(ModAttachment.SIDE_CHAIN))) {
                ModificationList modList = aPeptide.getModificationsAt(i, EnumSet.of(ModAttachment.SIDE_CHAIN));
                for (Modification mod : modList) {
                    if (!mods.contains(mod)) {
                        builder.addModification(i, mod);
                    }
                }
            }
        }
        if (aPeptide.hasModificationAt(ModAttachment.C_TERM)) {
            ModificationList modList = aPeptide.getModifications(EnumSet.of(ModAttachment.C_TERM));
            modList.stream().filter(mod -> !mods.contains(mod)).forEach(mod -> builder.addModification(ModAttachment
                    .C_TERM, mod));
        }
        return builder.build();
    }

    /**
     * Return the peptide form without given modification.
     *
     * @param aPeptide a peptide
     * @param mod      inspected Modification, modification is identified by its id.
     * @return a non modified version of the peptide
     */
    public static Pair<Peptide, Integer> removeModification(Peptide aPeptide, Modification mod)
    {
        Builder builder = new Builder(aPeptide.toSymbolString());

        int count = 0;
        if (aPeptide.hasModificationAt(ModAttachment.N_TERM)) {
            ModificationList modList = aPeptide.getModifications(ModAttachment.nTermSet);
            for (Modification modification : modList) {
                if (modification.getTitle().equals(mod.getTitle()))
                    count++;
                else
                    builder.addModification(ModAttachment.N_TERM, modification);
            }
        }

        for (int i = 0; i < aPeptide.size(); i++) {
            if (aPeptide.hasModificationAt(i, ModAttachment.sideChainSet)) {
                ModificationList modList = aPeptide.getModificationsAt(i, ModAttachment.sideChainSet);
                for (Modification modification : modList) {
                    if (modification.getTitle().equals(mod.getTitle()))
                        count++;
                    else
                        builder.addModification(i, mod);
                }
            }
        }
        if (aPeptide.hasModificationAt(ModAttachment.C_TERM)) {
            ModificationList modList = aPeptide.getModifications(ModAttachment.cTermSet);
            for (Modification modification : modList) {
                if (modification.getTitle().equals(mod.getTitle()))
                    count++;
                else
                    builder.addModification(ModAttachment.C_TERM, modification);
            }
        }
        return Pair.create(builder.build(), count);
    }

    /**
     * return the peptide form which does not contains given ptms.
     *
     * @param aPeptide a peptide
     * @param mods     inspected Modifications names
     * @return a non modified version of the peptide
     */
    public static Peptide removeModByName(Peptide aPeptide, Set<String> mods)
    {
        Builder builder = new Builder(aPeptide.toSymbolString());

        if (aPeptide.hasModificationAt(ModAttachment.N_TERM)) {
            ModificationList modList = aPeptide.getModifications(EnumSet.of(ModAttachment.N_TERM));
            for (Modification mod : modList) {
                if (!mods.contains(mod.getTitle())) {
                    builder.addModification(ModAttachment.N_TERM, mod);
                }
            }
        }

        for (int i = 0; i < aPeptide.size(); i++) {
            if (aPeptide.hasModificationAt(i, EnumSet.of(ModAttachment.SIDE_CHAIN))) {
                ModificationList modList = aPeptide.getModificationsAt(i, EnumSet.of(ModAttachment.SIDE_CHAIN));
                for (Modification mod : modList) {
                    if (!mods.contains(mod.getTitle())) {
                        builder.addModification(i, mod);
                    }
                }
            }
        }
        if (aPeptide.hasModificationAt(ModAttachment.C_TERM)) {
            ModificationList modList = aPeptide.getModifications(EnumSet.of(ModAttachment.C_TERM));
            for (Modification mod : modList) {
                if (!mods.contains(mod.getTitle())) {
                    builder.addModification(ModAttachment.C_TERM, mod);
                }
            }
        }
        return builder.build();
    }

    /**
     * @return mass of this peptide.
     */
    private double calculateMass()
    {
        if (hasAmbiguousAminoAcids()) {
            return -1;
        } else {
            final double peptMass = calculateMonomerMassSum() + Composition.H2O.getMolecularMass();
            checkState(peptMass > 0);
            return peptMass;
        }
    }

    /**
     * Returns a new peptide that is a sub sequence of this peptide. The sub
     * sequence begins at the specified <code>beginIndex</code> and extends to
     * the amino acid at index <code>endIndex - 1</code>. Thus the length of the
     * sub sequence is <code>endIndex-beginIndex</code>.
     * <p/>
     *
     * @param beginIndex the beginning index, inclusive.
     * @param endIndex   the ending index, exclusive.
     * @return the specified substring.
     * @throws IndexOutOfBoundsException if the <code>beginIndex</code> is negative, or <code>endIndex</code> is larger
     *                                   than the length of this <code>Peptide</code> object, or <code>beginIndex</code>
     *                                   is larger than <code>endIndex</code>.
     */
    public Peptide subSequence(int beginIndex, int endIndex)
    {
        checkElementIndex(beginIndex, size());
        checkPositionIndex(endIndex, size());

        return new Peptide(this, beginIndex, endIndex);
    }

    /**
     * Returns a new peptide fragment that is a subsequence of this peptide. The subsequence begins at the specified
     * <code>beginIndex</code> and extends to the amino acid at index <code>endIndex - 1</code>. Thus the length of
     * the subsequence is <code>endIndex-beginIndex</code>.
     *
     * @param beginIndex   the beginning index, inclusive.
     * @param endIndex     the ending index, exclusive.
     * @param fragmentType the type of the fragment
     * @return a new PeptideFragment with the specified sub sequence.
     * @throws IndexOutOfBoundsException if the <code>beginIndex</code> is negative, or <code>endIndex</code> is larger
     *                                   than the length of this <code>String</code> object, or <code>beginIndex</code>
     *                                   is larger than <code>endIndex</code>.
     */
    public PeptideFragment createFragment(int beginIndex, int endIndex, FragmentType fragmentType)
    {
        checkNotNull(fragmentType);

        if (fragmentType == FragmentType.FORWARD && beginIndex != 0)
            throw new IllegalArgumentException("begin=" + beginIndex + ", end=" + endIndex + ": cannot create " + FragmentType.FORWARD + " fragment!");
        else if (fragmentType == FragmentType.REVERSE && endIndex != size())
            throw new IllegalArgumentException("begin=" + beginIndex + ", end=" + endIndex + ": cannot create " + FragmentType.REVERSE + " fragment!");

        int size = size();
        checkElementIndex(beginIndex, size);
        checkElementIndex(endIndex, size + 1);

        return new PeptideFragment(fragmentType, this, beginIndex, endIndex);
    }

    /**
     * Create PeptideFragment that is sub sequence for the give ion type and
     * residue number
     * <p>
     * Given peptide CERVILAS createFragment(y, 1) will return S,
     * createFragment(b, 1) will return C createFragment(b, 3) will return CER.
     * <p>
     * If the IonType is i (immonium) the residue at residueNumber - 1 is
     * returned: createFragment(i, 1) is C, createFragment(i, 2) is E
     *
     * @param peptideIonType the type of the ion, only accepts ions that are FORWARD, REVERSE and MONOMER fragments
     * @param residueNumber  the index of the residue. The index of the first residue is 1
     * @return the new PeptideFragment
     */
    public PeptideFragment createFragment(Ion peptideIonType, int residueNumber)
    {
        checkNotNull(peptideIonType);
        checkArgument(residueNumber > 0);
        if (residueNumber > size())
            throw new IllegalStateException(peptideIonType.toString() + residueNumber + " from " + toString());

        FragmentType fragmentType = peptideIonType.getFragmentType();
        switch (fragmentType) {
            case FORWARD:
                return createFragment(0, residueNumber, fragmentType);
            case REVERSE:
                return createFragment(size() - residueNumber, size(), fragmentType);
            case MONOMER:
                return createFragment(residueNumber - 1, residueNumber, fragmentType);
            default:
                throw new IllegalArgumentException(
                        "Cannot create a fragment for " + fragmentType + " from ionType " + peptideIonType);
        }
    }

    /**
     * Create a sub sequence for the give ion type and residue number
     * <p>
     * Given peptide CERVILAS subSequence(y, 1) will return S, subSequence(b, 1)
     * will return C subSequence(b, 3) will return CER.
     * <p>
     * If the IonType is i (immonium) the residue at residueNumber - 1 is
     * returned: subSequence(i, 1) is C, subSequence(i, 2) is E
     *
     * @param ion           the type of the ion, only accepts ions that are FORWARD, REVERSE and MONOMER fragments
     * @param residueNumber the index of the residue. The index of the first residue is 1
     * @return the sub sequence
     */
    public Peptide subSequence(Ion ion, int residueNumber)
    {
        checkNotNull(ion);
        checkArgument(residueNumber > 0, "Residue number has to be > 0, was " + residueNumber);
        checkArgument(residueNumber <= size(), ion.toString() + residueNumber + " from " + toString());

        switch (ion.getFragmentType()) {
            case FORWARD:
                return subSequence(0, residueNumber);
            case REVERSE:
                return subSequence(size() - residueNumber, size());
            case MONOMER:
                return subSequence(residueNumber - 1, residueNumber);
            default:
                throw new IllegalArgumentException("Cannot create a fragment for "
                        + ion.getFragmentType() + " from ionType " + ion);
        }
    }

    /**
     * Calculate the m/z of this peptide at the given charge
     *
     * @param charge the charge
     * @return the m/z of this peptide
     */
    public double calculateMz(int charge)
    {
        checkArgument(charge != 0, "Cannot calculate m/z, charge was 0");
        if (mass == -1)
            throw new IllegalStateException(
                    "m/z cannot be calculated for peptides that have ambiguous amino acids. The peptide is "
                            + toString() + " which contains " + getAmbiguousAminoAcids());

        return Ion.calcMz(mass, charge);
    }

    /**
     * as this method is not used usually, so the mass is calculated dynamically.
     * it is maybe slow if you call it frequently
     *
     * @return the average mass
     */
    public double getAverageMass()
    {
        if (hasAmbiguousAminoAcids()) {
            return -1;
        } else {
            final double peptMass = calculateAverageMassSum() + Composition.H2O.getAverageMass();
            checkState(peptMass > 0);
            return peptMass;
        }
    }

    /**
     * Returns the mass of this peptide
     */
    public double getMolecularMass()
    {
        if (mass == -1)
            throw new IllegalStateException(
                    "molecular mass cannot be calculated for peptides that have ambiguous amino acids. The peptide is "
                            + toString() + " which contains " + getAmbiguousAminoAcids());
        return mass;
    }

    public Peptide replace(Set<AminoAcid> oldAAs, AminoAcid newAa)
    {
        return new Peptide(this, oldAAs, newAa);
    }


    public static class Builder
    {
        private final List<AminoAcid> sequence;
        private final ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        private final ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

        /**
         * Construct a peptide builder initialised by parsing the
         * <code>peptide</code> string
         *
         * @param peptide the initial state of the builder
         */
        public Builder(String peptide)
        {
            checkNotNull(peptide);
            sequence = new ArrayList<>(peptide.length());
            PeptideParser.getInstance().parsePeptide(peptide, sequence, sideChainModMap, termModMap);
        }

        /**
         * Construct a PeptideBuilder initialised to contain the amino acids from
         * <code>sequence</code>
         *
         * @param sequence the initial amino acids
         */
        public Builder(List<AminoAcid> sequence)
        {
            checkNotNull(sequence);
            this.sequence = new ArrayList<>(sequence);
        }

        /**
         * Construct a PeptideBuilder initialised to contain the amino acids from
         * <code>seq</code>
         *
         * @param seq the initial amino acids
         */
        public Builder(AminoAcid... seq)
        {
            checkNotNull(seq);
            sequence = new ArrayList<>(seq.length);
            Collections.addAll(sequence, seq);
        }

        /**
         * Constructs an PeptideBuilder by copying the residues from
         * <code>residues</code> starting at <code>fromInclusive</code> to
         * <code>toExclusive</code>.
         *
         * @param residues      the residues, the array cannot be empty
         * @param fromInclusive the beginning index, inclusive
         * @param toExclusive   the ending index, exclusive
         */
        public Builder(AminoAcid[] residues, int fromInclusive, int toExclusive)
        {
            checkNotNull(residues);
            checkElementIndex(fromInclusive, residues.length);
            checkPositionIndex(toExclusive, residues.length);
            checkArgument(toExclusive >= fromInclusive);

            sequence = new ArrayList<>(toExclusive - fromInclusive + 1);
            sequence.addAll(Arrays.asList(residues).subList(fromInclusive, toExclusive));
        }

        /**
         * Construct a PeptideBuilder initialised to contain the amino acids
         * sequence and modifications copied from the <code>peptide</code>
         *
         * @param peptide the peptide from which to copy the seqence and modifications
         */
        public Builder(Peptide peptide)
        {
            checkNotNull(peptide);
            sequence = new ArrayList<>(peptide.size());

            for (int i = 0; i < peptide.size(); i++) {

                sequence.add(peptide.getSymbol(i));

                for (Modification mod : peptide.getModificationsAt(i, EnumSet.of(ModAttachment.SIDE_CHAIN))) {
                    sideChainModMap.put(i, mod);
                }
            }

            termModMap.putAll(ModAttachment.N_TERM, peptide.getModifications(EnumSet.of(ModAttachment.N_TERM)));
            termModMap.putAll(ModAttachment.C_TERM, peptide.getModifications(EnumSet.of(ModAttachment.C_TERM)));
        }

        /**
         * Add a the <code>modification</code> to either end of the the peptide. The
         * end is specified busing the <code>modAttachment</code>
         *
         * @param modAttachment specifies which end of the peptide the modification is to be added
         * @param modification  the Modification that is to be added
         * @return the builder
         */
        public Builder addModification(ModAttachment modAttachment, Modification modification)
        {
            checkNotNull(modAttachment);
            checkNotNull(modification);
            checkArgument(modAttachment != ModAttachment.SIDE_CHAIN,
                    "To add a side chain modification a residue index is required. Use addModification(int index, " +
                            "Modification modification) instead");

            termModMap.put(modAttachment, modification);

            return this;
        }

        /**
         * Parse the <code>peptideSequence</code> string and add the sequence and
         * modifications to this builder
         *
         * @param peptideSequence the string to parse
         * @return the builder
         */
        public Builder parseAndAdd(String peptideSequence)
        {
            checkNotNull(peptideSequence);
            PeptideParser.getInstance().parsePeptide(peptideSequence, sequence, sideChainModMap, termModMap);
            return this;
        }

        /**
         * Add an amino acid
         *
         * @param aminoAcid the amino acid to add
         * @return this builder
         */
        public Builder add(AminoAcid aminoAcid)
        {
            sequence.add(aminoAcid);
            return this;
        }

        /**
         * Add a the <code>modification</code> to the side chain of residue at
         * <code>index</code>
         *
         * @param index        the index of the reside where the Modification is to be added
         * @param modification the ModificationMatch that is to be added
         * @return this builder
         */
        public Builder addModification(int index, Modification modification)
        {
            checkNotNull(modification);

            sideChainModMap.put(index, modification);
            return this;
        }

        /**
         * Add the <code>modification</code> to all amino acids that are equal to
         * <code>aminoAcid</code>
         *
         * @param modification the modification to add
         * @param aminoAcid    the amino acid to which the modification is to be added
         * @return this builder
         */
        public Builder addModificationToAll(Modification modification, AminoAcid aminoAcid)
        {
            checkNotNull(aminoAcid);
            checkNotNull(modification);

            for (int i = 0, sequenceSize = sequence.size(); i < sequenceSize; i++) {

                AminoAcid aa = sequence.get(i);

                if (aa.equals(aminoAcid)) {

                    addModification(i, modification);
                }
            }

            return this;
        }

        /**
         * Add all of the Modification's in <code>modifications</code> to all amino
         * acids that are equal to <code>aminoAcid</code>
         *
         * @param modifications the modifications to add
         * @param aminoAcid     the amino acid to which the modification is to be added
         * @return this builder
         */
        public Builder addModificationToAll(Collection<Modification> modifications, AminoAcid aminoAcid)
        {
            checkNotNull(aminoAcid);
            checkNotNull(modifications);

            for (int i = 0, sequenceSize = sequence.size(); i < sequenceSize; i++) {

                AminoAcid aa = sequence.get(i);

                if (aa.equals(aminoAcid)) {

                    sideChainModMap.putAll(i, modifications);
                }
            }

            return this;
        }

        /**
         * Reset this builder
         */
        public void clear()
        {
            sequence.clear();
            sideChainModMap.clear();
            termModMap.clear();
        }

        /**
         * Return the number of amino acids
         *
         * @return the number of amino acids
         */
        public int size()
        {
            return sequence.size();
        }

        /**
         * Build a Peptide
         *
         * @return the new Peptide
         */
        public Peptide build()
        {
            if (sequence.isEmpty())
                throw new IllegalStateException("Cannot build a peptide with empty sequence");

            Peptide peptide = new Peptide(sequence, sideChainModMap, termModMap);
            clear();
            return peptide;
        }

        /**
         * Build a PeptideFragment
         *
         * @param fragmentType the fragment type for the PeptideFragment
         * @return the new PeptideFragment
         */
        public PeptideFragment buildFragment(FragmentType fragmentType)
        {
            if (sequence.isEmpty())
                throw new IllegalStateException("Cannot build a peptide with empty sequence");

            PeptideFragment fragment = new PeptideFragment(fragmentType, sequence, sideChainModMap, termModMap);
            clear();
            return fragment;
        }
    }
}
