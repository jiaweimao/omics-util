package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.ModificationResolver;
import omics.util.protein.mod.NeutralLoss;
import omics.util.protein.ms.FragmentType;
import omics.util.protein.ms.Ion;

import java.util.ArrayList;
import java.util.List;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * A fragment of Peptide.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 16 Mar 2017, 7:36 PM
 */
public class PeptideFragment extends AminoAcidSequence
{
    private final FragmentType fragmentType;

    /**
     * Construct an PeptideFragment by copying the residues
     *
     * @param fragmentType the fragment type
     * @param firstResidue the first residue
     * @param residues     the rest of the residues
     */
    public PeptideFragment(FragmentType fragmentType, AminoAcid firstResidue, AminoAcid... residues)
    {
        super(firstResidue, residues);

        checkNotNull(fragmentType);
        this.fragmentType = fragmentType;
    }

    /**
     * Constructor that copies the residues and modifications from src. The new PeptideFragment begins at
     * the specified <code>beginIndex</code> and extends to the residue at index <code>endIndex - 1</code>.
     * <p>
     * Thus the length of the new PeptideFragment is <code>endIndex-beginIndex</code>.
     *
     * @param fragmentType the fragment type
     * @param src          the PeptideFragment from which to copy the sequence and modifications
     * @param beginIndex   the beginning index, inclusive.
     * @param endIndex     the ending index, exclusive.
     */
    public PeptideFragment(FragmentType fragmentType, AminoAcidSequence src, int beginIndex, int endIndex)
    {
        super(src, beginIndex, endIndex);

        checkNotNull(fragmentType);

        switch (fragmentType) {
            case FORWARD:
                checkArgument(0 == beginIndex, "Forward fragments have to start at 0, begin index was " + beginIndex);
                break;
            case REVERSE:
                checkArgument(src.size() == endIndex, "Reverse fragments have to end at " + src.size() + " , end index was " + endIndex);
                break;
            case MONOMER:
                checkArgument(endIndex - beginIndex == 1);
                break;
            case INTERNAL:
                checkArgument(beginIndex > 0 && endIndex < src.size());
                break;
            case INTACT:
                break;
            default:
                throw new IllegalArgumentException("Unknown fragment type " + fragmentType);
        }

        this.fragmentType = fragmentType;
    }

    /**
     * Constructs an PeptideFragment from the list of residues with the
     * modifications contained in side chain and term mod map.
     *
     * @param fragmentType    the fragment type
     * @param residues        the residues, this list must have at least one amino acid
     * @param sideChainModMap the side chain modifications, this map can be empty
     * @param termModMap      the terminal modifications, this map can be empty
     */
    public PeptideFragment(FragmentType fragmentType, List<AminoAcid> residues,
                           ListMultimap<Integer, Modification> sideChainModMap,
                           ListMultimap<ModAttachment, Modification> termModMap)
    {
        super(residues, sideChainModMap, termModMap);

        checkNotNull(fragmentType);
        this.fragmentType = fragmentType;
    }

    /**
     * Parse s to build a PeptideFragment.
     * <p/>
     * The expected format is a string of amino acids. Each amino acid can have
     * an optional list of modifications. The modifications can either be
     * formulas as defined by Composition, or PSI-MS Names found in Uni Mod.
     * <p>
     * <pre>
     * PEPTIDE              : the peptide PEPTIDE
     * PEPTM(O)IDE          : the peptide PEPTIDE with an oxidation on M
     * PEPTM(O, Phospho)IDE : the peptide PEPTIDE with an oxidation and phosphorylation on M
     * (Acetyl)_PEPTIDE_(O) : the peptide PEPTIDE with a n-term Acetylation and c_term oxidation modification
     * </pre>
     *
     * @param s            the string
     * @param fragmentType the fragment type
     * @return the PeptideFragment
     * @throws PeptideParseException if s is not a valid string
     */
    public static PeptideFragment parse(final String s, final FragmentType fragmentType)
    {
        List<AminoAcid> sequence = new ArrayList<>();
        ListMultimap<Integer, Modification> sideChainMatchMap = ArrayListMultimap.create();
        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

        PeptideParser.getInstance().parsePeptide(s, sequence, sideChainMatchMap, termModMap);

        return new PeptideFragment(fragmentType, sequence, sideChainMatchMap, termModMap);
    }

    /**
     * Parse <code>s</code> to build a PeptideFragment using the modResolver to
     * convert modification strings to Modification
     *
     * @param s            the string
     * @param fragmentType the fragment type
     * @param modResolver  Function to convert modification string to Modification
     * @return the Peptide
     * @throws PeptideParseException if s is not a valid string
     */
    public static PeptideFragment parse(final String s, final FragmentType fragmentType,
                                        ModificationResolver modResolver)
    {
        List<AminoAcid> sequence = new ArrayList<>();
        ListMultimap<Integer, Modification> sideChainMatchMap = ArrayListMultimap.create();
        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

        PeptideParser.getInstance().parsePeptide(s, sequence, sideChainMatchMap, termModMap, modResolver);

        return new PeptideFragment(fragmentType, sequence, sideChainMatchMap, termModMap);
    }

    /**
     * Return the fragmentType
     *
     * @return the fragmentType
     */
    public FragmentType getFragmentType()
    {
        return fragmentType;
    }

    /**
     * Calculate the m/z of this PeptideFragment given the <code>charge</code>
     * and <code>ionType</code>
     *
     * @param ion    the ionType
     * @param charge the charge
     * @return the m/z
     */
    public double calcMz(Ion ion, int charge)
    {
        checkArgument(ion.getFragmentType() == fragmentType);
        checkNotNull(ion);
        checkArgument(charge != 0, "Cannot calculate m/z, charge was 0");

        return Ion.calcMz(calcMass(ion), charge);
    }

    /**
     * Calculate the m/z of this PeptideFragment given the <code>charge</code>
     * and <code>ionType</code> and <code>neutralLoass</code>
     *
     * @param peptideIonType the ionType
     * @param neutralLoss    the neutralLoss
     * @param charge         the charge
     * @return the m/z
     */
    public double calcMz(Ion peptideIonType, NeutralLoss neutralLoss, int charge)
    {
        checkNotNull(peptideIonType);
        checkArgument(peptideIonType.getFragmentType() == fragmentType);

        return Ion.calcMz(calcMass(peptideIonType, neutralLoss), charge);
    }

    /**
     * Calculate the Peptide Fragment mass with given neutral loss
     *
     * @param ion         the PeptideIonType
     * @param neutralLoss a NeutralLoss
     * @return the mass
     */
    public double calcMass(Ion ion, NeutralLoss neutralLoss)
    {
        checkNotNull(ion);
        checkArgument(fragmentType.equals(ion.getFragmentType()));

        double mass = calculateMonomerMassSum();
        mass += ion.getDeltaMass();
        mass -= neutralLoss.getMolecularMass();
        return mass;
    }

    /**
     * Calculate the mass of this PeptideFragment given the <code>PeptideIonType</code>
     *
     * @param ion the ionType
     * @return the mass
     */
    public double calcMass(Ion ion)
    {
        checkNotNull(ion);
        checkArgument(fragmentType.equals(ion.getFragmentType()));

        double mass = calculateMonomerMassSum();
        mass += ion.getDeltaMass();

        return mass;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        PeptideFragment fragment = (PeptideFragment) o;

        return fragmentType == fragment.fragmentType;
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        result = 31 * result + fragmentType.hashCode();
        return result;
    }
}
