package omics.util.protein;

import omics.util.chem.Composition;
import omics.util.chem.Weighable;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.Pos;
import omics.util.protein.mod.Specificity;

import java.util.Comparator;
import java.util.Objects;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * A modification for database search.
 *
 * @author JiaweiMao
 * @version 1.6.4
 * @since 24 Jun 2019, 8:59 PM
 */
public class PeptideMod implements Weighable, Comparator<PeptideMod>
{
    /**
     * Create a {@link PeptideMod} instance.
     *
     * @param modification {@link Modification} to search, only accept composition as mass.
     * @param isFixed      true if it is a fixed modification
     * @param specificity  {@link Specificity} for the modification position
     */
    public static PeptideMod of(Modification modification, Specificity specificity, boolean isFixed)
    {
        return new PeptideMod(modification, specificity, isFixed);
    }

    /**
     * Create PeptideMod without NeutralLoss.
     *
     * @param modification a {@link Modification}
     * @param aminoAcid    target {@link AminoAcid}, {@link AminoAcid#X} represents any amino acid.
     * @param pos          {@link Pos} instance.
     * @param isFixed      true if the modification is fixed.
     */
    public static PeptideMod of(Modification modification, AminoAcid aminoAcid, Pos pos, boolean isFixed)
    {
        return new PeptideMod(modification, new Specificity(aminoAcid, pos), isFixed);
    }

    /**
     * Create PeptideMod at {@link Pos#ANY_WHERE} without NeutralLoss.
     *
     * @param modification a {@link Modification}
     * @param aminoAcid    an {@link AminoAcid}
     * @param isFixed      true if the modification is fixed.
     */
    public static PeptideMod of(Modification modification, AminoAcid aminoAcid, boolean isFixed)
    {
        return new PeptideMod(modification, new Specificity(aminoAcid, Pos.ANY_WHERE), isFixed);
    }

    /**
     * Create a variable modification
     *
     * @param modification {@link Modification} instance
     * @param aminoAcid    {@link AminoAcid} instance
     * @param pos          {@link Pos}
     */
    public static PeptideMod of(Modification modification, AminoAcid aminoAcid, Pos pos)
    {
        return of(modification, aminoAcid, pos, false);
    }

    /**
     * Create a variable modification at specific amino acid
     *
     * @param modification {@link Modification} instance
     * @param aminoAcid    {@link AminoAcid}
     */
    public static PeptideMod of(Modification modification, AminoAcid aminoAcid)
    {
        return of(modification, aminoAcid, Pos.ANY_WHERE);
    }

    private final Modification modification;
    private final Specificity specificity;
    private final boolean isFixed;

    /**
     * Constructor.
     *
     * @param modification {@link Modification} to search, only accept composition as mass.
     * @param isFixed      true if it is a fixed modification
     * @param specificity  {@link Specificity} for the modification position
     */
    public PeptideMod(Modification modification, Specificity specificity, boolean isFixed)
    {
        checkNotNull(modification);
        checkNotNull(specificity);

        this.modification = modification;
        this.isFixed = isFixed;
        this.specificity = specificity;
    }

    /**
     * @return target amino acid for this modification.
     */
    public AminoAcid getTargetAminoAcid()
    {
        return specificity.getAminoAcid();
    }

    /**
     * @return the modifications
     */
    public Modification getModification()
    {
        return modification;
    }

    /**
     * @return modification name.
     */
    public String getModificationName()
    {
        return modification.getTitle();
    }

    /**
     * @return true if this is a fixed modification
     */
    public boolean isFixed()
    {
        return isFixed;
    }

    /**
     * @return {@link Pos} of the modification
     */
    public Pos getPosition()
    {
        return specificity.getPosition();
    }

    /**
     * @return {@link Specificity}
     */
    public Specificity getSpecificity()
    {
        return specificity;
    }

    /**
     * @return the composition of this modification.
     */
    public Composition getComposition()
    {
        return (Composition) modification.getMass();
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder(modification.getTitle());
        builder.append(" (");
        AminoAcid aminoAcid = specificity.getAminoAcid();
        Pos pos = specificity.getPosition();
        if (aminoAcid == AminoAcid.N_TERM || aminoAcid == AminoAcid.C_TERM) {
            if (pos.isProteinTerm()) {
                builder.append(pos.getName());
            } else {
                builder.append(aminoAcid.getShortName());
            }
        } else {
            builder.append(aminoAcid.getSymbol());

            if (pos != Pos.ANY_WHERE) {
                builder.append(", ").append(pos.getName());
            }
        }

        builder.append(")");

        return builder.toString();
    }

    @Override
    public int compare(PeptideMod o1, PeptideMod o2)
    {
        return Double.compare(o1.getMolecularMass(), o2.getMolecularMass());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeptideMod that = (PeptideMod) o;
        return modification.equals(that.modification) &&
                specificity.equals(that.specificity);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(modification, specificity);
    }

    @Override
    public double getMolecularMass()
    {
        return modification.getMolecularMass();
    }
}
