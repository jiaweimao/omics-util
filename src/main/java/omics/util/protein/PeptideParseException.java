package omics.util.protein;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 23 Jan 2018, 8:51 AM
 */
public class PeptideParseException extends IllegalArgumentException {

    public PeptideParseException(String message) {
        super(message);
    }

    public PeptideParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
