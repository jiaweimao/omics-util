package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import it.unimi.dsi.fastutil.chars.CharOpenHashSet;
import it.unimi.dsi.fastutil.chars.CharSet;
import omics.util.protein.mod.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Parser of peptide
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 16 Mar 2017, 8:51 AM
 */
public class PeptideParser
{
    private static PeptideParser peptideParser;
    private final CharSet aaChars = new CharOpenHashSet();
    private final ModificationResolver defaultModResolver = new CompositeModResolver(new PTMResolver(),
            new NumericModificationResolver());

    private PeptideParser()
    {
        for (AminoAcid aa : AminoAcid.getAllAminoAcids()) {

            aaChars.add(aa.getSymbol().charAt(0));
        }
    }

    public static PeptideParser getInstance()
    {
        if (peptideParser == null)
            peptideParser = new PeptideParser();

        return peptideParser;
    }

    public Peptide parsePeptide(String s)
    {
        return parsePeptide(s, defaultModResolver);
    }

    Peptide parsePeptide(String s, ModificationResolver modResolver)
    {
        List<AminoAcid> residues = new ArrayList<>(s.length());
        ListMultimap<Integer, Modification> modMap = ArrayListMultimap.create();
        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

        parsePeptide(s, residues, modMap, termModMap, modResolver);

        return new Peptide(residues, modMap, termModMap);
    }

    /**
     * Extract the sequence and modification information from <code>s</code>
     *
     * @param s               the string to parse
     * @param sequence        list to hold the AA sequence
     * @param sideChainModMap multi map to hold the side chain modifications
     * @param termModMap      multi map to hold the n-term and c-term modifications
     * @throws PeptideParseException if s is not a valid string
     */
    public void parsePeptide(String s, List<AminoAcid> sequence, ListMultimap<Integer, Modification> sideChainModMap,
            ListMultimap<ModAttachment, Modification> termModMap)
    {
        parsePeptide(s, sequence, sideChainModMap, termModMap, defaultModResolver);
    }

    /**
     * Extract the sequence and modification information from <code>s</code>
     *
     * @param s               the string to parse
     * @param sequence        list to hold the AA sequence
     * @param sideChainModMap multi map to hold the side chain modifications
     * @param termModMap      multi map to hold the n-term and c-term modifications
     * @param modResolver     function to convert the modification strings found in s to Modification
     * @throws PeptideParseException if s is not a valid string
     */
    void parsePeptide(String s, List<AminoAcid> sequence, ListMultimap<Integer, Modification> sideChainModMap,
            ListMultimap<ModAttachment, Modification> termModMap, ModificationResolver modResolver)
    {
        String input = s;
        s = stripTerminalMods(s, termModMap, modResolver);

        int bracketCount = 0;
        int bracketStartIndex = -1;
        int lastModIndex = -1;
        for (int i = 0; i < s.length(); i++) {

            char c = s.charAt(i);

            if (bracketCount == 0 && aaChars.contains(c)) {

                sequence.add(AminoAcid.getAminoAcid(c));
            } else if (c == '(') {

                if (bracketCount == 0)
                    bracketStartIndex = i;
                bracketCount += 1;
            } else if (c == ')') {

                bracketCount -= 1;
                if (bracketCount == 0) {

                    if (lastModIndex == sequence.size() - 1) // Prevent A(mod1)(mod2) from being legal
                        throw new PeptideParseException(input + " has illegal modification format");
                    parseMods(s.substring(bracketStartIndex + 1, i), sideChainModMap, sequence.size() - 1, modResolver);
                    lastModIndex = sequence.size() - 1;
                }
            } else if (bracketCount == 0) {

                throw new PeptideParseException(input + " has illegal character " + c);
            }

            if (bracketCount < 0)
                throw new PeptideParseException(input + " has to many closing brackets" + i);
        }

        if (bracketCount != 0)
            throw new PeptideParseException(input + " has to many opening brackets");

        if (sequence.isEmpty())
            throw new PeptideParseException("The peptide '" + input + "' contains no amino acid residues");
    }

    private String stripTerminalMods(String s, ListMultimap<ModAttachment, Modification> termModMap,
            ModificationResolver modResolver)
    {
        String input = s;
        int length = s.length();
        if (length == 0)
            return s;

        if (s.charAt(0) == '(') {
            boolean foundNTerm = false;
            for (int i = 1; i < length; i++) {

                if (s.charAt(i) == '_') {

                    try {
                        parseMods(s.substring(1, i - 1), ModAttachment.N_TERM, termModMap, modResolver);
                    } catch (Exception e) {
                        throw new PeptideParseException("Illegal n-term mod format on " + input, e);
                    }

                    foundNTerm = true;
                    s = s.substring(i + 1);
                    break;
                }
            }

            if (!foundNTerm)
                throw new PeptideParseException("Invalid n-term mod format on " + input);
        }

        length = s.length();
        if (length > 2 && s.charAt(length - 1) == ')') {

            int paren = 1;
            for (int i = length - 2; i >= 0; i--) {

                if (s.charAt(i) == '(')
                    paren--;
                else if (s.charAt(i) == ')')
                    paren++;

                if (paren == 0) {
                    if (s.charAt(i - 1) == '_') {

                        try {
                            parseMods(s.substring(i + 1, length - 1), ModAttachment.C_TERM, termModMap, modResolver);
                        } catch (Exception e) {
                            throw new PeptideParseException("Illegal c-term mod format on " + input, e);
                        }

                        s = s.substring(0, i - 1);
                    }
                    break;
                }
            }
        }

        return s;
    }

    private void parseMods(String substring, ListMultimap<Integer, Modification> modMap, Integer index,
            ModificationResolver modResolver)
    {
        for (String sMod : substring.split(",\\s?")) {

            Modification mod = modResolver.resolve(sMod);
            if (mod != null)
                modMap.put(index, mod);
            else
                throw new PeptideParseException("Cannot resolve modification " + sMod + " on residue " + index);
        }
    }

    private void parseMods(String substring, ModAttachment modAttachment,
            ListMultimap<ModAttachment, Modification> termModMap, ModificationResolver modResolver)
    {
        for (String sMod : substring.split(",\\s?")) {

            Modification mod = modResolver.resolve(sMod);
            if (mod != null)
                termModMap.put(modAttachment, mod);
            else
                throw new PeptideParseException("Cannot resolve modification " + sMod);
        }
    }
}
