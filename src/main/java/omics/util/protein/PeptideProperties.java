package omics.util.protein;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Methods for peptide properties calculation.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 22 Jan 2017, 10:17 PM
 */
public class PeptideProperties
{
    private final static Logger logger = LoggerFactory.getLogger(PeptideProperties.class);

    private static Map<Character, Double> aa2Hydrophathicity = new HashMap<>();

    static {
        initHydropathicity();
    }

    /**
     * Does the initialization of hydropathicity based on http://web.expasy.org/protscale/pscale/Hphob.Doolittle.html
     */
    private static void initHydropathicity()
    {
        //		Ala(A):  1.800
        aa2Hydrophathicity.put('A', 1.800);
        //		Arg(R): -4.500
        aa2Hydrophathicity.put('R', -4.500);
        //		Asn(N): -3.500
        aa2Hydrophathicity.put('N', -3.500);
        //		Asp(D): -3.500
        aa2Hydrophathicity.put('D', -3.500);
        //		Cys(C):  2.500
        aa2Hydrophathicity.put('C', 2.500);
        //		Gln(E): -3.500
        aa2Hydrophathicity.put('E', -3.500);
        //		Glu(Q): -3.500
        aa2Hydrophathicity.put('Q', -3.500);
        //		Gly(G): -0.400
        aa2Hydrophathicity.put('G', -0.400);
        //		His(H): -3.200
        aa2Hydrophathicity.put('H', -3.200);
        //		Ile(I):  4.500
        aa2Hydrophathicity.put('I', 4.500);
        //		Leu(L):  3.800
        aa2Hydrophathicity.put('L', 3.800);
        //		Lys(K): -3.900
        aa2Hydrophathicity.put('K', -3.900);
        //		Met(M):  1.900
        aa2Hydrophathicity.put('M', 1.900);
        //		Phe(F):  2.800
        aa2Hydrophathicity.put('F', 2.800);
        //		Pro(P): -1.600
        aa2Hydrophathicity.put('P', -1.600);
        //		Ser(S): -0.800
        aa2Hydrophathicity.put('S', -0.800);
        //		Thr(T): -0.700
        aa2Hydrophathicity.put('T', -0.700);
        //		Trp(W): -0.900
        aa2Hydrophathicity.put('W', -0.900);
        //		Tyr(Y): -1.300
        aa2Hydrophathicity.put('Y', -1.300);
        //		Val(V):  4.200
        aa2Hydrophathicity.put('V', 4.200);
    }

    /**
     * Return the average hydropathy value of sequence, namely gravy value.
     * <p>
     * The average value for a sequence is calculated as the sum of hydropathy
     * values of all the amino acids, divided by the number of residues in the
     * sequence. Hydropathy values are based on (Kyte, J. and Doolittle, R.F.
     * (1982) A simple method for displaying the hydropathic character of a
     * protein. J. Mol. Biol. 157, 105-132).
     *
     * @param proteinSeq proteins equence
     * @return gravy value.
     */
    public static double calcGravy(String proteinSeq)
    {
        int validLength = 0;
        double total = 0.0;
        char[] seq = proteinSeq.toUpperCase().toCharArray();
        for (char aa : seq) {
            if (aa2Hydrophathicity.containsKey(aa)) {
                total += aa2Hydrophathicity.get(aa);
                validLength++;
            }
        }

        if (validLength == 0) {
            logger.warn("Valid length of sequence is 0, can't devide by 0 to calculate average hydropathy: setting to" +
                    " 0");
            return 0.0;
        }

        return total / validLength;
    }

    /**
     * Return the peptide mass
     *
     * @param peptideSeq peptide sequence, modification is supported as in the PeptideParser
     * @return peptide mass
     * @see Peptide#parse(String)
     */
    public static double calcMass(String peptideSeq)
    {
        Peptide peptide = Peptide.parse(peptideSeq);
        return peptide.getMolecularMass();
    }

}
