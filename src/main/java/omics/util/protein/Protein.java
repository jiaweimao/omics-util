package omics.util.protein;

import omics.util.chem.Sequence;

import java.util.Arrays;

import static omics.util.utils.ObjectUtils.checkElementIndex;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * amino acid sequence for protien
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 05 Jul 2017, 10:26 AM
 */
public class Protein implements Sequence<AminoAcid>
{
    private final AminoAcid[] aminoAcids;
    private final String accessionId;
    private final String description;

    /**
     * Construct with protein sequence, the accession is empty
     *
     * @param sequence amino acid sequence
     */
    public Protein(String sequence)
    {
        this("", sequence);
    }

    public Protein(String accessionId, String sequence)
    {
        this(accessionId, "", sequence);
    }

    /**
     * Create an instance from a sequence string
     *
     * @param sequence the string to create a Protein from
     * @throws IllegalArgumentException if the amino-acid sequence is invalid
     */
    public Protein(String accessionId, String description, String sequence)
    {
        checkNotNull(sequence);
        checkNotNull(accessionId);

        aminoAcids = new AminoAcid[sequence.length()];
        this.accessionId = accessionId;
        this.description = description;

        for (int i = 0; i < sequence.length(); i++) {

            aminoAcids[i] = AminoAcid.getAminoAcid(sequence.charAt(i));
        }
    }

    /**
     * Create an instance from a list of AminoAcids
     *
     * @param aas the list of amino-acids
     */
    public Protein(String accessionId, String description, AminoAcid... aas)
    {
        checkNotNull(aas);
        checkNotNull(accessionId);

        final int length = aas.length;
        aminoAcids = new AminoAcid[length];
        System.arraycopy(aas, 0, aminoAcids, 0, length);
        this.accessionId = accessionId;
        this.description = description;
    }

    private Protein(String accessionId, String description, AminoAcid[] aas, int length)
    {
        checkNotNull(aas);
        checkNotNull(accessionId);

        aminoAcids = new AminoAcid[length];
        System.arraycopy(aas, 0, aminoAcids, 0, length);
        this.accessionId = accessionId;
        this.description = description;
    }

    /**
     * @return protein accession, empty if not set
     */
    public String getAccession()
    {
        return accessionId;
    }

    public String getDescription()
    {
        return description;
    }

    /**
     * Create a PeptideBuilder that has the sequence initialised by copying this
     * Protein's amino acids starting at <code>fromInclusive</code> to
     * <code>toExclusive</code>.
     *
     * @param fromInclusive from index protein
     * @param toExclusive   to index protein
     * @return a new PeptideBuilder
     */
    public Peptide.Builder getPeptideBuilder(int fromInclusive, int toExclusive)
    {
        return new Peptide.Builder(aminoAcids, fromInclusive, toExclusive);
    }

    @Override
    public AminoAcid getSymbol(int index)
    {
        checkElementIndex(index, size());

        return aminoAcids[index];
    }

    @Override
    public String toSymbolString()
    {
        StringBuilder buff = new StringBuilder();

        for (AminoAcid aa : aminoAcids) {

            buff.append(aa.getSymbol());
        }

        return buff.toString();
    }

    @Override
    public int size()
    {
        return aminoAcids.length;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Protein protein = (Protein) o;

        return Arrays.equals(aminoAcids, protein.aminoAcids) && accessionId.equals(protein.accessionId);
    }

    @Override
    public int hashCode()
    {
        int result = Arrays.hashCode(aminoAcids);
        result = 31 * result + accessionId.hashCode();
        return result;
    }

    public String toString()
    {
        return toSymbolString();
    }

    public final static class Builder
    {
        private String accessionId;
        private String description;
        private AminoAcid[] aminoAcids;
        private int size = 0;

        public Builder(int size)
        {
            aminoAcids = new AminoAcid[size];
        }

        public Builder(String sequence)
        {
            aminoAcids = new AminoAcid[sequence.length()];
            setSequence(sequence);
        }

        public Builder setDescription(String description)
        {
            this.description = description;
            return this;
        }

        public Builder setSequence(String sequence)
        {
            size = 0;

            appendSequence(sequence);

            return this;
        }

        public Builder appendSequence(String sequence)
        {
            if (aminoAcids.length < size + sequence.length()) {

                AminoAcid[] tmp = new AminoAcid[size + sequence.length()];
                System.arraycopy(aminoAcids, 0, tmp, 0, size);

                aminoAcids = tmp;
            }

            for (int i = 0; i < sequence.length(); i++) {
                aminoAcids[size++] = AminoAcid.getAminoAcid(sequence.charAt(i));
            }

            return this;
        }

        public Protein build()
        {
            return new Protein(accessionId, description, aminoAcids, size);
        }

        public void reset()
        {
            size = 0;
            accessionId = null;
        }

        public Builder setAccession(String accessionId)
        {
            this.accessionId = accessionId;
            return this;
        }
    }
}
