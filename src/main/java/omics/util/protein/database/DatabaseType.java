package omics.util.protein.database;

/**
 * Type of Protein Database.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 12:04 PM
 */
public enum DatabaseType
{
    UniProtKB,
    SwissProt,
    NCBI,
    TrEMBL,
    IPI,
    Unknown;
}
