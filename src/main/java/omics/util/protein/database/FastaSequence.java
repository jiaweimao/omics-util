package omics.util.protein.database;

import it.unimi.dsi.fastutil.chars.Char2IntMap;
import it.unimi.dsi.fastutil.chars.Char2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.*;
import omics.util.interfaces.Clearable;
import omics.util.io.FilenameUtils;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.database.util.DoShuffleDB;
import omics.util.string.Alphabet;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * Fasta file sequence.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Jun 2019, 4:33 PM
 */
public class FastaSequence implements Clearable
{
    private static final String SEQ_FILE_EXTENSION = ".ms7";
    private static final String ANNOTATION_FILE_EXTENSION = ".ma7";
    private static final String STAT_FILE_EXTENSION = ".mt7";

    /**
     * The terminator byte representation.
     */
    private static final byte TERMINATOR = 1;
    /**
     * The terminator byte representation.
     */
    private static final char TERMINATOR_CHAR = '_';
    /**
     * The byte representation of the invalid character.
     */
    private static final byte INVALID_CHAR_CODE = 0;
    /**
     * The character representation of the invalid character.
     */
    private static final char INVALID_CHAR = '?';

    private Alphabet alphabet;
    private final String baseFilePath;
    private final String fastaPath;

    /**
     * sequence offset and its header, each offset point the last amino acid of the protein
     */
    private Int2ObjectMap<String> annotationMap;
    private Int2ObjectMap<String> accessionMap;
    private TreeSet<Integer> indexSet;
    /**
     * this map hold the number of various residues.
     */
    private Char2IntMap residue2CountMap;
    /**
     * this map hold the count of proteins of different length.
     */
    private Int2IntMap length2CountMap;
    private int residueCount;

    /**
     * Contents of the sequence concatenated into a long string
     */
    private byte[] seqByteArray;
    private byte[] seqCharArray;
    /**
     * number of characters of protein sequences, which is the length of this fasta sequence.
     */
    private int size;

    private int id;

    private String decoyProteinPrefix = DoShuffleDB.DECOY_PROTEIN_PREFIX;

    public FastaSequence(String filePath)
    {
        this(Alphabet.PROTEIN, filePath);
    }

    public FastaSequence(Alphabet alphabet, String filePath)
    {
        checkArgument(Files.exists(Paths.get(filePath)), "The fasta file does not exist: " + filePath);
        this.fastaPath = filePath;
        this.alphabet = alphabet;

        checkArgument(ProteinDB.isFastaFile(filePath), "Input file is not fasta file (.fasta, .fa)");

        this.baseFilePath = FilenameUtils.removeExtension(filePath);
        Path annoPath = Paths.get(baseFilePath + ANNOTATION_FILE_EXTENSION);
        Path seqPath = Paths.get(baseFilePath + SEQ_FILE_EXTENSION);
        Path statPath = Paths.get(baseFilePath + STAT_FILE_EXTENSION);

        try {
            if (Files.notExists(annoPath) || Files.notExists(seqPath) || Files.notExists(statPath)) {
                createFile(filePath, annoPath, seqPath, statPath);
            }

            int annoId = readAnnoFile();
            int seqId = readSeqFile();
            int statId = readStatFile();

            checkArgument(annoId == seqId, "The file " + annoPath + " and " + seqPath + " have different ids.");
            checkArgument(statId == seqId, "The file " + statPath + " and " + seqPath + " have different ids.");

            this.id = annoId;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return path of the fasta file.
     */
    public String getFastaPath()
    {
        return fastaPath;
    }

    /**
     * @return number of residues in the fasta file.
     */
    public int getResidueCount()
    {
        return residueCount;
    }

    /**
     * @param length protein length
     * @return the number of proteins of given length in the fasta file.
     */
    public int getProteinCount(int length)
    {
        return length2CountMap.get(length);
    }

    /**
     * @return protein length set.
     */
    public IntSet getProteinLengthSet()
    {
        return length2CountMap.keySet();
    }

    /**
     * Return the number of given residue in this fasta file.
     *
     * @param residue amino acid residue
     * @return the number of given residue in the fasta file.
     */
    public int getResidueCount(char residue)
    {
        return residue2CountMap.get(residue);
    }

    /**
     * @return the fasta file path without extension, which is used to create meta files.
     */
    public String getBaseFilePath()
    {
        return baseFilePath;
    }

    /**
     * Return the fraction of decoy proteins in this fasta file, the decoy identifier
     * should be the start of the protein accesion.
     *
     * @return the fraction of decoy proteins in the fasta file.
     */
    public double getDecoyProteinRatio()
    {
        int numDecoyProteins = 0;
        for (String value : accessionMap.values()) {
            if (value.startsWith(decoyProteinPrefix))
                numDecoyProteins++;
        }
        return numDecoyProteins / (double) getNumberOfProteins();
    }

    /**
     * @return decoy protein prefix.
     */
    public String getDecoyProteinPrefix()
    {
        return decoyProteinPrefix;
    }

    public void setDecoyProteinPrefix(String decoyProteinPrefix)
    {
        this.decoyProteinPrefix = decoyProteinPrefix;
    }

    /**
     * Return true if the byte in given position is terminator.
     *
     * @param index the position to inquire about
     */
    public boolean isTerminator(int index)
    {
        return seqByteArray[index] == TERMINATOR;
    }

    /**
     * return true if given character is terminator in the sequence.
     */
    public static boolean isTerminator(char aa)
    {
        return aa == TERMINATOR_CHAR;
    }

    /**
     * Return true if the byte in given position is invalid char.
     *
     * @param index the position to inquire about
     */
    public boolean isInvalid(int index)
    {
        return seqByteArray[index] == INVALID_CHAR_CODE;
    }

    public Alphabet getAlphabet()
    {
        return alphabet;
    }

    /**
     * @return sequence {@link ByteBuffer}
     */
    public byte[] getByteSequence()
    {
        return seqByteArray;
    }

    /**
     * @return {@link ByteBuffer} of protein amino acids in chars.
     */
    public byte[] getCharSequence()
    {
        return seqCharArray;
    }

    /**
     * @return concatenated protein sequence, for test only.
     */
    public String getSequence()
    {
        char[] seq = new char[seqCharArray.length];
        for (int i = 0; i < seqCharArray.length; i++) {
            seq[i] = (char) seqCharArray[i];
        }
        return new String(seq);
    }

    /**
     * return sequence in range
     *
     * @param start start index, inclusive
     * @param end   end index, exclusive
     */
    public String subString(int start, int end)
    {
        char[] seq = new char[end - start];
        for (int i = start; i < end; i++) {
            seq[i - start] = (char) seqCharArray[i];
        }
        return new String(seq);
    }

    /**
     * Return the peptide sequence ends with terminator or unknown characters.
     *
     * @param start start index, inclusive
     * @param end   end index, exclusive
     */
    public String subStringEndTerm(int start, int end)
    {
        char[] seq = new char[end - start];
        seq[0] = (char) seqCharArray[start];
        int count = 1;
        if (start + 1 < size) {
            for (int i = start + 1; i < end; i++) {
                char c = (char) seqCharArray[i];
                seq[i - start] = c;
                count++;
                if (c == TERMINATOR_CHAR || seqByteArray[i] == INVALID_CHAR_CODE)
                    break;
            }
        }

        return new String(seq, 0, count);
    }

    /**
     * @return number of chars in this sequence, including amino acids and terminators (both start and end terminators)
     */
    public int size()
    {
        return size;
    }

    /**
     * @return a unique id for this database generated by UUID.
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * @return number of proteins in this fasta file
     */
    public int getNumberOfProteins()
    {
        return accessionMap.size();
    }

    /**
     * @return an unmodifiable set of protein indices, which point to protein end indexes,
     * namely the position after last amino acid.
     */
    public SortedSet<Integer> getProteinIndices()
    {
        return Collections.unmodifiableSortedSet(indexSet);
    }

    /**
     * Return protein sequence at given position. <b>NOTE</b>, if the position point to the end of the
     *
     * @param position the position to query, if the position point to the end of protein sequence,
     *                 it will return the next protein sequence.
     */
    public String getProteinEntry(int position)
    {
        int startIndex = getStartIndex(position); // always "_" at start
        int endIndex = getEndIndex(position); // exclusive

        return this.subString(startIndex + 1, endIndex);
    }

    /**
     * Return the start index of the protein at given position, every protein start with '_', for example:
     * <p>
     * "_PROTEIN|_PROTEIN|_"
     * <p>
     * the start index point to "_".
     * <p>
     * As there is a "_" at end, if the index point to that '_', the start index maybe not what you want.
     *
     * @param index an position in the sequence.
     */
    public int getStartIndex(int index)
    {
        Integer floor = indexSet.floor(index);
        if (floor == null)
            floor = 0;
        return floor;
    }

    /**
     * Return end index of protein, which point to protein's last amino acid.
     *
     * @param index any index between the protein
     * @return end index of the protein.
     */
    public int getEndIndex(int index)
    {
        Integer celling = isTerminator(index) ? indexSet.ceiling(index + 1) : indexSet.ceiling(index);
        if (celling == null)
            celling = size - 1;
        return celling;
    }

    /**
     * @param index index in the sequence
     * @return character at the index
     */
    public char getChar(int index)
    {
        return (char) seqCharArray[index];
    }

    /**
     * Return true if the amino acid at given index is the protein N-term Methionine.
     *
     * @param index index
     * @return true if the amino acid at given index is a protein N-term M.
     */
    public boolean isLeadingMet(int index)
    {
        return getChar(index) == 'M' && isTerminator(index - 1);
    }

    /**
     * Return true if the given char is a valid amino acid
     *
     * @param c the character to check
     */
    public boolean inAlphabet(char c)
    {
        return alphabet.contains(c);
    }

    /**
     * Return the byte at given position of the sequence, it is could be an amino acid or a terminator.
     *
     * @param index the position to query.
     * @return byte at given position
     */
    public byte getByte(int index)
    {
        return seqByteArray[index];
    }

    /**
     * Return bytes in given range
     *
     * @param start the start index
     * @param end   the end index (exclusive)
     */
    public byte[] getBytes(int start, int end)
    {
        byte[] result = new byte[end - start];
        System.arraycopy(seqByteArray, start, result, 0, end - start);
        return result;
    }

    /**
     * Return protein annotation at given position, the position
     *
     * @param position point the position after the last amino acid of each proteins.
     */
    public String getAnnotation(int position)
    {
        return annotationMap.get(position);
    }

    /**
     * Return protein annotation at given position.
     *
     * @param position the position to query. Annotations are mapped to certain
     *                 ranges of the sequence indices, so this function will return the annotation
     *                 for the subsequence that falls within this range.
     *                 if position is the length of protein sequence + terminator, return the next protein annotation
     */
    public String getAnnotationUpper(int position)
    {
        Integer index = indexSet.higher(position);
        if (index != null)
            return annotationMap.get(index.intValue());

        return null;
    }

    /**
     * Return protein accession at given position
     *
     * @param position point the position after the last amino acid of each proteins.
     * @return protein accession, null if not found.
     */
    public String getAccession(int position)
    {
        return accessionMap.get(position);
    }

    /**
     * Return protein accession at given position which is just higher than the given position.
     *
     * @param position pointer to the total sequence, as the stored indices point to terminators,
     * @return protein accession, null if not found.
     */
    public String getAccessionUpper(int position)
    {
        Integer index = indexSet.higher(position);
        if (index != null)
            return accessionMap.get(index.intValue());
        return null;
    }

    private void createFile(String fastaFile, Path annoFile, Path seqFile, Path statFile) throws IOException
    {
        int id = UUID.randomUUID().hashCode();
        PrintWriter annoOut = new PrintWriter(Files.newBufferedWriter(annoFile));
        annoOut.println(id);
        annoOut.println(alphabet.getBase());

        Path fastaPath = Paths.get(fastaFile);
        ByteBuffer seqByteBuffer = ByteBuffer.allocate((int) Files.size(fastaPath));
        StringBuilder seqCharBuffer = new StringBuilder();

        Int2IntSortedMap size2countMap = new Int2IntAVLTreeMap();
        int[] aaCount = new int[128];
        int totalAA = 0; // total amino acids
        int offset = 0;
        FastaReader reader = new FastaReader(fastaPath);
        while (reader.hasNext()) {
            Protein protein = reader.next();
            String sequence = protein.getSequence();

            int N = sequence.length();
            for (int i = 0; i < N; i++) {
                char aa = sequence.charAt(i);
                aaCount[aa]++;
            }

            size2countMap.put(N, size2countMap.getOrDefault(N, 0) + 1);
            totalAA += N;

            byte[] bytes = alphabet.toByteIndices(sequence);
            seqByteBuffer.put(TERMINATOR);
            seqByteBuffer.put(bytes);

            seqCharBuffer.append(TERMINATOR_CHAR);
            seqCharBuffer.append(sequence);

            offset += (bytes.length + 1);
            annoOut.println(offset + ":" + protein.getAccession() + ":" + protein.getDescription());
        }
        reader.close();
        annoOut.flush();
        annoOut.close();

        seqByteBuffer.put(TERMINATOR);
        seqCharBuffer.append(TERMINATOR_CHAR);
        offset++;
        int size = offset;

        DataOutputStream seqOut = new DataOutputStream(new BufferedOutputStream(Files.newOutputStream(seqFile)));
        seqOut.writeInt(id);
        seqOut.writeInt(size);
        for (int i = 0; i < size; i++) {
            seqOut.writeByte(seqByteBuffer.get(i));
        }
        seqOut.write(seqCharBuffer.toString().getBytes());
        seqOut.flush();
        seqOut.close();

        DataOutputStream statOut = new DataOutputStream(new BufferedOutputStream(Files.newOutputStream(statFile)));
        statOut.writeInt(id);
        statOut.writeInt(totalAA);
        for (char a = 'A'; a <= 'Z'; a++) {
            statOut.writeInt(aaCount[a]);
        }
        statOut.writeInt(size2countMap.size());
        for (Int2IntMap.Entry entry : size2countMap.int2IntEntrySet()) {
            statOut.writeInt(entry.getIntKey());
            statOut.writeInt(entry.getIntValue());
        }
        statOut.flush();
        statOut.close();
    }

    /**
     * read the annotation file.
     */
    private int readAnnoFile() throws IOException
    {
        String filePath = baseFilePath + ANNOTATION_FILE_EXTENSION;
        int id;
        try (BufferedReader in = Files.newBufferedReader(Paths.get(filePath))) {
            id = Integer.parseInt(in.readLine());

            String alphabetStr = in.readLine().trim();
            this.alphabet = new Alphabet(alphabetStr);

            this.indexSet = new TreeSet<>();
            this.annotationMap = new Int2ObjectOpenHashMap<>();
            this.accessionMap = new Int2ObjectOpenHashMap<>();
            for (String line = in.readLine(); line != null; line = in.readLine()) {
                String[] values = line.split(":", 3);
                int index = Integer.parseInt(values[0]);
                indexSet.add(index);
                accessionMap.put(index, values[1]);
                annotationMap.put(index, values[2]);
            }
        }

        return id;
    }

    private int readSeqFile() throws IOException
    {
        String filePath = baseFilePath + SEQ_FILE_EXTENSION;
        int id;
        try (DataInputStream in = new DataInputStream(new BufferedInputStream(Files.newInputStream(Paths.get(filePath))))) {
            id = in.readInt();
            this.size = in.readInt();

            this.seqByteArray = new byte[size];
            in.read(seqByteArray);

            this.seqCharArray = new byte[size];
            in.read(seqCharArray);
        }
        return id;
    }


    private int readStatFile() throws IOException
    {
        String filePath = baseFilePath + STAT_FILE_EXTENSION;
        int id;
        try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(filePath)))) {
            id = dis.readInt();

            residueCount = dis.readInt();
            residue2CountMap = new Char2IntOpenHashMap(26);
            for (char a = 'A'; a <= 'Z'; a++) {
                residue2CountMap.put(a, dis.readInt());
            }
            int n = dis.readInt();
            length2CountMap = new Int2IntOpenHashMap(n);
            for (int i = 0; i < n; i++) {
                int len = dis.readInt();
                int count = dis.readInt();
                length2CountMap.put(len, count);
            }
        }

        return id;
    }

    @Override
    public void clear()
    {
        if (annotationMap != null) {
            annotationMap.clear();
            annotationMap = null;
        }

        if (accessionMap != null) {
            accessionMap.clear();
            accessionMap = null;
        }

        if (indexSet != null) {
            indexSet.clear();
            indexSet = null;
        }

        seqByteArray = null;
        seqCharArray = null;
    }
}
