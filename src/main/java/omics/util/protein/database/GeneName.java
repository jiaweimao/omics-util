package omics.util.protein.database;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2017, 9:57 PM
 */
public class GeneName {

    private String name;
    private List<String> synonymsList;
    private List<String> orderedLocusNameList;
    private List<String> orfNameList;

    /**
     * @return gene name
     */
    public String getName() {
        return name;
    }

    /**
     * set the gene name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * a "Synonyms" token can only be present if there is a "Name" token.
     */
    public List<String> getSynonymsList() {
        return synonymsList;
    }

    public void addSynonyms(String synonyms) {
        if (synonymsList == null)
            synonymsList = new ArrayList<>(1);
        synonymsList.add(synonyms);
    }

    /**
     * A name used to represent an ORF in a completely sequenced genome or chromosome. It is generally based on a prefix
     * representing the organism and a number which usually represents the sequential ordering of genes on the
     * chromosome. Depending on the genome sequencing center, numbers are only attributed to protein-coding genes, or
     * also to pseudogenes, or also to tRNAs and other features. If two predicted genes have been merged to form a new
     * gene, both gene identifiers are indicated, separated by a slash (see last example). Examples: HI0934, Rv3245c,
     * At5g34500, YER456W, YAR042W/YAR044W.
     */
    public List<String> getOrderedLocusNameList() {
        return orderedLocusNameList;
    }

    public void addOrderedLocusName(String locusName) {
        if (orderedLocusNameList == null)
            orderedLocusNameList = new ArrayList<>(1);
        this.orderedLocusNameList.add(locusName);
    }

    public List<String> getORFNameList() {
        return orfNameList;
    }

    public void addORFName(String orfName) {
        if (orfNameList == null)
            orfNameList = new ArrayList<>(1);
        this.orfNameList.add(orfName);
    }
}
