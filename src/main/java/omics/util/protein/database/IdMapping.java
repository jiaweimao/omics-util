package omics.util.protein.database;

import omics.util.io.csv.CsvReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Jun 2020, 1:02 PM
 */
public class IdMapping
{
    public static final String HUMAN = "Homo sapiens";

    private final String mappingFile;
    private ArrayList<Entry> entryList;
    private final HashMap<String, Entry> accMap;
    private final HashMap<String, Entry> entryNameMap;

    public IdMapping(String mappingFile)
    {
        this.mappingFile = mappingFile;
        init();
        accMap = new HashMap<>(entryList.size());
        entryNameMap = new HashMap<>(entryList.size());
        for (Entry entry : entryList) {
            accMap.put(entry.getAccession(), entry);
            entryNameMap.put(entry.getEntryName(), entry);
        }
    }

    private void init()
    {
        entryList = new ArrayList<>();
        try (CsvReader reader = new CsvReader(mappingFile)) {

            reader.readHeaders();
            while (reader.readRecord()) {
                String acc = reader.get("Accession");
                boolean reviewed = Boolean.parseBoolean(reader.get("Review"));
                String entryName = reader.get("Entry name");
                String proteinName = reader.get("Protein name");
                String geneName = reader.get("Gene name");
                String organism = reader.get("Organism");
                String taxonomicIdentifier = reader.get("Taxonomic identifier");

                Entry entry = new Entry(acc, reviewed, entryName, proteinName, geneName, organism, taxonomicIdentifier);
                entryList.add(entry);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        entryList.trimToSize();
    }

    public List<Entry> getIdList()
    {
        return entryList;
    }

    public Entry ofAccession(String acc)
    {
        return accMap.get(acc);
    }

    public Entry ofEntryName(String entryName)
    {
        return entryNameMap.get(entryName);
    }

    public static class Entry
    {
        private final String accession;
        private final boolean isReviewed;
        private final String entryName;
        private final String proteinName;
        private final String geneName;
        private final String organism;
        private final String taxonomicIdentifier;

        public Entry(String accession, boolean isReviewed, String entryName, String proteinName, String geneName,
                String organism, String taxonomicIdentifier)
        {
            this.accession = accession;
            this.isReviewed = isReviewed;
            this.entryName = entryName;
            this.proteinName = proteinName;
            this.geneName = geneName;
            this.organism = organism;
            this.taxonomicIdentifier = taxonomicIdentifier;
        }

        public String getAccession()
        {
            return accession;
        }

        public boolean isReviewed()
        {
            return isReviewed;
        }

        public String getEntryName()
        {
            return entryName;
        }

        public String getProteinName()
        {
            return proteinName;
        }

        public String getGeneName()
        {
            return geneName;
        }

        public String getOrganism()
        {
            return organism;
        }

        public String getTaxonomicIdentifier()
        {
            return taxonomicIdentifier;
        }
    }
}
