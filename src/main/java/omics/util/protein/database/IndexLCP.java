package omics.util.protein.database;

/**
 * This class hold a lcp item.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 Oct 2019, 4:31 PM
 */
public class IndexLCP {

    private int index;
    private byte lcp;

    /**
     * Constructor.
     *
     * @param index index of the suffix
     * @param lcp   lcp with previous suffix
     */
    public IndexLCP(int index, byte lcp) {
        this.index = index;
        this.lcp = lcp;
    }

    /**
     * @return index of the suffix
     */
    public int getIndex() {
        return index;
    }

    /**
     * @return lcp with previous suffix.
     */
    public byte getLCP() {
        return lcp;
    }
}
