package omics.util.protein.database;

/**
 * https://www.ncbi.nlm.nih.gov/Sitemap/sequenceIDs.html
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Jul 2020, 7:38 PM
 */
public class NCBIProtein extends Protein
{
    private String gi;
    private String version;

    public String getGI()
    {
        return gi;
    }

    public void setGI(String gi)
    {
        this.gi = gi;
    }

    @Override
    public String getFastaHeader()
    {
        if (fastaHeader == null) {
            this.fastaHeader = ">gi|" + gi +
                    "|" + getAccession() + "." + version +
                    "|" + description;
        }
        return fastaHeader;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }
}
