package omics.util.protein.database;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

/**
 * This class define the start and end index of a peptide in the fasta sequence.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 22 Sep 2019, 3:29 PM
 */
public class PeptideRange {

    private int startIndex;
    private int endIndex;
    private int length;
    private IntList degeneratePeptides;

    /**
     * Construct of Peptide range
     *
     * @param startIndex start index in the sequence
     * @param endIndex   end index in the fasta sequence
     */
    public PeptideRange(int startIndex, int endIndex) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.length = endIndex - startIndex;
    }

    /**
     * add a degenerate peptide, which has the same sequence as this peptide.
     *
     * @param startIndex start index of the degenerate peptide.
     */
    public void addDegeneratePeptide(int startIndex) {
        if (degeneratePeptides == null)
            degeneratePeptides = new IntArrayList();
        degeneratePeptides.add(startIndex);
    }

    /**
     * @return start index in the fasta sequence.
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * @return end index in the fasta sequence.
     */
    public int getEndIndex() {
        return endIndex;
    }

    /**
     * @return peptide length
     */
    public int getLength() {
        return length;
    }

    /**
     * @return start indexes of all degenerate peptides, null if there is no degenerate peptide for this peptide.
     */
    public IntList getDegeneratePeptides() {
        return degeneratePeptides;
    }
}
