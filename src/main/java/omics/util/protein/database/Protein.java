package omics.util.protein.database;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import omics.util.OmicsObject;
import omics.util.utils.Histogram;
import omics.util.utils.StringUtils;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * A protein in the database.
 *
 * @author JiaweiMao
 * @version 1.1.1
 * @since 12 Oct 2018, 6:45 PM
 */
public class Protein extends OmicsObject
{
    private String accession;
    protected String fastaHeader;
    protected String sequence;
    protected String description;
    private boolean isReviewed;

    public Protein() { }

    public Protein(String accession)
    {
        checkNotNull(accession);

        this.accession = accession;
    }

    /**
     * Construct with protein accession and sequence
     *
     * @param accession accession
     * @param sequence  amino acid sequence
     */
    public Protein(String accession, String sequence)
    {
        this.accession = accession;
        this.sequence = sequence;
    }

    /**
     * @return true if the protein is reviewed.
     */
    public boolean isReviewed()
    {
        return isReviewed;
    }

    /**
     * setter of the <code>isReviewed</code> field
     *
     * @param reviewed true if this protein has been reviewed
     */
    public void setReviewed(boolean reviewed)
    {
        isReviewed = reviewed;
    }

    /**
     * the protein header in the fasta file, which is start with >
     */
    public String getFastaHeader()
    {
        if (fastaHeader == null) {
            StringBuilder builder = new StringBuilder();
            builder.append(">");
            if (isReviewed) {
                builder.append("sp");
            } else {
                builder.append("tr");
            }
            builder.append("|").append(accession).append("|").append(getDescription());
            this.fastaHeader = builder.toString();
        }
        return fastaHeader;
    }

    /**
     * setter of protein header in fasta file, which is the first line of a protein, start with '>'
     */
    public void setFastaHeader(String fastaHeader)
    {
        this.fastaHeader = fastaHeader;
    }

    /**
     * @return protein sequence
     */
    public String getSequence()
    {
        return sequence;
    }

    /**
     * set the protein sequence
     *
     * @param sequence a {@link String} object.
     */
    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }

    /**
     * @return protein primary accession number
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * setter of protein primary accession
     */
    public void setAccession(String accession)
    {
        this.accession = accession;
    }

    /**
     * getter of protein description, which is the header of the protein in fasta format without accession.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * setter of protein description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return length of this protein
     */
    public int length()
    {
        return sequence.length();
    }


    /**
     * Return true if this Protein pass the filter
     *
     * @param filter a {@link Protein} object.
     * @return true if this FastaEntry pass the filter.
     */
    public boolean passFilter(ProteinFilter filter)
    {
        return filter.test(this);
    }

    /**
     * Return true if this Protein pass all the filters.
     *
     * @param filters Filter collection
     * @return true if pass
     */
    public boolean passFilter(Collection<ProteinFilter> filters)
    {
        boolean value = true;
        for (ProteinFilter filter : filters) {
            if (!filter.test(this)) {
                value = false;
                break;
            }
        }
        return value;
    }

    /**
     * This method can be used to append this FastaEntry to the FASTA DB file the PrintWrite points to.
     *
     * @param writer PrintWriter to write the file to.
     */
    public void writeToFasta(PrintWriter writer, int maxWidth)
    {
        writer.println(getFastaHeader());

        StringBuilder builder = new StringBuilder(sequence);
        if (builder.length() > maxWidth) {
            int offset = maxWidth;
            do {
                builder.insert(offset, "\n");
                offset += (maxWidth + 1);
            } while (offset <= builder.length());
        }
        writer.println(builder.toString());
    }

    /**
     * This method can be used to append this FastaEntry to the FASTA DB file the PrintWrite points to.
     *
     * @param writer PrintWriter to write the file to.
     */
    public void writeToFasta(PrintWriter writer)
    {
        writeToFasta(writer, 60);
    }

    /**
     * Get the indexes of given sequence pattern.
     *
     * @param pattern pattern for motif
     * @return indexes of the motif in the protein.
     */
    public int[] indexOfMotif(Pattern pattern)
    {
        IntArrayList list = new IntArrayList();
        Matcher matcher = pattern.matcher(sequence);
        int id = 0;
        while (matcher.find(id)) {
            list.add(matcher.start());
            id = matcher.end();
        }

        return list.toIntArray();
    }

    /**
     * Get the indexes of given peptide sequence in this protein sequence.
     *
     * @param pepSeq  peptide sequence to search for.
     * @param ignoreX true if Amino Acid 'X' in the protein sequence start can match any AA.
     * @return indexes of peptide sequence in the protein sequence, an empty array if the peptide sequence is not in.
     */
    public int[] indexOf(String pepSeq, boolean ignoreX)
    {
        IntList list = new IntArrayList();
        int index;
        int start = 0;
        while ((index = sequence.indexOf(pepSeq, start)) != -1) {
            list.add(index);
            start = index + pepSeq.length();
        }

        if (ignoreX && (sequence.charAt(0) == 'X')) {
            if (sequence.substring(1).startsWith(pepSeq.substring(1))) {
                list.add(0);
            }
        }

        return list.toIntArray();
    }

    /**
     * return substring of the protein sequence.
     *
     * @param index index in the protein
     * @param width number of chars to extract before index and after the index.
     * @return the sub string.
     */
    public String subString(int index, int width)
    {
        return subString(index, width, width);
    }

    /**
     * return substring of the protein sequence.
     *
     * @param index index in the protein
     * @param left  length before index
     * @param right length after index
     * @return the sub string.
     */
    public String subString(int index, int left, int right)
    {
        return subString(index, left, right, '_');
    }

    /**
     * return substring of the protein sequence.
     *
     * @param index       index in the protein.
     * @param left        length before index
     * @param right       length after index
     * @param replaceChar char to use if there is no char of given length.
     * @return the sub string.
     */
    public String subString(int index, int left, int right, char replaceChar)
    {
        return StringUtils.subString(sequence, index, left, right, replaceChar);
    }

    /**
     * @return count histogram of all amino acids
     */
    public Histogram<Character> getAminoAcidCount()
    {
        Histogram<Character> countHis = new Histogram<>(26);
        for (int i = 0; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            countHis.increase(c);
        }
        return countHis;
    }
}
