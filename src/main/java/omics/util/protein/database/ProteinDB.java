package omics.util.protein.database;

import com.google.common.collect.ImmutableList;
import omics.util.OmicsException;
import omics.util.interfaces.Clearable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Protein Database.
 *
 * @author JiaweiMao
 * @version 1.3.0
 * @since 19 Mar 2017, 11:37 AM
 */
public class ProteinDB implements Clearable, Iterable<Protein>
{
    /**
     * Return true if the file path ends with .fasta, .fa
     */
    public static boolean isFastaFile(String filePath)
    {
        String s = filePath.toLowerCase();

        return s.endsWith(".fasta") || s.endsWith(".fa");
    }

    public static long[] countChars(String fastaFile)
    {
        long[] aaCount = new long[Byte.MAX_VALUE]; // count of amino acid

        try (BufferedReader in = new BufferedReader(new FileReader(fastaFile));) {
            String s;
            while ((s = in.readLine()) != null) {
                if (s.startsWith(">"))    // annotation
                    continue;
                for (int i = 0; i < s.length(); i++) {
                    char c = s.charAt(i);
                    aaCount[c]++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return aaCount;
    }

    /**
     * Return the {@link ProteinDB} of given file, null for any exception.
     *
     * @param file a protein database file
     * @return {@link ProteinDB}
     */
    public static ProteinDB read(String file) throws OmicsException
    {
        return read(file, false);
    }

    /**
     * Return the {@link ProteinDB} of given file, null for any exception.
     *
     * @param file        protein database file
     * @param parseDetail true if true the header for fasta file
     * @return {@link ProteinDB}
     */
    public static ProteinDB read(String file, boolean parseDetail) throws OmicsException
    {
        ProteinDBAccessor accessor = new ProteinDBAccessor(Paths.get(file), parseDetail);
        accessor.go();

        return accessor.getValue();
    }

    private String name;
    private DatabaseType databaseType;
    private Map<String, Protein> dbMap;

    public ProteinDB()
    {
        this(DatabaseType.UniProtKB);
    }

    public ProteinDB(DatabaseType databaseType)
    {
        this.databaseType = databaseType;
        this.dbMap = new HashMap<>();
    }

    /**
     * @return database name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Set the database name.
     *
     * @param name name of database.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the DatabaseType of this proteinDB.
     */
    public DatabaseType getType()
    {
        return databaseType;
    }

    public void setType(DatabaseType databaseType)
    {
        this.databaseType = databaseType;
    }

    /**
     * @return an unmodifiable List of all the proteins in the DB.
     */
    public List<Protein> getAllProteins()
    {
        return ImmutableList.copyOf(dbMap.values());
    }

    /**
     * @return number of proteins in this ProteinDB.
     */
    public int size()
    {
        return dbMap.size();
    }

    /**
     * add a new Protein to the database.
     *
     * @param proteinEntry a {@link UniProtTxtProtein} object.
     */
    public void add(Protein proteinEntry)
    {
        dbMap.put(proteinEntry.getAccession(), proteinEntry);
    }

    /**
     * @param accession protein accession.
     * @return ProteinEntry of given accession, or null if not present.
     */
    public Protein get(String accession)
    {
        return dbMap.get(accession);
    }

    /**
     * Return true if the protein of given accession is contained in this database, false otherwise.
     *
     * @param accession protein accession.
     */
    public boolean contains(String accession)
    {
        return dbMap.containsKey(accession);
    }

    @Override
    public void clear()
    {
        dbMap.clear();
        dbMap = null;
    }

    @Override
    public Iterator<Protein> iterator()
    {
        return dbMap.values().iterator();
    }
}
