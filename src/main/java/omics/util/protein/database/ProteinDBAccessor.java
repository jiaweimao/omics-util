package omics.util.protein.database;

import omics.util.OmicsException;
import omics.util.OmicsTask;
import omics.util.io.MonitorableFileInputStream;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.database.uniprot.UniProtTxtReader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.zip.GZIPInputStream;

/**
 * Reader of protein database.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 16 Jan 2018, 2:32 PM
 */
public class ProteinDBAccessor extends OmicsTask<ProteinDB>
{
    private final Path file;
    private final boolean parseHeader;

    public ProteinDBAccessor(Path file)
    {
        this(file, false);
    }

    /**
     * Create a {@link ProteinDBAccessor}
     *
     * @param file        protein file
     * @param parseHeader true if get detailed information for fasta header, with protein type {@link UniProtFastaProtein}
     */
    public ProteinDBAccessor(Path file, boolean parseHeader)
    {
        this.file = file;
        this.parseHeader = parseHeader;
    }

    @Override
    public void go() throws OmicsException
    {
        updateTitle("Reading database file: " + file.getFileName());
        String name = file.toString().toLowerCase();
        if (name.endsWith(".txt")) {
            readTxt();
        } else if (name.endsWith(".txt.gz")) {
            readGzipTxt();
        } else if (name.endsWith(".fasta")) {
            readFasta();
        } else if (name.endsWith(".fasta.gz")) {
            readGzipFasta();
        } else {
            throw new OmicsException("Unrecognized file format: " + file.getFileName());
        }
        updateMessage("Finished");
    }

    private void readFasta()
    {
        try (MonitorableFileInputStream inputStream = new MonitorableFileInputStream(file);
             FastaReader reader = new FastaReader(inputStream, parseHeader)) {
            readFasta(reader, inputStream);
        } catch (IOException e) {
            setException(e);
            e.printStackTrace();
        }
    }

    private void readGzipFasta()
    {
        try (MonitorableFileInputStream inputStream = new MonitorableFileInputStream(file);
             FastaReader reader = new FastaReader(new GZIPInputStream(inputStream), parseHeader)
        ) {
            readFasta(reader, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFasta(FastaReader reader, MonitorableFileInputStream inputStream)
    {
        ProteinDB proteinDB = new ProteinDB();
        while (reader.hasNext()) {
            if (isStopped()) {
                updateMessage("Reading uniprot fasta: '" + file.getFileName() + "' is cancelled.");
                return;
            }

            Protein entry = reader.next();
            proteinDB.add(entry);

            updateProgress(inputStream.getProgress(), inputStream.getMaximum());
        }
        proteinDB.setType(reader.getDatabaseType());
        updateValue(proteinDB);
    }

    private void readGzipTxt()
    {
        try (MonitorableFileInputStream inputStream = new MonitorableFileInputStream(file);
             UniProtTxtReader reader = new UniProtTxtReader(new GZIPInputStream(inputStream))) {
            readTxt(reader, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readTxt()
    {
        try (MonitorableFileInputStream inputStream = new MonitorableFileInputStream(file);
             UniProtTxtReader reader = new UniProtTxtReader(inputStream)) {
            readTxt(reader, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readTxt(UniProtTxtReader reader, MonitorableFileInputStream inputStream)
    {
        ProteinDB proteins = new ProteinDB();
        while (reader.hasNext()) {
            if (isStopped()) {
                updateMessage("Reading uniprot txt: '" + file.getFileName() + "' is cancelled.");
                return;
            }
            UniProtTxtProtein entry = reader.next();
            proteins.add(entry);

            updateProgress(inputStream.getProgress(), inputStream.getMaximum());
        }
        updateValue(proteins);
    }

}
