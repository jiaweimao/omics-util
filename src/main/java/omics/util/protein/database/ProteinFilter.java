package omics.util.protein.database;

import omics.util.interfaces.Filter;

/**
 * interface to filter proteins in database.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 12:14 PM
 */
public interface ProteinFilter extends Filter<Protein> {}
