package omics.util.protein.database;

import java.util.function.Predicate;

/**
 * Parser for protein header information
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 09 Mar 2017, 9:36 AM
 */
public interface ProteinHeaderParser extends Predicate<String>
{
    /**
     * Get the DatabaseType after parsing the header.
     *
     * @return DatabaseType
     */
    DatabaseType getDatabaseType();

    /**
     * To test if this parser can parse the header.
     *
     * @param header a protein header string.
     * @return true if this parser can parse this header.
     */
    boolean test(String header);

    /**
     * parse the header and update the ProteinHeader value
     *
     * @param headerStr fasta header string
     * @param entry     a {@link Protein} to be updated.
     */
    Protein parse(String headerStr, Protein entry);
}
