package omics.util.protein.database;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import omics.util.OmicsObject;
import omics.util.interfaces.Copyable;
import omics.util.protein.AminoAcid;
import omics.util.utils.NumberFormatFactory;

import java.text.NumberFormat;
import java.util.Objects;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * A ptm site.
 *
 * @author JiaweiMao
 * @version 2.2.2
 * @since 09 Nov 2017, 7:59 PM
 */
public class PtmSite extends OmicsObject implements Comparable<PtmSite>, Copyable<PtmSite> {

    /**
     * Default number format, with 2 fraction digit
     */
    private static NumberFormat SiteScoreFormat = NumberFormatFactory.valueOf(2);
    private final String name;
    private final AminoAcid aminoAcid;
    private Integer proteinIndex;
    private Integer peptideIndex;
    private Double score;
    private String accession;
    private int size;
    private String window;

    /**
     * Copy constructor.
     *
     * @param ptmSite
     */
    public PtmSite(PtmSite ptmSite) {
        this.name = ptmSite.name;
        this.accession = ptmSite.accession;
        this.aminoAcid = ptmSite.aminoAcid;
        this.proteinIndex = ptmSite.proteinIndex;
        this.peptideIndex = ptmSite.peptideIndex;
        this.score = ptmSite.score;
        this.size = ptmSite.size;
        this.window = ptmSite.window;

        addMetas(ptmSite);

        PtmSite site = new PtmSite(this.name, this.accession, this.proteinIndex, this.aminoAcid, this.size);
        site.setScore(this.score);
        site.setIndexPeptide(this.peptideIndex);
        site.setSeqWindow(this.window);
        site.addMetas(this);
    }

    public PtmSite(String name, AminoAcid aminoAcid) {
        this(name, null, null, aminoAcid);
    }

    /**
     * Constructor with default size: 1.
     *
     * @param name      ptm name
     * @param accession protein accession
     * @param index     0-based ptm index in protein sequence
     * @param aminoAcid {@link AminoAcid} to be modified.
     */
    public PtmSite(String name, String accession, Integer index, AminoAcid aminoAcid) {
        this(name, accession, index, aminoAcid, 1);
    }

    /**
     * Constructor.
     *
     * @param name         ptm name
     * @param accession    protein accession
     * @param proteinIndex 0-based ptm index in protein
     * @param aminoAcid    amino acid
     * @param size         size
     */
    public PtmSite(String name, String accession, Integer proteinIndex, AminoAcid aminoAcid, int size) {
        checkNotNull(name);
        checkNotNull(aminoAcid);

        this.name = name;
        this.accession = accession;
        this.proteinIndex = proteinIndex;
        this.aminoAcid = aminoAcid;
        this.size = size;
    }

    @Override
    public PtmSite copy() {
        PtmSite site = new PtmSite(this.name, this.accession, this.proteinIndex, this.aminoAcid, this.size);
        site.setScore(this.score);
        site.setIndexPeptide(this.peptideIndex);
        site.setSeqWindow(this.window);
        site.addMetas(this);

        return site;
    }

    /**
     * parse the ptmsite, support 4 formats: <ul> <li>P62304:257:R3(CD3x2):100.0</li> <li>P62304:257:R3(CD3x2)</li>
     * <li>R3(CD3x2):100.0</li> <li>R3(CD3x2)</li> </ul>
     *
     * @param asite site string.
     * @return a {@link PtmSite}.
     */
    public static PtmSite parse(String asite) {
        String[] items = asite.split(":");

        String acc = null;
        Integer index = null;
        String coreName = null;
        Double score = null;

        if (items.length == 4) {
            acc = items[0];
            index = Integer.parseInt(items[1]) - 1;
            coreName = items[2];
            score = Double.parseDouble(items[3]);
        } else if (items.length == 3) {
            // acc:index:aa(namexsize)
            acc = items[0];
            index = Integer.parseInt(items[1]) - 1;
            coreName = items[2];
        } else if (items.length == 2) {
            // aa():score
            coreName = items[0];
            score = Double.parseDouble(items[1]);
        } else {
            coreName = items[0];
        }

        Integer pepIndex = null;
        String name;
        int size = 1;

        int leftParen = coreName.indexOf("(");
        int rightParen = coreName.indexOf(")");

        AminoAcid aa = AminoAcid.getAminoAcid(coreName.charAt(0));
        if (leftParen > 1) {
            pepIndex = Integer.parseInt(coreName.substring(1, leftParen)) - 1;
        }
        int xId = coreName.lastIndexOf("x");
        if (xId < 0 || !Character.isDigit(coreName.charAt(xId + 1))) {
            name = coreName.substring(leftParen + 1, rightParen);
        } else {
            name = coreName.substring(leftParen + 1, xId);
            size = Integer.parseInt(coreName.substring(xId + 1, rightParen));
        }

        PtmSite ptmSite = new PtmSite(name, acc, index, aa, size);
        ptmSite.setScore(score);

        if (pepIndex != null)
            ptmSite.setIndexPeptide(pepIndex);

        return ptmSite;
    }

    /**
     * set the site score toString format.
     *
     * @param format {@link NumberFormat} instance.
     */
    public static void setScoreFormat(NumberFormat format) {
        SiteScoreFormat = format;
    }

    /**
     * @return name of the ptm.
     */
    public String getName() {
        return name;
    }

    /**
     * @return {@link AminoAcid} this ptm modify.
     */
    public AminoAcid getAminoAcid() {
        return aminoAcid;
    }

    /**
     * @return protein accession of the protein this ptmsite on.
     */
    public String getAccession() {
        return accession;
    }

    /**
     * set the protein accession.
     *
     * @param accession protein accession.
     */
    public void setAccession(String accession) {
        this.accession = accession;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    /**
     * @return ptm index (0-based) in the protein sequence.
     */
    public Integer getIndexProtein() {
        return proteinIndex;
    }

    /**
     * set the ptm index(0-based) in protein sequence.
     *
     * @param index ptm index in protein.
     */
    public void setIndexProtein(int index) {
        this.proteinIndex = index;
    }

    /**
     * size means the number of PTM on the same amino acid, such as dimethylarginine, its size is 2.
     *
     * @return ptm size, default to be 1.
     */
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Return the sequence window.
     *
     * @return sequence window.
     */
    public String getSeqWindow() {
        return window;
    }

    public void setSeqWindow(String window) {
        this.window = window;
    }

    /**
     * @return the ptm index(0-based) in peptide.
     */
    public Integer getIndexPeptide() {
        return peptideIndex;
    }

    /**
     * set the index(0-based) of ptm in peptide sequence.
     *
     * @param peptideIndex ptm index in peptide.
     */
    public void setIndexPeptide(Integer peptideIndex) {
        this.peptideIndex = peptideIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PtmSite site = (PtmSite) o;
        return Objects.equals(peptideIndex, site.peptideIndex) &&
                Objects.equals(accession, site.accession) &&
                Objects.equals(proteinIndex, site.proteinIndex) &&
                aminoAcid == site.aminoAcid &&
                Objects.equals(name, site.name) &&
                Objects.equals(size, site.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(peptideIndex, accession, proteinIndex, aminoAcid, name, size);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (accession != null)
            builder.append(accession).append(":");
        if (proteinIndex != null)
            builder.append(proteinIndex + 1).append(":");
        builder.append(aminoAcid);
        if (peptideIndex != null)
            builder.append(peptideIndex + 1);
        builder.append("(");
        builder.append(name);
        if (size > 1)
            builder.append("x").append(size);

        builder.append(")");
        if (score != null)
            builder.append(":").append(SiteScoreFormat.format(score));

        return builder.toString();
    }

    @Override
    public int compareTo(PtmSite o) {
        return ComparisonChain.start()
                .compare(accession, o.accession, Ordering.natural().nullsLast())
                .compare(proteinIndex, o.proteinIndex, Ordering.natural().nullsLast())
                .compare(peptideIndex, o.peptideIndex, Ordering.natural().nullsLast())
                .compare(size, o.size).result();
    }

}
