package omics.util.protein.database;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 May 2019, 1:13 PM
 */
public class Site {

    private String proteinAccession;
    private int index;
    private String description;

    public Site(String proteinAccession, int index, String description) {
        this.proteinAccession = proteinAccession;
        this.index = index;
        this.description = description;
    }

    public String getProteinAccession() {
        return proteinAccession;
    }

    public int getIndex() {
        return index;
    }

    public String getDescription() {
        return description;
    }
}
