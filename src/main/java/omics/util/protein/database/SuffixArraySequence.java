package omics.util.protein.database;

import omics.util.string.SuffixArrays;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Suffix array for protein sequence.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 Jun 2019, 9:53 AM
 */
public class SuffixArraySequence
{
    private static final String EXT_SUFFIX = ".msa";

    public static final int MAX_LEN = 128;

    private final FastaSequence sequence;
    private final Path saPath;
    private final int size;

    /**
     * Create a {@link SuffixArraySequence} with given fasta file.
     *
     * @param fastaFile a fasta file path
     */
    public SuffixArraySequence(String fastaFile)
    {
        this(new FastaSequence(fastaFile));
    }

    public SuffixArraySequence(FastaSequence sequence)
    {
        this.sequence = sequence;
        this.size = sequence.size();
        this.saPath = Paths.get(sequence.getBaseFilePath() + EXT_SUFFIX);

        try {
            if (Files.notExists(saPath) || !checkFile()) {
                buildSA();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return number of characters of protein sequences.
     */
    public int getSize()
    {
        return size;
    }

    /**
     * @return the suffix array path.
     */
    public Path getSuffixArrayFile()
    {
        return saPath;
    }

    public FastaSequence getSequence()
    {
        return sequence;
    }

    private boolean checkFile() throws IOException
    {
        RandomAccessFile raf = new RandomAccessFile(saPath.toFile(), "r");
        raf.seek(0);
        int id = raf.readInt();
        int size = raf.readInt();

        raf.close();

        if (size != sequence.size())
            return false;

        return id == sequence.getId();
    }

    private void buildSA() throws IOException
    {
        DivSufSortByte divSufSortByte = new DivSufSortByte(sequence.getAlphabet());
        byte[] input = this.sequence.getByteSequence();
        int[] sa = divSufSortByte.buildSuffixArray(input, 0, input.length);
        byte[] lcps = SuffixArrays.createWithLCP(input, sa);

        int[] len2CountArray = new int[MAX_LEN + 1];
        Arrays.fill(len2CountArray, 0);

        DataOutputStream suffixOut = new DataOutputStream(new BufferedOutputStream(Files.newOutputStream(saPath)));
        suffixOut.writeInt(sequence.getId());
        suffixOut.writeInt(sequence.size());
        for (int i = 1; i < len2CountArray.length; i++) {
            suffixOut.writeInt(0);
        }

        for (int i = 0; i < sa.length; i++) {
            int index = sa[i];
            int lcp = lcps[i];

            suffixOut.writeInt(index);
            suffixOut.writeByte(lcp);

            if (i == 0)
                continue;

            String s = sequence.subStringEndTerm(index, Math.min(index + MAX_LEN + 2, size));
            if (s.length() < (lcp + 1))
                continue;
            for (int len = lcp + 1; len <= (s.length() - 2); len++) {
                len2CountArray[len]++;
            }
        }
        suffixOut.flush();
        suffixOut.close();

        FileChannel channel = new RandomAccessFile(saPath.toFile(), "rw").getChannel();
        channel.position(2 * Integer.BYTES);

        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES * (len2CountArray.length - 1));
        for (int i = 1; i < len2CountArray.length; i++) {
            buffer.putInt(len2CountArray[i]);
        }
        buffer.flip();
        channel.write(buffer);
        channel.close();
    }
}
