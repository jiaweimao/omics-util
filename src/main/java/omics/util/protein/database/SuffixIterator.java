package omics.util.protein.database;

import omics.util.io.csv.CsvWriter;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Iterator;

import static omics.util.protein.database.SuffixArraySequence.MAX_LEN;

/**
 * Iterator of suffix.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 Jun 2019, 3:55 PM
 */
public class SuffixIterator implements Iterator<IndexLCP>, Closeable
{
    private final SuffixArraySequence suffixArraySequence;
    private int currentId = 0;
    private final int size;
    private DataInputStream in;
    private int[] peptideCountArray;

    /**
     * Constructor.
     *
     * @param fastaFile fasta file.
     */
    public SuffixIterator(String fastaFile)
    {
        this(new SuffixArraySequence(fastaFile));
    }

    public SuffixIterator(SuffixArraySequence suffixArraySequence)
    {
        this.suffixArraySequence = suffixArraySequence;
        this.size = suffixArraySequence.getSize();
        try {
            this.in = new DataInputStream(new BufferedInputStream(Files.newInputStream(suffixArraySequence.getSuffixArrayFile())));
            in.skip(2 * Integer.SIZE / Byte.SIZE);
            peptideCountArray = new int[MAX_LEN + 1];
            Arrays.fill(peptideCountArray, 0);
            for (int i = 1; i < peptideCountArray.length; i++) {
                peptideCountArray[i] = in.readInt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the number of peptides of given length
     *
     * @param len peptide length
     */
    public int getPeptideCount(int len)
    {
        return peptideCountArray[len];
    }

    /**
     * @return maximum peptide length in statistics
     */
    public int getMaxPeptideLength()
    {
        return peptideCountArray.length - 1;
    }

    public FastaSequence getSequence()
    {
        return suffixArraySequence.getSequence();
    }

    public static void main(String[] args) throws IOException
    {
        SuffixIterator si = new SuffixIterator(new SuffixArraySequence("D:\\database\\uniprot\\uniprot-reviewed_yes.fasta"));
        FastaSequence sequence = si.getSequence();
        final int len = 64;
        int size = sequence.size();
        CsvWriter writer = new CsvWriter("D:\\database\\uniprot\\uniprot-reviewed_yes.csv");
        int cnt = 0;
        int ratio = 0;
        while (si.hasNext()) {
            IndexLCP pair = si.next();
            writer.write(pair.getIndex());
            writer.write(pair.getLCP());
            int index = pair.getIndex();
            int end = index + len;
            if (end > size)
                end = size;
            writer.write(sequence.subString(pair.getIndex(), end));
            writer.endRecord();
            cnt++;
            int newRatio = (int) (cnt / (double) size * 100);
            if (newRatio > ratio) {
                ratio = newRatio;
                System.out.println(ratio);
            }
        }
        writer.close();
        si.close();
    }

    @Override
    public boolean hasNext()
    {
        return currentId < size;
    }

    @Override
    public IndexLCP next()
    {
        int index = 0;
        byte lcp = 0;
        try {
            index = in.readInt();
            lcp = in.readByte();
            currentId++;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new IndexLCP(index, lcp);
    }

    @Override
    public void close() throws IOException
    {
        in.close();
    }
}
