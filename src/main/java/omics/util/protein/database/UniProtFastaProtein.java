package omics.util.protein.database;

/**
 * uniprot fasta protein with detailed information.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 22 Oct 2018, 7:28 PM
 */
public class UniProtFastaProtein extends Protein
{
    String entryName;
    String proteinName;
    String organismName;
    String taxonomicIdentifier;
    String geneName;
    String proteinExistence;
    String sequenceVersion;

    /**
     * The first item on the ID line is the entry name of the sequence. This name is a useful means of identifying a
     * sequence, but it is not a stable identifier as is the accession number. such as:
     * <p>
     * ID   CYC_BOVIN               Reviewed;         104 AA.
     */
    public String getEntryName()
    {
        return entryName;
    }

    public void setEntryName(String entryName)
    {
        this.entryName = entryName;
    }

    /**
     * The recommended name of the UniProtKB entry as annotated in the RecName field. For UniProtKB/TrEMBL entries
     * without a RecName field, the SubName field is used. In case of multiple SubNames, the first one is used.
     * The ‘precursor’ attribute is excluded, ‘Fragment’ is included with the name if applicable.
     */
    public String getProteinName()
    {
        return proteinName;
    }

    public void setProteinName(String proteinName)
    {
        this.proteinName = proteinName;
    }

    /**
     * scientific name of the organism, OS
     */
    public String getOrganismName()
    {
        return organismName;
    }

    /**
     * scientific name of the organism, OS
     */
    public void setOrganismName(String organismName)
    {
        this.organismName = organismName;
    }

    /**
     * unique identifier of the source organism, assigned by the NCBI, OX
     */
    public String getTaxonomicIdentifier()
    {
        return taxonomicIdentifier;
    }

    public void setTaxonomicIdentifier(String taxonomicIdentifier)
    {
        this.taxonomicIdentifier = taxonomicIdentifier;
    }

    /**
     * Return the main gene name, commonly it is the first gene name, and the one present in fasta file.
     */
    public String getGeneName()
    {
        return geneName;
    }

    public void setGeneName(String geneName)
    {
        this.geneName = geneName;
    }

    /**
     * the numerical value describing the evidence for the existence of the protein
     * With the following values:
     * 1: Evidence at protein level
     * 2: Evidence at transcript level
     * 3: Inferred from homology
     * 4: Predicted
     * 5: Uncertain
     */
    public String getProteinExistence()
    {
        return proteinExistence;
    }

    public void setProteinExistence(String proteinExistence)
    {
        this.proteinExistence = proteinExistence;
    }

    /**
     * @return version number of this protein sequence
     */
    public String getSequenceVersion()
    {
        return sequenceVersion;
    }

    /**
     * setter of the version of this protein sequence
     *
     * @param sequenceVersion version number
     */
    public void setSequenceVersion(String sequenceVersion)
    {
        this.sequenceVersion = sequenceVersion;
    }
}
