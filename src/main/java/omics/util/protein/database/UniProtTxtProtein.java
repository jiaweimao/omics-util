package omics.util.protein.database;

import omics.util.protein.AminoAcid;
import omics.util.utils.IntPair;
import omics.util.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A Protein Entry in the Database.
 *
 * @author JiaweiMao
 * @version 1.6.0
 * @since 19 Mar 2017, 10:35 AM
 */
public class UniProtTxtProtein extends UniProtFastaProtein
{
    private final List<String> accessionList = new ArrayList<>();
    private String integrateDate;
    private String sequenceDate;
    private String entryDate;
    private List<String> descriptionList = new ArrayList<>();
    private List<GeneName> geneNameList = new ArrayList<>();

    private List<String> organelleList;
    private int ncbiTaxID;
    private List<String> featureList;
    private Set<String> keywordSet;

    public UniProtTxtProtein() { }

    /**
     * @return the identifier of a specific organism in a taxonomic database
     */
    public int getNCBITaxID()
    {
        return ncbiTaxID;
    }

    public void setNCBITaxID(int taxID)
    {
        this.ncbiTaxID = taxID;
    }

    /**
     * Add an accession number to this protein entry.
     *
     * @param accession accession number.
     */
    public void addSecondaryAccession(String accession)
    {
        this.accessionList.add(accession);
    }

    /**
     * AC   AC_number_1;[ AC_number_2;]...[ AC_number_N;]
     * <p>
     * Researchers who wish to cite entries in their publications should always cite the first accession number. This is
     * commonly referred to as the 'primary accession number'. 'Secondary accession numbers' are sorted
     * alphanumerically.
     *
     * @return the accession number(s) associated with an entry
     */
    public List<String> getSecondaryAccessions()
    {
        return accessionList;
    }

    /**
     * The first DT line indicates when the entry first appeared in the database. The associated comment, 'integrated
     * into UniProtKB/database_name', indicates in which section of UniProtKB, Swiss-Prot or TrEMBL, the entry can be
     * found;format:
     * <p>
     * DT   DD-MMM-YYYY, integrated into UniProtKB/database_name.
     *
     * @return integrate date.
     */
    public String getIntegrateDate()
    {
        return integrateDate;
    }

    public void setIntegrateDate(String integrateDate)
    {
        this.integrateDate = integrateDate;
    }

    /**
     * The second DT line indicates when the sequence data was last modified. The associated comment, 'sequence
     * version', indicates the sequence version number. The sequence version number of an entry is incremented by one
     * when the amino acid sequence shown in the sequence record is modified; format:
     * <p>
     * DT   DD-MMM-YYYY, sequence version x.
     *
     * @return sequence update date.
     */
    public String getSequenceDate()
    {
        return sequenceDate;
    }

    public void setSequenceDate(String sequenceDate)
    {
        this.sequenceDate = sequenceDate;
    }

    /**
     * The third DT line indicates when data other than the sequence was last modified. The associated comment, 'entry
     * version', indicates the entry version number. The entry version number is incremented by one whenever any data in
     * the flat file representation of the entry is modified.
     * <p>
     * DT   DD-MMM-YYYY, entry version x.
     *
     * @return entry update date.
     */
    public String getEntryDate()
    {
        return entryDate;
    }

    public void setEntryDate(String entryDate)
    {
        this.entryDate = entryDate;
    }

    public List<GeneName> getGeneNameList()
    {
        return geneNameList;
    }

    public void setGeneNameList(List<GeneName> geneNameList)
    {
        this.geneNameList = geneNameList;
    }

    public List<String> getOrganelleList()
    {
        return organelleList;
    }

    public void setOrganelleList(List<String> organelleList)
    {
        this.organelleList = organelleList;
    }

    /**
     * @return feature list.
     */
    public List<String> getFeatureList()
    {
        return featureList;
    }

    /**
     * feature list setter.
     */
    public void setFeatureList(List<String> featureList)
    {
        this.featureList = featureList;
    }

    /**
     * Return all the PtmSite with name contains given string.
     *
     * @param name if a MOD_RES contains this name, it will be returned.
     * @return list of PtmSite, or an empty list if there is none.
     */
    public List<PtmSite> getPTMList(String name)
    {
        List<PtmSite> ptmSites = new ArrayList<>();
        for (String feature : featureList) {
            if (feature.startsWith("MOD_RES")) {
                String fromPoint = feature.substring(9, 15).trim();
                int index = Integer.parseInt(fromPoint);
                String fullname = feature.substring(29);

                int dotId = fullname.indexOf(".");
                if (dotId > 0)
                    fullname = fullname.substring(0, dotId);

                int semicolonId = fullname.indexOf(";");
                if (semicolonId > 0)
                    fullname = fullname.substring(0, semicolonId);

                if (StringUtils.containsIgnoreCase(fullname, name)) {
                    AminoAcid aminoAcid = AminoAcid.getAminoAcid(sequence.charAt(index - 1));
                    PtmSite ptmSite = new PtmSite(fullname, accessionList.get(0), index - 1, aminoAcid);

                    ptmSites.add(ptmSite);
                }
            }
        }
        return ptmSites;
    }

    /**
     * @return glycosylation sites in this protein, return an empty list for none.
     */
    public List<PtmSite> getGlycosylationSites()
    {
        List<PtmSite> ptmSites = new ArrayList<>();
        for (String feature : featureList) {
            if (!feature.startsWith("CARBOHYD"))
                continue;

            String fromPoint = feature.substring(9, 15).trim();
            int index = Integer.parseInt(fromPoint);
            String fullname = feature.substring(29);

            int dotId = fullname.indexOf(".");
            if (dotId > 0)
                fullname = fullname.substring(0, dotId);

            int semicolonId = fullname.indexOf(";");
            if (semicolonId > 0)
                fullname = fullname.substring(0, semicolonId);

            AminoAcid aminoAcid = AminoAcid.getAminoAcid(sequence.charAt(index - 1));
            PtmSite ptmSite = new PtmSite(fullname, getAccession(), index - 1, aminoAcid);

            ptmSites.add(ptmSite);
        }

        return ptmSites;
    }


    /**
     * @return Amino acid(s) involved in the activity of an enzyme.
     */
    public List<Site> getActivitySites()
    {
        List<Site> sites = new ArrayList<>();
        for (String feature : featureList) {
            if (!feature.startsWith("ACT_SITE"))
                continue;

            String fromPoint = feature.substring(9, 15).trim();
            int index = Integer.parseInt(fromPoint);
            String desp = "";
            if (feature.length() > 30)
                desp = feature.substring(29).trim();
            Site site = new Site(getAccession(), index, desp);
            sites.add(site);
        }
        return sites;
    }

    /**
     * @return Binding site for any chemical group (co-enzyme, prosthetic group, etc.).
     */
    public List<Site> getBindingSites()
    {
        List<Site> sites = new ArrayList<>();
        for (String feature : featureList) {
            if (!feature.startsWith("BINDING"))
                continue;

            String fromPoint = feature.substring(9, 15).trim();
            int index = Integer.parseInt(fromPoint);
            String desp = feature.substring(29).trim();
            Site site = new Site(getAccession(), index, desp);
            sites.add(site);
        }
        return sites;
    }

    /**
     * @return Any interesting single amino-acid site on the sequence, that is not defined by another feature key.
     * It can also apply to an amino acid bond which is represented by the positions of the two flanking amino acids.
     */
    public List<Site> getSites()
    {
        List<Site> sites = new ArrayList<>();
        for (String feature : featureList) {
            if (!feature.startsWith("SITE"))
                continue;

            String fromPoint = feature.substring(9, 15).trim();
            int index = Integer.parseInt(fromPoint);
            String desp = feature.substring(29).trim();
            Site site = new Site(getAccession(), index, desp);
            sites.add(site);
        }
        return sites;
    }

    /**
     * The signal peptide locate in the 'FT' section, with SIGNAL type.
     * if end if unknown, it is -1.
     *
     * @return the signal peptide position, or (-1,-1)
     */
    public IntPair getSignalPeptide()
    {
        for (String feature : featureList) {
            if (feature.startsWith("SIGNAL")) {
                String namePos = feature.split("\\n")[0];
                String pos = namePos.substring(6).trim();
                String[] poses = pos.split("\\.\\.");
                int start = Integer.parseInt(poses[0]);

                int end;
                if (poses[1].startsWith("?")) {
                    if (poses[1].length() == 1)
                        end = -1;
                    else
                        end = Integer.parseInt(poses[1].substring(1));
                } else
                    end = Integer.parseInt(poses[1]);

                return IntPair.create(start, end);
            }
        }
        return IntPair.create(-1, -1);
    }

    /**
     * Return a set of key words, of empty set if there is non key word.
     *
     * @return key word set.
     */
    public Set<String> getKeywords()
    {
        if (keywordSet == null)
            keywordSet = new HashSet<>();
        return keywordSet;
    }

    public void setKeywords(Set<String> keywordSet)
    {
        this.keywordSet = keywordSet;
    }


    @Override
    public String getDescription()
    {
        if (description == null) {
            StringBuilder builder = new StringBuilder();
            builder.append(entryName).append(" ");
            builder.append(proteinName).append(" ");
            if (getOrganismName() != null) {
                builder.append("OS=").append(organismName).append(" ");
            }
            if (taxonomicIdentifier != null) {
                builder.append("OX=").append(taxonomicIdentifier).append(" ");
            }
            if (geneName != null) {
                builder.append("GN=").append(geneName).append(" ");
            }
            if (proteinExistence != null) {
                builder.append("PE=").append(proteinExistence).append(" ");
            }
            if (sequenceVersion != null) {
                builder.append("SV=").append(sequenceVersion);
            }

            description = builder.toString();
        }
        return description;
    }

    /**
     * description list, contain protein name and alternative names...
     */
    public List<String> getDescriptionList()
    {
        return descriptionList;
    }

    /**
     * 'DE' lines in the txt format.
     */
    public void setDescriptionList(List<String> descriptionList)
    {
        this.descriptionList = descriptionList;
    }
}
