package omics.util.protein.database.uniprot;


import omics.util.protein.database.DatabaseType;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinHeaderParser;

/**
 * this class treat all the header as protein accession.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 May 2017, 9:37 AM
 */
public class AcceptAllHeaderParser implements ProteinHeaderParser
{
    @Override
    public DatabaseType getDatabaseType()
    {
        return DatabaseType.Unknown;
    }

    @Override
    public boolean test(String header)
    {
        return true;
    }

    @Override
    public Protein parse(String headerStr, Protein entry)
    {
        entry.setAccession(headerStr);
        return entry;
    }
}
