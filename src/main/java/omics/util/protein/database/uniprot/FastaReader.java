package omics.util.protein.database.uniprot;

import omics.util.io.IterativeReader;
import omics.util.io.LineReader;
import omics.util.protein.database.DatabaseType;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinHeaderParser;
import omics.util.utils.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * Reader for fasta file.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 16 Jan 2018, 1:17 PM
 */
public class FastaReader implements IterativeReader<Protein>
{
    private final List<ProteinHeaderParser> headerParsers;
    private final LineReader reader;
    private ProteinHeaderParser currentParser;
    private DatabaseType databaseType = null;
    private final boolean parseHeaderDetailed;

    public FastaReader(InputStream stream)
    {
        this(stream, false);
    }

    public FastaReader(String file) throws IOException
    {
        this(Paths.get(file));
    }

    /**
     * Create a {@link FastaReader} of given {@link Path}
     *
     * @param path {@link Path} of the file
     * @throws IOException for creating reader error
     */
    public FastaReader(Path path) throws IOException
    {
        this(Files.newBufferedReader(path), false);
    }

    public FastaReader(Reader reader, boolean parseHeaderDetailed)
    {
        this.parseHeaderDetailed = parseHeaderDetailed;
        this.reader = new LineReader(reader);

        this.headerParsers = new ArrayList<>();
        ServiceLoader<ProteinHeaderParser> parsers = ServiceLoader.load(ProteinHeaderParser.class);
        for (ProteinHeaderParser parser : parsers) {
            headerParsers.add(parser);
        }
        if (parseHeaderDetailed)
            currentParser = new UniProtKBFastaHeaderParser();
        else
            currentParser = new UniProtHeaderParser();
    }

    /**
     * Create FastaReader
     *
     * @param stream              a {@link InputStream}
     * @param parseHeaderDetailed true if parse the fasta header detailed.
     */
    public FastaReader(InputStream stream, boolean parseHeaderDetailed)
    {
        this.parseHeaderDetailed = parseHeaderDetailed;
        this.reader = new LineReader(new InputStreamReader(stream));

        this.headerParsers = new ArrayList<>();
        ServiceLoader<ProteinHeaderParser> parsers = ServiceLoader.load(ProteinHeaderParser.class);
        for (ProteinHeaderParser parser : parsers) {
            headerParsers.add(parser);
        }
        if (parseHeaderDetailed)
            currentParser = new UniProtKBFastaHeaderParser();
        else
            currentParser = new UniProtHeaderParser();
    }

    @Override
    public boolean hasNext()
    {
        String line = null;
        try {
            // skip empty lines
            for (line = reader.peek(); line != null && line.isEmpty(); line = reader.peek())
                reader.readLine();

            line = reader.peek();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return StringUtils.isNotEmpty(line);
    }

    @Override
    public Protein next()
    {
        Protein protein = null;
        StringBuilder builder = new StringBuilder();
        try {
            String header = reader.readLine();
            boolean parsed = false;
            if (currentParser != null) {
                if (currentParser.test(header)) {
                    protein = currentParser.parse(header, protein);
                    if (databaseType == null)
                        databaseType = currentParser.getDatabaseType();
                    parsed = true;
                }
            }

            if (!parsed) {
                for (ProteinHeaderParser parser : headerParsers) {
                    if (parser.test(header)) {
                        protein = parser.parse(header, protein);
                        parsed = true;
                        currentParser = parser;
                        break;
                    }
                }
            }

            if (!parsed) {
                currentParser = new AcceptAllHeaderParser();
                protein = currentParser.parse(header, protein);
            }

            for (String line = reader.peek(); StringUtils.isNotEmpty(line) && line.charAt(0) != '>'; line = reader.peek()) {
                builder.append(line);
                reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        protein.setSequence(builder.toString());
        return protein;
    }

    @Override
    public void close() throws IOException
    {
        reader.close();
    }

    /**
     * @return the {@link DatabaseType} of the fasta file.
     */
    public DatabaseType getDatabaseType()
    {
        return databaseType;
    }

}
