package omics.util.protein.database.uniprot;

import omics.util.protein.database.DatabaseType;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinHeaderParser;

/**
 * just extract accession number from the fasta header.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 22 Oct 2018, 7:19 PM
 */
public class UniProtHeaderParser implements ProteinHeaderParser
{
    private DatabaseType type;

    @Override
    public DatabaseType getDatabaseType()
    {
        return type;
    }

    @Override
    public boolean test(String header)
    {
        // 'sp' for UniProtKB/Swiss-Prot, 'tr' for UniProtKB/TrEMBL
        if (header.startsWith(">sp") || header.startsWith(">tr")) {
            type = DatabaseType.UniProtKB;
            return true;
        }

        return false;
    }

    @Override
    public Protein parse(String headerStr, Protein entry)
    {
        Protein protein;
        if (entry == null)
            protein = new Protein();
        else
            protein = entry;
        protein.setFastaHeader(headerStr);

        // remove ">"
        headerStr = headerStr.substring(1);
        int id1 = headerStr.indexOf("|");

        String id = headerStr.substring(0, id1); // sp or tr
        protein.setReviewed(id.equals("sp"));

        int id2 = headerStr.indexOf("|", id1 + 1);
        if (id2 == -1) {
            protein.setAccession(headerStr.substring(id1 + 1));
            return protein;
        }

        String acc = headerStr.substring(id1 + 1, id2);
        protein.setAccession(acc);

        headerStr = headerStr.substring(id2 + 1);
        protein.setDescription(headerStr);
        return protein;
    }
}
