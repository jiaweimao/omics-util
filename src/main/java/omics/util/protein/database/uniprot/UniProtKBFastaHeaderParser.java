package omics.util.protein.database.uniprot;

import omics.util.protein.database.DatabaseType;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinHeaderParser;
import omics.util.protein.database.UniProtFastaProtein;

/**
 * This class is used to parse information from the protein header string with format:
 * <a href="http://www.uniprot.org/help/fasta-headers">Header format</a>
 *
 * @author JiaweiMao
 * @version 1.0.5
 * @since 12 Oct 2017, 12:12 PM
 */
public class UniProtKBFastaHeaderParser implements ProteinHeaderParser
{
    private DatabaseType type;

    public DatabaseType getDatabaseType()
    {
        return type;
    }

    public boolean test(String header)
    {
        if (header.startsWith(">sp") || header.startsWith(">tr")) {
            type = DatabaseType.UniProtKB;
            return true;
        }

        return false;
    }

    @Override
    public Protein parse(String headerStr, Protein entry)
    {
        UniProtFastaProtein fastaProtein;
        if (entry == null) {
            fastaProtein = new UniProtFastaProtein();
        } else {
            fastaProtein = (UniProtFastaProtein) entry;
        }
        fastaProtein.setFastaHeader(headerStr);

        // remove ">"
        headerStr = headerStr.substring(1);
        int id1 = headerStr.indexOf("|");

        String id = headerStr.substring(0, id1); // sp or tr
        fastaProtein.setReviewed(id.equals("sp"));

        int id2 = headerStr.indexOf("|", id1 + 1);
        if (id2 == -1) {
            fastaProtein.setAccession(headerStr.substring(id1 + 1));
            return fastaProtein;
        }

        String acc = headerStr.substring(id1 + 1, id2);
        fastaProtein.setAccession(acc);

        headerStr = headerStr.substring(id2 + 1);
        fastaProtein.setDescription(headerStr);

        int osId = headerStr.indexOf("OS=");  //
        int oxId = headerStr.indexOf("OX=");
        int gnId = headerStr.indexOf("GN=");  // optional
        int peId = headerStr.indexOf("PE=");  // optional
        int svId = headerStr.indexOf("SV=");  // optional

        int end = headerStr.length();
        if (svId > 0) {
            fastaProtein.setSequenceVersion(headerStr.substring(svId + 3));
            end = svId - 1;
        }

        if (peId > 0) {
            fastaProtein.setProteinExistence(headerStr.substring(peId + 3, end));
            end = peId - 1;
        }

        if (gnId > 0) {
            fastaProtein.setGeneName(headerStr.substring(gnId + 3, end));
            end = gnId - 1;
        }

        if (oxId > 0) {
            fastaProtein.setTaxonomicIdentifier(headerStr.substring(oxId + 3, end));
            end = oxId - 1;
        }

        if (osId > 0) {
            fastaProtein.setOrganismName(headerStr.substring(osId + 3, end));
            end = osId - 1;
        }

        int spaceIndex = headerStr.indexOf(" ");
        if (spaceIndex > 0) {
            fastaProtein.setEntryName(headerStr.substring(0, spaceIndex));
            fastaProtein.setProteinName(headerStr.substring(spaceIndex + 1, end));
        } else {
            fastaProtein.setEntryName(headerStr.substring(0, end));
        }
        return fastaProtein;
    }
}
