package omics.util.protein.database.uniprot;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import omics.util.io.IterativeReader;
import omics.util.protein.database.GeneName;
import omics.util.protein.database.Protein;
import omics.util.protein.database.UniProtTxtProtein;
import omics.util.utils.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * UniProtKB txt format:
 * <p>
 * https://web.expasy.org/docs/userman.html
 * <p>
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 08 Nov 2017, 6:52 PM
 */
public class UniProtTxtReader implements IterativeReader<Protein>, AutoCloseable
{
    private final BufferedReader reader;
    private ListMultimap<String, String> currentMap;

    public UniProtTxtReader(InputStream stream)
    {
        this.reader = new BufferedReader(new InputStreamReader(stream));
    }

    @Override
    public boolean hasNext()
    {
        currentMap = ArrayListMultimap.create();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("//"))
                    break;
                String key = line.substring(0, 2);
                String content = line.substring(5);
                currentMap.put(key, content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return currentMap.containsKey("ID");
    }

    @Override
    public UniProtTxtProtein next()
    {
        UniProtTxtProtein entry = new UniProtTxtProtein();

        readID(currentMap.get("ID"), entry);
        readAC(currentMap.get("AC"), entry);
        readDT(currentMap.get("DT"), entry);
        readDE(currentMap.get("DE"), entry);   // once or more.
        readGN(currentMap.get("GN"), entry);
        readOS(currentMap.get("OS"), entry);
        readOG(currentMap.get("OG"), entry);
        readOC(currentMap.get("OC"), entry);
        readOX(currentMap.get("OX"), entry);

        readPE(currentMap.get("PE"), entry);
        readKW(currentMap.get("KW"), entry); // optional, multiple lines.
        readFT(currentMap.get("FT"), entry);
        readSQ(currentMap.get("  "), entry);

        return entry;
    }

    @Override
    public void close() throws IOException
    {
        reader.close();
    }

    /**
     * The ID (IDentification) line is always the first line of an entry. The general form of the ID line is:
     * <p>
     * ID   EntryName Status; SequenceLength.
     *
     * @param lines ID line
     */
    private void readID(List<String> lines, UniProtTxtProtein entry)
    {
        String line = lines.get(0);
        String[] items = line.split("\\s+");
//        System.out.println(Arrays.toString(items));
        checkArgument(items.length == 4, "ID line format is wrong.");

        entry.setEntryName(items[0]);

        // Reviewed;
        entry.setReviewed(!items[1].equals("Unreviewed;"));

        // the length is omitted.
    }

    /**
     * The AC (ACcession number) line lists the accession number(s) associated with an entry. The format of the AC line
     * is:
     * <p>
     * AC   AC_number_1;[ AC_number_2;]...[ AC_number_N;]
     *
     * @param lines lines of accession.
     */
    private void readAC(List<String> lines, UniProtTxtProtein entry)
    {
        boolean first = true;
        for (String line : lines) {
            String[] items = line.split(";");
            for (String item : items) {
                item = item.trim();
                if (first) {
                    entry.setAccession(item);
                    first = false;
                } else {
                    entry.addSecondaryAccession(item);
                }
            }
        }
    }

    /**
     * The DT (DaTe) lines show the date of creation and last modification of the database entry.
     * <p>
     * The format of the DT line in Swiss-Prot is:
     * <p>
     * DT   DD-MMM-YYYY, integrated into UniProtKB/database_name.
     * <p>
     * DT   DD-MMM-YYYY, sequence version x.
     * <p>
     * DT   DD-MMM-YYYY, entry version x.
     * <p>
     * There are always three DT lines in each entry, each of them is associated with a specific commnet.
     */
    private void readDT(List<String> lines, UniProtTxtProtein entry)
    {
        for (String line : lines) {
            String[] values = line.split(",");
            String date = values[0].trim();
            String name = values[1].trim();
            if (name.startsWith("integrated")) {
                entry.setIntegrateDate(date);
            } else if (name.startsWith("sequence")) {
                entry.setSequenceDate(date);
                entry.setSequenceVersion(name.substring(17, name.length() - 1));
            } else if (name.startsWith("entry")) {
                // entry version
                entry.setEntryDate(date);
                entry.setSequenceVersion(name.substring(14, name.length() - 1));
            }
        }
    }

    private void readDE(List<String> lines, UniProtTxtProtein entry)
    {
        entry.setDescriptionList(lines);
    }

    private void readGN(List<String> lines, UniProtTxtProtein entry)
    {
        List<GeneName> geneNameList = new ArrayList<>(1);

        GeneName geneName = new GeneName();
        for (String line : lines) {
            if (line.startsWith("and")) {
                geneNameList.add(geneName);
                geneName = new GeneName();
                continue;
            }

            String[] items = line.split(";");
            for (String item : items) {
                item = StringUtils.trimLeft(item);

                if (item.startsWith("Name=")) {
                    geneName.setName(item.substring(5));
                } else if (item.startsWith("Synonyms=")) {
                    String synnoyms = item.substring(9);
                    for (String syn : synnoyms.split(",")) {
                        geneName.addSynonyms(StringUtils.trimLeft(syn));
                    }
                } else if (item.startsWith("OrderedLocusNames=")) {
                    item = item.substring(18);
                    for (String locus : item.split(",")) {
                        geneName.addOrderedLocusName(StringUtils.trimLeft(locus));
                    }
                } else if (item.startsWith("ORFNames=")) {
                    item = item.substring(9);
                    for (String orf : item.split(",")) {
                        geneName.addORFName(StringUtils.trimLeft(orf));
                    }
                }
            }
        }
        geneNameList.add(geneName);

        entry.setGeneNameList(geneNameList);
    }

    private void readOS(List<String> lines, UniProtTxtProtein entry)
    {
        StringJoiner joiner = new StringJoiner(" ");
        for (String line : lines) {
            joiner.add(line);
        }
        String name = joiner.toString();
        int index = name.indexOf("(");
        if (index > 0)
            name = name.substring(0, index - 1);

        entry.setOrganismName(name);
    }

    private void readOG(List<String> lines, UniProtTxtProtein entry)
    {
        List<String> ogList = new ArrayList<>(1);
        for (String line : lines) {
            if (line.endsWith("."))
                line = line.substring(0, line.length() - 1);
            if (line.startsWith("Plastid")) {
                String[] values = line.split(",");
                for (String value : values) {
                    value = value.trim();
                    if (!value.startsWith("and")) {
                        ogList.add(value);
                    }
                }
            } else {
                ogList.add(line);
            }
        }
        entry.setOrganelleList(ogList);
    }

    private void readOC(List<String> lines, UniProtTxtProtein entry)
    {
    }

    /**
     * once
     */
    private void readOX(List<String> lines, UniProtTxtProtein entry)
    {
        String tax = lines.get(0);

        if (tax.startsWith("NCBI_TaxID")) { // NCBI_TaxID=9606 {ECO:0000312|EMBL:BAB69014.1};
            int id = StringUtils.parseIntFromString(tax.substring(tax.indexOf("=") + 1));
            entry.setNCBITaxID(id);
        }
    }

    /**
     * once.
     * <ul> <li>1: Evidence at protein level</li> <li>2: Evidence at transcript level</li> <li>3: Inferred from
     * homology</li> <li>4: Predicted 5: Uncertain</li> </ul>
     */
    private void readPE(List<String> lines, UniProtTxtProtein entry)
    {
        String pe = lines.get(0);
        pe = pe.substring(0, 1);
        entry.setProteinExistence(pe);
    }

    /**
     * optional.
     */
    private void readKW(List<String> lines, UniProtTxtProtein entry)
    {
        Set<String> keywordList = new HashSet<>();
        for (String line : lines) {
            String[] items = line.split(";");
            for (int i = 0; i < items.length - 1; i++) {
                keywordList.add(items[i].trim());
            }
            String last = items[items.length - 1];
            if (last.endsWith("."))
                last = last.substring(0, last.length() - 1);

            keywordList.add(last);
        }
        entry.setKeywords(keywordList);
    }

    /**
     * parse the FT lines and put them in the protein entry.
     *
     * @param lines FT lines without the 'FT' header.
     */
    private void readFT(List<String> lines, UniProtTxtProtein entry)
    {
        List<String> featureList = new ArrayList<>();
        StringBuilder value = new StringBuilder();
        for (String line : lines) {
            String key = line.substring(0, 8).trim();
//            System.out.println(line);
            if (key.isEmpty()) {
                value.append("\n").append(line.substring(16));
            } else {
                if (value.length() > 0) {
                    featureList.add(value.toString());
                }
                value = new StringBuilder(line);
            }
        }
        featureList.add(value.toString());
        entry.setFeatureList(featureList);
    }

    private void readSQ(List<String> lines, UniProtTxtProtein entry)
    {
        StringBuilder builder = new StringBuilder();
        for (String line : lines) {
            line = line.replace(" ", "");
            builder.append(line);
        }
        entry.setSequence(builder.toString());
    }
}
