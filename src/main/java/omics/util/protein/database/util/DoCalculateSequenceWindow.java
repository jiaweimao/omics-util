package omics.util.protein.database.util;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import omics.util.OmicsException;
import omics.util.OmicsTask;
import omics.util.io.csv.CsvReader;
import omics.util.io.csv.CsvWriter;
import omics.util.protein.AminoAcid;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinDB;
import omics.util.utils.Histogram;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to calculate sequence window.
 *
 * @author JiaweiMao
 * @version 2.3.0
 * @since 23 Jan 2018, 2:25 PM
 */
public class DoCalculateSequenceWindow extends OmicsTask<Void>
{
    private static final String PROTEIN_INDEX = "protein index";
    private static final String PEPTIDE_INDEX = "peptide index";
    private static final String PEPTIDE_SEQ = "peptide";
    private static final String PROTEIN = "protein";

    @Parameter(names = {"-f", "--fasta"}, required = true, description = "Fasta file path")
    private String fastaFile;

    @Parameter(names = {"-i", "--input"}, required = true, description = "Input csv file path")
    private String input;

    @Parameter(names = {"-o", "--output"}, required = true, description = "Output file path")
    private String outFile;

    @Parameter(names = {"-s", "--size"}, description = "Window size")
    private int windowSize = 7;

    public DoCalculateSequenceWindow(String fastaFile, String input, String outFile, int windowSize)
    {
        this.fastaFile = fastaFile;
        this.input = input;
        this.outFile = outFile;
        this.windowSize = windowSize;
    }

    public DoCalculateSequenceWindow() { }

    public static void main(String[] args)
    {
        DoCalculateSequenceWindow window = new DoCalculateSequenceWindow();
        JCommander jCommander = JCommander.newBuilder().addObject(window).build();
        jCommander.setProgramName("DoCalculateSequenceWindow");
        try {
            jCommander.parse(args);
            window.go();
        } catch (Exception e) {
            jCommander.usage();
            System.out.println("The input csv file header format:\n\tprotein[,protein index][,peptide][,peptide index]");
            System.out.println("If 'protein index' presents, then output the sequence window around the index");
            System.out.println("If 'peptide' presents, then output the position of the peptide, N-term window and C-term window");
            System.out.println("If 'peptide index' presents, then output sequence window around the index");
        }
    }

    @Override
    public void go() throws IOException, OmicsException
    {
        updateTitle("Calculate Sequence Window");

        ProteinDB proteinDB = ProteinDB.read(fastaFile);

        CsvReader reader = new CsvReader(input);
        reader.readHeaders();

        Map<String, Integer> headerMap = new HashMap<>();
        String[] headers = reader.getHeaders();
        for (int i = 0; i < headers.length; i++) {
            headerMap.put(headers[i].toLowerCase(), i);
        }

        CsvWriter writer = new CsvWriter(outFile);
        writer.writeValues(reader.getHeaders());

        boolean hasProteinId = headerMap.containsKey(PROTEIN_INDEX);
        boolean hasPeptideSeq = headerMap.containsKey(PEPTIDE_SEQ);
        boolean hasPeptideId = headerMap.containsKey(PEPTIDE_INDEX);

        writer.write("Protein Length");
        for (AminoAcid aa : AminoAcid.getStandardAminoAcids()) {
            writer.write(String.valueOf(aa.getOneLetterCode()));
        }
        if (hasProteinId) {
            writer.write("Site Window");
        }

        if (hasPeptideSeq) {
            writer.write("Start pos");
            writer.write("End pos");
            writer.write("N-term window");
            writer.write("C-term window");
            if (hasPeptideId)
                writer.write("Peptide Site Window");
        }
        writer.endRecord();

        while (reader.readRecord()) {
            String acc = reader.get(headerMap.get(PROTEIN));
            Protein entry = proteinDB.get(acc);
            if (entry == null) {
                writer.writeValues(reader.getValues());
                writer.write("PROTEIN NOT FOUND");
                writer.endRecord();
                continue;
            }

            // check for protein index
            String proteinWindow = null;
            if (hasProteinId) {
                int proteinId = Integer.parseInt(reader.get(headerMap.get(PROTEIN_INDEX)));
                proteinWindow = entry.subString(proteinId - 1, windowSize, windowSize);
            }

            // check for peptide sequence and peptide index
            if (hasPeptideSeq) {
                String peptide = reader.get(headerMap.get(PEPTIDE_SEQ));

                int[] indexes = entry.indexOf(peptide, false);
                if (indexes.length == 0) {
                    writer.writeValues(reader.getValues());
                    writeProtein(entry, writer);

                    if (proteinWindow != null) {
                        writer.write(proteinWindow);
                    }
                    writer.write("PEPTIDE NOT FOUND");
                    writer.endRecord();
                } else {
                    // for multiple indexes
                    for (int index : indexes) {
                        writer.writeValues(reader.getValues());
                        writeProtein(entry, writer);
                        if (proteinWindow != null) {
                            writer.write(proteinWindow);
                        }

                        writer.write(index + 1);
                        writer.write(index + peptide.length());
                        String leftWindow = entry.subString(index, windowSize, windowSize - 1);
                        String rightWindow = entry.subString(index + peptide.length(), windowSize, windowSize - 1);

                        writer.write(leftWindow);
                        writer.write(rightWindow);

                        // for peptide index
                        if (hasPeptideId) {
                            int peptideIndex = Integer.parseInt(reader.get(headerMap.get(PEPTIDE_INDEX)));
                            int proteinId = index + peptideIndex - 1;
                            String window = entry.subString(proteinId, windowSize, windowSize);
                            writer.write(window);
                        }

                        writer.endRecord();
                    }
                }
            } else {
                writer.writeValues(reader.getValues());
                writeProtein(entry, writer);
                if (proteinWindow != null) {
                    writer.write(proteinWindow);
                }
                writer.endRecord();
            }
        }

        reader.close();
        writer.close();
        proteinDB.clear();
    }

    private void writeProtein(Protein protein, CsvWriter writer) throws IOException
    {
        writer.write(protein.length());
        Histogram<Character> aaCount = protein.getAminoAcidCount();
        for (AminoAcid aa : AminoAcid.getStandardAminoAcids()) {
            int count = aaCount.getCount(aa.getOneLetterCode());
            writer.write(count);
        }
    }
}
