package omics.util.protein.database.util;

import omics.util.OmicsTask;
import omics.util.io.FilenameUtils;
import omics.util.io.IterativeReader;
import omics.util.protein.database.Protein;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.database.uniprot.UniProtTxtReader;
import omics.util.utils.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.GZIPInputStream;


/**
 * Used to generate decoy database.
 *
 * @author JiaweiMao
 * @version 2.3.0
 * @since 09 Nov 2017, 3:05 PM
 */
public class DoShuffleDB extends OmicsTask<Void>
{
    public static final String DECOY_PROTEIN_PREFIX = "REV_";
    public static final String TARGET_DECOY_DB_EXT = ".td.fa";
    public static final String TARGET_DB_EXT = ".t.fa";
    public static final String DECOY_DB_EXT = ".d.fa";

    public enum TagPos
    {
        HEAD,
        BEFORE_ACC,
        AFTER_ACC
    }

    /**
     * The type of decoy database.
     */
    public enum DecoyType
    {
        REVERSE, RANDOM
    }

    private final DecoyType decoyType;

    /**
     * Output writer.
     */
    private final Path inputFile;
    private final Path outputFile;
    private final boolean concated;
    private final TagPos tagPos;
    private final String tag;

    /**
     * Constructor with default settings:
     * <ul>
     * <li>output file in the same directory as the input file, with extension ".ForRev.fasta"</li>
     * <li>decoy tag: REV_</li>
     * <li>tag loc before protein accession</li>
     * <li>generate decoy protein with reverse sequence.</li>
     * </ul>
     *
     * @param inputFile input database file, support uniprot txt, txt.gz, fasta and fasta.gz.
     */
    public DoShuffleDB(File inputFile)
    {
        this(inputFile.toPath(), DecoyType.REVERSE, true, DECOY_PROTEIN_PREFIX, TagPos.BEFORE_ACC);
    }

    /**
     * This constructor takes an input DB, output file and task type. Output format is FASTA, tag is put after accession
     *
     * @param inputFile  File with the input database.
     * @param outputFile File with the output database. Output format is FASTA. Can be 'null' for output to StdOut.
     * @param taskType   int with the task type. To be chosen from the constants on this class.
     */
    public DoShuffleDB(File inputFile, File outputFile, DecoyType taskType, boolean concated, String tag)
    {
        this(inputFile, outputFile, taskType, concated, tag, TagPos.AFTER_ACC);
    }

    /**
     * Constructor to create a shuffleDBTask.
     *
     * @param inputFile  input file
     * @param outputFile output fasta file
     * @param taskType   task type, 0 for shuffle, 1 for reversed.
     * @param concated   true if concatenate target and decoy proteins
     * @param tag        decoy protein tag
     * @param tagPos     decoy protein tag position
     */
    public DoShuffleDB(File inputFile, File outputFile, DecoyType taskType, boolean concated, String tag, TagPos tagPos)
    {
        this(inputFile.toPath(), outputFile.toPath(), taskType, concated, tag, tagPos);
    }

    /**
     * Constructor with withput to fasta file with extension {@link this#TARGET_DECOY_DB_EXT}
     *
     * @param inputFile input file
     * @param taskType  {@link DecoyType}
     * @param concated  true if concatenate target and decoy proteins
     * @param tag       decoy protein tag
     * @param tagPos    decoy protein tag position
     */
    public DoShuffleDB(Path inputFile, DecoyType taskType, boolean concated, String tag, TagPos tagPos)
    {
        this(inputFile, FilenameUtils.newExtension(inputFile, TARGET_DECOY_DB_EXT), taskType, concated, tag, tagPos);
    }

    /**
     * Constructor to create a shuffleDBTask.
     *
     * @param inputFile  input file
     * @param outputFile output fasta file
     * @param taskType   task type, 0 for shuffle, 1 for reversed.
     * @param concated   true if concatenate target and decoy proteins
     * @param tag        decoy protein tag
     * @param tagPos     decoy protein tag position
     */
    public DoShuffleDB(Path inputFile, Path outputFile, DecoyType taskType, boolean concated, String tag, TagPos tagPos)
    {
        this.decoyType = taskType;
        this.outputFile = outputFile;
        this.inputFile = inputFile;
        this.concated = concated;
        this.tag = tag;
        this.tagPos = tagPos;
    }


    @Override
    public void go() throws IOException
    {
        updateTitle("Generate " + decoyType + " database for file: " + inputFile);

        IterativeReader<Protein> reader;

        BufferedInputStream inputStream = new BufferedInputStream(Files.newInputStream(inputFile));
        boolean parseHeader = false;
        String name = inputFile.getFileName().toString().toLowerCase();
        if (name.endsWith(".txt")) {
            reader = new UniProtTxtReader(inputStream);
        } else if (name.endsWith(".txt.gz")) {
            GZIPInputStream gz = new GZIPInputStream(inputStream);
            reader = new UniProtTxtReader(gz);
        } else if (name.endsWith(".fasta")) {
            reader = new FastaReader(inputStream, parseHeader);
        } else if (name.endsWith(".fasta.gz")) {
            GZIPInputStream gz = new GZIPInputStream(inputStream);
            reader = new FastaReader(gz, parseHeader);
        } else {
            throw new IOException("Unrecognized file format: " + inputFile);
        }

        PrintWriter writer = new PrintWriter(Files.newBufferedWriter(outputFile));
        while (reader.hasNext()) {
            Protein entry = reader.next();

            if (isStopped()) {
                updateMessage("Cancelled.");
                writer.close();
                reader.close();
            }
            if (concated) {
                entry.writeToFasta(writer);
            }

            switch (tagPos) {
                case HEAD: {
                    String header = entry.getFastaHeader();
                    String newHeader = ">" + tag + header.substring(1);
                    entry.setFastaHeader(newHeader);
                    break;
                }
                case BEFORE_ACC:
                    entry.setAccession(tag + entry.getAccession());
                    entry.setFastaHeader(null);
                    break;
                case AFTER_ACC:
                    entry.setAccession(entry.getAccession() + tag);
                    entry.setFastaHeader(null);
                    break;
            }

            if (decoyType == DecoyType.RANDOM)
                entry.setSequence(StringUtils.shuffle(entry.getSequence()));
            else
                entry.setSequence(StringUtils.reverse(entry.getSequence()));
            entry.writeToFasta(writer);

        }
        reader.close();
        writer.flush();
        writer.close();

        updateMessage("Finish");
    }
}
