package omics.util.protein.database.util;

import omics.util.protein.database.DatabaseType;
import omics.util.protein.database.NCBIProtein;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinHeaderParser;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Jul 2020, 7:46 PM
 */
public class NCBIHeaderParser implements ProteinHeaderParser
{
    @Override
    public DatabaseType getDatabaseType()
    {
        return DatabaseType.NCBI;
    }

    @Override
    public boolean test(String header)
    {
        return header.startsWith(">gi");
    }

    @Override
    public Protein parse(String headerStr, Protein entry)
    {
        NCBIProtein protein;
        if (entry == null)
            protein = new NCBIProtein();
        else
            protein = (NCBIProtein) entry;

        protein.setFastaHeader(headerStr);
        String header = headerStr.substring(4);

        int idx = header.indexOf("|");
        String gi = header.substring(0, idx);
        protein.setGI(gi);

        header = header.substring(idx + 1);
        if (header.startsWith("ref|"))
            header = header.substring(4);
        idx = header.indexOf("|");
        String acc = header.substring(0, idx);
        int dotIdx = acc.indexOf(".");
        protein.setAccession(acc.substring(0, dotIdx));
        protein.setVersion(acc.substring(dotIdx + 1, idx));
        protein.setDescription(header.substring(idx + 1).trim());
        return protein;
    }
}
