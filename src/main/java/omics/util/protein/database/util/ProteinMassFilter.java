package omics.util.protein.database.util;

import omics.util.protein.Peptide;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinFilter;

/**
 * Protein Mass filter
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Jul 2020, 10:23 AM
 */
public class ProteinMassFilter implements ProteinFilter
{
    private final double lower;
    private final double upper;

    /**
     * This constructor takes the upper and lower mass limits for this filter as
     * well as a boolean indicative of inversion.
     *
     * @param lower double with the lower mass threshold, inclusive
     * @param upper double with the upper mass threshold, inclusive
     */
    public ProteinMassFilter(double lower, double upper)
    {
        this.lower = lower;
        this.upper = upper;
    }

    @Override
    public boolean test(Protein filterable)
    {
        boolean result = false;

        Peptide peptide = Peptide.parse(filterable.getSequence());
        double mass = peptide.getMolecularMass();
        if ((mass <= upper) && (mass >= lower)) {
            result = true;
        }

        return result;
    }
}
