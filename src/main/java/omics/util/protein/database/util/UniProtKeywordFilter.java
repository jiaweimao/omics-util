package omics.util.protein.database.util;

import omics.util.interfaces.Filter;
import omics.util.protein.database.UniProtTxtProtein;

/**
 * This class implements the Taxonomy filter for the SwissProtDatabase.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Jul 2020, 11:22 AM
 */
public class UniProtKeywordFilter implements Filter<UniProtTxtProtein>
{
    private final String match;

    /**
     * Constructor
     *
     * @param match the match string must be detected in the KEYWORD field
     */
    public UniProtKeywordFilter(String match)
    {
        this.match = match.toUpperCase();
    }

    @Override
    public boolean test(UniProtTxtProtein filterable)
    {
        for (String keyword : filterable.getKeywords()) {
            if (keyword.toUpperCase().contains(match)) {
                return true;
            }
        }
        return false;
    }
}
