package omics.util.protein.digest;

import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntSortedSet;
import omics.util.chem.Sequence;
import omics.util.protein.AminoAcid;
import omics.util.protein.mod.Modification;
import omics.util.utils.StringUtils;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * This matcher is used to find the sites of cleavage in an amino-acid sequence.
 * <br>
 * The CleavageSiteMatcher is constructed from a string that represents the cleavage pattern.
 * <ol>
 * <li>The pattern is based on regex except that the | indicates the cleavage site.
 * The pattern consists of the amino acids that are matched to the left of the cut followed by | and
 * then the amino acids matched to the right of the cut.
 * <p>
 * K|X specifies that the cut is after every K (X is any amino acid).
 * </li>
 * <li>Amino acids that should not match can be specified using ^.
 * K|[^P] specifies that the cut should be after every K except is it is followed by a P.
 * As with regex any character class can be used to specify a position not just negation.
 * [KR]|[^P] specifies that the cut should be after every K or R except if followed by P.
 * </li>
 * <li>The pattern can be extended to more than one amino acid either side of the cleavage site.
 * For example [^P][FL]X[FLY]|[^P]X[^K] is a valid pattern that checks 4 amino acids to the left and
 * 3 amino acids to the right of the cleavage site.
 * </li>
 * <li>For Proteases that have two distinct patterns an "or" can be used. To cut before and
 * after K "K|X or X|K" can be used.
 * </li>
 * </ol>
 * The formal definition of the pattern defining the site is:
 * <h3>Formal definition of a cleavage site</h3>
 * <ul>
 * <li>{@code Site_altern} := {@code Site} (or {@code Site})*</li>
 * <li>{@code Site} := {@code Symbol_aa}+ {@code Cut-token} + {@code Symbol_aa}</li>
 * <li>{@code Symbol_aa} := {@code AA} | {@code AA_class} | {@code TERM_class}</li>
 * <li>{@code TERM_class} := 'ont{'({@code AA} | {@code AA_class})+'}' | 'oct{'(
 * {@code AA} | {@code AA_class})+'}'</li>
 * <li>{@code AA} := {@code Symbol_aa_namb} | {@code Symbol_aa_amb}</li>
 * <li>{@code Symbol_aa_namb} := [AC-IK-TVWY]</li>
 * <li>{@code Symbol_aa_amb} := [BJUXZ]</li>
 * <li>{@code AA_class} := '[' {@code AA} ']'</li>
 * <li>{@code Cut-token} := '|'</li>
 * </ul>
 * </p>
 * Examples:
 * <p>
 * [KR]|[^P] is the pattern for trypsin (cut after K or R except if followed byP)
 * DEVD|X or R|K this cuts to the right of DEVD or between R and K.
 * <p/>
 * To control the matching at the beginning or end of the protein the ont{} and
 * oct{} (<b>o</b>r <b>N</b> <b>t</b>erm & <b>o</b>r <b>C</b> <b>t</b>erm) can
 * be used to match the term or the pattern in the brackets for example:
 * <p>
 * ont{[^P]}X|S cuts ASPASS into A SPAS S
 * <p/>
 * http://web.expasy.org/peptide_cutter/peptidecutter_enzymes.html
 * <p>
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 13 Oct 2017, 11:03 AM
 */
public class CleavageSiteMatcher
{
    private final String cleavageFormat;
    private final Pattern cleavagePattern;
    private final Modification nTermMod;
    private final Modification cTermMod;

    /**
     * Pattern has a special format:
     * <p/>
     * Pn ... P4 P3 P2 P1 | P1' P2' P3' ... Pm'
     *
     * @throws CleavageSiteParseException if s is not a valid string
     */
    public CleavageSiteMatcher(String cleavageFormat) throws CleavageSiteParseException
    {
        this(cleavageFormat, null, null);
    }

    /**
     * Constructor.
     *
     * @param cleavageFormat the cleavage site
     * @param nTermMod       protease may modify peptide digest at nterm
     * @param cTermMod       protease may modify peptide digest at Cterm
     * @throws CleavageSiteParseException if s is not a valid string
     */
    public CleavageSiteMatcher(String cleavageFormat, Modification nTermMod, Modification cTermMod)
            throws CleavageSiteParseException
    {
        checkNotNull(cleavageFormat);

        this.cleavageFormat = cleavageFormat;
        if (StringUtils.isEmpty(cleavageFormat)) {
            this.cleavagePattern = Pattern.compile("(?!)");
        } else
            this.cleavagePattern = Pattern.compile(toRegex(cleavageFormat), Pattern.CASE_INSENSITIVE);

        this.nTermMod = nTermMod;
        this.cTermMod = cTermMod;
    }

    /**
     * Convert the cleavage pattern to 'Lookaround' type pattern.
     *
     * @param pattern the cleavage site pattern to convert.
     * @return the 'Lookaround' type internal pattern.
     * @throws CleavageSiteParseException if s is not a valid string
     */
    private static String toRegex(String pattern) throws CleavageSiteParseException
    {
        StringBuilder tmp = new StringBuilder();
        // X(AB(DN)CFGHIJKLMOPRSTUVWYZ(EQ))
        // 1. replace ambiguous symbols
        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            switch (c) {
                case 'X':
                    tmp.append("[A-Z]");
                    break;
                case 'B':
                    tmp.append("[DN]");
                    break;
                case 'J':
                    tmp.append("[IL]");
                    break;
                case 'Z':
                    tmp.append("[EQ]");
                    break;
                default:
                    tmp.append(c);
                    break;
            }
        }
        pattern = tmp.toString();

        // 2. split expressions
        StringBuilder sb = new StringBuilder();
        String[] expressions = pattern.split("\\s+or\\s+");
        for (String expr : expressions) {
            // 3. split at cleavage site
            String[] pprimes = expr.split("\\|");

            if (pprimes.length == 2) {
                // (?<=) match amino acids before cleavage site, (?=) match amino acids after cleavage site.
                sb.append("(?<=").append(pprimes[0]).append(")(?=").append(pprimes[1]).append(")|");
            } else if (pprimes.length < 2) {
                throw new CleavageSiteParseException("cleavage token '|' is missing in '" + pattern + "'!",
                        sb.length());
            } else {
                throw new CleavageSiteParseException("too many cleavage tokens '|' in '" + pattern + "'!", sb.length());
            }
        }
        // delete last '|'
        sb.delete(sb.length() - 1, sb.length());

        String regex = sb.toString();
        Pattern orCTermPattern = Pattern.compile("oct\\{(.*?)\\}");
        regex = orCTermPattern.matcher(regex).replaceAll("(?:$1|\\$)");

        Pattern orNTermPattern = Pattern.compile("ont\\{(.*?)\\}");
        regex = orNTermPattern.matcher(regex).replaceAll("(?:$1|\\^)");

        return regex;
    }


    /**
     * Return the cleavage site format
     *
     * @return The String representation of the matcher.
     */
    public String getCleavageFormat()
    {
        return cleavageFormat;
    }

    /**
     * Return the cleavage site regex pattern.
     *
     * @return Cleavage Pattern
     */
    public Pattern getRegex()
    {
        return cleavagePattern;
    }

    /**
     * Return the modification of the n-terminal of the released peptide
     *
     * @return the Nt modification, null for absent
     */
    public Optional<Modification> getNtermMod()
    {
        return Optional.ofNullable(nTermMod);
    }

    /**
     * Return the modification of the c-terminal of the released peptide
     *
     * @return the Ct modification.
     */
    public Optional<Modification> getCtermMod()
    {
        return Optional.ofNullable(cTermMod);
    }

    /**
     * Find potential cleavage sites over the given amino-acid sequence
     *
     * @param aaSeq   an amino-acid sequence
     * @param visitor an optional IntList to store cleavage sites
     * @return the number of cleavage sites
     */
    public int find(String aaSeq, IntList visitor)
    {
        Matcher matcher = cleavagePattern.matcher(aaSeq);
        int count = 0;
        while (matcher.find()) {
            if (visitor != null) {
                visitor.add(matcher.start());
            }
            count++;
        }

        return count;
    }


    /**
     * Find potential cleavage sites over the given amino-acid sequence
     *
     * @param aaSeq   an amino-acid sequence
     * @param visitor an optional CleavageSiteVisitor
     * @return the number of potential cleavage sites
     */
    public int find(String aaSeq, IntSortedSet visitor)
    {
        Matcher matcher = cleavagePattern.matcher(aaSeq);
        int count = 0;
        while (matcher.find()) {
            if (visitor != null) {
                visitor.add(matcher.start());
            }
            count++;
        }

        return count;
    }

    /**
     * Find potential cleavage sites over the given amino-acid sequence
     *
     * @param aaSeq   an amino-acid sequence
     * @param visitor an optional CleavageSiteVisitor
     * @return number of cleavage sites
     */
    public int find(Sequence<AminoAcid> aaSeq, IntList visitor)
    {
        Matcher matcher = cleavagePattern.matcher(aaSeq.toSymbolString());

        int count = 0;
        while (matcher.find()) {
            if (visitor != null) {
                visitor.add(matcher.start());
            }
            count++;
        }

        return count;
    }

    /**
     * Return true if the N-term of the sequence is cleavable by this enzyme, if the sequence start with '_', which indicate the start of protein, return true.
     *
     * @param sequence peptide sequence, should contain next amino acid after sequence, such as N.PEPTIDE,
     *                 the dot is used to indicate the C-term amino acid, it is in fact not exist in the sequence.
     * @return true if the N-term of the sequence is cleavable by this enzyme, false otherwise
     */
    public boolean isNTermCleavable(String sequence)
    {
        checkArgument(sequence.length() >= 2, "The sequence length is short than 2.");
        if (sequence.charAt(0) == '_')
            return true;

        Matcher matcher = cleavagePattern.matcher(sequence.substring(0, 2));
        return matcher.find();
    }

    /**
     * Return true if the C-term of the sequence is cleavable by this enzyme, if the sequence end with '_', which indicate the end of protein, return true.
     *
     * @param sequence peptide sequence, should contain next amino acid after sequence, such as PEPTIDE.C,
     *                 the dot is used to indicate the C-term amino acid, it is in fact not exist in the sequence.
     * @return true if the C-term of the sequence is cleavable by this enzyme, false otherwise
     */
    public boolean isCTermCleavable(String sequence)
    {
        checkArgument(sequence.length() >= 2, "The sequence length is short than 2.");
        if (sequence.charAt(sequence.length() - 1) == '_')
            return true;

        Matcher matcher = cleavagePattern.matcher(sequence.substring(sequence.length() - 2));
        return matcher.find();
    }

    /**
     * return the number of enzyme cleavaged termini. protein termini is identified by '_'
     *
     * @param annotation peptide annotation, peptide with previous and next amino acid.
     */
    public int getTerminiCount(String annotation)
    {
        int n = 0;
        if (annotation.charAt(0) == '_') {
            n++;
        } else {
            Matcher matcher = cleavagePattern.matcher(annotation.substring(0, 2));
            if (matcher.find())
                n++;
        }

        if (annotation.charAt(annotation.length() - 1) == '_') {
            n++;
        } else {
            Matcher matcher = cleavagePattern.matcher(annotation.substring(annotation.length() - 2));
            if (matcher.find())
                n++;
        }

        return n;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("Cleavage site: ").append(cleavageFormat);
        sb.append(", regex: ").append(cleavagePattern);
        if (cTermMod != null) {
            sb.append(", cterm: ").append(cTermMod);
        }
        if (nTermMod != null) {
            sb.append(", nterm: ").append(nTermMod);
        }
        return sb.toString();
    }
}
