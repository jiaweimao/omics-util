package omics.util.protein.digest;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Mar 2017, 8:34 AM
 */
public class CleavageSiteParseException extends IllegalArgumentException
{
    /**
     * The zero-based character offset into the string being parsed at which the
     * error was found during parsing.
     */
    private final int offset;

    public CleavageSiteParseException(String message)
    {
        this(message, -1);
    }

    public CleavageSiteParseException(String message, int offset)
    {
        super(message);
        this.offset = offset;
    }

    public int getOffset()
    {
        return offset;
    }
}
