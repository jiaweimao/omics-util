package omics.util.protein.digest;


import omics.util.protein.Peptide;
import omics.util.protein.Protein;

/**
 * This interface delegates to callbacks to control the digestion handled by <code>ProteinDigester</code>.
 * <p>
 * In the following we give some explanations about when those methods are called back.
 * <p>
 * <h2>Potential digest bounds</h2>
 * <p>
 * A bound in a digested protein is defined by the protease potential cleavage sites and the
 * protein N/C terminal end points. A protein with n potential cleavage sites will have n+2 bounds.
 * <p>
 * <h2>Digestion workflow</h2>
 * <p>
 * Internally <code>ProteinDigester</code> generates a sequence of pairs of these bounds for the digested protein
 * given the number of potential cleavage sites and the maximum number of missed cleavages. A protein with n potential
 * cleavage sites will potentially produce (n+1)(n+2)/2 potential digests.
 * <p>
 * Each pair of bounds then pass a series of process that lead to the production
 * of a peptide digest and its storing.
 * <p>
 * <h2>Callbacks</h2>
 * <p>
 * The following callbacks can affect these processes at different control point:
 * <ol>
 * <li>interruptDigestion(): the first control point where digestion can potentially stop</li>
 * <li>makeDigest(): this second control point lead to the potential building of
 * the current peptide digest</li>
 * <li>retainDigest(): this third control point potentially stores the digest
 * </li>
 * </ol>
 * <p>
 * The last method retainSemiDigest() is potentially called back when
 * semi-digests are generated after the previous digests have been produced.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Mar 2017, 8:22 AM
 */
public interface DigestionController
{
    /**
     * Control digestion flow given the potential digest bounds
     *
     * @param protein                  the protein being digested
     * @param proteinBoundsCount       the number of cleavage bounds found in the protein (+ 2 terminal bounds)
     * @param inclusiveLowerBoundIndex lower bound index (from 0 to proteinBoundsCount-2)
     * @param inclusiveUpperBoundIndex upper bound index (from 1 to proteinBoundsCount-1)
     * @return true if the digestion must be interrupted
     */
    boolean interruptDigestion(Protein protein, int proteinBoundsCount, int inclusiveLowerBoundIndex,
            int inclusiveUpperBoundIndex);

    /**
     * Make a peptide digest from the given protein.
     *
     * @param protein                    the protein to get peptide from.
     * @param inclusiveProteinLowerIndex the lower index of peptide in protein
     * @param exclusiveProteinUpperIndex the upper index of peptide in protein
     * @param missedCleavagesCount       the number of missed cleavages
     * @return true if a peptide digest has to be created
     */
    boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex,
            int missedCleavagesCount);

    /**
     * Process DigestedPeptide with/without missed cleavages
     *
     * @param protein     the protein being digested
     * @param digest      the actual digest
     * @param digestIndex the digest starting index in the protein
     * @return true if this digest is kept
     */
    boolean retainDigest(Protein protein, Peptide digest, int digestIndex);

    /**
     * Process Semi digested products. If enabled, the semi-digest are produced from all digests
     *
     * @param digest the actual semi digest
     * @return true if this semi digest is kept
     */
    boolean retainSemiDigest(Peptide digest);
}
