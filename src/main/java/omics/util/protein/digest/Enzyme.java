package omics.util.protein.digest;

import it.unimi.dsi.fastutil.chars.CharOpenHashSet;
import it.unimi.dsi.fastutil.chars.CharSet;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntSortedSet;
import omics.util.chem.Sequence;
import omics.util.protein.AminoAcid;
import omics.util.protein.mod.Modification;

import java.util.Objects;
import java.util.Optional;

import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * Enzyme is used to find Cleavage site in the protein sequence.
 * <p>
 * https://enzyme.expasy.org/
 *
 * @author JiaweiMao
 * @version 2.9.0
 * @since 13 Mar 2017, 12:46 PM
 */
public class Enzyme
{
    public static final String CNBr = "CNBr";
    public static final String Arg_C = "Arg-C";
    public static final String OpeRATOR = "OpeRATOR";
    public static final String Unspecific = "unspecific cleavage";

    public static final String Trypsin = "Trypsin";
    public static final String TrypsinP = "Trypsin/P";

    public static final Enzyme PEPSINE_PH_GT_2 = new Enzyme(new CleavageSiteMatcher(
            "ont{[^HKR]}ont{[^P]}[FLWY]|[X]oct{[^P]} or ont{[^HKR]}ont{[^P]}[^R]|[FLWY]oct{[^P]}"));
    public static final Enzyme CHYMOTRYPSIN_HIGH_SPEC = new Enzyme(new CleavageSiteMatcher("[FY]|[^P] or W|[^MP]"));
    public static final Enzyme CHYMOTRYPSIN_LOW_SPEC = new Enzyme(new CleavageSiteMatcher("[FLY]|[^P] or W|[^MP] or M|[^PY] or H|[^DMPW]"));
    public static final Enzyme PEPSINE_PH_1_3 = new Enzyme(new CleavageSiteMatcher(
            "ont{[^HKR]}ont{[^P]}[FL]|[X]oct{[^P]} or ont{[^HKR]}ont{[^P]}[^R]|[FL]oct{[^P]}"));

    /**
     * Return the Enzyme of given name.
     *
     * @param name enzyme name or PSI-MS accession
     * @return a {@link Enzyme} object.
     */
    public static Enzyme ofName(String name)
    {
        EnzymeFactory enzymeFactory = EnzymeFactory.getInstance();
        return enzymeFactory.get(name);
    }

    /**
     * N-term, C-term, any
     */
    public enum Mode
    {
        N,
        C,
        A
    }

    private final String name;
    private final String accession;
    private final String description;
    private final CleavageSiteMatcher cs;
    private double cleavageEfficiency = 0;
    private double neighborCleavageEfficiency = 0;

    private Mode mode;
    private CharOpenHashSet residueSet;

    /**
     * Create an Enzyme with full information.
     *
     * @param accession   accession
     * @param name        enzyme name
     * @param description enzyme description
     * @param pattern     match pattern
     * @param nTermMod    {@link Modification} add to N-term
     * @param cTermMod    {@link Modification} add to C-term
     */
    public Enzyme(String accession, String name, String description, String pattern,
                  Modification nTermMod, Modification cTermMod)
    {
        this(accession, name, description, new CleavageSiteMatcher(pattern, nTermMod, cTermMod));
    }

    public Enzyme(String accession, String name, String description, CleavageSiteMatcher cs)
    {
        checkNotNull(name);

        this.accession = accession;
        this.name = name;
        this.description = description;
        this.cs = cs;
        initCharset();
    }

    public Enzyme(CleavageSiteMatcher cs)
    {
        this(null, cs.getCleavageFormat(), null, cs);
    }

    private void initCharset()
    {
        String cleavageFormat = cs.getCleavageFormat();
        if (cleavageFormat.isEmpty()) {
            this.residueSet = new CharOpenHashSet(0);
            this.mode = Mode.A;
            return;
        }

        this.residueSet = new CharOpenHashSet(2);
        String[] exps = cleavageFormat.split("\\s+or\\s+");
        boolean isN = false;
        boolean isC = false;
        for (String exp : exps) {
            String[] primes = exp.split("\\|");
//            System.out.println(exp + "\t" + primes.length);
            assert primes.length == 2;

            String pre = primes[0];
            char c = pre.charAt(pre.length() - 1);
            if (c == ']') {
                int start;
                for (start = pre.length() - 2; start >= 0; start--) {
                    if (pre.charAt(start) == '[')
                        break;
                }
                if (pre.charAt(start + 1) != '^') {
                    isC = true;
                    for (int i = start + 1; i <= pre.length() - 2; i++) {
                        residueSet.add(pre.charAt(i));
                    }
                }
            } else {
                if (c != 'X') {
                    residueSet.add(c);
                    isC = true;
                }
            }

            String sub = primes[1];
            c = sub.charAt(0);
            if (c == '[') {
                if (sub.charAt(1) != '^') {
                    for (int i = 1; i < sub.length(); i++) {
                        char c1 = sub.charAt(i);
                        if (c1 == ']')
                            break;
                        residueSet.add(c1);
                    }
                    isN = true;
                }
            } else if (c != 'X') {
                residueSet.add(c);
                isN = true;
            }
        }
        residueSet.trim();
        if (isN == isC) {
            mode = Mode.A;
        } else if (isN) {
            mode = Mode.N;
        } else {
            mode = Mode.C;
        }
    }

    /**
     * @return name of this enzyme
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return PSI-MS accession of this enzyme.
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * @return the cleavage mode of the enzyme.
     */
    public Mode getMode()
    {
        return mode;
    }

    public boolean isCTerm()
    {
        return mode == Mode.C;
    }

    public boolean isNTerm()
    {
        return mode == Mode.N;
    }

    public boolean isAny()
    {
        return mode == Mode.A;
    }

    /**
     * Return true if the N-term of the sequence is cleavable by this enzyme, if the sequence start with '_', which indicate the start of protein, return true.
     *
     * @param sequence peptide sequence, should contain next amino acid after sequence, such as N.PEPTIDE,
     *                 the dot is used to indicate the C-term amino acid, it is in fact not exist in the sequence.
     * @return true if the N-term of the sequence is cleavable by this enzyme, false otherwise
     */
    public boolean isNTermCleavable(String sequence)
    {
        return cs.isNTermCleavable(sequence);
    }

    /**
     * @return residues around the cleavage sites.
     */
    public CharSet getResidueSet()
    {
        return residueSet;
    }

    /**
     * @return true if this enzyme has cleavage sites
     */
    public boolean hasCleavageSite()
    {
        return !residueSet.isEmpty();
    }

    /**
     * Return true if the cleavage amino acids of this Enzyme contain the residue
     *
     * @param residue amino acid residue
     * @return true if the cleavage amino acids of this enzyme contain the amino acid residue.
     */
    public boolean containResidue(char residue)
    {
        return residueSet.contains(residue);
    }

    /**
     * @return the amino acid residues around the cleavage site.
     */
    public String getResidues()
    {
        return new String(residueSet.toCharArray());
    }

    /**
     * The probability that a peptide end with a cleavage site.
     * <p>
     * E.g. for trypsin, the probability that a peptides ends with K or R.
     *
     * @return cleavage efficiency.
     */
    public double getCleavageEfficiency()
    {
        return cleavageEfficiency;
    }

    /**
     * Set the cleavage efficiency of this enzyme.
     *
     * @param efficiency cleavage efficiency
     */
    public void setCleavageEfficiency(double efficiency)
    {
        this.cleavageEfficiency = efficiency;
    }

    /**
     * The probability that the neighbor amino acid follows the enzyme rule.
     * <p>
     * E.g. trypsin, the amino acid before peptide is K or R.
     *
     * @return near cleavage efficiency
     */
    public double getNeighborCleavageEfficiency()
    {
        return neighborCleavageEfficiency;
    }

    /**
     * set the cleavage efficiency of the neighbor amino acids.
     *
     * @param efficiency cleavage efficiency.
     */
    public void setNeighborCleavageEfficiency(double efficiency)
    {
        this.neighborCleavageEfficiency = efficiency;
    }

    /**
     * @return enzyme description.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return n-terminal modification of the cleavage site.
     */
    public Optional<Modification> getNTermMod()
    {
        return cs.getNtermMod();
    }

    /**
     * @return c-terminal modification of the cleavage site.
     */
    public Optional<Modification> getCTermMod()
    {
        return cs.getCtermMod();
    }

    /**
     * Get the number of potential cleavage sites
     *
     * @param aaSeq an amino-acid sequence
     * @return number of potential cleavage sites
     */
    public int countCleavageSites(Sequence<AminoAcid> aaSeq)
    {
        return find(aaSeq, null);
    }

    /**
     * @return the cleavage site matcher
     */
    public CleavageSiteMatcher getCleavageSiteMatcher()
    {
        return cs;
    }

    /**
     * Find potential cleavage sites over the given amino-acid sequence
     *
     * @param aaSeq   an amino-acid sequence
     * @param visitor an optional CleavageSiteVisitor
     * @return number of cleavage sites
     */
    public int find(Sequence<AminoAcid> aaSeq, IntList visitor)
    {
        return cs.find(aaSeq, visitor);
    }

    /**
     * Return true if the C-term of the sequence is cleavable by this enzyme, if the sequence end with '_', which indicate the end of protein, return true.
     *
     * @param sequence peptide sequence, should contain next amino acid after sequence, such as PEPTIDE.C,
     *                 the dot is used to indicate the C-term amino acid, it is in fact not exist in the sequence.
     * @return true if the C-term of the sequence is cleavable by this enzyme, false otherwise
     */
    public boolean isCTermCleavable(String sequence)
    {
        return cs.isCTermCleavable(sequence);
    }

    /**
     * return the number of enzyme cleavaged termini. protein termini is identified by '_'
     *
     * @param annotation peptide annotation, peptide with previous and next amino acid.
     */
    public int getTerminiCount(String annotation)
    {
        return cs.getTerminiCount(annotation);
    }

    /**
     * Find potential cleavage sites over the given amino-acid sequence
     *
     * @param aaSeq   an amino-acid sequence
     * @param visitor an optional IntList to store cleavage sites
     * @return the number of cleavage sites
     */
    public int find(String aaSeq, IntList visitor)
    {
        return cs.find(aaSeq, visitor);
    }

    /**
     * Find potential cleavage sites over the given amino-acid sequence
     *
     * @param aaSeq   an amino-acid sequence
     * @param visitor an optional CleavageSiteVisitor
     * @return the number of potential cleavage sites
     */
    public int find(String aaSeq, IntSortedSet visitor)
    {
        return cs.find(aaSeq, visitor);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enzyme enzyme = (Enzyme) o;
        return name.equals(enzyme.name) &&
                Objects.equals(accession, enzyme.accession);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, accession);
    }

    @Override
    public String toString()
    {
        return name;
    }
}
