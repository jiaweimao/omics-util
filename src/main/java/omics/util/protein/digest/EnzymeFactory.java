package omics.util.protein.digest;

import omics.util.io.ParameterFile;
import omics.util.io.xml.XMLUtils;
import omics.util.protein.mod.Modification;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * Factory of Enzyme..
 *
 * @author JiaweiMao
 * @version 2.5.0
 * @since 13 Mar 2017, 12:47 PM
 */
public final class EnzymeFactory implements ParameterFile<Enzyme>
{
    private static class Holder
    {
        static final EnzymeFactory INSTANCE = new EnzymeFactory();
    }

    public static EnzymeFactory getInstance()
    {
        return Holder.INSTANCE;
    }

    private final HashMap<String, Enzyme> enzymeMap = new HashMap<>();
    private final ArrayList<Enzyme> enzymeList = new ArrayList<>();

    private EnzymeFactory()
    {
        try {
            List<Enzyme> initList = initialize(EnzymeFactory.class.getClassLoader());
            for (Enzyme enzyme : initList) {
                add(enzyme);
            }
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return an Enzyme with given name or PSI accession, or null if it is not contained in this factory..
     *
     * @param name enzyme name or PSI-MS accession, case insensitive.
     * @return a {@link Enzyme} object，or null if absent.
     */
    public Enzyme get(String name)
    {
        checkNotNull(name);

        return enzymeMap.get(name.toLowerCase());
    }

    /**
     * @return number of enzyme type in this Factory.
     */
    public int size()
    {
        return enzymeList.size();
    }

    /**
     * {@inheritDoc}
     *
     * @return available {@link Enzyme}s
     */
    @Override
    public List<Enzyme> getItemList()
    {
        return Collections.unmodifiableList(enzymeList);
    }

    /**
     * add a new enzyme.
     *
     * @param enzyme an {@link Enzyme}
     */
    public boolean add(Enzyme enzyme)
    {
        String nameKey = enzyme.getName().toLowerCase();
        if (enzymeMap.containsKey(nameKey) || (enzyme.getAccession() != null
                && enzymeMap.containsKey(enzyme.getAccession().toLowerCase()))) {
            return false;
        }
        enzymeMap.put(nameKey, enzyme);

        if (enzyme.getAccession() != null) {
            enzymeMap.put(enzyme.getAccession().toLowerCase(), enzyme);
        }
        enzymeList.add(enzyme);
        return true;
    }

    @Override
    public String getSerializeFileName()
    {
        return "enzymes.xml";
    }

    @Override
    public List<Enzyme> read(InputStream inputStream) throws XMLStreamException
    {
        XMLStreamReader reader = XMLUtils.createXMLStreamReader(inputStream);

        String name = null;
        String psi_ac = null;
        String description = null;
        String pattern = null;
        String nTermMod = null; // optional
        String cTermMod = null; // optional
        List<Enzyme> enzymeList = new ArrayList<>();
        while (reader.hasNext()) {
            int event = reader.next();
            if (XMLUtils.isEndElement(reader, "enzyme")) {
                Modification nTerm = null;
                if (nTermMod != null) {
                    nTerm = Modification.parseModification(nTermMod);
                    nTermMod = null;
                }
                Modification cTerm = null;
                if (cTermMod != null) {
                    cTerm = Modification.parseModification(cTermMod);
                    cTermMod = null;
                }

                Enzyme enzyme = new Enzyme(psi_ac, name, description, pattern, nTerm, cTerm);
                enzymeList.add(enzyme);
            } else if (event == XMLStreamConstants.START_ELEMENT) {
                String localName = reader.getLocalName();
                switch (localName) {
                    case "enzyme":
                        name = XMLUtils.mandatoryAttr(reader, "name");
                        psi_ac = reader.getAttributeValue(null, "psi_acc");
                        description = reader.getAttributeValue(null, "description");
                        break;
                    case "rule":
                        pattern = reader.getAttributeValue(null, "pattern");
                        break;
                    case "termMod":
                        cTermMod = reader.getAttributeValue(null, "cTerm");
                        nTermMod = reader.getAttributeValue(null, "nTerm");
                        break;
                }
            }
        }
        reader.close();
        return enzymeList;
    }

    @Override
    public void write(OutputStream outputStream) throws XMLStreamException
    {
        XMLStreamWriter writer = XMLUtils.createIndentXMLStreamWriter(outputStream);
        writer.writeStartDocument("UTF-8", "1.0");
        writer.writeStartElement("enzymeList");
        writer.writeAttribute("count", String.valueOf(enzymeList.size()));
        for (Enzyme enzyme : enzymeList) {
            writer.writeStartElement("enzyme");
            writer.writeAttribute("name", enzyme.getName());

            String accession = enzyme.getAccession();
            if (accession != null)
                writer.writeAttribute("psi_acc", enzyme.getAccession());
            if (enzyme.getDescription() != null)
                writer.writeAttribute("description", enzyme.getDescription());

            writer.writeEmptyElement("rule");
            CleavageSiteMatcher cleavageSiteMatcher = enzyme.getCleavageSiteMatcher();
            if (cleavageSiteMatcher != null) {
                writer.writeAttribute("pattern", cleavageSiteMatcher.getCleavageFormat());

                Optional<Modification> ntermMod = cleavageSiteMatcher.getNtermMod();
                Optional<Modification> ctermMod = cleavageSiteMatcher.getCtermMod();

                if (ctermMod.isPresent() || ntermMod.isPresent())
                    writer.writeEmptyElement("termMod");

                if (ntermMod.isPresent())
                    writer.writeAttribute("nTerm", ntermMod.get().getMass().toString());

                if (ctermMod.isPresent())
                    writer.writeAttribute("cTerm", ctermMod.get().getMass().toString());
            } else {
                writer.writeAttribute("pattern", "");
            }

            writer.writeEndElement();
        }
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();
    }
}
