package omics.util.protein.digest;

import omics.util.protein.AminoAcid;
import omics.util.protein.AminoAcidSet;
import omics.util.protein.database.FastaSequence;
import omics.util.protein.database.SuffixArraySequence;
import omics.util.protein.mod.Pos;

/**
 * Generate peptide variants according variable modifications for standard peptide sequence.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 02 Jul 2019, 9:46 AM
 */
public class PeptideGenerator
{
    private final AminoAcidSet aaSet;
    private final int maxNumVariantsPerPeptide;
    private final int maxNumMods;
    private final FastaSequence fastaSequence;
    /**
     * hold all possible amino acids' character
     */
    private final char[][] peptides;
    /**
     * size -> number of variants
     */
    private final int[] size;
    private int[][] numMods;
    /**
     * peptide length
     */
    private int length;

    /**
     * Construct with maximum 128 variants, 3 modifications and length 40 per peptide sequence,
     */
    public PeptideGenerator(AminoAcidSet aaSet, FastaSequence fastaSequence)
    {
        this(aaSet, 40, 3, 128, fastaSequence);
    }

    public PeptideGenerator(AminoAcidSet aaSet)
    {
        this(aaSet, null);
    }

    /**
     * constructor, as {@link FastaSequence} is not provided, you cannot use {@link PeptideGenerator#addWithEnd(int, int)}
     *
     * @param aaSet                    {@link AminoAcidSet}
     * @param maxPeptideLength         allowed maximum peptide length
     * @param maxNumMods               allowed maximum number of modifications
     * @param maxNumVariantsPerPeptide allowed maximum number of variants per peptide sequence.
     */
    public PeptideGenerator(AminoAcidSet aaSet, int maxPeptideLength, int maxNumMods, int maxNumVariantsPerPeptide)
    {
        this(aaSet, maxPeptideLength, maxNumMods, maxNumVariantsPerPeptide, null);
    }

    /**
     * Constructor.
     *
     * @param aaSet                    {@link AminoAcidSet}
     * @param maxPeptideLength         allowed maximum peptide length
     * @param maxNumMods               allowed maximum number of modifications per peptide.
     * @param maxNumVariantsPerPeptide maximum
     * @param fastaSequence            {@link FastaSequence}
     */
    public PeptideGenerator(AminoAcidSet aaSet, int maxPeptideLength, int maxNumMods,
                            int maxNumVariantsPerPeptide, FastaSequence fastaSequence)
    {
        this.aaSet = aaSet;
        this.maxNumVariantsPerPeptide = maxNumVariantsPerPeptide;
        this.maxNumMods = maxNumMods;
        this.fastaSequence = fastaSequence;
        this.peptides = new char[maxNumVariantsPerPeptide][maxPeptideLength + 1];
        this.size = new int[maxPeptideLength + 1];
        this.size[0] = 1;
        this.numMods = new int[maxNumVariantsPerPeptide][maxPeptideLength + 1];

        cacheAASet();
    }

    /**
     * @return current peptide length
     */
    public int getLength()
    {
        return length;
    }

    /**
     * @return number of variant peptides.
     */
    public int size()
    {
        return size[length];
    }

    /**
     * Return the peptide at given index.
     *
     * @param index index of the variant peptide
     * @return variant peptide at given index.
     */
    public String get(int index)
    {
        return new String(peptides[index], 1, length);
    }

    /**
     * add a new peptide, format: P-PEPTIDE-N, the first and the last amino acid is not treated as part of the peptide,
     * there are used to check if the peptide start from the protein n-term or end with protein c-term.
     */
    public void addWithEnd(String peptide)
    {
        this.length = peptide.length() - 2;
        if (FastaSequence.isTerminator(peptide.charAt(0))) {
            if (!addAminoAcid(protNTermAAResidue[peptide.charAt(1)], 1)) return;
        } else {
            if (!addAminoAcid(nTermAAResidue[peptide.charAt(1)], 1)) return;
        }

        for (int i = 2; i < peptide.length() - 2; i++) {
            if (!addAminoAcid(aaResidue[peptide.charAt(i)], i)) return;
        }
        if (FastaSequence.isTerminator(peptide.charAt(length + 1))) {
            addAminoAcid(protCTermAAResidue[peptide.charAt(length)], length);
        } else {
            addAminoAcid(cTermAAResidue[peptide.charAt(length)], length);
        }
    }


    /**
     * add a new peptide sequence
     *
     * @param peptide     peptide sequence
     * @param isProtNTerm true if the peptide start from the protein N-term
     * @param isProtCTerm true if the peptide end with the protein C-term
     */
    public void add(String peptide, boolean isProtNTerm, boolean isProtCTerm)
    {
        this.length = peptide.length();
        if (isProtNTerm) {
            if (!addAminoAcid(protNTermAAResidue[peptide.charAt(0)], 1)) return;
        } else {
            if (!addAminoAcid(nTermAAResidue[peptide.charAt(0)], 1)) return;
        }

        for (int i = 1; i < peptide.length() - 1; i++) {
            if (!addAminoAcid(aaResidue[peptide.charAt(i)], i + 1)) return;
        }
        if (isProtCTerm) {
            addAminoAcid(protCTermAAResidue[peptide.charAt(length - 1)], length);
        } else {
            addAminoAcid(cTermAAResidue[peptide.charAt(length - 1)], length);
        }
    }

    /**
     * add a new peptide.
     */
    public void addProteinCTermPeptide(String peptide)
    {
        this.length = peptide.length();
        if (!addAminoAcid(nTermAAResidue[peptide.charAt(0)], 1)) return;
        for (int i = 1; i < peptide.length() - 1; i++) {
            if (!addAminoAcid(aaResidue[peptide.charAt(i)], i + 1)) return;
        }
        addAminoAcid(protCTermAAResidue[peptide.charAt(length - 1)], length);
    }

    /**
     * add a new peptide.
     */
    public void addProteinNTermPeptide(String peptide)
    {
        this.length = peptide.length();
        if (!addAminoAcid(protNTermAAResidue[peptide.charAt(0)], 1)) return;
        for (int i = 1; i < peptide.length() - 1; i++) {
            if (!addAminoAcid(aaResidue[peptide.charAt(i)], i + 1)) return;
        }
        addAminoAcid(cTermAAResidue[peptide.charAt(length - 1)], length);
    }

    /**
     * add a new peptide
     */
    public void add(String peptide)
    {
        this.length = peptide.length();
        if (!addAminoAcid(nTermAAResidue[peptide.charAt(0)], 1)) return;
        for (int i = 1; i < peptide.length() - 1; i++) {
            if (!addAminoAcid(aaResidue[peptide.charAt(i)], i + 1)) return;
        }
        addAminoAcid(cTermAAResidue[peptide.charAt(length - 1)], length);
    }

    /**
     * Add a peptide in given range
     *
     * @param start       start index, inclusive
     * @param end         end index, exclusive
     * @param isProtNTerm true if this peptide start with protein N-term
     * @param isProtCTerm true if this peptide end with protein C-term
     */
    public void add(int start, int end, boolean isProtNTerm, boolean isProtCTerm)
    {
        this.length = end - start;
        if (isProtNTerm) {
            if (!addAminoAcid(protNTermAAResidue[fastaSequence.getChar(start)], 1)) return;
        } else {
            if (!addAminoAcid(nTermAAResidue[fastaSequence.getChar(start)], 1)) return;
        }

        for (int i = start + 1; i < end - 1; i++) {
            if (!addAminoAcid(aaResidue[fastaSequence.getChar(i)], i - start + 1)) return;
        }

        if (isProtCTerm) {
            addAminoAcid(protCTermAAResidue[fastaSequence.getChar(end - 1)], length);
        } else {
            addAminoAcid(cTermAAResidue[fastaSequence.getChar(end - 1)], length);
        }
    }

    /**
     * Add a peptide sequence in given range, the amino acid before and after peptide are already included.
     * <p>
     * They are used to test if the peptide start from the protein-NTerm or end with Protein-CTerm.
     *
     * @param start previous amino acid index
     * @param end   index after the next amino acid
     */
    public void addWithEnd(int start, int end)
    {
        this.length = end - start - 2;
        if (fastaSequence.isTerminator(start)) {
            if (!addAminoAcid(protNTermAAResidue[fastaSequence.getChar(start + 1)], 1)) return;
        } else {
            if (!addAminoAcid(nTermAAResidue[fastaSequence.getChar(start + 1)], 1)) return;
        }
        int len = 2;
        for (int i = start + 2; i < end - 2; i++) {
            if (!addAminoAcid(aaResidue[fastaSequence.getChar(i)], len)) return;
            len++;
        }
        if (fastaSequence.isTerminator(end - 1)) {
            addAminoAcid(protCTermAAResidue[fastaSequence.getChar(end - 2)], length);
        } else {
            addAminoAcid(cTermAAResidue[fastaSequence.getChar(end - 2)], length);
        }
    }

    /**
     * add a new residue to this peptide.
     *
     * @param aaResidueArr residue list
     * @param length       current length, for n-terminal, it is 1
     */
    private boolean addAminoAcid(char[] aaResidueArr, int length)
    {
        if (aaResidueArr == null) {
            this.size[this.length] = 0;
            return false;
        }
        int pepCount = size[length - 1];

        // add unmodified aa to current peptides
        for (int pepIndex = 0; pepIndex < pepCount; pepIndex++) {
            peptides[pepIndex][length] = aaResidueArr[0];
            numMods[pepIndex][length] = numMods[pepIndex][length - 1]; // for non-modified aa
        }
        size[length] = pepCount; // unmodified aa do not change the variant count

        // add modified residue: copy residues up to length - 1 into new array
        if (aaResidueArr.length > 1 && pepCount < maxNumVariantsPerPeptide) {
            int newIndex = pepCount;
            for (int pepIndex = 0; pepIndex < pepCount; pepIndex++) {
                int numModPep = numMods[pepIndex][length - 1];
                if (numModPep < maxNumMods) {
                    for (int j = 1; j < aaResidueArr.length; j++) {
                        // create new array for the new modified peptides
                        System.arraycopy(peptides[pepIndex], 1, peptides[newIndex], 1, length - 1);
                        peptides[newIndex][length] = aaResidueArr[j];
                        numMods[newIndex][length] = numModPep + 1;
                        newIndex++;
                        if (newIndex >= maxNumVariantsPerPeptide)
                            break;
                    }
                }
                if (newIndex >= maxNumVariantsPerPeptide)
                    break;
            }
            size[length] = newIndex;
        }
        return true;
    }


    //<editor-fold desc="amino acid cache">

    /**
     * amino acid residue -> amino acid residue list including modified
     */
    private char[][] aaResidue;
    private char[][] nTermAAResidue;
    private char[][] cTermAAResidue;
    private char[][] protNTermAAResidue;
    private char[][] protCTermAAResidue; // residue -> modified aa residue

    private void cacheAASet()
    {
        for (Pos location : Pos.getCommonsPositions()) {
            // reference to other locations
            char[][] stdResidue2Residues = null;

            if (location == Pos.ANY_WHERE) {
                stdResidue2Residues = aaResidue = new char[SuffixArraySequence.MAX_LEN][];
            } else if (location == Pos.ANY_N_TERM) {
                stdResidue2Residues = nTermAAResidue = new char[SuffixArraySequence.MAX_LEN][];
            } else if (location == Pos.ANY_C_TERM) {
                stdResidue2Residues = cTermAAResidue = new char[SuffixArraySequence.MAX_LEN][];
            } else if (location == Pos.PROTEIN_N_TERM) {
                stdResidue2Residues = protNTermAAResidue = new char[SuffixArraySequence.MAX_LEN][];
            } else if (location == Pos.PROTEIN_C_TERM) {
                stdResidue2Residues = protCTermAAResidue = new char[SuffixArraySequence.MAX_LEN][];
            }

            assert stdResidue2Residues != null;
            for (char aa : aaSet.getResiduesWithoutMods()) {
                AminoAcid[] aaArr = aaSet.getAminoAcids(location, aa);

                stdResidue2Residues[aa] = new char[aaArr.length];
                for (int i = 0; i < aaArr.length; i++) {
                    stdResidue2Residues[aa][i] = aaArr[i].getOneLetterCode();
                }
            }
        }
    }
    //</editor-fold>
}
