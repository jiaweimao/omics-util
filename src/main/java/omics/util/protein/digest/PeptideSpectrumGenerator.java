package omics.util.protein.digest;

import omics.util.protein.AminoAcid;
import omics.util.protein.AminoAcidSet;
import omics.util.protein.mod.Pos;

/**
 * This is class is used to generate theoretical spectrum of peptides.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 02 Jul 2019, 4:19 PM
 */
public class PeptideSpectrumGenerator
{
    private final AminoAcidSet aaSet;
    private final int[] nominalMasses;
    private final double[] masses;
    private int length;

    public PeptideSpectrumGenerator(AminoAcidSet aaSet, int maxPeptideLength)
    {
        this.aaSet = aaSet;
        this.nominalMasses = new int[maxPeptideLength + 1];
        this.masses = new double[maxPeptideLength + 1];
        cacheAASet();
        this.nominalMasses[0] = 0;
        this.masses[0] = 0;
    }

    /**
     * @return peptide nominal mass, the first is 0, and the last is the sum of peptide residues.
     */
    public int getNominalPeptideMass()
    {
        return nominalMasses[length];
    }

    /**
     * @return current peptide mass, which is the sum of all residues.
     */
    public double getPeptideMass()
    {
        return masses[length];
    }

    /**
     * @return the prefix nominal mass array of the peptide, the first is 0, and the last is the sum of peptide residues.
     */
    public int[] getPrefixNominalMasses()
    {
        return nominalMasses;
    }

    /**
     * @return prefix mass array of the peptide, the first is 0 and the last is the sum of peptide residues.
     */
    public double[] getPrefixMasses()
    {
        return masses;
    }

    /**
     * @return peptide length
     */
    public int getLength()
    {
        return length;
    }

    /**
     * Add a new peptide sequence to generate its theoretical spectrum, not start from protein-NTerm, nor
     * end with protein-CTerm.
     *
     * @param sequence peptide sequence
     */
    public void addPeptide(String sequence)
    {
        this.addPeptide(sequence, false, false);
    }

    /**
     * add a new peptide to generate its theoretical spectrum
     *
     * @param sequence       peptide sequence
     * @param isProteinNTerm true if the peptide start from the protein n-term
     * @param isProteinCTerm true if the peptide end with the protein c-term
     */
    public void addPeptide(String sequence, boolean isProteinNTerm, boolean isProteinCTerm)
    {
        this.length = sequence.length();
        if (isProteinNTerm) {
            nominalMasses[1] = protNTermAANominalMass[sequence.charAt(0)];
            masses[1] = protNTermAAMass[sequence.charAt(0)];
        } else {
            nominalMasses[1] = nTermAANominalMass[sequence.charAt(0)];
            masses[1] = nTermAAMass[sequence.charAt(0)];
        }

        for (int i = 1; i < sequence.length() - 1; i++) {
            nominalMasses[i + 1] = nominalMasses[i] + aaNominalMass[sequence.charAt(i)];
            masses[i + 1] = masses[i] + aaMass[sequence.charAt(i)];
        }

        if (isProteinCTerm) {
            nominalMasses[length] = nominalMasses[length - 1] + protCTermAANominalMass[sequence.charAt(length - 1)];
            masses[length] = masses[length - 1] + protCTermAAMass[sequence.charAt(length - 1)];
        } else {
            nominalMasses[length] = nominalMasses[length - 1] + cTermAANominalMass[sequence.charAt(length - 1)];
            masses[length] = masses[length - 1] + cTermAAMass[sequence.charAt(length - 1)];
        }
    }

    /**
     * amino acid residue -> amino acid nominal mass list
     */
    private int[] aaNominalMass;
    /**
     * amino acid residue -> amino acid accurate mass list
     */
    private double[] aaMass;

    /**
     * n-terminal nominal mass array, residue -> mass list
     */
    private int[] nTermAANominalMass;
    /**
     * n-terminal amino acid mass, residue -> mass list
     */
    private double[] nTermAAMass;

    // C-term aa set
    private int[] cTermAANominalMass; // residue -> mass list
    private double[] cTermAAMass;

    // Protein N-term aa set
    private int[] protNTermAANominalMass; // residue -> mass list
    private double[] protNTermAAMass;

    // Protein C-term aa set
    private int[] protCTermAANominalMass; // residue -> mass list
    private double[] protCTermAAMass;

    private void cacheAASet()
    {
        for (Pos location : Pos.getCommonsPositions())
            cacheAASet(location);
    }

    /**
     * cache AminoAcidSet to given {@link Pos}
     */
    private void cacheAASet(Pos location)
    {
        // reference to other locations
        int[] stdResidue2NominalMasses = null;
        double[] stdResidue2Masses = null;

        if (location == Pos.ANY_WHERE) {
            stdResidue2NominalMasses = aaNominalMass = new int[aaSet.getMaxResidue()];
            stdResidue2Masses = aaMass = new double[aaSet.getMaxResidue()];
        } else if (location == Pos.ANY_N_TERM) {
            stdResidue2NominalMasses = nTermAANominalMass = new int[aaSet.getMaxResidue()];
            stdResidue2Masses = nTermAAMass = new double[aaSet.getMaxResidue()];
        } else if (location == Pos.ANY_C_TERM) {
            stdResidue2NominalMasses = cTermAANominalMass = new int[aaSet.getMaxResidue()];
            stdResidue2Masses = cTermAAMass = new double[aaSet.getMaxResidue()];
        } else if (location == Pos.PROTEIN_N_TERM) {
            stdResidue2NominalMasses = protNTermAANominalMass = new int[aaSet.getMaxResidue()];
            stdResidue2Masses = protNTermAAMass = new double[aaSet.getMaxResidue()];
        } else if (location == Pos.PROTEIN_C_TERM) {
            stdResidue2NominalMasses = protCTermAANominalMass = new int[aaSet.getMaxResidue()];
            stdResidue2Masses = protCTermAAMass = new double[aaSet.getMaxResidue()];
        }

        AminoAcid[] aminoAcids = aaSet.getAminoAcids();
        for (AminoAcid aminoAcid : aminoAcids) {
            stdResidue2NominalMasses[aminoAcid.getOneLetterCode()] = aminoAcid.getNominalMass();
            stdResidue2Masses[aminoAcid.getOneLetterCode()] = aminoAcid.getMolecularMass();
        }
    }
}
