package omics.util.protein.digest;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import omics.util.protein.AminoAcid;
import omics.util.protein.Peptide;
import omics.util.protein.Protein;
import omics.util.protein.digest.util.AcceptAllDigestionController;
import omics.util.protein.digest.util.ForwardIntIntervalIterator;
import omics.util.protein.digest.util.IntInterval;
import omics.util.protein.digest.util.ReverseIntIntervalIterator;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;

import java.util.*;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * A {@code ProteinDigester} is defined by a name and a cleavage site matcher
 * that match the sites to be digested.
 * <p>
 * <h4>Terminal Modification</h4>
 * <p>
 * In rare case, a peptidase may modify the extremity of released digests. As an
 * example, CNBr modify the end terminal Methionine of the C-terminal digest in
 * homoserine lactone.
 * </p>
 * <p>
 * This object is immutable.
 * <p>
 * There are 2 modes of digestion:
 * <dl>
 * <dt>Mode.STANDARD</dt>
 * <dd>no-missed-cleavage and missed-cleavage digests produced depending on MC
 * param</dd>
 * <dt>Mode.SEMI</dt>
 * <dd>a supplementary step generates all the semi digests from the
 * no-missed-cleavage digests (http://thegpm.org/TANDEM/api/pcsemi.html)</dd>
 * </dl>
 *
 * @author JiaweiMao
 * @version 2.2.0
 * @since 23 Jan 2018, 8:24 AM
 */
public class Protease
{
    public static Protease Unspecific()
    {
        return new Protease(Enzyme.ofName("unspecific cleavage"), Integer.MAX_VALUE);
    }

    public enum Cleavage
    {
        /**
         * Specifies a full enzymatic digestion
         */
        Specific,
        /**
         * Specifies a semi-enzymatic digestion. The cleavage specific at one terminal end
         */
        Semi,
        /**
         * Specifies an unspecific digestion with an extra weight for enzymatically digested peptides if need.
         */
        UnSpecific
    }

    public enum CleavageSiteIteration
    {
        FORWARD, REVERSE
    }

    /**
     * the cleavage site pattern
     */
    private final Enzyme enzyme;
    private int maxMissCleavage;
    private Cleavage cleavage;

    private CleavageSiteIteration iterMode;
    private final SetMultimap<AminoAcid, Modification> fixedMods = HashMultimap.create();
    private DigestionController digestProcessor;

    public Protease(Enzyme enzyme, int maxMissCleavage, Cleavage cleavage, CleavageSiteIteration iterMode, DigestionController digestProcessor)
    {
        checkNotNull(enzyme);
        checkArgument(maxMissCleavage >= 0, "Missed cleavage should >= 0");

        this.enzyme = enzyme;
        this.maxMissCleavage = maxMissCleavage;
        this.cleavage = cleavage;
        this.iterMode = iterMode;
        this.digestProcessor = digestProcessor;

        check();
    }

    private void check()
    {
        if (this.enzyme.getName().equals(Enzyme.Unspecific)) {
            this.cleavage = Cleavage.UnSpecific;
        }
    }

    public Protease(Enzyme enzyme, int maxMissCleavage, Cleavage cleavage)
    {
        this(enzyme, maxMissCleavage, cleavage, CleavageSiteIteration.FORWARD, new AcceptAllDigestionController());
    }

    /**
     * Create a Protease with default settings.
     *
     * @param enzyme   {@link Enzyme}
     * @param cleavage {@link Cleavage} type
     */
    public Protease(Enzyme enzyme, Cleavage cleavage)
    {
        this(enzyme, 0, cleavage, CleavageSiteIteration.FORWARD, new AcceptAllDigestionController());
    }

    public Protease(Enzyme enzyme, int maxMissCleavage)
    {
        this(enzyme, maxMissCleavage, Cleavage.Specific, CleavageSiteIteration.FORWARD, new AcceptAllDigestionController());
    }

    /**
     * Create a {@link Protease} with 0 missed cleavage and {@link Cleavage#Specific}.
     */
    public Protease(Enzyme enzyme)
    {
        this(enzyme, 0, Cleavage.Specific, CleavageSiteIteration.FORWARD, new AcceptAllDigestionController());
    }

    public Protease(CleavageSiteMatcher matcher)
    {
        this(new Enzyme(matcher));
    }

    public Protease addFixedMod(AminoAcid target, Modification mod)
    {
        checkNotNull(target);
        checkNotNull(mod);

        fixedMods.put(target, mod);
        return this;
    }

    public Protease controller(DigestionController digestProcessor)
    {
        this.digestProcessor = digestProcessor;
        return this;
    }

    public Protease iterationOrder(CleavageSiteIteration siteIteration)
    {
        this.iterMode = siteIteration;
        return this;
    }

    /**
     * @return the name of the enzyme
     */
    public String getName()
    {
        return enzyme.getName();
    }

    /**
     * @return the Enzyme.
     */
    public Enzyme getEnzyme()
    {
        return enzyme;
    }

    /**
     * @return true if the enzyme is unspecific.
     */
    public boolean isUnspecific()
    {
        return this.cleavage == Cleavage.UnSpecific;
    }

    /**
     * @return true if the enzyme is semi-specific.
     */
    public boolean isSemi()
    {
        return this.cleavage == Cleavage.Semi;
    }

    public void setCleavage(Cleavage cleavage)
    {
        this.cleavage = cleavage;
        check();
    }

    /**
     * @return the {@link Cleavage} type.
     */
    public Cleavage getCleavage()
    {
        return cleavage;
    }

    /**
     * @return true if this enzyme is specific.
     */
    public boolean isSpecific()
    {
        return cleavage == Cleavage.Specific;
    }

    /**
     * Return the max allowed missed cleavage site.
     */
    public int getMaxMissedCleavage()
    {
        return maxMissCleavage;
    }

    /**
     * setter of the allowed maximum number of missed cleavage.
     */
    public void setMaxMissCleavage(int maxMissCleavage)
    {
        this.maxMissCleavage = maxMissCleavage;
    }

    public SetMultimap<AminoAcid, Modification> getFixedMods()
    {
        return fixedMods;
    }

    /**
     * Digest {@code protein}
     *
     * @param protein the protein to digest.
     * @return the digests.
     */
    public List<Peptide> digest(Protein protein)
    {
        // container for future digests
        List<Peptide> digests = new ArrayList<>();

        digest(protein, digests);

        return digests;
    }

    /**
     * Digest {@code protein} given a container for digests
     *
     * @param protein the protein to digest
     * @param digests a list of digests
     * @return the digests.
     */
    public Collection<Peptide> digest(Protein protein, Collection<Peptide> digests)
    {
        checkNotNull(protein);

        IntList bounds = new IntArrayList();
        bounds.add(0);
        // 1. detect all digests bounds
        enzyme.find(protein, bounds);
        bounds.add(protein.size());

        // 2. generate combinations of all digest given MC#
        generateDigests(protein, bounds, digests);

        // 3. SEMI digestion for MC#0 digests
        if (cleavage == Cleavage.Semi) {
            List<Peptide> semiDigests = new ArrayList<>();
            for (Peptide digest : digests) {
//                if (enzyme.countCleavageSites(digest) == 0)
                semiDigests.addAll(generateSemiDigests(digest));
            }

            digests.addAll(semiDigests);
        }

        return digests;
    }

    private List<Peptide> generateSemiDigests(Peptide digestedPeptide)
    {
        List<Peptide> semiDigests = new ArrayList<>();

        for (int i = 0; i < digestedPeptide.size() - 1; i++) {
            Optional<Peptide> nSemiDigest = newOptSemiDigestedPeptide(digestedPeptide, 0, i + 1);
            Optional<Peptide> cSemiDigest = newOptSemiDigestedPeptide(digestedPeptide, i + 1, digestedPeptide.size());
            nSemiDigest.ifPresent(semiDigests::add);
            cSemiDigest.ifPresent(semiDigests::add);
        }

        return semiDigests;
    }

    private void generateDigests(Protein protein, IntList sites, Collection<Peptide> digests)
    {
        Iterator<IntInterval> iterator;

        int rangeMax = (maxMissCleavage == Integer.MAX_VALUE) ? maxMissCleavage : maxMissCleavage + 1;
        switch (iterMode) {
            case FORWARD:
                iterator = new ForwardIntIntervalIterator(0, sites.size() - 1, rangeMax);
                break;
            case REVERSE:
                iterator = new ReverseIntIntervalIterator(0, sites.size() - 1, rangeMax);
                break;
            default:
                throw new IllegalStateException("Unknown iteration mode " + iterMode);
        }

        while (iterator.hasNext()) {
            IntInterval cursor = iterator.next();

            int currentMissedCleavages = cursor.getCurrentRange() - 1;
            int digestLowerIndex = sites.getInt(cursor.getLowerBound());
            int digestUpperIndex = sites.getInt(cursor.getUpperBound());

            if (!digestProcessor.makeDigest(protein, digestLowerIndex, digestUpperIndex, currentMissedCleavages))
                continue;

            Peptide.Builder peptideBuilder = protein.getPeptideBuilder(digestLowerIndex, digestUpperIndex);
            // adding mods
            addModificationsToAll(peptideBuilder, protein, digestLowerIndex, digestUpperIndex, fixedMods);
            addPotentialModsCausedByDigester(peptideBuilder, protein, digestLowerIndex, digestUpperIndex);

            Peptide peptide = peptideBuilder.build();

            if (digestProcessor.retainDigest(protein, peptide, digestLowerIndex))
                digests.add(peptide);

            if (digestProcessor.interruptDigestion(protein, sites.size(), cursor.getLowerBound(),
                    cursor.getUpperBound()))
                break;
        }
    }

    private Optional<Peptide> newOptSemiDigestedPeptide(Peptide parent, int leftBound, int rightBound)
    {
        Peptide digest = new Peptide(parent, leftBound, rightBound);
        if (digestProcessor.retainSemiDigest(digest))
            return Optional.of(digest);

        return Optional.empty();
    }

    /**
     * Some ProteinDigester can modify digest's N or C terminal
     */
    private void addPotentialModsCausedByDigester(Peptide.Builder peptideBuilder, Protein protein, int leftBoundIndex,
            int rightBoundIndex)
    {
        CleavageSiteMatcher cs = enzyme.getCleavageSiteMatcher();

        Optional<Modification> ctermMod = cs.getCtermMod();
        Optional<Modification> ntermMod = cs.getNtermMod();
        boolean addCterm = ctermMod.isPresent() && rightBoundIndex < protein.size() - 1;
        boolean addNterm = ntermMod.isPresent() && leftBoundIndex > 0;

        if (!addCterm && !addNterm)
            return;

        // mod potentially done by protease if not the last digest
        if (addCterm) {
            peptideBuilder.addModification(ModAttachment.C_TERM, ctermMod.get());
        }

        // mod potentially added by protease if not the first digest
        if (addNterm)
            peptideBuilder.addModification(ModAttachment.N_TERM, ntermMod.get());
    }

    private void addModificationsToAll(Peptide.Builder peptideBuilder, Protein protein, int start, int end,
            SetMultimap<AminoAcid, Modification> fixedModifications)
    {
        if (fixedMods.isEmpty())
            return;

        for (AminoAcid targetAA : fixedModifications.keys()) {

            Collection<Modification> modifications = fixedModifications.get(targetAA);
            for (int i = start; i < end; i++) {
                AminoAcid aa = protein.getSymbol(i);

                if (aa.equals(targetAA)) {
                    final int index = i - start;
                    for (Modification mod : modifications) {

                        peptideBuilder.addModification(index, mod);
                    }
                }
            }
        }
    }

}
