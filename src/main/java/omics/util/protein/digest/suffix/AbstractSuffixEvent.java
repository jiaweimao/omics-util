package omics.util.protein.digest.suffix;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 9:02 AM
 */
public abstract class AbstractSuffixEvent implements SuffixEvent
{
    private final int index;
    private final int lcp;

    public AbstractSuffixEvent(int index, int lcp)
    {
        this.index = index;
        this.lcp = lcp;
    }

    @Override
    public int getStartIndex()
    {
        return index;
    }

    @Override
    public int getLcp()
    {
        return lcp;
    }
}
