package omics.util.protein.digest.suffix;

import it.unimi.dsi.fastutil.booleans.BooleanArrayList;
import it.unimi.dsi.fastutil.booleans.BooleanList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 10:30 AM
 */
public class CleavageEvent extends AbstractSuffixEvent
{
    private final IntList siteList;
    private final BooleanList isCleavableList;

    public CleavageEvent(int index, int lcp)
    {
        super(index, lcp);
        this.siteList = new IntArrayList();
        this.isCleavableList = new BooleanArrayList();
    }

    public void addEndIndex(int index)
    {
        addEndIndex(index, false);
    }

    /**
     * add a new index
     *
     * @param index       index after the next amino acid
     * @param isCleavable true if the site is cleavable
     */
    public void addEndIndex(int index, boolean isCleavable)
    {
        this.siteList.add(index);
        this.isCleavableList.add(isCleavable);
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.CLEAVAGE;
    }

    public int size()
    {
        return siteList.size();
    }

    public IntList getEndIndexList()
    {
        return siteList;
    }

    public int getEndIndex(int index)
    {
        return siteList.getInt(index);
    }

    /**
     * Return true if the peptide is cleavable at given index, it is the index after the next AA, the actual
     * cleavage site is before the next AA.
     */
    public boolean isCleavable(int idx)
    {
        return isCleavableList.getBoolean(idx);
    }
}
