package omics.util.protein.digest.suffix;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.digest.Protease;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Semi specific in not supported in this mode.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 26 Nov 2019, 8:47 PM
 */
class DualEnzymePeptideIterator extends PeptideIterator
{
    private final IntList siteList1;
    private final IntList siteList2;

    private final int nrMaxMiss1;
    private final int nrMaxMiss2;

    private final Enzyme enzyme1;
    private final Enzyme enzyme2;

    private final int minLen;
    private final int maxLen;

    private final boolean addFirstEnzymePeptide;
    private final boolean limitMissEnz2inEnz1;

    /**
     * Constructor
     *
     * @param protease              the first {@link Protease}
     * @param protease2             the second {@link Protease}
     * @param fastaFile             fasta file
     * @param minPeptideLength      minimum peptide length allowed
     * @param maxPeptideLength      maximum peptide length allowed
     * @param removeProteinNTermM   true if remove protein N-term Methionine residue
     * @param addFirstEnzymePeptide true if add the peptides digested by the first enzyme
     * @param limitMissEnz2inEnz1   true if limit the maximum number of missed cleavages of the second enzyme in the
     *                              first enzyme digests, work only <code>addFirstEnzymePeptide</code> is true.
     */
    DualEnzymePeptideIterator(Protease protease, Protease protease2, Path fastaFile, int minPeptideLength,
            int maxPeptideLength, boolean removeProteinNTermM, boolean addFirstEnzymePeptide, boolean limitMissEnz2inEnz1) throws IOException
    {
        super(protease, fastaFile, minPeptideLength, maxPeptideLength, removeProteinNTermM);

        siteList1 = new IntArrayList();
        siteList2 = new IntArrayList();

        nrMaxMiss1 = protease.getMaxMissedCleavage();
        nrMaxMiss2 = protease2.getMaxMissedCleavage();

        enzyme1 = protease.getEnzyme();
        enzyme2 = protease2.getEnzyme();

        minLen = minPeptideLength + 2;
        maxLen = maxPeptideLength + 2;
        this.addFirstEnzymePeptide = addFirstEnzymePeptide;
        this.limitMissEnz2inEnz1 = limitMissEnz2inEnz1;
    }

    private final IntSet cacheSet = new IntOpenHashSet();

    @Override
    protected void parseNext() throws IOException
    {
        while (currentIndex < N) {
            int index = inputStream.readInt();
            byte lcp = inputStream.readByte();
            currentIndex++;
            updateProgress(currentIndex, N);

            if (index + minLen > N) {
                cacheQueue.addLast(new ShortSequenceEvent(index, lcp, N - index)); // end of sequence
                continue;
            }

            if (removeProteinNTermM && fastaSequence.isLeadingMet(index + 1)) {
                cacheQueue.addLast(new StartProteinEvent(index, lcp));
                continue;
            }

            int endIndex = Math.min(index + maxLen, N); // end of database
            String pepSeq = fastaSequence.subStringEndTerm(index, endIndex);
//            boolean found = false;
//            if (index == 0)
//                found = true;

            if (pepSeq.length() < minLen || pepSeq.length() <= lcp) { // too short valid sequence, end of protein
                cacheQueue.addLast(new ShortSequenceEvent(index, lcp, pepSeq.length()));
                continue;
            }

            boolean foundFirst = false;
            boolean foundLast = false;
            if (fastaSequence.isTerminator(index)
                    || (removeProteinNTermM && fastaSequence.isLeadingMet(index)))
                foundFirst = true;

            if (fastaSequence.isTerminator(index + pepSeq.length() - 1))
                foundLast = true;

//            if (found) {
//                System.out.println(foundFirst + "\t" + foundLast);
//            }

            siteList1.clear();
            siteList2.clear();
            enzyme1.find(pepSeq, siteList1);
            enzyme2.find(pepSeq, siteList2);

//            if (found) {
//                System.out.println(siteList1);
//                System.out.println(siteList2);
//            }

            // the first should be cleavage site
            if (!foundFirst && (siteList1.isEmpty() || siteList1.getInt(0) != 1)
                    && (siteList2.isEmpty() || siteList2.getInt(0) != 1)) {
                cacheQueue.addLast(new NoCleavageEvent(index, lcp));
                continue;
            }

            if (!foundLast
                    && (siteList1.isEmpty() || (siteList1.size() == 1 && siteList1.getInt(0) == 1))
                    && (siteList2.isEmpty() || (siteList2.size() == 1 && siteList2.getInt(0) == 1))) {
                cacheQueue.addLast(new NotEnoughSitesEvent(index, lcp, siteList1.size() + siteList2.size()));
                break;
            }

            CleavageEvent event = new CleavageEvent(index, lcp);
            boolean addLast = false;
            // suitable missed cleavages for enzyme1
            int end1 = pepSeq.length();
            if (!siteList1.isEmpty()) {
                if (addFirstEnzymePeptide) {
                    int maxLen = pepSeq.length();
                    if (limitMissEnz2inEnz1 && !siteList2.isEmpty()) {
                        if (siteList2.getInt(0) == 1) {
                            if (siteList2.size() > (nrMaxMiss2 + 1)) {
                                maxLen = siteList2.getInt(nrMaxMiss2 + 1) + 1;
                            }
                        } else {
                            if (siteList2.size() > nrMaxMiss2) {
                                maxLen = siteList2.getInt(nrMaxMiss2) + 1;
                            }
                        }
                    }

                    if (siteList1.getInt(0) == 1) {
                        if (siteList1.size() > 1) {
                            for (int i = 1; i <= Math.min(siteList1.size() - 1, nrMaxMiss1 + 1); i++) {
                                int len = siteList1.getInt(i) + 1;
                                if (len > maxLen)
                                    break;
                                if (len <= lcp)
                                    continue;

                                if (len >= minLen) {
                                    event.addEndIndex(index + len, true);
                                }
                            }
                        }
                        if (foundLast && maxLen == pepSeq.length() && (siteList1.size() < nrMaxMiss1 + 2)
                                && (siteList1.getInt(siteList1.size() - 1) != pepSeq.length() - 1)) {
                            event.addEndIndex(index + pepSeq.length(), true);
                            addLast = true;
                        }
                    } else {
                        int endId = Math.min(siteList1.size() - 1, nrMaxMiss1);
                        for (int i = 0; i <= endId; i++) {
                            int len = siteList1.getInt(i) + 1;
                            if (len > maxLen)
                                break;
                            if (len <= lcp)
                                continue;
                            if (len >= minLen) {
                                event.addEndIndex(index + len, true);
                            }
                        }
                        if (foundLast && maxLen == pepSeq.length() && (siteList1.size() <= nrMaxMiss1)
                                && (siteList1.getInt(siteList1.size() - 1) != pepSeq.length() - 1)) {
                            event.addEndIndex(index + pepSeq.length(), true);
                            addLast = true;
                        }
                    }

                }

                if (siteList1.getInt(0) == 1) {
                    if (siteList1.size() >= nrMaxMiss1 + 2) {
                        end1 = siteList1.getInt(nrMaxMiss1 + 1) + 1;
                    }
                } else { // the first site is not enzyme1 cleavable
                    if (siteList1.size() >= nrMaxMiss1 + 1) {
                        end1 = siteList1.getInt(nrMaxMiss1) + 1;
                    }
                }
            } else if (foundFirst && foundLast) {
                event.addEndIndex(index + pepSeq.length(), true);
                addLast = true;
            }

            cacheSet.clear();
            cacheSet.addAll(siteList1);
//            if (addFirstEnzymePeptide)
//                siteList2.removeAll(siteList1);

            if (!siteList2.isEmpty()) {
                if (siteList2.getInt(0) == 1) {
                    if (siteList2.size() > 1) {
                        int endId = Math.min(siteList2.size() - 1, nrMaxMiss2 + 1);
                        for (int i = 1; i <= endId; i++) {
                            int id = siteList2.getInt(i);
                            if (cacheSet.contains(id))
                                continue;
                            int len = id + 1;
                            if (len > end1)
                                break;
                            if (len > lcp && len >= minLen) {
                                event.addEndIndex(index + len, true);
                            }
                        }
                    }
                    if (foundLast && !addLast && end1 == pepSeq.length() && (siteList2.size() < nrMaxMiss2 + 2)
                            && (siteList2.getInt(siteList2.size() - 1) != pepSeq.length() - 1)) {
                        event.addEndIndex(index + pepSeq.length(), true);
                    }
                } else { // the first site is not enzyme2 cleavable
                    int endId = Math.min(siteList2.size() - 1, nrMaxMiss2);
                    for (int i = 0; i <= endId; i++) {
                        int id = siteList2.getInt(i);
                        if (cacheSet.contains(id))
                            continue;
                        int len = id + 1;
                        if (len > end1)
                            break;
                        if (len > lcp && len >= minLen) {
                            event.addEndIndex(index + len, true);
                        }
                    }
                    if (foundLast && !addLast && end1 == pepSeq.length() && (siteList2.size() <= nrMaxMiss2)
                            && (siteList2.getInt(siteList2.size() - 1) != pepSeq.length() - 1)) {
                        event.addEndIndex(index + pepSeq.length(), true);
                    }
                }
            }
            cacheQueue.addLast(event);
            break;
        }
    }
}
