package omics.util.protein.digest.suffix;

/**
 * Event occur when only setting remove protein N-term Methionine.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 9:37 AM
 */
public class LeadingMetEvent extends AbstractSuffixEvent
{
    public LeadingMetEvent(int index, int lcp)
    {
        super(index, lcp);
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.LEADING_MET;
    }
}
