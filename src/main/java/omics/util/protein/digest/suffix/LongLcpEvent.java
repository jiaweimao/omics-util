package omics.util.protein.digest.suffix;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 8:52 AM
 */
public class LongLcpEvent extends AbstractSuffixEvent
{
    private int limit;

    public LongLcpEvent(int index, int lcp, int limit)
    {
        super(index, lcp);
        this.limit = limit;
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.LONG_LCP;
    }

    /**
     * @return the limit which lcp exceed.
     */
    public int getLimit()
    {
        return limit;
    }
}
