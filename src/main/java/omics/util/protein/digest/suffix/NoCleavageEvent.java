package omics.util.protein.digest.suffix;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 10:25 AM
 */
public class NoCleavageEvent extends AbstractSuffixEvent
{
    public NoCleavageEvent(int index, int lcp)
    {
        super(index, lcp);
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.NO_CLEAVAGE;
    }
}
