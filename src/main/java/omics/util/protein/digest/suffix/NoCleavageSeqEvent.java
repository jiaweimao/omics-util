package omics.util.protein.digest.suffix;

import it.unimi.dsi.fastutil.booleans.BooleanArrayList;
import it.unimi.dsi.fastutil.booleans.BooleanList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

/**
 * For this event, the preAA of the peptide is not enzymatic cleavable.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 1:16 PM
 */
public class NoCleavageSeqEvent extends AbstractSuffixEvent
{
    private IntList endList;
    private BooleanList isCleavableList;

    public NoCleavageSeqEvent(int index, int lcp)
    {
        super(index, lcp);
        endList = new IntArrayList();
        isCleavableList = new BooleanArrayList();
    }

    public void addEndIndex(int index)
    {
        addEndIndex(index, false);
    }

    public void addEndIndex(int index, boolean isCleavable)
    {
        this.endList.add(index);
        this.isCleavableList.add(isCleavable);
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.NO_CLEAVAGE_SEQ;
    }

    public int size()
    {
        return endList.size();
    }

    public int getEndIndex(int index)
    {
        return endList.getInt(index);
    }

    public boolean isCleavable(int index)
    {
        return isCleavableList.getBoolean(index);
    }
}
