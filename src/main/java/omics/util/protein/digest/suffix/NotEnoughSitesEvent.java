package omics.util.protein.digest.suffix;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 10:20 AM
 */
public class NotEnoughSitesEvent extends AbstractSuffixEvent
{
    private int siteCount;

    public NotEnoughSitesEvent(int index, int lcp, int siteCount)
    {
        super(index, lcp);
        this.siteCount = siteCount;
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.NOT_ENOUGH_SITE;
    }

    public int getSiteCount()
    {
        return siteCount;
    }
}
