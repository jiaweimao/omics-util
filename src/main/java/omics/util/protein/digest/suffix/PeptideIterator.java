package omics.util.protein.digest.suffix;

import omics.util.InfoTask;
import omics.util.protein.database.FastaSequence;
import omics.util.protein.database.SuffixArraySequence;
import omics.util.protein.digest.Protease;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Generate peptides from fasta file according given enzyme.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 15 Jun 2019, 8:58 AM
 */
public abstract class PeptideIterator extends InfoTask<Void> implements Iterator<SuffixEvent>, Closeable
{
    public static PeptideIterator getIterator(Protease protease, String fastaFile, int minLength, int maxLength,
            boolean removeLeadingM) throws IOException
    {
        if (protease.isUnspecific()) {
            return new UnspecificIterator(protease, Paths.get(fastaFile), minLength, maxLength, removeLeadingM);

        } else {
            return new SpecificIterator(protease, Paths.get(fastaFile), minLength, maxLength, removeLeadingM);
        }
    }

    /**
     * Return a {@link PeptideIterator} of dual enzymes, semi-specific is not supported in this iterator.
     *
     * @param protease1      a {@link Protease}
     * @param protease2      a {@link Protease}
     * @param fastaFile      fasta file path
     * @param minLength      minimum peptide length
     * @param maxLength      maximum peptide length
     * @param removeLeadingM true if remove protein leading Methionine
     * @return a {@link PeptideIterator}
     */
    public static PeptideIterator getIterator(Protease protease1, Protease protease2, String fastaFile, int minLength, int maxLength,
            boolean removeLeadingM) throws IOException
    {
        return new DualEnzymePeptideIterator(protease1, protease2, Paths.get(fastaFile), minLength, maxLength, removeLeadingM, true, true);
    }

    /**
     * Return a {@link PeptideIterator} of dual enzymes, semi-specific is not supported.
     *
     * @param protease1             a {@link Protease}
     * @param protease2             a {@link Protease}
     * @param fastaFile             fasta file path
     * @param minLength             minimum peptide length
     * @param maxLength             minimum peptide length
     * @param removeLeadingM        true if remove protein leading Methionine
     * @param addFirstEnzymePeptide true if add the first enzymatic peptides.
     * @param limitMissEnz2inEnz1   true if limit the maximum number of missed cleavages of the second enzyme in the
     *                              first enzyme digests, work only <code>addFirstEnzymePeptide</code> is true.
     * @return a {@link PeptideIterator}
     */
    public static PeptideIterator getIterator(Protease protease1, Protease protease2, String fastaFile, int minLength, int maxLength,
            boolean removeLeadingM, boolean addFirstEnzymePeptide, boolean limitMissEnz2inEnz1) throws IOException
    {
        return new DualEnzymePeptideIterator(protease1, protease2, Paths.get(fastaFile), minLength, maxLength, removeLeadingM, addFirstEnzymePeptide, limitMissEnz2inEnz1);
    }

    final DataInputStream inputStream;
    final FastaSequence fastaSequence;
    final boolean removeProteinNTermM;
    final int minPeptideLength;
    final int maxPeptideLength;
    int currentIndex = 0;
    final int N;
    final Deque<SuffixEvent> cacheQueue;


    /**
     * Constructor.
     *
     * @param protease            {@link Protease} used to cleavage protein
     * @param fastaFile           fasta file
     * @param minPeptideLength    min peptide length
     * @param maxPeptideLength    max peptide length
     * @param removeProteinNTermM set true if remove the protein N-term Methionine residue before generating peptides.
     */
    PeptideIterator(Protease protease, Path fastaFile, int minPeptideLength, int maxPeptideLength, boolean removeProteinNTermM) throws IOException
    {
        checkNotNull(protease);
        checkNotNull(fastaFile);
        checkArgument(Files.exists(fastaFile), "The fasta file does not exist: " + fastaFile);

        this.removeProteinNTermM = removeProteinNTermM;
        this.minPeptideLength = minPeptideLength;
        this.maxPeptideLength = maxPeptideLength;

        //    private SuffixIterator si;
        SuffixArraySequence suffixArraySequence = new SuffixArraySequence(fastaFile.toString());
        this.N = suffixArraySequence.getSize();
        this.fastaSequence = suffixArraySequence.getSequence();
        this.cacheQueue = new ArrayDeque<>(this.maxPeptideLength);
        this.inputStream = new DataInputStream(new BufferedInputStream(Files.newInputStream(suffixArraySequence.getSuffixArrayFile())));
        inputStream.skip((SuffixArraySequence.MAX_LEN + 2) * Integer.BYTES);
    }

    protected abstract void parseNext() throws IOException;

    /**
     * Return peptide sequence in given
     *
     * @param start start index, inclusive
     * @param end   end index, exclusive
     * @return peptide quence in the range
     */
    public String getPeptide(int start, int end)
    {
        return fastaSequence.subString(start, end);
    }

    @Override
    public boolean hasNext()
    {
        if (cacheQueue.isEmpty()) {
            try {
                parseNext();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return !cacheQueue.isEmpty();
    }

    /**
     * peptide range array with length 4:
     * 0, start index;
     * 1, end index;
     * 2, number of missed cleavages, for unspecific, it is always 0
     * 3, lcp with the previous suffix
     */
    @Override
    public SuffixEvent next()
    {
        return cacheQueue.pollFirst();
    }

    @Override
    public void close() throws IOException
    {
        inputStream.close();
    }
}
