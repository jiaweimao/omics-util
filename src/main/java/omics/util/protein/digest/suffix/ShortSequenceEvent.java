package omics.util.protein.digest.suffix;

/**
 * Event for too short peptide sequence
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 10:13 AM
 */
public class ShortSequenceEvent extends AbstractSuffixEvent
{
    private final int length;

    /**
     * Constructor.
     *
     * @param index  index in the concanated protein sequence
     * @param lcp    length of lcp
     * @param length peptide length
     */
    public ShortSequenceEvent(int index, int lcp, int length)
    {
        super(index, lcp);
        this.length = length;
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.SHORT_SEQ;
    }


    public int getLength()
    {
        return length;
    }
}
