package omics.util.protein.digest.suffix;

import it.unimi.dsi.fastutil.ints.IntAVLTreeSet;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntSortedSet;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.digest.Protease;

import java.io.IOException;
import java.nio.file.Path;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 8:07 AM
 */
class SpecificIterator extends PeptideIterator
{
    private boolean needUpdate = false;
    private final IntList siteList;
    private final IntSortedSet preSiteList;
    private final IntList shiftList;
    private final int nrMaxMiss;
    private final boolean isSemispecifc;
    private boolean foundLeadingM = false;
    private final Enzyme enzyme;
    private final int minLen;
    private final int maxLen;

    /**
     * Constructor.
     *
     * @param protease            {@link Protease} used to cleavage protein
     * @param fastaFile           fasta file
     * @param minPeptideLength    min peptide length
     * @param maxPeptideLength    max peptide length
     * @param removeProteinNTermM set true if remove the protein N-term Methionine residue before generating peptides.
     */
    SpecificIterator(Protease protease, Path fastaFile, int minPeptideLength, int maxPeptideLength, boolean removeProteinNTermM) throws IOException
    {
        super(protease, fastaFile, minPeptideLength, maxPeptideLength, removeProteinNTermM);
        siteList = new IntArrayList(this.maxPeptideLength);
        preSiteList = new IntAVLTreeSet();
        shiftList = new IntArrayList(this.maxPeptideLength);
        nrMaxMiss = protease.getMaxMissedCleavage();
        enzyme = protease.getEnzyme();
        isSemispecifc = protease.isSemi();
        minLen = minPeptideLength + 2;
        maxLen = maxPeptideLength + 2;
    }

    private void addSemiSameLeading(int index, int lcp)
    {
        int endIndex = Math.min(index + maxLen, N);
        String pepSeq = fastaSequence.subStringEndTerm(index, endIndex);

        if (lcp >= pepSeq.length()) {
            cacheQueue.addLast(new ShortSequenceEvent(index, lcp, pepSeq.length()));
            needUpdate = false;
            return;
        }

        // now pep > lcp
        if (pepSeq.length() < minLen) {
            cacheQueue.addLast(new ShortSequenceEvent(index, lcp, pepSeq.length()));
            needUpdate = true;
            return;
        }

        // now pep >= minLen
        IntSortedSet headSet = new IntAVLTreeSet(preSiteList.headSet(lcp));
        // not start cleavage
        if (preSiteList.isEmpty() || preSiteList.firstInt() != 1) {
            if (headSet.size() >= nrMaxMiss + 1) {
                cacheQueue.addLast(new TooManySitesEvent(index, lcp, headSet.size(), nrMaxMiss));
                needUpdate = false;
            } else {
                // start no cleavage
                findShiftSites(index, lcp, pepSeq);
                if (siteList.isEmpty()) {
                    preSiteList.clear();
                    preSiteList.addAll(headSet);
                    needUpdate = false;
                    cacheQueue.addLast(new NotEnoughSitesEvent(index, lcp, 0));
                    return;
                }
                int startId = lcp + 1;
                // in this case, the previous sa has added
                if (preSiteList.contains(lcp)) {
                    startId++;
                }
                if (startId < minLen)
                    startId = minLen;
                int endId = Math.min(siteList.size(), nrMaxMiss - headSet.size() + 1);
                NoCleavageSeqEvent event = new NoCleavageSeqEvent(index, lcp);
                for (int i = 0; i < endId; i++) {
                    int end = siteList.getInt(i) + 1;
                    if (end >= startId)
                        event.addEndIndex(index + end, true);
                }
                cacheQueue.addLast(event);
                needUpdate = false;
                preSiteList.clear();
                preSiteList.addAll(headSet);
                preSiteList.addAll(siteList);
            }
        } else {
            // start 1
            if (headSet.size() > nrMaxMiss + 1) {
                cacheQueue.addLast(new TooManySitesEvent(index, lcp, headSet.size(), nrMaxMiss));
                needUpdate = false;
            } else {
                findShiftSites(index, lcp, pepSeq);
                preSiteList.clear();
                preSiteList.addAll(headSet);
                preSiteList.addAll(siteList);
                needUpdate = false;

                // lcp+1, the last aa is the same
                int startId = Math.max(lcp + 2, minLen);
                int endId;
                if (headSet.size() + siteList.size() < nrMaxMiss + 2) {
                    endId = pepSeq.length();
                } else {
                    // index after the last site
                    endId = siteList.getInt(nrMaxMiss + 1 - headSet.size()) + 1;
                }
                CleavageEvent event = new CleavageEvent(index, lcp);
                for (int i = startId; i <= endId; i++) {
                    if (preSiteList.contains(i - 1)) {
                        event.addEndIndex(index + i, true);
                    } else {
                        event.addEndIndex(index + i);
                    }
                }
                cacheQueue.addLast(event);
            }
        }
    }

    private void addSemiLCPPeps(int index, int lcp)
    {
        if (preSiteList.isEmpty() || preSiteList.firstInt() != 1) {
            if (removeProteinNTermM && fastaSequence.isLeadingMet(index)) {
                foundLeadingM = true;
                String pepSeq = fastaSequence.subStringEndTerm(index, Math.min(index + maxLen, N));
                if (pepSeq.length() < minLen) {
                    preSiteList.add(1);
                    cacheQueue.addLast(new ShortSequenceEvent(index, lcp, pepSeq.length()));
                    needUpdate = lcp < pepSeq.length();
                    return;
                }

                findSites(index, pepSeq);
                needUpdate = false;
                int endId = pepSeq.length();
                if (siteList.size() > nrMaxMiss + 1) {
                    endId = siteList.getInt(nrMaxMiss + 1) + 1;
                }
                if (endId < minLen) {
                    cacheQueue.addLast(new TooManySitesEvent(index, lcp, siteList.size(), nrMaxMiss));
                    return;
                }
                CleavageEvent event = new CleavageEvent(index, lcp);
                for (int i = minLen; i <= endId; i++) {
                    if (i < lcp && preSiteList.contains(i - 1)) {
                        continue;
                    }
                    if (preSiteList.contains(i - 1)) {
                        event.addEndIndex(index + i, true);
                    } else {
                        event.addEndIndex(index + i);
                    }
                }
                cacheQueue.addLast(event);
            } else {
                addSemiSameLeading(index, lcp);
            }
        } else {
            if (foundLeadingM && !fastaSequence.isLeadingMet(index)) {
                foundLeadingM = false;
                String pepSeq = fastaSequence.subStringEndTerm(index, Math.min(index + maxLen, N));
                if (pepSeq.length() < minLen || pepSeq.length() < lcp) {
                    preSiteList.remove(1);
                    cacheQueue.addLast(new ShortSequenceEvent(index, lcp, pepSeq.length()));
                    needUpdate = lcp < pepSeq.length();
                    return;
                }
                findSites(index, pepSeq);
                needUpdate = false;
                NoCleavageSeqEvent event = new NoCleavageSeqEvent(index, lcp);
                int lenLimit = Math.max(minLen, lcp);
                int endId = Math.min(nrMaxMiss, siteList.size() - 1);
                for (int i = 0; i <= endId; i++) {
                    int id = siteList.getInt(i) + 1;
                    if (id >= lenLimit) {
                        event.addEndIndex(index + id, true);
                    }
                }
                cacheQueue.addLast(event);
            } else {
                // same leading 1
                addSemiSameLeading(index, lcp);
            }
        }
    }

    private void addSpecificPeps(int index, int lcp)
    {
        int endIndex = Math.min(index + maxLen, N); // end of database
        String pepSeq = fastaSequence.subStringEndTerm(index, endIndex);
        if (pepSeq.length() < minLen) { // too short valid sequence, end of protein
            needUpdate = true;
            cacheQueue.addLast(new ShortSequenceEvent(index, lcp, pepSeq.length()));
            return;
        }

        findSites(index, pepSeq);

        if (siteList.size() < 2) {
            cacheQueue.addLast(new NotEnoughSitesEvent(index, lcp, siteList.size()));
            needUpdate = false;
            return;
        }
        if (siteList.getInt(0) != 1) {
            cacheQueue.addLast(new NoCleavageEvent(index, lcp));
            needUpdate = false;
            return;
        }

        CleavageEvent event = new CleavageEvent(index, lcp);
        int currentMiss = Math.min(siteList.size() - 1, nrMaxMiss + 1);
        for (int i = currentMiss; i > 0; i--) {
            int id = siteList.getInt(i); // the number of missed cleavage is equal to i-1
            if (id + 1 < minLen)
                break;
            event.addEndIndex(index + id + 1, true);
        }
        cacheQueue.addLast(event);
        needUpdate = false;
    }

    private void addSpecificLCPPeps(int index, int lcp)
    {
        if (preSiteList.isEmpty() || preSiteList.firstInt() != 1) {
            // from non-M to M
            if ((removeProteinNTermM && fastaSequence.isLeadingMet(index))
                    || fastaSequence.isTerminator(index)) {
                addSpecificPeps(index, lcp);
                return;
            } else {
                foundLeadingM = false;
                needUpdate = false;
                cacheQueue.add(new NoCleavageEvent(index, lcp));
                return;
            }
        } else {
            if (foundLeadingM && !fastaSequence.isLeadingMet(index)) {
                foundLeadingM = false;
                needUpdate = true;
                cacheQueue.addLast(new NoCleavageEvent(index, lcp));
                return;
            }

            if (lcp >= maxLen) {
                cacheQueue.addLast(new LongLcpEvent(index, lcp, maxLen));
                needUpdate = false;
                return;
            }
        }

        int endIndex = Math.min(index + maxLen, N);
        String pepSeq = fastaSequence.subStringEndTerm(index, endIndex);
        if (lcp >= pepSeq.length()) {
            cacheQueue.add(new ShortSequenceEvent(index, lcp, pepSeq.length()));
            return;
        }

        IntSortedSet headSet = new IntAVLTreeSet(preSiteList.headSet(lcp));
        if (headSet.size() > (nrMaxMiss + 1)) {
            // too many cleavage sites
            cacheQueue.add(new TooManySitesEvent(index, lcp, headSet.size(), nrMaxMiss));
            return;
        }

        findShiftSites(index, lcp, pepSeq);
        if (siteList.isEmpty()) { // no new sites
            preSiteList.clear();
            preSiteList.addAll(headSet);
            cacheQueue.add(new NotEnoughSitesEvent(index, lcp, 0));
            return;
        }

        int endLen = lcp + 1;
        if (preSiteList.contains(lcp)) {
            endLen++;
        }
        endLen = Math.max(endLen, minLen);
        int currentMiss = (siteList.size() + headSet.size() > nrMaxMiss + 2)
                ? (nrMaxMiss + 2 - headSet.size()) : siteList.size();
        CleavageEvent event = new CleavageEvent(index, lcp);
        for (int i = currentMiss - 1; i >= 0; i--) {
            int id = siteList.getInt(i);
            if (id + 1 < endLen)
                break;

            event.addEndIndex(index + id + 1, true);
        }
        cacheQueue.addLast(event);

        preSiteList.clear();
        preSiteList.addAll(headSet);
        preSiteList.addAll(siteList);
    }

    @Override
    protected void parseNext() throws IOException
    {
        while (currentIndex < N) {
            int index = inputStream.readInt();
            byte lcp = inputStream.readByte();
            currentIndex++;
            updateProgress(currentIndex, N);

            if (index + minLen > N) {
                cacheQueue.addLast(new ShortSequenceEvent(index, lcp, N - index));
                continue;
            }

            if (removeProteinNTermM && fastaSequence.isLeadingMet(index + 1)) {
                cacheQueue.addLast(new StartProteinEvent(index, lcp));
                needUpdate = true;
                continue;
            }

            if (isSemispecifc) {
                if (lcp < 2 || needUpdate) {
                    addSemiPeps(index, lcp);
                } else {
                    addSemiLCPPeps(index, lcp);
                }
            } else {
                if (lcp < 2 || needUpdate) {
                    addSpecificPeps(index, lcp);
                } else {
                    addSpecificLCPPeps(index, lcp);
                }
            }
            if (cacheQueue.isEmpty())
                continue;
            break;
        }
    }

    /**
     * Find cleavages sites in the peptide sequence.
     *
     * @param index start index
     * @param seq   preAA.peptide sequence.nextAA
     */
    private void findSites(int index, String seq)
    {
        siteList.clear();
        if (fastaSequence.isTerminator(index))
            siteList.add(1);
        foundLeadingM = removeProteinNTermM && fastaSequence.isLeadingMet(index);

        if (foundLeadingM)
            siteList.add(1);
        enzyme.find(seq, siteList);
        if (fastaSequence.isTerminator(index + seq.length() - 1)) {
            siteList.add(seq.length() - 1);
        }
        preSiteList.clear();
        preSiteList.addAll(siteList);
    }

    private void findShiftSites(int index, int lcp, String seq)
    {
        int start = lcp - 1;
        String subPep = seq.substring(start);
        shiftList.clear();
        enzyme.find(subPep, shiftList);
        if (fastaSequence.isTerminator(index + seq.length() - 1)) {
            shiftList.add(subPep.length() - 1);
        }
        siteList.clear();
        for (int i = 0; i < shiftList.size(); i++) {
            siteList.add(shiftList.getInt(i) + start);
        }
    }

    private void addSemiPeps(int index, int lcp)
    {
        int endIndex = Math.min(index + maxLen, N); // end of database
        String pepSeq = fastaSequence.subStringEndTerm(index, endIndex);
        if (pepSeq.length() < minLen) { // too short valid sequence, end of protein
            needUpdate = true;
            cacheQueue.addLast(new ShortSequenceEvent(index, lcp, pepSeq.length()));
            return;
        }
        // long enough
        findSites(index, pepSeq);
        needUpdate = false;
        if (siteList.isEmpty()) {
            cacheQueue.addLast(new NotEnoughSitesEvent(index, lcp, siteList.size()));
            return;
        }

        if (siteList.getInt(0) != 1) {
            NoCleavageSeqEvent event = new NoCleavageSeqEvent(index, lcp);
            int endId = Math.min(siteList.size() - 1, nrMaxMiss);
            for (int id = endId; id >= 0; id--) {
                int len = siteList.getInt(id) + 1;
                if (len < minLen)
                    break;
                event.addEndIndex(index + len, true);
            }
            cacheQueue.addLast(event);
            return;
        }

        // the first is 1
        int end = (siteList.size() <= (nrMaxMiss + 1))
                ? pepSeq.length() : siteList.getInt(nrMaxMiss + 1) + 1; // plus one, the last site is the end of the peptide
        CleavageEvent event = new CleavageEvent(index, lcp);
        for (int id = minLen; id <= end; id++) {
            if (preSiteList.contains(id - 1)) {
                event.addEndIndex(index + id, true);
            } else {
                event.addEndIndex(index + id);
            }
        }
        cacheQueue.addLast(event);
    }
}
