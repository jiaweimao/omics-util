package omics.util.protein.digest.suffix;

/**
 * Event occur in the start of protein
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 10:10 AM
 */
public class StartProteinEvent extends AbstractSuffixEvent
{
    public StartProteinEvent(int index, int lcp)
    {
        super(index, lcp);
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.START_PROTEIN;
    }
}
