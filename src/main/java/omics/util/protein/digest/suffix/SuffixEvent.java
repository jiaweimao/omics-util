package omics.util.protein.digest.suffix;

import static omics.util.protein.digest.suffix.SuffixEventType.*;

/**
 * Event generated during suffix generation.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 8:38 AM
 */
public interface SuffixEvent
{
    /**
     * @return the {@link SuffixEventType}
     */
    SuffixEventType getEventType();

    /**
     * @return index of the suffix
     */
    int getStartIndex();

    /**
     * @return lcp of the suffix
     */
    int getLcp();

    default LongLcpEvent asLongLcpEvent()
    {
        if (getEventType() == LONG_LCP)
            return (LongLcpEvent) this;
        throw new ClassCastException("Not " + LONG_LCP + " but " + getEventType());
    }

    /**
     * @return true if this event is a {@link SuffixEventType#CLEAVAGE} event.
     */
    default boolean isCleavageEvent()
    {
        return getEventType() == CLEAVAGE;
    }

    default boolean isNoCleavageSeqEvent()
    {
        return getEventType() == NO_CLEAVAGE_SEQ;
    }

    default CleavageEvent asCleavageEvent()
    {
        if (getEventType() == SuffixEventType.CLEAVAGE) {
            return (CleavageEvent) this;
        }
        throw new ClassCastException("Not " + CLEAVAGE + " but " + getEventType());
    }

    default NoCleavageSeqEvent asNoCleavageSeqEvent()
    {
        if (isNoCleavageSeqEvent()) {
            return (NoCleavageSeqEvent) this;
        }
        throw new ClassCastException("Not " + NO_CLEAVAGE_SEQ + " but " + getEventType());
    }
}
