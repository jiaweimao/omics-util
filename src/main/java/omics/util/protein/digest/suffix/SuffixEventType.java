package omics.util.protein.digest.suffix;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 8:39 AM
 */
public enum SuffixEventType
{
    LONG_LCP,
    START_PROTEIN,
    LEADING_MET,
    SHORT_SEQ,
    NOT_ENOUGH_SITE,
    TOO_MANY_SITE,
    CLEAVAGE,
    NO_CLEAVAGE,
    NO_CLEAVAGE_SEQ
}
