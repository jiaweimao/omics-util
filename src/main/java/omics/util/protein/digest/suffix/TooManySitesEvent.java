package omics.util.protein.digest.suffix;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 12:52 PM
 */
public class TooManySitesEvent extends AbstractSuffixEvent
{
    private int site;
    private int siteLimit;

    public TooManySitesEvent(int index, int lcp, int site, int siteLimit)
    {
        super(index, lcp);
        this.site = site;
        this.siteLimit = siteLimit;
    }

    @Override
    public SuffixEventType getEventType()
    {
        return SuffixEventType.TOO_MANY_SITE;
    }

    public int getSite()
    {
        return site;
    }

    public int getSiteLimit()
    {
        return siteLimit;
    }
}
