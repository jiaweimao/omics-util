package omics.util.protein.digest.suffix;

import omics.util.protein.digest.Protease;

import java.io.IOException;
import java.nio.file.Path;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 8:07 AM
 */
class UnspecificIterator extends PeptideIterator
{
    private final int[] lengthHis;

    UnspecificIterator(Protease protease, Path fastaFile, int minPeptideLength, int maxPeptideLength, boolean removeProteinNTermM) throws IOException
    {
        super(protease, fastaFile, minPeptideLength, maxPeptideLength, removeProteinNTermM);
        this.lengthHis = new int[maxPeptideLength + 1];
    }

    @Override
    protected void parseNext() throws IOException
    {
        while (currentIndex < N) {
            int index = inputStream.readInt();
            byte lcp = inputStream.readByte();
            currentIndex++;
            updateProgress(currentIndex, N);

            if (lcp >= maxPeptideLength) {
                cacheQueue.addLast(new LongLcpEvent(index, lcp, maxPeptideLength));
                break;
            }

            if (fastaSequence.isTerminator(index)) {
                cacheQueue.addLast(new StartProteinEvent(index, lcp));
                continue;
            }

            if (removeProteinNTermM && fastaSequence.isLeadingMet(index)) {
                cacheQueue.addLast(new LeadingMetEvent(index, lcp));
                continue;
            }

            int endIndex = Math.min(index + maxPeptideLength, N);
            int lenLower = minPeptideLength > lcp ? minPeptideLength : lcp + 1;
            CleavageEvent event = new CleavageEvent(index - 1, lcp);
            for (int i = index + 1; i <= endIndex; i++) {
                int len = i - index;
                if (len >= lenLower) {
                    event.addEndIndex(i + 1);
                    lengthHis[len]++;
                }
                if (fastaSequence.isTerminator(i) || fastaSequence.isInvalid(i)) {
                    break;
                }
            }

            cacheQueue.addLast(event);
            break;
        }
    }


    /**
     * count of sequence of this length
     *
     * @param length peptide length
     * @return number of unique sequence of this length, enzyme cleavage is not taken into account.
     */
    public int getCount(int length)
    {
        return lengthHis[length];
    }
}
