package omics.util.protein.digest.util;


import omics.util.protein.Peptide;
import omics.util.protein.Protein;
import omics.util.protein.digest.DigestionController;


/**
 * DigestionController that accepts all peptides and does not interrupt the digestion.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Mar 2017, 8:23 AM
 */
public class AcceptAllDigestionController implements DigestionController {
    /**
     * {@inheritDoc}
     * <p>
     * Digestion never interrupted.
     */
    @Override
    public boolean interruptDigestion(Protein protein, int proteinCleavageSiteCount, int inclusiveLowerBoundIndex,
            int inclusiveUpperBoundIndex) {
        return false;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Make all digest peptides.
     */
    @Override
    public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex,
            int missedCleavagesCount) {
        return true;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Retain all digests.
     */
    @Override
    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {
        return true;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Retain all semi digests.
     */
    @Override
    public boolean retainSemiDigest(Peptide digest) {
        return true;
    }
}
