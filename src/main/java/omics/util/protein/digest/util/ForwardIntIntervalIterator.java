package omics.util.protein.digest.util;


/**
 * Generates all possible intervals of integers in the closed interval [lower, upper] from
 * [lower, lower+1] to [upper-1, upper]
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Mar 2017, 8:33 AM
 */
public class ForwardIntIntervalIterator extends IntIntervalIterator {

    /**
     * Constructor
     *
     * @param lower the lower bound
     * @param upper the upper bound
     */
    public ForwardIntIntervalIterator(int lower, int upper) {
        super(lower, upper);
    }

    public ForwardIntIntervalIterator(int lower, int upper, int rangeMax) {
        super(lower, upper, rangeMax);
    }

    protected void init() {
        i = lower;
        j = lower;
    }

    protected void nextIteration(int rangeMax) {
        j++;
        if (isStopJ(rangeMax)) {
            i++;
            j = i + 1;

            // last iteration
            if (i == upper)
                hasNext = false;
        }
    }

    /**
     * to judge if the max value reached.
     *
     * @param rangeMax the max range
     * @return true if no more value available
     */
    private boolean isStopJ(int rangeMax) {
        return j == upper + 1 || j - i > rangeMax;
    }
}
