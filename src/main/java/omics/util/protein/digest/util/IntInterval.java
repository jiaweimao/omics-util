package omics.util.protein.digest.util;

import java.util.Objects;

/**
 * This class define an index range.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Mar 2017, 8:29 AM
 */
public class IntInterval {

    private int lowerBound;
    private int upperBound;

    void reset() {

        this.lowerBound = -1;
        this.upperBound = -1;
    }

    /**
     * @return current lower bound index of the interval.
     */
    public int getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    /**
     * @return current upper bound index of the interval.
     */
    public int getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    /**
     * @return the interval range.
     */
    public int getCurrentRange() {

        return upperBound - lowerBound;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntInterval that = (IntInterval) o;
        return lowerBound == that.lowerBound &&
                upperBound == that.upperBound;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lowerBound, upperBound);
    }

    @Override
    public String toString() {
        return "IntIntervalCursor{" + "currentLowerBound=" + lowerBound + ", currentUpperBound="
                + upperBound + '}';
    }
}
