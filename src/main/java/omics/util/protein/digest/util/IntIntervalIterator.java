package omics.util.protein.digest.util;

import java.util.Iterator;

import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * Generates all possible intervals of integers in the closed interval [lower,upper]
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Mar 2017, 8:29 AM
 */
public abstract class IntIntervalIterator implements Iterator<IntInterval> {

    /**
     * the lower and upper value.
     */
    protected final int lower, upper;
    /**
     * current interval cursor.
     */
    private final IntInterval intInterval;
    /**
     * the max range value.
     */
    private final int rangeMax;
    /**
     * current lower and upper bounds.
     */
    protected int i, j;
    /**
     * true if more iterator is available
     */
    protected boolean hasNext;
    private boolean consumed;

    protected IntIntervalIterator(int lower, int upper) {

        this(lower, upper, upper - lower);
    }

    protected IntIntervalIterator(int lower, int upper, int rangeMax) {
        checkArgument(lower < upper);
        checkArgument(rangeMax > 0);

        this.lower = lower;
        this.upper = upper;
        this.rangeMax = Math.min(upper - lower, rangeMax);

        intInterval = new IntInterval();
        intInterval.reset();

        hasNext = true;
        consumed = false;

        init();
    }

    /**
     * initialize the interval cursor index.
     */
    protected abstract void init();

    protected abstract void nextIteration(int rangeMax);

    @Override
    public boolean hasNext() {
        // not yet consumed
        if (!consumed)
            nextValidIteration();
        return hasNext;
    }

    private void nextValidIteration() {
        nextIteration(rangeMax);
        consumed = true;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Or return (-1,-1) if end reached.
     */
    @Override
    public IntInterval next() {
        if (!consumed)
            nextValidIteration();

        if (hasNext) {
            intInterval.setLowerBound(i);
            intInterval.setUpperBound(j);
        } else {
            intInterval.setLowerBound(-1);
            intInterval.setUpperBound(-1);
        }

        consumed = false;

        return intInterval;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("");
    }
}
