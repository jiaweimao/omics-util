package omics.util.protein.digest.util;

import omics.util.protein.Peptide;
import omics.util.protein.Protein;
import omics.util.protein.digest.DigestionController;


/**
 * DigestionController that excludes peptides that fall outside the length range.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Jun 2019, 5:50 PM
 */
public class LengthDigestionController implements DigestionController {

    private final int min;
    private final int max;

    /**
     * Create a DigestionController that excludes peptides that have a length &lt
     * <code>min</code> and &gt <code>max</code>
     *
     * @param min the minimum peptide length, peptides with a length smaller than this are excluded
     * @param max the maximum peptide length, peptides with a length larger than this are excluded
     */
    public LengthDigestionController(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean interruptDigestion(Protein protein, int proteinBoundsCount, int inclusiveLowerBoundIndex,
            int inclusiveUpperBoundIndex) {
        return false;
    }

    @Override
    public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex,
            int missedCleavagesCount) {
        int size = exclusiveProteinUpperIndex - inclusiveProteinLowerIndex;
        return size >= min && size <= max;
    }

    @Override
    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {
        return true;
    }

    @Override
    public boolean retainSemiDigest(Peptide digest) {
        int size = digest.size();
        return size >= min && size <= max;
    }
}
