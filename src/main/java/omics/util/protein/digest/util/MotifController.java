package omics.util.protein.digest.util;

import omics.util.protein.Peptide;
import omics.util.protein.Protein;
import omics.util.protein.digest.DigestionController;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * To test if the peptide contains given motif.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 03 May 2019, 10:47 AM
 */
public class MotifController implements DigestionController {

    private final Pattern pattern;

    public MotifController(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean interruptDigestion(Protein protein, int proteinBoundsCount, int inclusiveLowerBoundIndex, int inclusiveUpperBoundIndex) {
        return false;
    }

    @Override
    public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex, int missedCleavagesCount) {
        return true;
    }

    @Override
    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex) {
        String s = digest.toSymbolString();
        Matcher matcher = pattern.matcher(s);
        return matcher.find();
    }

    @Override
    public boolean retainSemiDigest(Peptide digest) {
        return true;
    }
}
