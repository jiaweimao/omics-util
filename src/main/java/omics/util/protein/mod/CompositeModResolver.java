package omics.util.protein.mod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


/**
 * Composite Modification Resolver, contains a list of Resolvers.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Mar 2017, 7:20 PM
 */
public class CompositeModResolver implements ModificationResolver {

    private final List<ModificationResolver> resolvers;

    public CompositeModResolver(ModificationResolver first, ModificationResolver... rest) {
        resolvers = new ArrayList<>(rest.length + 1);
        resolvers.add(first);
        Collections.addAll(resolvers, rest);
    }

    @Override
    public Modification resolve(String input) {
        for (ModificationResolver resolver : resolvers) {
            Modification mod = resolver.resolve(input);
            if (mod != null)
                return mod;
        }

        return null;
    }
}
