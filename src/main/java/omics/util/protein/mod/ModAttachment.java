package omics.util.protein.mod;

import java.util.EnumSet;
import java.util.Set;

/**
 * Specify the position of modification
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Mar 2017, 10:52 AM
 */
public enum ModAttachment {

    SIDE_CHAIN, N_TERM, C_TERM;

    public static final Set<ModAttachment> all = EnumSet.allOf(ModAttachment.class);
    public static final Set<ModAttachment> nTermSet = EnumSet.of(N_TERM);
    public static final Set<ModAttachment> cTermSet = EnumSet.of(C_TERM);
    public static final Set<ModAttachment> sideChainSet = EnumSet.of(SIDE_CHAIN);
    /**
     * the C_term and N-term mod
     */
    public static final Set<ModAttachment> termSet = EnumSet.of(N_TERM, C_TERM);
}
