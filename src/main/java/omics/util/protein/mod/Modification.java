package omics.util.protein.mod;

import omics.util.chem.Composition;
import omics.util.chem.Mass;
import omics.util.chem.Weighable;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * A mass object with title.
 *
 * @author JiaweiMao
 * @version 1.3.0
 * @since 16 Mar 2017, 11:58 AM
 */
public class Modification implements Weighable
{
    /**
     * regex explain:
     * [label:][formula][charge]
     * [formula]: [atom]...
     * [atom]: [A-Z][a-z]?\[\d+\]?
     * [charge]： (\d*[+-])
     */
    private static final Pattern parsePattern = Pattern
            .compile("(?:(.*):)?((?:[A-Z][a-z]?(?:\\[\\d+\\])?[+-]?\\d*)+(?:\\(\\d*[+-]\\))?)");

    protected final String title;
    protected final Mass mass;

    /**
     * constructor.
     *
     * @param title modification title.
     * @param mass  Modification delta mass.
     */
    public Modification(String title, Mass mass)
    {
        checkNotNull(title);
        checkNotNull(mass);

        this.title = title;
        this.mass = mass;
    }

    /**
     * Parse a modification. The format is (title:)?formula. An optional title
     * followed by a formula delimited by :
     *
     * @param s the string to parse
     * @return the parsed Modification
     * @throws IllegalArgumentException - if s is not a valid modification
     */
    public static Modification parseModification(String s)
    {
        Matcher matcher = parsePattern.matcher(s);

        if (!matcher.matches())
            throw new IllegalArgumentException(s + " is not a valid format for modifications");

        String label = matcher.group(1);
        String formula = matcher.group(2);

        if (label == null)
            label = formula;

        return new Modification(label, Composition.parseComposition(formula));
    }

    public Mass getMass()
    {
        return mass;
    }

    /**
     * cast the Modification to {@link PTM}, throw {@link UnsupportedOperationException} if it is not PTM.
     */
    public PTM asPTM()
    {
        if (this instanceof PTM) {
            return (PTM) this;
        } else {
            throw new UnsupportedOperationException("Cannot cast this Modification instance to PTM.");
        }
    }

    /**
     * @return the title of the Modification
     */
    public String getTitle()
    {
        return title;
    }

    @Override
    public double getMolecularMass()
    {
        return mass.getMolecularMass();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Modification)) return false;
        Modification that = (Modification) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(mass, that.mass);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(title, mass);
    }

    @Override
    public String toString()
    {
        String formula = mass.toString();
        if (formula.equals(title)) {
            return formula;
        } else {
            return String.format("%s:%s", title, formula);
        }
    }
}
