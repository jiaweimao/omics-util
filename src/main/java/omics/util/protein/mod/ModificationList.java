package omics.util.protein.mod;

import com.google.common.collect.Iterators;
import com.google.common.collect.UnmodifiableListIterator;
import omics.util.chem.Weighable;

import java.util.*;

/**
 * interface for Modification list
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 22 Oct 2017, 9:04 PM
 */
public class ModificationList implements Weighable, List<Modification>, RandomAccess {

    private List<Modification> modifications;
    private double mass;

    public ModificationList() {
        this.modifications = new ArrayList<>();
        this.mass = 0;
    }

    @Override
    public int size() {
        return modifications.size();
    }

    @Override
    public boolean isEmpty() {
        return modifications.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return modifications.contains(o);
    }

    @Override
    public Iterator<Modification> iterator() {
        return Iterators.unmodifiableIterator(modifications.iterator());
    }

    @Override
    public Object[] toArray() {
        return modifications.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return modifications.toArray(a);
    }

    @Override
    public boolean add(Modification modification) {
        this.mass += modification.getMolecularMass();
        return modifications.add(modification);
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return modifications.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Modification> c) {
        for (Modification modification : c) {
            mass += modification.getMolecularMass();
        }
        return modifications.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Modification> c) {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public Modification get(int index) {
        return modifications.get(index);
    }

    @Override
    public Modification set(int index, Modification element) {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public void add(int index, Modification element) {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public Modification remove(int index) {
        throw new UnsupportedOperationException("ModificationList is immutable");
    }

    @Override
    public int indexOf(Object o) {
        return modifications.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return modifications.lastIndexOf(o);
    }

    @Override
    public ListIterator<Modification> listIterator() {
        return new WrapperUnmodifiableListIterator(modifications.listIterator());
    }

    @Override
    public ListIterator<Modification> listIterator(int index) {
        return new WrapperUnmodifiableListIterator(modifications.listIterator(index));
    }

    @Override
    public List<Modification> subList(int fromIndex, int toIndex) {
        return modifications.subList(fromIndex, toIndex);
    }

    @Override
    public double getMolecularMass() {
        return mass;
    }

    private static class WrapperUnmodifiableListIterator extends UnmodifiableListIterator<Modification> {
        private final ListIterator<Modification> it;

        private WrapperUnmodifiableListIterator(ListIterator<Modification> it) {
            this.it = it;
        }

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public Modification next() {
            return it.next();
        }

        @Override
        public boolean hasPrevious() {
            return it.hasPrevious();
        }

        @Override
        public Modification previous() {
            return it.previous();
        }

        @Override
        public int nextIndex() {
            return it.nextIndex();
        }

        @Override
        public int previousIndex() {
            return it.previousIndex();
        }
    }

    public static ModificationList EMPTY_MOD_LIST = new ModificationList();
}
