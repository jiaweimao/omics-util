package omics.util.protein.mod;

/**
 * Interface for classes that can create a Modification from a String, namely parser of Modification.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Mar 2017, 7:18 PM
 */
public interface ModificationResolver {
    /**
     * Create or fetch a modification for <code>input</code>
     *
     * @param input the input
     * @return the modification if the input can be resolved, null if not.
     */
    Modification resolve(String input);
}
