package omics.util.protein.mod;

import omics.util.chem.Composition;
import omics.util.chem.Symbol;
import omics.util.chem.Weighable;
import omics.util.utils.ArrayUtils;

import java.util.*;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Class for Neutral loss.
 *
 * @author JiaweiMao
 * @version 1.5.0
 * @since 16 Mar 2017, 1:41 PM
 */
public class NeutralLoss implements Weighable, Symbol
{
    public static final NeutralLoss EMPTY = NeutralLoss.of("");
    public static final NeutralLoss H2O_LOSS = new NeutralLoss("H2O", "°", Composition.H2O);
    public static final NeutralLoss NH3_LOSS = new NeutralLoss("NH3", "*", Composition.NH3);
    public static final NeutralLoss H3PO4_LOSS = NeutralLoss.of("H3PO4", "p", Composition.H3PO4);

    /**
     * @param neutralLosses the neutral loss
     * @return neutral loss string.
     */
    public static String getFormattedSymbol(NeutralLoss[] neutralLosses)
    {
        if (ArrayUtils.isEmpty(neutralLosses))
            return "";
        Map<NeutralLoss, Integer> countMap = new HashMap<>();
        for (NeutralLoss neutralLoss : neutralLosses) {
            countMap.put(neutralLoss, countMap.getOrDefault(neutralLoss, 0) + 1);
        }
        List<NeutralLoss> lossList = new ArrayList<>(countMap.keySet());
        lossList.sort(Comparator.comparing(NeutralLoss::getSymbol));

        StringBuilder builder = new StringBuilder();
        for (NeutralLoss neutralLoss : lossList) {
            if (neutralLoss.getComposition().isEmpty())
                continue;
            Integer count = countMap.get(neutralLoss);
            for (int i = 0; i < count; i++) {
                builder.append(neutralLoss.getSymbol());
            }
        }

        return builder.toString();
    }

    /**
     * Formatted neutral loss name
     *
     * @param neutralLosses the neutral loss
     * @return neutral loss string.
     */
    public static String getFormattedName(NeutralLoss[] neutralLosses)
    {
        if (ArrayUtils.isEmpty(neutralLosses))
            return "";
        Map<NeutralLoss, Integer> countMap = new HashMap<>();
        for (NeutralLoss neutralLoss : neutralLosses) {
            countMap.put(neutralLoss, countMap.getOrDefault(neutralLoss, 0) + 1);
        }
        List<NeutralLoss> lossList = new ArrayList<>(countMap.keySet());
        lossList.sort(Comparator.comparing(NeutralLoss::getName));

        StringBuilder builder = new StringBuilder();
        for (NeutralLoss neutralLoss : lossList) {
            if (neutralLoss.getComposition().isEmpty())
                continue;
            Integer count = countMap.get(neutralLoss);
            builder.append('-');
            if (count > 1)
                builder.append(count);
            String name = neutralLoss.getName();
            if (name.startsWith("-")) {
                builder.append(name.substring(1));
            } else {
                builder.append(name);
            }
        }

        return builder.toString();
    }

    /**
     * Create an {@link NeutralLoss} with name and {@link Composition}
     *
     * @param name        name of the neutral loss
     * @param composition {@link Composition} of the neutral loss.
     * @return {@link NeutralLoss} instance.
     */
    public static NeutralLoss of(String name, String symbol, Composition composition)
    {
        return new NeutralLoss(name, symbol, composition);
    }

    /**
     * Create an {@link NeutralLoss} with name and {@link Composition}
     *
     * @param name        name of the neutral loss
     * @param composition {@link Composition} of the neutral loss.
     * @return {@link NeutralLoss} instance.
     */
    public static NeutralLoss of(String name, Composition composition)
    {
        return new NeutralLoss(name, name, composition);
    }

    /**
     * Create an {@link NeutralLoss} with the chemical formula
     *
     * @param formula chemical formula of the neutral loss
     * @return {@link NeutralLoss} instance.
     */
    public static NeutralLoss of(String formula)
    {
        return of(formula, Composition.parseComposition(formula));
    }

    /**
     * Create an {@link NeutralLoss} with {@link Composition},
     * <p>
     * the symbol and name is the same as formula.
     *
     * @param composition {@link Composition} of the neutral loss.
     * @return {@link NeutralLoss} instance.
     */
    public static NeutralLoss of(Composition composition)
    {
        return of(composition.getFormula(), composition);
    }

    private final String name;
    private final String symbol;
    private final Composition composition;

    /**
     * Constructor.
     *
     * @param symbol      short name as identifier.
     * @param name        name
     * @param composition Mass info.
     */
    NeutralLoss(String name, String symbol, Composition composition)
    {
        checkNotNull(composition);

        this.symbol = symbol;
        this.name = name;
        this.composition = composition;
    }

    @Override
    public String getSymbol()
    {
        return symbol;
    }

    /**
     * @return the name of the Neutral loss.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the Composition of this NeutralLoss
     */
    public Composition getComposition()
    {
        return composition;
    }

    @Override
    public String toString()
    {
        return "NeutralLoss{" +
                "name='" + name + '\'' +
                ", symbol='" + symbol + '\'' +
                ", composition=" + composition +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NeutralLoss that = (NeutralLoss) o;

        return composition.equals(that.composition);
    }

    @Override
    public int hashCode()
    {
        return composition.hashCode();
    }

    @Override
    public double getMolecularMass()
    {
        return composition.getMolecularMass();
    }

}
