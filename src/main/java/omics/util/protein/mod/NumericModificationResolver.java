package omics.util.protein.mod;

import omics.util.chem.NumericMass;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Parse numbers as modification.with format: </br>
 * "\\{?([-+]?\\d*\\.?\\d+)\\}?" </br>
 * for example: 65.9 -25.6
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Mar 2017, 7:22 PM
 */
public class NumericModificationResolver implements ModificationResolver {
    private final Pattern MOD_MASS_PATTERN = Pattern.compile("\\{?([-+]?\\d*\\.?\\d+)\\}?");

    @Override
    public Modification resolve(String input) {
        Matcher matcher = MOD_MASS_PATTERN.matcher(input);
        if (matcher.matches()) {

            double val = Double.parseDouble(matcher.group(1));
            return new Modification(input, new NumericMass(val));
        }

        return null;
    }
}
