package omics.util.protein.mod;

import omics.util.chem.Composition;
import omics.util.chem.Mass;
import omics.util.protein.PeptideMod;

import java.util.*;

/**
 * a PTM.
 *
 * @author JiaweiMao
 * @version 2.5.0
 * @since 16 Mar 2017, 9:48 PM
 */
public class PTM extends Modification
{
    /**
     * Return the PTM of given name.
     *
     * @param name ptm name
     * @return {@link PTM} of the name.
     */
    public static PTM ofName(String name)
    {
        PTMFactory factory = PTMFactory.getInstance();
        return factory.ofName(name);
    }

    /**
     * Return the PTM of given unimod id.
     *
     * @param name unimod id
     * @return {@link PTM} of the id.
     */
    public static PTM ofId(String name)
    {
        PTMFactory factory = PTMFactory.getInstance();
        return factory.ofUnimodId(name);
    }


    public static PTM Carbamidomethyl()
    {
        return ofName("Carbamidomethyl");
    }

    /**
     * @return the Oxidation {@link PTM} instance.
     */
    public static PTM Oxidation()
    {
        return ofName("Oxidation");
    }

    public static PTM Phospho()
    {
        return ofName("Phospho");
    }

    public static PTM Methyl()
    {
        return ofName("Methyl");
    }

    public static PTM Acetyl()
    {
        return ofName("Acetyl");
    }

    public static PTM Deamidated()
    {
        return ofName("Deamidated");
    }

    public static PTM Amidated()
    {
        return ofName("Amidated");
    }

    /**
     * PtmSite list where this PTM can occur.
     */
    private final HashSet<Specificity> specificities = new HashSet<>();

    private final String userName;
    private String accession;
    private String fullName;
    private String miscNote;

    /**
     * Copy Constructor.
     * copy the ptm without {@link Specificity}
     *
     * @param ptm src ptm
     */
    public PTM(PTM ptm)
    {
        super(ptm.title, ptm.mass);

        this.userName = ptm.userName;
        this.accession = ptm.accession;
        this.fullName = ptm.fullName;
        this.miscNote = ptm.miscNote;
    }

    /**
     * Constructor with empty Specificity set.
     *
     * @param title PTM title, should be unique.
     * @param mass  PTM mass.
     */
    public PTM(String title, Mass mass)
    {
        this(title, mass, null);
    }

    /**
     * Constructor with null description.
     *
     * @param title    PTM title, should be unique.
     * @param mass     PTM mass.
     * @param ptmSites PtmSites.
     */
    public PTM(String title, Mass mass, List<Specificity> ptmSites)
    {
        this(null, title, null, mass, ptmSites, "mjw", null);
    }

    public PTM(String title, String fullName, Mass mass, List<Specificity> ptmSites, String userName)
    {
        this(null, title, fullName, mass, ptmSites, userName, null);
    }

    /**
     * @param accession unimod record id
     * @param title     PTM title, should be unique, as it is used as id.
     * @param fullName  PTM description.
     * @param mass      {@link Mass}
     * @param ptmSites  {@link Specificity} list
     * @param userName  user name
     * @param miscNote  PTM miscellaneous note.
     */
    public PTM(String accession, String title, String fullName, Mass mass, List<Specificity> ptmSites,
               String userName, String miscNote)
    {
        super(title, mass);

        this.accession = accession;
        this.fullName = fullName;
        this.userName = userName;
        this.miscNote = miscNote;
        if (ptmSites != null) {
            this.specificities.addAll(ptmSites);
        }
    }

    /**
     * @return a unique accession value.
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * set the PTM id.
     */
    public void setAccession(String accession)
    {
        this.accession = accession;
    }

    /**
     * @return PTM full name, empty string for absent.
     */
    public String getFullName()
    {
        return fullName;
    }

    /**
     * @return the Composition if present, or null for absent.
     */
    public Composition getComposition()
    {
        if (mass instanceof Composition)
            return (Composition) mass;

        return null;
    }

    /**
     * @return an unmodifiable list of {@link Specificity} where this PTM can occur.
     */
    public List<Specificity> getSpecificityList()
    {
        return new ArrayList<>(specificities);
    }

    /**
     * @return {@link Specificity} of this PTM.
     */
    public Set<Specificity> getSpecificitySet()
    {
        return specificities;
    }

    public void addSpecificities(Collection<Specificity> specificities)
    {
        this.specificities.addAll(specificities);
    }

    public void addSpecificity(Specificity aSpecificity)
    {
        this.specificities.add(aSpecificity);
    }

    /**
     * @return all {@link omics.util.protein.PeptideMod} of this ptm.
     */
    public List<PeptideMod> getPeptideModList()
    {
        List<PeptideMod> peptideMods = new ArrayList<>(specificities.size());
        for (Specificity specificity : specificities) {
            peptideMods.add(new PeptideMod(this, specificity, false));
        }
        return peptideMods;
    }

    /**
     * Return true if this PTM contains given {@link Specificity}
     *
     * @param specificity {@link Specificity} to test
     * @return true if this PTM contains the given {@link Specificity}
     */
    public boolean containSpecificity(Specificity specificity)
    {
        return this.specificities.contains(specificity);
    }

    /**
     * @return user name
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * @return miscellaneous note about this PTM, null for absent.
     */
    public String getMiscNote()
    {
        return miscNote;
    }

    public void setMiscNote(String miscNote)
    {
        this.miscNote = miscNote;
    }
}
