package omics.util.protein.mod;

import com.google.common.collect.ComparisonChain;
import omics.util.chem.Composition;
import omics.util.chem.Weighable;
import omics.util.io.ParameterFile;
import omics.util.io.xml.XMLUtils;
import omics.util.ms.peaklist.Tolerance;
import omics.util.protein.AminoAcid;
import omics.util.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;


/**
 * This factory will load PTM from an xml file and provide them on demand as standard class.
 *
 * @author JiaweiMao
 * @version 3.2.0
 * @since 16 Mar 2017, 6:58 PM
 */
public final class PTMFactory implements ParameterFile<PTM>
{
    private static final Logger logger = LoggerFactory.getLogger(PTMFactory.class);

    // initialization on demand holder idiom
    private static class LazyHolder
    {
        static final PTMFactory INSTANCE = new PTMFactory();
    }

    /**
     * @return the instance of the factory.
     */
    public static PTMFactory getInstance()
    {
        return LazyHolder.INSTANCE;
    }

    private PTMFactory()
    {
        try {
            List<PTM> ptmList = initialize(PTMFactory.class.getClassLoader());
            for (PTM ptm : ptmList) {
                add(ptm);
            }

        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private final HashMap<String, PTM> titlePTMMap = new HashMap<>();
    private final HashMap<String, PTM> idMap = new HashMap<>();
    private final ArrayList<PTM> ptmList = new ArrayList<>();
    private boolean sorted = false;

    @Override
    public List<PTM> read(InputStream inputStream) throws XMLStreamException
    {
        List<PTM> ptmList = new ArrayList<>();

        XMLStreamReader reader = XMLUtils.createXMLStreamReader(inputStream);

        String id = null;
        String title = null;
        String description = null;
        String note = null;
        String formula = null;
        String user = null;
        List<Specificity> specificityList = new ArrayList<>();

        while (reader.hasNext()) {
            int event = reader.next();
            if (event == XMLStreamConstants.START_ELEMENT) {
                String localName = reader.getLocalName();
                switch (localName) {
                    case "mod":
                        id = reader.getAttributeValue(null, "id");
                        title = reader.getAttributeValue(null, "title");
                        description = reader.getAttributeValue(null, "desp");
                        user = reader.getAttributeValue(null, "user");
                        break;
                    case "delta":
                        formula = reader.getAttributeValue(null, "formula");
                        break;
                    case "site": {
                        String aa = reader.getAttributeValue(null, "aa");
                        String pos = reader.getAttributeValue(null, "pos");
                        String classification = reader.getAttributeValue(null, "class");

                        NeutralLoss neutralLoss = null;
                        while (reader.hasNext()) {
                            reader.next();
                            if (XMLUtils.isEndElement(reader, "site"))
                                break;
                            if (XMLUtils.isStartElement(reader, "NeutralLoss")) {
                                String nlName = reader.getAttributeValue(null, "name");
                                String nlSymbol = reader.getAttributeValue(null, "symbol");
                                String nlFormula = reader.getAttributeValue(null, "formula");
                                neutralLoss = NeutralLoss.of(nlName, nlSymbol, Composition.parseComposition(nlFormula));
                            }
                        }

                        Pos pos4Name = Pos.getPos4Name(pos);
                        AminoAcid aminoAcid;
                        if (aa.charAt(0) == '_') {
                            if (pos4Name.isCTerm()) {
                                aminoAcid = AminoAcid.C_TERM;
                            } else {
                                aminoAcid = AminoAcid.N_TERM;
                            }
                        } else {
                            aminoAcid = AminoAcid.getAminoAcid(aa.charAt(0));
                        }

                        Specificity ptmSite = new Specificity(aminoAcid, pos4Name, neutralLoss, classification);
                        specificityList.add(ptmSite);
                        break;
                    }
                    case "note":
                        note = reader.getElementText();
                        break;
                }
            } else if (XMLUtils.isEndElement(reader, "mod")) {
                PTM ptm = new PTM(id, title, description, Composition.parseComposition
                        (formula), new ArrayList<>(specificityList), user, note);

                ptmList.add(ptm);

                id = null;
                title = null;
                description = null;
                formula = null;
                note = null;
                user = null;
                specificityList.clear();
            }
        }
        reader.close();
        return ptmList;
    }

    @Override
    public void write(OutputStream outputStream) throws XMLStreamException
    {
        XMLStreamWriter writer = XMLUtils.createIndentXMLStreamWriter(outputStream);

        writer.writeStartDocument("UTF-8", "1.0");
        writer.writeStartElement("mods");
        writer.writeAttribute("count", String.valueOf(ptmList.size()));

        for (PTM ptm : ptmList) {
            writer.writeStartElement("mod");
            writer.writeAttribute("id", ptm.getAccession());
            writer.writeAttribute("title", ptm.getTitle());
            String description = ptm.getFullName();
            if (StringUtils.isNotEmpty(description)) {
                writer.writeAttribute("desp", description);
            }
            String userName = ptm.getUserName();
            if (StringUtils.isEmpty(userName))
                userName = "mjw";
            writer.writeAttribute("user", userName);

            // write delta
            Composition compOp = ptm.getComposition();
            String formula = "";
            if (compOp != null) {
                formula = compOp.getFormula();
            }
            writer.writeEmptyElement("delta");
            writer.writeAttribute("formula", formula);

            // write sites
            List<Specificity> specificityList = ptm.getSpecificityList();
            // sort for consistent output
            specificityList.sort((o1, o2) -> ComparisonChain.start()
                    .compare(o1.getAminoAcid().getOneLetterCode(), o2.getAminoAcid().getOneLetterCode())
                    .compare(o1.getPosition(), o2.getPosition()).result());
            for (Specificity site : specificityList) {
                if (site.getNeutralLoss().isPresent()) {
                    writer.writeStartElement("site");
                    writer.writeAttribute("aa", site.getAminoAcid().getSymbol());
                    writer.writeAttribute("pos", site.getPosition().getName());
                    String classification = site.getClassification();
                    if (StringUtils.isNotEmpty(classification))
                        writer.writeAttribute("class", classification);

                    NeutralLoss neutralLoss = site.getNeutralLoss().get();

                    writer.writeEmptyElement("NeutralLoss");
                    writer.writeAttribute("name", neutralLoss.getName());
                    writer.writeAttribute("symbol", neutralLoss.getSymbol());
                    writer.writeAttribute("formula", neutralLoss.getComposition().getFormula());
                    writer.writeEndElement();
                } else {
                    writer.writeEmptyElement("site");
                    writer.writeAttribute("aa", site.getAminoAcid().getSymbol());
                    writer.writeAttribute("pos", site.getPosition().getName());
                    String classification = site.getClassification();
                    if (StringUtils.isNotEmpty(classification))
                        writer.writeAttribute("class", classification);
                }
            }

            String miscNoteOp = ptm.getMiscNote();
            if (StringUtils.isNotEmpty(miscNoteOp)) {
                writer.writeStartElement("note");
                writer.writeCharacters(miscNoteOp);
                writer.writeEndElement();
            }

            writer.writeEndElement();
        }

        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();
    }

    /**
     * Return the PTM of given name, null for absent.
     *
     * @param ptmName ptm name
     */
    public PTM ofName(String ptmName)
    {
        return titlePTMMap.get(ptmName);
    }

    /**
     * Returns true if the PTM is loaded in the factory.
     *
     * @param name the name of the PTM
     */
    public boolean containPTM(String name)
    {
        return titlePTMMap.containsKey(name);
    }

    /**
     * Return the PTM of given UNIMOD record id or user defined id.
     */
    public PTM ofUnimodId(String id)
    {
        return idMap.get(id);
    }

    /**
     * Add a custom PTM to this factory.
     *
     * @param ptm a {@link PTM} object.
     */
    public boolean add(PTM ptm)
    {
        if (titlePTMMap.containsKey(ptm.getTitle())) {
            logger.error("PTM with name {} is already contained in this factory.", ptm.getTitle());
            return false;
        }

        if (ptm.getAccession() == null) {
            int count = 1;
            String userName = ptm.getUserName();
            while (idMap.containsKey(userName + count)) {
                count++;
            }
            ptm.setAccession(userName + count);
        } else {
            if (idMap.containsKey(ptm.getAccession())) {
                logger.error("PTM with accession {} is already exists.", ptm.getTitle());
                return false;
            }
        }

        titlePTMMap.put(ptm.getTitle(), ptm);
        idMap.put(ptm.getAccession(), ptm);
        ptmList.add(ptm);
        sorted = false;

        return true;
    }

    @Override
    public String getSerializeFileName()
    {
        return "mods.xml";
    }

    /**
     * Remove a custom PTM from {@link PTMFactory}
     *
     * @param ptm a PTM
     */
    public void remove(PTM ptm)
    {
        if (!titlePTMMap.containsKey(ptm.getTitle())) {
            return;
        }
        this.titlePTMMap.remove(ptm.getTitle());
        this.idMap.remove(ptm.getAccession());
        this.ptmList.remove(ptm);
    }

    /**
     * get all the PTMs with given mass at given Amino Acids
     *
     * @param mass       ptm mass
     * @param tolerance  mass tolerance
     * @param aminoAcids AminoAcid set
     */
    public List<PTM> get(final double mass, final Tolerance tolerance, final Set<AminoAcid> aminoAcids)
    {
        List<PTM> motifs = get(mass, tolerance);
        List<PTM> matchMotifs = new ArrayList<>();
        for (PTM motif : motifs) {
            List<Specificity> ptmSites = motif.getSpecificityList();
            for (Specificity site : ptmSites) {
                if (aminoAcids.contains(site.getAminoAcid())) {
                    matchMotifs.add(motif);
                    break;
                }
            }
        }

        return matchMotifs;
    }

    /**
     * Get all PTMs with the given mass, empty list for no match.
     *
     * @param mass      the PTM mass
     * @param tolerance Tolerance
     */
    public List<PTM> get(final double mass, final Tolerance tolerance)
    {
        if (!sorted) {
            ptmList.sort(Comparator.comparing(PTM::getMolecularMass));
            sorted = true;
        }

        List<PTM> lists = new ArrayList<>();
        Weighable key = () -> tolerance.getMin(mass);
        int index = Collections.binarySearch(ptmList, key, Comparator.comparingDouble(Weighable::getMolecularMass));
        if (index < 0)
            index = -1 * (index + 1);

        for (; index < ptmList.size(); index++) {

            PTM ptm = ptmList.get(index);
            Tolerance.Location location = tolerance.check(mass, ptm.getMolecularMass());
            if (location == Tolerance.Location.WITHIN) {
                lists.add(ptm);
            } else if (location == Tolerance.Location.LARGER)
                break;
            else
                throw new IllegalStateException("Have bug in code");
        }

        return lists;
    }

    /**
     * @return an unmodifiable list containing all the modifications that are managed by this
     */
    public List<PTM> getPTMList()
    {
        return Collections.unmodifiableList(ptmList);
    }


    /**
     * an unmodifiable list of available modifications
     *
     * @return available modifications
     */
    @Override
    public List<PTM> getItemList()
    {
        return Collections.unmodifiableList(ptmList);
    }

    @Override
    public int size()
    {
        return ptmList.size();
    }

    /**
     * @return the names of all imported PTMs.
     */
    public List<String> getPTMNames()
    {
        return new ArrayList<>(titlePTMMap.keySet());
    }

}
