package omics.util.protein.mod;

import java.util.HashMap;
import java.util.Map;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Resolve PTM in the unimod.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 16 Mar 2017, 7:18 PM
 */
public class PTMResolver implements ModificationResolver {
    private final Map<String, Modification> overrideMap = new HashMap<>();
    private final PTMFactory factory = PTMFactory.getInstance();

    @Override
    public Modification resolve(String input) {
        Modification mod;
        if (overrideMap.containsKey(input)) {
            mod = overrideMap.get(input);
        } else if (factory.containPTM(input)) {
            mod = factory.ofName(input);
        } else {
            try {
                return Modification.parseModification(input);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }

        return mod;
    }

    /**
     * Override the modification that the name is mapped to in Unimod.
     *
     * @param name         the modification name
     * @param modification the modification to return
     */
    public void putOverrideUnimod(String name, Modification modification) {
        checkNotNull(name);
        checkNotNull(modification);

        overrideMap.put(name, modification);
    }


    public void clearOverrides() {
        overrideMap.clear();
    }
}
