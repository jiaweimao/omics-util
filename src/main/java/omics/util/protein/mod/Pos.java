package omics.util.protein.mod;


import omics.util.cv.CvTerm;

/**
 * Position where the PTM occurs.
 *
 * @author JiaweiMao
 * @version 2.3.0
 * @since 05 Sep 2017, 10:55 AM
 */
public enum Pos
{
    ANY_WHERE("Anywhere"),
    ANY_C_TERM("Any C-term"),
    ANY_N_TERM("Any N-term"),
    PROTEIN_C_TERM("Protein C-term"),
    PROTEIN_N_TERM("Protein N-term"),
    NOT_C_TERM("Not C-term"),
    NOT_N_TERM("Not N-term"),
    NOT_TERM("Not Term");

    private String name;

    Pos(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    /**
     * @return true if this pos is a protein terminal specified.
     */
    public boolean isProteinTerm()
    {
        return this == Pos.PROTEIN_C_TERM || this == Pos.PROTEIN_N_TERM;
    }

    /**
     * @return true if this pos is a c-term position.
     */
    public boolean isCTerm()
    {
        return this == ANY_C_TERM || this == PROTEIN_C_TERM;
    }

    /**
     * @return true if this pos is a n-term position.
     */
    public boolean isNTerm()
    {
        return this == ANY_N_TERM || this == PROTEIN_N_TERM;
    }


    /**
     * @return this {@link CvTerm} of this Pos
     */
    public CvTerm getPSIMSCv()
    {
        switch (this) {
            case ANY_C_TERM:
                return new CvTerm("MS:1001190", "modification specificity peptide C-term");
            case ANY_N_TERM:
                return new CvTerm("MS:1001189", "modification specificity peptide N-term");
            case PROTEIN_N_TERM:
                return new CvTerm("MS:1002057", "modification specificity protein N-term");
            case PROTEIN_C_TERM:
                return new CvTerm("MS:1002058", "modification specificity protein N-term");
        }

        return null;
    }

    /**
     * @return commonly used {@link Pos}es
     */
    public static Pos[] getCommonsPositions()
    {
        return new Pos[]{ANY_WHERE, ANY_N_TERM, ANY_C_TERM, PROTEIN_N_TERM, PROTEIN_C_TERM};
    }

    /**
     * get the position from its name
     *
     * @param name position name
     * @return a {@link Pos} object.
     */
    public static Pos getPos4Name(String name)
    {
        for (Pos ptmPos : Pos.values()) {
            if (ptmPos.getName().equals(name))
                return ptmPos;
        }

        return Pos.ANY_WHERE;
    }

    /**
     * Return the Pos of given PSI-MS accession. {@link Pos#ANY_WHERE} for failed.
     *
     * @param accession PSI-MS accession.
     */
    public static Pos getPos4Acc(String accession)
    {
        switch (accession) {
            case "MS:1001189":
                return Pos.ANY_N_TERM;
            case "MS:1001190":
                return Pos.ANY_N_TERM;
            case "MS:1002057":
                return Pos.PROTEIN_N_TERM;
            case "MS:1002058":
                return Pos.PROTEIN_C_TERM;
        }
        return Pos.ANY_WHERE;
    }
}
