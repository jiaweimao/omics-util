package omics.util.protein.mod;

import omics.util.OmicsRuntimeException;
import omics.util.protein.AminoAcid;

import java.util.Objects;
import java.util.Optional;


/**
 * PtmSite is a class that contains the information of the amino acid specificity for
 * modifications and also specified the position of this amino acids (N-term, C-Term) or
 * Anywhere.
 *
 * @author JiaweiMao
 * @version 1.4.0
 * @since 16 Mar 2017, 9:18 AM
 */
public class Specificity
{
    private static final String N_TERM = "N-term";
    private static final String C_TERM = "C-term";
    private static final String POST_TRANSLATIONAL = "Post-translational";

    public static Specificity ProteinNTerm = new Specificity(AminoAcid.N_TERM, Pos.PROTEIN_N_TERM);
    public static Specificity ProteinCTerm = new Specificity(AminoAcid.C_TERM, Pos.PROTEIN_C_TERM);
    public static Specificity AnyNTerm = new Specificity(AminoAcid.N_TERM, Pos.ANY_N_TERM);
    public static Specificity AnyCTerm = new Specificity(AminoAcid.C_TERM, Pos.ANY_C_TERM);

    private final AminoAcid site;
    private final Pos position;
    private final NeutralLoss neutralLoss;
    private final String classification;
    private final String note;

    /**
     * Constructor.
     *
     * @param site           {@link AminoAcid}, Choose 'N-term'or 'C-Term' if the modification applies to a terminus
     *                       independent of the identity of the terminal residue, (e.g. methylation of a carboxy terminus).
     * @param position       Choose 'Anywhere' if the modification applies to a residue independent of its position, (e.g.
     *                       oxidation of methionine). Choose 'Any N-term' or 'Any C-term' if the modification applies to a
     *                       residue only when it is at a peptide terminus, (e.g. conversion of methionine to homoserine).
     *                       Choose 'Protein N-term' or 'Protein C-term' if the modification only applies to the original
     *                       terminus of the intact protein, not new peptide termini created by digestion, (e.g.
     *                       post-translational acetylation of the protein amino terminus). If Site was specified as 'N-term'
     *                       or 'C-Term', then 'Anywhere' becomes equivalent to 'Any N-term' or 'Any C-term'.
     * @param neutralLoss    {@link NeutralLoss}
     * @param classification description of the classification
     * @param note           optional note.
     */
    public Specificity(AminoAcid site, Pos position, NeutralLoss neutralLoss, String classification, String note)
    {
        this.site = site;
        this.position = position;
        this.neutralLoss = neutralLoss;
        this.classification = classification;
        this.note = note;
    }

    public Specificity(AminoAcid site, Pos position, NeutralLoss neutralLoss, String classification)
    {
        this(site, position, neutralLoss, classification, null);
    }

    /**
     * Constructor.
     *
     * @param site        {@link AminoAcid}, Choose 'N-term'or 'C-Term' if the modification applies to a terminus
     *                    independent of the identity of the terminal residue, (e.g. methylation of a carboxy terminus).
     * @param position    Choose 'Anywhere' if the modification applies to a residue independent of its position, (e.g.
     *                    oxidation of methionine). Choose 'Any N-term' or 'Any C-term' if the modification applies to a
     *                    residue only when it is at a peptide terminus, (e.g. conversion of methionine to homoserine).
     *                    Choose 'Protein N-term' or 'Protein C-term' if the modification only applies to the original
     *                    terminus of the intact protein, not new peptide termini created by digestion, (e.g.
     *                    post-translational acetylation of the protein amino terminus). If Site was specified as 'N-term'
     *                    or 'C-Term', then 'Anywhere' becomes equivalent to 'Any N-term' or 'Any C-term'.
     * @param neutralLoss {@link NeutralLoss}
     */
    public Specificity(AminoAcid site, Pos position, NeutralLoss neutralLoss)
    {
        this(site, position, neutralLoss, POST_TRANSLATIONAL);
    }

    public Specificity(AminoAcid site, Pos position)
    {
        this(site, position, null, POST_TRANSLATIONAL, null);
    }

    /**
     * Construct with {@link AminoAcid} and default {@link Pos#ANY_WHERE}
     */
    public Specificity(AminoAcid site)
    {

        this(site, Pos.ANY_WHERE);
    }

    public Specificity(AminoAcid site, Pos pos, String classification)
    {
        this(site, pos, null, classification);
    }

    /**
     * A simple parser for Mascot output {@link Specificity}.
     *
     * @param input input string with format: "[Position] [AA]"
     * @return a {@link Specificity}
     */
    public static Specificity parse(String input)
    {
        AminoAcid aa;
        Pos pos;
        if (input.contains(Pos.PROTEIN_N_TERM.getName())) {
            pos = Pos.PROTEIN_N_TERM;
            if (input.equals(Pos.PROTEIN_N_TERM.getName())) {
                aa = AminoAcid.N_TERM;
            } else {
                String aaSymbol = input.substring(Pos.PROTEIN_N_TERM.getName().length()).trim();
                aa = AminoAcid.getAminoAcid(aaSymbol.charAt(0));
            }
        } else if (input.contains(Pos.PROTEIN_C_TERM.getName())) {
            pos = Pos.PROTEIN_C_TERM;
            if (input.equals(Pos.PROTEIN_C_TERM.getName())) {
                aa = AminoAcid.C_TERM;
            } else {
                String aaSymbol = input.substring(Pos.PROTEIN_C_TERM.getName().length()).trim();
                aa = AminoAcid.getAminoAcid(aaSymbol.charAt(0));
            }
        } else if (input.contains(N_TERM)) {
            pos = Pos.ANY_N_TERM;
            if (input.equals(N_TERM)) {
                aa = AminoAcid.N_TERM;
            } else {
                String aaSymbol = input.substring(N_TERM.length()).trim();
                aa = AminoAcid.getAminoAcid(aaSymbol.charAt(0));
            }
        } else if (input.contains(C_TERM)) {
            pos = Pos.ANY_C_TERM;
            if (input.equals(C_TERM)) {
                aa = AminoAcid.C_TERM;
            } else {
                String aaSymbol = input.substring(C_TERM.length()).trim();
                aa = AminoAcid.getAminoAcid(aaSymbol.charAt(0));
            }
        } else {
            pos = Pos.ANY_WHERE;
            try {
                aa = AminoAcid.getAminoAcid(input.charAt(0));
            } catch (Exception e) {
                throw new OmicsRuntimeException("Cannot parser given specificity: " + input);
            }
        }

        return new Specificity(aa, pos);
    }

    /**
     * @return the site of modification, or AminoAcid.X if any amino acid acceptable
     */
    public AminoAcid getAminoAcid()
    {
        return site;
    }

    /**
     * @return Modification position
     */
    public Pos getPosition()
    {
        return position;
    }

    /**
     * @return additional information about this PtmSite.
     */
    public Optional<String> getNote()
    {
        return Optional.ofNullable(note);
    }

    public Optional<NeutralLoss> getNeutralLoss()
    {
        return Optional.ofNullable(neutralLoss);
    }

    /**
     * @return the classification of this {@link Specificity}
     */
    public String getClassification()
    {
        return classification;
    }

    @Override
    public String toString()
    {
        return "Specificity{" +
                "site=" + site +
                ", position=" + position +
                ", neutralLoss=" + neutralLoss +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Specificity that = (Specificity) o;
        return site == that.site &&
                position == that.position &&
                Objects.equals(neutralLoss, that.neutralLoss);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(site, position, neutralLoss);
    }
}
