package omics.util.protein.ms;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.protein.PeptideFragment;
import omics.util.protein.mod.NeutralLoss;
import omics.util.utils.StringUtils;

/**
 * Annotation of peptide backbone ions.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Aug 2020, 12:21 PM
 */
public class BackboneAnnotation extends PeptideFragmentAnnotation
{
    private final NeutralLoss[] neutralLosses;

    public BackboneAnnotation(PeptideIon peptideIon, PeptideFragment fragment, double theoreticalMz)
    {
        this(peptideIon.getIon(), peptideIon.getCharge(), fragment, theoreticalMz, peptideIon.getNeutralLosses());
    }

    /**
     * Constructor
     *
     * @param peptideIon {@link PeptideIon}
     * @param fragment   {@link PeptideFragment}
     */
    public BackboneAnnotation(PeptideIon peptideIon, PeptideFragment fragment)
    {
        this(peptideIon.getIon(), peptideIon.getCharge(), fragment, -1, null);
    }

    /**
     * Constructor
     *
     * @param ion           {@link Ion}
     * @param charge        charge of the ion
     * @param fragment      {@link PeptideFragment} of the ion
     * @param theoreticalMz theoretical m/z
     * @param neutralLosses {@link NeutralLoss} of this fragment
     */
    public BackboneAnnotation(Ion ion, int charge, PeptideFragment fragment, double theoreticalMz, NeutralLoss[] neutralLosses)
    {
        super(ion, charge, fragment, theoreticalMz);
        this.neutralLosses = neutralLosses;
        StringBuilder builder = new StringBuilder(ion.getName());
        builder.append(fragment.size());
        if (neutralLosses != null && neutralLosses.length > 0) {
            builder.append(NeutralLoss.getFormattedSymbol(neutralLosses));
        }
        if (charge > 1) {
            builder.append(StringUtils.superscript(charge + "+"));
        }
        this.symbol = builder.toString();
    }

    public NeutralLoss[] getNeutralLosses()
    {
        return neutralLosses;
    }

    @Override
    public PeakAnnotation copy()
    {
        return new BackboneAnnotation(ion, charge, fragment, theoreticalMz, neutralLosses);
    }
}
