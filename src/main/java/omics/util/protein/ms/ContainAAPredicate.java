package omics.util.protein.ms;

import omics.util.protein.AminoAcid;
import omics.util.protein.PeptideFragment;

import java.util.Set;
import java.util.function.Predicate;


/**
 * Used to test the presence of given amino acid set.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 4:02 PM
 */
public class ContainAAPredicate implements Predicate<PeptideFragment>
{
    final Set<AminoAcid> iAminoAcids;
    int iAACount;

    public ContainAAPredicate(Set<AminoAcid> aAminoAcids)
    {
        this.iAminoAcids = aAminoAcids;
    }

    @Override
    public boolean test(PeptideFragment peptideFragment)
    {
        iAACount = peptideFragment.countAminoAcidsIn(iAminoAcids);
        return iAACount > 0;
    }

    /**
     * Returns number of Amino Acids in the PeptideFragment match the given Amino acid set.
     * this methods should called after {@link #test(PeptideFragment)}
     *
     * @return number of test amino acid.
     */
    public int getAACount()
    {
        return iAACount;
    }
}
