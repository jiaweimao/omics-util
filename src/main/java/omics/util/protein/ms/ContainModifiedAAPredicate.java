package omics.util.protein.ms;

import omics.util.chem.Mass;
import omics.util.protein.AminoAcid;
import omics.util.protein.PeptideFragment;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.ModificationList;

import java.util.HashSet;
import java.util.Set;

/**
 * Test presence of amino acid that has a modification which is equal to the
 * mass of one modification in <code>modifications</code>.
 * <p>
 * For example if the amino acid set is {S, T} and the modification set is
 * {HPO3:phosphate} then a neutral loss is generated for PEPTS(HPO3)IDE,
 * PEPTS(HPO3:phosphate)IDE and PEPTS(H1P1O3)IDE but not PEPTS(79.9)IDE or
 * PEPTS(79.9:phosphate)IDE
 * </p>
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 20 Mar 2017, 4:03 PM
 */
public class ContainModifiedAAPredicate extends ContainAAPredicate
{
    private final Set<Mass> modificationMasses;
    private final Set<ModAttachment> modAttachments;
    private int count = 1;

    /**
     * Constructor.
     *
     * @param aAminoAcids        {@link Modification} target amino acids.
     * @param modificationMasses {@link Modification}s
     * @param modAttachments     {@link ModAttachment}s.
     * @param count              minimum Modified Amino acid required.
     */
    public ContainModifiedAAPredicate(Set<AminoAcid> aAminoAcids, Set<Mass> modificationMasses,
            Set<ModAttachment> modAttachments, int count)
    {

        super(aAminoAcids);
        this.modificationMasses = modificationMasses;
        this.modAttachments = modAttachments;
        this.count = count;
    }

    public ContainModifiedAAPredicate(Set<AminoAcid> aminoAcids, Set<Modification> modifications,
            Set<ModAttachment> modAttachments)
    {
        super(aminoAcids);
        this.modificationMasses = new HashSet<>(modifications.size());
        for (Modification mod : modifications) {
            modificationMasses.add(mod.getMass());
        }
        this.modAttachments = modAttachments;
    }

    @Override
    public boolean test(PeptideFragment fragment)
    {
        iAACount = 0;
        for (int i : fragment.getModificationIndexes(modAttachments)) {

            if (iAminoAcids.contains(fragment.getSymbol(i)) && hasModifications(fragment.getModificationsAt(i, modAttachments)))
                iAACount++;
        }

        return iAACount >= count;
    }

    private boolean hasModifications(ModificationList modificationsAt)
    {
        for (Modification mod : modificationsAt) {
            if (this.modificationMasses.contains(mod.getMass()))
                return true;
        }

        return false;
    }
}
