package omics.util.protein.ms;

/**
 * An enum to represent the types of fragments.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 15 Mar 2017, 1:17 PM
 */
public enum FragmentType
{
    /**
     * From N-term to C-term
     */
    FORWARD,
    /**
     * From C-term to N-term
     */
    REVERSE,
    /**
     * Internal
     */
    INTERNAL,
    /**
     * Only one amino acid, such as immonium ion.
     */
    MONOMER,
    /**
     * The intact peptide
     */
    INTACT,
    /**
     * Unknown type
     */
    UNKNOWN;
}
