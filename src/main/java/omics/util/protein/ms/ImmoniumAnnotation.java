package omics.util.protein.ms;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.protein.PeptideFragment;
import omics.util.utils.StringUtils;

/**
 * Annotation for immonium ions
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Aug 2020, 12:09 PM
 */
public class ImmoniumAnnotation extends PeptideFragmentAnnotation
{
    public ImmoniumAnnotation(int charge, PeptideFragment fragment, double theoreticalMz)
    {
        super(Ion.im, charge, fragment, theoreticalMz);
        this.symbol = charge > 1 ? fragment.getSymbol(0).getSymbol() + StringUtils.superscript(charge + "+")
                : fragment.getSymbol(0).getSymbol();
    }

    @Override
    public PeakAnnotation copy()
    {
        return new ImmoniumAnnotation(charge, fragment, theoreticalMz);
    }
}
