package omics.util.protein.ms;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.utils.StringUtils;

/**
 * Peptide internal fragment annotation
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Aug 2020, 10:57 AM
 */
public class InternalFragmentAnnotation extends PeptideFragmentAnnotation
{
    private final int beginIndex;
    private final int endIndex;

    /**
     * Constructor
     *
     * @param ion        {@link Ion} of the internal fragment
     * @param charge     ion charge
     * @param beginIndex begin index, inclusive
     * @param endIndex   end index, exclusive
     */
    public InternalFragmentAnnotation(Ion ion, int charge, int beginIndex, int endIndex)
    {
        super(ion, charge);
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
        StringBuilder builder = new StringBuilder(ion.getName());
        builder.append('[')
                .append(beginIndex + 1)
                .append(',')
                .append(endIndex + 1)
                .append(']');
        if (charge > 1)
            builder.append(StringUtils.superscript(charge + "+"));
        this.symbol = builder.toString();
    }

    public int getBeginIndex()
    {
        return beginIndex;
    }

    public int getEndIndex()
    {
        return endIndex;
    }

    @Override
    public PeakAnnotation copy()
    {
        return new InternalFragmentAnnotation(ion, charge, beginIndex, endIndex);
    }
}
