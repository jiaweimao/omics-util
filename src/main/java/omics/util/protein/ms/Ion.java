package omics.util.protein.ms;

import omics.util.chem.Composition;
import omics.util.chem.IonType;
import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.Peak;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static omics.util.chem.IonType.*;
import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * <p>
 * Define Type of ions of Peptide Fragment in Mass spectrometer. For exact definition of the different ion types, see R.
 * S. Johnson..., Novel fragmentation process of peptides by collision-induced decomposition in a tandem mass
 * spectrometer: differentiation of leucine and isoleucine. Analytical Chemistry 59, 2621-2625 (1987).
 * <p>
 * fragmentation ion type.
 * <p>
 * https://github.com/HUPO-PSI/psi-ms-CV/blob/master/psi-ms.obo
 * <p>
 * MS:1002307
 * <p>
 * http://www.matrixscience.com/help/fragmentation_help.html
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 09 Jul 2019, 2:10 PM
 */
public class Ion
{
    public static double CHARGE_PARTICLE_MASS = PeriodicTable.PROTON_MASS;

    public static final Ion a = new Ion(PEPTIDE_FRAGMENT_ION, "a", FragmentType.FORWARD,
            Composition.parseComposition("C-1O-1"), "MS:1001229");
    public static final Ion a_H2O = new Ion(PEPTIDE_FRAGMENT_ION, "a-H2O", FragmentType.FORWARD,
            Composition.parseComposition("C-1H-2O-2"), "MS:1001234");
    public static final Ion a_NH3 = new Ion(PEPTIDE_FRAGMENT_ION, "a-NH3", FragmentType.FORWARD,
            Composition.parseComposition("C-1O-1H-3N-1"), "MS:1001235");

    public static final Ion b = new Ion(PEPTIDE_FRAGMENT_ION, "b", FragmentType.FORWARD,
            Composition.EMPTY, "MS:1001224");
    public static final Ion b_H2O = new Ion(PEPTIDE_FRAGMENT_ION, "b-H2O", FragmentType.FORWARD,
            Composition.parseComposition("H-2O-1"), "MS:1001222");
    public static final Ion b_NH3 = new Ion(PEPTIDE_FRAGMENT_ION, "b-NH3", FragmentType.FORWARD,
            Composition.parseComposition("H-3N-1"), "MS:1001232");
    public static final Ion b_HexNAc = new Ion(PEPTIDE_FRAGMENT_ION, "bᶰ", FragmentType.FORWARD,
            Composition.parseComposition("C8H13NO5"), null);

    public static final Ion c = new Ion(PEPTIDE_FRAGMENT_ION, "c", FragmentType.FORWARD,
            Composition.parseComposition("NH3"), "MS:1001231");
    public static final Ion c_H2O = new Ion(PEPTIDE_FRAGMENT_ION, "c-H2O", FragmentType.FORWARD,
            Composition.parseComposition("HO-1N"), "MS:1001515");
    public static final Ion c_NH3 = new Ion(PEPTIDE_FRAGMENT_ION, "c-NH3", FragmentType.FORWARD,
            Composition.EMPTY, "MS:1001516");

    public static final Ion d = new Ion(PEPTIDE_FRAGMENT_ION, "d", FragmentType.FORWARD,
            Composition.EMPTY, "MS:1001236"); // todo

    public static final Ion x = new Ion(PEPTIDE_FRAGMENT_ION, "x", FragmentType.REVERSE,
            Composition.parseComposition("CO2"), "MS:1001228");
    public static final Ion x_H2O = new Ion(PEPTIDE_FRAGMENT_ION, "x-H2O", FragmentType.REVERSE,
            Composition.parseComposition("CH-2O"), "MS:1001519");
    public static final Ion x_NH3 = new Ion(PEPTIDE_FRAGMENT_ION, "x-NH3", FragmentType.REVERSE,
            Composition.parseComposition("CH-3O2N-1"), "MS:1001520");

    public static final Ion y = new Ion(PEPTIDE_FRAGMENT_ION, "y", FragmentType.REVERSE,
            Composition.parseComposition("H2O"), "MS:1001220");
    public static final Ion y_H2O = new Ion(PEPTIDE_FRAGMENT_ION, "y-H2O", FragmentType.REVERSE,
            Composition.EMPTY, "MS:1001223");
    public static final Ion y_NH3 = new Ion(PEPTIDE_FRAGMENT_ION, "y-NH3", FragmentType.REVERSE,
            Composition.parseComposition("H-1ON-1"), "MS:1001233");
    public static final Ion y_HexNAc = new Ion(PEPTIDE_FRAGMENT_ION, "yᶰ", FragmentType.REVERSE,
            Composition.parseComposition("C8H15NO6"), null);

    public static final Ion v = new Ion(PEPTIDE_FRAGMENT_ION, "v", FragmentType.REVERSE,
            Composition.EMPTY, "MS:1001237"); // todo
    public static final Ion w = new Ion(PEPTIDE_FRAGMENT_ION, "w", FragmentType.REVERSE,
            Composition.EMPTY, "MS:1001238"); // todo

    public static final Ion z = new Ion(PEPTIDE_FRAGMENT_ION, "z", FragmentType.REVERSE,
            Composition.parseComposition("N-1OH-1"), "MS:1001230");
    public static final Ion z_H2O = new Ion(PEPTIDE_FRAGMENT_ION, "z-H2O", FragmentType.REVERSE,
            Composition.parseComposition("N-1H-3"), "MS:1001517");
    public static final Ion z_NH3 = new Ion(PEPTIDE_FRAGMENT_ION, "z-NH3", FragmentType.REVERSE,
            Composition.parseComposition("N-2H-4O"), "MS:1001518");

    public static final Ion z1 = new Ion(PEPTIDE_FRAGMENT_ION, "z+1", FragmentType.REVERSE,
            Composition.parseComposition(""), "MS:1001367"); // todo
    public static final Ion z2 = new Ion(PEPTIDE_FRAGMENT_ION, "z+2", FragmentType.REVERSE,
            Composition.parseComposition(""), "MS:1001368"); // todo

    public static final Ion p = new Ion(PRECURSOR_ION, "[M]", FragmentType.INTACT,
            Composition.parseComposition("H2O"), "MS:1001523");
    public static final Ion p_H2O = new Ion(PRECURSOR_ION, "[M-H2O]", FragmentType.INTACT,
            Composition.parseComposition(""), "MS:1001521");
    public static final Ion p_NH3 = new Ion(PRECURSOR_ION, "[M-NH3]", FragmentType.INTACT,
            Composition.parseComposition(""), "MS:1001522");

    public static final Ion im = new Ion(IMMONIUM_ION, "immonium ion", FragmentType.MONOMER,
            Composition.parseComposition("C-1O-1"), "MS:1001239");

    public static final Ion yb = new Ion(PEPTIDE_FRAGMENT_ION, "yb", FragmentType.INTERNAL,
            Composition.EMPTY, "MS:1001365");
    public static final Ion ya = new Ion(PEPTIDE_FRAGMENT_ION, "ya", FragmentType.INTERNAL,
            Composition.parseComposition("C-1O-1"), "MS:1001366");

    public static final Ion B = new Ion(GLYCAN_ION, "B", FragmentType.FORWARD);
    public static final Ion Y = new Ion(GLYCAN_ION, "Y", FragmentType.REVERSE);
    public static final Ion mz = new Ion(ELEMENTARY_ION, "m/z", FragmentType.UNKNOWN);

    private final IonType ionType;
    private final String name;
    private final FragmentType fragmentType;
    private final Composition composition;
    private final String accession;

    public Ion(IonType ionType, String name, FragmentType fragmentType, Composition composition, String accession)
    {
        this.ionType = ionType;
        this.name = name;
        this.fragmentType = fragmentType;
        this.composition = composition;
        this.accession = accession;
    }

    public Ion(IonType ionType, String name, FragmentType fragmentType)
    {
        this(ionType, name, fragmentType, Composition.EMPTY, null);
    }

    /**
     * @return {@link IonType} of this {@link Ion}
     */
    public IonType getIonType()
    {
        return ionType;
    }

    /**
     * @return name of this fragment ion type
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return {@link FragmentType}
     */
    public FragmentType getFragmentType()
    {
        return fragmentType;
    }

    /**
     * @return add composition added to the neutral ions.
     */
    public Composition getComposition()
    {
        return composition;
    }

    public double getDeltaMass()
    {
        return composition.getMolecularMass();
    }

    /**
     * @return PSM-MS accession, null for absent.
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * Return the m/z of this ion type
     *
     * @param neutralMass peptide mass (sum of amino acid residues)
     * @param charge      charge
     * @return m/z value
     */
    public double getMz(double neutralMass, int charge)
    {
        return calcMz(neutralMass + composition.getMolecularMass(), charge);
    }

    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ion ion = (Ion) o;
        return ionType == ion.ionType &&
                name.equals(ion.name) &&
                fragmentType == ion.fragmentType &&
                composition.equals(ion.composition);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(ionType, name, fragmentType, composition);
    }

    private static final Map<String, Ion> ionMap = new HashMap<>();

    static {
        registerIon(a);
        registerIon(a_H2O);
        registerIon(a_NH3);

        registerIon(b);
        registerIon(b_H2O);
        registerIon(b_NH3);

        registerIon(c);
        registerIon(c_H2O);
        registerIon(c_NH3);

        registerIon(x);
        registerIon(x_H2O);
        registerIon(x_NH3);

        registerIon(y);
        registerIon(y_H2O);
        registerIon(y_NH3);

        registerIon(z);
        registerIon(z_H2O);
        registerIon(z_NH3);

        registerIon(p);
        registerIon(p_H2O);
        registerIon(p_NH3);

        registerIon(im);
        registerIon(ya);
        registerIon(yb);
        registerIon(b_HexNAc);
        registerIon(y_HexNAc);
    }

    public static boolean registerIon(Ion ion)
    {
        if (ionMap.containsKey(ion.getName())) {
            return false;
        }
        ionMap.put(ion.getName(), ion);
        return true;
    }

    /**
     * Return the {@link Ion} of given name.
     *
     * @param name ion name
     * @return {@link Ion} with the name
     */
    public static Ion valueOf(String name)
    {
        return ionMap.get(name);
    }

    /**
     * Calculate the m/z from the molecular mass and its charge.
     *
     * @param aMass  molecular mass
     * @param charge charge
     * @return mz value
     */
    public static double calcMz(double aMass, int charge)
    {
        return calcMz(aMass, charge, CHARGE_PARTICLE_MASS);
    }

    /**
     * Calculate the m/z from the molecular mass and its charge
     *
     * @param mass           molecular mass
     * @param charge         charge
     * @param chargeAtomMass the mass of the particle with charge
     * @return m/z value
     */
    public static double calcMz(double mass, int charge, double chargeAtomMass)
    {
        checkArgument(charge > 0, "Charge should be greater than 0");

        return (mass / charge) + chargeAtomMass;
    }

    /**
     * Convert the peak of given charge to charge 1.
     *
     * @param mz     m/z value
     * @param charge peak charge
     * @return m/z value with charge 1.
     */
    public static double toZ1(double mz, int charge)
    {
        return mz * charge - CHARGE_PARTICLE_MASS * (charge - 1);
    }

    /**
     * Calculate the neutral molecular weight given the peak.
     *
     * @param peak a {@link Peak} object.
     * @return neutral mass of the peak.
     */
    public static double calcNeutralMass(Peak peak)
    {
        return calcNeutralMass(peak.getMz(), peak.getCharge());
    }

    /**
     * Calculate the neutral molecular weight given a m/z, charge.
     *
     * @param mz     the m/z
     * @param charge the charge
     * @return the neutral molecular weight
     */
    public static double calcNeutralMass(double mz, int charge)
    {
        return calcNeutralMass(mz, charge, CHARGE_PARTICLE_MASS);
    }

    /**
     * Calculate the neutral molecular weight given a m/z, charge.
     *
     * @param mz             the m/z
     * @param charge         the charge
     * @param chargeAtomMass mass of the atom with charge
     * @return the neutral molecular weight
     */
    public static double calcNeutralMass(double mz, int charge, double chargeAtomMass)
    {
        checkArgument(charge > 0, "Charge should be larger than 0");

        return (mz * charge) - chargeAtomMass * charge;
    }

    /**
     * Return possible peptide ion charge in MS/MS fragment
     *
     * @param precursorCharge precursor charge
     * @return charge array
     */
    public static int[] getPrecursorCharges(int precursorCharge)
    {
        int[] charges = new int[precursorCharge];
        for (int i = 0; i < precursorCharge; i++) {
            charges[i] = i + 1;
        }
        return charges;
    }

    /**
     * Calculate all possible fragment charges from precursor charge.
     *
     * @param precursorCharge precursor charge
     * @return possible fragment charges
     */
    public static int[] getFragmentCharges(int precursorCharge)
    {
        int[] charges;
        if (precursorCharge > 2) {
            charges = new int[precursorCharge - 1];
            for (int i = 1; i < precursorCharge; i++) {
                charges[i - 1] = i;
            }
        } else {
            charges = new int[precursorCharge];
            for (int i = 1; i <= precursorCharge; i++) {
                charges[i - 1] = i;
            }
        }

        return charges;
    }

    /**
     * Generate possible fragment charges for phosphoRS. {1,2} or {1}.
     *
     * @param precursorCharge precursor charge
     * @return fragment charges
     */
    public static int[] getMascotCharges(int precursorCharge)
    {
        return precursorCharge > 1 ? new int[]{1, 2} : new int[]{1};
    }
}
