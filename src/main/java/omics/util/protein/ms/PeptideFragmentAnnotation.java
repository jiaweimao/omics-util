package omics.util.protein.ms;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.protein.PeptideFragment;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Annotation for peptide fragment
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Aug 2020, 11:02 AM
 */
public abstract class PeptideFragmentAnnotation implements PeakAnnotation
{
    protected final Ion ion;
    protected final int charge;
    protected final PeptideFragment fragment;

    protected String symbol;
    protected double theoreticalMz;

    /**
     * Constructor
     *
     * @param ion    {@link Ion}
     * @param charge peak charge
     */
    public PeptideFragmentAnnotation(Ion ion, int charge)
    {
        this(ion, charge, -1);
    }

    /**
     * Constructor
     *
     * @param ion           {@link Ion}
     * @param charge        peak charge
     * @param theoreticalMz theoretical m/z
     */
    public PeptideFragmentAnnotation(Ion ion, int charge, double theoreticalMz)
    {
        this(ion, charge, null, theoreticalMz);
    }

    public PeptideFragmentAnnotation(Ion ion, int charge, PeptideFragment fragment, double theoreticalMz)
    {
        checkNotNull(ion);

        this.ion = ion;
        this.charge = charge;
        this.fragment = fragment;
        this.theoreticalMz = theoreticalMz;
    }

    @Override
    public Ion getIon()
    {
        return ion;
    }

    @Override
    public String getSymbol()
    {
        return symbol;
    }

    /**
     * @return charge of the peak
     */
    public int getCharge()
    {
        return charge;
    }

    public PeptideFragment getFragment()
    {
        return fragment;
    }

    /**
     * setter of the theoretical m/z
     *
     * @param theoreticalMz peptide fragment m/z
     */
    public void setTheoreticalMz(double theoreticalMz)
    {
        this.theoreticalMz = theoreticalMz;
    }

    /**
     * @return theoretical m/z
     */
    public double getTheoreticalMz()
    {
        return theoreticalMz;
    }
}
