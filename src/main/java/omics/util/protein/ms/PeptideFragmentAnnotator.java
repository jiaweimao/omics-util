package omics.util.protein.ms;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.sim.PeakListAligner;
import omics.util.ms.peaklist.sim.impl.PreferLargerIntensityDuplicateAligner;
import omics.util.protein.Peptide;

import java.util.Collection;

import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * This facade highly simplifies and hides the complexity of annotated a
 * <code>peakList</code> given a <code>peptide</code> as this process implies
 * peptide fragmentation, aligning peaks and reporting annotations.
 *
 * @author JiaweiMao
 * @version 1.1.1
 * @since 20 Mar 2017, 9:01 AM
 */
public class PeptideFragmentAnnotator
{
    private final PeptideFragmenter fragmenter;
    private final PeakListAligner aligner;

    /**
     * Constructor with default PeakListAligner.
     *
     * @param fragmenter a {@link PeptideFragmenter}.
     * @param tolerance  a {@link Tolerance}.
     */
    public PeptideFragmentAnnotator(PeptideFragmenter fragmenter, Tolerance tolerance)
    {
        this(fragmenter, new PreferLargerIntensityDuplicateAligner(tolerance));
    }

    public PeptideFragmentAnnotator(PeptideFragmenter fragmenter, PeakListAligner aligner)
    {
        checkNotNull(fragmenter);
        checkNotNull(aligner);

        this.fragmenter = fragmenter;
        this.aligner = aligner;
    }

    /**
     * Set {@link PeptideIon}
     *
     * @param peptideIonList {@link PeptideIon} collection
     */
    public void setPeptideIonList(Collection<PeptideIon> peptideIonList)
    {
        this.fragmenter.setPeptideIonList(peptideIonList);
    }

    /**
     * set {@link Tolerance} to match.
     *
     * @param tolerance {@link Tolerance} instance.
     */
    public void setTolerance(Tolerance tolerance)
    {
        this.aligner.setTolerance(tolerance);
    }

    /**
     * Annotate peaklist from peptide at precursor charge
     *
     * @param peaklist the peaklist to be annotated
     * @param peptide  the peptide to fragment and to annotate peaklist with
     * @param <S>      type of the {@link omics.util.ms.peaklist.PeakAnnotation}
     * @return annotated peak list.
     */
    public <S extends PeakList<PeptideFragmentAnnotation>> S annotate(S peaklist, Peptide peptide)
    {
        checkNotNull(peaklist.getPrecursor());

        return annotate(peaklist, peptide, peaklist.getPrecursor().getCharge());
    }

    /**
     * Annotate peaklist from peptide at specific charge
     *
     * @param peaklist        the peaklist to annotate
     * @param peptide         the peptide to fragment and to annotate peaklist with
     * @param precursorCharge the precursor charge
     */
    public <S extends PeakList<PeptideFragmentAnnotation>> S annotate(S peaklist, Peptide peptide, int precursorCharge)
    {
        // fragment peptide and get theoretical annotated spectrum
        PeptideSpectrum spectrum = fragmenter.fragment(peptide, precursorCharge);
        if (spectrum.isEmpty())
            throw new IllegalStateException("cannot generate fragments from peptide precursor " + peptide.toString()
                    + " at given charge " + precursorCharge);

        // make alignment and report matched peak annotations to peaklist
        Int2IntMap int2IntMap = aligner.align(spectrum, peaklist);
        for (int key : int2IntMap.keySet()) {
            peaklist.addAnnotations(int2IntMap.get(key), spectrum.getAnnotations(key));
        }

        return peaklist;
    }
}
