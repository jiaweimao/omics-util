package omics.util.protein.ms;

import omics.util.protein.AminoAcid;
import omics.util.protein.Peptide;
import omics.util.protein.PeptideFragment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * class of peptide fragment generator.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Aug 2020, 9:49 AM
 */
public final class PeptideFragmentGenerator
{
    /**
     * Generate fragments of peptide in forward direction
     *
     * @param peptide {@link Peptide} instance
     * @return list of forward {@link PeptideFragment}
     */
    public static List<PeptideFragment> generateForward(Peptide peptide)
    {
        List<PeptideFragment> fragmentList = new ArrayList<>(peptide.size() - 1);
        for (int i = 1; i < peptide.size(); i++) {
            fragmentList.add(peptide.createFragment(0, i, FragmentType.FORWARD));
        }
        return fragmentList;
    }

    /**
     * Generate fragments of peptide in forward direction
     *
     * @param peptide {@link Peptide} instance
     * @return list of reverse {@link PeptideFragment}
     */
    public static List<PeptideFragment> generateReverse(Peptide peptide)
    {
        List<PeptideFragment> fragmentList = new ArrayList<>(peptide.size() - 1);
        for (int i = peptide.size() - 1; i > 0; i--) {
            fragmentList.add(peptide.createFragment(i, peptide.size(), FragmentType.REVERSE));
        }
        return fragmentList;
    }

    /**
     * Generate immonium fragments of peptide.
     *
     * @param peptide peptide
     */
    public static List<PeptideFragment> generateImmonium(Peptide peptide)
    {
        Set<AminoAcid> acids = new HashSet<>();
        for (int i = 0; i < peptide.size(); i++)
            acids.add(peptide.getSymbol(i));

        List<PeptideFragment> fragmentList = new ArrayList<>(acids.size());
        for (AminoAcid acid : acids) {
            PeptideFragment fragment = new PeptideFragment(FragmentType.MONOMER, acid);
            fragmentList.add(fragment);
        }
        return fragmentList;
    }

    /**
     * Generate internal fragments of peptide.
     *
     * @param peptide peptide
     */
    public static List<PeptideFragment> generateInternal(Peptide peptide)
    {
        List<PeptideFragment> fragmentList = new ArrayList<>();
        for (int i = 1; i < peptide.size() - 2; i++) {
            for (int j = i + 2; j < peptide.size(); j++) {
                PeptideFragment fragment = peptide.createFragment(i, j, FragmentType.INTERNAL);
                fragmentList.add(fragment);
            }
        }
        return fragmentList;
    }

}
