package omics.util.protein.ms;

import omics.util.ms.peaklist.Precision;
import omics.util.protein.AminoAcid;
import omics.util.protein.Peptide;
import omics.util.protein.PeptideFragment;
import omics.util.utils.Pair;

import java.util.*;

/**
 * in-silico peptide spectrum generator.
 *
 * @author JiaweiMao
 * @version 2.3.0
 * @since 05 Feb 2018, 6:00 PM
 */
public class PeptideFragmenter
{
    /**
     * Return a {@link PeptideFragmenter} instance of given {@link PeptideIon} list.
     *
     * @param ionList list of {@link PeptideIon}
     */
    public static PeptideFragmenter getInstance(List<PeptideIon> ionList)
    {
        List<Pair<PeptideIon, Double>> pairList = new ArrayList<>(ionList.size());
        for (PeptideIon peptideIon : ionList) {
            pairList.add(Pair.create(peptideIon, 100.));
        }
        return new PeptideFragmenter(pairList);
    }

    private final Precision precision;
    private final List<Pair<PeptideIon, Double>> ionTypeList;

    public PeptideFragmenter()
    {
        this(new ArrayList<>());
    }

    /**
     * Construct with default {@link PeptideIon} list.
     *
     * @param ionTypeList {@link PeptideIon} list.
     */
    public PeptideFragmenter(List<Pair<PeptideIon, Double>> ionTypeList)
    {
        this(ionTypeList, Precision.FLOAT);
    }

    public PeptideFragmenter(List<Pair<PeptideIon, Double>> ionTypeList, Precision precision)
    {
        this.precision = precision;
        this.ionTypeList = ionTypeList;
    }

    /**
     * Set {@link PeptideIon} used to generate theoretical ions.
     *
     * @param peptideIon {@link PeptideIon} list.
     */
    public void setPeptideIonList(Collection<PeptideIon> peptideIon)
    {
        this.ionTypeList.clear();
        for (PeptideIon ion : peptideIon) {
            ionTypeList.add(Pair.create(ion, 100.));
        }
    }

    /**
     * create a new esi trap fragmenter, including b, y, -NH3, -H2O
     *
     * @return PeptideFragmenter
     */
    public static PeptideFragmenter EsiTrapFragmenter()
    {
        List<Pair<PeptideIon, Double>> ionTypes = new ArrayList<>();
        ionTypes.add(Pair.create(PeptideIon.b(1), 100.0));
        ionTypes.add(Pair.create(PeptideIon.b(2), 100.0));
        ionTypes.add(Pair.create(PeptideIon.y(1), 100.0));
        ionTypes.add(Pair.create(PeptideIon.y(2), 100.0));
        ionTypes.add(Pair.create(PeptideIon.b_H2O(1), 50.0));
        ionTypes.add(Pair.create(PeptideIon.b_H2O(2), 50.0));
        ionTypes.add(Pair.create(PeptideIon.b_NH3(1), 50.0));
        ionTypes.add(Pair.create(PeptideIon.b_NH3(2), 50.0));
        ionTypes.add(Pair.create(PeptideIon.y_H2O(1), 50.0));
        ionTypes.add(Pair.create(PeptideIon.y_H2O(2), 50.0));
        ionTypes.add(Pair.create(PeptideIon.y_NH3(1), 50.0));
        ionTypes.add(Pair.create(PeptideIon.y_NH3(2), 50.0));
        ionTypes.add(Pair.create(PeptideIon.p(1), 100.0));
        ionTypes.add(Pair.create(PeptideIon.p(2), 100.0));

        return new PeptideFragmenter(ionTypes);
    }

    public PeptideSpectrum fragment(Peptide peptide, int precursorCharge)
    {
        return this.fragment(peptide, precursorCharge, ionTypeList);
    }

    public PeptideSpectrum fragment(Peptide peptide, int precursorCharge, List<Pair<PeptideIon, Double>> ionTypeList)
    {
        Set<FragmentType> fragmentTypes = new HashSet<>();
        for (Pair<PeptideIon, Double> ionType : ionTypeList) {
            fragmentTypes.add(ionType.getFirst().getFragmentType());
        }

        PeptideSpectrum spectrum = new PeptideSpectrum(peptide, precursorCharge, precision);

        if (fragmentTypes.contains(FragmentType.FORWARD)) {
            List<PeptideFragment> fragments = PeptideFragmentGenerator.generateForward(peptide);
            for (PeptideFragment fragment : fragments) {
                double monoMass = fragment.calculateMonomerMassSum();
                for (Pair<PeptideIon, Double> ionType : ionTypeList) {
                    PeptideIon peptideIon = ionType.getFirst();
                    if (peptideIon.getFragmentType() == FragmentType.FORWARD && peptideIon.test(fragment)) {
                        double mz = peptideIon.calcMz(monoMass);

                        BackboneAnnotation annotation = new BackboneAnnotation(peptideIon, fragment, mz);
                        spectrum.add(mz, ionType.getSecond(), annotation);
                    }
                }
            }

//            generatePeaks(fragments, spectrum, ionTypeList);
        }

        if (fragmentTypes.contains(FragmentType.REVERSE)) {
            List<PeptideFragment> fragments = PeptideFragmentGenerator.generateReverse(peptide);
            for (PeptideFragment fragment : fragments) {
                double monoMass = fragment.calculateMonomerMassSum();
                for (Pair<PeptideIon, Double> ionType : ionTypeList) {
                    PeptideIon peptideIon = ionType.getFirst();
                    if (peptideIon.getFragmentType() == FragmentType.REVERSE && peptideIon.test(fragment)) {
                        double mz = peptideIon.calcMz(monoMass);

                        BackboneAnnotation annotation = new BackboneAnnotation(peptideIon, fragment, mz);
                        spectrum.add(mz, ionType.getSecond(), annotation);
                    }
                }
            }
//            generatePeaks(fragments, spectrum, ionTypeList);
        }

        if (fragmentTypes.contains(FragmentType.MONOMER)) {
            Set<AminoAcid> acids = new HashSet<>();
            for (int i = 0; i < peptide.size(); i++) {
                acids.add(peptide.getSymbol(i));
            }

            for (AminoAcid acid : acids) {
                PeptideFragment fragment = new PeptideFragment(FragmentType.MONOMER, acid);
                double monoMass = fragment.calculateMonomerMassSum();

                for (Pair<PeptideIon, Double> ionType : ionTypeList) {
                    PeptideIon peptideIon = ionType.getFirst();
                    if (peptideIon.getFragmentType() == FragmentType.MONOMER && peptideIon.test(fragment)) {
                        double mz = peptideIon.calcMz(monoMass);
                        ImmoniumAnnotation annotation = new ImmoniumAnnotation(peptideIon.getCharge(), fragment, mz);
                        spectrum.add(mz, ionType.getSecond(), annotation);
                    }
                }
            }
        }

        if (fragmentTypes.contains(FragmentType.INTACT)) {
            PeptideFragment fragment = peptide.createFragment(0, peptide.size(), FragmentType.INTACT);
            double monoMass = fragment.calculateMonomerMassSum();
            for (Pair<PeptideIon, Double> ionType : ionTypeList) {
                PeptideIon peptideIon = ionType.getFirst();
                if (peptideIon.getFragmentType() == FragmentType.INTACT && peptideIon.test(fragment)) {
                    double mz = peptideIon.calcMz(monoMass);
                    PrecursorAnnotation annotation = new PrecursorAnnotation(peptideIon.getIon(),
                            peptideIon.getCharge(), mz, peptideIon.getNeutralLosses());
                    spectrum.add(mz, ionType.getSecond(), annotation);
                }
            }
        }

        if (fragmentTypes.contains(FragmentType.INTERNAL)) {
            for (int i = 1; i < peptide.size() - 2; i++) {
                for (int j = i + 2; j < peptide.size(); j++) {
                    PeptideFragment fragment = peptide.createFragment(i, j, FragmentType.INTERNAL);
                    double monoMass = fragment.calculateMonomerMassSum();

                    for (Pair<PeptideIon, Double> ionType : ionTypeList) {
                        PeptideIon peptideIon = ionType.getFirst();
                        if (peptideIon.getFragmentType() == FragmentType.INTERNAL && peptideIon.test(fragment)) {
                            double mz = peptideIon.calcMz(monoMass);
                            InternalFragmentAnnotation annotation = new InternalFragmentAnnotation(peptideIon.getIon(), peptideIon.getCharge(), i, j);
                            spectrum.add(mz, ionType.getSecond(), annotation);
                        }
                    }
                }
            }
        }

        spectrum.trimToSize();

        return spectrum;
    }

//    private void generatePeaks(List<PeptideFragment> fragments, PeptideSpectrum spectrum, List<Pair<PeptideIon, Double>> ionTypeList)
//    {
//        for (PeptideFragment fragment : fragments) {
//            double monoMass = fragment.calculateMonomerMassSum();
//
//            for (Pair<PeptideIon, Double> ionType : ionTypeList) {
//                PeptideIon peptideIon = ionType.getFirst();
//                if (peptideIon.getFragmentType() == fragment.getFragmentType() && peptideIon.test(fragment)) {
//                    double mz = peptideIon.calcMz(monoMass);
//                    PeptideFragAnnotation annotation = new PeptideFragAnnotation(fragment, peptideIon, peptideIon.getCharge());
//                    spectrum.add(mz, ionType.getSecond(), annotation);
//                }
//            }
//        }
//    }
}
