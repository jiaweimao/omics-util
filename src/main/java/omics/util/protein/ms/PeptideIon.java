package omics.util.protein.ms;

import omics.util.UnsupportedTypeException;
import omics.util.chem.Composition;
import omics.util.chem.IonType;
import omics.util.ms.Dissociation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.protein.PeptideFragment;
import omics.util.protein.mod.NeutralLoss;

import java.util.*;
import java.util.function.Predicate;

/**
 * This class define ion types used in database search.
 *
 * @author JiaweiMao
 * @version 2.9.2
 * @since 05 Feb 2018, 3:24 PM
 */
public class PeptideIon implements Predicate<PeptideFragment>
{
    public static final PeptideIon NOISE = new PeptideIon("Noise", Ion.mz, 1, Composition.EMPTY);

    public static ArrayList<PeptideIon> getPeptideIons(Dissociation dissociation, int maxCharge,
                                                       boolean includePhosNL, boolean includeHexNAc)
    {
        return getPeptideIons(dissociation, maxCharge, false, 0.01, includePhosNL, includeHexNAc);
    }

    /**
     * Generate possible {@link PeptideIon}
     *
     * @param ionList       type of {@link Ion}
     * @param neutralLosses {@link PeptideNeutralLoss} consider
     * @param minCharge     minimum charge
     * @param maxCharge     maximum charge
     * @param tolerance     {@link Tolerance} used to duplicate, null if do not duplicate.
     * @return possible {@link PeptideIon} list
     */
    public static List<PeptideIon> getPeptideIons(List<Ion> ionList, List<PeptideNeutralLoss> neutralLosses,
                                                  int minCharge, int maxCharge, Tolerance tolerance)
    {
        List<PeptideIon> peptideIonList = new ArrayList<>();
        for (Ion ion : ionList) {
            for (int i = minCharge; i <= maxCharge; i++) {
                peptideIonList.add(new PeptideIon(ion, i));
            }
            for (PeptideNeutralLoss neutralLoss : neutralLosses) {
                for (int i = minCharge; i <= maxCharge; i++) {
                    PeptideIon peptideIon = new PeptideIon(ion, neutralLoss, i);
                    peptideIonList.add(peptideIon);
                }
            }
        }

        peptideIonList.sort(Comparator.comparingInt(PeptideIon::getCharge)
                .thenComparingDouble(PeptideIon::getDeltaMz));
        if (tolerance != null) {
            LinkedList<PeptideIon> ions = new LinkedList<>();
            ions.add(peptideIonList.get(0));
            for (int i = 1; i < peptideIonList.size(); i++) {
                PeptideIon preIon = peptideIonList.get(i - 1);
                PeptideIon curIon = peptideIonList.get(i);
                if (tolerance.inTolerance(preIon.getDeltaMz(), curIon.getDeltaMz())
                        && preIon.getCharge() == curIon.getCharge()
                        && preIon.getFragmentType() == curIon.getFragmentType()) {
                    if (curIon.getName().length() < preIon.getName().length()) {
                        ions.removeLast();
                        ions.add(curIon);
                    }
                } else {
                    ions.add(curIon);
                }
            }
            return ions;
        } else {
            return peptideIonList;
        }
    }

    /**
     * Return possible {@link PeptideIon} of given {@link Dissociation}
     *
     * @param dissociation {@link Dissociation}
     * @param maxCharge    allowed maximum charge
     * @param removeDup    true if remove duplicate ions
     */
    public static ArrayList<PeptideIon> getPeptideIons(Dissociation dissociation, int maxCharge,
                                                       boolean removeDup, double deltaMz, boolean includePhosNL, boolean includeHexNAc)
    {
        ArrayList<PeptideIon> list = new ArrayList<>();
        List<Ion> ionList = new ArrayList<>();
        PeptideNeutralLoss nh3 = PeptideNeutralLoss.NH3();
        PeptideNeutralLoss[] neutralLosses = new PeptideNeutralLoss[]{
                null,
                PeptideNeutralLoss.H2O(), nh3,
                PeptideNeutralLoss.of(NeutralLoss.of("-2H2O", Composition.parseComposition("H4O2"))),
                PeptideNeutralLoss.of(NeutralLoss.of("-H2O-NH3", Composition.parseComposition("H5O1N1"))),
                PeptideNeutralLoss.of(NeutralLoss.of("-2NH3", Composition.parseComposition("N2H6"))),
                PeptideNeutralLoss.of(NeutralLoss.of("[C13]", Composition.parseComposition("C[13]-1C"))),
                PeptideNeutralLoss.of(NeutralLoss.of("[C14]", Composition.parseComposition("C[14]-1C"))),
                PeptideNeutralLoss.of(NeutralLoss.of("-H", Composition.parseComposition("H")))
        };

        if (dissociation == Dissociation.HCD || dissociation == Dissociation.CID) {
            ionList.addAll(Arrays.asList(Ion.a, Ion.b, Ion.x, Ion.y));
        } else if (dissociation == Dissociation.ETD || dissociation == Dissociation.ECD) {
            ionList.addAll(Arrays.asList(Ion.c, Ion.y, Ion.z));
        } else {
            ionList.addAll(Arrays.asList(Ion.a, Ion.b, Ion.c, Ion.x, Ion.y, Ion.z));
        }
        if (includeHexNAc) {
            ionList.add(Ion.b_HexNAc);
            ionList.add(Ion.y_HexNAc);
        }

        List<PeptideNeutralLoss> more = new ArrayList<>();
        if (includePhosNL) {
            more.add(PeptideNeutralLoss.H3PO4());
        }

        for (Ion ion : ionList) {
            for (PeptideNeutralLoss neutralLoss : neutralLosses) {
                if (ion == Ion.c && neutralLoss == nh3) // same as b
                    continue;

                for (int i = 1; i <= maxCharge; i++) {
                    PeptideIon peptideIon = new PeptideIon(ion, neutralLoss, i);
                    list.add(peptideIon);
                    if (!more.isEmpty()) {
                        for (PeptideNeutralLoss nl : more) {
                            if (neutralLoss == null) {
                                list.add(new PeptideIon(ion, nl, i));
                            } else {
                                list.add(new PeptideIon(ion, PeptideNeutralLoss.of(neutralLoss, nl), i));
                            }
                        }
                    }
                }
            }
        }

        list.sort(Comparator.comparingInt(PeptideIon::getCharge).thenComparingDouble(PeptideIon::getDeltaMz));

        if (removeDup) {
            LinkedList<PeptideIon> ions = new LinkedList<>();
            ions.add(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                PeptideIon preIon = list.get(i - 1);
                PeptideIon curIon = list.get(i);
                if ((curIon.getDeltaMz() - preIon.getDeltaMz() < deltaMz)
                        && curIon.getCharge() == preIon.getCharge()
                        && curIon.getFragmentType() == preIon.getFragmentType()) {
                    if (curIon.getName().length() < preIon.getName().length()) {
                        ions.removeLast();
                        ions.add(curIon);
                    }
                } else {
                    ions.add(curIon);
                }
            }
            return new ArrayList<>(ions);
        } else {
            return list;
        }
    }

    /**
     * get possible ion types of given max charge
     *
     * @param maxCharge allowed maximum charge
     * @param removeDup true of remove duplicate {@link PeptideIon} with same mass
     */
    public static ArrayList<PeptideIon> getPeptideIons(int maxCharge, boolean removeDup)
    {
        return getPeptideIons(maxCharge, removeDup, 0.01, false);
    }

    public static ArrayList<PeptideIon> getPeptideIons(int maxCharge, boolean removeDup, boolean includePhosNL)
    {
        return getPeptideIons(maxCharge, removeDup, 0.01, includePhosNL);
    }

    /**
     * Return all possible ions for given information
     *
     * @param maxCharge     allowed maximum ion charge
     * @param removeDup     true remove peptide ions with similar mass
     * @param tolMz         tolerance value to detect peptide ions with similar mass
     * @param includePhosNL true if include phospho neltral loss
     */
    public static ArrayList<PeptideIon> getPeptideIons(int maxCharge, boolean removeDup, double tolMz, boolean includePhosNL)
    {
        ArrayList<PeptideIon> list = new ArrayList<>();
        Ion[] base = new Ion[]{Ion.a, Ion.b, Ion.c, Ion.x, Ion.y, Ion.z};
        PeptideNeutralLoss nh3 = PeptideNeutralLoss.NH3();
        PeptideNeutralLoss[] neutralLosses = new PeptideNeutralLoss[]{
                null,
                PeptideNeutralLoss.H2O(), nh3,
                PeptideNeutralLoss.of(NeutralLoss.of("-2H2O", Composition.parseComposition("H4O2"))),
                PeptideNeutralLoss.of(NeutralLoss.of("-H2O-NH3", Composition.parseComposition("H5O1N1"))),
                PeptideNeutralLoss.of(NeutralLoss.of("-2NH3", Composition.parseComposition("N2H6"))),
                PeptideNeutralLoss.of(NeutralLoss.of("[C13]", Composition.parseComposition("C[13]-1C"))),
                PeptideNeutralLoss.of(NeutralLoss.of("[C14]", Composition.parseComposition("C[14]-1C"))),
                PeptideNeutralLoss.of(NeutralLoss.of("-H", Composition.parseComposition("H")))
        };

        List<PeptideNeutralLoss> more = new ArrayList<>();
        if (includePhosNL) {
            more.add(PeptideNeutralLoss.H3PO4());
        }

        for (Ion ion : base) {
            for (PeptideNeutralLoss neutralLoss : neutralLosses) {
                if (ion == Ion.c && neutralLoss == nh3) // same as b
                    continue;

                for (int i = 1; i <= maxCharge; i++) {
                    PeptideIon peptideIon = new PeptideIon(ion, neutralLoss, i);
                    list.add(peptideIon);
                    if (!more.isEmpty()) {
                        for (PeptideNeutralLoss nl : more) {
                            if (neutralLoss == null) {
                                list.add(new PeptideIon(ion, nl, i));
                            } else {
                                list.add(new PeptideIon(ion, PeptideNeutralLoss.of(neutralLoss, nl), i));
                            }
                        }
                    }
                }
            }
        }

        list.sort(Comparator.comparingInt(PeptideIon::getCharge).thenComparingDouble(PeptideIon::getDeltaMz));

        if (removeDup) {
            LinkedList<PeptideIon> ions = new LinkedList<>();
            ions.add(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                PeptideIon preIon = list.get(i - 1);
                PeptideIon curIon = list.get(i);
                if ((curIon.getDeltaMz() - preIon.getDeltaMz() < tolMz)
                        && curIon.getCharge() == preIon.getCharge()
                        && curIon.getFragmentType() == preIon.getFragmentType()) {
                    if (curIon.getName().length() < preIon.getName().length()) {
                        ions.removeLast();
                        ions.add(curIon);
                    }
                } else {
                    ions.add(curIon);
                }
            }
            return new ArrayList<>(ions);
        } else {
            return list;
        }
    }

    /**
     * Create a b-ion.
     *
     * @param charge charge
     */
    public static PeptideIon b(int charge)
    {
        return new PeptideIon(Ion.b, charge);
    }

    /**
     * Create a b-ion with neutral loss H2O.
     */
    public static PeptideIon b_H2O(int charge)
    {
        return new PeptideIon(Ion.b, PeptideNeutralLoss.H2O(), charge);
    }

    public static PeptideIon b_NH3(int charge)
    {
        return new PeptideIon(Ion.b, PeptideNeutralLoss.NH3(), charge);
    }

    public static PeptideIon y(int charge)
    {
        return new PeptideIon(Ion.y, charge);
    }

    public static PeptideIon y_H2O(int charge)
    {
        return new PeptideIon(Ion.y, PeptideNeutralLoss.H2O(), charge);
    }

    public static PeptideIon y_NH3(int charge)
    {
        return new PeptideIon(Ion.y, PeptideNeutralLoss.NH3(), charge);
    }

    public static PeptideIon ya(int charge)
    {
        return new PeptideIon(Ion.ya, charge);
    }

    public static PeptideIon yb(int charge)
    {
        return new PeptideIon(Ion.yb, charge);
    }

    public static PeptideIon c(int charge)
    {
        return new PeptideIon(Ion.c, charge);
    }

    public static PeptideIon x(int charge)
    {
        return new PeptideIon(Ion.x, charge);
    }

    public static PeptideIon z(int charge)
    {
        return new PeptideIon(Ion.z, charge);
    }

    public static PeptideIon a(int charge)
    {
        return new PeptideIon(Ion.a, charge);
    }

    public static PeptideIon p(int charge)
    {
        return new PeptideIon(Ion.p, charge);
    }

    public static PeptideIon immonium()
    {
        return new PeptideIon(Ion.im, 1);
    }

    private final Ion ion;
    private final PeptideNeutralLoss neutralLoss;
    private final int charge;
    private final Composition composition;
    private final double deltaMz;
    private final String name;

    /**
     * Constructor with given {@link Composition}, this is used to case where the {@link Composition}
     * of the {@link Ion} is not defined.
     *
     * @param name   ion name
     * @param ion    base {@link Ion}
     * @param charge charge
     */
    public PeptideIon(String name, Ion ion, int charge, Composition composition)
    {
        this.name = name;
        this.ion = ion;
        this.neutralLoss = null;
        this.composition = composition;
        this.charge = charge;
        this.deltaMz = Ion.calcMz(composition.getMolecularMass(), charge);
    }

    public PeptideIon(Ion ion, int charge)
    {
        this(ion, null, charge);
    }

    /**
     * Create a {@link PeptideIon}.
     *
     * @param ion         {@link Ion} of this PeptideFragment.
     * @param neutralLoss Neutral loss of this Peptide Fragment, the mass is positive
     */
    public PeptideIon(Ion ion, PeptideNeutralLoss neutralLoss, int charge)
    {
        IonType type = ion.getIonType();
        if (type == IonType.REPORTER_ION || type == IonType.RELATED_ION
                || type == IonType.TAG_FRAGMENT_ION || type == IonType.UNKNOWN) {
            throw new UnsupportedTypeException(type.name() + " is not supported.");
        }

        this.ion = ion;
        this.neutralLoss = neutralLoss;
        this.charge = charge;

        this.composition = neutralLoss == null ? ion.getComposition() :
                ion.getComposition().subtract(neutralLoss.getComposition());
        this.deltaMz = Ion.calcMz(composition.getMolecularMass(), charge);

        StringBuilder nameBuilder = new StringBuilder(ion.getName());
        if (neutralLoss != null)
            nameBuilder.append(neutralLoss.getName());
        if (charge != 1) {
            nameBuilder.append("(").append(charge).append(")");
        }
        this.name = nameBuilder.toString();
    }

    /**
     * @return name of the ion.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Return the m/z of given neutral residues sum mass.
     *
     * @param neutralMass sum of amino acid residue masses.
     */
    public double calcMz(double neutralMass)
    {
        return neutralMass / charge + deltaMz;
    }

    /**
     * Return the neutral peptide fragment mass
     *
     * @param mz m/z value
     * @return peptide fragment mass.
     */
    public double calcMass(double mz)
    {
        return (mz - deltaMz) * charge;
    }

    /**
     * @return delta m/z value of this {@link PeptideIon}, return {@link Double#NaN} for ion type with unknown mass, such as
     * Oxonium ions.
     */
    public double getDeltaMz()
    {
        return deltaMz;
    }

    /**
     * @return {@link Composition} of this {@link PeptideIon}, charge is not included.
     */
    public Composition getComposition()
    {
        return composition;
    }

    /**
     * @return charge of this {@link PeptideIon}
     */
    public int getCharge()
    {
        return charge;
    }

    /**
     * @return {@link Ion} of this Fragment.
     */
    public Ion getIon()
    {
        return ion;
    }

    /**
     * @return {@link FragmentType} of this fragment ion type
     */
    public FragmentType getFragmentType()
    {
        return ion.getFragmentType();
    }

    /**
     * @return true if this {@link PeptideIon} is a {@link FragmentType#FORWARD} ion.
     */
    public boolean isForwardIon()
    {
        return ion.getFragmentType() == FragmentType.FORWARD;
    }

    /**
     * @return neutral loss of this PeptideIonType, null for absent.
     */
    public PeptideNeutralLoss getPeptideNeutralLoss()
    {
        return neutralLoss;
    }

    /**
     * @return NeutralLoss of this IonType. null for none.
     */
    public NeutralLoss[] getNeutralLosses()
    {
        if (neutralLoss != null)
            return neutralLoss.getNeutralLosses();
        return null;
    }

    /**
     * @return true if this PeptideFragmentIonType contain NeutralLoss.
     */
    public boolean hasNeutralLoss()
    {
        return neutralLoss != null;
    }

    /**
     * Test if the {@link PeptideFragment} match the {@link PeptideNeutralLoss} predicate.
     *
     * @param fragment {@link PeptideFragment} instance
     */
    @Override
    public boolean test(PeptideFragment fragment)
    {
        return neutralLoss == null || neutralLoss.test(fragment);
    }

    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeptideIon that = (PeptideIon) o;
        return charge == that.charge &&
                ion == that.ion &&
                composition.equals(that.composition) &&
                name.equals(that.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(ion, charge, composition, name);
    }
}
