package omics.util.protein.ms;

import com.google.common.collect.Sets;
import omics.util.chem.Composition;
import omics.util.chem.Weighable;
import omics.util.protein.AminoAcid;
import omics.util.protein.PeptideFragment;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.NeutralLoss;
import omics.util.protein.mod.PTM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Peptide Fragment with Neutral Loss.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 05 Feb 2018, 4:53 PM
 */
public class PeptideNeutralLoss implements Predicate<PeptideFragment>, Weighable
{
    public static Predicate<PeptideFragment> H2OPredicate()
    {
        return new ContainAAPredicate(Sets.newHashSet(AminoAcid.S, AminoAcid.T, AminoAcid.D, AminoAcid.E));
    }

    public static Predicate<PeptideFragment> NH3Predicate()
    {
        return new ContainAAPredicate(Sets.newHashSet(AminoAcid.Q, AminoAcid.K, AminoAcid.R, AminoAcid.N));
    }

    public static Predicate<PeptideFragment> H3PO4Predicate()
    {
        return new ContainModifiedAAPredicate(Sets.newHashSet(AminoAcid.S, AminoAcid.T),
                Collections.singleton(PTM.Phospho()), ModAttachment.all);
    }

    public static final Predicate<PeptideFragment> AcceptAllPredicate = peptideFragment -> true;

    /**
     * @return {@link PeptideNeutralLoss} of H2O.
     */
    public static PeptideNeutralLoss H2O()
    {
        return PeptideNeutralLoss.of(H2OPredicate(), NeutralLoss.H2O_LOSS);
    }

    public static PeptideNeutralLoss NH3()
    {
        return PeptideNeutralLoss.of(NH3Predicate(), NeutralLoss.NH3_LOSS);
    }

    public static PeptideNeutralLoss H3PO4()
    {
        return PeptideNeutralLoss.of(H3PO4Predicate(), NeutralLoss.H3PO4_LOSS);
    }

    public static PeptideNeutralLoss H3PO4_NH3()
    {
        return PeptideNeutralLoss.of(NH3Predicate().and(H3PO4Predicate()), NeutralLoss.NH3_LOSS, NeutralLoss.H3PO4_LOSS);
    }

    public static PeptideNeutralLoss H3PO4_H2O()
    {
        return PeptideNeutralLoss.of(H2OPredicate().and(H3PO4Predicate()), NeutralLoss.H2O_LOSS, NeutralLoss.H3PO4_LOSS);
    }

    public static PeptideNeutralLoss NH3_H2O()
    {
        return PeptideNeutralLoss.of(H2OPredicate().and(NH3Predicate()), NeutralLoss.H2O_LOSS, NeutralLoss.NH3_LOSS);
    }

    /**
     * Create a {@link PeptideNeutralLoss} from the combination of other {@link PeptideNeutralLoss}es.
     *
     * @param peptideNeutralLosses {@link PeptideNeutralLoss} array
     * @return {@link PeptideNeutralLoss} instance
     */
    public static PeptideNeutralLoss of(PeptideNeutralLoss... peptideNeutralLosses)
    {
        checkNotNull(peptideNeutralLosses);
        checkArgument(peptideNeutralLosses.length > 0);

        Composition.Builder builder = new Composition.Builder();
        List<NeutralLoss> neutralLossList = new ArrayList<>();
        Predicate<PeptideFragment> predicate = AcceptAllPredicate;
        for (int i = 0; i < peptideNeutralLosses.length; i++) {
            PeptideNeutralLoss peptideNeutralLoss = peptideNeutralLosses[i];
            neutralLossList.addAll(Arrays.asList(peptideNeutralLoss.getNeutralLosses()));
            builder.add(peptideNeutralLoss.getComposition());
            if (i == 0) {
                predicate = peptideNeutralLoss.predicate;
            } else {
                predicate = peptideNeutralLoss.predicate.and(predicate);
            }
        }

        NeutralLoss[] neutralLosses = neutralLossList.toArray(new NeutralLoss[0]);
        Composition composition = builder.build();
        String name = NeutralLoss.getFormattedName(neutralLosses);

        return new PeptideNeutralLoss(neutralLosses, predicate, composition, name);
    }

    /**
     * Create a {@link PeptideNeutralLoss}.
     *
     * @param predicate     {@link Predicate} to test {@link PeptideFragment}
     * @param neutralLosses {@link NeutralLoss} array
     */
    public static PeptideNeutralLoss of(Predicate<PeptideFragment> predicate, NeutralLoss... neutralLosses)
    {
        checkNotNull(predicate);
        checkNotNull(neutralLosses);
        checkArgument(neutralLosses.length > 0);

        String name = NeutralLoss.getFormattedName(neutralLosses);
        Composition.Builder builder = new Composition.Builder();
        for (NeutralLoss neutralLoss : neutralLosses) {
            builder.add(neutralLoss.getComposition());
        }
        Composition composition = builder.build();

        return new PeptideNeutralLoss(neutralLosses, predicate, composition, name);
    }

    /**
     * Create a new {@link PeptideNeutralLoss}
     *
     * @param neutralLoss {@link NeutralLoss}
     * @param predicate   {@link Predicate} to test {@link PeptideFragment}
     */
    public static PeptideNeutralLoss of(Predicate<PeptideFragment> predicate, NeutralLoss neutralLoss)
    {
        return of(predicate, new NeutralLoss[]{neutralLoss});
    }

    /**
     * Create a {@link PeptideNeutralLoss} from a {@link NeutralLoss} instance, which
     * default pass all conditions.
     *
     * @param neutralLoss {@link NeutralLoss} instance.
     * @return a {@link PeptideNeutralLoss} instance.
     */
    public static PeptideNeutralLoss of(NeutralLoss neutralLoss)
    {
        return of(AcceptAllPredicate, neutralLoss);
    }

    private final NeutralLoss[] neutralLosses;
    private final Predicate<PeptideFragment> predicate;
    private final Composition composition;
    private final String name;

    /**
     * Constructor.
     *
     * @param neutralLosses {@link NeutralLoss} array, which means combinations of {@link NeutralLoss}es
     * @param predicate     {@link Predicate} to test if given {@link PeptideFragment} could occur the neutral loss
     * @param composition   sum {@link Composition} of all neutral loss
     * @param name          name of the neutral loss
     */
    public PeptideNeutralLoss(NeutralLoss[] neutralLosses, Predicate<PeptideFragment> predicate,
                              Composition composition, String name)
    {
        checkNotNull(neutralLosses);

        this.neutralLosses = neutralLosses;
        this.predicate = predicate;
        this.composition = composition;
        this.name = name;
    }

    /**
     * @return readable name of this neutral loss.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return {@link Composition} of this neutral loss.
     */
    public Composition getComposition()
    {
        return composition;
    }

    /**
     * @return {@link NeutralLoss} of this.
     */
    public NeutralLoss[] getNeutralLosses()
    {
        return neutralLosses;
    }

    @Override
    public double getMolecularMass()
    {
        return composition.getMolecularMass();
    }

    @Override
    public boolean test(PeptideFragment fragment)
    {
        return predicate.test(fragment);
    }
}
