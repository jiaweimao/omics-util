package omics.util.protein.ms;

import com.google.common.collect.ImmutableList;
import omics.util.ms.Spectrum;
import omics.util.ms.peaklist.*;
import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import omics.util.protein.Peptide;

import java.util.*;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * Represents peptide spectrum match, in silica fragment of Peptide.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 20 Mar 2017, 1:11 PM
 */
public class PeptideSpectrum extends Spectrum<PeptideFragmentAnnotation>
{
    private final Peptide peptide;
    private final int charge;
    private final Set<String> proteinAccessionNumbers = new LinkedHashSet<>();

    public PeptideSpectrum(Peptide peptide, int charge)
    {
        this(peptide, charge, 0, Precision.DOUBLE);
    }

    public PeptideSpectrum(Peptide peptide, int charge, Precision precision)
    {
        this(peptide, charge, 0, precision);
    }

    public PeptideSpectrum(Peptide peptide, int charge, int initialCapacity, Precision precision)
    {
        super(initialCapacity, precision);

        checkNotNull(peptide);
        checkArgument(charge != 0);

        this.peptide = peptide;
        this.charge = charge;

        setMsLevel(2);
    }

    public PeptideSpectrum(Peptide peptide, int charge, int initialCapacity, double constantIntensity,
                           Precision precision)
    {
        super(initialCapacity, constantIntensity, precision);

        checkNotNull(peptide);
        this.peptide = peptide;
        this.charge = charge;

        setMsLevel(2);
    }

    protected PeptideSpectrum(PeptideSpectrum src, PeakProcessor<PeptideFragmentAnnotation, PeptideFragmentAnnotation> peakProcessor)
    {
        super(src, peakProcessor);

        peptide = src.peptide;
        charge = src.charge;
    }

    protected PeptideSpectrum(PeptideSpectrum src, PeakProcessorChain<PeptideFragmentAnnotation> peakProcessorChain)
    {
        super(src, peakProcessorChain);

        peptide = src.peptide;
        charge = src.charge;
    }

    protected PeptideSpectrum(PeptideSpectrum src, SpectrumProcessor<PeptideFragmentAnnotation, PeptideFragmentAnnotation> spectrumProcessor)
    {
        super(src, spectrumProcessor);

        peptide = src.peptide;
        charge = src.charge;
    }

    public PeptideSpectrum(Spectrum<PeptideFragmentAnnotation> src, Peptide peptide, int charge)
    {
        super(src, new IdentityPeakProcessor<>());
        this.peptide = peptide;
        this.charge = charge;

        setMsLevel(2);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (!(o instanceof PeptideSpectrum))
            return false;
        if (!super.equals(o))
            return false;

        PeptideSpectrum that = (PeptideSpectrum) o;

        return !(peptide != null ? !peptide.equals(that.peptide) : that.peptide != null) && charge == that.charge;
    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        result = 31 * result + (peptide != null ? peptide.hashCode() : 0);
        result = 31 * result + charge;
        return result;
    }

    @Override
    public PeptideSpectrum copy(PeakProcessor<PeptideFragmentAnnotation, PeptideFragmentAnnotation> peakProcessor)
    {
        return new PeptideSpectrum(this, peakProcessor);
    }

    @Override
    public PeptideSpectrum copy(PeakProcessorChain<PeptideFragmentAnnotation> peakProcessorChain)
    {
        return new PeptideSpectrum(this, peakProcessorChain);
    }

    @Override
    public PeakList<PeptideFragmentAnnotation> copy(SpectrumProcessor<PeptideFragmentAnnotation, PeptideFragmentAnnotation> spectrumProcessor)
    {
        return new PeptideSpectrum(this, spectrumProcessor);
    }

    public Peptide getPeptide()
    {
        return peptide;
    }

    public int getCharge()
    {
        return charge;
    }

    public void addProteinAccessionNumbers(String... acc)
    {
        Collections.addAll(proteinAccessionNumbers, acc);
    }

    public void addProteinAccessionNumbers(Collection<String> accessionNumbers)
    {
        proteinAccessionNumbers.addAll(accessionNumbers);
    }

    public List<String> getProteinAccessionNumbers()
    {
        return ImmutableList.copyOf(proteinAccessionNumbers);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            builder.append(getX(i));
            builder.append(" ");
            builder.append(getY(i));
            builder.append(" ");
            builder.append(getFirstAnnotation(i).getSymbol());
            builder.append("\n");
        }

        return builder.toString();
    }
}
