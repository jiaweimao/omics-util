package omics.util.protein.ms;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.protein.mod.NeutralLoss;
import omics.util.utils.StringUtils;

/**
 * Precursor annotation
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Aug 2020, 11:20 AM
 */
public class PrecursorAnnotation extends PeptideFragmentAnnotation
{
    private final NeutralLoss[] neutralLosses;

    public PrecursorAnnotation(Ion ion, int charge, double theoreticalMz)
    {
        this(ion, charge, theoreticalMz, null);
    }

    public PrecursorAnnotation(Ion ion, int charge, double theoreticalMz, NeutralLoss[] neutralLosses)
    {
        super(ion, charge, theoreticalMz);
        this.neutralLosses = neutralLosses;
        StringBuilder builder = new StringBuilder(ion.getName());
        if (neutralLosses != null && neutralLosses.length > 0) {
            builder.append(NeutralLoss.getFormattedSymbol(neutralLosses));
        }
        if (charge > 1) {
            builder.append(StringUtils.superscript(charge + "+"));
        }
        this.symbol = builder.toString();
    }

    public NeutralLoss[] getNeutralLosses()
    {
        return neutralLosses;
    }

    @Override
    public PeakAnnotation copy()
    {
        return new PrecursorAnnotation(ion, charge, theoreticalMz, neutralLosses);
    }
}
