package omics.util.protein.ms;

import omics.util.chem.Mass;
import omics.util.chem.Weighable;


/**
 * The best short name could be Mass of the IonTools, with fixed number after decimal point.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 16 Mar 2017, 8:57 AM
 */
public class ReporterIon implements Weighable
{
    private final String name;
    private final Mass mass;

    public ReporterIon(String name, Mass mass)
    {
        this.name = name;
        this.mass = mass;
    }

    public String getName()
    {
        return name;
    }

    /**
     * Returns the index of a reporter ion. (i.e. its rounded m/z: 114 for iTRAQ 114).
     *
     * @return the index of a reporter ion.
     */
    public int getIndex()
    {
        return (int) getMolecularMass();
    }

    @Override
    public double getMolecularMass()
    {
        return mass.getMolecularMass();
    }
}
