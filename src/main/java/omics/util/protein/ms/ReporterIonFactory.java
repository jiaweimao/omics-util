package omics.util.protein.ms;

import omics.util.chem.Composition;
import omics.util.chem.NumericMass;

import java.util.HashMap;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 9:01 AM
 */
public class ReporterIonFactory
{
    private static ReporterIonFactory ionFactory;
    /**
     * Map of the implemented reporter ions. Name &gt; reporter ion
     */
    private final HashMap<String, ReporterIon> implementedIons;

    private ReporterIonFactory()
    {
        implementedIons = new HashMap<>();

        // Standard reporter ion iTRAQ 4Plex 114.
        addReporterIon(new ReporterIon("iTRAQ4Plex_114", Composition.parseComposition("C5C[13]H12N2")));
        // Standard reporter ion iTRAQ 4Plex 115.
        addReporterIon(new ReporterIon("iTRAQ4Plex_115", Composition.parseComposition("C5C[13]H12NN[15]")));
        // Standard reporter ion iTRAQ 4Plex 116.
        addReporterIon(new ReporterIon("iTRAQ4Plex_116", Composition.parseComposition("C4C[13]2H12NN[15]")));
        // Standard reporter ion iTRAQ 4Plex 117.
        addReporterIon(new ReporterIon("iTRAQ4Plex_117", Composition.parseComposition("C3C[13]3H12NN[15]")));
        //
        addReporterIon(new ReporterIon("iTRAQ4Plex_118", Composition.parseComposition("C4C[13]3H12NN[15]")));

        // Standard reporter ion iTRAQ 8Plex 113.
        addReporterIon(new ReporterIon("iTRAQ8Plex_113", Composition.parseComposition("C6H12N2")));
        //Standard reporter ion iTRAQ 8Plex 114.
        addReporterIon(new ReporterIon("iTRAQ8Plex_114", Composition.parseComposition("C5C[13]H12N2")));
        // Standard reporter ion iTRAQ 8Plex 115.
        addReporterIon(new ReporterIon("iTRAQ8Plex_115", Composition.parseComposition("C5C[13]H12N")));
        // Standard reporter ion iTRAQ 8Plex 116.
        addReporterIon(new ReporterIon("iTRAQ8Plex_116", Composition.parseComposition("C4C[13]2H12NN[15]")));
        // Standard reporter ion iTRAQ 8Plex 117.
        addReporterIon(new ReporterIon("iTRAQ8Plex_117", Composition.parseComposition("C3C[13]3H12NN[15]")));
        // Standard reporter ion iTRAQ 8Plex 118.
        addReporterIon(new ReporterIon("iTRAQ8Plex_118", Composition.parseComposition("C3C[13]3H12N[15]2")));
        // Standard reporter ion iTRAQ 8Plex 119.
        addReporterIon(new ReporterIon("iTRAQ8Plex_119", Composition.parseComposition("C2C[13]4H12N[15]2")));
        // Standard reporter ion iTRAQ 8Plex 121.
        addReporterIon(new ReporterIon("iTRAQ8Plex_121", Composition.parseComposition("C[13]6H12N[15]2")));
        // Standard reporter ion iTRAQ (reporter + balancer).@TODO: add the actual mass
        addReporterIon(new ReporterIon("iTRAQ145", new NumericMass(144.1)));
        // Standard reporter ion iTRAQ (reporter + balancer).
        addReporterIon(new ReporterIon("iTRAQ305", new NumericMass(304.2))); // @TODO: add the actual mass
        // Standard reporter ion TMT 126.
        addReporterIon(new ReporterIon("TMT_126", Composition.parseComposition("C8H15N")));
        // Standard reporter ion TMT 127N.
        addReporterIon(new ReporterIon("TMT_127N", Composition.parseComposition("C8H15N[15]")));
        // Standard reporter ion TMT 127C.
        addReporterIon(new ReporterIon("TMT_127C", Composition.parseComposition("C7C[13]H15N")));
        // Standard reporter ion TMT 128N.
        addReporterIon(new ReporterIon("TMT_128N", Composition.parseComposition("C7C[13]H15N[15]")));

        // Standard reporter ion TMT 128C.
        addReporterIon(new ReporterIon("TMT_128C", Composition.parseComposition("C6C[13]2H15N")));
        // Standard reporter ion TMT 129N.
        addReporterIon(new ReporterIon("TMT_129N", Composition.parseComposition("C6C[13]2H15N[15]")));
        // Standard reporter ion TMT 129C.
        addReporterIon(new ReporterIon("TMT_129C", Composition.parseComposition("C5C[13]3H15N")));
        // Standard reporter ion TMT 130N.
        addReporterIon(new ReporterIon("TMT_130N", Composition.parseComposition("C5C[13]3H15N[15]")));
        // Standard reporter ion TMT 130C.
        addReporterIon(new ReporterIon("TMT_130C", Composition.parseComposition("C4C[13]4H15N")));
        // Standard reporter ion TMT 131.
        addReporterIon(new ReporterIon("TMT_131", Composition.parseComposition("C4C[13]4H15N[15]")));
        // Standard reporter ion TMT 126 with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_126_ETD", Composition.parseComposition("C7H15N")));
        // Standard reporter ion TMT 127N with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_127N_ETD", Composition.parseComposition("C7H15N[15]")));
        // Standard reporter ion TMT 127C with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_127C_ETD", Composition.parseComposition("C7H15N")));
        // Standard reporter ion TMT 128N with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_128N_ETD", Composition.parseComposition("C7H15N[15]")));
        // Standard reporter ion TMT 128C with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_128C_ETD", Composition.parseComposition("C5C[13]2H15N")));
        // Standard reporter ion TMT 129N with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_129N_ETD", Composition.parseComposition("C5C[13]2H15N[15]")));
        // Standard reporter ion TMT 129C with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_129C_ETD", Composition.parseComposition("C5C[13]2H15N")));
        // Standard reporter ion TMT 130N with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_130N_ETD", Composition.parseComposition("C5C[13]2H15N[15]")));
        // Standard reporter ion TMT 130C with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_130C_ETD", Composition.parseComposition("C3C[13]4H15N")));
        // Standard reporter ion TMT 131 with ETD fragmentation.
        addReporterIon(new ReporterIon("TMT_131_ETD", Composition.parseComposition("C3C[13]4H15N[15]")));
        // Standard reporter ion TMT (reporter + balancer).
        addReporterIon(new ReporterIon("TMT230", new NumericMass(229.2)));// @TODO: add the actual mass
        // Standard reporter ion TMT (reporter + balancer).
        addReporterIon(new ReporterIon("TMT226", new NumericMass(225.2)));// @TODO: add the actual mass
        // Standard reporter ion for lysine acetylation (PMID: 18338905).
        addReporterIon(new ReporterIon("aceK126", Composition.parseComposition("C7H11ON")));
        // Standard reporter ion for lysine acetylation (PMID: 18338905).
        addReporterIon(new ReporterIon("aceK143", Composition.parseComposition("C7H14ON2")));
        // Standard reporter ion for phosphorylation of tyrosine (PMID: 11473401).
        addReporterIon(new ReporterIon("pY", Composition.parseComposition("C8H10NPO4")));
        // Standard reporter ion for formylation of K (PMID: 24895383).
        addReporterIon(new ReporterIon("fK112", Composition.parseComposition("C6H9NO")));
        // Standard reporter ion for methylation of R.
        addReporterIon(new ReporterIon("metR87", Composition.parseComposition("C4H10N2")));
        // Standard reporter ion for methylation of R (PMID: 16335983).
        addReporterIon(new ReporterIon("metR112", Composition.parseComposition("C5H9N3")));
        // Standard reporter ion for methylation of R (PMID: 16335983).
        addReporterIon(new ReporterIon("metR115", Composition.parseComposition("C5H10N2O")));
        // Standard reporter ion for methylation of R (PMID: 16335983).
        addReporterIon(new ReporterIon("metR143", Composition.parseComposition("C6H14N4")));
        // Standard reporter ion for methylation of R (PMID: 16335983).
        addReporterIon(new ReporterIon("metR70", Composition.parseComposition("C4H7N")));
        // Standard reporter ion for di-methylation of R (PMID: 16335983).
        addReporterIon(new ReporterIon("dimetR112", Composition.parseComposition("C5H9N3")));
        // Standard reporter ion for di-methylation of R (PMID: 16335983).
        addReporterIon(new ReporterIon("dimetR115", Composition.parseComposition("C5H10N2O")));
        // Standard reporter ion for di-methylation of R (PMID: 16335983).
        addReporterIon(new ReporterIon("metR157", Composition.parseComposition("C7H16N4")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQ", Composition.parseComposition("C10H16N4O4")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQ-H2O", Composition.parseComposition("C10H14N4O3")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQT", Composition.parseComposition("C14H24N5O6")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQT-H2O", Composition.parseComposition("C14H22N5O5")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQTG", Composition.parseComposition("C16H26N6O7")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQTG-H2O", Composition.parseComposition("C16H24N6O6")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQTGG", Composition.parseComposition("C18H29N7O8")));
        // Standard reporter ion for SUMO-2/3 Q87R.
        addReporterIon(new ReporterIon("QQTGG-H2O", Composition.parseComposition("C18H27N7O7")));
    }

    public static ReporterIonFactory getInstance()
    {
        if (ionFactory == null)
            ionFactory = new ReporterIonFactory();
        return ionFactory;
    }

    /**
     * Adds a reporter ion to the class static map. Reporter ions with the same
     * name will be overwritten.
     *
     * @param reporterIon the reporter ion to add
     */
    public void addReporterIon(ReporterIon reporterIon)
    {
        implementedIons.put(reporterIon.getName(), reporterIon);
    }

    /**
     * Returns the reporter ion corresponding to the given name.
     *
     * @param name the name of the reporter ion
     * @return the corresponding reporter ion
     */
    public ReporterIon getReporterIon(String name)
    {
        return implementedIons.get(name);
    }

    /**
     * @return the size of the factory, which represents the number of ReporterIon type
     */
    public int size()
    {
        return implementedIons.size();
    }
}
