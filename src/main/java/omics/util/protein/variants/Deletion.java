package omics.util.protein.variants;

import java.util.Objects;

/**
 * Class representing an amino acid deletion.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Nov 2018, 6:18 PM
 */
public class Deletion implements Variant {

    private char aa;

    /**
     * Constructor.
     *
     * @param aa the single character code of the deleted amino acid
     */
    public Deletion(char aa) {
        this.aa = aa;
    }

    @Override
    public String getDescription() {
        return "Deletion of " + aa;
    }

    public char getDeletedAminoAcid() {
        return aa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deletion deletion = (Deletion) o;
        return aa == deletion.aa;
    }

    @Override
    public int hashCode() {
        return Objects.hash(aa);
    }
}
