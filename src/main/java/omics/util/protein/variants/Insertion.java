package omics.util.protein.variants;

import java.util.Objects;

/**
 * Class representing an amino acid insertion.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Nov 2018, 6:19 PM
 */
public class Insertion implements Variant {

    private char aa;

    public Insertion(char aa) {
        this.aa = aa;
    }

    @Override
    public String getDescription() {
        return "Insertion of " + aa;
    }

    public char getInsertedAminoAcid() {
        return aa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Insertion insertion = (Insertion) o;
        return aa == insertion.aa;
    }

    @Override
    public int hashCode() {
        return Objects.hash(aa);
    }
}
