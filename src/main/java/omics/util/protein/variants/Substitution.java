package omics.util.protein.variants;

import java.util.Objects;

/**
 * Class representing an amino acid substitution.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Nov 2018, 6:20 PM
 */
public class Substitution implements Variant {
    /**
     * The single character code of the original amino acid.
     */
    private char originalAa;
    /**
     * The single character code of the substituted amino acid.
     */
    private char substitutedAa;

    /**
     * Constructor.
     *
     * @param originalAa    the single character code of the original amino acid
     * @param substitutedAa the single character code of the substituted amino acid
     */
    public Substitution(char originalAa, char substitutedAa) {
        this.originalAa = originalAa;
        this.substitutedAa = substitutedAa;
    }

    @Override
    public String getDescription() {
        return originalAa + " substituted by " + substitutedAa;
    }


    public char getOriginalAminoAcid() {
        return originalAa;
    }

    public char getSubstitutedAminoAcid() {
        return substitutedAa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Substitution that = (Substitution) o;
        return originalAa == that.originalAa &&
                substitutedAa == that.substitutedAa;
    }

    @Override
    public int hashCode() {
        return Objects.hash(originalAa, substitutedAa);
    }
}
