package omics.util.protein.variants;

import java.util.Objects;

/**
 * Class representing an amino acid swap.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Nov 2018, 6:21 PM
 */
public class Swap implements Variant {
    /**
     * The single character code of the amino acid originally to the left.
     */
    private char leftAa;
    /**
     * The single character code of the amino acid originally to the right.
     */
    private char rightAa;

    /**
     * Constructor.
     *
     * @param leftAa  the single character code of the amino acid originally to the left
     * @param rightAa the single character code of the amino acid originally to the right
     */
    public Swap(char leftAa, char rightAa) {
        this.leftAa = leftAa;
        this.rightAa = rightAa;
    }

    @Override
    public String getDescription() {
        return "Swap of " + leftAa + " and " + rightAa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Swap swap = (Swap) o;
        return leftAa == swap.leftAa &&
                rightAa == swap.rightAa;
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftAa, rightAa);
    }
}
