package omics.util.protein.variants;

/**
 * Interface for a variant of amino acid
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Nov 2018, 6:17 PM
 */
public interface Variant {
    /**
     * @return the description of the variant
     */
    String getDescription();
}
