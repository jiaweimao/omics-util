package omics.util.string;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * class for constructing suffix arrays on character sequences.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Jun 2019, 10:26 AM
 */
public class Alphabet
{
    public static final Alphabet DNA = new Alphabet("ACTG");
    /**
     * This alphabet is used for protein sequence, including sequence separator and unknown chars.
     */
    public static final Alphabet PROTEIN = new Alphabet("?_ACDEFGHIKLMNPQRSTVWY", 0);
    public static final Alphabet LOWERCASE = new Alphabet("abcdefghijklmnopqrstuvwxyz");
    public static final Alphabet UPPERCASE_26 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    /**
     * characters in the alphabet
     */
    private final char[] alphabet;
    private final int[] indices;
    private final int defaultValue;

    private final int radix;
    private final String base;

    /**
     * Constructor.
     *
     * @param alpha base string to create this alphabet, duplicate characters is not allowed.
     */
    public Alphabet(String alpha, int defaultValue)
    {
        this.base = alpha;
        this.defaultValue = defaultValue;
        boolean[] unicode = new boolean[Character.MAX_VALUE];
        for (int i = 0; i < alpha.length(); i++) { // check for duplicates
            char c = alpha.charAt(i);
            if (unicode[c]) throw new IllegalArgumentException("Illegal alphabet: duplicate character = '" + c + "'");
            else unicode[c] = true;
        }
        alphabet = alpha.toCharArray();
        radix = alpha.length();
        indices = new int[Character.MAX_VALUE];
        Arrays.fill(indices, defaultValue);
        for (int c = 0; c < radix; c++)
            indices[alphabet[c]] = c;
    }

    /**
     * Constructor with default value -1.
     *
     * @param alpha base string to create this alphabet, duplicate characters is not allowed.
     */
    public Alphabet(CharSequence alpha)
    {
        this(alpha.toString(), -1);
    }

    /**
     * Return true if character c in the alphabet
     *
     * @param c character to test
     * @return true if the character in this alphabet
     */
    public boolean contains(char c)
    {
        return indices[c] != defaultValue;
    }

    /**
     * @return radix, the number of the characters.
     */
    public int getRadix()
    {
        return radix;
    }

    /**
     * Return the index of given character, or <code>defaultValue</code> if the character is not in this alphabet.
     *
     * @param c a character in the alphabet
     * @return index of the character, or <code>defaultValue</code> if the character is not found.
     */
    public int toIndex(char c)
    {
        if (c >= indices.length)
            return defaultValue;
        return indices[c];
    }

    /**
     * Return the index of given character in byte, or <code>defaultValue</code> if the character is not in this alphabet.
     *
     * @param c a character
     * @return the index of the character in byte.
     */
    public byte toByteIndex(char c)
    {
        if (c >= indices.length)
            return (byte) defaultValue;
        return (byte) indices[c];
    }

    /**
     * Convert the string to byte[], for char not in this alphabet, given it value -1.
     *
     * @param s char array
     * @return map of the string
     */
    public byte[] toByteIndices(String s)
    {
        int N = s.length();
        byte[] arr = new byte[N];
        for (int i = 0; i < N; i++) {
            arr[i] = toByteIndex(s.charAt(i));
        }

        return arr;
    }

    /**
     * Convert the index to char
     *
     * @param index index
     * @return char at the index
     */
    public char toChar(byte index)
    {

        return alphabet[index];
    }

    /**
     * Convert the index to char
     *
     * @param index index
     * @return char at the index
     */
    public char toChar(int index)
    {

        return alphabet[index];
    }

    /**
     * convert string s over this alphabet into integer.
     */
    public int[] toIndices(String s)
    {
        char[] source = s.toCharArray();
        int[] target = new int[s.length()];
        for (int i = 0; i < source.length; i++)
            target[i] = toIndex(source[i]);
        return target;
    }

    /**
     * Convert integer into a string over this alphabet.
     *
     * @param indices indices of chars in this alpbabet.
     */
    public String toChars(int[] indices)
    {
        StringBuilder builder = new StringBuilder(indices.length);
        for (int index : indices) {
            builder.append(toChar(index));
        }
        return builder.toString();
    }

    /**
     * Convert integer into a string over this alphabet.
     *
     * @param indices indices of chars in this alpbabet.
     */
    public String toChars(byte[] indices)
    {
        StringBuilder builder = new StringBuilder(indices.length);
        for (int index : indices) {
            builder.append(toChar(index));
        }
        return builder.toString();
    }

    /**
     * @return the base string to create this alphabet.
     */
    public String getBase()
    {
        return base;
    }

    /**
     * @return alphabet array
     */
    public char[] getAlphabet()
    {
        return alphabet;
    }

    public long[] countChars(String fastaFile) throws IOException
    {
        BufferedReader in = new BufferedReader(new FileReader(fastaFile));
        long[] aaCount = new long[radix]; // count of amino acid
        String s;
        while ((s = in.readLine()) != null) {
            if (s.startsWith(">"))    // annotation
                continue;
            for (int i = 0; i < s.length(); i++) {
                if (contains(s.charAt(i)))
                    aaCount[toIndex(s.charAt(i))]++;
            }
        }

        return aaCount;
    }
}
