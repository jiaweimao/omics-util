package omics.util.utils;

import java.util.Comparator;

import static java.util.Objects.requireNonNull;

/**
 * Abstract class that compare indices of array of Number.
 * <p>
 * If you only want to know the rank of the value, but don't change the order of the elements, you can use this class.
 * <p>
 * The indexes for the items with same value will occur with the index ascending in the position in the array, such
 * as:
 * double[] array = {}1.0, 2.0, 2.0, 3.0};
 * ascending index array:
 * 0, 1, 2, 3
 * descending index array:
 * 3, 1, 2, 0
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 6:18 PM
 */
public abstract class ArrayIndexComparator<T extends Number> implements Comparator<Integer>
{
    protected T[] array;

    /**
     * Default constructor with null array.
     */
    public ArrayIndexComparator()
    {
        this.array = null;
    }

    /**
     * Constructor.
     *
     * @param array array to be sorted.
     */
    public ArrayIndexComparator(T[] array)
    {
        setArray(array);
    }

    /**
     * set the array for comparision
     *
     * @param array array to compare
     */
    public final void setArray(T[] array)
    {
        requireNonNull(array);

        this.array = array;
    }

    /**
     * @return the index array of the elements in the array to be sorted.
     */
    public Integer[] createIndexArray()
    {
        Integer[] indexes = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            indexes[i] = i;
        }
        return indexes;
    }

    @Override
    public abstract int compare(Integer index1, Integer index2);
}
