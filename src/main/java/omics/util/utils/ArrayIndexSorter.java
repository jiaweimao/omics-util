package omics.util.utils;

import java.util.Arrays;

import static java.util.Objects.requireNonNull;


/**
 * This class sort indices of an array object given an index comparator.
 * <p>
 * This can be used to sort PeakList with intensity property in descending order.
 * <p>
 * The original array is not changed, only return the indexes after sort.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 6:19 PM
 */
public final class ArrayIndexSorter
{

    /**
     * Sort the indices of array's object
     *
     * @return the sorted indices
     */
    public static <T extends Number> Integer[] sort(ArrayIndexComparator<T> comparator)
    {
        requireNonNull(comparator);

        Integer[] indices = comparator.createIndexArray();
        Arrays.sort(indices, comparator);

        return indices;
    }
}
