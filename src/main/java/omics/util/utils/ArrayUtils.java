package omics.util.utils;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

import java.util.Arrays;
import java.util.Collection;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * Array utils.
 *
 * @author JiaweiMao
 * @version 1.6.0
 * @since 07 Feb 2017, 11:01 AM
 */
public class ArrayUtils
{
    /**
     * An empty immutable {@code Double} array.
     */
    public static final Double[] EMPTY_DOUBLE_OBJECT_ARRAY = new Double[0];
    /**
     * An empty immutable {@code double} array.
     */
    public static final double[] EMPTY_DOUBLE_ARRAY = new double[0];

    private ArrayUtils() { }


    /**
     * Create an array of given values.
     *
     * @param a a int value
     * @return an int array
     */
    public static int[] array(int a)
    {
        return new int[]{a};
    }

    public static int[] array(int a1, int a2)
    {
        return new int[]{a1, a2};
    }

    /**
     * Grow the array arr by loadFactor
     *
     * @param arr        the array to grow
     * @param loadFactor the amount to grow the array
     * @return the new array
     */
    public static double[] grow(double[] arr, int loadFactor)
    {
        requireNonNull(arr);
        checkArgument(loadFactor > 0);

        double[] tmp = new double[arr.length + loadFactor];

        System.arraycopy(arr, 0, tmp, 0, arr.length);

        return tmp;
    }

    /**
     * Grow the array arr by loadFactor
     *
     * @param arr        the array to grow
     * @param loadFactor the amount to grow the array
     * @return the new array
     */
    public static float[] grow(float[] arr, int loadFactor)
    {
        requireNonNull(arr);
        checkArgument(loadFactor > 0);

        float[] tmp = new float[arr.length + loadFactor];

        System.arraycopy(arr, 0, tmp, 0, arr.length);

        return tmp;
    }

    /**
     * Insert the value into the array at index.
     *
     * @param value the value to insert
     * @param index the index in which the value is to be inserted
     * @param arr   the array to insert the value
     * @param size  the size of the peak list
     */
    public static void insert(double value, int index, double[] arr, int size)
    {
        System.arraycopy(arr, index, arr, index + 1, size - index);
        arr[index] = value;
    }

    /**
     * Insert the value into the array at index.
     *
     * @param value the value to insert
     * @param index the index in which the value is to be inserted
     * @param arr   the array to insert the value
     * @param size  the size of the peak list.
     */
    public static void insert(String value, int index, String[] arr, int size)
    {
        System.arraycopy(arr, index, arr, index + 1, size - index);
        arr[index] = value;
    }

    /**
     * Insert the value into the array at index.
     *
     * @param value the value to insert
     * @param index the index in which the value is to be inserted
     * @param arr   the array to insert the value
     * @param size  the size of the peak list
     */
    public static void insert(float value, int index, float[] arr, int size)
    {
        System.arraycopy(arr, index, arr, index + 1, size - index);
        arr[index] = value;
    }

    /**
     * Wraps System.arrayCopy and creates a new array if required
     *
     * @param src     the src array
     * @param srcPos  the src position
     * @param dest    the dest array
     * @param destPos the dest position
     * @param length  the number of elements to copy
     * @return the dest array or the newly created array
     */
    public static double[] copyArray(double[] src, int srcPos, double[] dest, int destPos, int length)
    {
        if (dest == null)
            dest = new double[destPos + length];

        System.arraycopy(src, srcPos, dest, destPos, length);

        return dest;
    }

    /**
     * Copies an array from the specified source array, beginning at the
     * specified position, to the specified position of the destination array.
     * <p/>
     * Not using System.arrayCopy because the src and dest array are not the
     * same type.
     *
     * @param src     the src array
     * @param srcPos  the src position
     * @param dest    the dest array
     * @param destPos the dest position
     * @param length  the number of elements to copy
     * @return the dest array or the newly created array
     */
    public static double[] copyArray(float[] src, int srcPos, double[] dest, int destPos, int length)
    {
        if (dest == null)
            dest = new double[destPos + length];

        for (int i = 0; i < length; i++) {

            dest[destPos + i] = src[srcPos + i];
        }

        return dest;
    }

    /**
     * Trim the arr to the size of this DoublePeakList
     *
     * @param arr the array to trim
     * @return the array
     */
    public static double[] trim(double[] arr, int size)
    {
        double[] tmp = new double[size];

        System.arraycopy(arr, 0, tmp, 0, size);

        return tmp;
    }

    /**
     * Trim the arr to the size of this DoublePeakList
     *
     * @param arr the array to trim
     * @return the array
     */
    public static float[] trim(float[] arr, int size)
    {
        float[] tmp = new float[size];

        System.arraycopy(arr, 0, tmp, 0, size);

        return tmp;
    }

    /**
     * Return true if the array is empty, which means it is null or length == 0.
     *
     * @param array array object
     * @param <T>   type of the array
     * @return true if the array is empty
     */
    public static <T> boolean isEmpty(T[] array)
    {
        return array == null || array.length == 0;
    }

    /**
     * Load doubles from src to dest arrays.
     *
     * @param src  the source array to load from.
     * @param dest the destination array to load to.
     * @return the dest array.
     */
    public static double[] loadDoubles(double[] src, double[] dest)
    {
        return loadDoubles(src, dest, 0);
    }

    /**
     * Load doubles from src to dest arrays.
     *
     * @param src     the source array to load from.
     * @param dest    the destination array to load to.
     * @param destPos starting position in the destination data.
     * @return the dest array.
     */
    public static double[] loadDoubles(double[] src, double[] dest, int destPos)
    {
        checkNotNull(src, "undefined src array to load into dest!");

        if (dest == null || dest.length < src.length)
            dest = new double[src.length];

        System.arraycopy(src, 0, dest, destPos, src.length);

        return dest;
    }

    /**
     * Convert double array to Double array.
     *
     * @param src  the source array to load from.
     * @param dest the destination array to load to.
     * @return the dest array.
     */
    public static Double[] toDoubles(double[] src, Double[] dest)
    {
        return toDoubles(src, dest, 0);
    }

    /**
     * Convert double array to Double array.
     *
     * @param src     the source array to load from.
     * @param dest    the destination array to load to.
     * @param destPos starting position in the destination data.
     * @return the dest array.
     */
    public static Double[] toDoubles(double[] src, Double[] dest, int destPos)
    {
        checkNotNull(src, "undefined src array to load into dest!");

        if (dest == null || dest.length < src.length)
            dest = new Double[src.length];

        for (int i = 0; i < src.length; i++) {
            dest[i + destPos] = src[i];
        }

        return dest;
    }

    /**
     * convert an double {@link Iterable} to double[]
     *
     * @param it a {@link Iterable<Double>}
     * @return double[] of the {@link Iterable}
     */
    public static double[] toDoubleArray(Collection<Double> it)
    {
        requireNonNull(it);
        DoubleList list = new DoubleArrayList(it.size());
        for (double value : it) {
            list.add(value);
        }
        return list.toDoubleArray();
    }

    /**
     * <p>Converts an array of primitive doubles to objects.
     *
     * <p>This method returns {@code null} for a {@code null} input array.
     *
     * @param array a {@code double} array
     * @return a {@code Double} array, {@code null} if null array input
     */
    public static Double[] toObject(final double[] array)
    {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return EMPTY_DOUBLE_OBJECT_ARRAY;
        }
        final Double[] result = new Double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    /**
     * <p>Converts an array of object Doubles to primitives.
     *
     * <p>This method returns {@code null} for a {@code null} input array.
     *
     * @param array a {@code Double} array, may be {@code null}
     * @return a {@code double} array, {@code null} if null array input
     * @throws NullPointerException if array content is {@code null}
     */
    public static double[] toPrimitive(final Double[] array)
    {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return EMPTY_DOUBLE_ARRAY;
        }
        final double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    /**
     * Get the index of the maximum value
     *
     * @param values the array to search for the max
     * @return an index or -1 if empty
     */
    public static int indexMax(double[] values)
    {
        requireNonNull(values);

        double valueMax = Double.NEGATIVE_INFINITY;
        int indexMax = -1;
        for (int i = 0; i < values.length; i++) {

            if (values[i] > valueMax) {
                valueMax = values[i];
                indexMax = i;
            }
        }

        return indexMax;
    }


    /**
     * Array hash code that takes into account the peak list size
     *
     * @param a the array
     * @return the hash code
     */
    public static int arrayHashCode(double[] a, int size)
    {
        if (a == null)
            return 0;

        int result = 1;
        for (int i = 0; i < size; i++) {

            double element = a[i];

            long bits = Double.doubleToLongBits(element);
            result = 31 * result + (int) (bits ^ (bits >>> 32));
        }
        return result;
    }

    /**
     * Array hash code that takes into account the peak list size
     *
     * @param a the array
     * @return the hash code
     */
    public static int arrayHashCode(float[] a, int size)
    {
        if (a == null)
            return 0;

        int result = 1;
        for (int i = 0; i < size; i++) {

            result = 31 * result + Float.floatToIntBits(a[i]);
        }

        return result;
    }

    /**
     * check {@link Double#NaN} and infinity values int the array
     *
     * @param values array
     * @param begin  begin index
     * @param length array range to check
     */
    public static void checkArray(final double[] values, final int begin, final int length)
    {
        for (int i = begin; i < begin + length; i++) {
            final double value = values[i];
            if (Double.isNaN(value)) {
                throw new IllegalArgumentException("NaN value at " + i);
            }
            if (Double.isInfinite(value)) {
                throw new IllegalArgumentException("Infinite value at " + i);
            }
        }
    }

    /**
     * Array equals that takes into account the size
     *
     * @param a  the first array
     * @param a2 the second array
     * @return true if the arrays are equals, false otherwise
     */
    public static boolean arrayEquals(double[] a, double[] a2, int size)
    {
        if (a == a2)
            return true;
        if (a == null || a2 == null)
            return false;

        for (int i = 0; i < size; i++)
            if (Double.doubleToLongBits(a[i]) != Double.doubleToLongBits(a2[i]))
                return false;

        return true;
    }

    /**
     * Array equals that takes into account the size
     *
     * @param a  the first array
     * @param a2 the second array
     * @return true if the arrays are equals, false otherwise
     */
    public static boolean arrayEquals(float[] a, float[] a2, int size)
    {
        if (a == a2)
            return true;
        if (a == null || a2 == null)
            return false;

        for (int i = 0; i < size; i++)
            if (Float.floatToIntBits(a[i]) != Float.floatToIntBits(a2[i]))
                return false;

        return true;
    }

    /**
     * convert a float[] to double array
     *
     * @param array a float array
     * @return double[]
     */
    public static double[] float2double(float[] array)
    {
        return float2double(array, null);
    }

    /**
     * convert a float[] to double array
     *
     * @param array a float array
     * @return double[]
     */
    public static double[] float2double(float[] array, double[] result)
    {
        if (result == null || result.length < array.length)
            result = new double[array.length];

        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    /**
     * convert a float[] to double array
     *
     * @param array a float array
     * @return double[]
     */
    public static double[] int2double(int[] array)
    {
        return int2double(array, null);
    }

    /**
     * convert a int[] to double[]
     *
     * @param array an int[]
     * @return double[]
     */
    public static double[] int2double(int[] array, double[] result)
    {
        if (result == null || result.length < array.length) {
            result = new double[array.length];
        }
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    /**
     * convert a float[] to double array
     *
     * @param array a float array
     * @return double[]
     */
    public static double[] long2double(long[] array)
    {
        return long2double(array, null);
    }

    /**
     * convert a float[] to double array
     *
     * @param array a float array
     * @return double[]
     */
    public static double[] long2double(long[] array, double[] result)
    {
        if (result == null || result.length < array.length)
            result = new double[array.length];

        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static float[] int2float(int[] array)
    {
        return int2float(array, null);
    }

    public static float[] int2float(int[] array, float[] result)
    {
        if (result == null || result.length < array.length) {
            result = new float[array.length];
        }
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static float[] long2float(long[] array)
    {
        return long2float(array, null);
    }

    public static float[] long2float(long[] array, float[] result)
    {
        if (result == null || result.length < array.length) {
            result = new float[array.length];
        }
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    /**
     * Return the minimum value in the array
     *
     * @param array an int array
     * @return minimum value in the array
     */
    public static int min(int[] array)
    {
        int min = Integer.MAX_VALUE;
        for (int i : array) {
            if (i < min)
                min = i;
        }
        return min;
    }

    /**
     * Return the maximum value in the array
     *
     * @param array an int array
     * @return maximum value in the array
     */
    public static int max(int[] array)
    {
        int max = Integer.MIN_VALUE;
        for (int i : array) {
            if (i > max)
                max = i;
        }
        return max;
    }

    /**
     * Return the sum of all values in the array.
     *
     * @param array an int array
     */
    public static int sum(int[] array)
    {
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        return sum;
    }

    public static float[] double2float(double[] array)
    {
        return double2float(array, null);
    }

    public static float[] double2float(double[] array, float[] result)
    {
        if (result == null || result.length < array.length)
            result = new float[array.length];

        for (int i = 0; i < array.length; i++) {
            result[i] = (float) array[i];
        }
        return result;
    }

    /**
     * return indexes of values within range [min, max]
     *
     * @param values values to be searched, should be in ascending order, or the result is not guaranteed.
     * @param min    minimum value
     * @param max    maximum value
     * @return indexes of values within given range in the array, if no value in the range, an empty array is returned.
     */
    public static int[] indexOf(double[] values, double min, double max)
    {
        requireNonNull(values);

        IntList list = new IntArrayList();
        int index = Arrays.binarySearch(values, min);
        if (index < 0) {
            index = -index - 1;
        }
        for (int id = index; id < values.length; id++) {
            if (values[id] > max)
                break;

            list.add(id);
        }

        return list.toIntArray();
    }
}
