package omics.util.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Utility class for bytes conversion.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 27 Aug 2018, 6:19 PM
 */
public class ByteUtils
{
    /**
     * Convert four bytes into one float
     *
     * @param bytes the quad to convert
     * @return one float
     * @throws IllegalArgumentException if not four bytes
     */
    public static float quadToFloat(byte... bytes)
    {
        checkArgument(bytes.length == 4, "needs exactly 4 bytes to make a float!");

        return ByteBuffer.wrap(bytes).asFloatBuffer().get();
    }

    /**
     * Convert eight bytes into one double
     *
     * @param bytes the octet to convert
     * @return one double
     * @throws IllegalArgumentException if not eight bytes
     */
    public static double octetToDouble(byte... bytes)
    {
        checkArgument(bytes.length == 8, "needs exactly 8 bytes to make a double!");

        return ByteBuffer.wrap(bytes).asDoubleBuffer().get();
    }

    /**
     * Convert one float into four bytes
     *
     * @param real the float to convert
     * @return an array of four bytes
     */
    public static byte[] floatToQuad(float real)
    {
        int intBits = Float.floatToIntBits(real);

        byte[] ba = new byte[4];

        for (int i = ba.length - 1; i >= 0; i--) {
            ba[i] = (byte) (intBits & 0xff);
            intBits >>= 8;
        }

        return ba;
    }

    /**
     * Convert one double into eight bytes
     *
     * @param real the double to convert
     * @return an array of eight bytes
     */
    public static byte[] doubleToOctet(double real)
    {
        long longBits = Double.doubleToLongBits(real);

        byte[] ba = new byte[8];

        for (int i = ba.length - 1; i >= 0; i--) {
            ba[i] = (byte) (longBits & 0xff);
            longBits >>= 8;
        }

        return ba;
    }

    public static float[] quadsToFloats(byte[] bytes, float[] floats)
    {
        checkArgument(bytes.length % 4 == 0);

        int len = bytes.length / 4;

        if (floats == null || floats.length < len) floats = new float[len];

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            floats[i++] = buff.getFloat();
        }

        return floats;
    }

    public static double[] quadsToDoubles(byte[] bytes, double[] doubles)
    {
        checkArgument(bytes.length % 4 == 0);

        int len = bytes.length / 4;

        if (doubles == null || doubles.length < len) doubles = new double[len];

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            doubles[i++] = buff.getFloat();
        }

        return doubles;
    }

    /**
     * convert byte array to double[], each double values was stored as 8 bytes in byte array.
     *
     * @param bytes     byte array
     * @param byteOrder {@link ByteOrder}
     * @return double[]
     */
    public static double[] bytes2Doubles(byte[] bytes, ByteOrder byteOrder, double[] result)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        byteBuffer.order(byteOrder);

        int len = bytes.length / 8;
        if (result == null || result.length < len)
            result = new double[len];
        for (int index = 0; index < bytes.length; index += 8) {
            double aDouble = byteBuffer.getDouble(index);
            result[index / 8] = aDouble;
        }
        return result;
    }

    /**
     * convert byte array to double[], each double values was stored as 8 bytes in byte array.
     *
     * @param bytes     byte array
     * @param byteOrder {@link ByteOrder}
     * @return double[]
     */
    public static double[] bytes2Doubles(byte[] bytes, ByteOrder byteOrder)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        byteBuffer.order(byteOrder);

        double[] resultArray = new double[bytes.length / 8];
        for (int index = 0; index < bytes.length; index += 8) {
            double aDouble = byteBuffer.getDouble(index);
            resultArray[index / 8] = aDouble;
        }
        return resultArray;
    }

    /**
     * convert byte array to float[], each float values was stored as 4 bytes in byte array.
     *
     * @param bytes     byte array
     * @param byteOrder {@link ByteOrder}
     * @return float[]
     */
    public static float[] bytes2Floats(byte[] bytes, ByteOrder byteOrder)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        byteBuffer.order(byteOrder);

        float[] resultArray = new float[bytes.length / 4];
        for (int index = 0; index < bytes.length; index += 4) {
            float value = byteBuffer.getFloat(index);
            resultArray[index / 4] = value;
        }
        return resultArray;
    }

    /**
     * convert byte array to long[], each long value was stored as 8 bytes in byte array.
     *
     * @param bytes     byte array
     * @param byteOrder {@link ByteOrder}
     * @return long[]
     */
    public static long[] bytes2Longs(byte[] bytes, ByteOrder byteOrder)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        byteBuffer.order(byteOrder);

        long[] resultArray = new long[bytes.length / 8];
        for (int index = 0; index < bytes.length; index += 8) {
            long value = byteBuffer.getLong(index);
            resultArray[index / 8] = value;
        }
        return resultArray;
    }

    /**
     * convert byte array to int[], each int value was stored as 4 bytes in byte array.
     *
     * @param bytes     byte array
     * @param byteOrder {@link ByteOrder}
     * @return int[]
     */
    public static int[] bytes2Ints(byte[] bytes, ByteOrder byteOrder)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        byteBuffer.order(byteOrder);

        int[] resultArray = new int[bytes.length / 4];
        for (int index = 0; index < bytes.length; index += 4) {
            int value = byteBuffer.getInt(index);
            resultArray[index / 4] = value;
        }
        return resultArray;
    }

    public static double[] octetsToDoubles(byte[] bytes, double[] doubles)
    {
        checkArgument(bytes.length % 8 == 0);

        int len = bytes.length / 8;

        if (doubles == null || doubles.length < len) doubles = new double[len];

        ByteBuffer buff = ByteBuffer.wrap(bytes);

        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {

            doubles[i++] = buff.getDouble();
        }

        return doubles;
    }

    public static byte[] floatsToQuads(float[] floats)
    {
        Objects.requireNonNull(floats);
        checkArgument(floats.length > 0);

        ByteBuffer buffer = ByteBuffer.allocate(floats.length * 4);
        for (float aFloat : floats) {

            buffer.putInt(Float.floatToRawIntBits(aFloat));
        }

        return buffer.array();
    }

    public static byte[] doublesToOctets(double[] doubles)
    {
        checkNotNull(doubles);
        checkArgument(doubles.length > 0);

        ByteBuffer buffer = ByteBuffer.allocate(doubles.length * 8);

        for (double aDouble : doubles) {

            buffer.putLong(Double.doubleToRawLongBits(aDouble));
        }

        return buffer.array();
    }

}
