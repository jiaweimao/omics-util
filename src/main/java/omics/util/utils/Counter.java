package omics.util.utils;

/**
 * {@code Counter} is a simple counter and a better alternative than {@code
 * Integer} as a new instance is created each time you want to set the value.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 3:55 PM
 */
public class Counter {

    private int count;

    public Counter() {
        count = 0;
    }

    /**
     * Constructor
     *
     * @param start the start count.
     */
    public Counter(int start) {
        count = start;
    }

    /**
     * Reset the count to 0
     */
    public void reset() {
        count = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Counter counter = (Counter) o;

        return count == counter.count;
    }

    @Override
    public int hashCode() {
        return count;
    }

    /**
     * Increment counter by 1.
     */
    public void increment() {
        count++;
    }

    /**
     * Increment counter by {@code inc}.
     */
    public void increment(int inc) {
        count += inc;
    }

    /**
     * Decrement counter by 1.
     */
    public void decrement() {
        count--;
    }

    /**
     * Decrement counter by {@code dec}.
     */
    public void decrement(int dec) {
        count -= dec;
    }

    /**
     * @return counter value.
     */
    public int count() {
        return count;
    }

    public String toString() {
        return Integer.toString(count);
    }

}
