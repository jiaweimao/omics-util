package omics.util.utils;

import omics.util.interfaces.Filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Filter delegate to a list of {@link Filter}
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 26 Oct 2018, 3:51 PM
 */
public class DelegateFilter<T> implements Filter<T>
{
    /**
     * Relationship between elements in the filters
     */
    public enum Mode
    {
        /**
         * AND means only pass all filters return true
         */
        AND,
        /**
         * OR means pass any filter return true
         */
        OR
    }

    private final List<Filter<T>> filterList;
    private final Mode mode;

    /**
     * Constructor
     *
     * @param filterList list of {@link Filter}
     * @param mode       {@link Mode} to define relationship between filters
     */
    public DelegateFilter(List<Filter<T>> filterList, Mode mode)
    {
        checkNotNull(filterList);

        this.filterList = new ArrayList<>(filterList);
        this.mode = mode;
    }

    /**
     * Contruct with default {@link Mode#AND}
     *
     * @param filterList list of {@link Filter}
     */
    public DelegateFilter(List<Filter<T>> filterList)
    {
        this(filterList, Mode.AND);
    }

    /**
     * Construct with initial empty filters
     *
     * @param mode {@link Mode}
     */
    public DelegateFilter(Mode mode)
    {
        this.mode = mode;
        this.filterList = new ArrayList<>();
    }

    /**
     * Construct with initial {@link Filter} and {@link Mode#AND}
     */
    public DelegateFilter()
    {
        this(Mode.AND);
    }

    /**
     * add a new Filter to this
     *
     * @param filter a {@link Filter}
     */
    public DelegateFilter<T> add(Filter<T> filter)
    {
        this.filterList.add(filter);
        return this;
    }

    public DelegateFilter<T> add(DelegateFilter<T> filter)
    {
        this.filterList.addAll(filter.filterList);
        return this;
    }

    /**
     * Add all filters to this {@link DelegateFilter}
     *
     * @param filters a list of {@link Filter}
     * @return this
     */
    public DelegateFilter<T> addAll(List<Filter<T>> filters)
    {
        this.filterList.addAll(filters);
        return this;
    }

    /**
     * @return number of child filters in it.
     */
    public int size()
    {
        return this.filterList.size();
    }

    /**
     * @return an unmodifiable filters in it.
     */
    public List<Filter<T>> getFilterList()
    {
        return Collections.unmodifiableList(filterList);
    }


    /**
     * @return true if this delegate filter contains no filter.
     */
    public boolean isEmpty()
    {
        return filterList.isEmpty();
    }

    @Override
    public boolean test(T filterable)
    {
        boolean result = false;
        switch (mode) {
            case AND:
                result = testAnd(filterable);
                break;
            case OR:
                result = testOr(filterable);
                break;
        }
        return result;
    }

    /**
     * evaluate all filters with AND operator.
     *
     * @param filterable instance to filter
     * @return boolean with the result of the operation (Filter1(entry) AND
     * Filter2(entry) AND ...).
     */
    private boolean testAnd(T filterable)
    {
        for (Filter<T> filter : filterList) {
            if (!filter.test(filterable)) {
                return false;
            }
        }
        return true;
    }

    private boolean testOr(T filterable)
    {
        for (Filter<T> filter : filterList) {
            if (filter.test(filterable)) {
                return true;
            }
        }
        return false;
    }
}
