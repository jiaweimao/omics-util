package omics.util.utils;


import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Comparator to return index of value in double array in ascending order.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 26 Jun 2018, 1:01 PM
 */
public class DoubleAscendingComparator extends ArrayIndexComparator<Double> {

    public DoubleAscendingComparator() {
        super();
    }

    /**
     * Constructor.
     *
     * @param array the double array to be sorted.
     */
    public DoubleAscendingComparator(Double[] array) {
        super(array);
    }

    public DoubleAscendingComparator(double[] values) {
        setArray(values);
    }

    /**
     * set the array to be sorted.
     *
     * @param array a double[]
     */
    public void setArray(double[] array) {
        checkNotNull(array);

        super.array = new Double[array.length];
        for (int i = 0; i < array.length; i++) {
            super.array[i] = array[i];
        }
    }

    @Override
    public int compare(Integer index1, Integer index2) {
        return Double.compare(array[index1], array[index2]);
    }
}
