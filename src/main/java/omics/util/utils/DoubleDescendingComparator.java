package omics.util.utils;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Comparator to return index in descending order.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 01 Jun 2018, 8:45 PM
 */
public class DoubleDescendingComparator extends ArrayIndexComparator<Double> {

    /**
     * Default constructor with null initial array.
     */
    public DoubleDescendingComparator() {
        super();
    }

    /**
     * Constructor.
     *
     * @param array the double array to be sorted.
     */
    public DoubleDescendingComparator(Double[] array) {
        super(array);
    }

    public DoubleDescendingComparator(double[] values) {
        setArray(values);
    }

    public void setArray(double[] array) {
        checkNotNull(array);

        super.array = new Double[array.length];
        for (int i = 0; i < array.length; i++) {
            super.array[i] = array[i];
        }
    }

    @Override
    public int compare(Integer index1, Integer index2) {
        return Double.compare(array[index2], array[index1]);
    }
}
