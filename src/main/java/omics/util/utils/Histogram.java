package omics.util.utils;

import java.util.*;

/**
 * Histogram dataset.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 09 Nov 2019, 8:59 AM
 */
public class Histogram<T>
{
    private final HashMap<T, Integer> countMap;
    private int count;

    public Histogram()
    {
        this.countMap = new HashMap<>();
    }

    /**
     * Constructor
     *
     * @param size init size
     */
    public Histogram(int size)
    {
        this.countMap = new HashMap<>(size);
    }

    /**
     * Increase one for given delta
     * T
     *
     * @param delta a delta name
     */
    public void increase(T delta)
    {
        countMap.put(delta, countMap.getOrDefault(delta, 0) + 1);
        this.count++;
    }

    /**
     * Increase count for given delta.
     *
     * @param delta delta name
     * @param count count
     */
    public void increase(T delta, int count)
    {
        countMap.put(delta, countMap.getOrDefault(delta, 0) + count);
        this.count += count;
    }

    /**
     * @return sum of all items added to this histogram.
     */
    public int getCount()
    {
        return count;
    }

    /**
     * Return count of given key.
     *
     * @param key key
     * @return count of the key.
     */
    public int getCount(T key)
    {
        return countMap.getOrDefault(key, 0);
    }

    /**
     * @return the probabilities of all deltas.
     */
    public Map<T, Double> getProbabilities()
    {
        HashMap<T, Double> probMap = new HashMap<>();
        for (Map.Entry<T, Integer> entry : countMap.entrySet()) {
            double prob = entry.getValue() / (double) count;
            probMap.put(entry.getKey(), prob);
        }
        return probMap;
    }

    /**
     * @return keys
     */
    public Set<T> keySet()
    {
        return Collections.unmodifiableSet(countMap.keySet());
    }

    /**
     * @return probabilities of all items in descending order.
     */
    public List<Pair<T, Double>> getSortedProbabilities()
    {
        List<Pair<T, Double>> list = new ArrayList<>();
        for (Map.Entry<T, Integer> entry : countMap.entrySet()) {
            list.add(Pair.create(entry.getKey(), entry.getValue() / (double) count));
        }

        list.sort((o1, o2) -> Double.compare(o2.getSecond(), o1.getSecond()));

        return list;
    }
}
