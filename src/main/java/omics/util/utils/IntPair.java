package omics.util.utils;

import java.util.Objects;

/**
 * A pair of int values.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Jun 2019, 3:39 PM
 */
public class IntPair {
    /**
     * Key.
     */
    private final int key;
    /**
     * Value.
     */
    private final int value;

    /**
     * Create an entry representing a mapping from the specified key to the
     * specified value.
     *
     * @param k Key (first element of the pair).
     * @param v Value (second element of the pair).
     */
    public IntPair(int k, int v) {
        key = k;
        value = v;
    }

    /**
     * Create an entry representing the same mapping as the specified entry.
     *
     * @param entry Entry to copy.
     */
    public IntPair(IntPair entry) {
        this(entry.getKey(), entry.getValue());
    }

    /**
     * Convenience factory method that calls the
     * {@link #IntPair(int, int)} constructor}.
     *
     * @param k First element of the pair.
     * @param v Second element of the pair.
     * @return a new {@code Pair} containing {@code k} and {@code v}.
     * @since 3.3
     */
    public static IntPair create(int k, int v) {
        return new IntPair(k, v);
    }

    /**
     * Get the key.
     *
     * @return the key (first element of the pair).
     */
    public int getKey() {
        return key;
    }

    /**
     * Get the value.
     *
     * @return the value (second element of the pair).
     */
    public int getValue() {
        return value;
    }

    /**
     * @return the first element of the pair.
     */
    public int getFirst() {
        return key;
    }

    /**
     * @return the second element of the pair.
     */
    public int getSecond() {
        return value;
    }

    /**
     * @return delta value between second and the first value.
     */
    public int range() {
        return value - key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntPair)) return false;
        IntPair pair = (IntPair) o;
        return Objects.equals(key, pair.key) &&
                Objects.equals(value, pair.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "[" + getKey() + ", " + getValue() + "]";
    }
}
