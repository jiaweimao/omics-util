package omics.util.utils;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 22 Oct 2018, 1:17 PM
 */
public class IterableUtils {

    /**
     * Determines if the given iterable contains no elements.
     *
     * <p>There is no precise {@link Iterator} equivalent to this method, since one can only ask an
     * iterator whether it has any elements <i>remaining</i> (which one does using {@link
     * Iterator#hasNext}).
     *
     * <p><b>{@code Stream} equivalent:</b> {@code !stream.findAny().isPresent()}
     *
     * @return {@code true} if the iterable contains no elements
     */
    public static boolean isEmpty(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection<?>) iterable).isEmpty();
        }
        return !iterable.iterator().hasNext();
    }
}
