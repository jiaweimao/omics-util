package omics.util.utils;

import java.util.*;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * This object generates all tuples of length n (n-tuple) where each element
 * * belongs to different base numbers bi.
 * * <p>
 * * As an example, from the mixed radix triplet [2,3,2] we can generate 12
 * * permutations of triplets from [000] to [121].
 * * <p>
 * * [Donald Knuth - The Art of Computer Programming Volume 4, Fascicle 2:
 * * Generating All tuples and permutations, (Addison-Wesley, February 14, 2005)
 * * v+127pp, ISBN 0-201-85393-0, section 7.1.1 : Algorithm M]
 * *
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 6:20 PM
 */
public class MixedRadixNtupleGenerator
{

    private final Handler handler;

    /**
     * @param handler provides the callback method that have to handle n-tuple
     */
    public MixedRadixNtupleGenerator(Handler handler)
    {
        requireNonNull(handler);

        this.handler = handler;
    }

    /**
     * Compute the number of n-tuples given the radices.
     *
     * @param radices the radices by position.
     * @return the total number of n-tuples.
     */
    public static long countNtuples(int[] radices)
    {
        requireNonNull(radices);

        long product = 1;
        for (final int limit : radices) {

            checkArgument(limit > 0);

            product *= limit;
        }
        return product;
    }

    /**
     * A simple string converter for n-tuples.
     *
     * @param ntuples the n-tuples to convert.
     * @return a string representation of n-tuples.
     */
    public static String toString(List<int[]> ntuples)
    {
        requireNonNull(ntuples);

        StringBuilder sb = new StringBuilder();

        // Tuples are usually written within parenthesis.
        // For example, (2, 7, 4, 1, 7) is a 5-tuple.
        sb.append("[");
        for (int[] ntuple : ntuples) {
            sb.append(Arrays.toString(ntuple)).append(", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        sb.append("]");

        return sb.toString();
    }

    /**
     * This algorithm generate all {@code n}-tuples with the same {@code radix}
     * at any positions.
     *
     * @param n     the tuple length.
     * @param radix the radix for each digit.
     * @return a list of tuples that satisfy the predicate
     */
    public void generate(int n, int radix)
    {
        int[] radices = new int[n];

        for (int i = 0; i < n; i++) {
            radices[i] = radix;
        }

        generate(radices);
    }

    /**
     * This algorithm generates all n-tuples with different radices and give
     * them to the <b>Handler</>
     * <p>
     * [from knuth the AOCP, section 7.1.1 : Algorithm M]
     *
     * @param radices the number bases [b0, ..., bn] such as ai in [0, bi].
     */
    public void generate(int[] radices)
    {
        requireNonNull(radices);

        /**
         * In mathematics, a tuple is a sequence (or ordered list) of finite
         * length. A n-tuple is a tuple with n elements.
         */
        final int[] upperLimitsExtended = new int[radices.length + 1];
        System.arraycopy(radices, 0, upperLimitsExtended, 1, radices.length);
        final int[] ntuple = new int[upperLimitsExtended.length];

        // M1. init
        upperLimitsExtended[0] = 2;
        for (int i = 0; i < ntuple.length; i++) {
            ntuple[i] = 0;
        }

        while (true) {
            // M2. visit current tuple : add new n-tuple
            final int[] currentNtuple = new int[ntuple.length - 1];
            System.arraycopy(ntuple, 1, currentNtuple, 0, currentNtuple.length);

            handler.handle(currentNtuple);

            // M3. prepare to add one
            int i = ntuple.length - 1;

            // M4. test limits
            while (ntuple[i] == upperLimitsExtended[i] - 1) {
                ntuple[i] = 0;
                i--;
            }

            // M5.
            if (i == 0) {
                break;
            } else {
                ntuple[i]++;
            }
        }
    }

    public interface Handler
    {
        void handle(int[] ntuple);
    }

    /**
     * A reusable container that stores generated n-tuples
     */
    public static class NtupleContainer implements Handler
    {
        protected List<int[]> ntuples;
        private Optional<Predicate<int[]>> pred;

        public NtupleContainer()
        {

            ntuples = new ArrayList<>();
            pred = Optional.empty();
        }

        public NtupleContainer(Predicate<int[]> pred)
        {
            this();

            requireNonNull(pred);

            this.pred = Optional.of(pred);
        }

        @Override
        public void handle(int[] ntuple)
        {
            if (!pred.isPresent() || pred.get().test(ntuple))

                ntuples.add(ntuple);
        }

        public void clear()
        {
            ntuples.clear();
        }

        public List<int[]> getNtuples()
        {
            return Collections.unmodifiableList(ntuples);
        }
    }
}
