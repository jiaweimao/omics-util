package omics.util.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;


/**
 * A factory to create Locale.US {@code NumberFormst}s.
 *
 * @author JiaweiMao
 * @version 1.2.1
 * @since 20 Mar 2017, 10:09 PM
 */
public class NumberFormatFactory
{
    /**
     * exponent value
     */
    private static final int DEFAULT_PRECISION = 2;

    private static final DecimalFormatSymbols US_SYMBOLS = new DecimalFormatSymbols(Locale.US);
    public static final DecimalFormat INT_PRECISION = NumberFormatFactory.valueOf(0);
    public static final DecimalFormat DIGIT2 = NumberFormatFactory.valueOf(2);
    public static final DecimalFormat FLOAT_PRECISION = NumberFormatFactory.valueOf(3);
    public static final DecimalFormat MASS_PRECISION = NumberFormatFactory.valueOf(4);
    public static final DecimalFormat HIGH_MASS_PRECISION = NumberFormatFactory.valueOf(6);
    public static final DecimalFormat DOUBLE_PRECISION = NumberFormatFactory.valueOf(10);
    private static final DecimalFormat DEFAULT_NUMBER_FORMAT = valueOf(DEFAULT_PRECISION);

    private NumberFormatFactory() { }

    /**
     * @return Default NumberFormat with 2 fraction digits.
     */
    public static DecimalFormat getInstance()
    {
        return DEFAULT_NUMBER_FORMAT;
    }

    public static DecimalFormat valueOf(String pattern)
    {
        requireNonNull(pattern);
        return new DecimalFormat(pattern, US_SYMBOLS);
    }

    /**
     * Make a new instance of DecimalFormat with one maximum integer digit.
     *
     * @param maxFractionDigits the maximum number of fractional digits.
     * @return instance of DecimalFormat.
     */
    public static DecimalFormat valueOf(int maxFractionDigits)
    {
        return valueOf(1, false, maxFractionDigits, false, false);
    }

    /**
     * Make a new instance of DecimalFormat with one maximum integer digit.
     *
     * @param maxFractionDigits the maximum number of fractional digits.
     * @return instance of DecimalFormat.
     */
    public static DecimalFormat valueOf(int maxFractionDigits, boolean optional)
    {
        return valueOf(1, false, maxFractionDigits, optional, false);
    }

    /**
     * Make a new instance of DecimalFormat.
     *
     * @param maxInteger        maximum number of integer digits
     * @param optionalInteger   true if integer digit is optional
     * @param maxFractionDigits maximum number of fractional digits
     * @param optionalFraction  true if fractional digit is optional
     * @param scientific        true if use scientific notation
     * @return {@link DecimalFormat} instance
     */
    public static DecimalFormat valueOf(int maxInteger, boolean optionalInteger,
                                        int maxFractionDigits, boolean optionalFraction,
                                        boolean scientific)
    {
        checkArgument(maxInteger >= 0);
        checkArgument(maxFractionDigits >= 0);

        return valueOf(createDecimalPattern(maxInteger, maxFractionDigits, optionalInteger, optionalFraction, scientific));
    }

    /**
     * Make a new instance of DecimalFormat given the maximum integer and
     * fraction digits.
     *
     * @param maxIntegerDigits  the maximum number of integer digits.
     * @param maxFractionDigits the maximum number of fractional digits.
     * @return instance of DecimalFormat.
     */
    public static DecimalFormat valueOf(int maxIntegerDigits, int maxFractionDigits)
    {
        checkArgument(maxFractionDigits >= 0 && maxIntegerDigits >= 0);

        return valueOf(maxIntegerDigits, false, maxFractionDigits, false, false);
    }

    /**
     * Make a new instance of DecimalFormat given the maximum integer and
     * fraction digits for scientific notation.
     *
     * @param maxFractionDigits the maximum number of fractional digits.
     * @return instance of DecimalFormat.
     */
    public static DecimalFormat valueOfSN(int maxFractionDigits)
    {
        checkArgument(maxFractionDigits >= 0);
        return valueOf(1, true, maxFractionDigits, true, true);
    }

    /**
     * @return the default Precision (2).
     */
    public static int getDefaultPrecision()
    {
        return DEFAULT_PRECISION;
    }

    /**
     * Make a string format for decimal printing.
     *
     * @param maxIntegerDigits  the maximum number of integer digits
     * @param maxFractionDigits the maximum number of fractional digits
     * @return a string format.
     */
    private static String createDecimalPattern(int maxIntegerDigits, int maxFractionDigits,
                                               boolean optionalInteger, boolean optionalFraction, boolean sci)
    {
        StringBuilder sb = new StringBuilder();

        if (optionalInteger) {
            for (int i = 0; i < maxIntegerDigits; i++)
                sb.append("#");
        } else {
            for (int i = 0; i < maxIntegerDigits; i++)
                sb.append("0");
        }

        if (maxFractionDigits > 0) {
            sb.append(".");
        }

        if (optionalFraction) {
            for (int i = 0; i < maxFractionDigits; i++) {
                sb.append("#");
            }
        } else {
            for (int i = 0; i < maxFractionDigits; i++) {
                sb.append("0");
            }
        }
        if (sci) {
            sb.append("E0");
        }
        return sb.toString();
    }
}
