package omics.util.utils;

import java.util.Comparator;
import java.util.Objects;

/**
 * Generic pair.
 * <br/>
 * Although the instances of this class are immutable, it is impossible
 * to ensure that the references passed to the constructor will not be
 * modified by the caller.
 *
 * @author JiaweiMao
 * @version 2.1.0
 * @since 06 Jan 2018, 4:17 PM
 */
public class Pair<K, V> {

    /**
     * Key.
     */
    private final K key;
    /**
     * Value.
     */
    private final V value;

    /**
     * Create an entry representing a mapping from the specified key to the
     * specified value.
     *
     * @param k Key (first element of the pair).
     * @param v Value (second element of the pair).
     */
    public Pair(K k, V v) {
        key = k;
        value = v;
    }

    /**
     * Create an entry representing the same mapping as the specified entry.
     *
     * @param entry Entry to copy.
     */
    public Pair(Pair<? extends K, ? extends V> entry) {
        this(entry.getKey(), entry.getValue());
    }

    /**
     * Convenience factory method that calls the
     * {@link #Pair(Object, Object) constructor}.
     *
     * @param <K> the key type
     * @param <V> the value type
     * @param k   First element of the pair.
     * @param v   Second element of the pair.
     * @return a new {@code Pair} containing {@code k} and {@code v}.
     * @since 3.3
     */
    public static <K, V> Pair<K, V> create(K k, V v) {
        return new Pair<>(k, v);
    }

    /**
     * Get the key.
     *
     * @return the key (first element of the pair).
     */
    public K getKey() {
        return key;
    }

    /**
     * Get the value.
     *
     * @return the value (second element of the pair).
     */
    public V getValue() {
        return value;
    }

    /**
     * @return the first element of the pair.
     */
    public K getFirst() {
        return key;
    }

    /**
     * @return the second element of the pair.
     */
    public V getSecond() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pair)) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(key, pair.key) &&
                Objects.equals(value, pair.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "[" + getKey() + ", " + getValue() + "]";
    }

    public static class PairComparator<K extends Comparable<? super K>, V extends Comparable<? super V>>
            implements Comparator<Pair<K, V>> {
        boolean useSecond;

        public PairComparator() {
            this(false);
        }

        public PairComparator(boolean useSecond) {
            this.useSecond = useSecond;
        }

        @Override
        public int compare(Pair<K, V> o1, Pair<K, V> o2) {
            if (useSecond) {
                return o1.getSecond().compareTo(o2.getSecond());
            } else {
                return o1.getFirst().compareTo(o2.getFirst());
            }
        }
    }

    public static class PairDecreaseComparator<K extends Comparable<? super K>, V extends Comparable<? super V>>
            implements Comparator<Pair<K, V>> {
        private boolean useSecond;

        public PairDecreaseComparator() {
            this(false);
        }

        /**
         * Constructor
         *
         * @param useSecond true if use the second value for comparision.
         */
        public PairDecreaseComparator(boolean useSecond) {
            this.useSecond = useSecond;
        }

        @Override
        public int compare(Pair<K, V> o1, Pair<K, V> o2) {
            if (useSecond)
                return o2.getSecond().compareTo(o1.getSecond());
            else
                return o2.getFirst().compareTo(o1.getFirst());
        }
    }
}
