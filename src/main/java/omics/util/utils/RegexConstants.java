package omics.util.utils;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 6:21 PM
 */
public class RegexConstants {
    /**
     * integer expression with no sign
     */
    public static final String INTEGER_POS = "\\d+";
    /**
     * About new lines, sources at http://en.wikipedia.org/wiki/Newline:
     * </p>
     * Systems based on ASCII or a compatible character set use either LF (Line
     * feed, '\n', 0x0A, 10 in decimal) or CR (Carriage return, '\r', 0x0D, 13
     * in decimal) individually, or CR followed by LF (CR+LF, 0x0D 0x0A). These
     * characters are based on printer commands: The line feed indicated that
     * one line of paper should feed out of the printer, and a carriage return
     * indicated that the printer carriage should return to the beginning of the
     * current line.
     * </p>
     * <ul>
     * <li>LF: Multics, Unix and Unix-like systems (GNU/Linux, AIX, Xenix, Mac
     * OS X, FreeBSD, etc.), BeOS, Amiga, RISC OS, and others</li>
     * <li>CR+LF: DEC RT-11 and most other early non-Unix, non-IBM OSes, CP/M,
     * MP/M, MS-DOS, OS/2, Microsoft Windows, Symbian OS</li>
     * <li>CR: Commodore 8-bit machines, TRS-80, Apple II family, Mac OS up to
     * version 9 and OS-9</li>
     * </ul>
     */
    public static final String LINE_DELIMITOR = "(?:\r\n|\n|\r)";
    /**
     * algebraic sign
     */
    private static final String SIGN = "[+-]";
    /**
     * integer expression
     */
    public static final String INTEGER = SIGN + "?\\d+";
    /**
     * float expression
     */
    public static final String REAL = SIGN + "?\\d+\\.?\\d*(?:[eE][-+]?\\d+)?";

    private RegexConstants() { }
}
