package omics.util.utils;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Set utilities.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 28 Mar 2020, 11:02 PM
 */
public class SetUtils
{
    /**
     * return count in (Abc, aBc, ABc, abC, AbC, aBC, ABC)
     *
     * @param a a set
     * @param b a set
     * @param c a set
     * @return set intersection regions.
     */
    public static <T> int[] venn(Set<T> a, Set<T> b, Set<T> c)
    {
        Sets.SetView<T> s37 = Sets.intersection(a, b);
        Sets.SetView<T> s57 = Sets.intersection(a, c);
        Sets.SetView<T> s67 = Sets.intersection(b, c);
        Sets.SetView<T> s7 = Sets.intersection(s37, s57);

        int s3_7 = s37.size();
        int s5_7 = s57.size();
        int s6_7 = s67.size();

        int[] count = new int[7];
        count[6] = s7.size();
        count[5] = s6_7 - count[6];
        count[4] = s5_7 - count[6];
        count[3] = c.size() - s5_7 - count[5];
        count[2] = s3_7 - count[6];
        count[1] = b.size() - s6_7 - count[2];
        count[0] = a.size() - s5_7 - count[2];
        return count;
    }

    /**
     * return count in (Ab, aB, AB)
     *
     * @param a a set
     * @param b a set
     * @return set intersection regions.
     */
    public static <T> int[] venn(Set<T> a, Set<T> b)
    {
        int[] count = new int[3];

        Sets.SetView<T> s = Sets.intersection(a, b);
        count[2] = s.size();
        count[0] = a.size() - count[2];
        count[1] = b.size() - count[2];

        return count;
    }
}
