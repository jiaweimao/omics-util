package omics.util.utils;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

import java.util.Arrays;
import java.util.Collection;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkElementIndex;


/**
 * Utils for common string operation.
 *
 * @author JiaweiMao
 * @version 1.4.0
 * @since 08 Mar 2017, 3:33 PM
 */
public class StringUtils
{
    public static final String SEP0 = ",";
    /**
     * commons separator
     */
    public static final String SEP1 = ";";
    /**
     * double separator, not easy to be confused
     */
    public static final String SEP2 = "::";

    /**
     * Represents a failed index search.
     */
    public static final int INDEX_NOT_FOUND = -1;

    /**
     * <p>{@code StringUtils} instances should NOT be constructed in standard programming. Instead, the class should be
     * used as {@code StringUtils.trim(" foo ");}.</p> <p> <p>This constructor is public to permit tools that require a
     * JavaBean instance to operate.</p>
     */
    private StringUtils()
    {
        super();
    }

    /**
     * <p>Checks if CharSequence contains a search CharSequence irrespective of case, handling {@code null}.
     * Case-insensitivity is defined as by {@link String#equalsIgnoreCase(String)}. <p> <p>A {@code null} CharSequence
     * will return {@code false}.</p>
     * <p>
     * <pre>
     * StringUtils.containsIgnoreCase(null, *) = false
     * StringUtils.containsIgnoreCase(*, null) = false
     * StringUtils.containsIgnoreCase("", "") = true
     * StringUtils.containsIgnoreCase("abc", "") = true
     * StringUtils.containsIgnoreCase("abc", "a") = true
     * StringUtils.containsIgnoreCase("abc", "z") = false
     * StringUtils.containsIgnoreCase("abc", "A") = true
     * StringUtils.containsIgnoreCase("abc", "Z") = false
     * </pre>
     *
     * @param str       the CharSequence to check, may be null
     * @param searchStr the CharSequence to find, may be null
     * @return true if the CharSequence contains the search CharSequence irrespective of case or false if not or {@code
     * null} string input
     */
    public static boolean containsIgnoreCase(final CharSequence str, final CharSequence searchStr)
    {
        if (str == null || searchStr == null) {
            return false;
        }
        final int len = searchStr.length();
        final int max = str.length() - len;
        for (int i = 0; i <= max; i++) {
            if (regionMatches(str, true, i, searchStr, 0, len)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Green implementation of regionMatches.
     *
     * @param cs         the {@code CharSequence} to be processed
     * @param ignoreCase whether or not to be case insensitive
     * @param thisStart  the index to start on the {@code cs} CharSequence
     * @param substring  the {@code CharSequence} to be looked for
     * @param start      the index to start on the {@code substring} CharSequence
     * @param length     character length of the region
     * @return whether the region matched
     */
    static boolean regionMatches(final CharSequence cs, final boolean ignoreCase, final int thisStart,
            final CharSequence substring, final int start, final int length)
    {
        if (cs instanceof String && substring instanceof String) {
            return ((String) cs).regionMatches(ignoreCase, thisStart, (String) substring, start, length);
        }
        int index1 = thisStart;
        int index2 = start;
        int tmpLen = length;

        // Extract these first so we detect NPEs the same as the java.lang.String version
        final int srcLen = cs.length() - thisStart;
        final int otherLen = substring.length() - start;

        // Check for invalid parameters
        if (thisStart < 0 || start < 0 || length < 0) {
            return false;
        }

        // Check that the regions are long enough
        if (srcLen < length || otherLen < length) {
            return false;
        }

        while (tmpLen-- > 0) {
            final char c1 = cs.charAt(index1++);
            final char c2 = substring.charAt(index2++);

            if (c1 == c2) {
                continue;
            }

            if (!ignoreCase) {
                return false;
            }

            // The same check as in String.regionMatches():
            if (Character.toUpperCase(c1) != Character.toUpperCase(c2)
                    && Character.toLowerCase(c1) != Character.toLowerCase(c2)) {
                return false;
            }
        }

        return true;
    }

    /**
     * <p>Counts how many times the char appears in the given string.</p>
     *
     * <p>A {@code null} or empty ("") String input returns {@code 0}.</p>
     *
     * <pre>
     * StringUtils.countMatches(null, *)       = 0
     * StringUtils.countMatches("", *)         = 0
     * StringUtils.countMatches("abba", 0)  = 0
     * StringUtils.countMatches("abba", 'a')   = 2
     * StringUtils.countMatches("abba", 'b')  = 2
     * StringUtils.countMatches("abba", 'x') = 0
     * </pre>
     *
     * @param str the CharSequence to check, may be null
     * @param ch  the char to count
     * @return the number of occurrences, 0 if the CharSequence is {@code null}
     */
    public static int countMatches(final CharSequence str, final char ch)
    {
        if (isEmpty(str)) {
            return 0;
        }
        int count = 0;
        // We could also call str.toCharArray() for faster look ups but that would generate more garbage.
        for (int i = 0; i < str.length(); i++) {
            if (ch == str.charAt(i)) {
                count++;
            }
        }
        return count;
    }

    /**
     * <p>Counts how many times the substring appears in the larger string.</p>
     *
     * <p>A {@code null} or empty ("") String input returns {@code 0}.</p>
     *
     * <pre>
     * StringUtils.countMatches(null, *)       = 0
     * StringUtils.countMatches("", *)         = 0
     * StringUtils.countMatches("abba", null)  = 0
     * StringUtils.countMatches("abba", "")    = 0
     * StringUtils.countMatches("abba", "a")   = 2
     * StringUtils.countMatches("abba", "ab")  = 1
     * StringUtils.countMatches("abba", "xxx") = 0
     * </pre>
     *
     * @param str the CharSequence to check, may be null
     * @param sub the substring to count, may be null
     * @return the number of occurrences, 0 if either CharSequence is {@code null}
     */
    public static int countMatches(final CharSequence str, final CharSequence sub)
    {
        if (isEmpty(str) || isEmpty(sub)) {
            return 0;
        }
        int count = 0;
        int idx = 0;
        while ((idx = indexOf(str, sub, idx)) != INDEX_NOT_FOUND) {
            count++;
            idx += sub.length();
        }
        return count;
    }

    /**
     * Used by the indexOf(CharSequence methods) as a green implementation of indexOf.
     *
     * @param cs         the {@code CharSequence} to be processed
     * @param searchChar the {@code CharSequence} to be searched for
     * @param start      the start index
     * @return the index where the search sequence was found
     */
    static int indexOf(final CharSequence cs, final CharSequence searchChar, final int start)
    {
        return cs.toString().indexOf(searchChar.toString(), start);
    }


    /**
     * Convert number to super script.
     *
     * @param str a string.
     * @return string with number converted to superscript.
     */
    public static String superscript(String str)
    {
        str = str.replaceAll("\\+", "⁺");
        str = str.replaceAll("-", "⁻");
        str = str.replaceAll("0", "⁰");
        str = str.replaceAll("1", "¹");
        str = str.replaceAll("2", "²");
        str = str.replaceAll("3", "³");
        str = str.replaceAll("4", "⁴");
        str = str.replaceAll("5", "⁵");
        str = str.replaceAll("6", "⁶");
        str = str.replaceAll("7", "⁷");
        str = str.replaceAll("8", "⁸");
        str = str.replaceAll("9", "⁹");
        return str;
    }

    public static String subscript(String str)
    {
        str = str.replaceAll("0", "₀");
        str = str.replaceAll("1", "₁");
        str = str.replaceAll("2", "₂");
        str = str.replaceAll("3", "₃");
        str = str.replaceAll("4", "₄");
        str = str.replaceAll("5", "₅");
        str = str.replaceAll("6", "₆");
        str = str.replaceAll("7", "₇");
        str = str.replaceAll("8", "₈");
        str = str.replaceAll("9", "₉");
        return str;
    }

    /**
     * <p>Checks if a CharSequence is empty ("") or null.</p> <p>
     * <pre>
     * StringUtils.isEmpty(null)      = true
     * StringUtils.isEmpty("")        = true
     * StringUtils.isEmpty(" ")       = false
     * StringUtils.isEmpty("bob")     = false
     * StringUtils.isEmpty("  bob  ") = false
     * </pre>
     * <p> <p>NOTE: This method changed in Lang version 2.0. It no longer trims the CharSequence. That functionality is
     * available in isBlank().</p>
     *
     * @param cs the CharSequence to check, may be null
     * @return {@code true} if the CharSequence is empty or null
     * @since 3.0 Changed signature from isEmpty(String) to isEmpty(CharSequence)
     */
    public static boolean isEmpty(final CharSequence cs)
    {
        return cs == null || cs.length() == 0;
    }

    /**
     * <p>Checks if a CharSequence is not empty ("") and not null.</p>
     * <p>
     * <pre>
     * StringUtils.isNotEmpty(null)      = false
     * StringUtils.isNotEmpty("")        = false
     * StringUtils.isNotEmpty(" ")       = true
     * StringUtils.isNotEmpty("bob")     = true
     * StringUtils.isNotEmpty("  bob  ") = true
     * </pre>
     *
     * @param cs the CharSequence to check, may be null
     * @return {@code true} if the CharSequence is not empty and not null
     * @since 3.0 Changed signature from isNotEmpty(String) to isNotEmpty(CharSequence)
     */
    public static boolean isNotEmpty(final CharSequence cs)
    {
        return !isEmpty(cs);
    }

    /**
     * This method shuffles the characters in the sequence.
     *
     * @return shuffle sequence of the original protein sequence.
     */
    public static String shuffle(String aSequence)
    {
        char[] chars = aSequence.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            int j = (int) (Math.random() * chars.length);
            if (i != j) {
                char temp = chars[i];
                chars[i] = chars[j];
                chars[j] = temp;
            }
        }

        return new String(chars);
    }

    /**
     * Reverses the characters in the sequence.
     *
     * @return String with the reversed sequence.
     */
    public static String reverse(String aSequence)
    {
        StringBuilder reversed = new StringBuilder(aSequence.length());
        for (int i = 1; i <= aSequence.length(); i++) {
            reversed.append(aSequence.charAt(aSequence.length() - i));
        }
        return reversed.toString().trim();
    }

    /**
     * Return the index of given pattern.
     *
     * @param seq     sequence to search
     * @param pattern {@link Pattern} to match
     * @return indexes of all patterns
     */
    public static int[] indexOf(final CharSequence seq, Pattern pattern)
    {
        Matcher matcher = pattern.matcher(seq);
        IntList list = new IntArrayList();
        while (matcher.find()) {
            int id = matcher.start();
            list.add(id);
        }
        return list.toIntArray();
    }

    /**
     * Return true if the sequence contains a given pattern.
     *
     * @param sequence sequence to test
     * @param pattern  pattern to match
     * @return true if the sequence contains the pattern
     */
    public static boolean contains(final CharSequence sequence, Pattern pattern)
    {
        Matcher matcher = pattern.matcher(sequence);
        return matcher.find();
    }

    /**
     * Return all the index of <code>shortSeq</code> in <code>seq</code>
     *
     * @param seq      a string sequence
     * @param shortSeq a short string to be searched
     * @return all the indexes of <code>searchSeq</code> in <code>seq</code>, or null if not in.
     */
    public static int[] indexOf(final CharSequence seq, final CharSequence shortSeq)
    {
        if (seq == null || shortSeq == null)
            return null;

        IntList list = new IntArrayList();
        int index;
        int start = 0;
        while ((index = seq.toString().indexOf(shortSeq.toString(), start)) != -1) {
            list.add(index);
            start = index + shortSeq.length();
        }
        return list.toIntArray();
    }

    /**
     * keep the string part, and ignore others.
     *
     * @param org the original string
     * @return new string without non alph
     */
    public static String remainString(String org)
    {
        char[] aas = org.toCharArray();
        StringBuilder tmp = new StringBuilder();
        for (char aa : aas) {
            if (Character.isAlphabetic(aa))
                tmp.append(aa);
        }
        return tmp.toString();
    }

    /**
     * Parse int from string, ignore none int part
     *
     * @param org the original string
     * @return int
     */
    public static int parseIntFromString(String org)
    {
        char[] aas = org.toCharArray();
        StringBuilder tmp = new StringBuilder();
        for (char aa : aas) {
            if (Character.isDigit(aa)) {
                tmp.append(aa);
            } else
                break;
        }
        return Integer.valueOf(tmp.toString());
    }

    /**
     * Return a subString in <code>aString</code> with <code>length</code> before the index, if the sub string length is
     * not enough, makeup with '_'
     *
     * @param aString a string
     * @param index   index in the string.
     * @param length  length of the sub string.
     * @return the substring with length before index.
     */
    public static String preString(String aString, int index, int length, char replace)
    {
        if (index < length) {
            StringBuilder buffer = new StringBuilder();
            for (int i = 0; i < (length - index); i++) {
                buffer.append(replace);
            }
            buffer.append(aString.substring(0, index));
            return buffer.toString();
        } else {
            return aString.substring(index - length, index);
        }
    }

    /**
     * return a subString in <code>aString</code> with <code>length</code> after the <code>index</code>, if the chars
     * after index is not enough, makrup with <code>replace</code>
     *
     * @param aString 原始字符串
     * @param index   索引
     * @param length  子字符串长度
     * @return index后长度为length的子字符串
     */
    public static String endString(String aString, int index, int length, char replace)
    {
        if (index > aString.length() - length) {

            StringBuilder buffer = new StringBuilder(aString.substring(index));
            for (int i = 0; i < length - aString.length() + index; i++) {
                buffer.append(replace);
            }
            return buffer.toString();
        } else {
            return aString.substring(index, index + length);
        }
    }

    /**
     * return substring of the protein sequence.
     *
     * @param index       index in the protein.
     * @param left        length before index
     * @param right       length after index
     * @param replaceChar char to use if there is no char of given length.
     * @return the sub string.
     */
    public static String subString(String rawString, int index, int left, int right, char replaceChar)
    {
        checkElementIndex(index, rawString.length() + 1);

        int len = left + right + 1;
        char[] result = new char[len];

        int lId = index - left;
        int lCount = 0;
        if (lId < 0) {
            lCount -= lId;
            lId = 0;
        }

        int rId = index + right + 1;
        int rCount = 0;
        if (rId > rawString.length()) {
            rCount = rId - rawString.length();
            rId = rawString.length();
        }

        if (lCount > 0)
            Arrays.fill(result, 0, lCount, replaceChar);

        int num = lCount;
        for (int id = lId; id < rId; id++) {
            result[num] = rawString.charAt(id);
            num++;
        }

        if (rCount > 0)
            Arrays.fill(result, num, len, replaceChar);

        return new String(result);
    }

    /**
     * Create a String with the {@code value} repeat count times.
     *
     * @param value value to repeat
     * @param n     repeat count
     * @return String with n repeats of the value.
     */
    public static String repeat(String value, int n)
    {
        checkArgument(n > 0);

        StringBuilder builder = new StringBuilder();
        while (n > 0) {
            builder.append(value);
            n--;
        }
        return builder.toString();
    }

    /**
     * Returns a string whose value is this string, with any leading whitespace removed.
     *
     * @param value String value to trim
     * @return A string whose value is this string, with any leading  white space removed, or this string if it has no
     * leading white space.
     */
    public static String trimLeft(String value)
    {
        int len = value.length();
        int st = 0;

        char[] val = value.toCharArray();

        while ((st < len) && (val[st] <= ' ')) {
            st++;
        }
        return (st > 0) ? value.substring(st, len) : value;
    }

    /**
     * join sequence collection with given delimiter, element's toString() method is called
     * in the join method.
     *
     * @param delimiter a sequence of characters that is used to separate each
     *                  of the {@code elements} in the resulting {@code String}
     * @param elements  an {@code Collection} that will have its {@code elements}
     *                  joined together.
     * @return a new {@code String} that is composed from the {@code elements}
     * argument
     */
    public static String join(CharSequence delimiter, Collection elements)
    {
        requireNonNull(delimiter);
        requireNonNull(elements);
        StringJoiner joiner = new StringJoiner(delimiter);
        for (Object element : elements) {
            joiner.add(element.toString());
        }
        return joiner.toString();
    }

    /**
     * join sequence array with given delimiter, element's toString() method is called
     * in the join method.
     *
     * @param delimiter a sequence of characters that is used to separate each
     *                  of the {@code elements} in the resulting {@code String}
     * @param elements  an {@code Collection} that will have its {@code elements}
     *                  joined together.
     * @return a new {@code String} that is composed from the {@code elements}
     * argument
     */
    public static <T> String join(CharSequence delimiter, T[] elements)
    {
        requireNonNull(delimiter);
        requireNonNull(elements);

        StringJoiner joiner = new StringJoiner(delimiter);
        for (T element : elements) {
            joiner.add(String.valueOf(element));
        }
        return joiner.toString();
    }

    /**
     * join sequence array with given delimiter, element's toString() method is called
     * in the join method.
     *
     * @param delimiter a sequence of characters that is used to separate each
     *                  of the {@code elements} in the resulting {@code String}
     * @param elements  an {@code Collection} that will have its {@code elements}
     *                  joined together.
     * @return a new {@code String} that is composed from the {@code elements}
     * argument
     */
    public static String join(CharSequence delimiter, int[] elements)
    {
        requireNonNull(delimiter);
        requireNonNull(elements);

        StringJoiner joiner = new StringJoiner(delimiter);
        for (int element : elements) {
            joiner.add(String.valueOf(element));
        }
        return joiner.toString();
    }

    /**
     * join sequence array with given delimiter, element's toString() method is called
     * in the join method.
     *
     * @param delimiter a sequence of characters that is used to separate each
     *                  of the {@code elements} in the resulting {@code String}
     * @param elements  an {@code Collection} that will have its {@code elements}
     *                  joined together.
     * @return a new {@code String} that is composed from the {@code elements}
     * argument
     */
    public static String join(CharSequence delimiter, double[] elements)
    {
        requireNonNull(delimiter);
        requireNonNull(elements);

        StringJoiner joiner = new StringJoiner(delimiter);
        for (double element : elements) {
            joiner.add(String.valueOf(element));
        }
        return joiner.toString();
    }

    /**
     * join sequence array with given delimiter, element's toString() method is called
     * in the join method.
     *
     * @param delimiter a sequence of characters that is used to separate each
     *                  of the {@code elements} in the resulting {@code String}
     * @param elements  an {@code Collection} that will have its {@code elements}
     *                  joined together.
     * @return a new {@code String} that is composed from the {@code elements}
     * argument
     */
    public static String join(CharSequence delimiter, float[] elements)
    {
        requireNonNull(delimiter);
        requireNonNull(elements);

        StringJoiner joiner = new StringJoiner(delimiter);
        for (float element : elements) {
            joiner.add(String.valueOf(element));
        }
        return joiner.toString();
    }
}
