package omics.util.utils;

/**
 * Helpers for {@code java.lang.System}.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 06 May 2017, 6:23 PM
 */
public class SystemUtils
{
    /**
     * <p> The {@code java.version} System Property. Java version number. </p> <p> Defaults to {@code null} if the
     * runtime does not have security access to read this property or the property does not exist. </p> <p> This value
     * is initialized when the class is loaded. If {@link System#setProperty(String, String)} or {@link
     * System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of sync
     * with that System property. </p>
     *
     * @since Java 1.1
     */
    public static final String JAVA_VERSION = getSystemProperty("java.version");
    /**
     * <p> The {@code java.specification.version} System Property. Java Runtime Environment specification version. </p>
     * <p> Defaults to {@code null} if the runtime does not have security access to read this property or the property
     * does not exist. </p> <p> This value is initialized when the class is loaded. If {@link System#setProperty(String,
     * String)} or {@link System#setProperties(java.util.Properties)} is called after this class is loaded, the value
     * will be out of sync with that System property. </p>
     *
     * @since Java 1.3
     */
    public static final String JAVA_SPECIFICATION_VERSION = getSystemProperty("java.specification.version");
    /**
     * <p> The {@code user.language} System Property. User's language code, such as {@code "en"}. </p> <p> Defaults to
     * {@code null} if the runtime does not have security access to read this property or the property does not exist.
     * </p> <p> This value is initialized when the class is loaded. If {@link System#setProperty(String, String)} or
     * {@link System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of
     * sync with that System property. </p>
     *
     * @since Java 1.2
     */
    public static final String USER_LANGUAGE = getSystemProperty("user.language");
    /**
     * <p> The {@code user.name} System Property. User's account name. </p> <p> Defaults to {@code null} if the runtime
     * does not have security access to read this property or the property does not exist. </p> <p> This value is
     * initialized when the class is loaded. If {@link System#setProperty(String, String)} or {@link
     * System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of sync
     * with that System property. </p>
     *
     * @since Java 1.1
     */
    public static final String USER_NAME = getSystemProperty("user.name");
    /**
     * <p> The {@code user.timezone} System Property. For example: {@code "America/Los_Angeles"}. </p> <p> Defaults to
     * {@code null} if the runtime does not have security access to read this property or the property does not exist.
     * </p> <p> This value is initialized when the class is loaded. If {@link System#setProperty(String, String)} or
     * {@link System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of
     * sync with that System property. </p>
     *
     * @since 2.1
     */
    public static final String USER_TIMEZONE = getSystemProperty("user.timezone");
    /**
     * <p> The {@code line.separator} System Property. Line separator (<code>&quot;\n&quot;</code> on UNIX). </p> <p>
     * Defaults to {@code null} if the runtime does not have security access to read this property or the property does
     * not exist. </p> <p> This value is initialized when the class is loaded. If {@link System#setProperty(String,
     * String)} or {@link System#setProperties(java.util.Properties)} is called after this class is loaded, the value
     * will be out of sync with that System property. </p>
     *
     * @since Java 1.1
     */
    public static final String LINE_SEPARATOR = getSystemProperty("line.separator");
    /**
     * <p> The {@code os.arch} System Property. Operating system architecture. </p> <p> Defaults to {@code null} if the
     * runtime does not have security access to read this property or the property does not exist. </p> <p> This value
     * is initialized when the class is loaded. If {@link System#setProperty(String, String)} or {@link
     * System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of sync
     * with that System property. </p>
     *
     * @since Java 1.1
     */
    public static final String OS_ARCH = getSystemProperty("os.arch");
    /**
     * <p> The {@code os.name} System Property. Operating system name. </p> <p> Defaults to {@code null} if the runtime
     * does not have security access to read this property or the property does not exist. </p> <p> This value is
     * initialized when the class is loaded. If {@link System#setProperty(String, String)} or {@link
     * System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of sync
     * with that System property. </p>
     */
    public static final String OS_NAME = getSystemProperty("os.name");

    public static final String OS_VERSION = getSystemProperty("os.version");

    /**
     * <p> Is {@code true} if this is Linux. </p> <p> The field will return {@code false} if {@code OS_NAME} is {@code
     * null}. </p>
     *
     * @since 2.0
     */
    public static final boolean IS_OS_LINUX = getOSMatchesName("Linux") || getOSMatchesName("LINUX");
    /**
     * <p> Is {@code true} if this is Mac. </p> <p> The field will return {@code false} if {@code OS_NAME} is {@code
     * null}. </p>
     *
     * @since 2.0
     */
    public static final boolean IS_OS_MAC_OSX = getOSMatchesName("Mac OS X");
    /**
     * <p> Is {@code true} if this is Java version 1.6 (also 1.6.x versions). </p> <p> The field will return {@code
     * false} if {@link #JAVA_VERSION} is {@code null}. </p>
     */
    public static final boolean IS_JAVA_1_6 = getJavaVersionMatches("1.6");
    /**
     * The prefix String for all Windows OS.
     */
    private static final String OS_NAME_WINDOWS_PREFIX = "Windows";
    /**
     * <p> Is {@code true} if this is Windows. </p> <p> The field will return {@code false} if {@code OS_NAME} is {@code
     * null}. </p>
     *
     * @since 2.0
     */
    public static final boolean IS_OS_WINDOWS = getOSMatchesName(OS_NAME_WINDOWS_PREFIX);
    /**
     * The System property key for the user directory.
     */
    private static final String USER_DIR_KEY = "user.dir";
    /**
     * <p> The {@code user.dir} System Property. User's current working directory. </p> <p> Defaults to {@code null} if
     * the runtime does not have security access to read this property or the property does not exist. </p> <p> This
     * value is initialized when the class is loaded. If {@link System#setProperty(String, String)} or {@link
     * System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of sync
     * with that System property. </p>
     *
     * @since Java 1.1
     */
    public static final String USER_DIR = getSystemProperty(USER_DIR_KEY);
    /**
     * The System property key for the user home directory.
     */
    private static final String USER_HOME_KEY = "user.home";
    /**
     * <p> The {@code user.home} System Property. User's home directory. </p> <p> Defaults to {@code null} if the
     * runtime does not have security access to read this property or the property does not exist. </p> <p> This value
     * is initialized when the class is loaded. If {@link System#setProperty(String, String)} or {@link
     * System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of sync
     * with that System property. </p>
     *
     * @since Java 1.1
     */
    public static final String USER_HOME = getSystemProperty(USER_HOME_KEY);
    /**
     * The System property key for the Java home directory.
     */
    private static final String JAVA_HOME_KEY = "java.home";
    /**
     * <p> The {@code java.home} System Property. Java installation directory. </p> <p> Defaults to {@code null} if the
     * runtime does not have security access to read this property or the property does not exist. </p> <p> This value
     * is initialized when the class is loaded. If {@link System#setProperty(String, String)} or {@link
     * System#setProperties(java.util.Properties)} is called after this class is loaded, the value will be out of sync
     * with that System property. </p>
     *
     * @since Java 1.1
     */
    public static final String JAVA_HOME = getSystemProperty(JAVA_HOME_KEY);

    public static final String JAVA_RUNTIME_VERSION = getSystemProperty("java.runtime.version");

    /**
     * name of the java VM.
     */
    public static final String JAVA_VM_NAME = getSystemProperty("java.vm.name");

    /**
     * The {@code java.vm.vendor} System Property.
     */
    public static final String JAVA_VM_VENDOR = getSystemProperty("java.vm.vendor");

    /**
     * @return true if current OS is 64.
     */
    public static boolean isAMD64()
    {
        boolean is64Bit;
        if (IS_OS_WINDOWS)
            is64Bit = (System.getenv("ProgramFiles(x86)") != null);
        else
            is64Bit = OS_ARCH.contains("64");
        return is64Bit;
    }

    /**
     * Decides if the operating system matches. <p> This method is package private instead of private to support unit
     * test invocation. </p>
     *
     * @param osName       the actual OS name
     * @param osNamePrefix the prefix for the expected OS name
     * @return true if matches, or false if not or can't determine
     */
    static boolean isOSNameMatch(final String osName, final String osNamePrefix)
    {
        if (osName == null) {
            return false;
        }
        return osName.startsWith(osNamePrefix);
    }

    /**
     * Decides if the operating system matches.
     *
     * @param osNamePrefix the prefix for the os name
     * @return true if matches, or false if not or can't determine
     */
    private static boolean getOSMatchesName(final String osNamePrefix)
    {
        return isOSNameMatch(OS_NAME, osNamePrefix);
    }

    /**
     * <p> Gets a System property, defaulting to {@code null} if the property cannot be read. </p> <p> If a {@code
     * SecurityException} is caught, the return value is {@code null} and a message is written to {@code System.err}.
     * </p>
     *
     * @param property the system property name
     * @return the system property value or {@code null} if a security problem occurs
     */
    private static String getSystemProperty(final String property)
    {
        try {
            return System.getProperty(property);
        } catch (final SecurityException ex) {
            // we are not allowed to look at this property
            System.err.println("Caught a SecurityException reading the system property '" + property
                    + "'; the SystemUtils property value will default to null.");
            return null;
        }
    }

    /**
     * <p> Decides if the Java version matches. </p>
     *
     * @param versionPrefix the prefix for the java version
     * @return true if matches, or false if not or can't determine
     */
    private static boolean getJavaVersionMatches(final String versionPrefix)
    {
        return isJavaVersionMatch(JAVA_SPECIFICATION_VERSION, versionPrefix);
    }

    /**
     * <p> Decides if the Java version matches. </p> <p> This method is package private instead of private to support
     * unit test invocation. </p>
     *
     * @param version       the actual Java version
     * @param versionPrefix the prefix for the expected Java version
     * @return true if matches, or false if not or can't determine
     */
    static boolean isJavaVersionMatch(final String version, final String versionPrefix)
    {
        if (version == null) {
            return false;
        }
        return version.startsWith(versionPrefix);
    }

    /**
     * @return number of available processors.
     */
    public static int getProcessorCount()
    {

        return Runtime.getRuntime().availableProcessors();
    }
}
