package omics.util.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import static java.util.Objects.requireNonNull;
import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * Utilities for zip.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 27 Aug 2018, 5:55 PM
 */
public class ZipUtils
{

    private ZipUtils() { }

    /**
     * Compress source data using deflate algorithm
     *
     * @param uncompressedData data to be compressed
     * @return compressed data
     */
    public static byte[] compressByDeflater(byte[] uncompressedData)
    {
        Objects.requireNonNull(uncompressedData);

        Deflater deflater = new Deflater();
        deflater.setInput(uncompressedData);
        deflater.finish();

        ByteArrayOutputStream bos = new ByteArrayOutputStream(uncompressedData.length);
        byte[] buf = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buf);
            bos.write(buf, 0, count);
        }
        try {
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bos.toByteArray();
    }

    public static byte[] decompress(byte[] bytes, int compressedLen, int uncompressedLen) throws DataFormatException
    {
        requireNonNull(bytes);
        checkArgument(bytes.length > 0);
        checkArgument(compressedLen > 0);
        checkArgument(uncompressedLen > 0);

        Inflater inflater = new Inflater();
        inflater.setInput(bytes, 0, compressedLen);
        byte[] result = new byte[uncompressedLen];
        inflater.inflate(result);
        inflater.end();

        return result;
    }

    /**
     * decompress source data using inflater algorithm
     *
     * @param data compressed data
     * @return uncompressed data
     */
    public static byte[] decompressByInflater(byte[] data)
    {
        try {
            Inflater inflater = new Inflater();
            inflater.setInput(data);

            ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
            byte[] buf = new byte[1024];
            while (!inflater.finished()) {
                int count = inflater.inflate(buf);
                bos.write(buf, 0, count);
            }
            bos.close();

            return bos.toByteArray();
        } catch (DataFormatException | IOException e) {
            throw new IllegalStateException("Decompress of binary data failed.");
        }
    }
}
