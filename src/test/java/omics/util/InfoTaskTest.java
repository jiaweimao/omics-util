package omics.util;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Feb 2019, 4:20 PM
 */
public class InfoTaskTest
{
    public static void main(String[] args) throws Exception
    {
        OmicsTask<Void> task = new OmicsTask<>()
        {
            @Override
            public void go() throws Exception
            {
                int total = 100;
                for (int i = 0; i < total; i++) {
                    Thread.sleep(500);
                    updateProgress(i + 1, total);
                    System.out.println("index=" + i);
                }
            }
        };

        task.progressProperty().addListener(evt -> System.out.println(evt.getNewValue()));
        task.go();
    }

}