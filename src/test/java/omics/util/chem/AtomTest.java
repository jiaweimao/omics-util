/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.util.chem;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Mar 2017, 7:44 PM
 */
class AtomTest
{

    @Test
    void calcAverageMass()
    {
        PeriodicTable pt = PeriodicTable.getInstance();

        double eps = 0.000001;
        assertEquals(1.00794, PeriodicTable.H.getAverageMass(), eps);
        assertEquals(2.014101779, PeriodicTable.H2.getAverageMass(), eps);
        assertEquals(12.0107, PeriodicTable.C.getAverageMass(), eps);
        assertEquals(14.0067, PeriodicTable.N.getAverageMass(), eps);
        assertEquals(15.9994, PeriodicTable.O.getAverageMass(), eps);
        assertEquals(30.973761, PeriodicTable.P.getAverageMass(), eps);
        assertEquals(32.065, PeriodicTable.S.getAverageMass(), eps);

        assertEquals(12.0, PeriodicTable.C_MASS, eps);
        assertEquals(13.0033548378, PeriodicTable.C13_MASS, eps);

        assertEquals(1.007825032, PeriodicTable.H_MASS, eps);
        assertEquals(2.014101778, pt.getAtom(AtomicSymbol.H, 2).getMass(), eps);


        assertEquals(14.003074005, PeriodicTable.N_MASS, eps);
        assertEquals(15.000108898, pt.getAtom(AtomicSymbol.N, 15).getMass(), eps);

        assertEquals(15.99491463, PeriodicTable.O_MASS, eps);
        assertEquals(16.9991312, pt.getAtom(AtomicSymbol.O, 17).getMass(), eps);
        assertEquals(17.9991604, pt.getAtom(AtomicSymbol.O, 18).getMass(), eps);

        assertEquals(31.9720707, PeriodicTable.S_MASS, eps);

        Atom h2 = PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2);
        assertEquals(h2.getMass(), h2.getAverageMass(), eps);
    }


    @Test
    void testConstructorNullSymbol()
    {
        assertThrows(NullPointerException.class, () -> new Atom(null, 1, 1, 1, 0.9));
    }

    @Test
    void testConstructorNegativeMass()
    {
        assertThrows(IllegalArgumentException.class, () -> new Atom(AtomicSymbol.H, 1, 1, -1, 0.9));
    }

    @Test
    void testConstructorNegativeAbundance()
    {
        assertThrows(IllegalArgumentException.class, () -> new Atom(AtomicSymbol.H, 1, 1, 1, -0.1));
    }

    @Test
    void testConstructorLargerThan1Abundance()
    {
        assertThrows(IllegalArgumentException.class, () -> new Atom(AtomicSymbol.H, 1, 1, 1, 1.5));
    }

    @Test
    void testEquals()
    {
        assertEquals(PeriodicTable.H, new Atom(AtomicSymbol.H, 1, 1, 1.007825032, 0.99985));
        assertNotEquals(PeriodicTable.H, PeriodicTable.C);
    }

    @Test
    void testHashCode()
    {
        assertEquals(PeriodicTable.H.hashCode(), new Atom(AtomicSymbol.H, 1, 1, 1.007825032, 0.99985).hashCode());
        assertNotEquals(PeriodicTable.H.hashCode(), PeriodicTable.C.hashCode());
    }

    @Test
    void testToString()
    {
        assertEquals("C[13]", PeriodicTable.C13.toString());
        assertEquals("C", PeriodicTable.C.toString());
    }

    @Test
    void testCompareTo()
    {
        Atom h = PeriodicTable.H;
        Atom c13 = PeriodicTable.C13;
        Atom c = PeriodicTable.C;

        assertEquals(0, h.compareTo(h));
        assertEquals(1, c13.compareTo(c));
        assertEquals(-1, c.compareTo(c13));
    }

    @Test
    void testGetSymbol()
    {
        Atom c13 = PeriodicTable.getInstance().getAtom(AtomicSymbol.C, 13);
        assertEquals(AtomicSymbol.C, c13.getSymbol());
    }
}