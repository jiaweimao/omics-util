/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.util.chem;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Mar 2017, 7:42 PM
 */
class AtomicCompositionParserTest
{
    private AtomicCompositionParser parser = AtomicCompositionParser.getInstance();

    // Compare mass against this site:
    // http://www.bmrb.wisc.edu/metabolomics/mol_mass.php
    // http://en.wikipedia.org/wiki/Chemical_file_format
    @Test
    void parseNBuild()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("C[13]2H-3ON-1(2+)", atomCounterMap);

        assertEquals(2, charge);
        assertEquals(4, atomCounterMap.size());
        assertEquals(2, atomCounterMap.getInt(PeriodicTable.getInstance().getAtom(AtomicSymbol.C, 13)));
        assertEquals(-3, atomCounterMap.getInt(PeriodicTable.H));
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.O));
        assertEquals(-1, atomCounterMap.getInt(PeriodicTable.N));
    }

    @Test
    void parseNBuild2()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("NH3", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(3, atomCounterMap.getInt(PeriodicTable.H));
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.N));

        charge = parser.parse("N[15]H3", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.getInstance().getAtom(AtomicSymbol.N, 15)));
        assertEquals(3, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseNBuildFormulaWithInternalRepetition()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("PO4H2(CH2)12CH3", atomCounterMap);
        assertEquals(0, charge);
        assertEquals(4, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.P));
        assertEquals(13, atomCounterMap.getInt(PeriodicTable.C));
        assertEquals(29, atomCounterMap.getInt(PeriodicTable.H));
        assertEquals(4, atomCounterMap.getInt(PeriodicTable.O));

        charge = parser.parse("PO4H2(C[13]H2)12C[13]H3", atomCounterMap);
        assertEquals(0, charge);
        assertEquals(4, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.P));
        assertEquals(13, atomCounterMap.getInt(PeriodicTable.getInstance().getAtom(AtomicSymbol.C, 13)));
        assertEquals(29, atomCounterMap.getInt(PeriodicTable.H));
        assertEquals(4, atomCounterMap.getInt(PeriodicTable.O));
    }

    @Test
    void parseSimpleFormula()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("H3O(+)", atomCounterMap);

        assertEquals(1, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(3, atomCounterMap.getInt(PeriodicTable.H));
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.O));
    }

    @Test
    void parseSimpleParticle()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("H", atomCounterMap);
        assertEquals(0, charge);
        assertEquals(1, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.H));

        charge = parser.parse("H(+)", atomCounterMap); // H-(e-)
        assertEquals(1, charge);
        assertEquals(1, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.H));

        charge = parser.parse("H(-)", atomCounterMap);// H+(e-)
        assertEquals(-1, charge);
        assertEquals(1, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseSimpleParticle2()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("H-1", atomCounterMap);
        assertEquals(0, charge);
        assertEquals(1, atomCounterMap.size());
        assertEquals(-1, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseSimpleParticle3()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("H+1", atomCounterMap);
        assertEquals(0, charge);
        assertEquals(1, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseSimpleParticleBadAtomCountFormat()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        assertThrows(IllegalArgumentException.class, () -> parser.parse("H++1", atomCounterMap));
    }

    @Test
    void parseSimpleEmptyParticle()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(0, atomCounterMap.size());
    }

    @Test
    void parseGroup1()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("(CH2)", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(1, atomCounterMap.getInt(PeriodicTable.C));
        assertEquals(2, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseGroup()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("(CH2)10", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(10, atomCounterMap.getInt(PeriodicTable.C));
        assertEquals(20, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseGroupSigned()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("(CH2)+10", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(10, atomCounterMap.getInt(PeriodicTable.C));
        assertEquals(20, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseGroupNeg()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("(CH2)-10", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(-10, atomCounterMap.getInt(PeriodicTable.C));
        assertEquals(-20, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseGroupTricky()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("(C-1H-2)-10", atomCounterMap);

        assertEquals(0, charge);
        assertEquals(2, atomCounterMap.size());
        assertEquals(10, atomCounterMap.getInt(PeriodicTable.C));
        assertEquals(20, atomCounterMap.getInt(PeriodicTable.H));
    }

    @Test
    void parseMinusProton()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();
        int charge = parser.parse("H-1(-)", atomCounterMap);

        assertEquals(-1, charge);
        assertEquals(1, atomCounterMap.size());
        assertEquals(-1, atomCounterMap.getInt(PeriodicTable.H));
    }
}