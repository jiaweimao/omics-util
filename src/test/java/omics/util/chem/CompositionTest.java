/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.util.chem;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import omics.util.protein.AminoAcid;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static omics.util.chem.AtomicSymbol.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Mar 2017, 7:45 PM
 */
class CompositionTest
{
    private double eps = 0.000001;

    @Test
    void testGetNominalMass()
    {
        Composition comp = Composition.parseComposition("C5H7N1O2S0");
        assertEquals(comp.getNominalMass(), 113);

        assertEquals(AminoAcid.G.getComposition().getNominalMass(), 57);
        assertEquals(AminoAcid.A.getComposition().getNominalMass(), 71);
        assertEquals(AminoAcid.S.getComposition().getNominalMass(), 87);
        assertEquals(AminoAcid.P.getComposition().getNominalMass(), 97);
        assertEquals(AminoAcid.V.getComposition().getNominalMass(), 99);
        assertEquals(AminoAcid.T.getComposition().getNominalMass(), 101);
        assertEquals(AminoAcid.L.getComposition().getNominalMass(), 113);
        assertEquals(AminoAcid.I.getComposition().getNominalMass(), 113);
        assertEquals(AminoAcid.N.getComposition().getNominalMass(), 114);
        assertEquals(AminoAcid.D.getComposition().getNominalMass(), 115);
        assertEquals(AminoAcid.Q.getComposition().getNominalMass(), 128);
        assertEquals(AminoAcid.K.getComposition().getNominalMass(), 128);
        assertEquals(AminoAcid.E.getComposition().getNominalMass(), 129);
        assertEquals(AminoAcid.M.getComposition().getNominalMass(), 131);
        assertEquals(AminoAcid.H.getComposition().getNominalMass(), 137);
        assertEquals(AminoAcid.F.getComposition().getNominalMass(), 147);
        assertEquals(AminoAcid.R.getComposition().getNominalMass(), 156);
        assertEquals(AminoAcid.Y.getComposition().getNominalMass(), 163);
        assertEquals(AminoAcid.W.getComposition().getNominalMass(), 186);
    }

    @Test
    void negativeCharge()
    {
        Composition sulfate = new Composition.Builder(AtomicSymbol.S).add(O, 4).charge(-2).build();
        Set<Atom> atoms = sulfate.getAtoms();
        assertTrue(atoms.contains(PeriodicTable.S));
        assertTrue(atoms.contains(PeriodicTable.O));

        assertEquals(4, sulfate.getCount(PeriodicTable.O));
        assertEquals(0, sulfate.getCount(PeriodicTable.C));

        double extractmass = sulfate.getMolecularMass();
        assertEquals(95.95282637981893, extractmass, eps);
    }

    @Test
    void getAtoms()
    {
        Composition molecule = Composition.parseComposition("PO4H2(CH2)12CH3");

        Set<Atom> atoms = molecule.getAtoms();
        assertEquals(4, atoms.size());
        assertTrue(atoms.contains(PeriodicTable.P));
        assertTrue(atoms.contains(PeriodicTable.O));
        assertTrue(atoms.contains(PeriodicTable.C));
        assertTrue(atoms.contains(PeriodicTable.H));
    }

    @Test
    void getMolecularMass()
    {
        Composition composition = new Composition(new Composition.Builder(H).build());
        assertEquals(PeriodicTable.H_MASS, composition.getMolecularMass(), 0.00000000001);

        composition = new Composition(new Composition.Builder(H).charge(1).build());
        assertEquals(PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS, composition.getMolecularMass(), 0.00000000001);

        composition = new Composition(new Composition.Builder(H).charge(-1).build());
        assertEquals(PeriodicTable.H_MASS + PeriodicTable.ELECTRON_MASS, composition.getMolecularMass(), 0.00000000001);

        composition = new Composition(new Composition.Builder(H).charge(2).build());
        assertEquals(PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS * 2, composition.getMolecularMass(),
                0.00000000001);

        composition = new Composition(new Composition.Builder(H).charge(-2).build());
        assertEquals(PeriodicTable.H_MASS + PeriodicTable.ELECTRON_MASS * 2, composition.getMolecularMass(),
                0.00000000001);
    }

    @Test
    void getFormula()
    {
        Composition composition = new Composition(new Composition.Builder(C).add(H, 3).charge(1).build());

        assertEquals(4, composition.size());
        assertEquals(15.022926516, composition.getMolecularMass(), 0.0000001);
        assertEquals("CH3(+)", composition.getFormula());
    }

    @Test
    void getMassDefect()
    {
        Composition composition = Composition.parseComposition("H");
        assertEquals(0.007825032, composition.getMassDefect(), 0.0000000001);

        composition = Composition.parseComposition("H-1");
        assertEquals(-0.007825032, composition.getMassDefect(), 0.0000000001);

        composition = Composition.parseComposition("H2O");
        assertEquals(0.010564694, composition.getMassDefect(), 0.00000001);

        composition = Composition.parseComposition("(H2O)-1");
        assertEquals(-0.010564694, composition.getMassDefect(), 0.00000001);
    }

    @Test
    void positiveCharge()
    {
        Composition composition = new Composition(
                new Composition.Builder(C).add(H, 3).charge(1).build());

        assertEquals("CH3(+)", composition.getFormula());
        assertEquals(1, composition.getCount(PeriodicTable.C));
        assertEquals(3, composition.getCount(PeriodicTable.H));
        assertEquals(15.022926, composition.getMolecularMass(), eps);
        assertEquals(1, composition.getCharge());
    }

    @Test
    void size()
    {
        Composition composition = Composition.parseComposition("C3C6");

        assertEquals(9, composition.getCount(PeriodicTable.C));
        assertEquals(9, composition.size());
    }

    @Test
    void negativeAtom()
    {
        Composition composition = new Composition(
                new Composition.Builder(H, -2).add(O, -1).build());

        assertEquals("H-2O-1", composition.getFormula());
        assertEquals(-2, composition.getCount(PeriodicTable.H));
        assertEquals(-1, composition.getCount(PeriodicTable.O));
        assertEquals(-18.010564, composition.getMolecularMass(), eps);
        assertEquals(0, composition.getCharge());
    }

    @Test
    void parseComposition()
    {
        Composition molecule = Composition.parseComposition("CH3(CH2)2O(CH2)3OH(-)");

        assertEquals("CH3(CH2)2O(CH2)3OH(-)", molecule.getFormula());
    }

    @Test
    void parseComposition2()
    {
        Composition molecule = Composition.parseComposition("C H3");
        assertEquals("C H3", molecule.getFormula());
    }

    @Test
    void parseComposition3()
    {
        assertThrows(IllegalArgumentException.class, () -> Composition.parseComposition("CH3(hello)2O(CH2)3OH(-)"));
        assertThrows(IllegalArgumentException.class, () -> Composition.parseComposition("CH3(CH2)2O(CH2)3OHhello(-)"));
    }

    @Test
    void subtractCompositions()
    {
        Composition composition1 = Composition.parseComposition("H3C");
        Composition composition2 = Composition.parseComposition("H2");
        assertEquals("CH", composition1.subtract(composition2).getFormula());

        composition1 = Composition.parseComposition("H3C");
        composition2 = Composition.parseComposition("H3C");
        assertEquals("", composition1.subtract(composition2).getFormula());

        composition1 = Composition.parseComposition("H3C");
        composition2 = Composition.parseComposition("H4C2");
        assertEquals("C-1H-1", composition1.subtract(composition2).getFormula());
    }


    @Test
    void equals()
    {
        Composition.Builder atomCounterMap1 = new Composition.Builder(C, 2).add(H, 6)
                .add(O, 1);
        Composition composition1 = new Composition(atomCounterMap1.build());

        Composition.Builder atomCounterMap2 = new Composition.Builder(C, 2).add(H, 6)
                .add(O, 1);
        Composition composition2 = new Composition(atomCounterMap2.build());

        assertEquals(composition1, composition2);
    }

    @Test
    void equals2()
    {
        Composition composition1 = Composition.parseComposition("");
        Composition composition2 = Composition.parseComposition("");

        assertEquals(composition1, composition2);
    }

    @Test
    void testParseComposition5()
    {
        Composition composition = Composition.parseComposition("C11H17N1O8");
        assertEquals(291.095416, composition.getMolecularMass(), eps);
    }

    @Test
    void testNegativeGroupOcc()
    {
        Composition mol1 = Composition.parseComposition("(H2O)3(NH3)-3");
        Composition mol2 = Composition.parseComposition("N-3H-3O3");

        assertEquals(mol1, mol2);
    }

    @Test
    void testParseCompositionMass()
    {
        Composition mol = Composition.parseComposition("CH4");
        assertEquals(16.031300, mol.getMolecularMass(), eps);
    }

    @Test
    void testParseCompositionNegativeMass()
    {
        Composition mol = Composition.parseComposition("C-1H-4");
        assertEquals(-16.031300, mol.getMolecularMass(), eps);
    }

    // Compare mass against this site:
    // http://www.bmrb.wisc.edu/metabolomics/mol_mass.php
    // http://en.wikipedia.org/wiki/Chemical_file_format
    @Test
    void parseNBuild()
    {
        Composition molecule = Composition.parseComposition("C[13]2H-3ON-1(2+)");

        assertEquals(24.97, molecule.getMolecularMass(), 0.01);
    }

    @Test
    void parseNBuild2()
    {
        Composition molecule = Composition.parseComposition("NH3");

        assertEquals(17.026, molecule.getMolecularMass(), 0.001);

        molecule = Composition.parseComposition("N[15]H3");

        assertEquals(18.02, molecule.getMolecularMass(), 0.01);
    }

    @Test
    void parseNBuildFormulaWithInternalRepetition()
    {
        Composition molecule = Composition.parseComposition("PO4H2(CH2)12CH3");
        assertEquals(280.1803, molecule.getMolecularMass(), 0.0001);

        molecule = Composition.parseComposition("PO4H2(C[13]H2)12C[13]H3");

        assertEquals(293.2239, molecule.getMolecularMass(), 0.0001);
    }

    @Test
    void parseSimpleFormula()
    {
        Composition molecule = Composition.parseComposition("H3O(+)");

        assertEquals(19.0178, molecule.getMolecularMass(), 0.0001);
    }

    @Test
    void parseSimpleParticle()
    {
        Composition molecule = Composition.parseComposition("H");
        double hMass = 1.007825032;
        assertEquals(hMass, molecule.getMolecularMass(), 0.000000001);

        molecule = Composition.parseComposition("H(+)"); // H-(e-)
        double mass = molecule.getMolecularMass();

        assertEquals(1.00727645209054, mass, 0.000000001);
        assertEquals(mass, hMass - PeriodicTable.ELECTRON_MASS, 0.000000001);

        molecule = Composition.parseComposition("H(-)");// H+(e-)
        mass = molecule.getMolecularMass();

        assertEquals(mass, hMass + PeriodicTable.ELECTRON_MASS, 0.000000001);
    }

    // Test that changing the atoms collection does not change the mass
    @Test
    void testImmutableAtomCollection()
    {
        Composition molecule = Composition.parseComposition("PO4H2(CH2)12CH3");

        Set<Atom> atoms = molecule.getAtoms();
        assertThrows(UnsupportedOperationException.class, atoms::clear);

    }

    @Test
    void testAvgMassCalc()
    {
        Composition molecule = Composition.parseComposition("NH3");

        assertEquals(17.03, molecule.getAverageMass(), 0.01);

        molecule = Composition.parseComposition("N[15]H3");

        assertEquals(18.02, molecule.getAverageMass(), 0.01);
    }

    @Test
    void testGetFormula()
    {
        Composition composition = Composition.parseComposition("H3C");

        assertEquals("H3C", composition.getFormula());
    }

    @Test
    void testIsotopeDelta() throws Exception
    {
        assertEquals(1.0033548378, Composition.parseComposition("C[13]").getIsotopeDelta(), eps);
    }

    @Test
    void testCopy()
    {
        Composition composition = Composition.parseComposition("H3C");

        assertEquals("H3C", composition.getFormula());

        Composition copiedComposition = new Composition(composition);
        assertEquals("H3C", copiedComposition.getFormula());
        assertEquals(3, copiedComposition.getCount(PeriodicTable.H));
        assertEquals(1, copiedComposition.getCount(PeriodicTable.C));
        assertEquals(composition.hashCode(), copiedComposition.hashCode());
        assertEquals(composition, copiedComposition);
        assertEquals(copiedComposition, composition);
    }

    @Test
    void testCopyConstructor()
    {
        Composition composition = Composition.parseComposition("H3C");

        assertEquals("H3C", composition.getFormula());

        Composition copiedComposition = new Composition(composition);
        assertEquals("H3C", copiedComposition.getFormula());
        assertEquals(3, copiedComposition.getCount(PeriodicTable.H));
        assertEquals(1, copiedComposition.getCount(PeriodicTable.C));
        assertEquals(composition.hashCode(), copiedComposition.hashCode());
        assertEquals(composition, copiedComposition);
        assertEquals(copiedComposition, composition);
    }

    @Test
    void testConstructor()
    {
        Composition composition = new Composition(Composition.parseComposition("C"), Composition.parseComposition("H"),
                Composition.parseComposition("H"), Composition.parseComposition("H"));

        assertEquals("CH3", composition.getFormula());
    }

    @Test
    void testToString()
    {
        Composition composition = Composition.parseComposition("H3C");
        assertEquals(composition.getFormula(), composition.toString());
    }

    @Test
    void test1()
    {
        Composition.Builder builder = new Composition.Builder().add(C).add(O, 2);

        assertEquals(Composition.parseComposition("CO2"), builder.build());
    }

    @Test
    void test2()
    {
        Composition.Builder builder = new Composition.Builder(C).add(O, 2);

        assertEquals(Composition.parseComposition("CO2"), builder.build());
    }

    @Test
    void test3()
    {
        Composition.Builder builder = new Composition.Builder(C, 1).add(O, 2);

        assertEquals(Composition.parseComposition("CO2"), builder.build());
    }

    @Test
    void test4()
    {
        Composition.Builder builder = new Composition.Builder(C, -1).add(O, -2);

        assertEquals(Composition.parseComposition("C-1O-2"), builder.build());
    }

    @Test
    void testIsotope1()
    {
        Composition.Builder builder = new Composition.Builder().addIsotope(C, 13, -1).add(O, -2);
        assertEquals(Composition.parseComposition("C[13]-1O-2"), builder.build());
    }

    @Test
    void testIsotope2()
    {
        Composition.Builder builder = new Composition.Builder().addIsotope(C, 13).add(O, 2);

        assertEquals(Composition.parseComposition("C[13]O2"), builder.build());
    }

    @Test
    void testCharge()
    {
        Composition comp = new Composition.Builder(O).add(H).charge(-1).build();

        assertEquals(1, comp.getCount(PeriodicTable.O));
        assertEquals(1, comp.getCount(PeriodicTable.H));
        assertEquals(-1, comp.getCharge());
    }

    @Test
    void testReuseBuild()
    {
        Composition.Builder builder = new Composition.Builder(C, -1).add(O, -2);
        Composition composition1 = builder.build();
        assertEquals(Composition.parseComposition("C-1O-2"), composition1);
        assertEquals(composition1.getFormula(), "C-1O-2");

        builder.add(C, 2);
        builder.add(O, 4);
        Composition composition2 = builder.build();
        assertEquals(Composition.parseComposition("C1O2"), composition2);

        assertEquals(-1, composition1.getCount(PeriodicTable.getInstance().getAtom(C)));
        assertEquals(-2, composition1.getCount(PeriodicTable.getInstance().getAtom(O)));
    }

    @Test
    void testMakeICATD2H8Formula()
    {
        // ICAT-D:2H(8):C20H26H[2]8N4O5S
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(C), 20);
        atomCounterMap.put(periodicTable.getAtom(H), 26);
        atomCounterMap.put(periodicTable.getAtom(H, 2), 8);
        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.N), 4);
        atomCounterMap.put(periodicTable.getAtom(O), 5);
        atomCounterMap.put(periodicTable.getAtom(AtomicSymbol.S), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, 0);

        assertEquals("C20H26H[2]8N4O5S", formulaString);
    }

    @Test
    void testMakeChargedFormula()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(O), 1);
        atomCounterMap.put(periodicTable.getAtom(H), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, -1);

        assertEquals("HO(-)", formulaString);
    }

    @Test
    void testMakeChargedFormula2()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(H), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, 1);

        assertEquals("H(+)", formulaString);
    }

    @Test
    void testMakeChargedFormula3()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(S), 1);
        atomCounterMap.put(periodicTable.getAtom(O), 3);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, -2);

        assertEquals("O3S(2-)", formulaString);
    }

    @Test
    void testMakeChargedFormula4()
    {
        Object2IntOpenHashMap<Atom> atomCounterMap = new Object2IntOpenHashMap<>();

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        atomCounterMap.put(periodicTable.getAtom(Fe), 1);

        String formulaString = Composition.Builder.makeFormula(atomCounterMap, 2);

        assertEquals("Fe(2+)", formulaString);
    }

    @Test
    void testBuilderToString()
    {
        Composition.Builder builder = new Composition.Builder(C, 20).add(H, 26).addIsotope(H, 2, 8).add(N, 4).add(O, 5)
                .add(S);
        assertEquals("C20H26H[2]8N4O5S", builder.toString());
    }

    @Test
    void testBuilderAddAll()
    {
        Composition.Builder builder = new Composition.Builder(AtomicSymbol.P);

        builder.add(Composition.parseComposition("H3O4"));

        Composition composition = builder.build();

        PeriodicTable periodicTable = PeriodicTable.getInstance();
        assertEquals(3, composition.getCount(periodicTable.getAtom(H)));
        assertEquals(1, composition.getCount(periodicTable.getAtom(AtomicSymbol.P)));
        assertEquals(4, composition.getCount(periodicTable.getAtom(O)));
        assertEquals(8, composition.size());
    }

    @Test
    void testIsEmpty()
    {
        assertTrue(new Composition().isEmpty());
        assertFalse(new Composition.Builder().charge(1).build().isEmpty());
        assertFalse(new Composition.Builder(Pt, 12).build().isEmpty());
    }

    @Test
    void testBuilderIsEmpty()
    {
        assertTrue(new Composition.Builder().isEmpty());
        assertFalse(new Composition.Builder().charge(1).isEmpty());
        assertFalse(new Composition.Builder(Pt, 12).isEmpty());
    }

    @Test
    void testGroups()
    {
        Composition composition = Composition.parseComposition("(OH)-1");
        assertEquals(-1, composition.getCount(PeriodicTable.O));
        assertEquals(-1, composition.getCount(PeriodicTable.H));
    }

    @Test
    void testElectron()
    {
        Composition composition = Composition.parseComposition("(-)");

        assertEquals(PeriodicTable.ELECTRON_MASS, composition.getMolecularMass(), 0.000001);
        assertEquals(-1, composition.getCharge());
    }

    @Test
    void testEmpty()
    {
        assertTrue(Composition.EMPTY.getFormula().isEmpty());
    }

    @Test
    void testMinusProton()
    {
        Composition composition = Composition.parseComposition("H-1(-)");

        assertEquals(-PeriodicTable.PROTON_MASS, composition.getMolecularMass(), 0.000001);
        assertEquals(-1, composition.getCharge());
    }

    @Test
    void testIsotopes()
    {
        Composition composition = Composition.parseComposition("H[2]3H-1C[13]");
        System.out.println(composition.getMolecularMass());
    }
}