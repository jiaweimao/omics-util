package omics.util.chem;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jul 2017, 10:24 PM
 */
class PeriodicTableTest
{
    private double eps = 0.000001;

    @Test
    void proton()
    {
        double m1 = PeriodicTable.PROTON_MASS;
        double m2 = PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS;
        assertEquals(m1, m2, eps);
    }

    /**
     * Get main isotope atom given an *AtomicSymbol*
     */
    @Test
    void getIsotopeFromSymbol()
    {
        Atom atom = PeriodicTable.getInstance().getAtom(AtomicSymbol.H);
        assertEquals(1, atom.getNucleonCount());
        assertTrue(atom.isDefaultIsotope());
    }

    /**
     * get specific isotope given an *AtomicSymbol*
     */
    @Test
    void getIsotope()
    {
        PeriodicTable instance = PeriodicTable.getInstance();
        Atom dH = instance.getAtom(AtomicSymbol.H, 2);
        assertEquals(2, dH.getNucleonCount());

        Atom atom = instance.getAtom(AtomicSymbol.H, 3);
        assertEquals(3, atom.getNucleonCount());

        Atom c14 = instance.getAtom(AtomicSymbol.C, 14);
        assertEquals(c14.getNucleonCount(), 14);
    }

    /**
     * get *atom* from a string
     */
    @Test
    void getAtomString()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("H[2]");
        assertEquals(2, atom.getNucleonCount());
        assertEquals(atom.getMass(), 2.014101779, eps);

        atom = periodicTable.getAtom("C[13]");
        assertEquals(atom.getMass(), 13.00335483, eps);

        atom = periodicTable.getAtom("O[18]");
        assertEquals(atom.getMass(), 17.9991603, eps);

        atom = periodicTable.getAtom("N[15]");
        assertEquals(atom.getMass(), 15.00010897, eps);
    }

    /**
     * PeriodicTable provides static Atom and masses that are ost used.
     */
    @Test
    void getMass()
    {
        Atom c = PeriodicTable.C;
        assertEquals(PeriodicTable.C_MASS, c.getMass(), eps);
    }

    @Test
    void atomInfo()
    {
        Atom c = PeriodicTable.C;
        assertEquals(AtomicSymbol.C, c.getSymbol());
        assertEquals(0.9893, c.getAbundance(), 0.0001);
        assertEquals(12, c.getMass(), eps);
    }

    /**
     * fetch the list of isotopes from an *AtomicSymbol*
     */
    @Test
    void getElement()
    {
        Element element = PeriodicTable.getInstance().getElement(AtomicSymbol.C);
        List<Atom> isotopeList = element.getIsotopeList();

        assertEquals(15, isotopeList.size());
    }

    @Test
    void getAtomMassNumber()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();
        assertSame(periodicTable.getAtom(AtomicSymbol.H, 1), periodicTable.getAtom(AtomicSymbol.H, 1));
        assertSame(periodicTable.getAtom(AtomicSymbol.H, 2), periodicTable.getAtom(AtomicSymbol.H, 2));
        assertSame(PeriodicTable.H2, periodicTable.getAtom(AtomicSymbol.H, 2));
        assertNotSame(periodicTable.getAtom(AtomicSymbol.H, 1), periodicTable.getAtom(AtomicSymbol.H, 2));
    }

    @Test
    void getAtomInt2()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom hOne = periodicTable.getAtom(AtomicSymbol.H, 1);
        Atom hTwo = periodicTable.getAtom(AtomicSymbol.H);

        assertSame(hOne, hTwo);

        Atom atom = periodicTable.getAtom(AtomicSymbol.O, 18);
        assertEquals(atom.getMass(), 17.9991603, eps);

        Atom n15 = periodicTable.getAtom(AtomicSymbol.N, 15);
        assertEquals(n15.getMass(), 15.0001088984, eps);
    }

    @Test
    void getAtom()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();
        Atom atom = periodicTable.getAtom(AtomicSymbol.H);
        assertEquals(atom.getMass(), 1.007825035, eps);

        atom = periodicTable.getAtom(AtomicSymbol.C);
        assertEquals(atom.getMass(), 12.0, eps);

        atom = periodicTable.getAtom(AtomicSymbol.N);
        assertEquals(atom.getMass(), 14.003074, eps);

        atom = periodicTable.getAtom(AtomicSymbol.O);
        assertEquals(atom.getMass(), 15.99491463, eps);

        atom = periodicTable.getAtom(AtomicSymbol.S);
        assertEquals(atom.getMass(), 31.9720707, eps);

        atom = periodicTable.getAtom(AtomicSymbol.P);
        assertEquals(atom.getMass(), 30.973762, eps);
    }

    @Test
    void testGetAtom()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();
        Atom atom1 = periodicTable.getAtom(AtomicSymbol.Po);
        assertNull(atom1);
    }

    @Test
    void getCarbon()
    {
        assertEquals(AtomicSymbol.C, PeriodicTable.getInstance().getAtom("C").getSymbol());
    }

    @Test
    void testGetCarbon14()
    {
        Atom c14 = PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 14);

        assertEquals(AtomicSymbol.C, c14.getSymbol());
        assertEquals(14, c14.getNucleonCount());
        assertEquals(14.003241988, c14.getMass(), eps);
    }

    @Test
    void testHashSet()
    {
        Set<Atom> atoms = new HashSet<>();
        atoms.add(PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 12));
        atoms.add(PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 13));
        atoms.add(PeriodicTable.getInstance().getAtom(AtomicSymbol.valueOf("C"), 14));
        atoms.add(PeriodicTable.getInstance().getAtom("C[14]"));
        atoms.add(PeriodicTable.getInstance().getAtom("C"));

        assertEquals(3, atoms.size());
    }

    @Test
    void testGetBadFormatAtom()
    {
        assertThrows(IllegalArgumentException.class, () -> PeriodicTable.getInstance().getAtom("C(13)"));
    }

    @Test
    void testInvalidAtom()
    {
        assertThrows(IllegalArgumentException.class, () -> PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 12));
    }

    @Test
    void testGetAtoms()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        List<Atom> atoms = periodicTable.getElement(AtomicSymbol.Ca).getIsotopeList();

        assertEquals(24, atoms.size());

        // Check that atoms are sorted by mass
        double mass = 0;
        for (Atom atom : atoms) {

            assertTrue(mass < atom.getMass(), "expecting atoms to be sorted");
            mass = atom.getMass();
        }
    }

    @Test
    void testToString1()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[12]");
        assertTrue(atom.isDefaultIsotope());

        assertEquals("C", atom.toString());
    }

    @Test
    void testToString2()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");
        assertTrue(!atom.isDefaultIsotope());

        assertEquals("C[13]", atom.toString());
    }

    @Test
    void testIsotope()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");
        assertEquals(AtomicSymbol.C, atom.getSymbol());
        assertEquals(13, atom.getNucleonCount());
    }

    @Test
    void testIsotope2()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");
        assertEquals(AtomicSymbol.C, atom.getSymbol());
        assertEquals(13, atom.getNucleonCount());
    }

    @Test
    void testIssue43()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        Atom atom = periodicTable.getAtom("C[13]");
        Atom atom2 = periodicTable.getAtom(atom.toString());

        assertSame(atom, atom2);
    }

    @Test
    void testIllegalFormat()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        assertThrows(IllegalArgumentException.class, () -> periodicTable.getAtom("C[1a]"));
    }

    @Test
    void testIllegalFormat2()
    {

        PeriodicTable periodicTable = PeriodicTable.getInstance();

        assertThrows(IllegalArgumentException.class, () -> periodicTable.getAtom("C[14"));
    }

    @Test
    void testIllegalFormat3()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        assertThrows(IllegalArgumentException.class, () -> periodicTable.getAtom("C14]"));
    }

    @Test
    void testIllegalFormat5()
    {
        PeriodicTable periodicTable = PeriodicTable.getInstance();

        assertThrows(IllegalArgumentException.class, () -> periodicTable.getAtom("C14[]"));
    }
}