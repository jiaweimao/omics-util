package omics.util.io;


import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 26 Oct 2019, 10:20 AM
 */
class FileTypeTest
{
    @Test
    void testGetPathMatcher()
    {
        PathMatcher pathMatcher = FileType.MGF.getPathMatcher();
        assertTrue(pathMatcher.matches(Paths.get("spec.mgf")));
        assertTrue(pathMatcher.matches(Paths.get("spec.MGF")));
    }

    @Test
    void testlistPaths()
    {
        Path[] paths = FileType.MZIdentML.listPaths(Paths.get("Z:\\MaoJiawei\\o-glycan\\liuluyao\\A-24-raw"));
        for (Path path : paths) {
            System.out.println(path);
        }

    }

    @Test
    void testlistPaths2()
    {

        List<Path> paths = new ArrayList<>();
        FileType.MZIdentML.listPaths(Paths.get("D:\\data\\s1.d.mzid"), paths);
        for (Path path : paths) {
            System.out.println(path);
        }

    }

    @Test
    void testPredicate()
    {
        Path path = Paths.get("file.mzid");
        assertTrue(FileType.MZIdentML.test(path));
    }

}