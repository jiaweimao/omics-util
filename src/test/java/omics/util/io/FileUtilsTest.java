package omics.util.io;

import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Path;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Oct 2019, 7:44 PM
 */
class FileUtilsTest
{
    @Test
    void testGetResourcePath()
    {
        Path path = FileUtils.getResourcePath("a_pep.fasta");
        System.out.println(path);
    }

    @Test
    void testGetResource()
    {
        URL resource = FileUtils.getResource("a_pep.fasta");
        System.out.println(resource);
    }
}