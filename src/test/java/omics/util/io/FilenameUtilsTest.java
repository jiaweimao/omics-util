package omics.util.io;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Oct 2019, 1:08 PM
 */
class FilenameUtilsTest
{

    @Test
    void testLegalPath()
    {
        String path = "Trypsin/P.";
        String s = FilenameUtils.legalPath(path, "_");
        System.out.println(s);
    }

    @Test
    void testNewExtension()
    {
        Path path = Paths.get("C:/test/hello.txt");
        Path newPath = FilenameUtils.newExtension(path, ".java");
        assertTrue(newPath.toString().endsWith(".java"));
    }
}