/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.util.io;

import omics.util.io.xml.IndentXMLStreamWriter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 May 2018, 4:38 PM
 */
class IndentXMLStreamWriterTest
{
    private XMLStreamWriter writer;
    private StringWriter stringWriter;

    @BeforeEach
    void setUp() throws XMLStreamException
    {
        stringWriter = new StringWriter();
        XMLOutputFactory xmlOf = XMLOutputFactory.newInstance();
        writer = new IndentXMLStreamWriter(xmlOf.createXMLStreamWriter(stringWriter));
    }


    @AfterEach
    void tearDown() throws XMLStreamException
    {
        writer.close();
    }

    @Test
    void test() throws XMLStreamException, IOException
    {
        writer.writeStartDocument();
        writer.writeStartElement("person");
        writer.writeNamespace("", "http://universe.com");
        writer.writeAttribute("gender", "female");
        writer.writeStartElement("name");
        writer.writeCharacters("Jesus");
        writer.writeEndElement();
        writer.writeEndElement();
        writer.writeEndDocument();

        writer.close();
        stringWriter.close();

        assertEquals("<?xml version=\"1.0\" ?>\n" +
                "<person xmlns=\"http://universe.com\" gender=\"female\">\n" +
                "  <name>Jesus</name>\n" +
                "</person>", stringWriter.toString());
    }

    @Test
    void emptyElement() throws XMLStreamException
    {
        writer.writeStartDocument();
        writer.writeEmptyElement("person");
        writer.writeEndDocument();
        assertEquals("<?xml version=\"1.0\" ?>\n" +
                "<person/>", stringWriter.toString());
    }

    @Test
    void emptyElement2() throws XMLStreamException
    {
        writer.writeStartDocument();

        writer.writeEmptyElement("Person");
        writer.writeAttribute("gender", "f");
        writer.writeAttribute("age", "22");

        writer.writeEndDocument();

        assertEquals("<?xml version=\"1.0\" ?>\n" +
                "<Person gender=\"f\" age=\"22\"/>", stringWriter.toString());
    }

    @Test
    void emptyElement3() throws XMLStreamException
    {
        writer.writeStartDocument();
        writer.writeStartElement("scanList");
        writer.writeAttribute("count", "1");

        // fileContent
        writer.writeStartElement("scan");

        writer.writeEmptyElement("cvParam");

//        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "MS");
        writer.writeAttribute("name", "MS1 spectrum");
//
//        writer.writeEmptyElement("cvParam");
//        writer.writeAttribute("cvRef", "MS");
//        writer.writeAttribute("name", "MSn spectrum");

        writer.writeEndElement();
        writer.writeEndElement();
        writer.writeEndDocument();
    }

}