/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.util.io;


import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileReader;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Jan 2018, 1:25 PM
 */
class LineReaderTest
{
    @Test
    void testPeek() throws Exception
    {
        LineReader reader = new LineReader(new FileReader(new File(getClass().getClassLoader().getResource("test.fasta").toURI())));
        String peek = reader.peek();

        String line1 = ">sp|P61981|1433G_HUMAN 14-3-3 protein gamma OS=Homo sapiens GN=YWHAG PE=1 SV=2";
        String line2 = "MVDREQLVQKARLAEQAERYDDMAAAMKNVTELNEPLSNEERNLLSVAYKNVVGARRSSW";
        String line3 = "RVISSIEQKTSADGNEKKIEMVRAYREKIEKELEAVCQDVLSLLDNYLIKNCSETQYESK";

        assertEquals(peek, line1);
        assertEquals(reader.readLine(), line1);

        assertEquals(reader.peek(), line2);
        assertEquals(reader.readLine(), line2);

        assertEquals(reader.peek(), line3);
        assertEquals(reader.peek(), line3);
        assertEquals(reader.peek(), line3);
        assertEquals(reader.peek(), line3);

        reader.close();
    }
}