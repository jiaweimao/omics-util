/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.util.io;

import org.junit.jupiter.api.Test;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Feb 2018, 8:47 PM
 */
class XMLUtilsTest
{
    @Test
    void detectFileEncoding()
    {
    }

    @Test
    void detectFileEncoding1()
    {
    }

    @Test
    void seekStartElement()
    {
    }

    @Test
    void toStartElement() throws IOException, XMLStreamException
    {
//        URL resource = Resources.getResource("enzymes.xml");
//        XMLInputFactory factory = XMLInputFactory.newInstance();
//        XMLStreamReader xmlStreamReader = factory.createXMLStreamReader(resource.openStream());
//
//        while (xmlStreamReader.hasNext()) {
//            System.out.println(xmlStreamReader.getTextCharacters());
//        }

    }

    @Test
    void nextStartElementIf() throws IOException, XMLStreamException
    {

//        URL resource = Resources.getResource("enzymes.xml");
//        XMLInputFactory factory = XMLInputFactory.newInstance();
//        XMLEventReader xmlEventReader = factory.createXMLEventReader(resource.openStream());
//
//
//        while (xmlEventReader.hasNext()) {
//            XMLEvent xmlEvent = xmlEventReader.nextEvent();
//            System.out.println(xmlEvent.getEventType());
//        }
////
//        Optional<StartElement> enzyme = XMLUtils.nextStartElementIf(xmlEventReader, "enzyme");
//
//        StartElement startElement = enzyme.get();
//        System.out.println(startElement.getName());


    }

    @Test
    void seekNextCharacters()
    {
    }

    @Test
    void nextCharacters()
    {
    }

    @Test
    void getOptionalAttribute()
    {
    }

    @Test
    void getMandatoryAttribute()
    {
    }

    @Test
    void getMandatoryXMLEvent()
    {
    }

    @Test
    void writeAllEventsUntilStartElement()
    {
    }

}