package omics.util.io.csv;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 09 May 2018, 8:37 AM
 */
class CsvReaderTest
{
    @Test
    void testSetSkipEmptyRecords() throws IOException
    {
        CsvReader reader = new CsvReader(CsvReaderTest.class.getClassLoader().getResourceAsStream("test.csv"), ',', StandardCharsets.UTF_8);

        reader.readRecord();
        assertEquals("col1,col2", reader.getRawRecord());

        reader.readRecord();
        assertEquals("v1,v2", reader.getRawRecord());

        assertFalse(reader.readRecord());
        reader.close();


        reader = new CsvReader(CsvReaderTest.class.getClassLoader().getResourceAsStream("test.csv"), ',', StandardCharsets.UTF_8);
        reader.setSkipEmptyRecords(false);

        reader.readRecord();
        assertEquals("", reader.getRawRecord());

        reader.readRecord();
        assertEquals("col1,col2", reader.getRawRecord());

        reader.readRecord();
        assertEquals("", reader.getRawRecord());

        reader.readRecord();
        assertEquals("v1,v2", reader.getRawRecord());

        assertFalse(reader.readRecord());


        reader.close();
    }
}