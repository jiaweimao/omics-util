package omics.util.math;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Nov 2018, 10:13 AM
 */
class HistogramTest
{
    @Test
    void test()
    {
        double[] values = new double[]{-3, 0.5, 1, 2, 3, 2.3, 4.5, 3.6, 6.8, 8.9, 11, 12};
        Histogram histogram = new Histogram(values, 1, 11, 2);
        ArrayList<Histogram.Bin> bins = histogram.getBins();
        for (Histogram.Bin bin : bins) {
            System.out.println(bin);
        }
        System.out.println(histogram.getLeftExtreme());
        System.out.println(histogram.getRightExtreme());
    }

    @Test
    void testSearch()
    {
        int[] array = {1, 2, 34};
        int i = Arrays.binarySearch(array, 0);
        System.out.println(-i - 1);
        System.out.println(i);

    }

}