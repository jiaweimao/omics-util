/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.util.math;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author JiaweiMao 2017.03.25
 * @since 1.0.0
 */
class MathUtilsTest
{

    @Test
    void testVariance()
    {
        double[] values = new double[]{1, 2, 3, 3, 9, 10};
        assertEquals(MathUtils.variance(values, false), 12.22222, 1E-5);
        assertEquals(MathUtils.variance(values), 14.667, 1E-3);
        assertEquals(MathUtils.standardDeviation(values), 3.83, 0.01);
        assertEquals(MathUtils.mean(values), 4.667, 0.001);

    }

    @Test
    void testCombination()
    {
        int[] array = new int[]{3, 2, 1};
        List<int[]> list = MathUtils.combination(array);
        assertArrayEquals(new int[]{0, 0, 0}, list.get(0));
        assertArrayEquals(new int[]{1, 0, 0}, list.get(1));
        assertArrayEquals(new int[]{2, 0, 0}, list.get(2));
        assertArrayEquals(new int[]{0, 1, 0}, list.get(3));
        assertArrayEquals(new int[]{1, 1, 0}, list.get(4));
        assertArrayEquals(new int[]{2, 1, 0}, list.get(5));

        int[] array1 = new int[]{1, 2, 3};
        list = MathUtils.combination(array1);
        assertArrayEquals(new int[]{0, 0, 0}, list.get(0));
        assertArrayEquals(new int[]{0, 1, 0}, list.get(1));
        assertArrayEquals(new int[]{0, 0, 1}, list.get(2));
        assertArrayEquals(new int[]{0, 1, 1}, list.get(3));
        assertArrayEquals(new int[]{0, 0, 2}, list.get(4));
        assertArrayEquals(new int[]{0, 1, 2}, list.get(5));

        array1 = new int[]{2};
        list = MathUtils.combination(array1);
        assertArrayEquals(new int[]{0}, list.get(0));
        assertArrayEquals(new int[]{1}, list.get(1));
    }

    @Test
    void permutation()
    {
        int arr[] = {1, 2, 3, 4, 5};
        int k = 3;
        List<int[]> list = MathUtils.permutation(arr, k);
        assertEquals(10, list.size());
        assertArrayEquals(new int[]{1, 2, 3}, list.get(0));
        assertArrayEquals(new int[]{3, 4, 5}, list.get(9));

        arr = new int[]{5, 3, 4, 2};
        list = MathUtils.permutation(arr, k);
        assertEquals(4, list.size());
        assertArrayEquals(new int[]{5, 3, 4}, list.get(0));
        assertArrayEquals(new int[]{3, 4, 2}, list.get(3));

        arr = new int[]{1, 4, 6, 11, 12, 21, 22, 25, 27, 29, 31, 32, 33, 34};
        list = MathUtils.permutation(arr, 2);
        assertEquals(91, list.size());

        assertArrayEquals(new int[]{1, 4}, list.get(0));
        assertArrayEquals(new int[]{33, 34}, list.get(90));
    }

    @Test
    void permutationDup()
    {
        int arr[] = {1, 2, 2, 4, 5};
        int k = 3;
        List<int[]> list = MathUtils.permutationDup(arr, k);
        assertEquals(7, list.size());
        assertArrayEquals(new int[]{1, 2, 2}, list.get(0));
        assertArrayEquals(new int[]{1, 2, 4}, list.get(1));
        assertArrayEquals(new int[]{1, 2, 5}, list.get(2));
        assertArrayEquals(new int[]{1, 4, 5}, list.get(3));
        assertArrayEquals(new int[]{2, 2, 4}, list.get(4));
        assertArrayEquals(new int[]{2, 2, 5}, list.get(5));
        assertArrayEquals(new int[]{2, 4, 5}, list.get(6));
    }

    @Test
    void permutationDup2()
    {
        int[] arr = {1, 2, 2, 4, 5};
        assertThrows(IllegalArgumentException.class, () -> MathUtils.permutationDup(arr, 6));
    }

    @Test
    void permutationDupOrder()
    {
        int arr[] = {4, 5, 1, 1, 4, 3};
        List<int[]> list = MathUtils.permutationDup(arr, 3);
        assertArrayEquals(new int[]{1, 1, 3}, list.get(0));
        assertEquals(10, list.size());
        assertArrayEquals(new int[]{4, 4, 5}, list.get(9));
    }

    @Test
    void testMean()
    {
        double[] value = new double[]{1, 2, 3., 4};
        double mean = MathUtils.mean(value);
        assertEquals(2.5, mean, 1E-4);
    }

    @Test
    void scale()
    {
        double a = 46.656;
        double scale = MathUtils.round(a, 2);
        assertEquals(46.66, scale, 0.0001);
    }
}