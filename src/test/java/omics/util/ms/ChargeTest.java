package omics.util.ms;


import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;

import static omics.util.ms.Charge.PAT_CHARGE;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao on 2017.08.15
 * @since 1.0-SNAPSHOT
 */
class ChargeTest
{
    @Test
    void test()
    {
        String s = "2+";
        Matcher matches = PAT_CHARGE.matcher(s);
        assert matches.matches();
        assertEquals(matches.group(1), "2");
        assertEquals(matches.group(2), "+");
    }
}