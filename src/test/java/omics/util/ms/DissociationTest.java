package omics.util.ms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertSame;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 Oct 2019, 1:22 PM
 */
class DissociationTest
{
    @Test
    void testEqual()
    {
        Dissociation hcd = Dissociation.fromName("HCD");
        assertSame(hcd, Dissociation.HCD);
    }
}