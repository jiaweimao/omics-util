package omics.util.ms;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Precision;
import omics.util.ms.peaklist.impl.BasePeakListTest;
import omics.util.ms.peaklist.impl.DoublePeakList;
import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import org.junit.jupiter.api.Test;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;


public class MsnSpectrumTest extends BasePeakListTest
{
    public MsnSpectrumTest()
    {
        super(Precision.DOUBLE);
    }

    private double[] getMzField(DoublePeakList peakList) throws NoSuchFieldException, IllegalAccessException
    {
        Field field = peakList.getClass().getDeclaredField("xList");
        field.setAccessible(true);
        return (double[]) field.get(peakList);
    }

    @Override
    protected void checkPeakList(double[] expectedMzs, double[] expectedIntensities, PeakList<? extends
            PeakAnnotation> spectrum) throws NoSuchFieldException, IllegalAccessException
    {
        DoublePeakList peakList = getPeakList(spectrum);

        peakList.trimToSize();
        double[] mzs = getMzField(peakList);
        assertEquals(expectedMzs.length, mzs.length);
        for (int i = 0; i < mzs.length; i++) {

            assertEquals(expectedMzs[i], mzs[i], 0.0001);
        }

        double[] intensities = getIntensityField(peakList);
        assertEquals(expectedIntensities.length, intensities.length);
        for (int i = 0; i < intensities.length; i++) {
            assertEquals(expectedIntensities[i], intensities[i], 0.0001);
        }
    }

    private double[] getIntensityField(DoublePeakList peakList) throws NoSuchFieldException, IllegalAccessException
    {
        Field field = peakList.getClass().getDeclaredField("yList");
        field.setAccessible(true);
        return (double[]) field.get(peakList);
    }

    private MsnSpectrum newPeakList(int initialCapacity)
    {
        return new MsnSpectrum(initialCapacity, Precision.DOUBLE);
    }

    @Override
    protected <A extends PeakAnnotation> int getIntensityArrayLength(PeakList<A> spectrum) throws
            NoSuchFieldException, IllegalAccessException
    {
        return getMzField(getPeakList(spectrum)).length;
    }

    @Override
    protected <A extends PeakAnnotation> int getMzArrayLength(PeakList<A> spectrum) throws NoSuchFieldException,
            IllegalAccessException
    {
        return getIntensityField(getPeakList(spectrum)).length;
    }

    private DoublePeakList getPeakList(PeakList spectrum) throws NoSuchFieldException, IllegalAccessException
    {
        Field field = Spectrum.class.getDeclaredField("peakList");
        field.setAccessible(true);
        return (DoublePeakList) field.get(spectrum);
    }

    protected MsnSpectrum newPeakList()
    {
        return newPeakList(0);
    }

    @Test
    public void testEquals()
    {
        runTestEquals(newPeakList(), newPeakList());
    }

    @Test
    public void testHashCode()
    {
        runTestHashCode(newPeakList(), newPeakList());
    }

    @Test
    public void testgetIntensityValues()
    {
        MsnSpectrum peakList = newPeakList();

        runTestgetIntensityValues(peakList);
    }

    @Test
    public void testgetIntensityValues2()
    {
        runTestgetIntensityValues2(newPeakList());
    }

    @Test
    public void testgetIntensityValues3()
    {
        runTestgetIntensityValues(newPeakList());
    }

    @Test
    public void testgetIntensityValues4()
    {
        runTestgetIntensityValues4(newPeakList());
    }

    @Test
    public void testgetIntensityValues5()
    {
        runTestgetIntensityValues5(newPeakList());
    }

    @Test
    public void testgetMzValues1()
    {
        runTestgetMzValues1(newPeakList());
    }

    @Test
    public void testgetMzValues2()
    {
        runTestgetMzValues2(newPeakList());
    }

    @Test
    public void testgetIntensityValuesArr()
    {
        runTestgetIntensityValuesArr(newPeakList());
    }

    @Test
    public void testGetIntensityAt()
    {
        MsnSpectrum peakList = newPeakList();

        runTestGetIntensityAt(peakList);
    }

    @Test
    public void testAddSame()
    {
        runTestAddSame(newPeakList());
    }

    @Test
    public void testEnsureCapacity() throws NoSuchFieldException, IllegalAccessException
    {
        runTestEnsureCapacity(newPeakList());
    }

    @Test
    void testWrite2Mgf()
    {
        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.setTitle("a spectrum");
        spectrum.addScanNumber(ScanNumber.discrete(2));
        spectrum.addRetentionTime(RetentionTime.discrete(12));
        Random random = new Random(1111);
        for (int i = 0; i < 100; i++) {
            spectrum.add(i, random.nextDouble());
        }
        PrintWriter writer = new PrintWriter(System.out);
        spectrum.write2Mgf(writer);
        writer.close();
    }

    @Test
    public void testSetLoadFactor()
    {
        //Only for PeakLists
    }

    @Test
    public void testGetMostIntenseIndex()
    {
        runTestGetMostIntenseIndex(newPeakList(15));
    }

    @Override
    @Test
    public void testGetMostIntenseIndex2()
    {
        runTestGetMostIntenseIndex2(newPeakList(0));
    }

    @Test
    public void testGetMostIntenseIndex3()
    {
        runTestGetMostIntenseIndex3(newPeakList(0));
    }

    @Test
    public void testGetMostIntenseIndex4()
    {
        runTestGetMostIntenseIndex4(newPeakList(0));
    }

    @Test
    public void testGetMostIntenseIndex5()
    {
        runTestGetMostIntenseIndex5(newPeakList(0));
    }

    @Test
    public void testgetMzValues3()
    {
        runTestgetMzValues3(newPeakList());
    }

    @Override
    @Test
    public void testTotalIonCurrent()
    {
        runTestTotalIonCurrent(newPeakList());
    }

    @Override
    @Test
    public void testCopyMzs()
    {
        runTestCopyMzs(newPeakList(0));
    }

    @Override
    @Test
    public void testEmptyPeakListgetIntensityValues()
    {
        runTestEmptyPeakListgetIntensityValues(newPeakList());
    }

    @Override
    @Test
    public void testSetIntensity()
    {
        int initialCapacity = 0;
        runTestSetIntensity(newPeakList(initialCapacity));
    }

    @Override
    @Test
    public void testValueOf()
    {
        runTestValueOf(precision);
    }

    @Override
    @Test
    public void testValueOfWithIntensities()
    {
        runTestValueOfWithIntensities(precision);
    }

    @Override
    @Test
    public void testAddPeakWithAnnotation()
    {
        // only for PeakList
    }

    @Override
    @Test
    public void testGetMostIntenseIndexWholeSpectra()
    {
        runGetMostIntenseIndexWholeSpectra(newPeakList());
    }

    @Override
    @Test
    public void testEmptyPeakListGetMz()
    {
        MsnSpectrum peakList = newPeakList();
        assertThrows(IndexOutOfBoundsException.class, () -> runTestEmptyPeakListGetMz(peakList));
    }

    @Override
    @Test
    public void testEmptyPeakListGetIntensityAt()
    {
        assertThrows(IndexOutOfBoundsException.class, () -> runTestEmptyPeakListGetIntensityAt(newPeakList()));
    }

    @Override
    @Test
    public void testEmptyPeakListgetMzValues()
    {
        runTestEmptyPeakListgetMzValues(newPeakList());
    }

    @Override
    @Test
    public void testClear() throws Exception
    {
        MsnSpectrum peakList = newPeakList();
        runTestClear(peakList);
    }

    @Override
    @Test
    public void testPrecision()
    {
        MsnSpectrum peakList = newPeakList();
        Precision precision = this.precision;
        runTestPrecision(peakList, precision);
    }

    @Test
    public void testBulkAdd() throws Exception
    {
        runTestBulkAdd(newPeakList());
    }

    @Override
    @Test
    public void testBulkAdd2() throws Exception
    {
        runTestBulkAdd2(newPeakList());
    }

    @Override
    @Test
    public void testBulkAdd3() throws Exception
    {
        runTestBulkAdd3(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException() throws Exception
    {
        runTestAddException(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException2() throws Exception
    {
        runTestBulkAddException2(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException3() throws Exception
    {
        runTestBulkAddException3(newPeakList());
    }

    @Override
    @Test
    public void testBulkAddException4() throws Exception
    {
        runTestBulkAddException4(newPeakList());
    }

    @Test
    public void testInsert() throws Exception
    {
        runTestInsert(newPeakList());
    }

    @Test
    public void testMerge()
    {
        runTestMerge(newPeakList());
    }

    @Test
    public void testMerge2()
    {
        runTestMerge2(newPeakList());
    }

    @Test
    public void testMerge3()
    {
        runTestMerge3(newPeakList());
    }

    @Test
    public void testMerge4()
    {
        runTestMerge4(newPeakList());
    }

    @Test
    public void testMerge5()
    {
        //Only for PeakList
    }

    @Test
    public void testMerge6()
    {
        //Only for PeakList
    }

    @Override
    public void testAddPeaks()
    {
        //Only for PeakList
    }

    @Test
    public void testAddDuplicateWithAnnotation()
    {
        runTestAddDuplicateWithAnnotation(newPeakList(), PeakAnnotation.class);
    }

    @Test
    public void testDoInsert()
    {
        //Only for PeakList
    }

    @Override
    public void testIndexOf()
    {
        runTestIndexOf(newPeakList());
    }

    @Override
    public void testGetClosestIndex()
    {

        runTestGetClosestIndex(newPeakList());
    }

    @Test
    public void testAddUnsortedPeaks()
    {
        //Only for PeakList
    }

    @Test
    void testComment()
    {
        MsnSpectrum spectrum = new MsnSpectrum(0, Precision.FLOAT);

        assertEquals("", spectrum.getTitle());

        spectrum.setTitle("Some comment");
        assertEquals("Some comment", spectrum.getTitle());
    }


    @Test
    void testScanNumbers()
    {
        MsnSpectrum spectrum = new MsnSpectrum();
        assertEquals(0, spectrum.getScanNumberList().size());

        spectrum.addScanNumber(ScanNumber.discrete(2));
        assertEquals(Collections.singletonList(ScanNumber.discrete(2)), spectrum.getScanNumberList());

        spectrum.addScanNumber(ScanNumber.interval(5, 7));
        assertEquals(Arrays.asList(ScanNumber.discrete(2), ScanNumber.interval(5, 7)),
                spectrum.getScanNumberList());

        spectrum.addScanNumber(ScanNumberList.from(8, 9, 10));
        assertEquals(Arrays.asList(
                ScanNumber.discrete(2),
                ScanNumber.interval(5, 7),
                ScanNumber.discrete(8),
                ScanNumber.discrete(9),
                ScanNumber.discrete(10)),
                spectrum.getScanNumberList());
    }


    @Test
    void testRetentionTime()
    {
        MsnSpectrum spectrum = new MsnSpectrum();
        assertEquals(0, spectrum.getRetentionTimeList().size());

        spectrum.addRetentionTime(RetentionTime.discrete(50));
        assertEquals(Collections.singletonList(RetentionTime.discrete(50)), spectrum.getRetentionTimeList());

        spectrum.addRetentionTime(new RetentionTimeList(
                RetentionTime.discrete(90),
                RetentionTime.discrete(150)
        ));
        assertEquals(Arrays.asList(
                RetentionTime.discrete(50),
                RetentionTime.discrete(90),
                RetentionTime.discrete(150)
        ), spectrum.getRetentionTimeList());
    }


    @Test
    void testMsnSpectrumCopyConstructor()
    {
        MsnSpectrum src = new MsnSpectrum(0, Precision.FLOAT, SpectrumId.of(MsDataId.of("file://source.mgf")));
        src.addScanNumber(ScanNumber.discrete(1));
        src.addRetentionTime(RetentionTime.discrete(1));
        MsnSpectrum copy = src.copy(new IdentityPeakProcessor<>());

        assertEquals("", copy.getTitle());
        SpectrumId id = copy.getId();
        assertFalse(id instanceof MoreSpectrumId);
        assertTrue(id instanceof DefaultSpectrumId);

        assertEquals(id.getMsDataId(), MsDataId.of("file://source.mgf"));
        assertEquals(ScanNumber.discrete(1), src.getScanNumber());
        assertEquals(1, src.getRetentionTime().getTime(), 0.1);
    }
}