package omics.util.ms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 22 Oct 2019, 12:12 PM
 */
class RankSpectrumTest
{
    @Test
    void testGetRank()
    {
        MsnSpectrum spectrum = new MsnSpectrum();
        double[] ins = new double[]{1, 3, 6, 4, 9, 5, 4};

        for (int i = 0; i < ins.length; i++) {
            spectrum.add(i, ins[i]);
        }

        RankSpectrum rankSpectrum = RankSpectrum.from(spectrum);
        int[] ranks = new int[]{7, 6, 2, 4, 1, 3, 5};
        for (int i = 0; i < rankSpectrum.size(); i++) {
            assertEquals(rankSpectrum.getRank(i), ranks[i]);
        }
    }
}