package omics.util.ms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 17 Apr 2018, 12:41 PM
 */
class ScanNumberListTest
{
    @Test
    void testHashCode()
    {
        ScanNumberList list1 = ScanNumberList.from(3, 5);
        ScanNumberList list2 = ScanNumberList.from(5, 3);

        assertNotEquals(list1.hashCode(), list2.hashCode());

        ScanNumberList list3 = new ScanNumberList(1028);
        assertEquals(list1.hashCode(), list3.hashCode());
    }

    @Test
    void testToStringEmpty()
    {
        ScanNumberList list = new ScanNumberList();
        assertEquals(list.toString(), "");
    }
}