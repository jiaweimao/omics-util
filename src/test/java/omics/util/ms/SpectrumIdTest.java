package omics.util.ms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 23 Feb 2020, 3:22 PM
 */
class SpectrumIdTest
{
    @Test
    void testBuilder()
    {
        SpectrumId id = new SpectrumId.Builder().build();
        assertEquals(id.getClass(), DefaultSpectrumId.class);
    }


    @Test
    void testGetSpectrumSource()
    {
        SpectrumId.Builder builder = new SpectrumId.Builder();
        SpectrumId spectrumID = builder.build();
        assertNull(spectrumID.getMsDataId());

        spectrumID = builder.msDataId(MsDataId.of("test")).build();
        assertEquals(spectrumID.getMsDataId(), MsDataId.of("test"));

        spectrumID = builder.msDataId(MsDataId.of("M0M3_HepG2_NUCL_SCX_F5.mzML")).build();
        assertEquals(spectrumID.getMsDataId(), MsDataId.of("M0M3_HepG2_NUCL_SCX_F5.mzML"));
    }


    @Test
    void testParentScanNumber()
    {
        SpectrumId spectrumID = new SpectrumId.Builder().build();
        assertFalse(spectrumID.getParentScanNumber().isPresent());

        ScanNumber parentScanNumber = ScanNumber.interval(3, 8);
        spectrumID = new SpectrumId.Builder().parentScanNumber(parentScanNumber).build();

        assertTrue(spectrumID.getParentScanNumber().isPresent());
    }

}