package omics.util.ms.peaklist;

import omics.util.protein.ms.Ion;

import java.util.Objects;

public class MockPeakAnnotation implements PeakAnnotation
{
    private final Ion peptideIonType;
    private final int charge;
    private final int id;

    public MockPeakAnnotation(Ion ionType, int charge, int id)
    {
        Objects.requireNonNull(ionType);
        this.peptideIonType = ionType;

        this.charge = charge;
        this.id = id;
    }

    @Override
    public PeakAnnotation copy()
    {
        return new MockPeakAnnotation(peptideIonType, charge, id);
    }

    public int getCharge()
    {
        return charge;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        MockPeakAnnotation that = (MockPeakAnnotation) o;

        return charge == that.charge && id == that.id && peptideIonType == that.peptideIonType;
    }

    @Override
    public int hashCode()
    {
        int result = peptideIonType.hashCode();
        result = 31 * result + charge;
        result = 31 * result + id;
        return result;
    }

    @Override
    public String toString()
    {
        return "MockPeakAnnotation{" + "fragmentIonType=" + peptideIonType + ", charge=" + charge + ", id=" + id + '}';
    }

    public Ion getPeptideIonType()
    {
        return peptideIonType;
    }

    public int getId()
    {
        return id;
    }

    @Override
    public String getSymbol()
    {
        return null;
    }

    @Override
    public Ion getIon()
    {
        return peptideIonType;
    }
}
