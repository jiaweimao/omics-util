package omics.util.ms.peaklist;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;

import java.util.List;

/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
public class MockPeakSink<A extends PeakAnnotation> implements PeakSink<A>
{
    private final DoubleArrayList mzList = new DoubleArrayList();
    private final DoubleArrayList intensityList = new DoubleArrayList();

    private int startCalled = 0;
    private int endCalled = 0;

    @Override
    public void processPeak(double mz, double intensity, List<A> annotations)
    {
        mzList.add(mz);
        intensityList.add(intensity);
    }

    public void end()
    {
        endCalled++;
    }

    public void start(int size)
    {
        mzList.ensureCapacity(size);
        intensityList.ensureCapacity(size);

        startCalled++;
    }

    public double[] getMzList()
    {
        return mzList.toDoubleArray();
    }

    public double[] getIntensityList()
    {
        return intensityList.toDoubleArray();
    }

    public int getStartCalled()
    {
        return startCalled;
    }

    public int getEndCalled()
    {
        return endCalled;
    }
}
