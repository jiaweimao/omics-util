package omics.util.ms.peaklist;

import omics.util.ms.peaklist.impl.DoubleConstantPeakList;
import omics.util.ms.peaklist.impl.DoublePeakList;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class PeakListTest
{
    @Test
    void testIndexOfDoubleDouble()
    {
        DoublePeakList<PeakAnnotation> list = new DoublePeakList<>();
        list.addSorted(new double[]{1., 10., 50, 100, 101, 250., 300.}, new double[]{1, 1, 1, 1, 1, 1, 1});
        int[] ids = list.indexOf(5, 6);
        assertEquals(0, ids.length);

        ids = list.indexOf(2, 11);
        assertEquals(ids, new int[]{1});

        ids = list.indexOf(2, 200);
        assertEquals(ids, new int[]{1, 2, 3, 4});
    }

    @Test
    void testIndexOf()
    {
        List<PeakList<PeakAnnotation>> peakLists = build(new double[]{1, 2, 3, 4, 5, 6});

        for (PeakList<PeakAnnotation> peakList : peakLists) {
            assertEquals(0, peakList.indexOf(1), peakList.getPrecision().toString());
            assertEquals(5, peakList.indexOf(6), peakList.getPrecision().toString());
        }
    }

    private List<PeakList<PeakAnnotation>> build(double[] mzs)
    {
        double[] intensities = new double[mzs.length];
        Arrays.fill(intensities, 1);

        List<PeakList<PeakAnnotation>> peakLists = new ArrayList<>();
        for (Precision precision : Precision.values()) {

            peakLists.add(PeakListFactory.newPeakList(precision, mzs, intensities, mzs.length));
        }

        return peakLists;
    }


    @Test
    void testCalculateDoubleLength()
    {
        assertEquals(6.245, PeakListUtil.calcLength(new double[]{2, 5, 3, 1, 6, 7, 3}, 4), 0.00001);
    }

    @Test
    void testCalculateFloatLength()
    {
        assertEquals(6.245, PeakListUtil.calcLength(new float[]{2, 5, 3, 1, 6, 7, 3}, 4), 0.00001);
    }

    @Test
    void testSplit()
    {
        DoubleConstantPeakList<PeakAnnotation> list = new DoubleConstantPeakList<>(1.0);
        list.add(1.0);
        list.add(10.0);
        list.add(50.0);
        list.add(100.0);
        list.add(101.0);
        list.add(200.0);
        list.add(201.0);
        list.add(250.0);
        list.add(300.0);
        list.add(301.0);
        list.add(350.0);
        list.add(400.0);
        list.add(401.0);
        list.add(450.0);
        list.add(501.0);

        ArrayList<PeakList<PeakAnnotation>> lists = PeakListUtil.split(list, 100);
        assertEquals(5, lists.size());
        assertEquals("[1.0, 10.0, 50.0, 100.0, 101.0]", Arrays.toString(lists.get(0).getXs()));
        assertEquals("[200.0, 201.0]", Arrays.toString(lists.get(1).getXs()));
        assertEquals("[250.0, 300.0, 301.0]", Arrays.toString(lists.get(2).getXs()));
        assertEquals("[350.0, 400.0, 401.0]", Arrays.toString(lists.get(3).getXs()));
        assertEquals("[450.0, 501.0]", Arrays.toString(lists.get(4).getXs()));

        lists = PeakListUtil.split(list, 200);
        assertEquals(lists.size(), 3);
        assertEquals(Arrays.toString(lists.get(0).getXs()), "[1.0, 10.0, 50.0, 100.0, 101.0, 200.0, 201.0]");
        assertEquals(Arrays.toString(lists.get(1).getXs()), "[250.0, 300.0, 301.0, 350.0, 400.0, 401.0]");
        assertEquals(Arrays.toString(lists.get(2).getXs()), "[450.0, 501.0]");
    }
}