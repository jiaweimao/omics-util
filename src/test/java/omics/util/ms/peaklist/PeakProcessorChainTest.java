package omics.util.ms.peaklist;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class PeakProcessorChainTest
{
    @Test
    void testInitialize()
    {
        PeakProcessorChain<PeakAnnotation> peakProcessorChain = new PeakProcessorChain<>();

        PeakProcessor<PeakAnnotation, PeakAnnotation> processor1 = mock(PeakProcessor.class);
        PeakProcessor<PeakAnnotation, PeakAnnotation> processor2 = mock(PeakProcessor.class);

        peakProcessorChain.add(processor1).add(processor2);

        PeakList<PeakAnnotation> peakList1 = mock(PeakList.class);
        when(peakList1.size()).thenReturn(12);

        PeakSink<PeakAnnotation> sink1 = mock(PeakSink.class);
        peakProcessorChain.process(peakList1, sink1);

        verify(processor1).sink(processor2);
        verify(processor2).sink(sink1);
        verify(processor1).start(12);
        verify(processor1, times(12)).processPeak(0, 0, Collections.<PeakAnnotation>emptyList());
        verify(processor1).end();

        PeakList<PeakAnnotation> peakList2 = mock(PeakList.class);
        when(peakList2.size()).thenReturn(13);

        reset(processor1);
        PeakSink<PeakAnnotation> sink2 = mock(PeakSink.class);
        peakProcessorChain.process(peakList2, sink2);
        verify(processor1).sink(processor2);
        verify(processor2).sink(sink2);
        verify(processor1).start(13);
        verify(processor1, times(13)).processPeak(0, 0, Collections.<PeakAnnotation>emptyList());
        verify(processor1).end();
    }

    @SuppressWarnings("unchecked")
    @Test
    void testSingletonProcessor() throws Exception
    {

        PeakProcessorChain<PeakAnnotation> peakProcessorChain = new PeakProcessorChain<PeakAnnotation>();

        PeakProcessor<PeakAnnotation, PeakAnnotation> processor = mock(PeakProcessor.class);
        peakProcessorChain.add(processor);

        PeakList<PeakAnnotation> peakList = mock(PeakList.class);
        when(peakList.size()).thenReturn(12);

        PeakSink<PeakAnnotation> sink = mock(PeakSink.class);
        peakProcessorChain.process(peakList, sink);
        verify(processor).sink(sink);
        verify(processor).start(12);
        verify(processor, times(12)).processPeak(0, 0, Collections.<PeakAnnotation>emptyList());
        verify(processor).end();
    }

    @Test
    void testEmptyList()
    {
        PeakProcessorChain<PeakAnnotation> peakProcessorChain = new PeakProcessorChain<>();
        assertTrue(peakProcessorChain.isEmpty());

        @SuppressWarnings("unchecked") // Mockito can't mock generics
                PeakList<PeakAnnotation> peakList = mock(PeakList.class);
        when(peakList.size()).thenReturn(12);

        @SuppressWarnings("unchecked") // Mockito can't mock generics
                PeakSink<PeakAnnotation> sink = mock(PeakSink.class);

        peakProcessorChain.process(peakList, sink);

        verify(sink).start(12);
        verify(sink, times(12)).processPeak(0, 0, Collections.emptyList());
        verify(sink).end();
    }

    @SuppressWarnings("unchecked")
    @Test
    void testSharedProcessor()
    {
        PeakProcessor<PeakAnnotation, PeakAnnotation> processor1 = new IdentityPeakProcessor<>();
        PeakProcessor<PeakAnnotation, PeakAnnotation> processor2 = new IdentityPeakProcessor<>();
        PeakProcessor<PeakAnnotation, PeakAnnotation> processor3 = new IdentityPeakProcessor<>();

        PeakProcessorChain<PeakAnnotation> chain1 = new PeakProcessorChain<>();
        PeakProcessorChain<PeakAnnotation> chain2 = new PeakProcessorChain<>();

        chain1.add(processor1).add(processor2);
        chain2.add(processor1).add(processor3);

        PeakList<PeakAnnotation> peaks = mock(PeakList.class);
        when(peaks.size()).thenReturn(1);
        when(peaks.getX(0)).thenReturn(10.68);
        when(peaks.getY(0)).thenReturn(12.0);

        PeakSink<PeakAnnotation> sink1 = mock(PeakSink.class);
        PeakSink<PeakAnnotation> sink2 = mock(PeakSink.class);

        chain1.process(peaks, sink1);

        chain2.process(peaks, sink2);

        verify(sink1).start(1);
        verify(sink1).processPeak(10.68, 12.0, Collections.emptyList());
        verify(sink1).end();

        verify(sink2).start(1);
        verify(sink2).processPeak(10.68, 12.0, Collections.emptyList());
        verify(sink2).end();
    }

    @SuppressWarnings("unchecked")
    @Test
    void testProcessTList()
    {
        PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor = mock(PeakProcessor.class);
        PeakSink<PeakAnnotation> peakSink = mock(PeakSink.class);

        DoubleList mzs = new DoubleArrayList(new double[]{1, 2, 3});
        DoubleList intensities = new DoubleArrayList(new double[]{10, 20, 30});
        PeakAnnotation annotation = mock(PeakAnnotation.class);
        Int2ObjectMap<List<PeakAnnotation>> annotationMap = new Int2ObjectOpenHashMap<>();
        annotationMap.put(1, Arrays.asList(annotation));

        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<>().add(peakProcessor);
        processorChain.process(mzs, intensities, annotationMap, peakSink);

        verify(peakProcessor).sink(peakSink);
        verify(peakProcessor).start(3);
        verify(peakProcessor).processPeak(1, 10, Collections.emptyList());
        verify(peakProcessor).processPeak(2, 20, Arrays.asList(annotation));
        verify(peakProcessor).processPeak(3, 30, Collections.emptyList());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }

    @SuppressWarnings("unchecked")
    @Test
    void testProcessNumber()
    {
        PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor = mock(PeakProcessor.class);
        PeakSink<PeakAnnotation> peakSink = mock(PeakSink.class);

        Number[] mzs = new Number[]{1, 2, 3};
        Number[] intensities = new Number[]{10, 20, 30};
        PeakAnnotation annotation = mock(PeakAnnotation.class);
        Int2ObjectMap<List<PeakAnnotation>> annotationMap = new Int2ObjectOpenHashMap<>();
        annotationMap.put(1, Arrays.asList(annotation));

        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<>(peakProcessor);
        assertFalse(processorChain.isEmpty());

        processorChain.process(mzs, intensities, annotationMap, peakSink);

        verify(peakProcessor).sink(peakSink);
        verify(peakProcessor).start(3);
        verify(peakProcessor).processPeak(1, 10, Collections.emptyList());
        verify(peakProcessor).processPeak(2, 20, Arrays.asList(annotation));
        verify(peakProcessor).processPeak(3, 30, Collections.emptyList());
        verify(peakProcessor).end();
        verifyNoMoreInteractions(peakProcessor);
    }
}