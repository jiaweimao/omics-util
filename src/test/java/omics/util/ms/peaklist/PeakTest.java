package omics.util.ms.peaklist;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PeakTest
{
    @Test
    void testBadConstrMzNeg()
    {
        assertThrows(IllegalArgumentException.class, () -> Peak.noIntensity(Double.NaN, 3));
    }

    @Test
    void testConstrZeroCharge()
    {
        Peak p = Peak.noIntensity(189, 0);
        assertEquals(189, p.getMz(), 0.00);
    }

    @Test
    void testMassForNoCharge()
    {
        Peak peak = new Peak(463.87, 12);
        assertThrows(IllegalStateException.class, peak::getMass);
    }

    @Test
    void testEquality()
    {
        Peak peak1 = Peak.noIntensity(189, 3);
        Peak peak2 = Peak.noIntensity(189, 3);

        assertEquals(0, peak1.compareTo(peak2));
        assertEquals(peak1, peak2);
        assertEquals(peak1.hashCode(), peak2.hashCode());
    }

    @Test
    void testEquality2()
    {
        Peak peak1 = Peak.noIntensity(189, -4);
        Peak peak2 = Peak.noIntensity(189, -3);

        assertEquals(-1, peak1.compareTo(peak2));
        assertNotEquals(peak1, peak2);
        assertNotEquals(peak1.hashCode(), peak2.hashCode());
    }

    @Test
    void testCompChargeFirst()
    {
        Peak peak1 = Peak.noIntensity(189, 3);
        Peak peak2 = Peak.noIntensity(189, 2);

        assertTrue(peak1.compareTo(peak2) > 0);
    }

    @Test
    void testCompMzSecond()
    {
        Peak peak1 = Peak.noIntensity(183, 2);
        Peak peak2 = Peak.noIntensity(189, 2);

        assertTrue(peak1.compareTo(peak2) < 0);
    }

    @Test
    void testCompIntensityLast()
    {
        Peak peak1 = new Peak(189, 19, 2);
        Peak peak2 = new Peak(189, 29, 2);

        assertTrue(peak1.compareTo(peak2) < 0);
    }

    @Test
    void testChargeList()
    {
        final int[] constructorChargeList = {2, 3, 4};
        Peak peak = new Peak(67.9, 12.0, constructorChargeList);

        assertEquals(2, peak.getCharge());
        assertArrayEquals(new int[]{2, 3, 4}, peak.getCharges());

        // Test that changing the charge array does not alter the charge list
        // stored by the peak
        peak.getCharges()[0] = 0;
        assertEquals(2, peak.getCharge());
        assertArrayEquals(new int[]{2, 3, 4}, peak.getCharges());

        // Test that the array used during construction is copied
        constructorChargeList[0] = 0;
        assertEquals(2, peak.getCharge());
        assertArrayEquals(new int[]{2, 3, 4}, peak.getCharges());
    }

    @Test
    void testZeroCharge()
    {
        assertThrows(IllegalArgumentException.class, () -> new Peak(12.8, 4, 2, 0));

    }

    @Test
    void testEmptyPeak()
    {
        Peak peak = new Peak();

        assertEquals(0, peak.getMz(), 0.00000001);
        assertEquals(0, peak.getIntensity(), 0.00000001);
        assertEquals(0, peak.getCharge(), 0.00000001);
        assertArrayEquals(new int[]{}, peak.getCharges());
    }

    @Test
    void testCopy()
    {
        Peak peak = new Peak(425.98, 34238.8, 2, -1, -9);

        assertEquals(425.98, peak.getMz(), 0.00000001);
        assertEquals(34238.8, peak.getIntensity(), 0.00000001);
        assertEquals(2, peak.getCharge());

        assertArrayEquals(new int[]{2, -1, -9}, peak.getCharges());

        assertEquals(849.945447, peak.getMass(), 0.0000001);

        Peak copied = peak.copy();

        assertEquals(425.98, copied.getMz(), 0.00000001);
        assertEquals(34238.8, copied.getIntensity(), 0.00000001);
        assertEquals(2, copied.getCharge());
        assertArrayEquals(new int[]{2, -1, -9}, copied.getCharges());
        assertEquals(849.945447, copied.getMass(), 0.0000001);
    }

    @Test
    void testSetIntensity()
    {
        Peak peak = new Peak(451.9361, 12);
        assertEquals(12.0, peak.getIntensity(), 0.000000001);

        peak.setIntensity(45);
        assertEquals(45.0, peak.getIntensity(), 0.000000001);
    }
}