package omics.util.ms.peaklist;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Oct 2018, 1:29 PM
 */
class ToleranceTest
{
    @Test
    void testFrom()
    {
        Tolerance tol = Tolerance.from("10ppm");
        assertEquals(tol, Tolerance.ppm(10));
        assertEquals(tol, Tolerance.from(tol.toString()));

        Tolerance daTol = Tolerance.from("0.05Da");
        assertEquals(daTol, Tolerance.abs(0.05));
        assertEquals(daTol, Tolerance.from(daTol.toString()));

        Tolerance rangeDaTol = Tolerance.from("(0.3, 0.5) Da");
        assertEquals(rangeDaTol, Tolerance.abs(0.3, 0.5));
        assertEquals(rangeDaTol, Tolerance.from(rangeDaTol.toString()));

        Tolerance rangePpmTol = Tolerance.from("(5, 6) ppm");
        assertEquals(rangePpmTol, Tolerance.ppm(5, 6));
        assertEquals(rangePpmTol, Tolerance.from(rangePpmTol.toString()));
    }

    @Test
    void testHashCode()
    {
        Tolerance da = Tolerance.abs(2);
        Tolerance ppm = Tolerance.ppm(2);

        Set<Tolerance> set = new HashSet<>();
        set.add(da);
        set.add(ppm);
        assertEquals(set.size(), 2);
    }

    @Test
    void testRange()
    {
        double mz = 400;
        Tolerance tol = Tolerance.ppm(1);
        assertEquals(tol.getMin(mz), 399.9996, 1e-4);
        assertEquals(tol.getMax(mz), 400.0004, 1e-4);
    }

    @Test
    void testError()
    {
        double theo = 1000;
        double exp = 999.999876;
        System.out.println(Tolerance.getErrorInPpm(theo, exp));
    }

}