package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.DoublePeakList;
import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Feb 2018, 8:50 PM
 */
class BinnedSpectrumFilterTest
{
    private double[] masses = new double[]{1, 2, 3, 4.6, 5, 6, 7, 8, 9, 10, 10.3, 11.5, 13, 14, 15.7, 16};
    private double[] intensities = new double[]{2, 4, 6, 8, 10, 1, 3, 5, 8, 9, 6, 8, 10, 1, 3, 5};

    @Test
    void testSum()
    {
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);
        DoublePeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        BinnedSpectrumFilter<PeakAnnotation> binnedSpectrumFilter = new BinnedSpectrumFilter<>(0, 20.0,
                2.0, IntensityMode.SUM);
        newPL.apply(binnedSpectrumFilter);

        assertArrayEquals(new double[]{1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0}, newPL.getXs());
        assertArrayEquals(new double[]{2.0, 10.0, 18.0, 4.0, 13.0, 23.0, 10.0, 4.0, 5.0}, newPL.getYs());
    }

    @Test
    void testHighest()
    {
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);
        DoublePeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        BinnedSpectrumFilter<PeakAnnotation> binnedSpectrumFilter = new BinnedSpectrumFilter<>(0, 20.0,
                2.0, IntensityMode.HIGHEST);
        newPL.apply(binnedSpectrumFilter);
        final double[] ms = newPL.getXs();
        final double[] intens = newPL.getYs();
        assertArrayEquals(new double[]{1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0}, ms, 0.0000001);
        assertArrayEquals(new double[]{2.0, 6.0, 10.0, 3.0, 8.0, 9.0, 10.0, 3.0, 5.0}, intens, 0.0000001);
    }

    @Test
    void testAverage()
    {
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);

        DoublePeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());

        BinnedSpectrumFilter<PeakAnnotation> binnedSpectrumFilter = new BinnedSpectrumFilter<>(0, 20.0, 2.0, IntensityMode.AVG);
        newPL.apply(binnedSpectrumFilter);

        assertArrayEquals(new double[]{1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0}, newPL.getXs(), 0.0000001);
        assertArrayEquals(new double[]{2.0, 5.0, 9.0, 2.0, 6.5, 7.6666, 10.0, 2.0, 5.0}, newPL.getYs(), 0.001);
    }

    @Test
    void annotationTest()
    {
        DoublePeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(21.520550658199998, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(30.525833001199995, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(35.5180079692, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(35.5362007222, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(42.033825208699994, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(44.5232903122, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(49.5154652802, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(56.54709975920001, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(58.520747623199995, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(60.04438989469999, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(65.06037430970001, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(70.0287398307, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(70.0651253367, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(79.5498394132, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(88.0393045167, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(88.0631139637, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(93.03147948469999, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(98.0236544527, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(102.03676182769999, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(112.08692341070002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(116.0342191387, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(128.0762213387, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(129.11347251170002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(136.5894958892, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(141.55786141020002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(150.56314375320002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(158.09240271870001, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(171.1002277507, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(171.5922355432, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(175.11895181970002, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(180.1055100937, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(185.05568286169998, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(203.0662475477, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(228.6136992662, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(229.1057070587, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(237.6189816092, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(255.1451665697, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(272.1717156707, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(282.1084467127, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(300.1190113987, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(341.19317939369995, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(342.17719497869996, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(359.20374407969996, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(456.22012242470004, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(457.20413800970005, 1.0, Collections.singletonList(mockPeakAnnotation()));
        peakList.add(474.23068711070005, 1.0, Collections.singletonList(mockPeakAnnotation()));

        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        int annotCnt = 0;
        for (int i = 0; i < peakList.size(); i++) {
            expPeakList.add(peakList.getX(i), peakList.getY(i), peakList.getAnnotations(i));
            annotCnt += peakList.getAnnotations(i).size();
        }

        DoublePeakList<PeakAnnotation> newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        newPL.apply(new BinnedSpectrumFilter<>(0.0, 500.0, 10.0, IntensityMode.SUM));

        int[] indexes = newPL.getAnnotationIndexes();
        int annotCntNew = 0;
        for (int indexe : indexes) {
            annotCntNew += newPL.getAnnotations(indexe).size();
        }

        assertEquals(annotCnt, annotCntNew);
    }

    private PeakAnnotation mockPeakAnnotation()
    {
        PeakAnnotation peakAnnotation = Mockito.mock(PeakAnnotation.class);
        when(peakAnnotation.copy()).thenReturn(peakAnnotation);
        return peakAnnotation;
    }
}