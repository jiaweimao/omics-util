package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.DoublePeakList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class MzFilterTest
{
    @Test
    void test()
    {
        MzFilter<PeakAnnotation> filter = new MzFilter<>(Tolerance.abs(0.01));

        PeakList<PeakAnnotation> pl = new DoublePeakList<>();
        for (int i = 0; i < 100; i++) {
            pl.add(i + 1, 1);
            filter.addMz(i * 10 + 0.005);
        }

        assertEquals(100, pl.size());

        pl.apply(filter);

        assertEquals(90, pl.size());

        int closestIndex = pl.getClosestIndex(10);
        assertEquals(9.0, pl.getX(closestIndex), 0.01);

        closestIndex = pl.getClosestIndex(20);
        assertEquals(19.0, pl.getX(closestIndex), 0.01);
    }
}