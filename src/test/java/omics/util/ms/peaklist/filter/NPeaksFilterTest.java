package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.MockPeakSink;
import omics.util.ms.peaklist.PeakAnnotation;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class NPeaksFilterTest
{
    @Test
    void test()
    {
        NPeaksFilter<PeakAnnotation> filter = new NPeaksFilter<PeakAnnotation>(3);

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        filter.sink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        filter.start(7);
        filter.processPeak(1, 150, annotations);
        filter.processPeak(2, 10, annotations);
        filter.processPeak(3, 1, annotations);
        filter.processPeak(4, 100, annotations);
        filter.processPeak(5, 200, annotations);
        filter.processPeak(6, 5, annotations);
        filter.processPeak(7, 1000, annotations);
        filter.end();

        assertArrayEquals(new double[]{1, 5, 7}, sink.getMzList(), 0.00001);
        assertArrayEquals(new double[]{150, 200, 1000}, sink.getIntensityList(), 0.000001);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }


    @Test
    void test2()
    {
        NPeaksFilter<PeakAnnotation> filter = new NPeaksFilter<PeakAnnotation>(3);

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        filter.sink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        filter.start(4);
        filter.processPeak(1, 4, annotations);
        filter.processPeak(2, 7, annotations);
        filter.processPeak(3, 10, annotations);
        filter.processPeak(4, 5, annotations);
        filter.end();

        assertArrayEquals(new double[]{2, 3, 4}, sink.getMzList(), 0.00001);
        assertArrayEquals(new double[]{7, 10, 5}, sink.getIntensityList(), 0.00001);
    }

    @Test
    void test3()
    {
        NPeaksFilter<PeakAnnotation> filter = new NPeaksFilter<PeakAnnotation>(3);

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();

        filter.sink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        filter.start(7);
        filter.processPeak(1, 150, annotations);
        filter.processPeak(2, 10, annotations);
        filter.processPeak(3, 1, annotations);
        filter.processPeak(4, 100, annotations);
        filter.processPeak(5, 100, annotations);
        filter.processPeak(6, 5, annotations);
        filter.processPeak(7, 1000, annotations);
        filter.end();

        assertArrayEquals(new double[]{1, 4, 7}, sink.getMzList(), 0.000001);
        assertArrayEquals(new double[]{150, 100, 1000}, sink.getIntensityList(), 0.000001);
    }
}