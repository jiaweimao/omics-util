package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.MockPeakSink;
import omics.util.ms.peaklist.PeakAnnotation;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class NPeaksPerBinFilterTest
{
    @Test
    void test()
    {
        NPeaksPerBinFilter<PeakAnnotation> binFilter = new NPeaksPerBinFilter<PeakAnnotation>(3, 500);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        binFilter.sink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        binFilter.start(31);

        binFilter.processPeak(104, 77, annotations);
        binFilter.processPeak(144, 932, annotations);
        binFilter.processPeak(166, 604, annotations);
        binFilter.processPeak(187, 394, annotations);
        binFilter.processPeak(219, 541, annotations);
        binFilter.processPeak(363, 684, annotations);
        binFilter.processPeak(467, 653, annotations);
        binFilter.processPeak(469, 412, annotations);
        binFilter.processPeak(478, 990, annotations);

        binFilter.processPeak(548, 176, annotations);
        binFilter.processPeak(591, 528, annotations);
        binFilter.processPeak(749, 615, annotations);
        binFilter.processPeak(754, 394, annotations);
        binFilter.processPeak(763, 232, annotations);
        binFilter.processPeak(834, 639, annotations);
        binFilter.processPeak(866, 166, annotations);
        binFilter.processPeak(925, 684, annotations);
        binFilter.processPeak(927, 198, annotations);
        binFilter.processPeak(980, 878, annotations);
        binFilter.processPeak(982, 290, annotations);

        binFilter.processPeak(1011, 984, annotations);
        binFilter.processPeak(1015, 480, annotations);
        binFilter.processPeak(1074, 202, annotations);
        binFilter.processPeak(1241, 815, annotations);
        binFilter.processPeak(1249, 997, annotations);
        binFilter.processPeak(1308, 224, annotations);
        binFilter.processPeak(1319, 795, annotations);
        binFilter.processPeak(1360, 301, annotations);
        binFilter.processPeak(1416, 719, annotations);

        binFilter.processPeak(1526, 959, annotations);
        binFilter.processPeak(1546, 232, annotations);

        binFilter.end();

        assertArrayEquals(new double[]{144, 363, 478, 834, 925, 980, 1011, 1241, 1249, 1526, 1546}, sink.getMzList(),
                0.000001);
        assertArrayEquals(new double[]{932, 684, 990, 639, 684, 878, 984, 815, 997, 959, 232},
                sink.getIntensityList(), 0.000001);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }
}