package omics.util.ms.peaklist.filter;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.impl.DoublePeakList;
import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class NPeaksPerSlidingWindowFilterTest
{
    double[] masses = new double[]{1, 2, 3, 4.6, 5, 6, 7, 8, 9, 10, 10.3, 11.5, 13, 14, 15.7, 16};
    double[] intensities = new double[]{2, 4, 6, 8, 10, 1, 3, 5, 8, 9, 6, 8, 10, 1, 3, 5};

    double[] masses2 = new double[]{995.465, 997.549, 1002.213, 1009.445, 1012.809, 1015.269, 1023.958, 1024.746,
            1032.158, 1038.543, 1049.680, 1055.237, 1056.236, 1061.347, 1063.269, 1069.886, 1074.268, 1075.122,
            1080.235, 1088.610, 1094.540, 1096.541, 1102.804, 1107.303, 1112.495, 1117.861, 1120.826, 1125.643,
            1129.048, 1136.221, 1138.760, 1141.752, 1149.159, 1154.643, 1167.231, 1168.390, 1172.392, 1177.803,
            1185.206, 1191.259, 1192.455, 1199.366, 1209.212, 1210.209, 1216.670};
    double[] intensities2 = new double[]{18.288, 10.924, 6.442, 17.300, 17.788, 18.947, 21.898, 11.378, 22.440,
            10.302, 50.827, 17.054, 22.280, 11.892, 7.995, 10.341, 215.683, 88.473, 18.228, 13.208, 3.964, 5.049, 2.980,
            1.780, 13.860, 5.808, 5.583, 2.364, 1.033, 13.762, 21.549, 5.541, 14.566, 19.965, 239.495, 40.558, 21.821,
            9.397, 1820.518, 630.056, 23.832, 3.843, 6.159, 383.467, 58.726};

    PeakList<PeakAnnotation> expPeakList, expPeakList2, newPL;
    NPeaksPerSlidingWindowFilter<PeakAnnotation> perSlidingWindowFilter1Sliding, perSlidingWindowFilter2Sliding,
            perSlidingWindowFilter3Sliding, perSlidingWindowFilter4Sliding, perSlidingWindowFilter5Sliding,
            perSlidingWindowFilter6Sliding, perSlidingWindowFilter7Sliding;

    @BeforeEach
    void init()
    {
        newPL = new DoublePeakList<>();
        expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);
        expPeakList2 = new DoublePeakList<>();
        expPeakList2.addSorted(masses2, intensities2, masses2.length);
        perSlidingWindowFilter1Sliding = new NPeaksPerSlidingWindowFilter<>(1, 2.0, 0.0);
        perSlidingWindowFilter2Sliding = new NPeaksPerSlidingWindowFilter<>(2, 2.0, 0.0);
        perSlidingWindowFilter3Sliding = new NPeaksPerSlidingWindowFilter<>(2, 5.0, 1.2);
        perSlidingWindowFilter4Sliding = new NPeaksPerSlidingWindowFilter<>(2, 2.0, 0.5);
        perSlidingWindowFilter5Sliding = new NPeaksPerSlidingWindowFilter<>(1, 0.001, 0.0);
        perSlidingWindowFilter6Sliding = new NPeaksPerSlidingWindowFilter<>(1, 0.001, 10.0);
        perSlidingWindowFilter7Sliding = new NPeaksPerSlidingWindowFilter<>(2, 10.0, 1.2);
    }

    @Test
    void testOneFilter1()
    {
        newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        newPL.apply(perSlidingWindowFilter1Sliding);
        final double[] ms = newPL.getXs();
        assertTrue(Arrays.equals(new double[]{5, 10, 13, 16}, ms));
    }

    @Test
    void testOneFilter2()
    {
        newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        newPL.apply(perSlidingWindowFilter2Sliding);
        final double[] ms = newPL.getXs();
        assertTrue(Arrays.equals(new double[]{2.0, 4.6, 5.0, 9.0, 10.0, 13.0, 15.7, 16.0}, ms));
    }

    @Test
    void testOneFilter3()
    {
        newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        newPL.apply(perSlidingWindowFilter3Sliding);
        final double[] ms = newPL.getXs();
        assertArrayEquals(new double[]{4.6, 5, 6, 13, 14}, ms);
    }

    @Test
    void testOneFilter4()
    {
        newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        newPL.apply(perSlidingWindowFilter4Sliding);
        final double[] ms = newPL.getXs();
        assertArrayEquals(new double[]{2.0, 4.6, 5.0, 9.0, 10.0, 10.3, 13.0, 15.7, 16.0}, ms);
    }

    @Test
    void testOneFilter5()
    {
        newPL = expPeakList.copy(new IdentityPeakProcessor<PeakAnnotation>());
        newPL.apply(perSlidingWindowFilter5Sliding);
        final double[] ms = newPL.getXs();
        assertArrayEquals(masses, ms);
    }

    @Test
    void testOneFilter6()
    {
        newPL = expPeakList.copy(new IdentityPeakProcessor<>());
        newPL.apply(perSlidingWindowFilter6Sliding);
        final double[] ms = newPL.getXs();
        assertArrayEquals(masses, ms);
    }

    @Test
    void testOneFilterReal()
    {
        newPL = expPeakList2.copy(new IdentityPeakProcessor<>());
        newPL.apply(perSlidingWindowFilter7Sliding);
        final double[] ms = newPL.getXs();
        double[] res = new double[]{995.465, 997.549, 1012.809, 1015.269, 1023.958, 1032.158, 1038.543, 1049.68,
                1056.236, 1074.268, 1075.122, 1088.61, 1096.541, 1112.495, 1117.861, 1136.221, 1138.76, 1149.159,
                1154.643, 1167.231, 1168.39, 1185.206, 1191.259, 1210.209, 1216.67};
        assertArrayEquals(res, ms, 0.0001);
    }
}