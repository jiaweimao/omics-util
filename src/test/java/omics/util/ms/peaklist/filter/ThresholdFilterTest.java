package omics.util.ms.peaklist.filter;

import omics.util.math.MathUtils;
import omics.util.ms.peaklist.MockPeakSink;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakProcessor;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class ThresholdFilterTest
{
    @Test
    void test()
    {
        double[] mzs = new double[]{1009.0023719667987, 1080.6453215726483, 1143.6371799568678, 1313.3446681570776,
                1497.9970067622116, 1573.7126261762332, 1589.3719155794226, 1647.788997704897, 1659.2531083438853,
                1674.3943350407728, 1808.801080376506, 1856.343197234145, 1862.2359488629222, 1882.9627022854636,
                1895.1735998300373, 2089.4074737867454, 2136.024062438867, 2184.3728777275037, 2206.722055021579,
                2293.9203234875304};
        double[] intensities = new double[]{7.0, 15.0, 36.0, 22.0, 12.0, 30.0, 7.0, 9.0, 14.0, 12.0, 20.0, 6.0, 2.0,
                4.0, 1.0, 24.0, 4.0, 14.0, 24.0, 10.0};

        double mean = MathUtils.mean(intensities);
        double rsd = Math.sqrt(MathUtils.variance(intensities, mean)) / mean;
        double rsd2 = Math.sqrt(MathUtils.variance(intensities)) / mean;

//        System.out.println(mean);
//        System.out.println(rsd);
//        System.out.println(rsd2);

        ThresholdFilter<PeakAnnotation> filter = new ThresholdFilter<PeakAnnotation>(10);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        filter.sink(sink);

        runProcessor(filter, mzs, intensities);

        double[] actualMz = sink.getMzList();
        double[] actualIntensities = sink.getIntensityList();

        assertEquals(12, actualMz.length);

        assertEquals(1080.6453215726483, actualMz[0], 0.000000000001);
        assertEquals(15.0, actualIntensities[0], 0.000000000001);
        assertEquals(1143.6371799568678, actualMz[1], 0.000000000001);
        assertEquals(36.0, actualIntensities[1], 0.000000000001);
        assertEquals(1313.3446681570776, actualMz[2], 0.000000000001);
        assertEquals(22.0, actualIntensities[2], 0.000000000001);
        assertEquals(1497.9970067622116, actualMz[3], 0.000000000001);
        assertEquals(12.0, actualIntensities[3], 0.000000000001);
        assertEquals(1573.7126261762332, actualMz[4], 0.000000000001);
        assertEquals(30.0, actualIntensities[4], 0.000000000001);
        assertEquals(1659.2531083438853, actualMz[5], 0.000000000001);
        assertEquals(14.0, actualIntensities[5], 0.000000000001);
        assertEquals(1674.3943350407728, actualMz[6], 0.000000000001);
        assertEquals(12.0, actualIntensities[6], 0.000000000001);
        assertEquals(1808.801080376506, actualMz[7], 0.000000000001);
        assertEquals(20.0, actualIntensities[7], 0.000000000001);
        assertEquals(2089.4074737867454, actualMz[8], 0.000000000001);
        assertEquals(24.0, actualIntensities[8], 0.000000000001);
        assertEquals(2184.3728777275037, actualMz[9], 0.000000000001);
        assertEquals(14.0, actualIntensities[9], 0.000000000001);
        assertEquals(2206.722055021579, actualMz[10], 0.000000000001);
        assertEquals(24.0, actualIntensities[10], 0.000000000001);
        assertEquals(2293.9203234875304, actualMz[11], 0.000000000001);
        assertEquals(10.0, actualIntensities[11], 0.000000000001);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }

    @Test
    void test2() throws Exception
    {

        double[] mzs = new double[]{1009.0023719667987, 1080.6453215726483, 1143.6371799568678, 1313.3446681570776,
                1497.9970067622116, 1573.7126261762332, 1589.3719155794226, 1647.788997704897, 1659.2531083438853,
                1674.3943350407728, 1808.801080376506, 1856.343197234145, 1862.2359488629222, 1882.9627022854636,
                1895.1735998300373, 2089.4074737867454, 2136.024062438867, 2184.3728777275037, 2206.722055021579,
                2293.9203234875304};
        double[] intensities = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0};

        ThresholdFilter<PeakAnnotation> filter = new ThresholdFilter<PeakAnnotation>(0);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        filter.sink(sink);

        runProcessor(filter, mzs, intensities);

        double[] actualMz = sink.getMzList();

        assertEquals(20, actualMz.length);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }

    @Test
    void test3() throws Exception
    {
        double[] mzs = new double[]{1009.0023719667987, 1080.6453215726483, 1143.6371799568678, 1313.3446681570776,
                1497.9970067622116, 1573.7126261762332, 1589.3719155794226, 1647.788997704897, 1659.2531083438853,
                1674.3943350407728, 1808.801080376506, 1856.343197234145, 1862.2359488629222, 1882.9627022854636,
                1895.1735998300373, 2089.4074737867454, 2136.024062438867, 2184.3728777275037, 2206.722055021579,
                2293.9203234875304};
        double[] intensities = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0};

        ThresholdFilter<PeakAnnotation> filter = new ThresholdFilter<PeakAnnotation>(0,
                ThresholdFilter.Inequality.GREATER);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<PeakAnnotation>();
        filter.sink(sink);

        runProcessor(filter, mzs, intensities);

        double[] actualMz = sink.getMzList();
        double[] actualIntensities = sink.getIntensityList();

        assertEquals(1, actualMz.length);

        assertEquals(1009.0023719667987, actualMz[0], 0.000000000001);
        assertEquals(1.0, actualIntensities[0], 0.000000000001);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }

    private void runProcessor(PeakProcessor<PeakAnnotation, PeakAnnotation> processor, double[] keys, double[] values)
    {
        processor.start(keys.length);

        for (int i = 0; i < keys.length; i++) {

            List<PeakAnnotation> objects = Collections.emptyList();
            processor.processPeak(keys[i], values[i], objects);
        }

        processor.end();
    }
}