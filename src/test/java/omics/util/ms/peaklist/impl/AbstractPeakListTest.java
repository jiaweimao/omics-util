package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.*;
import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import omics.util.protein.ms.Ion;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


class AbstractPeakListTest
{
    @Test
    void testCopyConstructor()
    {
        DoubleConstantPeakList<MockPeakAnnotation> peakList = new DoubleConstantPeakList<>(6);
        peakList.getPrecursor().setMzAndCharge(348.892354, 12);
        peakList.getPrecursor().setIntensity(500);

        peakList.add(10.0, 10.0);
        peakList.add(20.0, 20.0);
        peakList.add(30.0, 30.0);
        peakList.add(40.0, 40.0);
        peakList.add(50.0, 50.0, new MockPeakAnnotation(Ion.y, 10, 2));
        peakList.add(60.0, 60.0, new MockPeakAnnotation(Ion.y, 10, 3));

        DoubleConstantPeakList<MockPeakAnnotation> copy = peakList.copy(new IdentityPeakProcessor<>());

        Peak precursorCopy = copy.getPrecursor();

        assertEquals(348.892354, precursorCopy.getMz(), 0.000001);
        assertEquals(500, precursorCopy.getIntensity(), 0.000001);
        assertEquals(12, precursorCopy.getCharge());

        assertTrue(copy.hasAnnotations());
        assertArrayEquals(new int[]{4, 5}, copy.getAnnotationIndexes());

        MockPeakAnnotation annotation = copy.getFirstAnnotation(4);
        assertEquals(Ion.y, annotation.getPeptideIonType());
        assertEquals(10, annotation.getCharge());
        assertEquals(2, annotation.getId());

        annotation = copy.getFirstAnnotation(5);
        assertEquals(Ion.y, annotation.getPeptideIonType());
        assertEquals(10, annotation.getCharge());
        assertEquals(3, annotation.getId());
    }

    @Test
    void testShiftAnnotations() throws Exception
    {
        MockPeakList peakList = new MockPeakList(4);

        peakList.addAnnotations(1, Collections.singletonList(new MockPeakAnnotation(Ion.y, 2, 11)));
        peakList.addAnnotations(2, Collections.singletonList(new MockPeakAnnotation(Ion.y, 2, 11)));
        peakList.addAnnotations(3, Collections.singletonList(new MockPeakAnnotation(Ion.y, 2, 11)));

        assertArrayEquals(new int[]{1, 2, 3}, peakList.getAnnotationIndexes());

        peakList.shiftAnnotations(2);
        assertArrayEquals(new int[]{1, 3, 4}, peakList.getAnnotationIndexes());
    }

    @Test
    void testShiftAnnotations2()
    {

        DoublePeakList<MockPeakAnnotation> peakList = new DoublePeakList<>(4);

        peakList.add(4, 5, new MockPeakAnnotation(Ion.a, 2, 14));
        peakList.add(1, 5, new MockPeakAnnotation(Ion.a, 2, 11));
        peakList.add(3, 5, new MockPeakAnnotation(Ion.a, 2, 13));
        peakList.add(2, 5, new MockPeakAnnotation(Ion.a, 2, 12));

        assertEquals(4, peakList.annotationMap.size());
        peakList.add(2.5, 5, new MockPeakAnnotation(Ion.a, 2, 15));

        assertEquals(5, peakList.annotationMap.size());

        peakList.shiftAnnotations(1);
        // the shift operation does not change the size.
        assertEquals(5, peakList.annotationMap.size());

        assertArrayEquals(new int[]{0, 2, 3, 4, 5}, peakList.getAnnotationIndexes());
        assertEquals(5, peakList.size);

        assertEquals(11, peakList.getFirstAnnotation(0).getId());
        assertEquals(12, peakList.getFirstAnnotation(2).getId());
        assertEquals(15, peakList.getFirstAnnotation(3).getId());
        assertEquals(13, peakList.getFirstAnnotation(4).getId());
        assertEquals(14, peakList.getFirstAnnotation(5).getId());
    }

    @Test
    void testExceptionOnAddSorted() throws Exception
    {
        MockPeakList peakList = new MockPeakList(0);
        assertThrows(IllegalStateException.class, () -> peakList.addSorted(new double[]{1, 2}, new double[]{10}));
    }

    @Test
    void testWrapperPeakCursor() throws Exception
    {
        @SuppressWarnings("rawtypes")
        PeakCursor cursor = mock(PeakCursor.class);
        @SuppressWarnings("rawtypes")
        Function<List, List> listListFunction = mock(Function.class);
        @SuppressWarnings("rawtypes")
        WrapperPeakCursor wrapper = new WrapperPeakCursor(cursor, listListFunction);

        Mockito.reset(cursor);
        wrapper.size();
        verify(cursor).size();

        Mockito.reset(cursor);
        wrapper.isEmpty();
        verify(cursor).isEmpty();

        Mockito.reset(cursor);
        wrapper.resetCursor();
        verify(cursor).resetCursor();

        Mockito.reset(cursor);
        wrapper.next();
        verify(cursor).next();

        Mockito.reset(cursor);
        wrapper.previous();
        verify(cursor).previous();

        Mockito.reset(cursor);
        wrapper.next(1234.98);
        verify(cursor).next(1234.98);

        Mockito.reset(cursor);
        wrapper.currIntensity();
        verify(cursor).currIntensity();

        Mockito.reset(cursor);
        wrapper.currAnnotations();
        verify(cursor).currAnnotations();
        verify(listListFunction).apply(anyList());

        Mockito.reset(cursor);
        wrapper.currMz();
        verify(cursor).currMz();

        Mockito.reset(cursor);
        wrapper.canPeek(8);
        verify(cursor).canPeek(8);

        Mockito.reset(cursor);
        wrapper.peekIntensity(4);
        verify(cursor).peekIntensity(4);

        Mockito.reset(cursor);
        wrapper.peekMz(6);
        verify(cursor).peekMz(6);

        Mockito.reset(cursor);
        wrapper.lastMz();
        verify(cursor).lastMz();

        Mockito.reset(cursor);
        wrapper.lastIntensity();
        verify(cursor).lastIntensity();

        Mockito.reset(cursor);
        wrapper.mz(2);
        verify(cursor).mz(2);

        Mockito.reset(cursor);
        wrapper.intensity(7);
        verify(cursor).intensity(7);

        Mockito.reset(cursor);
        wrapper.moveToClosest(4321.123);
        verify(cursor).moveToClosest(4321.123);

        Mockito.reset(cursor);
        wrapper.moveBefore(1234.321);
        verify(cursor).moveBefore(1234.321);

        Mockito.reset(cursor);
        wrapper.movePast(5678.123);
        verify(cursor).movePast(5678.123);

        Mockito.reset(cursor);
        wrapper.getClosestIndex(3521.734);
        verify(cursor).getClosestIndex(3521.734);
    }

    private static class MockPeakList extends AbstractPeakList<MockPeakAnnotation>
    {
        private MockPeakList(int size)
        {
            this.size = size;
        }

        @Override
        protected int doInsert(double mz, double intensity)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        protected PeakSink<MockPeakAnnotation> newMergeSink()
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        protected int doAppend(double mz, double intensity)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double getX(int index)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double getY(int index)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public void setYAt(double y, int index)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public int getMostIntenseIndex(double minX, double maxX)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public int getMostIntenseIndex()
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getXs()
        {
            return new double[0];
        }

        @Override
        public double[] getXs(double[] dest)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getXs(double[] dest, int destPos)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getXs(int srcPos, double[] dest, int destPos, int length)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }


        @Override
        public double[] getYs()
        {
            return new double[0];
        }

        @Override
        public double[] getYs(double[] dest)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getYs(double[] dest, int destPos)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public double[] getYs(int srcPos, double[] dest, int destPos, int length)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public void trimToSize()
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public void ensureCapacity(int minCapacity)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public Precision getPrecision()
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public MockPeakList copy(PeakProcessor<MockPeakAnnotation, MockPeakAnnotation> peakProcessor)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public MockPeakList copy(PeakProcessorChain<MockPeakAnnotation> peakProcessorChain)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public PeakList<MockPeakAnnotation> copy(SpectrumProcessor<MockPeakAnnotation, MockPeakAnnotation> spectrumProcessor)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

        @Override
        public int indexOf(int fromIndex, int toIndex, double xKey)
        {
            throw new UnsupportedOperationException("Mock class does not support");
        }

    }
}