package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.MockPeakAnnotation;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakList;
import omics.util.protein.ms.Ion;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AbstractPeakProcessorTest
{
    @Test
    void testInPlace()
    {
        MockPeakProcessor processor = new MockPeakProcessor();

        PeakList<MockPeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(356.7385, 98);
        peakList.add(443.9525, 12);

        peakList.apply(processor);

        assertSame(peakList, peakList);
        assertTrue(processor.startCalled, "Start was not called");
        assertEquals(2, processor.size);
        assertTrue(processor.endCalled, "End was not called");
    }

    @Test
    void testCopyAnnotation()
    {
        MockPeakProcessor processor = new MockPeakProcessor();

        MockPeakAnnotation annotation1 = new MockPeakAnnotation(Ion.b, 1, 3);
        MockPeakAnnotation annotation2 = new MockPeakAnnotation(Ion.y, 1, 2);

        PeakList<MockPeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(356.7385, 98, annotation1);
        peakList.add(443.9525, 12, annotation2);

        Peak precursor = peakList.getPrecursor();
        precursor.setMzAndCharge(1763.2154, 1);
        precursor.setIntensity(55);

        peakList.apply(processor);

        assertTrue(processor.startCalled, "Start was not called");
        assertEquals(2, processor.size);
        assertTrue(processor.endCalled, "End was not called");

        assertEquals(annotation1, peakList.getFirstAnnotation(0));
        assertNotSame(annotation1, peakList.getFirstAnnotation(0));

        assertEquals(annotation2, peakList.getFirstAnnotation(1));
        assertNotSame(annotation1, peakList.getFirstAnnotation(1));

        precursor = peakList.getPrecursor();
        assertEquals(1763.2154, precursor.getMz(), 0.0000001);
        assertEquals(1, precursor.getCharge());
        assertEquals(55, precursor.getIntensity(), 0.0000001);
    }

    private class MockPeakProcessor extends AbstractPeakProcessor<MockPeakAnnotation, MockPeakAnnotation>
    {
        int size = -1;
        boolean startCalled = false;
        boolean endCalled = false;

        @Override
        public void start(int size)
        {
            this.size = size;
            startCalled = true;

            sink.start(size);
        }

        @Override
        public void end()
        {
            endCalled = true;
            sink.end();
        }

        @Override
        public void processPeak(double mz, double intensity, List<MockPeakAnnotation> annotations)
        {
            sink.processPeak(mz, intensity, annotations);
        }
    }
}