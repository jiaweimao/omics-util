package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakCursor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2018, 4:09 PM
 */
class ArrayCursorTest
{

    @Test
    void test()
    {
        double[] mzs = {101.0909, 202.1079, 203.1045, 244.128, 254.1056, 255.191, 270.2388, 272.2092, 273.1984,
                283.1682, 284.1255, 295.1564, 300.1432, 301.1701, 302.1986, 312.1813, 313.166, 313.3103, 314.1407,
                330.185};
        double[] intensities = {15.4762, 21.4762, 5.3333, 14.381, 3.4286, 7.2381, 2.1905, 27.8095, 10.3333, 8.381,
                4.381, 4.2857, 3.1905, 15.381, 3.3333, 7.0, 9.381, 3.2857, 7.0476, 17.5714};

        runTest(new ArrayCursor<>(mzs, intensities, mzs.length));
    }

    private void runTest(ArrayCursor<PeakAnnotation> cursor)
    {
        double mzDelta = 0.001;
        double intensityDelta = 0.001;

        assertEquals(330.185, cursor.lastMz(), mzDelta);
        assertEquals(17.5714, cursor.lastIntensity(), intensityDelta);

        assertEquals(20, cursor.size());

        assertTrue(cursor.next());
        assertEquals(101.0909, cursor.currMz(), mzDelta);
        assertEquals(15.4762, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(202.1079, cursor.currMz(), mzDelta);
        assertEquals(21.4762, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(203.1045, cursor.currMz(), mzDelta);
        assertEquals(5.3333, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(244.128, cursor.currMz(), mzDelta);
        assertEquals(14.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(254.1056, cursor.currMz(), mzDelta);
        assertEquals(3.4286, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(255.191, cursor.currMz(), mzDelta);
        assertEquals(7.2381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(270.2388, cursor.currMz(), mzDelta);
        assertEquals(2.1905, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(272.2092, cursor.currMz(), mzDelta);
        assertEquals(27.8095, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(273.1984, cursor.currMz(), mzDelta);
        assertEquals(10.3333, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(283.1682, cursor.currMz(), mzDelta);
        assertEquals(8.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(284.1255, cursor.currMz(), mzDelta);
        assertEquals(4.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(295.1564, cursor.currMz(), mzDelta);
        assertEquals(4.2857, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(300.1432, cursor.currMz(), mzDelta);
        assertEquals(3.1905, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(301.1701, cursor.currMz(), mzDelta);
        assertEquals(15.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(302.1986, cursor.currMz(), mzDelta);
        assertEquals(3.3333, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(312.1813, cursor.currMz(), mzDelta);
        assertEquals(7.0, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(313.166, cursor.currMz(), mzDelta);
        assertEquals(9.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(313.3103, cursor.currMz(), mzDelta);
        assertEquals(3.2857, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(314.1407, cursor.currMz(), mzDelta);
        assertEquals(7.0476, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.next());
        assertEquals(330.185, cursor.currMz(), mzDelta);
        assertEquals(17.5714, cursor.currIntensity(), intensityDelta);
        assertFalse(cursor.next());

        assertTrue(cursor.previous());
        assertEquals(314.1407, cursor.currMz(), mzDelta);
        assertEquals(7.0476, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(313.3103, cursor.currMz(), mzDelta);
        assertEquals(3.2857, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(313.166, cursor.currMz(), mzDelta);
        assertEquals(9.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(312.1813, cursor.currMz(), mzDelta);
        assertEquals(7.0, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(302.1986, cursor.currMz(), mzDelta);
        assertEquals(3.3333, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(301.1701, cursor.currMz(), mzDelta);
        assertEquals(15.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(300.1432, cursor.currMz(), mzDelta);
        assertEquals(3.1905, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(295.1564, cursor.currMz(), mzDelta);
        assertEquals(4.2857, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(284.1255, cursor.currMz(), mzDelta);
        assertEquals(4.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(283.1682, cursor.currMz(), mzDelta);
        assertEquals(8.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(273.1984, cursor.currMz(), mzDelta);
        assertEquals(10.3333, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(272.2092, cursor.currMz(), mzDelta);
        assertEquals(27.8095, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(270.2388, cursor.currMz(), mzDelta);
        assertEquals(2.1905, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(255.191, cursor.currMz(), mzDelta);
        assertEquals(7.2381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(254.1056, cursor.currMz(), mzDelta);
        assertEquals(3.4286, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(244.128, cursor.currMz(), mzDelta);
        assertEquals(14.381, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(203.1045, cursor.currMz(), mzDelta);
        assertEquals(5.3333, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(202.1079, cursor.currMz(), mzDelta);
        assertEquals(21.4762, cursor.currIntensity(), intensityDelta);
        assertTrue(cursor.previous());
        assertEquals(101.0909, cursor.currMz(), mzDelta);
        assertEquals(15.4762, cursor.currIntensity(), intensityDelta);
    }

    @Test
    public void testMoveTo()
    {
        final double[] mzs = new double[]{10.0, 10.3};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<>(mzs, new double[]{1, 1}, mzs.length);

        vector.moveToClosest(10.1);
        assertEquals(10.0, vector.currMz(), 0.000001);

        vector.moveToClosest(10.2);
        assertEquals(10.3, vector.currMz(), 0.000001);

        vector.moveToClosest(100);
        assertEquals(10.3, vector.currMz(), 0.000001);
    }

    @Test
    void testNext()
    {
        final double[] mzs = new double[]{12.5, 78.6, 158.9};
        final double[] ins = new double[]{100, 200, 300};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<>(mzs, ins, mzs.length);

        vector.resetCursor();

        assertTrue(vector.next());
        assertEquals(12.5, vector.currMz(), 0.000001);
        assertEquals(100.0, vector.currIntensity(), 0.000001);
        assertTrue(vector.next());
        assertEquals(78.6, vector.currMz(), 0.000001);
        assertEquals(200.0, vector.currIntensity(), 0.000001);
        assertTrue(vector.next());
        assertEquals(158.9, vector.currMz(), 0.000001);
        assertEquals(300.0, vector.currIntensity(), 0.000001);
        assertFalse(vector.next());
    }

    @Test
    public void testNextWithCentroid()
    {
        final double[] mzs = new double[]{12.5, 78.6, 158.9, 241.68};
        final double[] ins = new double[]{100, 200, 300, 400};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<>(mzs, ins, mzs.length);

        vector.resetCursor();

        // under on first curr was ? is now = 12.5
        assertTrue(vector.next(2));
        assertEquals(12.5, vector.currMz(), 0.000001);
        assertEquals(100.0, vector.currIntensity(), 0.000001);

        // under curr was 12.5 is now 78.6
        assertTrue(vector.next(58.59));
        assertEquals(78.6, vector.currMz(), 0.000001);
        assertEquals(200.0, vector.currIntensity(), 0.000001);

        // hold by under curr was 78.6 isn now 78.6
        assertTrue(vector.next(60.9));
        assertEquals(78.6, vector.currMz(), 0.000001);
        assertEquals(200.0, vector.currIntensity(), 0.000001);

        // exact curr was 78.6 is now 158.9
        assertTrue(vector.next(78.6));
        assertEquals(158.9, vector.currMz(), 0.000001);
        assertEquals(300.0, vector.currIntensity(), 0.000001);

        // over curr was 158.9 is now 241.68
        assertTrue(vector.next(500.91));
        assertEquals(241.68, vector.currMz(), 0.000001);
        assertEquals(400.0, vector.currIntensity(), 0.000001);

        assertFalse(vector.next());
    }

    @Test
    void testEmpty()
    {
        final double[] mzs = new double[0];
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<>(mzs, new double[0], mzs.length);

        vector.resetCursor();
        assertFalse(vector.next());
        assertFalse(vector.next(10));
    }


    @Test
    void testMovePast()
    {
        final double[] mzs = new double[]{12.5, 52.5, 78.6, 158.9};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<>(mzs, new double[]{100, 200, 300, 400},
                mzs.length);

        vector.resetCursor();

        vector.movePast(54);
        assertEquals(78.6, vector.currMz(), eps);
        vector.movePast(12.5);
        assertEquals(52.5, vector.currMz(), eps);
        vector.movePast(179);
        assertEquals(158.9, vector.currMz(), eps);
    }

    private double eps = 0.000001;

    @Test
    void testMoveBefore()
    {
        final double[] mzs = new double[]{12.5, 52.5, 78.6, 158.9};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<>(mzs, new double[]{100, 200, 300, 400}, mzs.length);

        vector.resetCursor();

        vector.moveBefore(54);
        assertEquals(52.5, vector.currMz(), eps);

        vector.moveBefore(179);
        assertEquals(158.9, vector.currMz(), eps);

        vector.moveBefore(78);
        assertEquals(52.5, vector.currMz(), eps);

        vector.moveBefore(12.5);

        vector.next();
        assertEquals(12.5, vector.currMz(), eps);
    }

    @Test
    void testGetMzAtIndexOutOfBound()
    {
        final double[] mzs = new double[]{12.5, 78.6, 158.9};
        PeakCursor<PeakAnnotation> vector = new ArrayCursor<>(mzs, new double[]{100, 200, 300}, mzs.length);

        assertThrows(IndexOutOfBoundsException.class, vector::currMz);
    }
}