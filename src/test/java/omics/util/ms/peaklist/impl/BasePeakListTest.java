package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.*;
import omics.util.ms.peaklist.transform.IdentityPeakProcessor;
import omics.util.ms.peaklist.transform.SqrtTransformer;
import omics.util.protein.ms.Ion;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public abstract class BasePeakListTest
{
    protected final double deltaMz;
    protected final Precision precision;
    private final double deltaIntensity;
    private final boolean checkTotalIonCurrent;

    protected BasePeakListTest(Precision precision)
    {
        switch (precision) {

            case DOUBLE:
            case DOUBLE_FLOAT:
            case DOUBLE_CONSTANT:
                this.deltaMz = 0.00000000001;
                break;
            case FLOAT:
            case FLOAT_CONSTANT:
                this.deltaMz = 0.0001;
                break;
            default:
                throw new IllegalArgumentException("Cannot set delta mz for " + precision);
        }

        switch (precision) {

            case DOUBLE:
                this.deltaIntensity = 0.000000000001;
                checkTotalIonCurrent = true;
                break;
            case DOUBLE_FLOAT:
            case FLOAT:
                this.deltaIntensity = 0.1;
                checkTotalIonCurrent = true;
                break;
            case DOUBLE_CONSTANT:
            case FLOAT_CONSTANT:
                this.deltaIntensity = 100000000;
                checkTotalIonCurrent = false;
                break;
            default:
                throw new IllegalArgumentException("Cannot set delta mz for " + precision);
        }

        this.precision = precision;
    }

    protected abstract <A extends PeakAnnotation> int getIntensityArrayLength(PeakList<A> peakList) throws
            NoSuchFieldException, IllegalAccessException;

    protected abstract <A extends PeakAnnotation> int getMzArrayLength(PeakList<A> peakList) throws
            NoSuchFieldException, IllegalAccessException;

    protected abstract void checkPeakList(double[] expectedMzs, double[] expectedIntensities, PeakList<? extends
            PeakAnnotation> peakList) throws NoSuchFieldException, IllegalAccessException;

    @Test
    protected abstract void testInsert() throws Exception;

    protected <A extends PeakAnnotation> void runTestInsert(PeakList<A> peakList) throws NoSuchFieldException,
            IllegalAccessException
    {

        int index;
        //Setup
        index = peakList.add(10, 1);
        assertEquals(0, index);
        index = peakList.add(15, 3);
        assertEquals(1, index);

        //Test insert in middle
        index = peakList.add(11, 2);
        assertEquals(1, index);
        checkPeakList(new double[]{10, 11, 15}, new double[]{1, 2, 3}, peakList);
        if (checkTotalIonCurrent) assertEquals(6, peakList.getTotalIonCurrent(), 0.0000001);

        //Test insert duplicate m/z
        index = peakList.add(11, 2);
        assertEquals(1, index);
        checkPeakList(new double[]{10, 11, 15}, new double[]{1, 4, 3}, peakList);
        if (checkTotalIonCurrent) assertEquals(8, peakList.getTotalIonCurrent(), 0.0000001);

        //Test insert at start
        index = peakList.add(9.99, 8);
        assertEquals(0, index);
        checkPeakList(new double[]{9.99, 10, 11, 15}, new double[]{8, 1, 4, 3}, peakList);
        if (checkTotalIonCurrent) assertEquals(16, peakList.getTotalIonCurrent(), 0.0000001);
    }

    @Test
    protected abstract void testAddSame() throws Exception;

    protected <A extends PeakAnnotation> void runTestAddSame(PeakList<A> peakList)
    {

        peakList.add(12.35823, 1);
        peakList.add(12.35823, 1);

        assertEquals(1, peakList.size());
        assertEquals(12.35823, peakList.getX(0), deltaMz);
        assertEquals(2.0, peakList.getY(0), deltaIntensity);
    }

    @Test
    public abstract void testBulkAdd() throws Exception;

    protected <A extends PeakAnnotation> void runTestBulkAdd(PeakList<A> peakList) throws NoSuchFieldException,
            IllegalAccessException
    {

        peakList = build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        peakList.trimToSize();

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        peakList.addSorted(
                new double[]{201, 202, 203, 204, 205, 206},
                new double[]{9, 8, 7, 6, 5, 4}, 3);

        checkPeakList(new double[]{87.6f, 98.65f, 123.54f, 169.54f, 201, 202, 203}, new double[]{1, 1, 1, 1, 9, 8,
                7}, peakList);
    }

    <A extends PeakAnnotation> PeakList<A> build(PeakList<A> peakList, double[] mzs)
    {
        for (double mz : mzs) {

            peakList.add(mz, 1);
        }

        peakList.trimToSize();
        return peakList;
    }

    protected abstract <A extends PeakAnnotation> PeakList<A> newPeakList();

    protected <A extends PeakAnnotation> PeakList<A> build(PeakList<A> peakList, double[] mzs, double[] intensities)
    {

        assertEquals(mzs.length, intensities.length);
        peakList.ensureCapacity(mzs.length);

        for (int i = 0; i < mzs.length; i++) {
            peakList.add(mzs[i], intensities[i]);
        }

        peakList.trimToSize();
        return peakList;
    }

    <A extends PeakAnnotation> void build(PeakList<A> peakList, double[] mzs, double[] intensities, Map<Integer, A>
            annotationMap)
    {
        assertEquals(mzs.length, intensities.length);
        peakList.ensureCapacity(mzs.length);

        Set<Integer> annotSites = annotationMap.keySet();

        for (int i = 0; i < mzs.length; i++) {
            if (annotSites.contains(i)) {
                peakList.add(mzs[i], intensities[i], annotationMap.get(i));
            } else {
                peakList.add(mzs[i], intensities[i]);
            }
        }

        peakList.trimToSize();
    }

    @Test
    public abstract void testBulkAdd2() throws Exception;

    protected <A extends PeakAnnotation> void runTestBulkAdd2(PeakList<A> peakList) throws NoSuchFieldException,
            IllegalAccessException
    {
        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        peakList.trimToSize();

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        peakList.addSorted(
                new double[]{1, 2, 3, 4, 5, 6},
                new double[]{9, 8, 7, 6, 5, 4}, 0);

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        checkPeakList(new double[]{87.6f, 98.65f, 123.54f, 169.54f}, new double[]{1, 1, 1, 1}, peakList);
    }

    @Test
    public abstract void testBulkAdd3() throws Exception;

    protected <A extends PeakAnnotation> void runTestBulkAdd3(PeakList<A> peakList) throws NoSuchFieldException,
            IllegalAccessException
    {

        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        peakList.trimToSize();

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        peakList.addSorted(
                new double[]{201, 202, 203, 204, 205, 206},
                new double[]{9, 8, 7, 6, 5, 4}, 6);

        assertEquals(10, peakList.size());

        assertEquals(10, getMzArrayLength(peakList));
        assertEquals(10, getIntensityArrayLength(peakList));

        checkPeakList(
                new double[]{87.6f, 98.65f, 123.54f, 169.54f, 201f, 202f, 203f, 204f, 205f, 206f},
                new double[]{1, 1, 1, 1, 9, 8, 7, 6, 5, 4},
                peakList);
    }

    @Test
    public abstract void testBulkAddException() throws Exception;

    protected <A extends PeakAnnotation> void runTestAddException(PeakList<A> peakList) throws NoSuchFieldException,
            IllegalAccessException
    {

        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        peakList.trimToSize();

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        IndexOutOfBoundsException exception = null;
        try {
            peakList.addSorted(
                    new double[]{1, 2, 3, 4, 5, 6},
                    new double[]{9, 8, 7, 6, 5, 4}, -3);
        } catch (IndexOutOfBoundsException e) {

            exception = e;
        }
        assertNotNull(exception);

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        checkPeakList(
                new double[]{87.6f, 98.65f, 123.54f, 169.54f},
                new double[]{1, 1, 1, 1},
                peakList);
    }

    @Test
    public abstract void testBulkAddException2() throws Exception;

    protected <A extends PeakAnnotation> void runTestBulkAddException2(PeakList<A> peakList) throws
            NoSuchFieldException, IllegalAccessException
    {

        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        peakList.trimToSize();

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        IndexOutOfBoundsException exception = null;
        try {
            peakList.addSorted(
                    new double[]{1, 2, 3, 4, 5, 6},
                    new double[]{9, 8, 7, 6, 5, 4}, 25);
        } catch (IndexOutOfBoundsException e) {

            exception = e;
        }
        assertNotNull(exception);

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        checkPeakList(
                new double[]{87.6f, 98.65f, 123.54f, 169.54f},
                new double[]{1, 1, 1, 1},
                peakList);
    }

    @Test
    public abstract void testBulkAddException3() throws Exception;

    protected <A extends PeakAnnotation> void runTestBulkAddException3(PeakList<A> peakList) throws
            NoSuchFieldException, IllegalAccessException
    {

        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        peakList.trimToSize();

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        IndexOutOfBoundsException exception = null;
        try {
            peakList.addSorted(
                    new double[]{1, 2, 3, 4, 5, 6},
                    new double[]{9, 8, 7}, 6);
        } catch (IndexOutOfBoundsException e) {

            exception = e;
        }
        assertNotNull(exception);

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        checkPeakList(
                new double[]{87.6f, 98.65f, 123.54f, 169.54f},
                new double[]{1, 1, 1, 1},
                peakList);
    }

    @Test
    public abstract void testBulkAddException4() throws Exception;

    protected <A extends PeakAnnotation> void runTestBulkAddException4(PeakList<A> peakList) throws
            NoSuchFieldException, IllegalAccessException
    {

        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        peakList.trimToSize();

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        IndexOutOfBoundsException exception = null;
        try {
            peakList.addSorted(
                    new double[]{1, 2, 3},
                    new double[]{9, 8, 7, 6, 5, 4}, 6);

        } catch (IndexOutOfBoundsException e) {

            exception = e;
        }
        assertNotNull(exception);

        assertEquals(4, peakList.size());

        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        checkPeakList(
                new double[]{87.6f, 98.65f, 123.54f, 169.54f},
                new double[]{1, 1, 1, 1},
                peakList);
    }

    @Test
    public abstract void testClear() throws Exception;

    protected <A extends PeakAnnotation> void runTestClear(PeakList<A> peakList) throws NoSuchFieldException,
            IllegalAccessException
    {

        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});

        assertEquals(4, peakList.size());
        assertEquals(4, getMzArrayLength(peakList));
        assertEquals(4, getIntensityArrayLength(peakList));

        peakList.clear();
        assertEquals(0, getMzArrayLength(peakList));
        assertEquals(0, getIntensityArrayLength(peakList));
    }

    @Test
    public abstract void testCopyMzs();

    protected <A extends PeakAnnotation> void runTestCopyMzs(PeakList<A> peakList)
    {
        peakList.add(106.05, 1000);
        peakList.add(177.09, 10);
        peakList.add(233.06, 100);
        peakList.add(290.17, 10);
        peakList.add(389.16, 1000);
        peakList.trimToSize();

        double[] copy = peakList.getXs(2, new double[3], 0, 3);

        assertArrayEquals(new double[]{233.06, 290.17, 389.16}, copy, deltaMz);
    }

    @Test
    public abstract void testEmptyPeakListgetIntensityValues() throws Exception;

    public <A extends PeakAnnotation> void runTestEmptyPeakListgetIntensityValues(PeakList<A> peakList)
    {
        build(peakList, new double[0]);

        assertEquals(0, peakList.getYs().length);
    }

    @Test
    public abstract void testEmptyPeakListGetMz() throws Exception;

    protected <A extends PeakAnnotation> void runTestEmptyPeakListGetMz(PeakList<A> peakList)
    {
        build(peakList, new double[0]);

        assertEquals(0.0, peakList.getX(0), deltaMz);
    }

    @Test
    public abstract void testEmptyPeakListGetIntensityAt() throws Exception;

    protected <A extends PeakAnnotation> void runTestEmptyPeakListGetIntensityAt(PeakList<A> peakList)
    {
        build(peakList, new double[0]);

        assertEquals(0.0, peakList.getY(0), deltaIntensity);
    }

    @Test
    public abstract void testEmptyPeakListgetMzValues() throws Exception;

    protected <A extends PeakAnnotation> void runTestEmptyPeakListgetMzValues(PeakList<A> peakList)
    {
        build(peakList, new double[0]);

        assertEquals(0, peakList.getXs().length);
    }

    @Test
    public abstract void testEnsureCapacity() throws NoSuchFieldException, IllegalAccessException;

    protected <A extends PeakAnnotation> void runTestEnsureCapacity(PeakList<A> peakList) throws
            NoSuchFieldException, IllegalAccessException
    {

        peakList.ensureCapacity(200);

        assertEquals(200, getMzArrayLength(peakList));
    }

    @Test
    public abstract void testEquals() throws Exception;

    protected <A extends PeakAnnotation> void runTestEquals(PeakList<A> peakList1, PeakList<A> peakList2)
    {

        build(peakList1, new double[]{4, 5, 6}, new double[]{1, 1, 1});
        build(peakList2, new double[]{4, 5, 6}, new double[]{2, 2, 2});

        assertNotEquals(peakList1, peakList2);
    }

    @Test
    public abstract void testgetIntensityValues() throws Exception;

    public <A extends PeakAnnotation> void runTestgetIntensityValues(PeakList<A> peakList)
    {

        build(peakList, new double[]{56, 87, 125, 986});

        double[] intensities = peakList.getYs();
        double[] expectedIntensities = new double[]{1, 1, 1, 1};
        assertArrayEquals(expectedIntensities, intensities, deltaIntensity);
    }

    @Test
    public abstract void testgetIntensityValues2() throws Exception;

    protected <A extends PeakAnnotation> void runTestgetIntensityValues2(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986});

        double[] intensities = peakList.getYs(new double[4]);
        double[] expectedIntensities = new double[]{1, 1, 1, 1};
        assertArrayEquals(expectedIntensities, intensities, deltaIntensity);
    }

    @Test
    public abstract void testgetIntensityValues3() throws Exception;

    protected <A extends PeakAnnotation> void runTestgetIntensityValues3(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{100, 202, 354, 1});

        double[] dest = new double[]{-1, -1, -1, -1};
        double[] intensities = peakList.getYs(1, dest, 2, 2);
        double[] expectedIntensities = new double[]{-1, -1, 202, 354};
        assertArrayEquals(expectedIntensities, intensities, deltaIntensity);
    }

    @Test
    public abstract void testgetIntensityValues4() throws Exception;

    protected <A extends PeakAnnotation> void runTestgetIntensityValues4(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{100, 202, 354, 1});

        double[] intensities = peakList.getYs(1, null, 2, 2);
        double[] expectedIntensities = new double[]{0, 0, 202, 354};
        assertArrayEquals(expectedIntensities, intensities, deltaIntensity);
    }

    @Test
    public abstract void testgetIntensityValues5() throws Exception;

    protected <A extends PeakAnnotation> void runTestgetIntensityValues5(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{100, 202, 354, 1});

        double[] intensities = peakList.getYs(null, 3);
        double[] expectedIntensities = new double[]{0, 0, 0, 100, 202, 354, 1};
        assertArrayEquals(expectedIntensities, intensities, deltaIntensity);
    }

    @Test
    public abstract void testgetMzValues1() throws Exception;

    protected <A extends PeakAnnotation> void runTestgetMzValues1(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{100, 202, 354, 1});

        double[] dest = new double[]{-1, -1, -1, -1};
        double[] mzs = peakList.getXs(1, dest, 2, 2);
        double[] expectedMzs = new double[]{-1, -1, 87, 125};
        assertArrayEquals(expectedMzs, mzs, deltaMz);
    }

    @Test
    public abstract void testgetMzValues2() throws Exception;

    protected <A extends PeakAnnotation> void runTestgetMzValues2(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{100, 202, 354, 1});

        double[] mzs = peakList.getXs(1, null, 3, 2);
        double[] expectedMzs = new double[]{0, 0, 0, 87, 125};
        assertArrayEquals(expectedMzs, mzs, deltaMz);
    }

    @Test
    public abstract void testgetIntensityValuesArr() throws Exception;

    public <A extends PeakAnnotation> void runTestgetIntensityValuesArr(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{1, 2, 3, 8});

        double[] actualMzs = peakList.getYs(new double[4]);
        assertArrayEquals(new double[]{1, 2, 3, 8}, actualMzs, deltaIntensity);
    }

    @Test
    public abstract void testGetIntensityAt() throws Exception;

    public <A extends PeakAnnotation> void runTestGetIntensityAt(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{1, 2, 3, 4});

        double intensity = peakList.getY(2);

        assertEquals(3.0, intensity, deltaIntensity);
    }

    @Test
    public abstract void testGetMostIntenseIndex();

    protected <A extends PeakAnnotation> void runTestGetMostIntenseIndex(PeakList<A> peakList)
    {
        if (!checkTotalIonCurrent) return;

        peakList.add(106.05, 1000);
        peakList.add(177.09, 10);
        peakList.add(233.06, 100);
        peakList.add(290.17, 10);
        peakList.add(389.16, 1000);
        peakList.trimToSize();

        assertEquals(2, peakList.getMostIntenseIndex(177.09, 300.17));
        assertEquals(2, peakList.getMostIntenseIndex(160.09, 300.17));
        assertEquals(4, peakList.getMostIntenseIndex(177.09, 389.16));
        assertEquals(0, peakList.getMostIntenseIndex(106.05, 210.39));
        assertEquals(-1, peakList.getMostIntenseIndex(390, 987.64));
    }

    @Test
    public abstract void testGetMostIntenseIndex2();

    protected <A extends PeakAnnotation> void runTestGetMostIntenseIndex2(PeakList<A> peakList)
    {
        peakList.trimToSize();

        assertEquals(-1, peakList.getMostIntenseIndex(177.09, 300.17));
    }

    @Test
    public abstract void testGetMostIntenseIndex3();

    protected <A extends PeakAnnotation> void runTestGetMostIntenseIndex3(PeakList<A> peakList)
    {
        peakList.add(106.05, 1000);
        peakList.add(177.09, 10);
        peakList.add(233.06, 100);
        peakList.add(290.17, 10);
        peakList.add(389.16, 1000);
        peakList.trimToSize();

        assertEquals(-1, peakList.getMostIntenseIndex(458.09, 500.17));
    }

    @Test
    public abstract void testGetMostIntenseIndex4();

    protected <A extends PeakAnnotation> void runTestGetMostIntenseIndex4(PeakList<A> peakList)
    {
        peakList.add(106.05, 1000);
        peakList.add(177.09, 10);
        peakList.add(233.06, 100);
        peakList.add(290.17, 10);
        peakList.add(389.16, 1000);
        peakList.trimToSize();

        assertEquals(2, peakList.getMostIntenseIndex(233.06, 300.0));
    }

    @Test
    public abstract void testGetMostIntenseIndex5();

    protected <A extends PeakAnnotation> void runTestGetMostIntenseIndex5(PeakList<A> peakList)
    {
        if (!checkTotalIonCurrent) return;

        peakList.add(106.05, 1000);
        peakList.add(177.09, 10);
        peakList.add(233.06, 100);
        peakList.add(290.17, 10);
        peakList.add(389.16, 1000);
        peakList.trimToSize();

        assertEquals(4, peakList.getMostIntenseIndex(233.06, 389.16));
    }

    @Test
    public void testGetMostIntenseIndex6()
    {
        if (!checkTotalIonCurrent) return;

        PeakList<PeakAnnotation> peakList = newPeakList();
        peakList.trimToSize();

        assertEquals(-1, peakList.getMostIntenseIndex(177.09, 300.17));
        assertEquals(-1, peakList.getMostIntenseIndex(160.09, 300.17));
        assertEquals(-1, peakList.getMostIntenseIndex(177.09, 389.16));
        assertEquals(-1, peakList.getMostIntenseIndex(106.05, 210.39));
        assertEquals(-1, peakList.getMostIntenseIndex(390, 987.64));
    }

    @Test
    public void testGetMostIntenseIndex7()
    {
        if (!checkTotalIonCurrent) return;

        PeakList<PeakAnnotation> peakList = newPeakList();
        peakList.add(100, 10);
        peakList.trimToSize();

        assertEquals(0, peakList.getMostIntenseIndex(90, 300.17));
    }

    @Test
    public void testGetMostIntenseIndex8()
    {
        if (!checkTotalIonCurrent) return;

        PeakList<PeakAnnotation> peakList = newPeakList();
        peakList.add(100, 10);
        peakList.trimToSize();

        assertEquals(-1, peakList.getMostIntenseIndex(100.1, 300.17));
    }

    @Test
    public abstract void testgetMzValues3() throws Exception;

    protected <A extends PeakAnnotation> void runTestgetMzValues3(PeakList<A> peakList)
    {
        build(peakList, new double[]{56, 87, 125, 986}, new double[]{100, 202, 354, 1});

        double[] mzs = peakList.getXs(null, 1);
        double[] expectedMzs = new double[]{0, 56, 87, 125, 986};
        assertArrayEquals(expectedMzs, mzs, deltaMz);
    }

    @Test
    public abstract void testHashCode() throws Exception;

    protected <A extends PeakAnnotation> void runTestHashCode(PeakList<A> peakList1, PeakList<A> peakList2)
    {
        build(peakList1, new double[]{4, 5, 6}, new double[]{1, 1, 1});
        build(peakList2, new double[]{4, 5, 6}, new double[]{2, 2, 2});

        assertTrue(peakList1.hashCode() != peakList2.hashCode());
    }

    @Test
    public abstract void testPrecision() throws Exception;

    protected <A extends PeakAnnotation> void runTestPrecision(PeakList<A> peakList, Precision precision)
    {
        build(peakList, new double[]{87.6f, 98.65f, 123.54f, 169.54f});
        assertEquals(precision, peakList.getPrecision());
    }

    @Test
    public abstract void testSetIntensity();

    protected <A extends PeakAnnotation> void runTestSetIntensity(PeakList<A> peakList)
    {
        peakList.add(106.05, 1000);
        peakList.add(177.09, 10);
        peakList.add(233.06, 100);
        peakList.add(290.17, 10);
        peakList.add(389.16, 1000);
        peakList.trimToSize();

        if (checkTotalIonCurrent) assertEquals(2120.0, peakList.getTotalIonCurrent(), 0.00001);
        peakList.setYAt(20, 1);

        if (checkTotalIonCurrent) assertEquals(2130.0, peakList.getTotalIonCurrent(), 0.00001);
    }

    @Test
    public abstract void testSetLoadFactor() throws NoSuchFieldException, IllegalAccessException;

    protected <A extends PeakAnnotation> void runTestSetLoadFactor(AbstractPeakList<A> peakList) throws
            NoSuchFieldException, IllegalAccessException
    {

        peakList.setLoadFactor(20);
        peakList.add(12.89, 785.3);

        assertEquals(20, getMzArrayLength(peakList));
        assertEquals(20, peakList.getLoadFactor());
    }

    @Test
    public abstract void testTotalIonCurrent();

    protected <A extends PeakAnnotation> void runTestTotalIonCurrent(PeakList<A> peakList)
    {

        build(peakList, new double[]{58.3, 87.6, 894.6}, new double[]{1, 2, 3});

        assertEquals(6.0, peakList.getTotalIonCurrent(), 0.00001);
    }

    @Test
    public abstract void testValueOf();

    protected void runTestValueOf(Precision precision)
    {
        PeakList<PeakAnnotation> pl =
                AbstractPeakList.valueOf("120.07913 Da, " +
                        "147.14932 Da, " +
                        "158.09187 Da, " +
                        "165.10173 Da", precision);

        assertEquals(4, pl.size());

        assertEquals(120.07913f, (float) pl.getX(0), deltaMz);
        assertEquals(147.14932f, (float) pl.getX(1), deltaMz);
        assertEquals(158.09187f, (float) pl.getX(2), deltaMz);
        assertEquals(165.10173f, (float) pl.getX(3), deltaMz);
    }

    @Test
    public abstract void testValueOfWithIntensities();

    protected void runTestValueOfWithIntensities(Precision precision)
    {
        PeakList<PeakAnnotation> pl = AbstractPeakList
                .valueOf("120.07913 Da (85002.6)," +
                        "147.14932 Da (20311.2)," +
                        "158.09187 Da (33321.5), " +
                        "165.10173 Da (487886.5), " +
                        "166.10524 Da (26983.5), " +
                        "175.11842 Da (428709.2), " +
                        "189.95845 Da (19962.7), " +
                        "191.11719 Da (33407.9), " +
                        "194.08276 Da (24121.1), " +
                        "214.08656 Da (55177.2), " +
                        "219.11382 Da (25720.4), " +
                        "231.11353 Da (44814.6), " +
                        "242.08058 Da (456825.2), " +
                        "243.08276 Da (46099.4), " +
                        "243.10849 Da (52023.8), " +
                        "248.13817 Da (37212.6)", precision);

        assertEquals(16, pl.size());
//        for(int i = 0; i< pl.size(); i++){
//            System.out.println(pl.getMz(i)+","+pl.getIntensity(i));
//        }

        assertEquals(120.07913, pl.getX(0), deltaMz);
        assertEquals(85002.6, pl.getY(0), deltaIntensity);
        assertEquals(147.14932, pl.getX(1), deltaMz);
        assertEquals(20311.2, pl.getY(1), deltaIntensity);
        assertEquals(158.09187, pl.getX(2), deltaMz);
        assertEquals(33321.5, pl.getY(2), deltaIntensity);
        assertEquals(165.10173, pl.getX(3), deltaMz);
        assertEquals(487886.5, pl.getY(3), deltaIntensity);
        assertEquals(166.10524, pl.getX(4), deltaMz);
        assertEquals(26983.5, pl.getY(4), deltaIntensity);
        assertEquals(175.11842, pl.getX(5), deltaMz);
        assertEquals(428709.2, pl.getY(5), deltaIntensity);
        assertEquals(189.95845, pl.getX(6), deltaMz);
        assertEquals(19962.7, pl.getY(6), deltaIntensity);
        assertEquals(191.11719, pl.getX(7), deltaMz);
        assertEquals(33407.9, pl.getY(7), deltaIntensity);
        assertEquals(194.08276, pl.getX(8), deltaMz);
        assertEquals(24121.1, pl.getY(8), deltaIntensity);
        assertEquals(214.08656, pl.getX(9), deltaMz);
        assertEquals(55177.2, pl.getY(9), deltaIntensity);
        assertEquals(219.11382, pl.getX(10), deltaMz);
        assertEquals(25720.4, pl.getY(10), deltaIntensity);
        assertEquals(231.11353, pl.getX(11), deltaMz);
        assertEquals(44814.6, pl.getY(11), deltaIntensity);
        assertEquals(242.08058, pl.getX(12), deltaMz);
        assertEquals(456825.2, pl.getY(12), deltaIntensity);
        assertEquals(243.08276, pl.getX(13), deltaMz);
        assertEquals(46099.4, pl.getY(13), deltaIntensity);
        assertEquals(243.10849, pl.getX(14), deltaMz);
        assertEquals(52023.8, pl.getY(14), deltaIntensity);
        assertEquals(248.13817, pl.getX(15), deltaMz);
        assertEquals(37212.6, pl.getY(15), deltaIntensity);
    }

    @Test
    public abstract void testAddPeakWithAnnotation() throws Exception;

    protected void runTestAddPeakWithAnnotation(PeakList<MockPeakAnnotation> peakList)
    {
        MockPeakAnnotation annotation1 = new MockPeakAnnotation(Ion.y, 1, 1);
        MockPeakAnnotation annotation2 = new MockPeakAnnotation(Ion.y, 1, 2);
        MockPeakAnnotation annotation3 = new MockPeakAnnotation(Ion.y, 1, 4);

        peakList.add(123.2, 45, Collections.singletonList(annotation1));

        assertEquals(1, peakList.size());
        assertEquals(123.2, peakList.getX(0), deltaMz);
        assertEquals(45, peakList.getY(0), deltaIntensity);
        assertEquals(Collections.singletonList(annotation1), peakList.getAnnotations(0));

        peakList.add(782.9, 645, Collections.singletonList(annotation3));

        assertEquals(2, peakList.size());
        assertEquals(123.2, peakList.getX(0), deltaMz);
        assertEquals(45, peakList.getY(0), deltaIntensity);
        assertEquals(Collections.singletonList(annotation1), peakList.getAnnotations(0));
        assertEquals(782.9, peakList.getX(1), deltaMz);
        assertEquals(645, peakList.getY(1), deltaIntensity);
        assertEquals(Collections.singletonList(annotation3), peakList.getAnnotations(1));

        peakList.add(349.6, 637, Collections.singletonList(annotation2));

        assertEquals(3, peakList.size());
        assertEquals(123.2, peakList.getX(0), deltaMz);
        assertEquals(45, peakList.getY(0), deltaIntensity);
        assertEquals(Collections.singletonList(annotation1), peakList.getAnnotations(0));
        assertEquals(349.6, peakList.getX(1), deltaMz);
        assertEquals(637, peakList.getY(1), deltaIntensity);
        assertEquals(Collections.singletonList(annotation2), peakList.getAnnotations(1));
        assertEquals(782.9, peakList.getX(2), deltaMz);
        assertEquals(645, peakList.getY(2), deltaIntensity);
        assertEquals(Collections.singletonList(annotation3), peakList.getAnnotations(2));
    }

    @Test
    public void testAddPeakWithAnnotation2() throws NoSuchFieldException, IllegalAccessException
    {
        PeakList<PeakAnnotation> destPeakList = newPeakList();
        PeakList<PeakAnnotation> srcPeakList = newPeakList();

        PeakAnnotation annotation1 = mock(MockPeakAnnotation.class, "1");
        PeakAnnotation annotation3_1 = mock(MockPeakAnnotation.class, "2");
        PeakAnnotation annotation5 = mock(MockPeakAnnotation.class, "3");

        destPeakList.add(1, 1, Collections.singletonList(annotation1));
        destPeakList.add(3, 3, Collections.singletonList(annotation3_1));
        destPeakList.add(5, 5, Collections.singletonList(annotation5));
        destPeakList.add(6, 3);
        destPeakList.trimToSize();

        PeakAnnotation annotation2 = mock(MockPeakAnnotation.class, "2");
        PeakAnnotation annotation3_2 = mock(MockPeakAnnotation.class, "3_2");
        PeakAnnotation annotation4 = mock(MockPeakAnnotation.class, "4");
        PeakAnnotation annotation6 = mock(MockPeakAnnotation.class, "6");
        srcPeakList.add(2, 2, Collections.singletonList(annotation2));
        srcPeakList.add(3, 3, Collections.singletonList(annotation3_2));
        srcPeakList.add(4, 4, Collections.singletonList(annotation4));
        srcPeakList.add(6, 3, annotation6);
        srcPeakList.trimToSize();

        destPeakList.addPeaks(srcPeakList);

        assertEquals(6, destPeakList.size());

        checkPeakList(new double[]{1, 2, 3, 4, 5, 6}, new double[]{1, 2, 6, 4, 5, 6}, destPeakList);

        assertEquals(24, destPeakList.getTotalIonCurrent(), deltaIntensity);

        assertEquals(Collections.singletonList(annotation1), destPeakList.getAnnotations(0));
        assertEquals(Collections.singletonList(annotation2), destPeakList.getAnnotations(1));
        assertEquals(Arrays.asList(annotation3_2, annotation3_1), destPeakList.getAnnotations(2));
        assertEquals(Collections.singletonList(annotation4), destPeakList.getAnnotations(3));
        assertEquals(Collections.singletonList(annotation5), destPeakList.getAnnotations(4));
        assertEquals(Collections.singletonList(annotation6), destPeakList.getAnnotations(5));

        //Check that the array from the dest is copied
        srcPeakList.addAnnotation(0, mock(PeakAnnotation.class, "new getAnnotation"));
        assertEquals(Collections.singletonList(annotation2), destPeakList.getAnnotations(1));
    }

    @Test
    public abstract void testGetMostIntenseIndexWholeSpectra() throws Exception;

    protected void runGetMostIntenseIndexWholeSpectra(PeakList peakList)
    {
        assertEquals(-1, peakList.getMostIntenseIndex());

        peakList.addSorted(new double[]{1, 2, 3, 4, 5, 6}, new double[]{1, 2, 5, 5, 4, 2});
        assertEquals(2, peakList.getMostIntenseIndex());
    }

    @Test
    public abstract void testMerge() throws Exception;

    protected <A extends PeakAnnotation> void runTestMerge(PeakList<A> peakList)
    {
        double[] masses =
                new double[]{995.465, 997.549, 1002.213, 1009.445, 1012.809, 1015.269, 1023.958, 1024.746, 1032.158,
                        1038.543,
                        1049.680, 1055.237, 1056.236, 1061.347, 1063.269, 1069.886, 1074.268, 1075.122, 1080.235,
                        1088.610,
                        1094.540, 1096.541, 1102.804, 1107.303, 1112.495, 1117.861, 1120.826, 1125.643, 1129.048,
                        1136.221,
                        1138.760, 1141.752, 1149.159, 1154.643, 1167.231, 1168.390, 1172.392, 1177.803, 1185.206,
                        1191.259,
                        1192.455, 1199.366, 1209.212, 1210.209, 1216.670};

        double[] masses2 =
                new double[]{995.833, 997.966, 1002.981, 1010.051, 1013.222, 1015.804, 1024.514, 1025.329, 1032.446,
                        1039.026,
                        1049.747, 1055.329, 1056.479, 1061.676, 1064.147, 1070.748, 1074.629, 1075.850, 1080.466,
                        1089.234,
                        1094.547, 1096.997, 1103.555, 1108.065, 1112.761, 1118.130, 1121.452, 1126.181, 1129.544,
                        1137.025,
                        1138.924, 1142.534, 1149.749, 1155.341, 1168.126, 1169.230, 1173.107, 1178.550, 1186.049,
                        1191.336,
                        1192.786, 1199.839, 1209.575, 1210.388, 1216.734};

        double[] intensities =
                new double[]{18.288, 10.924, 6.442, 17.300, 17.788, 18.947, 21.898, 11.378, 22.440, 10.302,
                        50.827, 17.054, 22.280, 11.892, 7.995, 10.341, 215.683, 88.473, 18.228, 13.208,
                        3.964, 5.049, 2.980, 1.780, 13.860, 5.808, 5.583, 2.364, 1.033, 13.762,
                        21.549, 5.541, 14.566, 19.965, 239.495, 40.558, 21.821, 9.397, 1820.518, 630.056,
                        23.832, 3.843, 6.159, 383.467, 58.726};

        peakList.addSorted(masses, intensities, masses.length);
        peakList.addSorted(masses2, intensities, masses.length);

        for (int i = 0; i < masses.length; i++) {

            assertEquals(masses[i], peakList.getX(2 * i), deltaMz);
            assertEquals(masses2[i], peakList.getX(2 * i + 1), deltaMz);
            assertEquals(intensities[i], peakList.getY(2 * i), deltaIntensity);
            assertEquals(intensities[i], peakList.getY(2 * i + 1), deltaIntensity);
        }
    }

    @Test
    public abstract void testMerge2();

    protected <A extends PeakAnnotation> void runTestMerge2(PeakList<A> consPeakList)
    {
        double[] masses =
                new double[]{995.465, 997.549, 1002.213, 1009.445, 1012.809, 1015.269, 1023.958, 1024.746, 1032.158,
                        1038.543,
                        1049.680, 1055.237, 1056.236, 1061.347, 1063.269, 1069.886, 1074.268, 1075.122, 1080.235,
                        1088.610,
                        1094.540, 1096.541, 1102.804, 1107.303, 1112.495, 1117.861, 1120.826, 1125.643, 1129.048,
                        1136.221,
                        1138.760, 1141.752, 1149.159, 1154.643, 1167.231, 1168.390, 1172.392, 1177.803, 1185.206,
                        1191.259,
                        1192.455, 1199.366, 1209.212, 1210.209, 1216.670};
        double[] intensities =
                new double[]{18.288, 10.924, 6.442, 17.300, 17.788, 18.947, 21.898, 11.378, 22.440, 10.302,
                        50.827, 17.054, 22.280, 11.892, 7.995, 10.341, 215.683, 88.473, 18.228, 13.208,
                        3.964, 5.049, 2.980, 1.780, 13.860, 5.808, 5.583, 2.364, 1.033, 13.762,
                        21.549, 5.541, 14.566, 19.965, 239.495, 40.558, 21.821, 9.397, 1820.518, 630.056,
                        23.832, 3.843, 6.159, 383.467, 58.726};

//        RandomDataGenerator generator = new RandomDataGenerator();
//        int[] permutation = generator.nextPermutation(masses.length, masses.length);

        double[] pMasses = new double[masses.length];
        double[] pIntensities = new double[masses.length];
        for (int i = 0; i < masses.length; i++) {
//            int j = permutation[i];
//            pMasses[i] = masses[j];
//            pIntensities[i] = intensities[j];
        }

        double[] pSubSetMasses = new double[masses.length];
        double[] pSubSetIntensities = new double[masses.length];
        final Comparator<double[]> comparator = Comparator.comparingDouble(o -> o[0]);

        double[][] data = new double[9][2];
        for (int i = 0; i < 5; i++) {
            System.arraycopy(pMasses, i * 9, pSubSetMasses, 0, 9);
            System.arraycopy(pIntensities, i * 9, pSubSetIntensities, 0, 9);

            for (int j = 0; j < 9; j++) {
                data[j][0] = pSubSetMasses[j];
                data[j][1] = pSubSetIntensities[j];
            }

            // Sort the IntDoublePairs
            Arrays.sort(data, 0, 9, comparator);

            for (int j = 0; j < 9; j++) {
                pSubSetMasses[j] = data[j][0];
                pSubSetIntensities[j] = data[j][1];
            }

            consPeakList.addSorted(pSubSetMasses, pSubSetIntensities, 9);
        }

        for (int i = 0; i < masses.length; i++) {
            assertEquals(masses[i], consPeakList.getX(i), deltaMz);
            assertEquals(intensities[i], consPeakList.getY(i), deltaIntensity);
        }
    }

    @Test
    public abstract void testMerge3();

    protected <A extends PeakAnnotation> void runTestMerge3(PeakList<A> peakList)
    {
        double[] masses =
                new double[]{995.465, 997.549, 1002.213, 1009.445, 1012.809, 1015.269, 1023.958, 1024.746, 1032.158,
                        1038.543,
                        1049.680, 1055.237, 1056.236, 1061.347, 1063.269, 1069.886, 1074.268, 1075.122, 1080.235,
                        1088.610,
                        1094.540, 1096.541, 1102.804, 1107.303, 1112.495, 1117.861, 1120.826, 1125.643, 1129.048,
                        1136.221,
                        1138.760, 1141.752, 1149.159, 1154.643, 1167.231, 1168.390, 1172.392, 1177.803, 1185.206,
                        1191.259,
                        1192.455, 1199.366, 1209.212, 1210.209, 1216.670};
        double[] intensities =
                new double[]{18.288, 10.924, 6.442, 17.300, 17.788, 18.947, 21.898, 11.378, 22.440, 10.302,
                        50.827, 17.054, 22.280, 11.892, 7.995, 10.341, 215.683, 88.473, 18.228, 13.208,
                        3.964, 5.049, 2.980, 1.780, 13.860, 5.808, 5.583, 2.364, 1.033, 13.762,
                        21.549, 5.541, 14.566, 19.965, 239.495, 40.558, 21.821, 9.397, 1820.518, 630.056,
                        23.832, 3.843, 6.159, 383.467, 58.726};

        for (int i = masses.length - 1; i >= 0; i--) {
            peakList.add(masses[i], intensities[i]);
        }
        assertArrayEquals(peakList.getXs(), masses, deltaMz);
        assertArrayEquals(peakList.getYs(), intensities, deltaIntensity);
    }

    @Test
    public abstract void testMerge4();

    protected <A extends PeakAnnotation> void runTestMerge4(PeakList<A> consPeakList)
    {

        double[] masses =
                new double[]{995.465, 997.549, 1002.213, 1009.445, 1012.809, 1015.269, 1023.958, 1024.746, 1032.158,
                        1038.543,
                        1049.680, 1055.237, 1056.236, 1061.347, 1063.269, 1069.886, 1074.268, 1075.122, 1080.235,
                        1088.610,
                        1094.540, 1096.541, 1102.804, 1107.303, 1112.495, 1117.861, 1120.826, 1125.643, 1129.048,
                        1136.221,
                        1138.760, 1141.752, 1149.159, 1154.643, 1167.231, 1168.390, 1172.392, 1177.803, 1185.206,
                        1191.259,
                        1192.455, 1199.366, 1209.212, 1210.209, 1216.670};
        double[] intensities =
                new double[]{18.288, 10.924, 6.442, 17.300, 17.788, 18.947, 21.898, 11.378, 22.440, 10.302,
                        50.827, 17.054, 22.280, 11.892, 7.995, 10.341, 215.683, 88.473, 18.228, 13.208,
                        3.964, 5.049, 2.980, 1.780, 13.860, 5.808, 5.583, 2.364, 1.033, 13.762,
                        21.549, 5.541, 14.566, 19.965, 239.495, 40.558, 21.821, 9.397, 1820.518, 630.056,
                        23.832, 3.843, 6.159, 383.467, 58.726};

//        RandomDataGenerator generator = new RandomDataGenerator();
//        int[] permutation = generator.nextPermutation(masses.length, masses.length);
        double[] pMasses = new double[masses.length];
        double[] pIntensities = new double[masses.length];
        for (int i = 0; i < masses.length; i++) {
//            int j = permutation[i];
//            pMasses[i] = masses[j];
//            pIntensities[i] = intensities[j];
        }

        for (int i = masses.length - 1; i >= 0; i--) {
            consPeakList.add(pMasses[i], pIntensities[i]);
        }
        assertArrayEquals(consPeakList.getXs(null), masses, deltaMz);
        assertArrayEquals(consPeakList.getYs(null), intensities, deltaIntensity);
    }

    @Test
    public abstract void testMerge5() throws Exception;

    protected void runTestMerge5(PeakList<MockPeakAnnotation> peakList) throws NoSuchFieldException,
            IllegalAccessException
    {
        peakList.add(1, 1, Collections.singletonList(new MockPeakAnnotation(Ion.b, 1, 1)));
        peakList.add(3, 3, Collections.singletonList(new MockPeakAnnotation(Ion.b, 3, 1)));
        peakList.add(5, 5, Collections.singletonList(new MockPeakAnnotation(Ion.b, 5, 1)));

        peakList.addSorted(new double[]{2, 4}, new double[]{2, 4});

        assertEquals(5, peakList.size());

        checkPeakList(new double[]{1, 2, 3, 4, 5}, new double[]{1, 2, 3, 4, 5}, peakList);

        assertEquals(15, peakList.getTotalIonCurrent(), deltaIntensity);

        assertEquals(1, peakList.getAnnotations(0).size());
        assertEquals(0, peakList.getAnnotations(1).size());
        assertEquals(1, peakList.getAnnotations(2).size());
        assertEquals(0, peakList.getAnnotations(3).size());
        assertEquals(1, peakList.getAnnotations(4).size());
    }

    @Test
    public abstract void testMerge6() throws Exception;

    protected void runTestMerge6(PeakList<MockPeakAnnotation> peakList1, PeakList<MockPeakAnnotation> peakList2)
            throws NoSuchFieldException, IllegalAccessException
    {

        peakList1.add(1, 1, Collections.singletonList(new MockPeakAnnotation(Ion.b, 1, 1)));
        peakList1.add(3, 3, Collections.singletonList(new MockPeakAnnotation(Ion.b, 3, 1)));
        peakList1.add(5, 5, Collections.singletonList(new MockPeakAnnotation(Ion.b, 5, 1)));

        peakList2.add(2, 2, Collections.singletonList(new MockPeakAnnotation(Ion.y, 2, 1)));
        peakList2.add(4, 4, Collections.singletonList(new MockPeakAnnotation(Ion.y, 4, 1)));

        peakList1.addPeaks(peakList2);

        assertEquals(5, peakList1.size());

        checkPeakList(new double[]{1, 2, 3, 4, 5}, new double[]{1, 2, 3, 4, 5}, peakList1);

        assertEquals(15, peakList1.getTotalIonCurrent(), deltaIntensity);

        assertEquals(Ion.b, peakList1.getAnnotations(0).get(0).getPeptideIonType());
        assertEquals(Ion.y, peakList1.getAnnotations(1).get(0).getPeptideIonType());
        assertEquals(Ion.b, peakList1.getAnnotations(2).get(0).getPeptideIonType());
        assertEquals(Ion.y, peakList1.getAnnotations(3).get(0).getPeptideIonType());
        assertEquals(Ion.b, peakList1.getAnnotations(4).get(0).getPeptideIonType());
    }

    @Test
    public abstract void testAddPeaks() throws Exception;

    protected void runTestAddPeaks(PeakList<MockPeakAnnotation> peakList1, PeakList<PeakAnnotation> peakList2) throws
            NoSuchFieldException, IllegalAccessException
    {

        peakList1.add(1, 1, Collections.singletonList(new MockPeakAnnotation(Ion.b, 1, 1)));
        peakList1.add(3, 3, Collections.singletonList(new MockPeakAnnotation(Ion.b, 3, 1)));
        peakList1.add(5, 5, Collections.singletonList(new MockPeakAnnotation(Ion.b, 5, 1)));

        peakList2.add(2, 2, Collections.singletonList(mock(MockPeakAnnotation.class)));
        peakList2.add(4, 4, Collections.singletonList(mock(MockPeakAnnotation.class)));

        peakList1.addPeaksNoAnnotations(peakList2);

        assertEquals(5, peakList1.size());

        checkPeakList(new double[]{1, 2, 3, 4, 5}, new double[]{1, 2, 3, 4, 5}, peakList1);

        assertEquals(15, peakList1.getTotalIonCurrent(), deltaIntensity);

        assertEquals(Ion.b, peakList1.getAnnotations(0).get(0).getPeptideIonType());
        assertEquals(0, peakList1.getAnnotations(1).size());
        assertEquals(Ion.b, peakList1.getAnnotations(2).get(0).getPeptideIonType());
        assertEquals(0, peakList1.getAnnotations(3).size());
        assertEquals(Ion.b, peakList1.getAnnotations(4).get(0).getPeptideIonType());
    }

    @Test
    public abstract void testAddDuplicateWithAnnotation() throws Exception;

    protected <A extends PeakAnnotation> void runTestAddDuplicateWithAnnotation(PeakList<A> peakList, Class<A>
            classToMock)
    {

        peakList.add(1, 2, Collections.singletonList(mock(classToMock)));
        peakList.add(1, 2, Collections.singletonList(mock(classToMock)));

        assertEquals(1, peakList.size());
        assertEquals(1, peakList.getAnnotationIndexes().length);
        assertEquals(4, peakList.getY(0), deltaIntensity);
        assertEquals(2, peakList.getAnnotations(0).size());
    }

    @Test
    public abstract void testDoInsert() throws Exception;

    protected <A extends PeakAnnotation> void runTestDoInsert(AbstractPeakList<A> peakList)
    {

        double vd = 58.0287398307;
        peakList.grow();
        peakList.doInsert(vd, 1);
        peakList.grow();
        peakList.doInsert(vd, 1);

        assertEquals(1, peakList.size());
    }

    @Test
    public abstract void testIndexOf() throws Exception;

    protected void runTestIndexOf(PeakList peakList)
    {

        peakList.add(12, 1);
        peakList.add(12.5, 1);
        peakList.add(124, 1);

        assertEquals(-1, peakList.indexOf(10));
        assertEquals(0, peakList.indexOf(12));
        assertEquals(-2, peakList.indexOf(12.4));
        assertEquals(1, peakList.indexOf(12.5));
        assertEquals(-3, peakList.indexOf(12.51));
        assertEquals(2, peakList.indexOf(124));
        assertEquals(-4, peakList.indexOf(124.1));
    }

    @Test
    public abstract void testGetClosestIndex();

    protected void runTestGetClosestIndex(PeakList peakList)
    {
        peakList.add(12, 1);
        peakList.add(12.5, 1);
        peakList.add(124, 1);

        assertEquals(0, peakList.getClosestIndex(10));
        assertEquals(0, peakList.getClosestIndex(12));
        assertEquals(0, peakList.getClosestIndex(12.249));

        assertEquals(0, peakList.getClosestIndex(12.25));
        assertEquals(1, peakList.getClosestIndex(12.5));
        assertEquals(1, peakList.getClosestIndex(20));

        assertEquals(2, peakList.getClosestIndex(124));
        assertEquals(2, peakList.getClosestIndex(1000));
    }

    @Test
    public void testIsEmpty() throws Exception
    {
        PeakList peakList = newPeakList();
        assertTrue(peakList.isEmpty());

        peakList.add(12, 3);
        assertFalse(peakList.isEmpty());
    }

    @Test
    public void testAddAnnotationInEmptyPeakList() throws Exception
    {
        PeakList<PeakAnnotation> peakList = newPeakList();

        assertThrows(IndexOutOfBoundsException.class, () -> peakList.addAnnotation(0, mock(PeakAnnotation.class)));

    }

    @Test
    public void testHasAnnotationAt() throws Exception
    {
        PeakList<MockPeakAnnotation> peakList = newPeakList();
        peakList.add(34, 2, Collections.singletonList(mock(MockPeakAnnotation.class)));
        peakList.add(45, 5);

        assertTrue(peakList.hasAnnotationsAt(0));
        assertFalse(peakList.hasAnnotationsAt(1));
    }

    @Test
    public void testGetFirstAnnotation() throws Exception
    {
        MockPeakAnnotation annotation = mock(MockPeakAnnotation.class);

        PeakList<MockPeakAnnotation> peakList = newPeakList();
        peakList.add(34, 2, Collections.singletonList(annotation));
        peakList.add(45, 5);

        assertEquals(annotation, peakList.getFirstAnnotation(0));
        assertNull(peakList.getFirstAnnotation(1));
    }

    @Test
    public void testRemoveAnnotation() throws Exception
    {
        MockPeakAnnotation annotation = mock(MockPeakAnnotation.class);

        PeakList<MockPeakAnnotation> peakList = newPeakList();
        peakList.add(34, 2, Collections.singletonList(annotation));
        peakList.add(45, 5);

        assertEquals(annotation, peakList.getFirstAnnotation(0));
        assertEquals(null, peakList.getFirstAnnotation(1));

        boolean removed = peakList.removeAnnotation(annotation, 0);
        assertEquals(true, removed);
        assertEquals(false, peakList.hasAnnotationsAt(0));

        removed = peakList.removeAnnotation(annotation, 0);
        assertEquals(false, removed);
    }

    @Test
    public void testClearAnnotations() throws Exception
    {

        PeakList<MockPeakAnnotation> peakList = newPeakList();
        peakList.add(34, 2, Collections.singletonList(mock(MockPeakAnnotation.class)));
        peakList.add(45, 5, Collections.singletonList(mock(MockPeakAnnotation.class)));

        assertEquals(true, peakList.hasAnnotations());

        peakList.clearAnnotations();

        assertEquals(false, peakList.hasAnnotations());
    }

    @Test
    public void testClearAnnotationsAt()
    {

        PeakList<MockPeakAnnotation> peakList = newPeakList();
        peakList.add(34, 2, Arrays.asList(mock(MockPeakAnnotation.class), mock(MockPeakAnnotation.class)));
        peakList.add(45, 5, Collections.singletonList(mock(MockPeakAnnotation.class)));

        assertTrue(peakList.hasAnnotationsAt(0));
        assertTrue(peakList.hasAnnotationsAt(1));

        peakList.clearAnnotationsAt(0);

        assertFalse(peakList.hasAnnotationsAt(0));
        assertTrue(peakList.hasAnnotationsAt(1));
    }

    @Test
    public void testSortAnnotations() throws Exception
    {

        List<MockPeakAnnotation> annotationList = new ArrayList<>(3);
        annotationList.add(new MockPeakAnnotation(Ion.b, 3, 2));
        annotationList.add(new MockPeakAnnotation(Ion.b, 2, 2));
        annotationList.add(new MockPeakAnnotation(Ion.b, 1, 2));

        PeakList<MockPeakAnnotation> peakList = newPeakList();
        peakList.add(34, 2, annotationList);
        peakList.add(45, 5);

        peakList.sortAnnotations(Comparator.comparingDouble(MockPeakAnnotation::getCharge));

        List<MockPeakAnnotation> expectedAnnotations = new ArrayList<>(3);
        expectedAnnotations.add(new MockPeakAnnotation(Ion.b, 1, 2));
        expectedAnnotations.add(new MockPeakAnnotation(Ion.b, 2, 2));
        expectedAnnotations.add(new MockPeakAnnotation(Ion.b, 3, 2));

        assertEquals(expectedAnnotations, peakList.getAnnotations(0));
    }

    @Test
    public void testSetPrecursor() throws Exception
    {
        PeakList peakList = newPeakList();

        Peak peak = Peak.noIntensity(12);
        assertNotSame(peak, peakList.getPrecursor());

        peakList.setPrecursor(peak);
        assertSame(peak, peakList.getPrecursor());
    }

    @Test
    public void testApplyPeakProcessor()
    {
        PeakList<PeakAnnotation> peakList = newPeakList();

        Precision precision = peakList.getPrecision();
        if (precision == Precision.DOUBLE_CONSTANT || precision == Precision.FLOAT_CONSTANT)
            return;

        peakList.addSorted(
                new double[]{1.2, 1.3, 1.4, 1.5, 1.6},
                new double[]{4, 9, 16, 25, 36}
        );

        peakList.apply(new SqrtTransformer<>());

        assertArrayEquals(new double[]{2, 3, 4, 5, 6}, peakList.getYs(new double[5]), 0.00001);
    }

    @Test
    public void testApplyPeakProcessorChain() throws Exception
    {
        if (!checkTotalIonCurrent) return;

        PeakList<PeakAnnotation> peakList = newPeakList();

        peakList.addSorted(
                new double[]{1.2, 1.3, 1.4, 1.5, 1.6},
                new double[]{4, 9, 16, 25, 36}
        );

        peakList.apply(new PeakProcessorChain<>(new SqrtTransformer<>()));

        assertArrayEquals(new double[]{2, 3, 4, 5, 6}, peakList.getYs(new double[5]), 0.00001);
        assertEquals(2 + 3 + 4 + 5 + 6, peakList.getTotalIonCurrent(), deltaIntensity);
    }

    @Test
    public void testAddUnsortedPeaksNotExpected() throws Exception
    {
        PeakList pl = newPeakList();

        assertThrows(UnsortedPeakListException.class, () -> pl.addSorted(new double[]{1, 20, 3}, new double[]{1, 1,
                1}));
    }

    @Test
    public void testCopy()
    {
        PeakList<MockPeakAnnotation> peakList = newPeakList();
        Map<Integer, MockPeakAnnotation> annotationMap = new HashMap<>();
        annotationMap.put(0, new MockPeakAnnotation(Ion.y, 2, 0));
        annotationMap.put(2, new MockPeakAnnotation(Ion.y, 2, 2));
        build(peakList, new double[]{1, 2, 3, 4}, new double[]{10, 20, 30, 40}, annotationMap);

        PeakList<MockPeakAnnotation> copy = peakList.copy(new IdentityPeakProcessor<>());

        assertNotSame(peakList, copy);
        assertEquals(peakList, copy);

        copy = peakList.copy(new PeakProcessorChain<>(new IdentityPeakProcessor<>()));

        assertNotSame(peakList, copy);
        assertEquals(peakList, copy);
    }

    @Test
    public void testIonCurrent() throws Exception
    {
        if (!checkTotalIonCurrent) return;

        PeakList<PeakAnnotation> peakList = newPeakList();

        peakList.add(100.0, 10.0);
        assertEquals(10, peakList.getTotalIonCurrent(), deltaIntensity);
        peakList.add(100.0, 10.0);
        assertEquals(20, peakList.getTotalIonCurrent(), deltaIntensity);
        peakList.add(200.0, 12.0);
        assertEquals(32, peakList.getTotalIonCurrent(), deltaIntensity);
        peakList.add(300.0, 13.0);
        assertEquals(45, peakList.getTotalIonCurrent(), deltaIntensity);
    }

    @Test
    public void testCopy2() throws Exception
    {
        PeakList<MockPeakAnnotation> peakList = newPeakList();
        Map<Integer, MockPeakAnnotation> annotationMap = new HashMap<>();
        annotationMap.put(0, new MockPeakAnnotation(Ion.y, 2, 0));
        annotationMap.put(2, new MockPeakAnnotation(Ion.y, 2, 2));
        build(peakList, new double[]{1, 2, 3, 4}, new double[]{10, 20, 30, 40}, annotationMap);

        PeakList<MockPeakAnnotation> copy = peakList.copy(new SqrtTransformer<MockPeakAnnotation>());

        assertEquals(19.43619451055638, copy.getTotalIonCurrent(), deltaIntensity);

        copy = peakList.copy(new PeakProcessorChain<>(new SqrtTransformer<MockPeakAnnotation>()));

        assertEquals(19.43619451055638, copy.getTotalIonCurrent(), deltaIntensity);
    }

    @Test
    public void testAddUnsortedPeaks() throws Exception
    {

        PeakList<MockPeakAnnotation> pl = newPeakList();

        pl.add(1, 1, Collections.singletonList(new MockPeakAnnotation(Ion.b, 1, 1)));
        pl.add(3, 1, Collections.singletonList(new MockPeakAnnotation(Ion.b, 1, 3)));
        pl.add(2, 1, Collections.singletonList(new MockPeakAnnotation(Ion.b, 1, 2)));

        assertEquals(1.0, pl.getX(0), 0.1);
        assertEquals(2.0, pl.getX(1), 0.1);
        assertEquals(3.0, pl.getX(2), 0.1);

        assertEquals(1, pl.getAnnotations(0).get(0).getId());
        assertEquals(2, pl.getAnnotations(1).get(0).getId());
        assertEquals(3, pl.getAnnotations(2).get(0).getId());
    }

    @Test
    public void testBasePeakMzAndIntensity()
    {
        if (!checkTotalIonCurrent) return;

        PeakList peakList = newPeakList();

        peakList.add(106.05, 1000);
        peakList.add(177.09, 10);
        peakList.add(233.06, 100);
        peakList.add(290.17, 10);
        peakList.add(389.16, 1000);
        peakList.trimToSize();

        assertEquals(0, peakList.getMostIntenseIndex());
    }

    @Test
    public void testGetBasePeak() throws Exception
    {
        PeakList peakList = newPeakList();

        if (peakList.getPrecision() == Precision.FLOAT_CONSTANT || peakList.getPrecision() == Precision.DOUBLE_CONSTANT)
            return;

        peakList.add(1, 1);
        peakList.add(2, 2);
        peakList.add(3, 3);
        peakList.add(4, 4);
        peakList.add(5, 10);
        peakList.add(6, 9);
        peakList.add(7, 8);
        peakList.add(8, 7);
        peakList.add(9, 6);
        peakList.add(10, 5);

        assertEquals(5.0, peakList.getBasePeakX(), 0.00000001);
        assertEquals(10.0, peakList.getBasePeakY(), 0.00000001);
    }

    @Test
    public void testConvertAnnotationOnPeakAddition() throws Exception
    {
        PeakList<PeakAnnotation> srcPeakList = newPeakList();

        PeakAnnotation annotation = Mockito.mock(PeakAnnotation.class);
        srcPeakList.add(1, 1, annotation);

        PeakList<PeakAnnotation> destPeakList = newPeakList();

        Function<List<PeakAnnotation>, List<PeakAnnotation>> annotationConverter = Mockito.mock(Function.class);
        when(annotationConverter.apply(Mockito.any(List.class))).thenReturn(Collections.<PeakAnnotation>emptyList());

        destPeakList.addPeaks(srcPeakList, annotationConverter);

        verify(annotationConverter, times(1)).apply(Mockito.anyList());
        assertArrayEquals(destPeakList.getAnnotationIndexes(), new int[0]);
    }

    @Test
    public void testCursor()
    {
        PeakList<PeakAnnotation> peakList = newPeakList();

        peakList.add(1, 1);
        peakList.add(2, 2);
        peakList.add(3, 3);
        peakList.add(4, 4);
        peakList.add(5, 5);
        peakList.add(6, 6);

        PeakCursor<PeakAnnotation> cursor = peakList.cursor();

        assertEquals(3, cursor.getClosestIndex(3.9));
        assertEquals(2, cursor.getClosestIndex(3.5));

        cursor.movePast(4.3);
        assertEquals(5.0, cursor.currMz(), deltaMz);

        cursor.moveBefore(2.5);
        assertEquals(2.0, cursor.currMz(), deltaMz);
        cursor.moveBefore(2.9);
        assertEquals(2.0, cursor.currMz(), deltaMz);

        cursor.moveToClosest(5.5);
        assertEquals(5.0, cursor.currMz(), deltaMz);

        cursor.moveToClosest(5.51);
        assertEquals(6.0, cursor.currMz(), deltaMz);

        assertEquals(false, cursor.canPeek(1));
        assertEquals(false, cursor.canPeek(2));

        assertEquals(true, cursor.canPeek(-3));
        assertEquals(3.0, cursor.peekMz(-3), deltaMz);

        boolean previous = cursor.previous();
        assertEquals(true, previous);
        assertEquals(5.0, cursor.currMz(), deltaMz);

        cursor.resetCursor();
        boolean next = cursor.next();
        assertEquals(true, next);
        assertEquals(1.0, cursor.currMz(), deltaMz);
    }
}
