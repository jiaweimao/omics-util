package omics.util.ms.peaklist.impl;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.MockPeakAnnotation;
import omics.util.ms.peaklist.MockPeakSink;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.protein.ms.Ion;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class DelayedPeakProcessorTest
{
    @Test
    void testOrder()
    {
        DelayedPeakProcessor<PeakAnnotation, PeakAnnotation> delayedPeakProcessor = new DelayedPeakProcessor<PeakAnnotation, PeakAnnotation>()
        {
            @Override
            protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap<List<PeakAnnotation>> annotationMap)
            {
                for (int i = 0; i < mzList.size(); i++) {
                    sink.processPeak(mzList.getDouble(i) + 0.1, intensityList.getDouble(i), annotationMap.get(i));
                    sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), annotationMap.get(i));
                    sink.processPeak(mzList.getDouble(i) - 0.1, intensityList.getDouble(i), annotationMap.get(i));
                }
            }
        };

        MsnSpectrum spectrum = new MsnSpectrum();
        for (int i = 1; i <= 10; i++) {
            spectrum.add(i, 1);
        }

        spectrum.apply(delayedPeakProcessor);
        System.out.println(spectrum);
    }

    @Test
    void test()
    {
        MockDelayedPeakProcessor peakProcessor = new MockDelayedPeakProcessor();
        List<MockPeakAnnotation> empty = Collections.emptyList();

        MockPeakSink<MockPeakAnnotation> sink = new MockPeakSink<MockPeakAnnotation>();
        peakProcessor.sink(sink);

        peakProcessor.start(3);

        peakProcessor.processPeak(124.87, 3498, empty);
        peakProcessor.processPeak(876.87, 12,
                Collections.singletonList(new MockPeakAnnotation(Ion.a, 2, 2)));
        peakProcessor.processPeak(1174.82, 100, empty);

        peakProcessor.end();

        Int2ObjectMap<List<MockPeakAnnotation>> annotationMap = new Int2ObjectOpenHashMap<>();
        annotationMap.put(1, Collections.singletonList(new MockPeakAnnotation(Ion.a, 2, 2)));

        peakProcessor.check(
                new DoubleArrayList(new double[]{124.87, 876.87, 1174.82}),
                new DoubleArrayList(new double[]{3498, 12, 100}),
                annotationMap
        );

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getStartCalled());
    }

    @Test
    void testClearingOnStart()
    {
        MockDelayedPeakProcessor peakProcessor = new MockDelayedPeakProcessor();
        List<MockPeakAnnotation> empty = Collections.emptyList();

        MockPeakSink<MockPeakAnnotation> sink = new MockPeakSink<MockPeakAnnotation>();
        peakProcessor.sink(sink);

        peakProcessor.start(3);

        peakProcessor.processPeak(876.87, 12,
                Collections.singletonList(new MockPeakAnnotation(Ion.a, 2, 2)));

        peakProcessor.end();

        //Process next peak list
        peakProcessor.start(3);

        peakProcessor.processPeak(124.87, 3498, empty);
        peakProcessor.processPeak(876.87, 12,
                Collections.singletonList(new MockPeakAnnotation(Ion.a, 2, 2)));
        peakProcessor.processPeak(1174.82, 100, empty);

        peakProcessor.end();

        Int2ObjectMap<List<MockPeakAnnotation>> annotationMap = new Int2ObjectOpenHashMap<>();
        annotationMap.put(1, Collections.singletonList(new MockPeakAnnotation(Ion.a, 2, 2)));

        peakProcessor.check(
                new DoubleArrayList(new double[]{124.87, 876.87, 1174.82}),
                new DoubleArrayList(new double[]{3498, 12, 100}),
                annotationMap
        );

        assertEquals(2, sink.getStartCalled());
        assertEquals(2, sink.getEndCalled());
    }

    private static class MockDelayedPeakProcessor extends DelayedPeakProcessor<MockPeakAnnotation, MockPeakAnnotation>
    {
        //The cached m/z's
        private DoubleArrayList mzList;
        //The cached intensities
        private DoubleArrayList intensityList;
        //The cached annotations
        private Int2ObjectMap<List<MockPeakAnnotation>> annotationMap;

        public void check(DoubleArrayList expectedMzs, DoubleArrayList expectedIntensities,
                Int2ObjectMap<List<MockPeakAnnotation>> expectedAnnotations)
        {
            assertEquals(expectedMzs, mzList);
            assertEquals(expectedIntensities, intensityList);
            assertEquals(expectedAnnotations, annotationMap);
        }

        @Override
        protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList,
                Int2ObjectMap<List<MockPeakAnnotation>> annotationMap)
        {
            this.mzList = mzList;
            this.intensityList = intensityList;
            this.annotationMap = annotationMap;
        }
    }
}