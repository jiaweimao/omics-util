package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class PeakCollectorSinkTest
{
    @Test
    void testProcessPeak()
    {
        @SuppressWarnings("unchecked")
        PeakList<PeakAnnotation> peakList = mock(PeakList.class);

        PeakCollectorSink<PeakAnnotation> sink = new PeakCollectorSink<PeakAnnotation>(peakList);

        List<PeakAnnotation> annotations = Collections.emptyList();
        sink.processPeak(456.78, 341.87, annotations);

        sink.end();

        verify(peakList).clear();
        verify(peakList).add(456.78, 341.87);
        verifyNoMoreInteractions(peakList);
    }
}