package omics.util.ms.peaklist.impl;

import omics.util.ms.peaklist.MockPeakSink;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class PeakListMergerTest
{
    @Test
    void testMergeOne()
    {
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.sink(sink);

        PeakList<PeakAnnotation> list1 = new FloatPeakList<>();
        list1.add(1, 1);
        list1.add(3, 1);
        list1.add(5, 1);
        list1.add(6, 1);

        merger.merge(Collections.singletonList(list1));

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());

        assertArrayEquals(new double[]{1, 3, 5, 6}, sink.getMzList(), 0.0001);
    }

    @Test
    void testMerge()
    {
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.sink(sink);

        PeakList<PeakAnnotation> list1 = new FloatPeakList<>();
        list1.add(1, 1);
        list1.add(3, 1);
        list1.add(5, 1);
        list1.add(6, 1);

        PeakList<PeakAnnotation> list2 = new FloatPeakList<>();
        list2.add(2, 2);
        list2.add(4, 2);
        list2.add(6, 2);

        merger.merge(Arrays.asList(list2, list1));

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());

        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6, 6}, sink.getMzList(), 0.0001);
    }

    @Test
    void testMerge2()
    {
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.sink(sink);

        PeakList<PeakAnnotation> list1 = new FloatPeakList<>();
        list1.add(1, 1);
        list1.add(3, 1);

        PeakList<PeakAnnotation> list2 = new FloatPeakList<>();
        list2.add(4, 2);
        list2.add(6, 2);

        merger.merge(Arrays.asList(list2, list1));

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());

        assertArrayEquals(new double[]{1, 3, 4, 6}, sink.getMzList(), 0.0001);
    }

    @Test
    void testMergeRandom()
    {
        double[] mzs = new double[5000];

        Random random = new Random();

        for (int i = 0; i < mzs.length; i++) {

            mzs[i] = random.nextDouble() * 1500;
        }

        Arrays.sort(mzs);

        int partitions = 50;

        List<PeakList<PeakAnnotation>> spectra = new ArrayList<>(partitions);

        for (int i = 0; i < partitions; i++) {

            spectra.add(new DoubleConstantPeakList<>(i + 1));
        }

        for (double mz : mzs) {

            int spectrum = random.nextInt(partitions);

            spectra.get(spectrum).add(mz, 1);
        }

        for (PeakList peakList : spectra) {

            peakList.trimToSize();
        }

        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();

        PeakListMerger<PeakAnnotation> merger = new PeakListMerger<>();
        merger.sink(sink);

        merger.merge(spectra);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());

        assertArrayEquals(mzs, sink.getMzList(), 0.00000001);
    }
}