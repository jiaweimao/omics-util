package omics.util.ms.peaklist.sim;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.DoublePeakList;
import omics.util.ms.peaklist.sim.impl.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


class PeakListAlignerTest
{
    @Test
    void align_mz()
    {
        PeakListAligner<PeakAnnotation, PeakAnnotation> aligner = new PreferCloserMzAligner<>(
                Tolerance.abs(0.01));

        PeakList<PeakAnnotation> list1 = new DoublePeakList<>();
        list1.add(4.7, 1.0);
        list1.add(4.77, 1.0);

        PeakList<PeakAnnotation> list2 = new DoublePeakList<>();
        list2.add(4.75, 2.0);
        list2.add(4.78, 1.0);

        Int2IntMap map = aligner.align(list1, list2);
        assertEquals("{0=>0, 1=>1}", map.toString());

        list1.clear();
        list2.clear();

        list1.add(340.7588806152344, 10);

        list2.add(340.66839599609375, 10);
        list2.add(340.742431640625, 10);

        map = aligner.align(list1, list2);
        assertEquals("{0=>1}", map.toString());
    }

    @Test
    void align()
    {
        Tolerance tolerance = Tolerance.abs(0.05);

        PeakListAligner<PeakAnnotation, PeakAnnotation> alignerMz = new PreferCloserMzAligner<>(tolerance);
        PeakListAligner<PeakAnnotation, PeakAnnotation> alignerMzDup = new PreferCloserMzDuplicateAligner<>(tolerance);
        PeakListAligner<PeakAnnotation, PeakAnnotation> alignerIn = new PreferCloserIntensityAligner<>(tolerance);
        PeakListAligner<PeakAnnotation, PeakAnnotation> alignerInDup = new PreferCloserIntensityDuplicateAligner<>(tolerance);
        PeakListAligner<PeakAnnotation, PeakAnnotation> alignerMaxIn = new PreferLargerIntensityAligner<>(tolerance);
        PeakListAligner<PeakAnnotation, PeakAnnotation> alignerMaxInDup = new PreferLargerIntensityDuplicateAligner<>(tolerance);

        PeakList<PeakAnnotation> list1 = new DoublePeakList<>();
        PeakList<PeakAnnotation> list2 = new DoublePeakList<>();

        for (int i = 1; i <= 5; i++) {
            list1.add(i, 1);
            list1.add(i + 0.02, 3);

            list2.add(i + 0.02, 2);
            list2.add(i + 0.041, 1);
            list2.add(i + 0.042, 3);
        }

        test(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                new int[]{1, 0, 4, 3, 7, 6, 10, 9, 13, 12}, alignerMz.align(list1, list2));

        test(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                new int[]{0, 0, 3, 3, 6, 6, 9, 9, 12, 12}, alignerMzDup.align(list1, list2));

        test(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                new int[]{1, 2, 4, 5, 7, 8, 10, 11, 13, 14}, alignerIn.align(list1, list2));

        test(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                new int[]{1, 2, 4, 5, 7, 8, 10, 11, 13, 14}, alignerInDup.align(list1, list2));

        test(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                new int[]{0, 2, 3, 5, 6, 8, 9, 11, 12, 14}, alignerMaxIn.align(list1, list2));

        test(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                new int[]{2, 2, 5, 5, 8, 8, 11, 11, 14, 14}, alignerMaxInDup.align(list1, list2));
    }

    private void test(int[] keys, int[] values, Int2IntMap map)
    {
        int[] ints = map.keySet().toIntArray();
        Arrays.sort(ints);
        assertArrayEquals(keys, ints);

        int[] array = new int[ints.length];
        int count = 0;
        for (int key : ints) {
            array[count] = map.get(key);
            count++;
        }

        assertArrayEquals(values, array);
    }

}