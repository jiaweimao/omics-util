package omics.util.ms.peaklist.transform;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.DoublePeakList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class AddFlankingPeaksTransformerTest
{
    double[] masses = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    double[] intensities = new double[]{50, 0, 0, 0, 50, 50, 0, 0, 0, 50, 0, 0, 0, 0, 50, 0, 0, 0, 0, 50};

    @Test
    void testFlankingPeaksTransformer()
    {
        AddFlankingPeaksTransformer<PeakAnnotation> transformer = new AddFlankingPeaksTransformer<>(0.5,
                30.0);
        DoublePeakList<PeakAnnotation> expPeakList = new DoublePeakList<>();
        expPeakList.addSorted(masses, intensities, masses.length);

        expPeakList.apply(transformer);

        double[] newIntensities = new double[]{50, 25, 0, 25, 50, 50, 25, 0, 25, 50, 25, 0, 0, 25, 50, 25, 0, 0, 25,
                50};

        double[] flankedInt = expPeakList.getYs();

        assertArrayEquals(newIntensities, flankedInt, 0.0001);
    }
}