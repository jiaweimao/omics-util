package omics.util.ms.peaklist.transform;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Precision;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 14 Aug 2019, 5:49 PM
 */
class MzShiftTransformerTest
{
    @Test
    void test()
    {
        MsnSpectrum spectrum = new MsnSpectrum(10, Precision.FLOAT);
        for (int i = 0; i < 10; i++) {
            spectrum.add(i, 1);
        }

        MsnSpectrum shift = spectrum.copy(new MzShiftTransformer<>(0.3));
        for (int i = 0; i < shift.size(); i++) {
            assertEquals(spectrum.getX(i) + 0.3, shift.getX(i), 0.001);
        }
    }
}