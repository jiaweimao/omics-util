package omics.util.ms.peaklist.transform;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.MockPeakSink;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Precision;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class NHighestPeaksNormalizerTest
{
    @Test
    void test()
    {
        NHighestPeaksNormalizer<PeakAnnotation> norm = new NHighestPeaksNormalizer<>(5, 100);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();
        norm.sink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        norm.start(15);
        norm.processPeak(1, 493, annotations);
        norm.processPeak(2, 229, annotations);
        norm.processPeak(3, 138, annotations);
        norm.processPeak(4, 704, annotations);
        norm.processPeak(5, 751, annotations);
        norm.processPeak(6, 18, annotations);
        norm.processPeak(7, 874, annotations);
        norm.processPeak(8, 435, annotations);
        norm.processPeak(9, 446, annotations);
        norm.processPeak(10, 222, annotations);
        norm.processPeak(11, 375, annotations);
        norm.processPeak(12, 88, annotations);
        norm.processPeak(13, 459, annotations);
        norm.processPeak(14, 327, annotations);
        norm.processPeak(15, 222, annotations);
        norm.end();

        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, sink.getMzList(), 0.000001);
        assertArrayEquals(new double[]{15.02591, 6.97958, 4.20603, 21.45687, 22.88936, 0.54861, 26.63822,
                13.25815, 13.59342, 6.76623, 11.42944, 2.68211, 13.98964, 9.96647, 6.76623}, sink.getIntensityList(), 0.00001);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }

    @Test
    void testReuse()
    {
        MsnSpectrum s1 = new MsnSpectrum(3, Precision.FLOAT);
        MsnSpectrum s2 = new MsnSpectrum(3, Precision.FLOAT);

        s1.add(1, 1);
        s1.add(2, 2);
        s1.add(3, 3);

        s2.add(3, 5);
        s2.add(4, 8);
        s2.add(5, 10);

        NHighestPeaksNormalizer<PeakAnnotation> n = new NHighestPeaksNormalizer<>(1, 100);

        MsnSpectrum copy1 = s2.copy(n);
        System.out.println(copy1);

        MsnSpectrum copy = s1.copy(n);
        System.out.println(copy);
    }
}