package omics.util.ms.peaklist.transform;

import omics.util.ms.peaklist.MockPeakSink;
import omics.util.ms.peaklist.PeakAnnotation;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class NthPeakNormalizerTest
{
    @Test
    void test1()
    {
        NthPeakNormalizer<PeakAnnotation> norm = new NthPeakNormalizer<>(1);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();
        norm.sink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        norm.start(6);

        norm.processPeak(1, 5, annotations);
        norm.processPeak(2, 10, annotations);
        norm.processPeak(3, 2, annotations);
        norm.processPeak(4, 8, annotations);
        norm.processPeak(5, 6, annotations);
        norm.processPeak(6, 4, annotations);

        norm.end();

        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, sink.getMzList(), 0.000001);
        assertArrayEquals(new double[]{5 / 10.0, 1.0, 2 / 10.0, 8 / 10.0, 6 / 10.0, 4 / 10.0},
                sink.getIntensityList(), 0.000001);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }

    @Test
    void test2()
    {
        NthPeakNormalizer<PeakAnnotation> norm = new NthPeakNormalizer<>(2);
        MockPeakSink<PeakAnnotation> sink = new MockPeakSink<>();
        norm.sink(sink);

        List<PeakAnnotation> annotations = Collections.emptyList();

        norm.start(6);

        norm.processPeak(1, 5, annotations);
        norm.processPeak(2, 10, annotations);
        norm.processPeak(3, 2, annotations);
        norm.processPeak(4, 8, annotations);
        norm.processPeak(5, 6, annotations);
        norm.processPeak(6, 4, annotations);

        norm.end();

        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, sink.getMzList(), 0.000001);
        assertArrayEquals(new double[]{0.625, 1.25, 0.25, 1.00, 0.75, 0.50}, sink.getIntensityList(),
                0.000001);

        assertEquals(1, sink.getStartCalled());
        assertEquals(1, sink.getEndCalled());
    }
}