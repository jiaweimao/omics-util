package omics.util.ms.peaklist.transform;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.Precision;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 May 2018, 2:14 PM
 */
class RemoveAllPeakProcessorTest
{
    @Test
    void testProcessPeak()
    {
        MsnSpectrum spectrum = new MsnSpectrum(5, Precision.DOUBLE);
        spectrum.add(1.0, 2.0);
        spectrum.add(2.0, 2.0);
        spectrum.add(3.0, 2.0);
        spectrum.add(4.0, 2.0);
        spectrum.add(5.0, 2.0);

        spectrum.setPrecursor(Peak.noIntensity(250., 2));
        assertEquals(5, spectrum.size());

        MsnSpectrum copy = spectrum.copy(new RemoveAllPeakProcessor<>());
        assertEquals(0, copy.size());

        assertEquals(copy.getPrecursor().getCharge(), 2);
        assertEquals(copy.getPrecursor().getMz(), 250., 0.01);
    }
}