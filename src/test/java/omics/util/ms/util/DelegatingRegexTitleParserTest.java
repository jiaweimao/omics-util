package omics.util.ms.util;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumId;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2018, 2:28 PM
 */
class DelegatingRegexTitleParserTest
{
    @Test
    void testMsconvert()
    {
        String title = "b1937_293T_proteinID_01B_QE3_122212.1881.1881.3";
        DelegatingRegexTitleParser titleParser = new DelegatingRegexTitleParser();
        SpectrumId.Builder builder = new SpectrumId.Builder();
        MsnSpectrum spectrum = new MsnSpectrum();
        titleParser.parse(title, spectrum);
        assertEquals(spectrum.getScanNumber().getValue(), 1881);
    }
}