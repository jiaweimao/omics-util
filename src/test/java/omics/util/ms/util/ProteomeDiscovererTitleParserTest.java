package omics.util.ms.util;

import omics.util.ms.MsnSpectrum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 15 Nov 2017, 11:45 AM
 */
class ProteomeDiscovererTitleParserTest
{
    @Test
    void testParseTitle()
    {
        String title = "TITLE=File4506 Spectrum1 scans: 475";
        MsnSpectrum spectrum = new MsnSpectrum();
        ProteomeDiscovererTitleParser parser = new ProteomeDiscovererTitleParser();
        parser.parse(title, spectrum);
        assertEquals(475, spectrum.getScanNumber().getValue());
    }
}