package omics.util.ms.util;


import org.junit.jupiter.api.Test;

import java.util.OptionalInt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Sep 2018, 4:39 PM
 */
class ScanTitleParserTest
{
    @Test
    void testScan()
    {
        ScanTitleParser parser = new ScanTitleParser();
        String title = "b1937_293T_proteinID_01B_QE3_122212.1881.1881.3";
        OptionalInt scan = parser.getScan(title);
        assertTrue(scan.isPresent());
        assertEquals(scan.getAsInt(), 1881);
    }
}