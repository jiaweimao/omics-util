package omics.util.protein;

import omics.util.chem.Composition;
import omics.util.protein.mod.PTM;
import omics.util.protein.mod.Pos;
import omics.util.protein.ms.FragmentType;
import omics.util.protein.ms.Ion;
import omics.util.protein.ms.PeptideIon;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Aug 2019, 2:20 PM
 */
class AminoAcidArrayTest
{
    @Test
    void testCtr()
    {
        String pep = "M(Oxidation)PEPTIDE";
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(PeptideMod.of(PTM.Oxidation(), AminoAcid.M, true));

        Peptide peptide = Peptide.parse(pep);
        AminoAcidArray aaa = new AminoAcidArray(peptide, aaSet);

        assertEquals(peptide.size(), aaa.size());
        assertEquals(aaa.getPeptideMass(), peptide.getMolecularMass(), 1E-4);

        AminoAcid symbol = aaa.getSymbol(0);
        assertEquals(symbol.getComposition(), new Composition(AminoAcid.M.getComposition(), PTM.Oxidation().getComposition()));
    }

    @Test
    void testCtrVary()
    {
        String pep = "(Acetyl)_MPEPT(Phospho)ID(Oxidation)E";
        List<PeptideMod> mods = new ArrayList<>();
        mods.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.D));
        mods.add(PeptideMod.of(PTM.Acetyl(), AminoAcid.X, Pos.PROTEIN_N_TERM));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(mods);

        Peptide parse = Peptide.parse(pep);
        AminoAcidArray aaa = new AminoAcidArray(parse, aaSet);
        assertEquals(aaa.toString(), pep);
    }

    @Test
    void testGetMzArray()
    {
        AminoAcidArray aaa = new AminoAcidArray("PEPTIDE", AminoAcidSet.getStandardAminoAcidSet());
        double[] massArray = aaa.getMassArray(FragmentType.FORWARD);
        assertEquals(massArray.length, 6);
        assertEquals(massArray[0], AminoAcid.P.getMolecularMass(), 1E-6);
        assertEquals(massArray[5], Peptide.parse("PEPTID").getMolecularMass() - Composition.H2O.getMolecularMass(), 1E-6);
        double[] mzArray = aaa.getMzArray(PeptideIon.b(1));
        System.out.println(Arrays.toString(mzArray));
    }

    @Test
    void internalMassArray()
    {
        AminoAcidArray aaa = new AminoAcidArray("SAMPLER", AminoAcidSet.getStandardAminoAcidSet());
        double[] massArray = aaa.getMassArray(FragmentType.INTERNAL);
        for (double v : massArray) {
            double mz = Ion.yb.getMz(v, 1);
            System.out.println(mz);
        }

    }
}