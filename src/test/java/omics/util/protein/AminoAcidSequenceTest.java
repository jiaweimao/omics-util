package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Sets;
import omics.util.chem.Sequence;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author JiaweiMao 2017.03.17
 * @since 1.0.0
 */
public class AminoAcidSequenceTest
{
    @Test
    void testGetSymbolIndexes()
    {
        AminoAcidSequence sequence = new MockAminoAcidSequence(new AminoAcid[]{AminoAcid.C, AminoAcid.E, AminoAcid.R,
                AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S, AminoAcid.A});
        assertArrayEquals(new int[]{6, 8}, sequence.getSymbolIndexes(AminoAcid.A));
    }

    @Test
    void testHasModifications()
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);


        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence modSeq = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);
        assertTrue(modSeq.hasModifications());

        AminoAcidSequence noModSeq = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        assertFalse(noModSeq.hasModifications());
    }

    @Test
    void testHasModificationAtIndex()
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence modSeq = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);
        assertTrue(modSeq.hasModificationAt(0));
        assertFalse(modSeq.hasModificationAt(1));
        assertTrue(modSeq.hasModificationAt(3));
        assertTrue(modSeq.hasModificationAt(7));
    }

    @Test
    void testHasModificationAtAttachment1() throws Exception
    {
        AminoAcidSequence seq = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        assertFalse(seq.hasModificationAt(ModAttachment.C_TERM));
        assertFalse(seq.hasModificationAt(EnumSet.allOf(ModAttachment.class)));
    }

    @Test
    void testHasModificationAtAttachment2() throws Exception
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();

        AminoAcidSequence seq = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);
        assertTrue(seq.hasModificationAt(ModAttachment.SIDE_CHAIN));
        assertFalse(seq.hasModificationAt(ModAttachment.N_TERM));
        assertFalse(seq.hasModificationAt(ModAttachment.C_TERM));
        assertFalse(seq.hasModificationAt(EnumSet.of(ModAttachment.N_TERM, ModAttachment.C_TERM)));
        assertTrue(seq.hasModificationAt(EnumSet.of(ModAttachment.SIDE_CHAIN, ModAttachment.C_TERM)));
    }

    @Test
    void testHasModificationAtAttachment3()
    {
        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence seq = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);
        assertFalse(seq.hasModificationAt(ModAttachment.SIDE_CHAIN));
        assertTrue(seq.hasModificationAt(ModAttachment.N_TERM));
        assertTrue(seq.hasModificationAt(ModAttachment.C_TERM));
        assertTrue(seq.hasModificationAt(EnumSet.of(ModAttachment.N_TERM, ModAttachment.C_TERM)));
        assertTrue(seq.hasModificationAt(EnumSet.of(ModAttachment.SIDE_CHAIN, ModAttachment.C_TERM)));
    }

    @Test
    void testHasAmbiguousAA()
    {
        assertFalse(new MockAminoAcidSequence(new AminoAcid[]
                {AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S, AminoAcid.A}).hasAmbiguousAminoAcids());
        assertTrue(new MockAminoAcidSequence(new AminoAcid[]
                {AminoAcid.X, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S, AminoAcid.A}).hasAmbiguousAminoAcids());
    }

    @Test
    void testGetModificationsAt()
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);

        assertEquals(Arrays.asList(nTerm1, nTerm2), sequence.getModificationsAt(0, EnumSet.of(ModAttachment.N_TERM)));
        assertEquals(Arrays.asList(mod0, nTerm1, nTerm2),
                sequence.getModificationsAt(0, EnumSet.allOf(ModAttachment.class)));
        assertEquals(Arrays.asList(mod0), sequence.getModificationsAt(0, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertEquals(Collections.<Modification>emptyList(),
                sequence.getModificationsAt(3, EnumSet.of(ModAttachment.N_TERM)));
        assertEquals(Arrays.asList(mod3_1, mod3_2),
                sequence.getModificationsAt(3, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertEquals(Arrays.asList(mod5), sequence.getModificationsAt(5, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertEquals(Arrays.asList(cTerm), sequence.getModificationsAt(7, EnumSet.of(ModAttachment.C_TERM)));
    }

    @Test
    void testGetModificationsForAttachment()
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);

        assertEquals(Arrays.asList(nTerm1, nTerm2), sequence.getModifications(EnumSet.of(ModAttachment.N_TERM)));
        assertEquals(Arrays.asList(nTerm1, nTerm2, cTerm, mod0, mod3_1, mod3_2, mod5),
                sequence.getModifications(EnumSet.allOf(ModAttachment.class)));
        assertEquals(Arrays.asList(mod0, mod3_1, mod3_2, mod5),
                sequence.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertEquals(Arrays.asList(cTerm), sequence.getModifications(EnumSet.of(ModAttachment.C_TERM)));
        assertEquals(Arrays.asList(nTerm1, nTerm2, cTerm), sequence.getModifications(ModAttachment.termSet));
    }

    @Test
    void testHasModificationsAt()
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);

        assertTrue(sequence.hasModificationAt(0, EnumSet.of(ModAttachment.N_TERM)));
        assertFalse(sequence.hasModificationAt(0, EnumSet.of(ModAttachment.C_TERM)));
        assertTrue(sequence.hasModificationAt(0, EnumSet.allOf(ModAttachment.class)));
        assertTrue(sequence.hasModificationAt(0, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertFalse(sequence.hasModificationAt(1, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertFalse(sequence.hasModificationAt(3, EnumSet.of(ModAttachment.N_TERM)));
        assertTrue(sequence.hasModificationAt(3, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertTrue(sequence.hasModificationAt(5, EnumSet.of(ModAttachment.SIDE_CHAIN)));
        assertTrue(sequence.hasModificationAt(7, EnumSet.of(ModAttachment.C_TERM)));
        assertFalse(sequence.hasModificationAt(7, EnumSet.of(ModAttachment.SIDE_CHAIN)));
    }

    @Test
    void testModificationIndexes()
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);
        final Modification mod3_2 = mockMod("mod3_2", 32);
        final Modification mod5 = mockMod("mod5", 5);

        final Modification nTerm1 = mockMod("n_term_1", 56);
        final Modification nTerm2 = mockMod("n_term_2", 52);
        final Modification cTerm = mockMod("c_term", .87);

        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, mod0);
        sidechainModMap.put(3, mod3_1);
        sidechainModMap.put(3, mod3_2);
        sidechainModMap.put(5, mod5);

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, nTerm1);
        termModMap.put(ModAttachment.N_TERM, nTerm2);
        termModMap.put(ModAttachment.C_TERM, cTerm);

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);

        assertArrayEquals(new int[]{0, 3, 5, 7}, sequence.getModificationIndexes(ModAttachment.all));
        assertArrayEquals(new int[]{0}, sequence.getModificationIndexes(ModAttachment.nTermSet));
        assertArrayEquals(new int[]{7}, sequence.getModificationIndexes(ModAttachment.cTermSet));
    }

    @Test
    void testCalculateMonomerMassSum() throws Exception
    {
        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, Modification.parseModification("HPO3"));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, Modification.parseModification("CH3"));
        termModMap.put(ModAttachment.C_TERM, Modification.parseModification("O"));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);

        assertEquals(982.443293748, sequence.calculateMonomerMassSum(), 0.000001);
    }

    @Test
    void testCalculateMonomerMassDefect() throws Exception
    {
        ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
        sidechainModMap.put(0, Modification.parseModification("HPO3"));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, Modification.parseModification("CH3"));
        termModMap.put(ModAttachment.C_TERM, Modification.parseModification("O"));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                termModMap);

        assertEquals(0.4432935129999773, sequence.calculateMonomerMassDefect(), 0.000001);
    }

    @Test
    void testCountAminoAcidsIn()
    {
        AminoAcidSequence sequence = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S, AminoAcid.S, AminoAcid.S);

        assertEquals(1, sequence.countAminoAcidsIn(Sets.newHashSet(AminoAcid.E)));
        assertEquals(2, sequence.countAminoAcidsIn(Sets.newHashSet(AminoAcid.R, AminoAcid.V)));
        assertEquals(0, sequence.countAminoAcidsIn(Sets.newHashSet(AminoAcid.J)));
        assertEquals(3, sequence.countAminoAcidsIn(Sets.newHashSet(AminoAcid.S)));
    }

    @Test
    void testIsEmpty()
    {
        assertFalse(new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S, AminoAcid.S, AminoAcid.S).isEmpty());
    }

    @Test
    void testModListGetMolecularMass()
    {
        final Modification mod0 = mockMod("mod0", 40);
        final Modification mod3_1 = mockMod("mod3_1", 31);

        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mod0);
        sideChainModMap.put(3, mod3_1);
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);

        assertEquals(216.87, sequence.getModifications(ModAttachment.all).getMolecularMass(), 0.00000001);
    }

    @Test
    void testToString()
    {
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);

        assertEquals("(n_term_1, n_term_2)_C(mod0)ERV(mod3_1, mod3_2)IL(mod5)AS_(c_term)", sequence.toString());
        assertEquals("CERVILAS", sequence.toSymbolString());
    }

    @Test
    void testHasSameSequence()
    {
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence1 = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);
        AminoAcidSequence sequence2 = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        AminoAcidSequence sequence3 = new MockAminoAcidSequence(Arrays.asList(AminoAcid.R, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);

        assertTrue(sequence1.hasSameSequence(sequence2));
        assertTrue(sequence2.hasSameSequence(sequence1));
        assertFalse(sequence1.hasSameSequence(sequence3));
        assertFalse(sequence3.hasSameSequence(sequence1));
    }

    @Test
    void testHasSameSymbolSequence()
    {
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence1 = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);
        Sequence<AminoAcid> sequence2 = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        Sequence<AminoAcid> sequence3 = new MockAminoAcidSequence(Arrays.asList(AminoAcid.R, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                sideChainModMap, termModMap);

        assertTrue(sequence1.hasSameSequence(sequence2));
        assertFalse(sequence1.hasSameSequence(sequence3));
    }

    @Test
    public void testEquals() throws Exception
    {
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence1 = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);
        AminoAcidSequence sequence2 = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        AminoAcidSequence sequence3 = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);

        assertEquals(false, sequence1.equals(sequence2));
        assertEquals(false, sequence2.equals(sequence1));
        assertEquals(true, sequence1.equals(sequence3));
        assertEquals(true, sequence3.equals(sequence1));
        assertEquals(sequence1.hashCode(), sequence3.hashCode());
    }

    @Test
    public void testGetModCount() throws Exception
    {
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap,
                termModMap);

        assertEquals(7, sequence.getModificationCount());
        assertEquals(2, sequence.getModificationCount(ModAttachment.N_TERM));
        assertEquals(1, sequence.getModificationCount(ModAttachment.C_TERM));
        assertEquals(4, sequence.getModificationCount(ModAttachment.SIDE_CHAIN));

        sequence = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        assertEquals(0, sequence.getModificationCount());
        assertEquals(0, sequence.getModificationCount(ModAttachment.N_TERM));
        assertEquals(0, sequence.getModificationCount(ModAttachment.C_TERM));
        assertEquals(0, sequence.getModificationCount(ModAttachment.SIDE_CHAIN));
    }

    @Test
    public void testGetAmbiguousAminoAcids() throws Exception
    {
        MockAminoAcidSequence sequence = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.X, AminoAcid.B, AminoAcid.Z, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        assertEquals(Sets.newHashSet(AminoAcid.X, AminoAcid.B, AminoAcid.Z), sequence.getAmbiguousAminoAcids());

        sequence = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        assertEquals(Collections.<AminoAcid>emptySet(), sequence.getAmbiguousAminoAcids());
    }

    @Test
    public void testGetModifiedResidueCount()
    {
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(0, mockMod("mod0", 40));
        sideChainModMap.put(3, mockMod("mod3_1", 31));
        sideChainModMap.put(3, mockMod("mod3_2", 32));
        sideChainModMap.put(5, mockMod("mod5", 5));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
        termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
        termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

        AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V,
                AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sideChainModMap, termModMap);
        assertEquals(4, sequence.getModifiedResidueCount());

        sequence = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
        assertEquals(0, sequence.getModifiedResidueCount());

        sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), ArrayListMultimap.create(),
                termModMap);
        assertEquals(sequence.getModifiedResidueCount(), 2);
    }

    private Modification mockMod(String label, double mw)
    {
        Modification mod = mock(Modification.class);
        when(mod.getTitle()).thenReturn(label);
        when(mod.getMolecularMass()).thenReturn(mw);
        when(mod.toString()).thenReturn(label);

        return mod;
    }

    private static class MockAminoAcidSequence extends AminoAcidSequence
    {
        private MockAminoAcidSequence(AminoAcid firstResidue, AminoAcid... residues)
        {
            super(firstResidue, residues);
        }

        private MockAminoAcidSequence(AminoAcid[] residues)
        {
            super(residues);
        }

        private MockAminoAcidSequence(AminoAcidSequence src, int beginIndex, int endIndex)
        {
            super(src, beginIndex, endIndex);
        }

        private MockAminoAcidSequence(List<AminoAcid> residues, ListMultimap<Integer, Modification> sideChainModMap,
                ListMultimap<ModAttachment, Modification> termModMap)
        {
            super(residues, sideChainModMap, termModMap);
        }
    }

    public class TestConstructor
    {
        @Test
        void testArrayConstructor() throws Exception
        {
            AminoAcidSequence sequence = new MockAminoAcidSequence(new AminoAcid[]{AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S});
            assertEquals(AminoAcid.C, sequence.getSymbol(0));
            assertEquals(AminoAcid.E, sequence.getSymbol(1));
            assertEquals(AminoAcid.R, sequence.getSymbol(2));
            assertEquals(AminoAcid.V, sequence.getSymbol(3));
            assertEquals(AminoAcid.I, sequence.getSymbol(4));
            assertEquals(AminoAcid.L, sequence.getSymbol(5));
            assertEquals(AminoAcid.A, sequence.getSymbol(6));
            assertEquals(AminoAcid.S, sequence.getSymbol(7));
        }

        @Test
        void testVarargConstructor() throws Exception
        {
            AminoAcidSequence sequence = new MockAminoAcidSequence(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid
                    .V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S);
            assertEquals(AminoAcid.C, sequence.getSymbol(0));
            assertEquals(AminoAcid.E, sequence.getSymbol(1));
            assertEquals(AminoAcid.R, sequence.getSymbol(2));
            assertEquals(AminoAcid.V, sequence.getSymbol(3));
            assertEquals(AminoAcid.I, sequence.getSymbol(4));
            assertEquals(AminoAcid.L, sequence.getSymbol(5));
            assertEquals(AminoAcid.A, sequence.getSymbol(6));
            assertEquals(AminoAcid.S, sequence.getSymbol(7));
        }

        @Test
        void testCopyConstructor1() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);
            final Modification mod3_2 = mockMod("mod3_2", 32);
            final Modification mod5 = mockMod("mod5", 5);

            final Modification nTerm1 = mockMod("n_term_1", 56);
            final Modification nTerm2 = mockMod("n_term_2", 52);
            final Modification cTerm = mockMod("c_term", .87);

            ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
            sidechainModMap.put(0, mod0);
            sidechainModMap.put(3, mod3_1);
            sidechainModMap.put(3, mod3_2);
            sidechainModMap.put(5, mod5);

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, nTerm1);
            termModMap.put(ModAttachment.N_TERM, nTerm2);
            termModMap.put(ModAttachment.C_TERM, cTerm);

            AminoAcidSequence src = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                    termModMap);
            AminoAcidSequence dest = new MockAminoAcidSequence(src, 0, 8);

            assertEquals(8, dest.size());
            assertEquals(Arrays.asList(nTerm1, nTerm2), dest.getModifications(ModAttachment.nTermSet));
            assertEquals(Arrays.asList(cTerm), dest.getModifications(ModAttachment.cTermSet));
            assertEquals(Arrays.asList(mod0), dest.getModificationsAt(0, ModAttachment.sideChainSet));
            assertEquals(Arrays.asList(mod3_1, mod3_2), dest.getModificationsAt(3, ModAttachment.sideChainSet));
            assertEquals(Arrays.asList(mod5), dest.getModificationsAt(5, ModAttachment.sideChainSet));
        }

        @Test
        void testCopyConstructor2() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);
            final Modification mod3_2 = mockMod("mod3_2", 32);
            final Modification mod5 = mockMod("mod5", 5);

            final Modification nTerm1 = mockMod("n_term_1", 56);
            final Modification nTerm2 = mockMod("n_term_2", 52);
            final Modification cTerm = mockMod("c_term", .87);

            ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
            sidechainModMap.put(0, mod0);
            sidechainModMap.put(3, mod3_1);
            sidechainModMap.put(3, mod3_2);
            sidechainModMap.put(5, mod5);

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, nTerm1);
            termModMap.put(ModAttachment.N_TERM, nTerm2);
            termModMap.put(ModAttachment.C_TERM, cTerm);

            AminoAcidSequence src = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                    termModMap);
            AminoAcidSequence dest = new MockAminoAcidSequence(src, 1, 8);

            assertEquals(7, dest.size());
            assertEquals(Collections.<Modification>emptyList(), dest.getModifications(ModAttachment.nTermSet));
            assertEquals(Collections.singletonList(cTerm), dest.getModifications(ModAttachment.cTermSet));
            assertEquals(Collections.<Modification>emptyList(), dest.getModificationsAt(0, ModAttachment.sideChainSet));
            assertEquals(Arrays.asList(mod3_1, mod3_2), dest.getModificationsAt(2, ModAttachment.sideChainSet));
            assertEquals(Collections.singletonList(mod5), dest.getModificationsAt(4, ModAttachment.sideChainSet));
        }

        /**
         * Tests that the lazy initialization of the modMaps is the same for the
         * ListMultimap and copy constructor
         *
         * @throws Exception because it is a test
         */
        @Test
        void testCopyConstructor3() throws Exception
        {
            ListMultimap<Integer, Modification> sidechainModMap = ArrayListMultimap.create();
            sidechainModMap.put(0, mockMod("mod0", 40));
            sidechainModMap.put(3, mockMod("mod3_1", 31));
            sidechainModMap.put(3, mockMod("mod3_2", 32));
            sidechainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));

            AminoAcidSequence src = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S), sidechainModMap,
                    termModMap);
            AminoAcidSequence dest = new MockAminoAcidSequence(src, 7, 8);

            assertEquals(new MockAminoAcidSequence(AminoAcid.S), dest);
        }
    }

    public class ModificationListTest
    {
        @Test
        void testModListImmutable1() throws Exception
        {
            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mockMod("mod0", 40));
            sideChainModMap.put(3, mockMod("mod3_1", 31));
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () ->
                    sequence.getModifications(EnumSet.allOf(ModAttachment.class)).clear());
        }

        @Test
        void testModListImmutable2() throws Exception
        {
            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mockMod("mod0", 40));
            sideChainModMap.put(3, mockMod("mod3_1", 31));
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).add(mockMod("unused", 98)));
        }

        @Test
        void testModListImmutable3() throws Exception
        {
            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mockMod("mod0", 40));
            sideChainModMap.put(3, mockMod("mod3_1", 31));
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> {
                final Iterator<Modification> iterator = sequence.getModifications(EnumSet.allOf(ModAttachment.class))
                        .iterator();
                iterator.next();
                iterator.remove();
            });
        }

        @Test
        void testModListImmutable4() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mockMod("mod3_1", 31));
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).remove(mod0));
        }

        @Test
        void testModListImmutable5() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).addAll(Arrays.asList(mod0, mod3_1)));
        }

        @Test
        void testModListImmutable6() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).addAll(3, Arrays.asList(mod0, mod3_1)));
        }

        @Test
        void testModListImmutable7() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).removeAll(Arrays.asList(mod0, mod3_1)));
        }

        @Test
        void testModListImmutable8() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).retainAll(Arrays.asList(mod0, mod3_1)));
        }

        @Test
        void testModListImmutable9() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).set(1, mod0));
        }

        @Test
        void testModListImmutable10() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment
                            .class)).add(1, mod0));
        }

        @Test
        void testModListImmutable11() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment.class)).remove(1));
        }

        @Test
        void testModListImmutable12() throws Exception
        {
            final Modification mod0 = mockMod("mod0", 40);
            final Modification mod3_1 = mockMod("mod3_1", 31);

            ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
            sideChainModMap.put(0, mod0);
            sideChainModMap.put(3, mod3_1);
            sideChainModMap.put(3, mockMod("mod3_2", 32));
            sideChainModMap.put(5, mockMod("mod5", 5));

            ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_1", 56));
            termModMap.put(ModAttachment.N_TERM, mockMod("n_term_2", 52));
            termModMap.put(ModAttachment.C_TERM, mockMod("c_term", .87));

            AminoAcidSequence sequence = new MockAminoAcidSequence(Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.I, AminoAcid.L, AminoAcid.A, AminoAcid.S),
                    sideChainModMap,
                    termModMap);

            assertThrows(UnsupportedOperationException.class, () -> sequence.getModifications(EnumSet.allOf
                    (ModAttachment.class)).subList(1, 3));
        }
    }
}