package omics.util.protein;

import it.unimi.dsi.fastutil.chars.Char2IntMap;
import it.unimi.dsi.fastutil.chars.Char2IntOpenHashMap;
import it.unimi.dsi.fastutil.chars.CharComparators;
import it.unimi.dsi.fastutil.chars.CharList;
import omics.util.chem.Composition;
import omics.util.io.FileUtils;
import omics.util.protein.database.FastaSequence;
import omics.util.protein.database.Protein;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.PTM;
import omics.util.protein.mod.PTMFactory;
import omics.util.protein.mod.Pos;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jun 2019, 9:46 PM
 */
class AminoAcidSetTest
{
    private static AminoAcidSet stdSet;
    private static AminoAcidSet carbamiSet;
    private static AminoAcidSet phosSet;
    private static AminoAcidSet oglycanSet;

    @BeforeAll
    static void setUp()
    {
        stdSet = AminoAcidSet.getStandardAminoAcidSet();
        carbamiSet = AminoAcidSet.getCarbamidomethylCysAminoAcidSet();

        List<PeptideMod> mods = new ArrayList<>();

        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));
        mods.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.M));
        mods.add(PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C, true));
        phosSet = AminoAcidSet.getAminoAcidSet(mods);

        List<PeptideMod> mods2 = new ArrayList<>();
        mods2.add(PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C, true));
        mods2.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.M));
        mods2.add(PeptideMod.of(PTM.Deamidated(), AminoAcid.N));
        mods2.add(PeptideMod.of(PTM.Deamidated(), AminoAcid.Q));
        oglycanSet = AminoAcidSet.getAminoAcidSet(mods2);
    }

    @Test
    void testRegisterEnzyme()
    {
        FastaSequence fastaSequence = new FastaSequence("C:\\Users\\Chen\\IdeaProjects\\data\\homo_20191210.fasta");
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSet();
        aaSet.setAminoAcidProbabilities(fastaSequence);

        Enzyme trypsin = Enzyme.ofName("Trypsin");
        Enzyme op = Enzyme.ofName("OpeRATOR");
        double[] values = new double[]{0.9, 0.99, 0.999, 0.9999, 0.99999, 0.999999};
        for (double value : values) {
            trypsin.setCleavageEfficiency(value);
            trypsin.setNeighborCleavageEfficiency(value);
            op.setCleavageEfficiency(value);
            op.setNeighborCleavageEfficiency(value);
            aaSet.registerEnzyme(trypsin, op);
            System.out.println(aaSet.getEnzymeCutCredit() + "\t" + aaSet.getEnzymeCutPenalty() + "\t" + aaSet.getNeighborCutCredit() + "\t" + aaSet.getNeighborCutPenalty());
            aaSet.registerEnzyme(trypsin);
            System.out.println(aaSet.getEnzymeCutCredit() + "\t" + aaSet.getEnzymeCutPenalty() + "\t" + aaSet.getNeighborCutCredit() + "\t" + aaSet.getNeighborCutPenalty());

        }
    }

    @Test
    public void testSetProbabilities() throws IOException
    {
        Path path = FileUtils.getResourcePath("test.fasta");

        int N = 0;
        Char2IntMap map = new Char2IntOpenHashMap();
        FastaReader reader = new FastaReader(path);
        while (reader.hasNext()) {
            Protein protein = reader.next();
            String sequence = protein.getSequence();
            N += sequence.length();
            for (char c : sequence.toCharArray()) {
                map.put(c, map.getOrDefault(c, 0) + 1);
            }
        }
        reader.close();

        FastaSequence fastaSequence = new FastaSequence(path.toString());
        AminoAcidSet stdAASet = AminoAcidSet.getStandardAminoAcidSet();
        stdAASet.setAminoAcidProbabilities(fastaSequence);
        AminoAcid[] aminoAcids = stdAASet.getAminoAcids();
        for (AminoAcid aminoAcid : aminoAcids) {
            assertEquals(aminoAcid.getProbability(), (double) map.get(aminoAcid.getOneLetterCode()) / N, 1E-6);
        }
        fastaSequence.clear();
    }

    @Test
    public void testStd()
    {
        AminoAcidSet aaSet = AminoAcidSet.getCarbamidomethylCysAminoAcidSet();
        aaSet.printAASet();
    }

    @Test
    public void testGetMaxResidue()
    {
        char maxResidue = stdSet.getMaxResidue();
    }


    @Test
    public void testFixedModFile()
    {
        AminoAcidArray aaa = new AminoAcidArray("IPSCDSSPVSTEQLAPTAPPELTPVVQDCYHGDGQSYR", oglycanSet);
        Peptide peptide = AminoAcidArray.getPeptide("IPSCDSSPVSTEQLAPTAPPELTPVVQDCYHGDGQSYR", oglycanSet);
        System.out.println(aaa.getMolecularMass());
        System.out.println(aaa.getPeptideMass());
        System.out.println(peptide.getMolecularMass());
    }

    @Test
    public void testGetAminoAcidSetFromMods()
    {
        List<PeptideMod> mods = new ArrayList<>();
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));
        mods.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.M, true));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(mods);
        assertEquals(aaSet.size(), 23);
        assertTrue(aaSet.contains('S'));
        assertTrue(aaSet.contains('s'));
        assertTrue(aaSet.contains('t'));
        assertTrue(aaSet.contains('y'));
        assertTrue(aaSet.contains('M'));
        assertFalse(aaSet.contains('m'));

        CharList residueList = aaSet.getResidueList();
        assertArrayEquals(residueList.toCharArray(), new char[]{'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y', 's', 't', 'y'});
    }

    @Test
    public void testNTermMod()
    {
        List<PeptideMod> mods = new ArrayList<>();
//        mods.add(new PeptideMod(PTM.Oxidation, AminoAcid.N, Pos.PROTEIN_N_TERM, true));
//        mods.add(new PeptideMod(PTM.Acetyl, AminoAcid.N));
        mods.add(PeptideMod.of(PTM.Acetyl(), AminoAcid.N, Pos.ANY_N_TERM, true));
        mods.add(PeptideMod.of(PTM.Acetyl(), AminoAcid.N, Pos.ANY_C_TERM, true));
        mods.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.N, Pos.ANY_WHERE));
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(mods);
        AminoAcid[] ns = aaSet.getAminoAcids(Pos.PROTEIN_N_TERM, 'N');
        System.out.println(ns.length);
        System.out.println(ns[0]);
        System.out.println(ns[1]);

        System.out.println(aaSet.getResidueList());
        System.out.println(Arrays.toString(aaSet.getAminoAcids()));

//        assertTrue(aaSet.contains('N'));
//        assertTrue(aaSet.contains('n'));
//        System.out.println(aaSet.getAAList(Pos.PROTEIN_N_TERM));
    }

    @Test
    public void testGetResidueListWithoutMods()
    {
        List<PeptideMod> mods = new ArrayList<>();
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));
        mods.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.M, true));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(mods);
        CharList residueListWithoutMods = aaSet.getResiduesWithoutMods();
        assertEquals(residueListWithoutMods.size(), 20, "Modified Amino acid do not affect this");

        AminoAcid[] aminoAcids = aaSet.getAminoAcids(Pos.ANY_WHERE, 'S');
        assertEquals(aminoAcids.length, 2);
    }

    @Test
    public void testgetModifiedResidue()
    {
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSet();
        char s = aaSet.getModifiedResidue('S');
        assertEquals(s, 's');
        assertNotEquals('S', aaSet.getModifiedResidue('S'));
    }

    @Test
    public void testGetNeighboringAACleavageCredit()
    {
        assertEquals(stdSet.getNeighborCutCredit(), 0, 1E-6);
    }

    @Test
    public void testTrypsinCredit()
    {
        AminoAcidSet aaSet = AminoAcidSet.getCarbamidomethylCysAminoAcidSet();
        aaSet.registerEnzyme(Enzyme.ofName("Trypsin/P"));
        System.out.println("PeptideCleavageCredit: " + aaSet.getEnzymeCutCredit());
        System.out.println("PeptideCleavagePenalty: " + aaSet.getEnzymeCutPenalty());
        System.out.println("NeighborCredit: " + aaSet.getNeighborCutCredit());
        System.out.println("NeighborPenalty: " + aaSet.getNeighborCutPenalty());
    }

    @Test
    public void testGetResidueList()
    {
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSet();

        List<Character> stdAAs = Arrays.asList('A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y');

        CharList residueList = aaSet.getResidueList();
        List<Character> aas = new ArrayList<>(stdAAs);
        aas.sort(Comparator.naturalOrder());

        assertEquals(residueList, aas);

        aaSet = AminoAcidSet.getCarbamidomethylCysAminoAcidSet();
        assertEquals(aaSet.getResidueList(), stdAAs);

        // with a M oxidation
        aaSet = AminoAcidSet.getAminoAcidSet(Collections.singletonList(PeptideMod.of(PTM.Oxidation(), AminoAcid.M)));
        residueList = aaSet.getResidueList();
        residueList.sort(CharComparators.NATURAL_COMPARATOR);

        aas.add('m');
        aas.sort(Comparator.naturalOrder());
        assertEquals(aas, residueList);

        // Cys fixed and phos var
        List<PeptideMod> mods = new ArrayList<>();
        mods.add(PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C, true));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        mods.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));

        aaSet = AminoAcidSet.getAminoAcidSet(mods);
        residueList = aaSet.getResidueList();
        residueList.sort(CharComparators.NATURAL_COMPARATOR);

        aas = new ArrayList<>(stdAAs);
        aas.add('s');
        aas.add('t');
        aas.add('y');

        assertEquals(aas, residueList);
    }

    @Test
    public void testContains()
    {
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSet();
        assertTrue(aaSet.contains('A'));
        assertTrue(aaSet.contains('C'));
        assertTrue(aaSet.contains('D'));
        assertTrue(aaSet.contains('E'));
        assertTrue(aaSet.contains('F'));
        assertTrue(aaSet.contains('G'));
        assertTrue(aaSet.contains('H'));
        assertTrue(aaSet.contains('I'));
        assertTrue(aaSet.contains('K'));
        assertTrue(aaSet.contains('L'));
        assertTrue(aaSet.contains('M'));
        assertTrue(aaSet.contains('N'));
        assertTrue(aaSet.contains('P'));
        assertTrue(aaSet.contains('Q'));
        assertTrue(aaSet.contains('R'));
        assertTrue(aaSet.contains('S'));
        assertTrue(aaSet.contains('T'));
        assertTrue(aaSet.contains('V'));
        assertTrue(aaSet.contains('W'));
        assertTrue(aaSet.contains('Y'));

        assertFalse(aaSet.contains('m'));

        PTMFactory factory = PTMFactory.getInstance();
        List<PeptideMod> modList = new ArrayList<>();
        modList.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        aaSet = AminoAcidSet.getAminoAcidSet(modList);
        assertTrue(aaSet.contains('s'));
    }

    @Test
    public void testGetAminoAcidSet()
    {
        ArrayList<PeptideMod> mods = new ArrayList<>();
        mods.add(PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(mods);
        assertTrue(aaSet.contains('c'));
    }

    @Test
    public void testContainsCTermMod()
    {
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSet();
        assertFalse(aaSet.hasCTermMod());

        aaSet = AminoAcidSet.getAminoAcidSet(PeptideMod.of(PTM.Acetyl(), AminoAcid.X, Pos.PROTEIN_C_TERM));
        assertTrue(aaSet.hasCTermMod());
    }

    @Test
    public void testGetAAList()
    {
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(PeptideMod.of(PTM.Oxidation(), AminoAcid.M));

        List<AminoAcid> aaList = aaSet.getAAList(Pos.ANY_WHERE);
        assertEquals(aaList.size(), 21);

        AminoAcid aa = aaSet.getAminoAcid('m');
        assertTrue(aa.isModified());
        assertEquals(aa.getStandardResidue(), 'M');
        assertEquals(aa.getOneLetterCode(), 'm');
        assertTrue(aa.isResidueModified());
        assertFalse(aa.isTerminalModified());
        assertEquals(aa.getSymbol(), "M(Oxidation)");

        aaList = aaSet.getAAList(Pos.ANY_N_TERM);
        assertEquals(aaList.size(), 21);
    }

    @Test
    public void testGetAAListFixMod()
    {
        List<PeptideMod> modList = new ArrayList<>();
        modList.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        modList.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        modList.add(PeptideMod.of(PTM.Acetyl(), AminoAcid.X, Pos.PROTEIN_N_TERM, true));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(modList);
        List<AminoAcid> aaList = aaSet.getAAList(Pos.ANY_C_TERM);
        assertEquals(aaList.size(), 22);

        AminoAcid aa = aaSet.getAminoAcid('s');
        assertFalse(aa.isModified()); // fixed modification is not treated as fixed.

        ModifiedAminoAcid modAA = (ModifiedAminoAcid) aa;
        assertTrue(modAA.isFixed());
        Modification mod = modAA.getModification();
        assertEquals(mod, PTM.Acetyl());

        AminoAcid aminoAcid = aaSet.getAminoAcid(AminoAcid.S, PTM.Phospho());
        assertTrue(aminoAcid.isModified());

        aminoAcid = aaSet.getAminoAcid(AminoAcid.S, PTM.Acetyl());
        assertFalse(aminoAcid.isModified());
        assertEquals(aminoAcid.getOneLetterCode(), 's');
        assertEquals(aminoAcid.getSymbol(), "(Acetyl)_S");
    }

    @Test
    public void testFixedVary()
    {
        List<PeptideMod> modList = new ArrayList<>();
        modList.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        modList.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        modList.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.T, true));
        modList.add(PeptideMod.of(PTM.Acetyl(), AminoAcid.X, Pos.PROTEIN_N_TERM, true));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(modList);

        AminoAcid aminoAcid = aaSet.getAminoAcid(AminoAcid.T, PTM.Phospho());
        assertNull(aminoAcid);

        AminoAcid aa1 = aaSet.getAminoAcid(AminoAcid.T, PTM.Oxidation());
        AminoAcid aa2 = aaSet.getAminoAcid(aa1, PTM.Phospho());
        assertEquals(aa2.getSymbol(), "T(Phospho)");

        AminoAcid aa = aaSet.getAminoAcid(AminoAcid.T, PTM.Oxidation(), PTM.Phospho());
        assertEquals(aa.getSymbol(), "T(Phospho)");
        assertEquals(aa.getComposition(), new Composition(AminoAcid.T.getComposition(),
                PTM.Oxidation().getComposition(), PTM.Phospho().getComposition()));
    }

    @Test
    public void testGetAminoAcids()
    {
        AminoAcid[] s = phosSet.getAminoAcids(Pos.ANY_WHERE, 'S');
        assertEquals(s.length, 2);
        assertSame(s[0], AminoAcid.S);
        assertEquals(s[1].getMolecularMass(),
                AminoAcid.S.getMolecularMass() + PTM.Phospho().getMolecularMass(), 1E-6);

        List<PeptideMod> modList = new ArrayList<>();
        modList.add(PeptideMod.of(PTM.Acetyl(), AminoAcid.X, Pos.PROTEIN_N_TERM, true));
        modList.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.M));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(modList);
        AminoAcid[] ms = aaSet.getAminoAcids(Pos.PROTEIN_N_TERM, 'M');
        assertEquals(ms.length, 2);
    }

    @Test
    public void testGetMaxNominalMass()
    {
        assertEquals(stdSet.getMaxNominalMass(), 186);
        assertEquals(carbamiSet.getMaxNominalMass(), 186);
        assertEquals(phosSet.getMaxNominalMass(), new Composition(AminoAcid.Y.getComposition(),
                PTM.Phospho().getComposition()).getNominalMass());
    }


    @Test
    public void testGetHeaviestAA()
    {
        assertEquals(stdSet.getHeaviestAA().getMolecularMass(), AminoAcid.W.getMolecularMass(), 1E-6);
        assertEquals(carbamiSet.getHeaviestAA().getMolecularMass(), AminoAcid.W.getMolecularMass(), 1E-6);
        assertEquals(phosSet.getHeaviestAA().getMolecularMass(), AminoAcid.Y.getMolecularMass()
                + PTM.Phospho().getMolecularMass(), 1E-6);
        assertEquals(oglycanSet.getHeaviestAA().getMolecularMass(), AminoAcid.W.getMolecularMass(), 1E-6);
    }

    @Test
    public void testTestGetAminoAcid3()
    {
        AminoAcid aa = stdSet.getAminoAcid(0);
        assertSame(aa, AminoAcid.G);

        assertEquals(stdSet.size(), 20);
        assertSame(stdSet.getAminoAcid(19), AminoAcid.W);

        assertEquals(phosSet.size(), 24);
        aa = phosSet.getAminoAcid('s');
        assertTrue(aa.isModified());
        assertSame(aa.asModifiedAminoAcid().getTargetAminoAcid(), AminoAcid.S);
        assertEquals(aa.getMolecularMass(), AminoAcid.S.getMolecularMass()
                + PTM.Phospho().getMolecularMass(), 1E-6);

        aa = phosSet.getAminoAcid('m');
        assertEquals(aa.getMolecularMass(), AminoAcid.M.getMolecularMass()
                + PTM.Oxidation().getMolecularMass(), 1E-6);

        aa = phosSet.getAminoAcid('C');
        assertEquals(aa.getMolecularMass(), AminoAcid.C.getMolecularMass()
                + PTM.Carbamidomethyl().getMolecularMass(), 1E-6);
        assertSame(phosSet.getFixedModification('C').getFirst(), PTM.Carbamidomethyl());
    }

    @Test
    public void testGetModifications()
    {
        assertNull(stdSet.getModifications());

        List<PeptideMod> modList = carbamiSet.getModifications();
        assertEquals(modList.size(), 1);
        PeptideMod mod = modList.get(0);
        assertSame(mod.getModification(), PTM.Carbamidomethyl());
        assertSame(mod.getTargetAminoAcid(), AminoAcid.C);

        // Phospho()
        List<PeptideMod> modList2 = phosSet.getModifications();
        assertEquals(modList2.size(), 5);
        modList2.sort(Comparator.comparing(PeptideMod::getComposition));
        PeptideMod peptideMod = modList2.get(0);
        assertSame(peptideMod.getTargetAminoAcid(), AminoAcid.M);
        assertSame(peptideMod.getModification(), PTM.Oxidation());
        assertSame(peptideMod.getPosition(), Pos.ANY_WHERE);

        PeptideMod peptideMod1 = modList2.get(4);
        assertSame(peptideMod1.getModification(), PTM.Phospho());
        assertSame(peptideMod1.getPosition(), Pos.ANY_WHERE);
    }

    @Test
    public void testGetStandardAminoAcidSet()
    {
        AminoAcid[] aminoAcids = stdSet.getAminoAcids();
        for (AminoAcid aminoAcid : aminoAcids) {
            assertEquals(aminoAcid.getMolecularMass(), AminoAcid.getAminoAcid(aminoAcid.getOneLetterCode()).getMolecularMass(), 1E-6);
        }
    }

    @Test
    public void testGetStandardAminoAcidSetWithFixedCarbamidomethylatedCys()
    {
        AminoAcid[] aas = carbamiSet.getAminoAcids();
        for (AminoAcid aa : aas) {
            if (aa.getOneLetterCode() == 'C') {
                assertEquals(aa.getMolecularMass(), AminoAcid.C.getMolecularMass()
                        + PTM.Carbamidomethyl().getMolecularMass(), 1E-6);
            } else {
                assertEquals(aa.getMolecularMass(), AminoAcid.getAminoAcid(aa.getOneLetterCode()).getMolecularMass(), 1E-6);
            }
        }
    }
}