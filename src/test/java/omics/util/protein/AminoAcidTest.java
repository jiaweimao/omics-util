package omics.util.protein;

import com.google.common.collect.Sets;
import omics.util.chem.Composition;
import omics.util.chem.PeriodicTable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Aug 2019, 5:14 PM
 */
public class AminoAcidTest
{
    private static final double eps = 0.000001;

    private static Stream<Arguments> aminoAcidsSymbol()
    {
        return Stream.of(
                Arguments.of(AminoAcid.A, 'A'),
                Arguments.of(AminoAcid.R, 'R'),
                Arguments.of(AminoAcid.N, 'N'),
                Arguments.of(AminoAcid.D, 'D'),
                Arguments.of(AminoAcid.C, 'C'),
                Arguments.of(AminoAcid.E, 'E'),
                Arguments.of(AminoAcid.Q, 'Q'),
                Arguments.of(AminoAcid.G, 'G'),
                Arguments.of(AminoAcid.H, 'H'),
                Arguments.of(AminoAcid.K, 'K'),
                Arguments.of(AminoAcid.M, 'M'),
                Arguments.of(AminoAcid.F, 'F'),
                Arguments.of(AminoAcid.P, 'P'),
                Arguments.of(AminoAcid.S, 'S'),
                Arguments.of(AminoAcid.T, 'T'),
                Arguments.of(AminoAcid.W, 'W'),
                Arguments.of(AminoAcid.Y, 'Y'),
                Arguments.of(AminoAcid.V, 'V'),
                Arguments.of(AminoAcid.J, 'J'),
                Arguments.of(AminoAcid.I, 'I'),
                Arguments.of(AminoAcid.L, 'L'),
                Arguments.of(AminoAcid.O, 'O'),
                Arguments.of(AminoAcid.U, 'U'),
                Arguments.of(AminoAcid.X, 'X'),
                Arguments.of(AminoAcid.B, 'B'),
                Arguments.of(AminoAcid.Z, 'Z')
        );
    }

    @ParameterizedTest
    @MethodSource("aminoAcidsSymbol")
    void testValueOf(AminoAcid a, char aa)
    {
        assertEquals(a, AminoAcid.getAminoAcid(aa));
    }

    @Test
    void testAvgMass()
    {
        AminoAcid[] aminoAcids = AminoAcid.getStandardAminoAcids();
        for (AminoAcid aa : aminoAcids) {
            assertEquals(aa.getAverageMass(), aa.getComposition().getAverageMass(), 0.01);
        }
    }

    private static Stream<Arguments> massAAs()
    {
        return Stream.of(
                Arguments.of(57.021464, AminoAcid.G),
                Arguments.of(71.037114, AminoAcid.A),
                Arguments.of(57.021464, AminoAcid.G),
                Arguments.of(71.037114, AminoAcid.A),
                Arguments.of(87.032028, AminoAcid.S),
                Arguments.of(97.052764, AminoAcid.P),
                Arguments.of(99.068414, AminoAcid.V),
                Arguments.of(101.047679, AminoAcid.T),
                Arguments.of(103.009185, AminoAcid.C),
                Arguments.of(113.084064, AminoAcid.J),
                Arguments.of(114.042927, AminoAcid.N),
                Arguments.of(115.026943, AminoAcid.D),
                Arguments.of(128.058578, AminoAcid.Q),
                Arguments.of(128.094963, AminoAcid.K),
                Arguments.of(129.042593, AminoAcid.E),
                Arguments.of(131.040485, AminoAcid.M),
                Arguments.of(137.058912, AminoAcid.H),
                Arguments.of(147.068414, AminoAcid.F),
                Arguments.of(156.101111, AminoAcid.R),
                Arguments.of(163.063329, AminoAcid.Y),
                Arguments.of(186.079313, AminoAcid.W),
                Arguments.of(113.084064, AminoAcid.I),
                Arguments.of(113.084064, AminoAcid.L),
                Arguments.of(113.084064, AminoAcid.J)
        );
    }

    @ParameterizedTest
    @MethodSource("massAAs")
    void testComposition(double mass, AminoAcid aminoAcid)
    {
        assertEquals(mass, aminoAcid.getMolecularMass(), eps);
    }

    @Test
    public void testDefinedComposition()
    {
        Set<AminoAcid> unDefined = Sets.newHashSet(AminoAcid.X, AminoAcid.B, AminoAcid.Z, AminoAcid.J, AminoAcid.U, AminoAcid.O);
        Set<AminoAcid> defined = Sets.newHashSet(AminoAcid.getAllAminoAcids());
        defined.removeAll(unDefined);

        for (AminoAcid aa : defined) {
            assertTrue(aa.isUnambiguous());
        }

        for (AminoAcid aa : unDefined) {
            assertFalse(aa.isUnambiguous());
        }
    }


    @Test
    void testGetCompositionOnDefined()
    {
        assertEquals(AminoAcid.I.getComposition(), AminoAcid.L.getComposition());
    }

    @Test
    public void testisStandardAminoAcid()
    {
        assertTrue(AminoAcid.isStandardAminoAcid('A'));
        assertFalse(AminoAcid.isStandardAminoAcid('B'));

        assertTrue(AminoAcid.isStandardAminoAcid('C'));
        assertTrue(AminoAcid.isStandardAminoAcid('D'));
        assertTrue(AminoAcid.isStandardAminoAcid('E'));
        assertTrue(AminoAcid.isStandardAminoAcid('F'));
        assertTrue(AminoAcid.isStandardAminoAcid('G'));
        assertTrue(AminoAcid.isStandardAminoAcid('H'));
        assertTrue(AminoAcid.isStandardAminoAcid('I'));
        assertFalse(AminoAcid.isStandardAminoAcid('J'));

        assertTrue(AminoAcid.isStandardAminoAcid('K'));
        assertTrue(AminoAcid.isStandardAminoAcid('L'));
        assertTrue(AminoAcid.isStandardAminoAcid('M'));
        assertTrue(AminoAcid.isStandardAminoAcid('N'));
        assertFalse(AminoAcid.isStandardAminoAcid('O'));

        assertTrue(AminoAcid.isStandardAminoAcid('P'));
        assertTrue(AminoAcid.isStandardAminoAcid('N'));
        assertTrue(AminoAcid.isStandardAminoAcid('R'));
        assertTrue(AminoAcid.isStandardAminoAcid('S'));
        assertTrue(AminoAcid.isStandardAminoAcid('T'));
        assertFalse(AminoAcid.isStandardAminoAcid('U'));

        assertTrue(AminoAcid.isStandardAminoAcid('V'));
        assertTrue(AminoAcid.isStandardAminoAcid('W'));
        assertFalse(AminoAcid.isStandardAminoAcid('X'));
        assertTrue(AminoAcid.isStandardAminoAcid('Y'));
        assertFalse(AminoAcid.isStandardAminoAcid('Z'));
    }


    @Test
    public void testAAComposition()
    {
        Composition comp = AminoAcid.R.getComposition();

        assertEquals(6, comp.getCount(PeriodicTable.C));
        assertEquals(12, comp.getCount(PeriodicTable.H));
        assertEquals(4, comp.getCount(PeriodicTable.N));
        assertEquals(1, comp.getCount(PeriodicTable.O));
    }

    @Test
    void testUndefinedCompositionB()
    {
        assertThrows(NullPointerException.class, AminoAcid.B::getComposition);
    }

    @Test
    void testGetCompositionOnUndefined()
    {
        assertThrows(NullPointerException.class, AminoAcid.X::getComposition);
    }

    @Test
    public void testX()
    {
        Composition xComp = Composition.parseComposition("C2H6NO");
        System.out.println(xComp.getMolecularMass());
    }

    @Test
    void testAAUnknownComposition() throws Exception
    {
        assertThrows(NullPointerException.class, AminoAcid.Z::getComposition);
    }

}