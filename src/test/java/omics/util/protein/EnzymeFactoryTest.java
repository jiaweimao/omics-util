package omics.util.protein;


import omics.util.protein.digest.Enzyme;
import omics.util.protein.digest.EnzymeFactory;
import omics.util.protein.digest.Protease;
import org.junit.jupiter.api.Test;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class EnzymeFactoryTest
{
    @Test
    void get()
    {
        EnzymeFactory factory = EnzymeFactory.getInstance();
        assertEquals(factory.size(), 22);

        Enzyme trypsin = factory.get("trypsin");
        assertEquals("Trypsin", trypsin.getName());

        Peptide peptide = Peptide.parse("PEPTIDEKDEYSLKJGRRSG");

        int count = trypsin.countCleavageSites(peptide);
        assertEquals(count, 4);
    }

    @Test
    void testNoCleavage()
    {
        EnzymeFactory factory = EnzymeFactory.getInstance();
        Enzyme enzyme = factory.get("MS:1001955");
        assertEquals(enzyme.getName(), "no cleavage");

        Protease digester = new Protease(enzyme, 1);
        assertFalse(digester.isSemi());
        assertEquals(digester.getMaxMissedCleavage(), 1);
    }


    @Test
    void convert2json() throws IOException, XMLStreamException
    {
        EnzymeFactory instance = EnzymeFactory.getInstance();
        instance.write(new FileOutputStream(new File("D:\\text.xml")));
    }
}