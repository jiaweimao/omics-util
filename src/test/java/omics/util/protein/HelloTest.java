package omics.util.protein;

import omics.util.protein.digest.Enzyme;
import omics.util.utils.NumberFormatFactory;
import org.junit.jupiter.api.Test;

import java.text.NumberFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Nov 2017, 9:54 AM
 */
class HelloTest
{
    @Test
    void hashTest()
    {
        NumberFormat numberFormat = NumberFormatFactory.valueOf("0.#E0");
        double v1 = 15900000;
        assertEquals(numberFormat.format(v1), "1.6E7");
    }
}
