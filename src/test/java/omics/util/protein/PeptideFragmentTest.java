package omics.util.protein;

import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.ModificationResolver;
import omics.util.protein.ms.Ion;
import omics.util.protein.ms.FragmentType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.EnumSet;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * http://db.systemsbiology.net:8080/proteomicsToolkit/FragIonServlet.html
 */
class PeptideFragmentTest
{
    private double eps = 0.0001;

    @Test
    void varargConstructor()
    {
        PeptideFragment frag = new PeptideFragment(FragmentType.FORWARD, AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        assertEquals(FragmentType.FORWARD, frag.getFragmentType());
        assertEquals(4, frag.size());
        assertEquals(AminoAcid.C, frag.getSymbol(0));
        assertEquals(AminoAcid.E, frag.getSymbol(1));
        assertEquals(AminoAcid.R, frag.getSymbol(2));
        assertEquals(AminoAcid.V, frag.getSymbol(3));
    }

    @Test
    void testSubSeqConstructorForward()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.FORWARD, peptide, 0, 3);

        assertEquals(FragmentType.FORWARD, frag.getFragmentType());
        assertEquals(3, frag.size());
        assertEquals(AminoAcid.C, frag.getSymbol(0));
        assertEquals(AminoAcid.E, frag.getSymbol(1));
        assertEquals(AminoAcid.R, frag.getSymbol(2));
    }

    @Test
    void testSubSeqConstructorForwardIllegalIndex()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        assertThrows(IllegalArgumentException.class, () ->
                new PeptideFragment(FragmentType.FORWARD, peptide, 2, 3));
    }

    @Test
    void testSubSeqConstructorReverse()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.REVERSE, peptide, 5, 8);

        assertEquals(FragmentType.REVERSE, frag.getFragmentType());
        assertEquals(3, frag.size());
        assertEquals(AminoAcid.L, frag.getSymbol(0));
        assertEquals(AminoAcid.A, frag.getSymbol(1));
        assertEquals(AminoAcid.S, frag.getSymbol(2));
    }

    @Test
    void testSubSeqConstructorReverseIllegalIndex()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        assertThrows(IllegalArgumentException.class, () -> new PeptideFragment(FragmentType.REVERSE, peptide, 5, 7));
    }

    @Test
    void testSubSeqConstructorMonomer()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.MONOMER, peptide, 3, 4);

        assertEquals(FragmentType.MONOMER, frag.getFragmentType());
        assertEquals(1, frag.size());
        assertEquals(AminoAcid.V, frag.getSymbol(0));
    }

    @Test
    void testSubSeqConstructorMonomerIllegalIndex()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        assertThrows(IllegalArgumentException.class, () -> new PeptideFragment(FragmentType.MONOMER, peptide, 3, 5));
    }

    @Test
    void testSubSeqConstructorInternal()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        PeptideFragment frag = new PeptideFragment(FragmentType.INTERNAL, peptide, 1, 7);

        assertEquals(FragmentType.INTERNAL, frag.getFragmentType());
        assertEquals(6, frag.size());
        assertEquals(AminoAcid.E, frag.getSymbol(0));
        assertEquals(AminoAcid.R, frag.getSymbol(1));
        assertEquals(AminoAcid.V, frag.getSymbol(2));
        assertEquals(AminoAcid.I, frag.getSymbol(3));
        assertEquals(AminoAcid.L, frag.getSymbol(4));
        assertEquals(AminoAcid.A, frag.getSymbol(5));
    }

    @Test
    void testSubSeqConstructorUnknown()
    {
        Peptide peptide = Peptide.parse("CERVILAS");
        assertThrows(IllegalArgumentException.class, () -> new PeptideFragment(FragmentType.UNKNOWN, peptide, 3, 6));
    }

    @Test
    void parse()
    {
        Modification modification = Modification.parseModification("p:HPO3");

        ModificationResolver modResolver = mock(ModificationResolver.class);
        when(modResolver.resolve("p")).thenReturn(modification);

        PeptideFragment fragment = PeptideFragment.parse("PEPS(p)IDE", FragmentType.FORWARD, modResolver);

        assertEquals(1, fragment.getModificationCount());
        assertEquals(modification, fragment.getModificationsAt(3, EnumSet.allOf(ModAttachment.class)).get(0));
        assertEquals(FragmentType.FORWARD, fragment.getFragmentType());
    }

    @Test
    void testImmonium()
    {
        double eps = 0.01;

        double a = createImmonium(AminoAcid.A);
        assertEquals(a, 44.05, eps);

        a = createImmonium(AminoAcid.G);
        assertEquals(a, 30.03, eps);

        a = createImmonium(AminoAcid.S);
        assertEquals(a, 60.05, eps);

        a = createImmonium(AminoAcid.P);
        assertEquals(a, 70.07, eps);

        a = createImmonium(AminoAcid.V);
        assertEquals(a, 72.08, eps);

        a = createImmonium(AminoAcid.T);
        assertEquals(a, 74.06, eps);

        a = createImmonium(AminoAcid.C);
        assertEquals(a, 76.02, eps);

        a = createImmonium(AminoAcid.I);
        assertEquals(a, 86.10, eps);

        a = createImmonium(AminoAcid.L);
        assertEquals(a, 86.10, eps);

        a = createImmonium(AminoAcid.N);
        assertEquals(a, 87.06, eps);

        a = createImmonium(AminoAcid.D);
        assertEquals(a, 88.04, eps);

        a = createImmonium(AminoAcid.Q);
        assertEquals(a, 101.07, eps);

        a = createImmonium(AminoAcid.K);
        assertEquals(a, 101.11, eps);

        a = createImmonium(AminoAcid.E);
        assertEquals(a, 102.06, eps);

        a = createImmonium(AminoAcid.M);
        assertEquals(a, 104.05, eps);

        a = createImmonium(AminoAcid.H);
        assertEquals(a, 110.07, eps);

        a = createImmonium(AminoAcid.F);
        assertEquals(a, 120.08, eps);

        a = createImmonium(AminoAcid.R);
        assertEquals(a, 129.11, eps);

        a = createImmonium(AminoAcid.Y);
        assertEquals(a, 136.08, eps);

        a = createImmonium(AminoAcid.W);
        assertEquals(a, 159.09, eps);
    }

    private static Stream<Arguments> create_b_data()
    {
        return Stream.of(
                Arguments.of(98.060040319, "P"),
                Arguments.of(227.10263341, "PE"),
                Arguments.of(324.15539727, "PEP"),
                Arguments.of(425.20307574, "PEPT"),
                Arguments.of(538.28713972, "PEPTI"),
                Arguments.of(653.31408275, "PEPTID"),
                Arguments.of(72.044390254, "A"),
                Arguments.of(175.05362, "AC"),
                Arguments.of(290.08056, "ACD"),
                Arguments.of(419.12316, "ACDE"),
                Arguments.of(566.19157, "ACDEF"),
                Arguments.of(623.21303, "ACDEFG"),
                Arguments.of(760.27194, "ACDEFGH"),
                Arguments.of(873.35601, "ACDEFGHI"),
                Arguments.of(1001.45097, "ACDEFGHIK"),
                Arguments.of(1114.53503, "ACDEFGHIKL"),
                Arguments.of(1245.57552, "ACDEFGHIKLM"),
                Arguments.of(1359.61845, "ACDEFGHIKLMN")
        );
    }

    @ParameterizedTest
    @MethodSource("create_b_data")
    void calcMz_b(double mass, String seq)
    {
        assertEquals(mass, createIon(seq, Ion.b, FragmentType.FORWARD), eps);
    }

    @Test
    void calcMz_y()
    {
        assertEquals(148.06043425, createIon("E", Ion.y, FragmentType.REVERSE), eps);
        assertEquals(263.08737728, createIon("DE", Ion.y, FragmentType.REVERSE), eps);
        assertEquals(376.17144126, createIon("IDE", Ion.y, FragmentType.REVERSE), eps);
        assertEquals(477.21911974, createIon("TIDE", Ion.y, FragmentType.REVERSE), eps);
        assertEquals(574.27188359, createIon("PTIDE", Ion.y, FragmentType.REVERSE), eps);
        assertEquals(703.31447668, createIon("EPTIDE", Ion.y, FragmentType.REVERSE), eps);
    }

    @Test
    void testEquals()
    {
        PeptideFragment same1 = new PeptideFragment(FragmentType.FORWARD, AminoAcid.C, AminoAcid.E, AminoAcid.R,
                AminoAcid.V);
        PeptideFragment same2 = new PeptideFragment(FragmentType.FORWARD, AminoAcid.C, AminoAcid.E, AminoAcid.R,
                AminoAcid.V);
        PeptideFragment different = new PeptideFragment(FragmentType.REVERSE, AminoAcid.C, AminoAcid.E, AminoAcid.R,
                AminoAcid.V);

        assertEquals(same2, same1);
        assertEquals(same1, same2);
        assertNotEquals(same1, different);
        assertNotEquals(different, same2);
    }

    @Test
    void testHashCode()
    {
        PeptideFragment same1 = new PeptideFragment(FragmentType.FORWARD, AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);
        PeptideFragment same2 = new PeptideFragment(FragmentType.FORWARD, AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);
        PeptideFragment different = new PeptideFragment(FragmentType.REVERSE, AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V);

        assertEquals(same1.hashCode(), same2.hashCode());
        assertNotEquals(different.hashCode(), same1.hashCode());
    }

    private double createIon(String seq, Ion ionType, FragmentType fragmentType)
    {
        PeptideFragment fragment = PeptideFragment.parse(seq, fragmentType);
        return fragment.calcMz(ionType, 1);
    }

    private double createImmonium(AminoAcid aa)
    {
        PeptideFragment fragment = new PeptideFragment(FragmentType.MONOMER, aa);
        return fragment.calcMz(Ion.im, 1);
    }
}