package omics.util.protein;

import omics.util.protein.mod.PTM;
import omics.util.protein.mod.Specificity;
import org.junit.jupiter.api.Test;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Dec 2019, 10:23 AM
 */
class PeptideModTest
{

    @Test
    void testToString()
    {
        PeptideMod peptideMod = new PeptideMod(PTM.Phospho(), Specificity.ProteinNTerm, false);
        System.out.println(peptideMod);
    }
}