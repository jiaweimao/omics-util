package omics.util.protein;

import omics.util.chem.Composition;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.mod.ModificationResolver;
import omics.util.protein.mod.PTMResolver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class PeptideParserTest
{
    @Test
    void testParseBug()
    {
        PTMResolver resolver = new PTMResolver();
        resolver.putOverrideUnimod("m_met_C13D", new Modification("m_met_C13D", Composition.parseComposition("C[13]H[2]3C-1H-3")));
        resolver.putOverrideUnimod("CD3", new Modification("CD3", Composition.parseComposition("CH[2]3H-1")));

        String seq = "ISSPM(m_met_C13D)HPSAG(CD3)G(CD3)QRGGFGSPGGGSGGM(m_met_C13D)SR(CD3)";

        PeptideParser peptideParser = PeptideParser.getInstance();
        Peptide peptide = peptideParser.parsePeptide(seq, resolver);

        assertEquals(seq, peptide.toString());
    }

    @Test
    void testParse1()
    {
        Peptide peptide = Peptide.parse("AAADLMAYCEAHAK");

        assertEquals("AAADLMAYCEAHAK", peptide.toSymbolString());
        assertEquals(0, peptide.getModificationCount());
    }

    @Test
    void testParse2()
    {
        Peptide peptide = Peptide.parse("AAADLMAYC(ICAT-D:2H(8))EAHAK");
        assertEquals("AAADLMAYCEAHAK", peptide.toSymbolString());
        assertEquals(1, peptide.getModificationCount());

        assertEquals(Modification.parseModification("ICAT-D:2H(8):H26H[2]8C20N4O5S"), peptide.getModificationsAt(8, ModAttachment.all).get(0));
    }

    @Test
    void testParse3()
    {
        Modification nTermMod = mock(Modification.class);
        Modification cTermMod = mock(Modification.class);
        Modification internal = Modification.parseModification("ICAT-D:2H(8):H26H[2]8C20N4O5S");

        ModificationResolver modResolver = mock(ModificationResolver.class);
        when(modResolver.resolve("n-term")).thenReturn(nTermMod);
        when(modResolver.resolve("c-term")).thenReturn(cTermMod);
        when(modResolver.resolve("ICAT-D:2H(8)")).thenReturn(internal);

        Peptide peptide = Peptide.parse("(n-term)_AAADLMAYC(ICAT-D:2H(8))EAHAK_(c-term)", modResolver);

        assertEquals("AAADLMAYCEAHAK", peptide.toSymbolString());

        assertEquals(1, peptide.getModificationsAt(8, EnumSet.allOf(ModAttachment.class)).size());
        assertEquals(internal, peptide.getModificationsAt(8, EnumSet.allOf(ModAttachment.class)).get(0));

        assertEquals(1, peptide.getModifications(EnumSet.of(ModAttachment.N_TERM)).size());
        assertEquals(nTermMod, peptide.getModifications(EnumSet.of(ModAttachment.N_TERM)).get(0));

        assertEquals(1, peptide.getModifications(EnumSet.of(ModAttachment.C_TERM)).size());
        assertEquals(cTermMod, peptide.getModifications(EnumSet.of(ModAttachment.C_TERM)).get(0));
    }

    @Test
    void testParse4()
    {
        Peptide peptide = Peptide.parse("MQRSTATGCFKLX");

        assertFalse(peptide.hasModifications());
        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    @Test
    void testParse5()
    {
        Peptide peptide = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        assertTrue(peptide.hasModifications());
        assertEquals(2, peptide.getModifiedResidueCount());

        List<Modification> mods1 = peptide.getModificationsAt(2, ModAttachment.all);
        assertEquals(1, mods1.size());
        List<Modification> mods2 = peptide.getModificationsAt(6, ModAttachment.all);
        assertEquals(1, mods2.size());

        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    @Test
    void testParse6()
    {
        Peptide peptide = Peptide.parse("(O2)_MQRSTATGCFKLX_(C)");

        assertTrue(peptide.hasModifications());
        assertEquals(2, peptide.getModifiedResidueCount());

        List<Modification> mods1 = peptide.getModificationsAt(0, ModAttachment.all);
        assertEquals(1, mods1.size());
        List<Modification> mods2 = peptide.getModificationsAt(12, ModAttachment.all);
        assertEquals(1, mods2.size());

        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    /**
     * Test that we can parse a large peptide
     */
    @Test
    void testParse7()
    {
        String seq = "MGLPLARLAAVCLALSLAGGSELQTEGRTRYHGRNVCSTWGNFHYKTFDGDVFRFPGLCD"
                + "YNFASDCRGSYKEFAVHLKRGPGQAEAPAGVESILLTIKDDTIYLTRHLAVLNGAVVSTP"
                + "HYSPGLLIEKSDAYTKVYSRAGLTLMWNREDALMLELDTKFRNHTCGLCGDYNGLQSYSE"
                + "FLSDGVLFSPLEFGNMQKINQPDVVCEDPEEEVAPASCSEHRAECERLLTAEAFADCQDL"
                + "VPLEPYLRACQQDRCRCPGGDTCVCSTVAEFSRQCSHAGGRPGNWRTATLCPKTCPGNLV"
                + "YLESGSPCMDTCSHLEVSSLCEEHRMDGCFCPEGTVYDDIGDSGCVPVSQCHCRLHGHLY"
                + "TPGQEITNDCEQCVCNAGRWVCKDLPCPGTCALEGGSHITTFDGKTYTFHGDCYYVLAKG"
                + "DHNDSYALLGELAPCGSTDKQTCLKTVVLLADKKKNAVVFKSDGSVLLNQLQVNLPHVTA"
                + "SFSVFRPSSYHIMVSMAIGVRLQVQLAPVMQLFVTLDQASQGQVQGLCGNFNGLEGDDFK"
                + "TASGLVEATGAGFANTWKAQSTCHDKLDWLDDPCSLNIESANYAEHWCSLLKKTETPFGR"
                + "CHSAVDPAEYYKRCKYDTCNCQNNEDCLCAALSSYARACTAKGVMLWGWREHVCNKDVGS"
                + "CPNSQVFLYNLTTCQQTCRSLSEADSHCLEGFAPVDGCGCPDHTFLDEKGRCVPLAKCSC"
                + "YHRGLYLEAGDVVVRQEERCVCRDGRLHCRQIRLIGQSCTAPKIHMDCSNLTALATSKPR"
                + "ALSCQTLAAGYYHTECVSGCVCPDGLMDDGRGGCVVEKECPCVHNNDLYSSGAKIKVDCN"
                + "TCTCKRGRWVCTQAVCHGTCSIYGSGHYITFDGKYYDFDGHCSYVAVQDYCGQNSSLGSF"
                + "SIITENVPCGTTGVTCSKAIKIFMGRTELKLEDKHRVVIQRDEGHHVAYTTREVGQYLVV"
                + "ESSTGIIVIWDKRTTVFIKLAPSYKGTVCGLCGNFDHRSNNDFTTRDHMVVSSELDFGNS"
                + "WKEAPTCPDVSTNPEPCSLNPHRRSWAEKQCSILKSSVFSICHSKVDPKPFYEACVHDSC"
                + "SCDTGGDCECFCSAVASYAQECTKEGACVFWRTPDLCPIFCDYYNPPHECEWHYEPCGNR"
                + "SFETCRTINGIHSNISVSYLEGCYPRCPKDRPIYEEDLKKCVTADKCGCYVEDTHYPPGA"
                + "SVPTEETCKSCVCTNSSQVVCRPEEGKILNQTQDGAFCYWEICGPNGTVEKHFNICSITT"
                + "RPSTLTTFTTITLPTTPTSFTTTTTTTTPTSSTVLSTTPKLCCLWSDWINEDHPSSGSDD"
                + "GDREPFDGVCGAPEDIECRSVKDPHLSLEQHGQKVQCDVSVGFICKNEDQFGNGPFGLCY"
                + "DYKIRVNCCWPMDKCITTPSPPTTTPSPPPTTTTTLPPTTTPSPPTTTTTTPPPTTTPSP"
                + "PITTTTTPLPTTTPSPPISTTTTPPPTTTPSPPTTTPSPPTTTPSPPTTTTTTPPPTTTP"
                + "SPPMTTPITPPASTTTLPPTTTPSPPTTTTTTPPPTTTPSPPTTTPITPPTSTTTLPPTT"
                + "TPSPPPTTTTTPPPTTTPSPPTTTTPSPPTITTTTPPPTTTPSPPTTTTTTPPPTTTPSP"
                + "PTTTPITPPTSTTTLPPTTTPSPPPTTTTTPPPTTTPSPPTTTTPSPPITTTTTPPPTTT"
                + "PSSPITTTPSPPTTTMTTPSPTTTPSSPITTTTTPSSTTTPSPPPTTMTTPSPTTTPSPP"
                + "TTTMTTLPPTTTSSPLTTTPLPPSITPPTFSPFSTTTPTTPCVPLCNWTGWLDSGKPNFH"
                + "KPGGDTELIGDVCGPGWAANISCRATMYPDVPIGQLGQTVVCDVSVGLICKNEDQKPGGV"
                + "IPMAFCLNYEINVQCCECVTQPTTMTTTTTENPTPPTTTPITTTTTVTPTPTPTGTQTPT"
                + "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP"
                + "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT"
                + "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT"
                + "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV"
                + "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT"
                + "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT"
                + "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT"
                + "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ"
                + "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT"
                + "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT"
                + "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP"
                + "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT"
                + "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT"
                + "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP"
                + "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTP"
                + "ITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPT"
                + "GTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTT"
                + "TVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTP"
                + "TTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPT"
                + "PTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPI"
                + "TTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTG"
                + "TQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTT"
                + "VTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPT"
                + "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP"
                + "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT"
                + "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT"
                + "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV"
                + "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT"
                + "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT"
                + "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT"
                + "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ"
                + "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT"
                + "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT"
                + "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP"
                + "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT"
                + "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT"
                + "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP"
                + "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTGPPTH"
                + "TSTAPIAELTTSNPPPESSTPQTSRSTSSPLTESTTLLSTLPPAIEMTSTAPPSTPTAPT"
                + "TTSGGHTLSPPPSTTTSPPGTPTRGTTTGSSSAPTPSTVQTTTTSAWTPTPTPLSTPSII"
                + "RTTGLRPYPSSVLICCVLNDTYYAPGEEVYNGTYGDTCYFVNCSLSCTLEFYNWSCPSTP"
                + "SPTPTPSKSTPTPSKPSSTPSKPTPGTKPPECPDFDPPRQENETWWLCDCFMATCKYNNT"
                + "VEIVKVECEPPPMPTCSNGLQPVRVEDPDGCCWHWECDCYCTGWGDPHYVTFDGLYYSYQ"
                + "GNCTYVLVEEISPSVDNFGVYIDNYHCDPNDKVSCPRTLIVRHETQEVLIKTVHMMPMQV"
                + "QVQVNRQAVALPYKKYGLEVYQSGINYVVDIPELGVLVSYNGLSFSVRLPYHRFGNNTKG"
                + "QCGTCTNTTSDDCILPSGEIVSNCEAAADQWLVNDPSKPHCPHSSSTTKRPAVTVPGGGK"
                + "TTPHKDCTPSPLCQLIKDSLFAQCHALVPPQHYYDACVFDSCFMPGSSLECASLQAYAAL"
                + "CAQQNICLDWRNHTHGACLVECPSHREYQACGPAEEPTCKSSSSQQNNTVLVEGCFCPEG"
                + "TMNYAPGFDVCVKTCGCVGPDNVPREFGEHFEFDCKNCVCLEGGSGIICQPKRCSQKPVT"
                + "HCVEDGTYLATEVNPADTCCNITVCKCNTSLCKEKPSVCPLGFEVKSKMVPGRCCPFYWC"
                + "ESKGVCVHGNAEYQPGSPVYSSKCQDCVCTDKVDNNTLLNVIACTHVPCNTSCSPGFELM"
                + "EAPGECCKKCEQTHCIIKRPDNQHVILKPGDFKSDPKNNCTFFSCVKIHNQLISSVSNIT"
                + "CPNFDASICIPGSITFMPNGCCKTCTPRNETRVPCSTVPVTTEVSYAGCTKTVLMNHCSG"
                + "SCGTFVMYSAKAQALDHSCSCCKEEKTSQREVVLSCPNGGSLTHTYTHIESCQCQDTVCG" + "LPTGTSRRARRSPRHLGSG";

        Peptide peptide = Peptide.parse(seq);
        assertEquals(seq, peptide.toSymbolString());
    }

    @Test
    void testParse8()
    {
        Peptide peptide = Peptide.parse("CERVILAS(HPO3)");
        assertEquals("CERVILAS", peptide.toSymbolString());
        assertEquals(1, peptide.getModificationCount());
        assertEquals(Modification.parseModification("HPO3"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(0));
    }

    @Test
    void testParse9()
    {
        Peptide peptide = Peptide.parse("CERVILAS(HPO3, O)");
        assertEquals("CERVILAS", peptide.toSymbolString());
        assertEquals(2, peptide.getModificationCount());
        assertEquals(Modification.parseModification("HPO3"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(0));
        assertEquals(Modification.parseModification("O"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(1));
    }

    @Test
    void testParse10()
    {
        Peptide peptide = Peptide.parse("AAAAAAGAGPEM(O)VR");
        assertEquals("AAAAAAGAGPEMVR", peptide.toSymbolString());
        assertEquals(1, peptide.getModificationCount());
        assertEquals(Modification.parseModification("O"), peptide.getModifications(EnumSet.of(ModAttachment.SIDE_CHAIN)).get(0));
    }


    private static Stream<Arguments> illegalData()
    {
        return Stream.of(
                Arguments.of("AAAD(O,  )LMAYCEAHAK"),
                Arguments.of("AAAD()LMAYCEAHAK"),
                Arguments.of("PEPT((phospho)IDE"),
                Arguments.of("CER_"),
                Arguments.of("CER_)"),
                Arguments.of("_)"),
                Arguments.of("(_"),
                Arguments.of(")"),
                Arguments.of("("),
                Arguments.of("C___C___(H2C2O)"),
                Arguments.of("C_((H2C2O))"),
                Arguments.of("C_((H2C2O)"),
                Arguments.of("CER<<>>V"),
                Arguments.of(""),
                Arguments.of("_(H2C2O)"),
                Arguments.of("(CH3)_"),
                Arguments.of("(CH3)"),
                Arguments.of("A(CH3)(H2C2O)CPLEK"),
                Arguments.of("#!/bin/bash"),
                Arguments.of("()_UHRCUHHA(C4H12)U(C)EHE_()")// miss term mod
        );
    }

    @ParameterizedTest
    @MethodSource("illegalData")
    void testParse(String value)
    {
        assertThrows(PeptideParseException.class, () -> Peptide.parse(value));
    }
}