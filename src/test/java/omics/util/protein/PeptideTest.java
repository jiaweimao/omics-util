package omics.util.protein;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Sets;
import omics.util.chem.AtomicSymbol;
import omics.util.chem.Composition;
import omics.util.chem.Sequence;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.ms.Ion;
import omics.util.protein.ms.FragmentType;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PeptideTest
{

    private static final double eps = 0.000001;

    @Test
    void calcMass()
    {
        Peptide peptide = Peptide.parse("QELLLNPTHQIYPVTQPLAPVHNPISV");
        assertEquals(peptide.getMolecularMass(), 3017.6338418192, 1e-6);
    }

    @Test
    void testSubSequence()
    {
        Peptide protein = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        Peptide peptide = protein.subSequence(3, 12);
        assertEquals("STATGCFKL", peptide.toSymbolString());
        assertEquals(1, peptide.getModifiedResidueCount());
        assertTrue(peptide.hasModificationAt(3));

        Set<Modification> modSet = new HashSet<>();
        modSet.add(Modification.parseModification("C"));

        Peptide nodMOd = Peptide.removeMod(protein, modSet);
        assertEquals(nodMOd.toString(), "MQR(O2)STATGCFKLX");
    }

    @Test
    void testSubSequence2()
    {
        Peptide protein = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        Peptide peptide = protein.subSequence(0, 3);
        assertEquals("MQR", peptide.toSymbolString());
        assertEquals(1, peptide.getModifiedResidueCount());
        assertTrue(peptide.hasModificationAt(2));
    }

    @Test
    void testSubSequence3()
    {
        Peptide protein = Peptide.parse("MQR(O2)STAT(C)GCFKLX");

        Peptide peptide = protein.subSequence(0, protein.size());
        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
        assertEquals(2, peptide.getModifiedResidueCount());
        assertTrue(peptide.hasModificationAt(2));
        assertTrue(peptide.hasModificationAt(6));
    }

    @Test
    void testPeptide()
    {
        Peptide peptide = Peptide.parse("UHRCUHHA(C4H12)U(C)EHE");

        assertTrue(peptide.hasModificationAt(7));
        assertTrue(peptide.hasModificationAt(8));
    }

    @Test
    void testPeptideCopy()
    {
        Peptide peptide = Peptide.parse("UHRCUHHA(C4H9)U(C)EHE");

        Peptide copy = new Peptide(peptide);

        assertEquals("UHRCUHHAUEHE", copy.toSymbolString());
        assertTrue(copy.hasModificationAt(7));
        assertTrue(copy.hasModificationAt(8));
    }

    @Test
    void testPeptideWithNTermModsCopy()
    {
        Peptide peptide = new Peptide.Builder("UHRCUHHAUEHE")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();
        Peptide copy = new Peptide(peptide);

        assertEquals("UHRCUHHAUEHE", copy.toSymbolString());
        assertTrue(copy.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    void testPeptideWithCTermModsCopy()
    {
        Peptide peptide = new Peptide.Builder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();
        Peptide copy = new Peptide(peptide);

        assertEquals("UHRCUHHAUEHE", copy.toSymbolString());
        assertTrue(copy.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    void testHasModsAt()
    {
        Peptide peptide = new Peptide.Builder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        assertEquals("UHRCUHHAUEHE", peptide.toSymbolString());
        assertTrue(peptide.hasModificationAt(EnumSet.of(ModAttachment.C_TERM, ModAttachment.N_TERM)));
    }

    @Test
    void testSuffixPeptideWithNTermModsCopy()
    {
        Peptide peptide = new Peptide.Builder("UHRCUHHAUEHE")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();
        Peptide copy = new Peptide(peptide, 1, 12);

        assertEquals("HRCUHHAUEHE", copy.toSymbolString());
        assertFalse(copy.hasModifications());
    }

    @Test
    void testSuffixPeptideWithCTermModsCopy()
    {
        Peptide peptide = new Peptide.Builder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide, 1, 12);

        assertEquals("HRCUHHAUEHE", copy.toSymbolString());
        assertTrue(copy.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    void testPrefixPeptideWithNTermModsCopy()
    {
        Peptide peptide = new Peptide.Builder("UHRCUHHAUEHE")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide, 0, 5);

        assertEquals("UHRCU", copy.toSymbolString());
        assertTrue(copy.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    void testPrefixPeptideWithCTermModsCopy()
    {
        Peptide peptide = new Peptide.Builder("UHRCUHHAUEHE")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        Peptide copy = new Peptide(peptide, 0, 5);

        assertEquals("UHRCU", copy.toSymbolString());
        assertFalse(copy.hasModifications());
    }

    @Test
    void testStringConstructor()
    {
        Peptide peptide = Peptide.parse("PEPTIDE");

        assertEquals(7, peptide.size());
        assertEquals(AminoAcid.P, peptide.getSymbol(0));
        assertEquals(AminoAcid.E, peptide.getSymbol(1));
        assertEquals(AminoAcid.P, peptide.getSymbol(2));
        assertEquals(AminoAcid.T, peptide.getSymbol(3));
        assertEquals(AminoAcid.I, peptide.getSymbol(4));
        assertEquals(AminoAcid.D, peptide.getSymbol(5));
        assertEquals(AminoAcid.E, peptide.getSymbol(6));
    }

    @Test
    void testMods()
    {
        Peptide peptide = new Peptide.Builder(AminoAcid.P, AminoAcid.E, AminoAcid.T)
                .addModification(1, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(1, new Modification("O", new Composition.Builder(AtomicSymbol.O).build())).build();

        String string = peptide.toString();

        Peptide peptide2 = Peptide.parse(string);

        assertEquals(AminoAcid.P, peptide2.getSymbol(0));
        assertEquals(AminoAcid.E, peptide2.getSymbol(1));
        assertEquals(AminoAcid.T, peptide2.getSymbol(2));

        List<Modification> mods = peptide2.getModificationsAt(1, ModAttachment.all);
        assertEquals(2, mods.size());

        assertEquals(Modification.parseModification("CH3"), mods.get(0));

        assertEquals(Modification.parseModification("O"), mods.get(1));
    }

    @Test
    void testModificationList()
    {
        Modification mod1 = mockMod("7.69", 7.69);
        Modification mod2 = mockMod("18.0", 18);

        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(0, mod1)
                .addModification(ModAttachment.N_TERM, mod2)
                .build();

        List<Modification> mods = peptide.getModificationsAt(0, ModAttachment.all);

        assertEquals(2, mods.size());
        assertEquals(mod1, mods.get(0));
        assertEquals(mod2, mods.get(1));

        Modification cTermMod = mockMod("cTerm", 873.69);
        Modification mod3 = mockMod("last", 18);

        peptide = new Peptide.Builder("CERVILAS")
                .addModification(0, mod1)
                .addModification(ModAttachment.N_TERM, mod2)
                .addModification(ModAttachment.C_TERM, cTermMod)
                .addModification(peptide.size() - 1, mod3)
                .build();
        List<Modification> cTermList = peptide.getModificationsAt(peptide.size() - 1, ModAttachment.all);
        assertEquals(2, cTermList.size());
        assertEquals(mod3, cTermList.get(0));
        assertEquals(cTermMod, cTermList.get(1));
        assertTrue(cTermList.contains(cTermMod));
    }

    @Test
    void testGetAllModifications()
    {
        Modification nTermMod = mockMod("18.0", 18);
        Modification mod1 = mockMod("7.69", 7.69);
        Modification mod2 = mockMod("last", 18);
        Modification cTermMod = mockMod("cTerm", 873.69);

        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, nTermMod)
                .addModification(0, mod1).addModification(7, mod2)
                .addModification(ModAttachment.C_TERM, cTermMod)
                .build();

        List<Modification> modifications = new ArrayList<>(peptide.getModifications(ModAttachment.all));
        modifications.sort(Comparator.comparing(Modification::toString));
        System.out.println(peptide.toString());

        assertEquals(4, modifications.size());
        assertEquals(nTermMod, modifications.get(0));
        assertEquals(mod1, modifications.get(1));
        assertEquals(cTermMod, modifications.get(2));
        assertEquals(mod2, modifications.get(3));
    }

    @Test
    void testGetAllModifications2()
    {
        Modification nTermMod = mockMod("18.0", 18);
        Modification mod1 = mockMod("7.69", 7.69);
        Modification mod2 = mockMod("last", 18);
        Modification cTermMod = mockMod("cTerm", 873.69);

        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, nTermMod)
                .addModification(0, mod1)
                .addModification(7, mod2)
                .addModification(ModAttachment.C_TERM, cTermMod)
                .build();

        List<Modification> modifications = new ArrayList<>(
                peptide.getModifications(ModAttachment.sideChainSet));
        modifications.sort(Comparator.comparing(Modification::toString));

        assertEquals(2, modifications.size());
        assertEquals(mod1, modifications.get(0));
        assertEquals(mod2, modifications.get(1));
    }

    @Test
    void testModificationIndexes()
    {
        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("18.0", 18))
                .addModification(1, mockMod("7.69", 7.69))
                .addModification(6, mockMod("last", 18))
                .addModification(ModAttachment.C_TERM, mockMod("cTerm", 873.69))
                .build();

        assertArrayEquals(new int[]{0, 1, 6, 7}, peptide.getModificationIndexes(ModAttachment.all));
        assertArrayEquals(new int[]{1, 6}, peptide.getModificationIndexes(ModAttachment.sideChainSet));
        assertArrayEquals(new int[]{0}, peptide.getModificationIndexes(ModAttachment.nTermSet));
        assertArrayEquals(new int[]{7}, peptide.getModificationIndexes(ModAttachment.cTermSet));
    }

    @Test
    void testModificationIndexes2()
    {
        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("18.0", 18))
                .addModification(1, mockMod("7.69", 7.69))
                .addModification(7, mockMod("last", 18))
                .addModification(ModAttachment.C_TERM, mockMod("cTerm", 873.69))
                .build();

        assertArrayEquals(new int[]{0, 1, 7}, peptide.getModificationIndexes(ModAttachment.all));
    }

    @Test
    void testModificationIndexes3()
    {
        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("18.0", 18))
                .addModification(0, mockMod("7.69", 7.69))
                .addModification(6, mockMod("last", 18))
                .addModification(ModAttachment.C_TERM, mockMod("cTerm", 873.69))
                .build();

        assertArrayEquals(new int[]{0, 6, 7}, peptide.getModificationIndexes(ModAttachment.all));
    }

    @Test
    void testModResidueCount()
    {
        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("1", 1))
                .addModification(ModAttachment.N_TERM, mockMod("2", 2))
                .addModification(0, mockMod("13.6", 13.6))
                .build();
        assertEquals(1, peptide.getModifiedResidueCount());

        peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, mockMod("1", 1))
                .addModification(ModAttachment.N_TERM, mockMod("2", 2))
                .addModification(0, mockMod("13.6", 13.6))
                .addModification(ModAttachment.C_TERM, mockMod("7.6", 7.6))
                .build();
        assertEquals(2, peptide.getModifiedResidueCount());
    }

    @Test
    void testToString()
    {
        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(0, new Modification("O", new Composition.Builder(AtomicSymbol.O).build()))
                .addModification(0, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(ModAttachment.N_TERM, new Modification("H", new Composition.Builder(AtomicSymbol.H).build()))
                .build();

        assertEquals("(H)_C(O, CH3)ERVILAS", peptide.toString());
    }

    @Test
    void testToString2()
    {
        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, new Modification("H", new Composition.Builder(AtomicSymbol.H).build()))
                .addModification(0, new Modification("O", new Composition.Builder(AtomicSymbol.O).build()))
                .addModification(0, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(7, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(ModAttachment.C_TERM, new Modification("F", new Composition.Builder(AtomicSymbol.F).build()))
                .build();

        assertEquals("(H)_C(O, CH3)ERVILAS(CH3)_(F)", peptide.toString());
    }

    @Test
    void testCTermMods()
    {
        Modification cTermMod = Modification.parseModification("O");
        Peptide peptide = new Peptide.Builder("PEPT")
                .addModification(ModAttachment.C_TERM, cTermMod)
                .addModification(3, Modification.parseModification("H"))
                .build();

        assertEquals(Modification.parseModification("O"), peptide.getModifications(ModAttachment.cTermSet).get(0));
    }

    @Test
    void testNTermMods()
    {
        Modification nTermMod = Modification.parseModification("O");
        Peptide peptide = new Peptide.Builder("PEPT")
                .addModification(ModAttachment.N_TERM, nTermMod)
                .addModification(0, Modification.parseModification("H"))
                .build();

        assertEquals(Modification.parseModification("O"), peptide.getModifications(ModAttachment.nTermSet).get(0));
    }

    @Test
    void testCalculateMassJPL41()
    {
        Peptide peptide = Peptide.parse("AAAAYIYEAVNK");

        assertEquals(1282.6557, peptide.getMolecularMass(), 0.0001);
    }

    @Test
    void testCalculateMass2()
    {
        Peptide peptide = new Peptide.Builder("AAAAYIYEAVNK")
                .addModification(5, mockMod("mod", 45))
                .build();

        assertEquals(1327.6557, peptide.getMolecularMass(), 0.0001);
    }

    @Test
    void testCalculateUnknownMass()
    {
        Peptide peptide = Peptide.parse("BEBTIDE");
        assertThrows(IllegalStateException.class, peptide::getMolecularMass);
    }

    @Test
    void testHasknownComposition()
    {
        Peptide peptide = Peptide.parse("PEPTIDE");

        assertFalse(peptide.hasAmbiguousAminoAcids());
        assertEquals(Collections.<AminoAcid>emptySet(), peptide.getAmbiguousAminoAcids());
    }

    @Test
    void testHasUnknownComposition()
    {
        Peptide peptide = Peptide.parse("BEBTIDE");

        assertTrue(peptide.hasAmbiguousAminoAcids());
        assertEquals(ImmutableSet.of(AminoAcid.B), peptide.getAmbiguousAminoAcids());
    }

    @Test
    void testCopy()
    {
        Peptide peptide = Peptide.parse("ANKER");
        Peptide copy = new Peptide(peptide);

        assertEquals(peptide, copy);
    }

    @Test
    void testCopyWithMods()
    {
        Peptide peptide = new Peptide.Builder("AAAAYIYEAVNK")
                .addModification(5, mockMod("mod", 45))
                .build();

        Peptide copy = new Peptide(peptide);

        assertEquals(peptide, copy);
    }

    @Test
    void testCalculateMz()
    {
        Peptide peptide = Peptide.parse("(H2C2O)_AAAAAAGAGPEMVR");

        assertEquals(642.8218864448405, peptide.calculateMz(2), eps);
    }

    @Test
    void testCreateFragment()
    {
        String seq = "PEPTIDE";

        Peptide peptide = Peptide.parse(seq);

        for (int i = 1; i < peptide.size(); i++) {

            assertEquals(seq.substring(0, i), peptide.createFragment(0, i, FragmentType.FORWARD).toString());
        }
    }

    @Test
    void testCreateNtermFragmentFromModNtermPeptideHotFix14()
    {
        Peptide peptide = new Peptide.Builder("VV")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(0, 1, FragmentType.FORWARD);

        assertTrue(fragment.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    void testCreateCtermFragmentFromModNtermPeptideHotFix14()
    {
        Peptide peptide = new Peptide.Builder("VV")
                .addModification(ModAttachment.N_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(1, 2, FragmentType.REVERSE);

        assertFalse(fragment.hasModificationAt(ModAttachment.N_TERM));
    }

    @Test
    void testCreateNtermFragmentFromModCtermPeptideHotFix14()
    {
        Peptide peptide = new Peptide.Builder("VV")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(0, 1, FragmentType.FORWARD);

        assertFalse(fragment.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    void testCreateCtermFragmentFromModCtermPeptideHotFix14()
    {
        Peptide peptide = new Peptide.Builder("VV")
                .addModification(ModAttachment.C_TERM, mock(Modification.class))
                .build();

        PeptideFragment fragment = peptide.createFragment(1, 2, FragmentType.REVERSE);

        assertTrue(fragment.hasModificationAt(ModAttachment.C_TERM));
    }

    @Test
    void testHashAndEquals()
    {
        Peptide peptide1 = Peptide.parse("STYY");
        Peptide peptide2 = Peptide.parse("STYY");
        Peptide peptide3 = new Peptide.Builder("STYY")
                .addModification(ModAttachment.N_TERM, mockMod("nTerm", 78.69))
                .build();

        assertEquals(peptide2, peptide1);
        assertNotEquals(peptide3, peptide1);
        assertNotEquals(peptide3, peptide2);

        assertEquals(peptide1.hashCode(), peptide2.hashCode());
        assertNotSame(peptide1.hashCode(), peptide3.hashCode());
        assertNotSame(peptide2.hashCode(), peptide3.hashCode());
    }

    @Test
    void testHashAndEquals2()
    {
        Peptide peptide1 = Peptide.parse("STYY");
        Peptide peptide2 = Peptide.parse("STYY");
        Peptide peptide3 = new Peptide.Builder("STYY")
                .addModification(1, mockMod("nTerm", 78.69)).build();

        assertEquals(peptide2, peptide1);
        assertNotEquals(peptide3, peptide1);
        assertNotEquals(peptide3, peptide2);

        assertEquals(peptide1.hashCode(), peptide2.hashCode());
        assertNotSame(peptide1.hashCode(), peptide3.hashCode());
        assertNotSame(peptide2.hashCode(), peptide3.hashCode());
    }

    @Test
    void testHasSameSequence()
    {

        Peptide peptide1 = Peptide.parse("STYY");
        Peptide peptide2 = Peptide.parse("STYY");
        Peptide peptide3 = new Peptide.Builder("STYY")
                .addModification(1, mockMod("nTerm", 78.69)).build();
        Peptide peptide4 = Peptide.parse("STYK");

        assertTrue(peptide1.hasSameSequence(peptide2));
        assertTrue(peptide2.hasSameSequence(peptide3));
        assertFalse(peptide2.hasSameSequence(peptide4));
    }

    @Test
    void testHasSameSequence2()
    {
        Peptide peptide = Peptide.parse("STYY");
        @SuppressWarnings("unchecked")
        Sequence<AminoAcid> seqSame = (Sequence<AminoAcid>) mock(Sequence.class);
        when(seqSame.size()).thenReturn(4);
        when(seqSame.getSymbol(0)).thenReturn(AminoAcid.S);
        when(seqSame.getSymbol(1)).thenReturn(AminoAcid.T);
        when(seqSame.getSymbol(2)).thenReturn(AminoAcid.Y);
        when(seqSame.getSymbol(3)).thenReturn(AminoAcid.Y);

        @SuppressWarnings("unchecked")
        Sequence<AminoAcid> seqDiffLen = (Sequence<AminoAcid>) mock(Sequence.class);
        when(seqDiffLen.size()).thenReturn(5);

        @SuppressWarnings("unchecked")
        Sequence<AminoAcid> seqDiffSeq = (Sequence<AminoAcid>) mock(Sequence.class);
        when(seqDiffSeq.size()).thenReturn(4);
        when(seqDiffSeq.getSymbol(0)).thenReturn(AminoAcid.S);
        when(seqDiffSeq.getSymbol(1)).thenReturn(AminoAcid.Y);
        when(seqDiffSeq.getSymbol(2)).thenReturn(AminoAcid.T);
        when(seqDiffSeq.getSymbol(3)).thenReturn(AminoAcid.Y);

        assertTrue(peptide.hasSameSequence(seqSame));
        assertFalse(peptide.hasSameSequence(seqDiffLen));
        assertFalse(peptide.hasSameSequence(seqDiffSeq));
    }

    @Test
    void testSearchAAIndices()
    {
        Peptide peptide = Peptide.parse("PEPTIDE");

        int[] indices = peptide.getSymbolIndexes(AminoAcid.P);

        assertArrayEquals(new int[]{0, 2}, indices);
    }

    @Test
    void testSearchAAIndicesNotFound()
    {
        Peptide peptide = Peptide.parse("PEPTIDE");

        int[] indices = peptide.getSymbolIndexes(AminoAcid.R);

        assertArrayEquals(new int[]{}, indices);
    }

    @Test
    void testIsSameSequence()
    {
        Peptide peptide1 = new Peptide.Builder("CERVILAS")
                .addModification(3, mockMod("mod", 16.8)).build();
        Peptide peptide2 = Peptide.parse("CERVILAS");
        Peptide peptide3 = Peptide.parse("CERVILAK");
        Peptide peptide4 = Peptide.parse("CERV");

        assertTrue(peptide1.hasSameSequence(peptide2));
        assertFalse(peptide1.hasSameSequence(peptide3));
        assertFalse(peptide1.hasSameSequence(peptide4));
    }

    private Modification mockMod(String label, double mw)
    {
        Modification mod = mock(Modification.class);
        when(mod.getTitle()).thenReturn(label);
        when(mod.getMolecularMass()).thenReturn(mw);
        when(mod.toString()).thenReturn(label);

        return mod;
    }

    @Test
    void testParse()
    {
        Peptide peptide = Peptide.parse("MQRSTATGCFKLX");

        assertFalse(peptide.hasModifications());
        assertEquals("MQRSTATGCFKLX", peptide.toSymbolString());
    }

    @Test
    void testGetModificationCount()
    {
        Peptide peptide = new Peptide.Builder("CERVILAS")
                .addModification(ModAttachment.N_TERM, new Modification("H", new Composition.Builder(AtomicSymbol.H).build()))
                .addModification(0, new Modification("O", new Composition.Builder(AtomicSymbol.O).build()))
                .addModification(0, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(7, new Modification("CH3", new Composition.Builder(AtomicSymbol.C).add(AtomicSymbol.H, 3).build()))
                .addModification(ModAttachment.C_TERM, new Modification("F", new Composition.Builder(AtomicSymbol.F).build()))
                .build();

        assertEquals(5, peptide.getModificationCount());
    }

    @Test
    void testAAOccurrence()
    {
        Peptide peptide = Peptide.parse("CEYRSPTSILK");

        assertEquals(4, peptide.countAminoAcidsIn(Sets.newHashSet(AminoAcid.S, AminoAcid.T, AminoAcid.Y)));
        assertEquals(0, peptide.countAminoAcidsIn(new HashSet<>()));
    }

    @Test
    void testSunRegExpBugToFix20()
    {
        String seq =
                "ATMYPDVPIGQLGQTVVCDVSVGLICKNEDQKPGGVIPMAFCLNYEINVQCCECVTQPTTMTTTTTENPTPPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT" +
                        "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV" +
                        "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPT" +
                        "PTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP" +
                        "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT" +
                        "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTP" +
                        "TTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTP" +
                        "ITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT" +
                        "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTT" +
                        "VTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP" +
                        "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT" +
                        "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTG" +
                        "TQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT" +
                        "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT" +
                        "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT" +
                        "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV" +
                        "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT" +
                        "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTP" +
                        "TTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT" +
                        "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT" +
                        "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTG" +
                        "TQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT" +
                        "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTT" +
                        "VTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT" +
                        "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTGPPTHTSTAPIAELTTSNPPPESSTPQTSR";

        Peptide.parse(seq);
    }

    @Test
    void testSubSequence5()
    {
        Peptide peptide = Peptide.parse("CERVILASK");

        assertEquals(Peptide.parse("C"), peptide.subSequence(Ion.a, 1));
        assertEquals(Peptide.parse("CER"), peptide.subSequence(Ion.a, 3));
        assertEquals(Peptide.parse("CERVILASK"), peptide.subSequence(Ion.a, 9));

        assertEquals(Peptide.parse("K"), peptide.subSequence(Ion.y, 1));
        assertEquals(Peptide.parse("ASK"), peptide.subSequence(Ion.y, 3));
        assertEquals(Peptide.parse("CERVILASK"), peptide.subSequence(Ion.y, 9));

        assertEquals(Peptide.parse("C"), peptide.subSequence(Ion.im, 1));
        assertEquals(Peptide.parse("V"), peptide.subSequence(Ion.im, 4));
        assertEquals(Peptide.parse("K"), peptide.subSequence(Ion.im, 9));
    }

    @Test
    void testSubSequence6()
    {
        Peptide peptide = Peptide.parse("MSK");

        assertThrows(IllegalArgumentException.class, () -> peptide.subSequence(Ion.im, 0));
    }

    @Test
    void testSubSequence7()
    {
        Peptide peptide = Peptide.parse("MSK");

        assertThrows(IllegalArgumentException.class, () -> peptide.subSequence(Ion.im, 4));
    }

    @Test
    void testSubSequence8()
    {
        Peptide peptide = Peptide.parse("MSK");
        assertThrows(IllegalArgumentException.class, () -> peptide.subSequence(Ion.p, 1));
    }

    @Test
    void testSubSequence9()
    {
        Peptide peptide = Peptide.parse("MSK");

        assertThrows(IllegalArgumentException.class, () -> peptide.subSequence(Ion.yb, 1));
    }

    @Test
    void testGetModificationCount2()
    {
        List<AminoAcid> residues = Arrays.asList(AminoAcid.C, AminoAcid.E, AminoAcid.R, AminoAcid.V, AminoAcid.K);
        ListMultimap<Integer, Modification> sideChainModMap = ArrayListMultimap.create();
        sideChainModMap.put(2, mock(Modification.class));
        sideChainModMap.put(3, mock(Modification.class));
        sideChainModMap.put(3, mock(Modification.class));

        ListMultimap<ModAttachment, Modification> termModMap = ArrayListMultimap.create();
        termModMap.put(ModAttachment.N_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.N_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.N_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));
        termModMap.put(ModAttachment.C_TERM, mock(Modification.class));

        Peptide peptide = new Peptide(residues, sideChainModMap, termModMap);

        assertEquals(3, peptide.getModificationCount(ModAttachment.SIDE_CHAIN));
        assertEquals(3, peptide.getModificationCount(ModAttachment.N_TERM));
        assertEquals(7, peptide.getModificationCount(ModAttachment.C_TERM));
    }

    @Test
    void testReplace()
    {
        Peptide peptide = Peptide.parse("ISL");
        assertEquals(Peptide.parse("JSJ"), peptide.replace(Sets.newHashSet(AminoAcid.I, AminoAcid.L), AminoAcid.J));

        Modification mod = mockMod("mod", 45);
        peptide = new Peptide.Builder("AALAYIYEAVNK").addModification(5, mod).build();

        assertEquals(new Peptide.Builder("AAJAYJYEAVNK").addModification(5, mod).build(),
                peptide.replace(Sets.newHashSet(AminoAcid.I, AminoAcid.L), AminoAcid.J));
    }
}