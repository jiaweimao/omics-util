package omics.util.protein.database;

import com.google.common.collect.Sets;
import omics.util.io.FileUtils;
import omics.util.io.FilenameUtils;
import omics.util.string.Alphabet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Jun 2019, 9:35 PM
 */
class FastaSequenceTest
{
    private static FastaSequence sequence;

    @BeforeAll
    static void setUp()
    {
        String file = FileUtils.getResource("test.fasta").getFile();
        file = file.substring(1);
        sequence = new FastaSequence(file);
    }

    @Test
    void testgetProteinEntry()
    {
        assertEquals(sequence.getProteinEntry(248), "MAVMAPRTLLLLLSGALALTQTWAGSHSMRYFFTSVSRPGRGEPRFIAVGYVDDTQFVRFDSDAASQRMEPRAPWIEQEGPEYWDQETRNVKAQSQTDRVDLGTLRGYYNQSEAGSHTIQIMYGCDVGSDGRFLRGYRQDAYDGKDYIALNEDLRSWTAADMAAQITKRKWEAAHEAEQLRAYLDGTCVEWLRRYLENGKETLQRTDPPKTHMTHHPISDHEATLRCWALGFYPAEITLTWQRDGEDQTQDTELVETRPAGDGTFQKWAAVVVPSGEEQRYTCHVQHEGLPKPLTLRWELSSQPTIPIVGIIAGLVLLGAVITGAVVAAVMWRRKSSDRKGGSYTQAASSDSAQGSDVSLTACKV");
        int endIndex = sequence.getEndIndex(1);
        assertEquals(sequence.getAccession(endIndex), "P61981");
        endIndex = sequence.getEndIndex(sequence.size() - 10);
        assertEquals(sequence.getAccession(endIndex), "A7E2S9-2");
    }

    @Test
    void testGetAlphabet()
    {
        Alphabet alphabet = sequence.getAlphabet();
        assertEquals(alphabet.getBase(), "_?ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        assertEquals(alphabet.getRadix(), 28);
    }

    @Test
    void testGetSize()
    {
        assertEquals(sequence.size(), 2325);
    }

    @Test
    void testGetIndex()
    {
        // start
        int index = 0;
        assertEquals(sequence.getStartIndex(index), 0);
        assertEquals(sequence.getEndIndex(index), 248);

        // middle
        index = 124;
        assertEquals(sequence.getStartIndex(index), 0);
        assertEquals(sequence.getEndIndex(index), 248);

        // end
        index = 247;
        assertEquals(sequence.getStartIndex(index), 0);
        assertEquals(sequence.getEndIndex(index), 248);

        index = 248;
        assertEquals(sequence.getStartIndex(index), 248);
        assertEquals(sequence.getEndIndex(index), 614);

        index = 2324;
        System.out.println(sequence.getStartIndex(index));
        System.out.println(sequence.getEndIndex(index));
    }


    @Test
    void testSet()
    {
        TreeSet<Integer> set = new TreeSet<>(Arrays.asList(248, 614, 980, 1346, 1709, 2072, 2324));
        int index = 248;
        System.out.println(set.floor(index));
        System.out.println(set.ceiling(index));
    }

    @Test
    void testgetProteinLengths()
    {
        assertEquals(sequence.getProteinLengthSet(), Sets.newHashSet(251, 365, 247, 362));
        assertEquals(sequence.getProteinCount(365), 3);
    }

    @Test
    void testGetNumberOfProteins()
    {
        assertEquals(sequence.getNumberOfProteins(), 7);
    }

    @Test
    void testGetBaseFilePath()
    {
        String name = FilenameUtils.getName(sequence.getBaseFilePath());
        assertEquals(name, "test");
    }

    @Test
    void testGetByte()
    {
        Alphabet alphabet = sequence.getAlphabet();
        byte aByte = sequence.getByte(0);
        assertEquals(alphabet.toChar(aByte), '_');
        assertEquals(alphabet.toChar(sequence.getByte(1)), 'M');
    }

    @Test
    void testGetBytes()
    {
        byte[] bytes = sequence.getBytes(0, 3);
        Alphabet alphabet = sequence.getAlphabet();
        String aas = alphabet.toChars(bytes);
        assertEquals(aas, "_MV");
        assertEquals("MVDREQLVQ", alphabet.toChars(sequence.getBytes(1, 10)));
    }

    @Test
    void testGetChar()
    {
        assertEquals('_', sequence.getChar(0));
        assertEquals('M', sequence.getChar(1));
        assertEquals('_', sequence.getChar(sequence.size() - 1));
    }

    @Test
    void testGetDecoyProteinRatio()
    {
        assertEquals(0., sequence.getDecoyProteinRatio(), 1e-6);
    }

    @Test
    void testGetProteinIndices()
    {
        Set<Integer> proteinIndices = sequence.getProteinIndices();
        assertEquals(proteinIndices, Sets.newHashSet(Arrays.asList(248, 614, 980, 1346, 1709, 2072, 2324)));

        for (Integer id : proteinIndices) {
            assertEquals(sequence.getChar(id), '_');
        }
    }

    @Test
    void testGetAccession()
    {
        SortedSet<Integer> proteinIndices = sequence.getProteinIndices();
        Iterator<Integer> it = proteinIndices.iterator();
        assertEquals(sequence.getAccession(it.next()), "P61981");
        assertEquals(sequence.getAccession(it.next()), "P04439");
        assertEquals(sequence.getAccession(it.next()), "P30456");
        assertEquals(sequence.getAccession(it.next()), "P10316");
        assertEquals(sequence.getAccession(it.next()), "P30460");
        assertEquals(sequence.getAccession(it.next()), "Q95365");
        assertEquals(sequence.getAccession(it.next()), "A7E2S9-2");

        assertThrows(NoSuchElementException.class, it::next);
    }

    @Test
    void testGetAccessionUpper()
    {
        assertEquals(sequence.getAccessionUpper(0), "P61981");
        assertEquals(sequence.getAccessionUpper(247), "P61981");
        assertEquals(sequence.getAccessionUpper(248), "P04439");
        assertEquals(sequence.getAccessionUpper(2323), "A7E2S9-2");
        assertNull(sequence.getAccessionUpper(2324));
    }

    @Test
    void testGetAnnotation()
    {
        SortedSet<Integer> proteinIndices = sequence.getProteinIndices();
        Iterator<Integer> it = proteinIndices.iterator();
        assertEquals(sequence.getAnnotation(it.next()), "1433G_HUMAN 14-3-3 protein gamma OS=Homo sapiens GN=YWHAG PE=1 SV=2");
        assertEquals(sequence.getAnnotation(it.next()), "1A03_HUMAN HLA class I histocompatibility antigen, A-3 alpha chain OS=Homo sapiens GN=HLA-A PE=1 SV=2");
        assertEquals(sequence.getAnnotation(it.next()), "1A43_HUMAN HLA class I histocompatibility antigen, A-43 alpha chain OS=Homo sapiens GN=HLA-A PE=1 SV=1");
        assertEquals(sequence.getAnnotation(it.next()), "1A69_HUMAN HLA class I histocompatibility antigen, A-69 alpha chain OS=Homo sapiens GN=HLA-A PE=1 SV=2");
        assertEquals(sequence.getAnnotation(it.next()), "1B08_HUMAN HLA class I histocompatibility antigen, B-8 alpha chain OS=Homo sapiens GN=HLA-B PE=1 SV=1");
        assertEquals(sequence.getAnnotation(it.next()), "1B38_HUMAN HLA class I histocompatibility antigen, B-38 alpha chain OS=Homo sapiens GN=HLA-B PE=1 SV=1");
        assertEquals(sequence.getAnnotation(it.next()), "A30BL_HUMAN Isoform 2 of Putative ankyrin repeat domain-containing protein 30B-like OS=Homo sapiens GN=ANKRD30BL");

        assertThrows(NoSuchElementException.class, it::next);
    }

    @Test
    void testGetAnnotationUpper()
    {
        SortedSet<Integer> proteinIndices = sequence.getProteinIndices();
        Iterator<Integer> it = proteinIndices.iterator();
        // the first is skipped
        assertEquals(sequence.getAnnotationUpper(it.next()), "1A03_HUMAN HLA class I histocompatibility antigen, A-3 alpha chain OS=Homo sapiens GN=HLA-A PE=1 SV=2");
        assertEquals(sequence.getAnnotationUpper(it.next()), "1A43_HUMAN HLA class I histocompatibility antigen, A-43 alpha chain OS=Homo sapiens GN=HLA-A PE=1 SV=1");
        assertEquals(sequence.getAnnotationUpper(it.next()), "1A69_HUMAN HLA class I histocompatibility antigen, A-69 alpha chain OS=Homo sapiens GN=HLA-A PE=1 SV=2");
        assertEquals(sequence.getAnnotationUpper(it.next()), "1B08_HUMAN HLA class I histocompatibility antigen, B-8 alpha chain OS=Homo sapiens GN=HLA-B PE=1 SV=1");
        assertEquals(sequence.getAnnotationUpper(it.next()), "1B38_HUMAN HLA class I histocompatibility antigen, B-38 alpha chain OS=Homo sapiens GN=HLA-B PE=1 SV=1");
        assertEquals(sequence.getAnnotationUpper(it.next()), "A30BL_HUMAN Isoform 2 of Putative ankyrin repeat domain-containing protein 30B-like OS=Homo sapiens GN=ANKRD30BL");

        assertNull(sequence.getAnnotationUpper(it.next()));
    }
}