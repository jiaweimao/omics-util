package omics.util.protein.database;

import omics.util.OmicsException;
import omics.util.io.csv.CsvWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Feb 2018, 4:27 PM
 */
class ProteinDBReaderTest
{
    @Test
    void genIDMapping() throws OmicsException, IOException
    {
        ProteinDB proteinDB = ProteinDB.read("D:\\igd\\uniprot_homo_20200627.fasta", true);
        CsvWriter writer = new CsvWriter("D:\\igd\\uniprot_homo_20200627.csv");
        writer.writeRecord(new String[]{"Accession", "Review", "Entry name", "Protein name", "Gene name", "Organism", "Taxonomic identifier"});
        for (Protein protein : proteinDB) {
            UniProtFastaProtein fastaProtein = (UniProtFastaProtein) protein;

            writer.write(fastaProtein.getAccession());
            writer.write(fastaProtein.isReviewed());
            writer.write(fastaProtein.getEntryName());
            writer.write(fastaProtein.getProteinName());
            writer.write(fastaProtein.getGeneName());
            writer.write(fastaProtein.getOrganismName());
            writer.write(fastaProtein.getTaxonomicIdentifier());
            writer.endRecord();
        }
        writer.close();
        proteinDB.clear();
    }

    @Test
    void testGo() throws Exception
    {
        String file = getClass().getClassLoader().getResource("test.fasta").getFile();
        ProteinDBAccessor task = new ProteinDBAccessor(Paths.get(file), true);
        task.go();

        ProteinDB proteinDB = task.getValue();
        assertNotNull(proteinDB);

        List<Protein> proteins = proteinDB.getAllProteins();
        assertEquals(7, proteins.size());

        UniProtFastaProtein entry = (UniProtFastaProtein) proteinDB.get("A7E2S9-2");
        assertEquals("ANKRD30BL", entry.getGeneName());
        assertNull(entry.getProteinExistence());
        assertNull(entry.getSequenceVersion());

        assertEquals(entry.getDescription(), "A30BL_HUMAN Isoform 2 of Putative ankyrin repeat domain-containing protein 30B-like OS=Homo sapiens GN=ANKRD30BL");
        assertEquals("A30BL_HUMAN", entry.getEntryName());
        assertTrue(entry.isReviewed());
        assertEquals("A7E2S9-2", entry.getAccession());
    }
}