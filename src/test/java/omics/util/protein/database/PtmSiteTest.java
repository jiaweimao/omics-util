package omics.util.protein.database;

import omics.util.protein.AminoAcid;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 29 Nov 2017, 3:53 PM
 */
class PtmSiteTest
{
    @Test
    void testParse()
    {
        // simple
        PtmSite site = PtmSite.parse("R3(CD3x2)");
        test(site, null, null, AminoAcid.R, 2, "CD3", 2, null);

        // with score
        site = PtmSite.parse("R3(CD3x2):100.0");
        test(site, null, null, AminoAcid.R, 2, "CD3", 2, 100.0);

        // with protein
        site = PtmSite.parse("P62304:257:R3(CD3x2)");
        test(site, "P62304", 256, AminoAcid.R, 2, "CD3", 2, null);

        // with protein, score
        site = PtmSite.parse("P62304:257:R3(CD3x2):100.0");
        test(site, "P62304", 256, AminoAcid.R, 2, "CD3", 2, 100.0);

        // without pepindex.
        site = PtmSite.parse("P62304:257:R(CD3x2):100.0");
        test(site, "P62304", 256, AminoAcid.R, null, "CD3", 2, 100.0);
    }

    private void test(PtmSite site, String acc, Integer prot_index, AminoAcid aminoAcid, Integer pep_index, String modName, int size, Double score)
    {
        assertEquals(site.getAccession(), acc);
        assertEquals(site.getIndexProtein(), prot_index);
        assertEquals(site.getAminoAcid(), aminoAcid);
        assertEquals(site.getIndexPeptide(), pep_index);
        assertEquals(site.getName(), modName);
        assertEquals(site.getSize(), size);
        assertEquals(site.getScore(), score);
    }

    @Test
    void testhashCode()
    {
        PtmSite site1 = new PtmSite("CH3", "Q86U42", 16, AminoAcid.R, 1);
        site1.setIndexPeptide(15);
        PtmSite site2 = new PtmSite("CH3", "Q86U42", 16, AminoAcid.R, 1);
        site2.setIndexPeptide(15);
        System.out.println(site1.hashCode());
        System.out.println(site2.hashCode());
        System.out.println(site1.hashCode() == site2.hashCode());
    }
}