package omics.util.protein.database;

import omics.util.io.FileUtils;
import omics.util.io.csv.CsvReader;
import omics.util.io.csv.CsvWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 Jun 2019, 4:04 PM
 */
class SuffixIteratorTest
{
    @Test
    void len()
    {
        int min = 6;
        int max = 40;
        for (int i = min; i < 42; i++) {
            System.out.println(i);
        }
    }

    @Test
    void test() throws IOException
    {
        String file = FileUtils.getResource("a_pep.fasta").getFile();
        file = file.substring(1);
        SuffixIterator it = new SuffixIterator(new SuffixArraySequence(file));
        FastaSequence sequence = it.getSequence();
        int count = 0;
        while (it.hasNext()) {
            IndexLCP pair = it.next();
            int index = pair.getIndex();
            int lcp = pair.getLCP();

            String s = sequence.subString(index, sequence.size());
            System.out.println(count + "\t" + index + "\t" + lcp + "\t" + s);
            count++;
        }
        it.close();
    }

    @Test
    void generateSuffix() throws IOException
    {
        SuffixIterator it = new SuffixIterator("D:\\database\\uniprot\\protein101.fasta");
        CsvWriter writer = new CsvWriter("D:\\database\\uniprot\\protein101_suffix.csv");
        FastaSequence sequence = it.getSequence();
        while (it.hasNext()) {
            IndexLCP lcp = it.next();
            writer.writeRecord(String.valueOf(lcp.getIndex()), String.valueOf(lcp.getLCP()),
                    sequence.subStringEndTerm(lcp.getIndex(), lcp.getIndex() + 40));
        }
        writer.close();
    }

    @Test
    void check() throws IOException
    {
        String file1 = "D:\\database\\uniprot\\uniprot-reviewed_yes.csv";
        String file2 = "D:\\database\\uniprot\\uniprot-reviewed_yes_msgf.csv";
        CsvReader reader1 = new CsvReader(file1);
        CsvReader reader2 = new CsvReader(file2);
        int index = 0;
        int err = 0;
//        final int len = 80;
        FastaSequence sequence = new FastaSequence("D:\\database\\uniprot\\uniprot-reviewed_yes.fasta");
        CsvWriter writer = new CsvWriter("D:\\database\\uniprot\\uniprot-reviewed_yes_msgf2util.csv");
        while (reader1.readRecord() && reader2.readRecord()) {
            int id1 = Integer.parseInt(reader1.get(0));
            int id2 = Integer.parseInt(reader2.get(0));

            byte lcp1 = Byte.parseByte(reader1.get(1));
            byte lcp2 = Byte.parseByte(reader2.get(1));

            String seq1 = reader1.get(2);
            String seq2 = reader2.get(2);

            if (id2 != id1 && !seq1.equals(seq2)) {
                err++;
//                System.out.println(index + "\t" + id1 + "\t" + id2);
                writer.write(index);
                writer.write(id1);
                writer.write(id2);
                writer.endRecord();
            }

            index++;
        }
        System.out.println(err);
        writer.close();
        reader1.close();
        reader2.close();
    }
}