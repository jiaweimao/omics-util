package omics.util.protein.database;

import omics.util.OmicsException;
import omics.util.io.csv.CsvWriter;
import omics.util.protein.database.uniprot.UniProtTxtReader;
import omics.util.utils.IntPair;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 09 Nov 2017, 4:05 PM
 */
class UniProtTxtProteinTest
{
    @Test
    void testSinglePeptide() throws IOException
    {
        UniProtTxtReader reader = new UniProtTxtReader(Files.newInputStream(Paths.get("D:\\data\\fasta\\uniprot-proteome_UP000186698.txt")));
        PrintWriter writer = new PrintWriter("D:\\data\\fasta\\uniprot-proteome_UP000186698_removeSP.fasta");
        CsvWriter logWriter = new CsvWriter("D:\\data\\fasta\\uniprot-proteome_UP000186698_removeSP.csv");
        int count = 0;
        while (reader.hasNext()) {
            UniProtTxtProtein protein = reader.next();
            IntPair signalPeptide = protein.getSignalPeptide();
            if(signalPeptide.getSecond() != -1){
                String sequence = protein.getSequence();
                String substring = sequence.substring(signalPeptide.getSecond());
                protein.setSequence(substring);
                logWriter.write(protein.getAccession());
                logWriter.write(signalPeptide.getFirst());
                logWriter.write(signalPeptide.getSecond());
                logWriter.endRecord();
            }
            protein.writeToFasta(writer);
        }
        writer.close();
        logWriter.close();
        reader.close();
    }

    @Test
    void testGetGlycosylationSites() throws IOException, OmicsException
    {
        ProteinDB proteinDB = ProteinDB.read("D:\\database\\uniprot\\uniprot_human_review_20190628.txt.gz");
        CsvWriter writer = new CsvWriter("D:\\database\\uniprot\\uniprot_human_review_20190628_gly.csv");
        for (Protein protein : proteinDB) {
            UniProtTxtProtein txtProtein = (UniProtTxtProtein) protein;
            List<PtmSite> glycosylationSites = txtProtein.getGlycosylationSites();
            if (glycosylationSites.isEmpty())
                continue;

            for (PtmSite site : glycosylationSites) {
                writer.write(protein.getAccession());
                writer.write(site.getName());
                writer.write(site.getIndexProtein() + 1);
                writer.write(site.getAminoAcid());
                writer.endRecord();
            }
        }
        writer.close();
        proteinDB.clear();
    }

    @Test
    void testSites() throws IOException, OmicsException
    {
        ProteinDB proteinDB = ProteinDB.read("D:\\database\\uniprot-proteome_UP000005640.txt.gz");
        try (CsvWriter writer = new CsvWriter("D:\\database\\uniprot-proteome_UP000005640_sites.csv");) {

            writer.writeRecord(new String[]{"Entry", "Entry name", "index", "type", "description"});
            for (Protein protein : proteinDB) {
                UniProtTxtProtein txtProtein = (UniProtTxtProtein) protein;
                List<Site> activitySites = txtProtein.getActivitySites();
                for (Site site : activitySites) {
                    writer.write(txtProtein.getAccession());
                    writer.write(txtProtein.getEntryName());
                    writer.write(site.getIndex());
                    writer.write("activity of an enzyme");
                    writer.write(site.getDescription());
                    writer.endRecord();
                }
                for (Site site : txtProtein.getBindingSites()) {
                    writer.write(txtProtein.getAccession());
                    writer.write(txtProtein.getEntryName());
                    writer.write(site.getIndex());
                    writer.write("Binding site");
                    writer.write(site.getDescription());
                    writer.endRecord();
                }
                for (Site site : txtProtein.getSites()) {
                    writer.write(txtProtein.getAccession());
                    writer.write(txtProtein.getEntryName());
                    writer.write(site.getIndex());
                    writer.write("site");
                    writer.write(site.getDescription());
                    writer.endRecord();
                }
            }
        }
        proteinDB.clear();
    }


    @Test
    void getSequenceWindowX()
    {
        UniProtTxtProtein entry = new UniProtTxtProtein();
        entry.setSequence("XRHLAPTGNAPASRGLPTTTQRVGSECPDRLAMDFGGAGAAQQGLTDSCQSGGVPTAVQN" +
                "LAPRAAVAAAAPRAVAPYKYASSVRSPHPAIQPLQAPQPAVHVQGQEPLTASMLAAAPPQ" +
                "EQKQMLGERLFPLIQTMHSNLAGKITGMLLEIDNSELLHMLESPESLRSKVDEAVAVLQA" +
                "HHAKKEAAQKDSKAK");

        int[] ids = entry.indexOf("IRHLAPTGNAPASR", true);
        assertEquals(1, ids.length);
        assertEquals(0, ids[0]);

        String window = entry.subString(1, 10, 10);
        assertEquals("_________XRHLAPTGNAPA", window);
    }

    @Test
    void subString_sym()
    {
        UniProtTxtProtein entry = new UniProtTxtProtein();
        entry.setSequence("ABCDEFG");

        assertEquals("_ABCDEF", entry.subString(2, 3, 3, '_'));
        assertEquals("ABCDEFG", entry.subString(3, 3, 3, '_'));
        assertEquals("BCDEFG_", entry.subString(4, 3, 3, '_'));
    }

}