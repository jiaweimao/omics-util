package omics.util.protein.database.uniprot;

import omics.util.OmicsException;
import omics.util.io.FileUtils;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinDB;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class FastaAccessorTest
{
    @Test
    void execute() throws OmicsException
    {
        ProteinDB result = ProteinDB.read(FileUtils.getResource("test.fasta").getFile());

        assertNotNull(result);
        assertEquals(result.size(), 7);

        Protein entry = result.get("P61981");
        assertEquals("MVDREQLVQKARLAEQAERYDDMAAAMKNVTELNEPLSNEERNLLSVAYKNVVGARRSSW" +
                "RVISSIEQKTSADGNEKKIEMVRAYREKIEKELEAVCQDVLSLLDNYLIKNCSETQYESK" +
                "VFYLKMKGDYYRYLAEVATGEKRATVVESSEKAYSEAHEISKEHMQPTHPIRLGLALNYS" +
                "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQQDD" +
                "DGGEGNN", entry.getSequence());
        assertEquals("1433G_HUMAN 14-3-3 protein gamma OS=Homo sapiens GN=YWHAG PE=1 SV=2", entry.getDescription());

        entry = result.get("P04439");
        assertEquals("MAVMAPRTLLLLLSGALALTQTWAGSHSMRYFFTSVSRPGRGEPRFIAVGYVDDTQFVRF" +
                "DSDAASQRMEPRAPWIEQEGPEYWDQETRNVKAQSQTDRVDLGTLRGYYNQSEAGSHTIQ" +
                "IMYGCDVGSDGRFLRGYRQDAYDGKDYIALNEDLRSWTAADMAAQITKRKWEAAHEAEQL" +
                "RAYLDGTCVEWLRRYLENGKETLQRTDPPKTHMTHHPISDHEATLRCWALGFYPAEITLT" +
                "WQRDGEDQTQDTELVETRPAGDGTFQKWAAVVVPSGEEQRYTCHVQHEGLPKPLTLRWEL" +
                "SSQPTIPIVGIIAGLVLLGAVITGAVVAAVMWRRKSSDRKGGSYTQAASSDSAQGSDVSL" +
                "TACKV", entry.getSequence());

        entry = result.get("Q95365");
        assertEquals("MLVMAPRTVLLLLSAALALTETWAGSHSMRYFYTSVSRPGRGEPRFISVGYVDDTQFVRF" +
                "DSDAASPREEPRAPWIEQEGPEYWDRNTQICKTNTQTYRENLRIALRYYNQSEAGSHTLQ" +
                "RMYGCDVGPDGRLLRGHNQFAYDGKDYIALNEDLSSWTAADTAAQITQRKWEAARVAEQL" +
                "RTYLEGTCVEWLRRYLENGKETLQRADPPKTHVTHHPISDHEATLRCWALGFYPAEITLT" +
                "WQRDGEDQTQDTELVETRPAGDRTFQKWAAVVVPSGEEQRYTCHVQHEGLPKPLTLRWEP" +
                "SSQSTVPIVGIVAGLAVLAVVVIGAVVAAVMCRRKSSGGKGGSYSQAASSDSAQGSDVSL" +
                "TA", entry.getSequence());

        result.clear();
    }
}