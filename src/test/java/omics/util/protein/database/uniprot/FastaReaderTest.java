package omics.util.protein.database.uniprot;

import omics.util.protein.database.NCBIProtein;
import omics.util.protein.database.Protein;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 May 2018, 3:30 PM
 */
class FastaReaderTest
{
    @Test
    void test() throws IOException
    {
        FastaReader reader = new FastaReader(getClass().getClassLoader().getResourceAsStream("test.fasta"));
        int count = 0;
        while (reader.hasNext()) {
            Protein entry = reader.next();
            count++;
        }
        reader.close();

        assertEquals(count, 7);
    }

    @Test
    void testNCBI() throws IOException
    {
        String ncbiFasta = ">gi|53828740|ref|NP_001005484.1| olfactory receptor 4F5 [Homo sapiens]\n" +
                "MVTEFIFLGLSDSQELQTFLFMLFFVFYGGIVFGNLLIVITVVSDSHLHSPMYFLLANLSLIDLSLSSVT\n" +
                "APKMITDFFSQRKVISFKGCLVQIFLLHFFGGSEMVILIAMGFDRYIAICKPLHYTTIMCGNACVGIMAV\n" +
                "TWGIGFLHSVSQLAFAVHLLFCGPNEVDSFYCDLPRVIKLACTDTYRLDIMVIANSGVLTVCSFVLLIIS\n" +
                "YTIILMTIQHRPLDKSSKALSTLTAHITVVLLFFGPCVFIYAWPFPIKSLDKFLAVFYSVITPLLNPIIY\n" +
                "TLRNKDMKTAIRQLRKWDAHSSVKF\n" +
                ">gi|767901760|ref|XP_011542107.1| PREDICTED: uncharacterized protein LOC102725121 isoform X2 [Homo sapiens]\n" +
                "MSDSINFSHNLGQLLSPPRCVVMPGMPFPSIRSPELQKTTADLDHTLVSVPSVAESLHHPEITFLTAFCL\n" +
                "PSFTRSRPLPDRQLHHCLALCPSFALPAGDGVCHGPGLQGSCYKGETQESVESRVLPGPRHRH\n" +
                ">gi|767901762|ref|XP_011542108.1| PREDICTED: uncharacterized protein LOC102725121 isoform X2 [Homo sapiens]\n" +
                "MSDSINFSHNLGQLLSPPRCVVMPGMPFPSIRSPELQKTTADLDHTLVSVPSVAESLHHPEITFLTAFCL\n" +
                "PSFTRSRPLPDRQLHHCLALCPSFALPAGDGVCHGPGLQGSCYKGETQESVESRVLPGPRHRH";
        StringReader reader = new StringReader(ncbiFasta);
        FastaReader fastaReader = new FastaReader(reader, false);

        while (fastaReader.hasNext()) {
            Protein protein = fastaReader.next();
            assertTrue(protein instanceof NCBIProtein);
            System.out.println(protein.getAccession()+"\t"+protein.getDescription());
        }
        fastaReader.close();
    }

    @Test
    void count() throws IOException
    {
        FastaReader reader = new FastaReader("D:\\igd\\fasta\\homo_20191210_add.fasta");
        int count = 0;
        int pep = 0;
        int pre = 0;
        while (reader.hasNext()) {
            Protein protein = reader.next();
            if (protein instanceof NCBIProtein) {
                NCBIProtein ncbiProtein = (NCBIProtein) protein;
                System.out.println(ncbiProtein.getAccession() + "\t" + ncbiProtein.getGI()+"\t"+ncbiProtein.getDescription());
            } else {
                String accession = protein.getAccession();
                System.out.println(accession);
            }

            count++;
        }
        reader.close();
        System.out.println("ALL=" + count);
        System.out.println("PROTEIN=" + (count - pep));
        System.out.println("PEP=" + pep);
        System.out.println("Predicated=" + pre);
    }
}