package omics.util.protein.database.uniprot;

import omics.util.protein.database.UniProtTxtProtein;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class UniProtKBFastaHeaderParserTest
{
    @Test
    void spTest()
    {
        String header = ">sp|P27748|ACOX_RALEH Acetoin catabolism protein X OS=Ralstonia eutropha (strain ATCC 17699 " +
                "/ H16 / DSM 428 / Stanier 337) GN=acoX PE=4 SV=2";

        UniProtKBFastaHeaderParser parser = new UniProtKBFastaHeaderParser();
        assertTrue(parser.test(header));

        UniProtTxtProtein proteinEntry = new UniProtTxtProtein();
        parser.parse(header, proteinEntry);

        assertEquals("P27748", proteinEntry.getAccession());
        assertEquals("ACOX_RALEH Acetoin catabolism protein X OS=Ralstonia eutropha (strain ATCC 17699 " +
                "/ H16 / DSM 428 / Stanier 337) GN=acoX PE=4 SV=2", proteinEntry.getDescription());

        assertEquals("ACOX_RALEH", proteinEntry.getEntryName());
        assertEquals("ACOX_RALEH Acetoin catabolism protein X OS=Ralstonia eutropha (strain ATCC 17699 / H16 / DSM 428 / Stanier 337) GN=acoX PE=4 SV=2", proteinEntry.getDescription());
        assertEquals("Ralstonia eutropha (strain ATCC 17699 / H16 / DSM 428 / Stanier 337)", proteinEntry.getOrganismName());
        assertEquals("acoX", proteinEntry.getGeneName());
        assertEquals("4", proteinEntry.getProteinExistence());
        assertEquals("2", proteinEntry.getSequenceVersion());
        assertTrue(proteinEntry.isReviewed());
    }

    @Test
    void trTest()
    {
        String header = ">tr|Q9TS58|Q9TS58_BOVIN B-43=43 kDa serine proteinase inhibitor-like PROTEIN|SERPIN-like " +
                "protein (Fragments) OS=Bos taurus PE=1 SV=1";

        UniProtTxtProtein entry = new UniProtTxtProtein();

        UniProtKBFastaHeaderParser parser = new UniProtKBFastaHeaderParser();
        assertTrue(parser.test(header));

        parser.parse(header, entry);

        assertFalse(entry.isReviewed());
        assertEquals("Q9TS58", entry.getAccession());
        assertEquals("Q9TS58_BOVIN B-43=43 kDa serine proteinase inhibitor-like PROTEIN|SERPIN-like " +
                "protein (Fragments) OS=Bos taurus PE=1 SV=1", entry.getDescription());
        assertEquals("Q9TS58_BOVIN", entry.getEntryName());
        assertNull(entry.getGeneName());
    }

}