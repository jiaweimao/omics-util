package omics.util.protein.database.util;

import omics.util.io.FileType;
import omics.util.io.FilenameUtils;
import omics.util.io.csv.CsvReader;
import omics.util.io.csv.CsvWriter;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinDB;
import omics.util.utils.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 23 Jan 2018, 9:05 PM
 */
class DoCalculateSequenceWindowTest
{
    @Test
    void testGo() throws Exception
    {
        ProteinDB proteinDB = ProteinDB.read("Z:\\MaoJiawei\\data\\LiuXiaoyan\\human_reviewed_2017_0.fasta");
        int size = 15;
        File[] files = FileType.CSV.listFiles(new File("Z:\\LiuXiaoyan\\LiuXiaoyan\\20181130"));
        for (File file : files) {
            CsvReader reader = new CsvReader(file.getAbsolutePath());
            CsvWriter writer = new CsvWriter(FilenameUtils.appendSuffix(file.getAbsolutePath(), "_window"));
            writer.writeRecord(new String[]{"Peptide", "Protein", "Y Count", "Window"});

            reader.readHeaders();
            while (reader.readRecord()) {
                String seq = reader.get(0);
                String proteins = reader.get(1);

                int[] yIndexes = StringUtils.indexOf(seq, "Y");

                String[] proteinArray = proteins.split(";");
                for (String protein : proteinArray) {
                    Protein protein1 = proteinDB.get(protein.trim());
                    if (protein1 == null)
                        continue;

                    int[] peptideIndexes = protein1.indexOf(seq, true);
                    if (peptideIndexes.length == 0)
                        continue;

                    for (int peptideIndex : peptideIndexes) {
                        for (int yIndex : yIndexes) {
                            String window = protein1.subString(peptideIndex + yIndex, size, size);
                            writer.write(seq);
                            writer.write(protein);
                            writer.write(yIndexes.length);
                            writer.write(window);
                            writer.endRecord();
                        }
                    }
                }
            }


            reader.close();
            writer.close();
        }
    }
}