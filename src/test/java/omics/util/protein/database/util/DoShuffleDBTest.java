package omics.util.protein.database.util;

import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 22 Jan 2018, 4:38 PM
 */
class DoShuffleDBTest
{
    @Test
    void testGo() throws Exception
    {
        DoShuffleDB task = new DoShuffleDB(new File("D:\\NMDA.fasta"));
        task.go();
    }
}