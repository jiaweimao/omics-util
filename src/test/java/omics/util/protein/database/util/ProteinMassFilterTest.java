package omics.util.protein.database.util;

import omics.util.interfaces.Filter;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinFilter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Jul 2020, 10:40 AM
 */
class ProteinMassFilterTest
{
    @Test
    void testMassFilter()
    {
        final Protein pass = new Protein(">Protein that passes filter", "LENNAR");
        final Protein noPass1 = new Protein(">Protein that does not pass filter", "LENNARTMARTENS");
        final Protein noPass2 = new Protein(">Protein that does not pass filter", "L");

        // Simplest constructor.
        ProteinFilter pf = new ProteinMassFilter(600.0, 1000.0);
        assertTrue(pass.passFilter(pf));
        assertFalse(pf.test(noPass1));
        assertFalse(pf.test(noPass2));

        // Constructor with flag for inversion (set).
        Filter<Protein> filter = new ProteinMassFilter(600.0, 1000.0).negate();
        assertFalse(filter.test(pass));
        assertTrue(filter.test(noPass1));
        assertTrue(filter.test(noPass2));
    }
}