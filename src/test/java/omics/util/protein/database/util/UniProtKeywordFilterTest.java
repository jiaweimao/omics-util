package omics.util.protein.database.util;

import omics.util.io.FileUtils;
import omics.util.protein.database.UniProtTxtProtein;
import omics.util.protein.database.uniprot.UniProtTxtReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 05 Jul 2020, 11:30 AM
 */
class UniProtKeywordFilterTest
{
    @Test
    void test() throws IOException
    {
        UniProtKeywordFilter filter = new UniProtKeywordFilter("Mitosis");
        final String file = "test.spr";
        UniProtTxtReader reader = new UniProtTxtReader(Files.newInputStream(FileUtils.getResourcePath(file)));
        assertTrue(reader.hasNext());
        UniProtTxtProtein protein = reader.next();
        assertTrue(filter.test(protein));

        reader.close();
    }

}