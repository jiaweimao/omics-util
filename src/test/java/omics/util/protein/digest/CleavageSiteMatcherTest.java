package omics.util.protein.digest;


import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import omics.util.protein.mod.Modification;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;


class CleavageSiteMatcherTest
{
    @Test
    @ParameterizedTest()
    void testNothing(String str)
    {
        Pattern.matches("(?!)", "AAKT");
        Pattern pattern = Pattern.compile("(?!)");
    }

    @Test
    void test()
    {
        Enzyme enzyme = Enzyme.ofName("Glu-C");
        System.out.println(enzyme.getCleavageSiteMatcher().getRegex().pattern());
        Enzyme chymotrypsinHighSpec = Enzyme.CHYMOTRYPSIN_HIGH_SPEC;
        System.out.println(chymotrypsinHighSpec.getCleavageSiteMatcher().getRegex().pattern());
    }


    @Test
    void testIsCTermCleavable()
    {
        CleavageSiteMatcher trypsin = new CleavageSiteMatcher("[KR]|[^P]");
        assertTrue(trypsin.isCTermCleavable("KACDEFGRS"));
        assertTrue(trypsin.isCTermCleavable("RACDEFGH_"));
        assertFalse(trypsin.isCTermCleavable("KACDEFGRP"));
        assertThrows(IllegalArgumentException.class, () -> trypsin.isCTermCleavable(""));
    }

    @Test
    void testIsNTermCleavable()
    {
        CleavageSiteMatcher trypsin = new CleavageSiteMatcher("[KR]|[^P]");
        assertTrue(trypsin.isNTermCleavable("KACDEFGRS"));
        assertTrue(trypsin.isNTermCleavable("_RACDEFGH"));
        assertFalse(trypsin.isNTermCleavable("KPCDEFGR"));
        assertThrows(IllegalArgumentException.class, () -> trypsin.isNTermCleavable(""));
    }


    @Test
    void testOr()
    {
        CleavageSiteMatcher matcher1 = new CleavageSiteMatcher("[KR]|[^P]");
        CleavageSiteMatcher matcher2 = new CleavageSiteMatcher("X|[ST]");
        String trp = "CDEFKA";
        String op = "ASKPLRM";
        String both = "MSMRLKJKA";

        CleavageSiteMatcher matcher = new CleavageSiteMatcher(matcher1.getCleavageFormat() + " or " + matcher2.getCleavageFormat());
        Enzyme enzyme1 = new Enzyme(matcher1);
        Enzyme enzyme2 = new Enzyme(matcher2);
        Enzyme enzyme = new Enzyme(matcher);

        IntList list = new IntArrayList();
        int i = enzyme1.find(trp, list);
        assertEquals(i, 1);
        assertEquals(list.getInt(0), 5);
        list.clear();

        int i1 = enzyme2.find(op, list);
        assertEquals(i1, 1);
        assertEquals(list.getInt(0), 1);

        list.clear();
        int i2 = enzyme.find(both, list);
        assertEquals(i2, 4);
        assertArrayEquals(list.toIntArray(), new int[]{1, 4, 6, 8});
        System.out.println(matcher.getRegex().pattern());
    }

    @Test
    void testCterm()
    {
        CleavageSiteMatcher csm = new CleavageSiteMatcher("KR|^P");
        assertEquals("KR|^P", csm.getCleavageFormat());
        Optional<Modification> ctermMod = csm.getCtermMod();
        assertFalse(ctermMod.isPresent());
        Optional<Modification> ntermMod = csm.getNtermMod();
        assertFalse(ntermMod.isPresent());

        assertEquals("(?<=KR)(?=^P)", csm.getRegex().pattern());
    }

    @Test
    void testTrypsin()
    {
        CleavageSiteMatcher csm = new CleavageSiteMatcher("[KR]|X");
        assertEquals(csm.getCleavageFormat(), "[KR]|X");

        Pattern regex = csm.getRegex();
        assertEquals(regex.pattern(), "(?<=[KR])(?=[A-Z])");

        Matcher matcher = regex.matcher("MVDREQLVQKARLA");
        int st = 0;
        while (matcher.find(st)) {
            System.out.println(matcher.start());
            st = matcher.start() + 1;
        }
    }

    @Test
    void testPattern()
    {
        CleavageSiteMatcher cs = new CleavageSiteMatcher("M|X");
        assertEquals("(?<=M)(?=[A-Z])", cs.getRegex().pattern());
    }

    @Test
    void testMissingCleavageToken() throws CleavageSiteParseException
    {
        assertThrows(CleavageSiteParseException.class, () -> new CleavageSiteMatcher("[KR][^P]"));
    }

    @Test
    void testTooManyCleavageTokens() throws CleavageSiteParseException
    {
        assertThrows(CleavageSiteParseException.class, () -> new CleavageSiteMatcher("[KR]|[^P]|K"));
    }
}