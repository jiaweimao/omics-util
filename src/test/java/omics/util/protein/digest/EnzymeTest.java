package omics.util.protein.digest;

import it.unimi.dsi.fastutil.ints.IntAVLTreeSet;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntSortedSet;
import omics.util.protein.Protein;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 Oct 2017, 2:13 PM
 */
class EnzymeTest
{
    @Test
    void testSet()
    {
        EnzymeFactory instance = EnzymeFactory.getInstance();
        List<Enzyme> enzymeList = instance.getItemList();
        for (Enzyme enzyme : enzymeList) {
            System.out.println(enzyme.getName() + "\t" + enzyme.getMode() + "\t" + enzyme.getResidueSet());
        }

    }

    @Test
    void testhasCleavageSite()
    {
        Enzyme unspecific = Enzyme.ofName("unspecific cleavage");
        assertTrue(unspecific.hasCleavageSite());
        assertEquals(unspecific.getMode(), Enzyme.Mode.A);
    }

    @Test
    void testContainResidue()
    {
        Enzyme trypsin = Enzyme.ofName("Trypsin");
        assertTrue(trypsin.containResidue('K'));
        assertTrue(trypsin.containResidue('R'));
        assertFalse(trypsin.containResidue('T'));
        assertFalse(trypsin.containResidue('P'));
    }

    @Test
    void testGetTerminiCount0()
    {
        Enzyme enzyme = Enzyme.ofName("Trypsin/P");
        // full tryptic
        assertEquals(enzyme.getTerminiCount("KACDEFGRS"), 2);
        assertEquals(enzyme.getTerminiCount("RACDEFGRS"), 2);
        assertEquals(enzyme.getTerminiCount("_ACDEFGRS"), 2); // protein n-term
        assertEquals(enzyme.getTerminiCount("RACDEFGH_"), 2); // protein c-term
        assertEquals(enzyme.getTerminiCount("KACDEFGHS"), 1);
        assertEquals(enzyme.getTerminiCount("LACDEFGRS"), 1);
        assertEquals(enzyme.getTerminiCount("KACDEFGRP"), 2);
        assertEquals(enzyme.getTerminiCount("KPCDEFGRS"), 2);

        assertEquals(enzyme.getTerminiCount("LACDEFGHS"), 0);
        assertEquals(enzyme.getTerminiCount("_ACDEFGHS"), 1);
        assertEquals(enzyme.getTerminiCount("LACDEFGH_"), 1);
    }

    @Test
    void testTerminiCountTryp()
    {
        Enzyme trypsin = Enzyme.ofName("Trypsin");
        assertEquals(trypsin.getTerminiCount("KACDEFGRP"), 1);
        assertEquals(trypsin.getTerminiCount("KACDEFGRS"), 2);
    }

    @Test
    void testCountCleavageSites()
    {
        Enzyme finder = new Enzyme(new CleavageSiteMatcher("[KR]|[^P]"));
        assertEquals(3, finder.countCleavageSites(new Protein("ACC1", "RESALYTNIKALASKR")));
    }

    @Test
    void testCountCleavageSites2()
    {
        Enzyme finder = new Enzyme(new CleavageSiteMatcher("[KR]|[^P]"));
        assertEquals(0, finder.countCleavageSites(new Protein("ACC1", "ESALYTNI")));
    }

    @Test
    void testFindCleavageSites()
    {
        Enzyme finder = new Enzyme(new CleavageSiteMatcher("[KR]|[^P]"));

        IntList sites = new IntArrayList();
        finder.find(new Protein("ACC1", "RESALYTNIKALASKR"), sites);

        assertEquals(3, sites.size());
        assertArrayEquals(new int[]{1, 10, 15}, sites.toIntArray());
    }

    @Test
    void testTerminal()
    {
        Enzyme finder = new Enzyme(new CleavageSiteMatcher("[KR]|[^P]"));

        IntList sites = new IntArrayList();
        finder.find(new Protein("ACC1", "RESALYTNIKALASKR"), sites);

        assertEquals(3, sites.size());
        assertArrayEquals(new int[]{1, 10, 15}, sites.toIntArray());
    }

    @Test
    void testTerm()
    {
        Enzyme finder = new Enzyme(new CleavageSiteMatcher("[KR]|[^P]"));

        IntList sites = new IntArrayList();
        int count = finder.find("RESALYTNIKALASKR_", sites);

        assertEquals(4, count);
        assertEquals(Arrays.asList(1, 10, 15, 16), sites);
    }

    @Test
    void testDoFind()
    {
        Enzyme enzyme = Enzyme.ofName("Trypsin");
        IntSortedSet set = new IntAVLTreeSet();
        enzyme.find("ASGEPTSTPTTEAVESTVATLEDSPEVIESPP", set);
        System.out.println(set);
    }
}