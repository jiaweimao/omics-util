package omics.util.protein.digest;

import omics.util.io.FileUtils;
import omics.util.protein.AminoAcid;
import omics.util.protein.AminoAcidSet;
import omics.util.protein.PeptideMod;
import omics.util.protein.database.FastaSequence;
import omics.util.protein.mod.PTM;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 02 Jul 2019, 1:35 PM
 */
class PeptideGeneratorTest
{
    private static FastaSequence fastaSequence;
    private static AminoAcidSet aaSet;

    @BeforeAll
    static void setUp()
    {
        fastaSequence = new FastaSequence(FileUtils.getResourcePath("a_pep.fasta").toString());

        List<PeptideMod> list = new ArrayList<>();
        list.add(PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C, true));
        list.add(PeptideMod.of(PTM.Oxidation(), AminoAcid.M));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));

        aaSet = AminoAcidSet.getAminoAcidSet(list);
    }

    @Test
    void testNoMod()
    {
        PeptideGenerator gen = new PeptideGenerator(aaSet);
        gen.add("A");
        assertEquals(gen.size(), 1);

        gen.add("AA");
        assertEquals(gen.size(), 1);

        gen.add("AAA");
        assertEquals(gen.size(), 1);
        assertEquals(gen.get(0), "AAA");
    }

    @Test
    void testMod()
    {
        PeptideGenerator gen = new PeptideGenerator(aaSet);

        PeptideSpectrumGenerator spectrumGenerator = new PeptideSpectrumGenerator(aaSet, 40);

        gen.add("S");
        assertEquals(gen.size(), 2);
        assertEquals(gen.get(0), "S");
        assertEquals(gen.get(1), "s");
        spectrumGenerator.addPeptide("S");
        assertEquals(spectrumGenerator.getPeptideMass(), AminoAcid.S.getMolecularMass());

        gen.add("ST");
        assertEquals(gen.size(), 4);
        assertEquals(gen.get(0), "ST");
        assertEquals(gen.get(1), "sT");
        assertEquals(gen.get(2), "St");
        assertEquals(gen.get(3), "st");
        spectrumGenerator.addPeptide("st");
        assertEquals(spectrumGenerator.getPeptideMass(), AminoAcid.S.getMolecularMass() + AminoAcid.T.getMolecularMass() + PTM.Phospho().getMolecularMass() * 2);

        gen.add("STY");
        assertEquals(gen.size(), 8);
        assertEquals(gen.get(0), "STY");
        assertEquals(gen.get(1), "sTY");
        assertEquals(gen.get(2), "StY");
        assertEquals(gen.get(3), "stY");
        assertEquals(gen.get(4), "STy");
        assertEquals(gen.get(5), "sTy");
        assertEquals(gen.get(6), "Sty");
        assertEquals(gen.get(7), "sty");
    }

    @Test
    void testMod_UnMod()
    {
        PeptideGenerator gen = new PeptideGenerator(aaSet);
        gen.add("S");
        assertEquals(gen.size(), 2);
        gen.add("SA");
        assertEquals(gen.size(), 2);

        gen.add("SAY");
        assertEquals(gen.size(), 4);
        assertEquals(gen.get(0), "SAY");
        assertEquals(gen.get(1), "sAY");
        assertEquals(gen.get(2), "SAy");
        assertEquals(gen.get(3), "sAy");
    }

    @Test
    void testStandardAminoacids()
    {
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSet();

        PeptideGenerator generator = new PeptideGenerator(aaSet, 40, 3, 128, fastaSequence);
        generator.addWithEnd(0, 21);
        assertEquals(generator.size(), 1);
        assertEquals(generator.get(0), "MKWVTFISLLLLFSSAYSR");
        assertEquals(generator.getLength(), 19);
    }

    @Test
    void testFixedMod()
    {
        PeptideMod mod = PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C, true);
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(Collections.singletonList(mod));

        PeptideGenerator generator = new PeptideGenerator(aaSet, 40, 3, 128, fastaSequence);
        generator.addWithEnd(0, 21);
        assertEquals(generator.size(), 1);
        assertEquals(generator.get(0), "MKWVTFISLLLLFSSAYSR");
        assertEquals(generator.getLength(), 19);
    }

    @Test
    void testaddPeptide()
    {
        List<PeptideMod> list = new ArrayList<>();
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(list);
        PeptideGenerator generator = new PeptideGenerator(aaSet, 40, 3, 128);
        generator.addWithEnd("RGVFRRDTHKS");
        assertEquals(generator.size(), 2);
        assertEquals(generator.get(0), "GVFRRDTHK");
        assertEquals(generator.get(1), "GVFRRDtHK");
    }

    @Test
    void testaddPeptideStr()
    {
        List<PeptideMod> list = new ArrayList<>();
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(list);
        PeptideGenerator generator = new PeptideGenerator(aaSet, 40, 3, 128);
        generator.add("GVFRRDTHK", false, false);
        assertEquals(generator.size(), 2);
        assertEquals(generator.get(0), "GVFRRDTHK");
        assertEquals(generator.get(1), "GVFRRDtHK");
    }

    @Test
    void testaddPeptide2()
    {
        List<PeptideMod> list = new ArrayList<>();
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(list);
        PeptideGenerator generator = new PeptideGenerator(aaSet, 40, 3, 128, fastaSequence);
        generator.addWithEnd(0, 21);

        assertEquals(generator.size(), 42);

        assertEquals(generator.get(0), "MKWVTFISLLLLFSSAYSR");
        assertEquals(generator.get(1), "MKWVtFISLLLLFSSAYSR");
        assertEquals(generator.get(2), "MKWVTFIsLLLLFSSAYSR");
        assertEquals(generator.get(3), "MKWVtFIsLLLLFSSAYSR");
        assertEquals(generator.get(41), "MKWVTFISLLLLFSsAysR");
    }

    @Test
    void testAddPeptideBool()
    {
        List<PeptideMod> list = new ArrayList<>();
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.S));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.T));
        list.add(PeptideMod.of(PTM.Phospho(), AminoAcid.Y));

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(list);
        PeptideGenerator generator = new PeptideGenerator(aaSet, 40, 3, 128, fastaSequence);
        generator.add(1, 20, true, false);

        assertEquals(generator.size(), 42);

        assertEquals(generator.get(0), "MKWVTFISLLLLFSSAYSR");
        assertEquals(generator.get(1), "MKWVtFISLLLLFSSAYSR");
        assertEquals(generator.get(2), "MKWVTFIsLLLLFSSAYSR");
        assertEquals(generator.get(3), "MKWVtFIsLLLLFSSAYSR");
        assertEquals(generator.get(41), "MKWVTFISLLLLFSsAysR");
    }

    @Test
    void testUnknown()
    {
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(PeptideMod.of(PTM.ofName("Guanidinyl"), AminoAcid.K, false),
                PeptideMod.of(PTM.ofName("Oxidation"), AminoAcid.M, false),
                PeptideMod.of(PTM.Carbamidomethyl(), AminoAcid.C, true));
        PeptideGenerator generator = new PeptideGenerator(aaSet);
        generator.add("XAAAAAAAAAAAAAGVGGER");
        assertEquals(generator.size(), 0);
    }
}