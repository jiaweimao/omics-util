package omics.util.protein.digest;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;
import com.google.common.collect.SetMultimap;
import omics.util.chem.AtomicSymbol;
import omics.util.chem.Composition;
import omics.util.io.csv.CsvReader;
import omics.util.io.csv.CsvWriter;
import omics.util.protein.AminoAcid;
import omics.util.protein.Peptide;
import omics.util.protein.Protein;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.digest.util.AcceptAllDigestionController;
import omics.util.protein.digest.util.LengthDigestionController;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


class ProteaseTest
{
    @Test
    void testDigestion()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        List<Peptide> digests = new Protease(Enzyme.ofName("Trypsin")).digest(prot);

        assertEquals(4, digests.size());
        assertEquals("R", digests.get(0).toSymbolString());
        assertEquals("ESALYTNIK", digests.get(1).toSymbolString());
        assertEquals("ALASK", digests.get(2).toSymbolString());
        assertEquals("R", digests.get(3).toSymbolString());
    }

    @Test
    void testDigestionWithContainer()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease digester = new Protease(Enzyme.ofName("Trypsin"));

        List<Peptide> digests = new ArrayList<>();

        digester.digest(prot, digests);

        assertEquals(4, digests.size());
        assertEquals("R", digests.get(0).toSymbolString());
        assertEquals("ESALYTNIK", digests.get(1).toSymbolString());
        assertEquals("ALASK", digests.get(2).toSymbolString());
        assertEquals("R", digests.get(3).toSymbolString());
    }

    @Test
    void testDigestionWithMissedCleavages()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        List<Peptide> digests = new Protease(Enzyme.ofName("Trypsin"), 3).digest(prot);

        assertEquals(10, digests.size());

        class MissedCleavagePredicate implements Predicate<Peptide>
        {

            private final int mc;
            private final Enzyme csf;

            MissedCleavagePredicate(int mc, Enzyme csf)
            {
                this.mc = mc;
                this.csf = csf;
            }

            @Override
            public boolean test(Peptide peptide)
            {
                return csf.countCleavageSites(peptide) == mc;
            }
        }

        List<Peptide> digests0 = digests.stream().filter(new MissedCleavagePredicate(0, Enzyme.ofName("Trypsin"))).collect(Collectors.toList());

        assertEquals(4, digests0.size());
        assertEquals("R", digests0.get(0).toSymbolString());
        assertEquals("ESALYTNIK", digests0.get(1).toSymbolString());
        assertEquals("ALASK", digests0.get(2).toSymbolString());
        assertEquals("R", digests0.get(3).toSymbolString());

        List<Peptide> digests1 = digests.stream().filter(new MissedCleavagePredicate(1, Enzyme.ofName("Trypsin"))).collect(Collectors.toList());

        assertEquals(3, digests1.size());
        assertEquals("RESALYTNIK", digests1.get(0).toSymbolString());
        assertEquals("ESALYTNIKALASK", digests1.get(1).toSymbolString());
        assertEquals("ALASKR", digests1.get(2).toSymbolString());

        List<Peptide> digests2 = digests.stream().filter(new MissedCleavagePredicate(2, Enzyme.ofName("Trypsin"))).collect(Collectors.toList());

        assertEquals(2, digests2.size());
        assertEquals("RESALYTNIKALASK", digests2.get(0).toSymbolString());
        assertEquals("ESALYTNIKALASKR", digests2.get(1).toSymbolString());

        List<Peptide> digests3 = digests.stream().filter(new MissedCleavagePredicate(3, Enzyme.ofName("Trypsin"))).collect(Collectors.toList());

        assertEquals(1, digests3.size());
        assertEquals("RESALYTNIKALASKR", digests3.get(0).toSymbolString());
    }

    @Test
    void testDigestTermMod()
    {
        Modification nMod = new Modification("nmod",
                new Composition.Builder(AtomicSymbol.C, -3).add(AtomicSymbol.H, 2).build());
        Modification cMod = new Modification("cmod",
                new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.P).add(AtomicSymbol.O, 3).build());

        CleavageSiteMatcher cs = new CleavageSiteMatcher("M|X", nMod, cMod);

        Protease proteinDigester = new Protease(new Enzyme(cs));

        Protein prot = new Protein("ACC1", "AHNGMRWPTG");

        List<Peptide> digests = proteinDigester.digest(prot);

        assertEquals(-34., digests.get(1).getModifications(ModAttachment.nTermSet).get(0).getMolecularMass(), 0.1);
        assertEquals(79.9, digests.get(0).getModifications(ModAttachment.cTermSet).get(0).getMolecularMass(), 0.1);
    }

    @Test
    void testDigestionWithCnbr()
    {
        Protein prot = new Protein("ACC1", "AHNGMRWPTG");

        List<Peptide> digests = new Protease(Enzyme.ofName("CNBr")).digest(prot);

        assertEquals(-48.0039, digests.get(0).getModifications(ModAttachment.cTermSet).get(0).getMolecularMass(), 0.01);
    }

    @Test
    void testSemiTrypticDigestion()
    {
        Protein prot = new Protein("ACC1", "TFGQVVAR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 0, Protease.Cleavage.Semi);

        List<Peptide> digests = proteinDigester.digest(prot);

        assertEquals(15, digests.size());
        assertEquals("TFGQVVAR", digests.get(0).toSymbolString());
        assertEquals("T", digests.get(1).toSymbolString());
        assertEquals("FGQVVAR", digests.get(2).toSymbolString());
        assertEquals("TF", digests.get(3).toSymbolString());
        assertEquals("GQVVAR", digests.get(4).toSymbolString());
        assertEquals("TFG", digests.get(5).toSymbolString());
        assertEquals("QVVAR", digests.get(6).toSymbolString());
        assertEquals("TFGQ", digests.get(7).toSymbolString());
        assertEquals("VVAR", digests.get(8).toSymbolString());
        assertEquals("TFGQV", digests.get(9).toSymbolString());
        assertEquals("VAR", digests.get(10).toSymbolString());
        assertEquals("TFGQVV", digests.get(11).toSymbolString());
        assertEquals("AR", digests.get(12).toSymbolString());
        assertEquals("TFGQVVA", digests.get(13).toSymbolString());
        assertEquals("R", digests.get(14).toSymbolString());
    }

    @Test
    void testSemiTrypticDigestion2()
    {
        Protein prot = new Protein("ACC1", "TFGQVVAR");

        Protease proteinDigester = new Protease(Enzyme.ofName("CNBr"), Protease.Cleavage.Semi)
                .controller(new AcceptAllDigestionController()
                {
                    @Override
                    public boolean retainSemiDigest(Peptide digest)
                    {
                        return digest.size() > 1;
                    }
                });

        List<Peptide> digests = proteinDigester.digest(prot);

        assertEquals(13, digests.size());

        assertEquals("TFGQVVAR", digests.get(0).toSymbolString());
        assertEquals("FGQVVAR", digests.get(1).toSymbolString());
        assertEquals("TF", digests.get(2).toSymbolString());
        assertEquals("GQVVAR", digests.get(3).toSymbolString());
        assertEquals("TFG", digests.get(4).toSymbolString());
        assertEquals("QVVAR", digests.get(5).toSymbolString());
        assertEquals("TFGQ", digests.get(6).toSymbolString());
        assertEquals("VVAR", digests.get(7).toSymbolString());
        assertEquals("TFGQV", digests.get(8).toSymbolString());
        assertEquals("VAR", digests.get(9).toSymbolString());
        assertEquals("TFGQVV", digests.get(10).toSymbolString());
        assertEquals("AR", digests.get(11).toSymbolString());
        assertEquals("TFGQVVA", digests.get(12).toSymbolString());
    }

    @Test
    void testDigestionWithMods()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Modification mod = new Modification("79.9",
                new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.P).add(AtomicSymbol.O, 3).build());

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin")).addFixedMod(AminoAcid.Y, mod)
                .addFixedMod(AminoAcid.S, mod).addFixedMod(AminoAcid.T, mod);

        List<Peptide> digests = proteinDigester.digest(prot);

        assertEquals(4, digests.size());
        assertFalse(digests.get(0).hasModifications());
        assertArrayEquals(new int[]{1, 4, 5}, digests.get(1).getModificationIndexes(ModAttachment.all));
        assertArrayEquals(new int[]{3}, digests.get(2).getModificationIndexes(ModAttachment.all));
        assertFalse(digests.get(3).hasModifications());
    }


    @Test
    void testLargeProteinBugToFix20()
    {
        Protein prot = new Protein("ACC1",
                "MGLPLARLAAVCLALSLAGGSELQTEGRTRYHGRNVCSTWGNFHYKTFDGDVFRFPGLCD"
                        + "YNFASDCRGSYKEFAVHLKRGPGQAEAPAGVESILLTIKDDTIYLTRHLAVLNGAVVSTP"
                        + "HYSPGLLIEKSDAYTKVYSRAGLTLMWNREDALMLELDTKFRNHTCGLCGDYNGLQSYSE"
                        + "FLSDGVLFSPLEFGNMQKINQPDVVCEDPEEEVAPASCSEHRAECERLLTAEAFADCQDL"
                        + "VPLEPYLRACQQDRCRCPGGDTCVCSTVAEFSRQCSHAGGRPGNWRTATLCPKTCPGNLV"
                        + "YLESGSPCMDTCSHLEVSSLCEEHRMDGCFCPEGTVYDDIGDSGCVPVSQCHCRLHGHLY"
                        + "TPGQEITNDCEQCVCNAGRWVCKDLPCPGTCALEGGSHITTFDGKTYTFHGDCYYVLAKG"
                        + "DHNDSYALLGELAPCGSTDKQTCLKTVVLLADKKKNAVVFKSDGSVLLNQLQVNLPHVTA"
                        + "SFSVFRPSSYHIMVSMAIGVRLQVQLAPVMQLFVTLDQASQGQVQGLCGNFNGLEGDDFK"
                        + "TASGLVEATGAGFANTWKAQSTCHDKLDWLDDPCSLNIESANYAEHWCSLLKKTETPFGR"
                        + "CHSAVDPAEYYKRCKYDTCNCQNNEDCLCAALSSYARACTAKGVMLWGWREHVCNKDVGS"
                        + "CPNSQVFLYNLTTCQQTCRSLSEADSHCLEGFAPVDGCGCPDHTFLDEKGRCVPLAKCSC"
                        + "YHRGLYLEAGDVVVRQEERCVCRDGRLHCRQIRLIGQSCTAPKIHMDCSNLTALATSKPR"
                        + "ALSCQTLAAGYYHTECVSGCVCPDGLMDDGRGGCVVEKECPCVHNNDLYSSGAKIKVDCN"
                        + "TCTCKRGRWVCTQAVCHGTCSIYGSGHYITFDGKYYDFDGHCSYVAVQDYCGQNSSLGSF"
                        + "SIITENVPCGTTGVTCSKAIKIFMGRTELKLEDKHRVVIQRDEGHHVAYTTREVGQYLVV"
                        + "ESSTGIIVIWDKRTTVFIKLAPSYKGTVCGLCGNFDHRSNNDFTTRDHMVVSSELDFGNS"
                        + "WKEAPTCPDVSTNPEPCSLNPHRRSWAEKQCSILKSSVFSICHSKVDPKPFYEACVHDSC"
                        + "SCDTGGDCECFCSAVASYAQECTKEGACVFWRTPDLCPIFCDYYNPPHECEWHYEPCGNR"
                        + "SFETCRTINGIHSNISVSYLEGCYPRCPKDRPIYEEDLKKCVTADKCGCYVEDTHYPPGA"
                        + "SVPTEETCKSCVCTNSSQVVCRPEEGKILNQTQDGAFCYWEICGPNGTVEKHFNICSITT"
                        + "RPSTLTTFTTITLPTTPTSFTTTTTTTTPTSSTVLSTTPKLCCLWSDWINEDHPSSGSDD"
                        + "GDREPFDGVCGAPEDIECRSVKDPHLSLEQHGQKVQCDVSVGFICKNEDQFGNGPFGLCY"
                        + "DYKIRVNCCWPMDKCITTPSPPTTTPSPPPTTTTTLPPTTTPSPPTTTTTTPPPTTTPSP"
                        + "PITTTTTPLPTTTPSPPISTTTTPPPTTTPSPPTTTPSPPTTTPSPPTTTTTTPPPTTTP"
                        + "SPPMTTPITPPASTTTLPPTTTPSPPTTTTTTPPPTTTPSPPTTTPITPPTSTTTLPPTT"
                        + "TPSPPPTTTTTPPPTTTPSPPTTTTPSPPTITTTTPPPTTTPSPPTTTTTTPPPTTTPSP"
                        + "PTTTPITPPTSTTTLPPTTTPSPPPTTTTTPPPTTTPSPPTTTTPSPPITTTTTPPPTTT"
                        + "PSSPITTTPSPPTTTMTTPSPTTTPSSPITTTTTPSSTTTPSPPPTTMTTPSPTTTPSPP"
                        + "TTTMTTLPPTTTSSPLTTTPLPPSITPPTFSPFSTTTPTTPCVPLCNWTGWLDSGKPNFH"
                        + "KPGGDTELIGDVCGPGWAANISCRATMYPDVPIGQLGQTVVCDVSVGLICKNEDQKPGGV"
                        + "IPMAFCLNYEINVQCCECVTQPTTMTTTTTENPTPPTTTPITTTTTVTPTPTPTGTQTPT"
                        + "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP"
                        + "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT"
                        + "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT"
                        + "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV"
                        + "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT"
                        + "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT"
                        + "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT"
                        + "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ"
                        + "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT"
                        + "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT"
                        + "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP"
                        + "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT"
                        + "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT"
                        + "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP"
                        + "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTP"
                        + "ITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPT"
                        + "GTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTT"
                        + "TVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTP"
                        + "TTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPT"
                        + "PTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPI"
                        + "TTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTG"
                        + "TQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTT"
                        + "VTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPT"
                        + "TTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTP"
                        + "TPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPIT"
                        + "TTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGT"
                        + "QTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTV"
                        + "TPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTT"
                        + "TPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPT"
                        + "PTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITT"
                        + "TTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQ"
                        + "TPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVT"
                        + "PTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTT"
                        + "PITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTP"
                        + "TGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTT"
                        + "TTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQT"
                        + "PTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTP"
                        + "TPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTPTTTPITTTTTVTPTPTPTGTQTGPPTH"
                        + "TSTAPIAELTTSNPPPESSTPQTSRSTSSPLTESTTLLSTLPPAIEMTSTAPPSTPTAPT"
                        + "TTSGGHTLSPPPSTTTSPPGTPTRGTTTGSSSAPTPSTVQTTTTSAWTPTPTPLSTPSII"
                        + "RTTGLRPYPSSVLICCVLNDTYYAPGEEVYNGTYGDTCYFVNCSLSCTLEFYNWSCPSTP"
                        + "SPTPTPSKSTPTPSKPSSTPSKPTPGTKPPECPDFDPPRQENETWWLCDCFMATCKYNNT"
                        + "VEIVKVECEPPPMPTCSNGLQPVRVEDPDGCCWHWECDCYCTGWGDPHYVTFDGLYYSYQ"
                        + "GNCTYVLVEEISPSVDNFGVYIDNYHCDPNDKVSCPRTLIVRHETQEVLIKTVHMMPMQV"
                        + "QVQVNRQAVALPYKKYGLEVYQSGINYVVDIPELGVLVSYNGLSFSVRLPYHRFGNNTKG"
                        + "QCGTCTNTTSDDCILPSGEIVSNCEAAADQWLVNDPSKPHCPHSSSTTKRPAVTVPGGGK"
                        + "TTPHKDCTPSPLCQLIKDSLFAQCHALVPPQHYYDACVFDSCFMPGSSLECASLQAYAAL"
                        + "CAQQNICLDWRNHTHGACLVECPSHREYQACGPAEEPTCKSSSSQQNNTVLVEGCFCPEG"
                        + "TMNYAPGFDVCVKTCGCVGPDNVPREFGEHFEFDCKNCVCLEGGSGIICQPKRCSQKPVT"
                        + "HCVEDGTYLATEVNPADTCCNITVCKCNTSLCKEKPSVCPLGFEVKSKMVPGRCCPFYWC"
                        + "ESKGVCVHGNAEYQPGSPVYSSKCQDCVCTDKVDNNTLLNVIACTHVPCNTSCSPGFELM"
                        + "EAPGECCKKCEQTHCIIKRPDNQHVILKPGDFKSDPKNNCTFFSCVKIHNQLISSVSNIT"
                        + "CPNFDASICIPGSITFMPNGCCKTCTPRNETRVPCSTVPVTTEVSYAGCTKTVLMNHCSG"
                        + "SCGTFVMYSAKAQALDHSCSCCKEEKTSQREVVLSCPNGGSLTHTYTHIESCQCQDTVCG" + "LPTGTSRRARRSPRHLGSG");

        List<Peptide> digests = new Protease(Enzyme.ofName("Arg-C")).digest(prot);
        // 87 R in the sequence, 8 RP, total 79 cleavable sites, 80 fragments
        assertEquals(digests.size(), 80);
    }

    @Test
    void testGetDigestPositions()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        PositionAccu cb = new PositionAccu();

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin")).controller(cb);

        List<Peptide> digests = proteinDigester.digest(prot);
        Iterator<Integer> expectedFromPositions = Arrays.asList(0, 1, 10, 15).iterator();
        Iterator<Integer> expectedToPositions = Arrays.asList(0, 9, 14, 15).iterator();

        assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(0)).get(0));
        assertEquals(expectedToPositions.next(), cb.getTo(digests.get(0)).get(0));
        assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(1)).get(0));
        assertEquals(expectedToPositions.next(), cb.getTo(digests.get(1)).get(0));
        assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(2)).get(0));
        assertEquals(expectedToPositions.next(), cb.getTo(digests.get(2)).get(0));
        assertEquals(expectedFromPositions.next(), cb.getFrom(digests.get(3)).get(1));
        assertEquals(expectedToPositions.next(), cb.getTo(digests.get(3)).get(1));
    }

    @Test
    void testGetNtDigests()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 2)
                .controller(new NTermFilter());

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = ImmutableList.of("R", "RESALYTNIK", "RESALYTNIKALASK");
        List<Integer> expectedDigestMc = ImmutableList.of(0, 1, 2);

        int count = 0;
        for (Peptide digest : digests) {

            assertEquals(expectedDigestMc.get(count).intValue(), Enzyme.ofName("Trypsin").countCleavageSites(digest));
            assertEquals(expectedDigestSequence.get(count), digest.toSymbolString());
            count++;
        }
    }

    @Test
    void testGetNtDigests2()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), Integer.MAX_VALUE)
                .controller(new NTermFilter());

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = ImmutableList.of("R", "RESALYTNIK", "RESALYTNIKALASK",
                "RESALYTNIKALASKR");

        int mc = 0;
        for (Peptide digest : digests) {

            assertEquals(expectedDigestSequence.get(mc), digest.toSymbolString());
            mc++;
        }
    }

    @Test
    void testGetCtDigests()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 2)
                .controller(new CTermFilter());

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = ImmutableList.of("R", "ESALYTNIKALASKR", "ALASKR");
        List<Integer> expectedDigestMc = ImmutableList.of(0, 2, 1);

        int i = 0;
        for (Peptide digest : digests) {

            assertEquals(expectedDigestMc.get(i).intValue(), Enzyme.ofName("Trypsin").countCleavageSites(digest));
            assertEquals(expectedDigestSequence.get(i), digest.toSymbolString());

            i++;
        }
    }

    @Test
    void testGetCtDigestsWithReverseIteration()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 2)
                .iterationOrder(Protease.CleavageSiteIteration.REVERSE)
                .controller(new CTermFilter());

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = ImmutableList.of("R", "ESALYTNIKALASKR", "ALASKR");
        List<Integer> expectedDigestMc = ImmutableList.of(0, 2, 1);

        int i = 0;
        for (Peptide digest : digests) {

            assertEquals(expectedDigestMc.get(i).intValue(), Enzyme.ofName("Trypsin").countCleavageSites(digest));
            assertEquals(expectedDigestSequence.get(i), digest.toSymbolString());

            i++;
        }
    }

    @Test
    void testGetNtMc2Digest()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 100)
                .controller(new ExactMcNTermFilter(2));

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = ImmutableList.of("RESALYTNIKALASK");

        assertEquals(1, digests.size());
        assertEquals(expectedDigestSequence.get(0), digests.get(0).toSymbolString());
    }

    @Test
    void testGetNtMc5OrMaxMcDigest()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 100)
                .controller(new EitherExactOrGreatestMc(5));

        List<Peptide> digests = proteinDigester.digest(prot);

        List<String> expectedDigestSequence = ImmutableList.of("RESALYTNIKALASKR");

        assertEquals(1, digests.size());
        assertEquals(expectedDigestSequence.get(0), digests.get(0).toSymbolString());
    }

    @Test
    void testGetNtMc3DigestsThenInterrupt()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");
        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 3)
                .controller(new AcceptAllDigestionController()
                {
                    @Override
                    public boolean interruptDigestion(Protein protein, int proteinCleavageSiteCount,
                                                      int inclusiveLowerBoundIndex, int inclusiveUpperBoundIndex)
                    {
                        return inclusiveUpperBoundIndex == proteinCleavageSiteCount - 1;
                    }

                    @Override
                    public boolean retainDigest(Protein protein, Peptide digest, int digestPos)
                    {
                        return digestPos == 0;
                    }
                });

        List<Peptide> digests = proteinDigester.digest(prot);
        List<String> expectedDigestSequence = ImmutableList.of("R", "RESALYTNIK", "RESALYTNIKALASK",
                "RESALYTNIKALASKR");

        for (int i = 0; i < digests.size(); i++) {
            assertEquals(expectedDigestSequence.get(i), digests.get(i).toSymbolString());
        }
    }

    @Test
    void testGetNtMc3DigestOnlyThenInterrupt()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"), 3)
                .controller(new AcceptAllDigestionController()
                {
                    @Override
                    public boolean interruptDigestion(Protein protein, int proteinCleavageSiteCount,
                                                      int inclusiveLowerBoundIndex, int inclusiveUpperBoundIndex)
                    {
                        return inclusiveUpperBoundIndex == proteinCleavageSiteCount;
                    }

                    @Override
                    public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex,
                                              int exclusiveProteinUpperIndex, int missedCleavagesCount)
                    {
                        return missedCleavagesCount == 3;
                    }

                    @Override
                    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex)
                    {
                        return digestIndex == 0;
                    }
                });

        List<Peptide> digests = proteinDigester.digest(prot);

        assertEquals(1, digests.size());
        assertEquals("RESALYTNIKALASKR", digests.get(0).toSymbolString());
    }

    @Test
    void testSemi()
    {

        Peptide dp = new Peptide(Peptide.parse("PEPTIDE"));

        Peptide digest = new Peptide(dp, 0, 3);

        assertEquals(AminoAcid.P, digest.getSymbol(0));
        assertEquals(AminoAcid.E, digest.getSymbol(1));
        assertEquals(AminoAcid.P, digest.getSymbol(2));
    }

    @Test
    void testSemi2()
    {
        Peptide dp = new Peptide(Peptide.parse("PEPT(OH)IDE"));

        Peptide digest = new Peptide(dp, 2, 4);

        assertEquals(17, digest.getModificationsAt(1, ModAttachment.sideChainSet).get(0).getMolecularMass(), 0.01);
    }

    @Test
    void testDigestionWithMassRange()
    {
        Protein prot = new Protein("ACC1", "RESALYTNIKALASKR");

        Protease proteinDigester = new Protease(Enzyme.ofName("Trypsin"))
                .controller(new AcceptAllDigestionController()
                {
                    @Override
                    public boolean retainDigest(Protein protein, Peptide digest, int digestIndex)
                    {
                        return Range.closed(500., 2000.).contains(digest.getMolecularMass());
                    }
                });

        List<Peptide> digests = proteinDigester.digest(prot);

        assertEquals(1, digests.size());
        assertEquals("ESALYTNIK", digests.get(0).toSymbolString());
        assertEquals(1037.539, digests.get(0).getMolecularMass(), 0.001);
    }

    @Test
    void test() throws Exception
    {
        Protein protein = new Protein("ACC1",
                "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW"
                        + "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK"
                        + "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS"
                        + "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD" + "EGGETNN");

        Protease digester = new Protease(Enzyme.ofName("Trypsin"));

        List<Peptide> digests = digester.digest(protein);

        assertEquals(32, digests.size());
    }

    @Test
    void testMC1() throws Exception
    {
        Protein protein = new Protein("ACC1",
                "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW"
                        + "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK"
                        + "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS"
                        + "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD" + "EGGETNN");

        Protease digester = new Protease(Enzyme.ofName("Trypsin"), 1);

        List<Peptide> digests = digester.digest(protein);

        assertEquals(63, digests.size());
    }

    @Test
    void testMC2()
    {
        Protein protein = new Protein("ACC1",
                "MVDREQLVQKARLAEQAERYDDMAAAMKSVTELNEALSNEERNLLSVAYKNVVGARRSSW"
                        + "RVISSIEQKTSADGNEKKMEMVRAYREKIEKELETVCRDVLNLLDNFLIKNCNETQHESK"
                        + "VFYLKMKGDYYRYLAEVATGEKRVGVVESSEKSYSEAHEISKEHMQPTHPIRLGLALNYS"
                        + "VFYYEIQNAPEQACHLAKTAFDDAIAELDTLNEDSYKDSTLIMQLLRDNLTLWTSDQZDD" + "EGGETNN");

        Protease digester = new Protease(Enzyme.ofName("Trypsin"), 2);

        List<Peptide> digests = digester.digest(protein);

        assertEquals(93, digests.size());
    }

    @Test
    void testPEPSINE_PH_GT_2() throws Exception
    {
        Protein protein = new Protein("P1", "MTNHQLSTTEWNDETLYQEFNGLKKMNPKLKTLLAIGGWNFGTQKFTDMVATANNRQTFV");

        Protease digester = new Protease(Enzyme.PEPSINE_PH_GT_2);
        List<Peptide> digests = digester.digest(protein);

        assertEquals(21, digests.size());
        assertEquals(Peptide.parse("MTNHQ"), digests.get(0));
        assertEquals(Peptide.parse("LSTTE"), digests.get(1));
        assertEquals(Peptide.parse("W"), digests.get(2));
        assertEquals(Peptide.parse("NDET"), digests.get(3));
        assertEquals(Peptide.parse("L"), digests.get(4));
        assertEquals(Peptide.parse("Y"), digests.get(5));
        assertEquals(Peptide.parse("QE"), digests.get(6));
        assertEquals(Peptide.parse("F"), digests.get(7));
        assertEquals(Peptide.parse("NG"), digests.get(8));
        assertEquals(Peptide.parse("L"), digests.get(9));
        assertEquals(Peptide.parse("KKMNPKL"), digests.get(10));
        assertEquals(Peptide.parse("KT"), digests.get(11));
        assertEquals(Peptide.parse("LL"), digests.get(12));
        assertEquals(Peptide.parse("AIGG"), digests.get(13));
        assertEquals(Peptide.parse("W"), digests.get(14));
        assertEquals(Peptide.parse("N"), digests.get(15));
        assertEquals(Peptide.parse("F"), digests.get(16));
        assertEquals(Peptide.parse("GTQK"), digests.get(17));
        assertEquals(Peptide.parse("F"), digests.get(18));
        assertEquals(Peptide.parse("TDMVATANNRQTF"), digests.get(19));
        assertEquals(Peptide.parse("V"), digests.get(20));
    }

    @Test
    void testPEPSINE_PH_1_3() throws Exception
    {
        Protein protein = new Protein("P1", "MTNHQLSTTEWNDETLYQEFNGLKKMNPKLKTLLAIGGWNFGTQKFTDMVATANNRQTFV");

        Protease digester = new Protease(Enzyme.PEPSINE_PH_1_3);
        List<Peptide> digests = digester.digest(protein);

        assertEquals(16, digests.size());
        assertEquals(Peptide.parse("MTNHQ"), digests.get(0));
        assertEquals(Peptide.parse("LSTTEWNDET"), digests.get(1));
        assertEquals(Peptide.parse("L"), digests.get(2));
        assertEquals(Peptide.parse("YQE"), digests.get(3));
        assertEquals(Peptide.parse("F"), digests.get(4));
        assertEquals(Peptide.parse("NG"), digests.get(5));
        assertEquals(Peptide.parse("L"), digests.get(6));
        assertEquals(Peptide.parse("KKMNPKL"), digests.get(7));
        assertEquals(Peptide.parse("KT"), digests.get(8));
        assertEquals(Peptide.parse("LL"), digests.get(9));
        assertEquals(Peptide.parse("AIGGWN"), digests.get(10));
        assertEquals(Peptide.parse("F"), digests.get(11));
        assertEquals(Peptide.parse("GTQK"), digests.get(12));
        assertEquals(Peptide.parse("F"), digests.get(13));
        assertEquals(Peptide.parse("TDMVATANNRQTF"), digests.get(14));
        assertEquals(Peptide.parse("V"), digests.get(15));
    }

    @Test
    void testOrNTerm()
    {
        Protease digester = new Protease(new CleavageSiteMatcher("ont{I}ont{I}L|K"));

        assertEquals(Arrays.asList(Peptide.parse("AIIL"), Peptide.parse("K")),
                digester.digest(new Protein("P1", "AIILK")));
        assertEquals(Arrays.asList(Peptide.parse("IIL"), Peptide.parse("K")),
                digester.digest(new Protein("P1", "IILK")));
        assertEquals(Arrays.asList(Peptide.parse("IL"), Peptide.parse("K")), digester.digest(new Protein("P1", "ILK")));
        assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("K")), digester.digest(new Protein("P1", "LK")));
    }

    @Test
    void testOrCTerm()
    {
        Protease digester = new Protease(new CleavageSiteMatcher("L|Koct{K}oct{K}"));

        assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("KKKR")),
                digester.digest(new Protein("P1", "LKKKR")));
        assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("KKK")),
                digester.digest(new Protein("P1", "LKKK")));
        assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("KK")), digester.digest(new Protein("P1", "LKK")));
        assertEquals(Arrays.asList(Peptide.parse("L"), Peptide.parse("K")), digester.digest(new Protein("P1", "LK")));
    }

    @Test
    void testUnspecific() throws IOException
    {
        Protease protease = new Protease(Enzyme.ofName("unspecific cleavage"), Integer.MAX_VALUE).controller(new LengthDigestionController(6, 30));
        FastaReader reader = new FastaReader(Paths.get("D:\\database\\uniprot\\uniprot-reviewed_yes.fasta"));
        CsvWriter writer = new CsvWriter("D:\\database\\uniprot\\uniprot-reviewed_yes_normal.fasta");
        while (reader.hasNext()) {
            omics.util.protein.database.Protein protein = reader.next();
            List<Peptide> peptideList = protease.digest(new Protein(protein.getAccession(), protein.getSequence()));
            for (Peptide peptide : peptideList) {
                writer.write(peptide.toSymbolString());
                writer.endRecord();
            }
        }
        writer.close();
        reader.close();
    }

    @Test
    void compare() throws IOException
    {
        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();
        CsvReader reader1 = new CsvReader("D:\\database\\oglycan_proteins_sa.csv");
        while (reader1.readRecord()) {
            set1.add(reader1.get(0));
        }
        reader1.close();
        CsvReader reader2 = new CsvReader("D:\\database\\oglycan_proteins_normal.csv");
        while (reader2.readRecord()) {
            set2.add(reader2.get(0));
        }
        reader2.close();

        System.out.println(set1.size());
        System.out.println(set2.size());
        System.out.println(set1.equals(set2));
    }

    private static class NTermFilter extends AcceptAllDigestionController
    {
        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex)
        {
            return digestIndex == 0;
        }
    }

    private static class CTermFilter extends AcceptAllDigestionController
    {
        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex)
        {
            return digestIndex == protein.size() - 1;
        }
    }

    private static class ExactMcNTermFilter extends AcceptAllDigestionController
    {
        private final int mc;

        private ExactMcNTermFilter(int mc)
        {
            this.mc = mc;
        }

        @Override
        public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex,
                                  int missedCleavagesCount)
        {
            return missedCleavagesCount == mc;
        }

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestPos)
        {
            return digestPos == 0;
        }
    }

    private static class PositionAccu extends AcceptAllDigestionController
    {
        private final SetMultimap<Peptide, Integer> from = HashMultimap.create();
        private final SetMultimap<Peptide, Integer> to = HashMultimap.create();

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex)
        {
            from.put(digest, digestIndex);
            to.put(digest, digestIndex + digest.size() - 1);

            return true;
        }

        List<Integer> getFrom(Peptide digest)
        {
            return ImmutableList.copyOf(from.get(digest));
        }

        List<Integer> getTo(Peptide digest)
        {
            return ImmutableList.copyOf(to.get(digest));
        }
    }

    private static class EitherExactOrGreatestMc extends AcceptAllDigestionController
    {
        private final int mc;

        EitherExactOrGreatestMc(int mc)
        {
            this.mc = mc;
        }

        @Override
        public boolean makeDigest(Protein protein, int inclusiveProteinLowerIndex, int exclusiveProteinUpperIndex,
                                  int missedCleavagesCount)
        {
            return missedCleavagesCount == mc
                    || exclusiveProteinUpperIndex - inclusiveProteinLowerIndex == protein.size();
        }

        @Override
        public boolean retainDigest(Protein protein, Peptide digest, int digestIndex)
        {
            return digestIndex == 0 && digest.size() == protein.size();
        }
    }
}