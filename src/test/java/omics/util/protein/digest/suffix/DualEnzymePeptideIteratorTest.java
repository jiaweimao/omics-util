package omics.util.protein.digest.suffix;

import omics.util.io.FilenameUtils;
import omics.util.io.csv.CsvWriter;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.digest.Protease;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Jan 2020, 7:42 PM
 */
class DualEnzymePeptideIteratorTest
{
    @Test
    void testEnzyme()
    {
        Enzyme enzyme = Enzyme.ofName("OpeRATOR");
        System.out.println(enzyme.getCleavageSiteMatcher().getCleavageFormat());
    }

    @Test
    void test() throws IOException
    {
        Protease p1 = new Protease(Enzyme.ofName("Trypsin/P"), 2);
        Protease p2 = new Protease(Enzyme.ofName("OpeRATOR"), 5);
        PeptideIterator it = PeptideIterator.getIterator(p1, p2, "D:\\igd\\fasta\\b.fa", 6, 46, false);
//        CsvWriter writer = new CsvWriter("D:\\igd\\homo_20191210.pep.csv");
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                int startIndex = cleavageEvent.getStartIndex();
                for (int i = 0; i < cleavageEvent.size(); i++) {
                    int endIndex = cleavageEvent.getEndIndex(i);
                    String peptide = it.getPeptide(startIndex + 1, endIndex - 1);

                    System.out.println(peptide);
//                    writer.writeRecord(peptide);
                }
            }
        }
        it.close();
//        writer.close();
    }

    @Test
    void genPep() throws IOException
    {
        String fasta = "D:\\igd\\fasta\\test.fasta";
        Protease p1 = new Protease(Enzyme.ofName("Trypsin"), 2);
        Protease p2 = new Protease(Enzyme.ofName("OpeRATOR"), 5);
        PeptideIterator pi = PeptideIterator.getIterator(p1, p2, fasta, 6, 46, false);
        CsvWriter writer = new CsvWriter(FilenameUtils.newExtension(Paths.get(fasta), ".peptide.csv").toString());
        Set<String> pepSet = new HashSet<>();
        int id = 0;
        while (pi.hasNext()) {
            SuffixEvent suffixEvent = pi.next();
//            System.out.println(id + "\t" + suffixEvent.getEventType() + "\t" + suffixEvent.getStartIndex());

            id++;
            if (suffixEvent.isCleavageEvent()) {
                CleavageEvent cleavageEvent = suffixEvent.asCleavageEvent();
                int startIndex = suffixEvent.getStartIndex();
                for (int i = 0; i < cleavageEvent.size(); i++) {
                    int endIndex = cleavageEvent.getEndIndex(i);
                    String seq = pi.getPeptide(startIndex + 1, endIndex - 1);
                    if (seq.indexOf('S') >= 0 || seq.indexOf('T') >= 0) {
                        pepSet.add(seq);
                        writer.writeRecord(seq);
                    }
                }
            }
        }
        writer.close();
        pi.close();
        System.out.println(pepSet.size());
    }

}