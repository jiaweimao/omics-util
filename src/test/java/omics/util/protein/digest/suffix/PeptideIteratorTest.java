package omics.util.protein.digest.suffix;

import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import omics.util.io.FileUtils;
import omics.util.protein.Peptide;
import omics.util.protein.database.Protein;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.digest.Protease;
import omics.util.protein.digest.util.LengthDigestionController;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 23 Jun 2019, 3:46 PM
 */
class PeptideIteratorTest
{
    private final String protein1000 = "D:\\database\\uniprot\\proteins1000.fasta";
    private final String protein101 = "D:\\database\\uniprot\\protein101.fasta";
    private final String p3 = FileUtils.getResourcePath("protein_peptide.fasta").toString();
    private final String p2 = FileUtils.getResourcePath("p2.fasta").toString();
    private final String human = "D:\\database\\uniprot\\human_reviewed_20190322.fasta";
    private final int minLen = 6;
    private final int maxLen = 46;

    @Test
    void testDual() throws IOException
    {
        Protease protease1 = new Protease(Enzyme.ofName("Trypsin"), 1);
        protease1.controller(new LengthDigestionController(minLen, Integer.MAX_VALUE));
        Protease protease2 = new Protease(Enzyme.ofName("OpeRATOR"), 2);
        protease2.controller(new LengthDigestionController(minLen, maxLen));

        test(protein1000, false, protease1, protease2, true, false, false);

    }

    private void test(String fasta, boolean removeM, Protease protease1, Protease protease2, boolean print, boolean equalTest, boolean large) throws IOException
    {
        long count0 = 0;
        int dup1 = 0;
        Range<Integer> range = Range.closed(minLen, maxLen);
        Set<String> seqSet = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease1, protease2, fasta, minLen, maxLen, removeM);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                count0 += cleavageEvent.size();
                if (!large) {
                    int startIndex = cleavageEvent.getStartIndex();
                    for (int i = 0; i < cleavageEvent.size(); i++) {
                        int endIndex = cleavageEvent.getEndIndex(i);
                        String peptide = it.getPeptide(startIndex + 1, endIndex - 1);
//                        if (peptide.equals("GTELWERDREVWNGKEHDEN")) {
//                            System.out.println(startIndex);
//                            System.out.println(cleavageEvent.getEndIndexList());
//                            return;
//                        }
                        assertTrue(range.contains(peptide.length()), peptide.length() + "\t" + startIndex + "\t" + endIndex);
                        boolean add = seqSet.add(peptide);
                        if (!add) {
                            if (print)
                                System.out.println("DP0:" + startIndex + "\t" + endIndex + "\t" + it.getPeptide(startIndex, endIndex));
                            dup1++;
                        }
                    }
                }
            }
        }
        it.close();

        if (!large)
            System.out.println("PEP=" + seqSet.size());

        FastaReader reader = new FastaReader(Paths.get(fasta));
        Set<String> set2 = new HashSet<>();
        int dup = 0;
        long count2 = 0;
        while (reader.hasNext()) {
            omics.util.protein.database.Protein protein = reader.next();
            String sequence = protein.getSequence();
            if (removeM && sequence.startsWith("M")) {
                sequence = sequence.substring(1);
            }
            String accession = protein.getAccession();
            List<Peptide> peptides = protease1.digest(new omics.util.protein.Protein(accession, sequence));
            for (Peptide peptide : peptides) {
                String s = peptide.toSymbolString();

                List<Peptide> digests = protease2.digest(new omics.util.protein.Protein(accession, s));
                for (Peptide digest : digests) {
                    String pep = digest.toSymbolString();
                    if (pep.contains("U"))
                        continue;
                    if (pep.length() < minLen || pep.length() > maxLen)
                        continue;
                    count2++;
                    if (!large) {
                        assertTrue(seqSet.contains(pep), protein.getAccession() + "\t" + pep);
                        boolean add = set2.add(pep);
                        if (!add) {
                            dup++;
                        }
                    }
                }

                if (s.contains("U"))
                    continue;
                if (s.length() < minLen || s.length() > maxLen)
                    continue;
                count2++;
                if (!large) {
                    assertTrue(seqSet.contains(s), protein.getAccession() + "\t" + s);
                    boolean add = set2.add(s);
                    if (!add) {
                        dup++;
                    }
                }
            }
        }
        reader.close();
        if (equalTest)
            assertEquals(seqSet, set2);
        System.out.println();
        System.out.println("SET1 only=" + Sets.difference(seqSet, set2).size());
        System.out.println("Intersection=" + Sets.intersection(seqSet, set2).size());
        System.out.println("SET2 only=" + Sets.difference(set2, seqSet).size());

        if (large) {
            System.out.println("Count: " + count0 + "/" + count2);
        } else {
            System.out.println("Duplicate: " + dup1 + "/" + dup);
        }
    }

    @Test
    void testOpeRATOR() throws IOException
    {
        Protease protease = new Protease(Enzyme.ofName("OpeRATOR"), 3);
        PeptideIterator it = PeptideIterator.getIterator(protease, FileUtils.getResourcePath("a_pep.fasta").toString(),
                6, 46, true);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent ce = event.asCleavageEvent();

                for (int i = 0; i < ce.size(); i++) {
                    int startIndex = ce.getStartIndex();
                    int endIndex = ce.getEndIndex(i);

                    String peptide = it.getPeptide(startIndex + 1, endIndex - 1);
                    System.out.println(peptide);
                }
            } else if (event.isNoCleavageSeqEvent()) {
                NoCleavageSeqEvent event1 = event.asNoCleavageSeqEvent();
                for (int i = 0; i < event1.size(); i++) {
//                    System.out.println(it.getPeptide(event1.getStartIndex() + 1, event1.getEndIndex(i) - 1));
                }
            }
        }
        it.close();
    }

    @Test
    void test() throws IOException
    {
        FastaReader reader = new FastaReader(Paths.get("D:\\database\\uniprot\\uniprot-reviewed_yes.fasta"));
        while (reader.hasNext()) {
            Protein protein = reader.next();
            if (!protein.getSequence().startsWith("M")) {
                System.out.println(protein.getAccession());
            }
        }
        reader.close();
    }
}