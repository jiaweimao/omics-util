package omics.util.protein.digest.suffix;

import com.google.common.collect.Range;
import omics.util.io.FileUtils;
import omics.util.protein.Peptide;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.digest.Protease;
import omics.util.protein.digest.util.LengthDigestionController;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 8:26 AM
 */
class SpecificIteratorTest
{
    private final String protein1000 = "D:\\database\\uniprot\\proteins1000.fasta";
    private final String protein101 = "D:\\database\\uniprot\\protein101.fasta";
    private final String p3 = FileUtils.getResourcePath("protein_peptide.fasta").toString();
    private final String p2 = FileUtils.getResourcePath("p2.fasta").toString();
    private final String human = "D:\\database\\uniprot\\human_reviewed_20190322.fasta";

    void testLarge(String fasta, boolean removeM, Protease.Cleavage cleavage) throws IOException
    {
        int miss = 3;
        int minLen = 6;
        int maxLen = 40;
        Protease protease = new Protease(Enzyme.ofName("Trypsin/P"), miss, cleavage).controller(new LengthDigestionController(minLen, maxLen));

        long count0 = 0;
        PeptideIterator it = PeptideIterator.getIterator(protease, fasta, minLen, maxLen, removeM);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                count0 += cleavageEvent.size();
            } else if (event.isNoCleavageSeqEvent()) {
                NoCleavageSeqEvent event1 = event.asNoCleavageSeqEvent();
                count0 += event1.size();
            }
        }
        it.close();

        FastaReader reader = new FastaReader(Paths.get(fasta));
        long count2 = 0;
        while (reader.hasNext()) {
            omics.util.protein.database.Protein protein = reader.next();
            String sequence = protein.getSequence();
            if (removeM && sequence.startsWith("M"))
                sequence = sequence.substring(1);
            List<Peptide> peptideList = protease.digest(new omics.util.protein.Protein(protein.getAccession(), sequence));
            for (Peptide peptide : peptideList) {
                String s = peptide.toSymbolString();
                if (s.contains("U"))
                    continue;
                count2++;
            }
        }
        reader.close();
        System.out.println("Duplicate : " + count0 + "/" + count2);
    }

    @Test
    void testSpecificP3RM() throws IOException
    {
        test(p3, true, Protease.Cleavage.Specific, false, true, false);
    }

    @Test
    void testSpecificP3KM() throws IOException
    {
        test(p3, false, Protease.Cleavage.Specific, false, true, false);
    }

    @Test
    void testSpecificP101KM() throws IOException
    {
        test(protein101, false, Protease.Cleavage.Specific, false, true, false);
    }

    @Test
    void testSpecificP101RM() throws IOException
    {
        test(protein101, true, Protease.Cleavage.Specific, false, true, false);
    }

    @Test
    void testSpecificRMP1000() throws IOException
    {
        test(protein1000, true, Protease.Cleavage.Specific, false, true, false);
    }

    @Test
    void testSpecificKMP1000() throws IOException
    {
        test(protein1000, false, Protease.Cleavage.Specific, false, true, false);
    }

    @Test
    void testSpecificHumanRM() throws IOException
    {
        test(human, true, Protease.Cleavage.Specific, false, true, false);
    }

    @Test
    void testSpecificHumanKM() throws IOException
    {
        test(human, false, Protease.Cleavage.Specific, false, true, false);
    }

    private void test(String fasta, boolean removeM, Protease.Cleavage cleavage, boolean print, boolean equalTest, boolean large) throws IOException
    {
        int miss = 3;
        int minLen = 6;
        int maxLen = 40;
        long count0 = 0;
        int dup1 = 0;
        Range<Integer> range = Range.closed(minLen, maxLen);
        Protease protease = new Protease(Enzyme.ofName("Trypsin/P"), miss, cleavage).controller(new LengthDigestionController(minLen, maxLen));
        Set<String> seqSet = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease, fasta, minLen, maxLen, removeM);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                count0 += cleavageEvent.size();
                if (!large) {
                    int startIndex = cleavageEvent.getStartIndex();
                    for (int i = 0; i < cleavageEvent.size(); i++) {
                        int endIndex = cleavageEvent.getEndIndex(i);
                        String peptide = it.getPeptide(startIndex + 1, endIndex - 1);
                        assertTrue(range.contains(peptide.length()), peptide.length() + "\t" + startIndex + "\t" + endIndex);
//                    System.out.println(startIndex + "\t" + endIndex);
                        boolean add = seqSet.add(peptide);
                        if (!add) {
                            if (print)
                                System.out.println("DP0:" + startIndex + "\t" + endIndex + "\t" + peptide);
                            dup1++;
                        }
                    }
                }
            } else if (event.isNoCleavageSeqEvent()) {
                NoCleavageSeqEvent event1 = event.asNoCleavageSeqEvent();
                count0 += event1.size();
                if (!large) {
                    for (int i = 0; i < event1.size(); i++) {
                        int startIndex = event1.getStartIndex();
                        int endIndex = event1.getEndIndex(i);
//                    System.out.println(event1.getStartIndex() + "\t" + event1.getEndIndex(i));
                        String pep = it.getPeptide(startIndex + 1, endIndex - 1);
                        assertTrue(range.contains(pep.length()), pep.length() + "\t" + startIndex + "\t" + endIndex);
                        boolean add = seqSet.add(pep);
                        if (!add) {
                            if (print)
                                System.out.println("DP1:" + startIndex + "\t" + endIndex + "\t" + pep);
                            dup1++;
                        }
                    }
                }
            }
        }
        it.close();

        if (!large)
            System.out.println("PEP=" + seqSet.size());

        FastaReader reader = new FastaReader(Paths.get(fasta));
        Set<String> set2 = new HashSet<>();
        int dup = 0;
        long count2 = 0;
        while (reader.hasNext()) {
            omics.util.protein.database.Protein protein = reader.next();
            String sequence = protein.getSequence();
            if (removeM && sequence.startsWith("M")) {
                sequence = sequence.substring(1);
            }
            List<Peptide> peptideList = protease.digest(new omics.util.protein.Protein(protein.getAccession(), sequence));
            for (Peptide peptide : peptideList) {
                String s = peptide.toSymbolString();
                if (s.contains("U"))
                    continue;
                count2++;
                if (!large) {
                    assertTrue(seqSet.contains(s), protein.getAccession() + "\t" + s);
                    boolean add = set2.add(s);
                    if (!add) {
                        dup++;
                    }
                }
            }
        }
        reader.close();
        if (equalTest)
            assertEquals(seqSet, set2);
        if (large) {
            System.out.println("Count: " + count0 + "/" + count2);
        } else {
            System.out.println("Duplicate: " + dup1 + "/" + dup);
        }
    }

    @Test
    void testSemiP3KM() throws IOException
    {
        test(p3, false, Protease.Cleavage.Semi, false, false, false);
    }

    @Test
    void testSemiP3RM() throws IOException
    {
        test(p3, true, Protease.Cleavage.Semi, false, false, false);
    }

    @Test
    void testSemiP1000RM() throws IOException
    {
        test(protein1000, true, Protease.Cleavage.Semi, false, false, false);
    }

    @Test
    void testSemiP1000KM() throws IOException
    {
        test(protein1000, false, Protease.Cleavage.Semi, false, false, false);
    }

    @Test
    void testSemi101KeepM() throws IOException
    {
        test(protein101, false, Protease.Cleavage.Semi, false, false, false);
    }

    @Test
    void testSemi101RemoveM() throws IOException
    {
        test(protein101, true, Protease.Cleavage.Semi, false, false, false);
    }

    @Test
    void testSemiHumanRemoveM() throws IOException
    {
        test(human, true, Protease.Cleavage.Semi, false, false, true);
    }

    @Test
    void testSemiUniprotRemoveM() throws IOException
    {
        Path database = Paths.get("D:\\database\\uniprot\\uniprot-reviewed_yes.fasta");
        boolean removeM = true;
        testLarge(database.toString(), removeM, Protease.Cleavage.Semi);
    }

    @Test
    void testSemiHumanKeepM() throws IOException
    {
        Path database = Paths.get("D:\\database\\uniprot\\human_reviewed_20190322.fasta");
        boolean removeM = false;
        testLarge(database.toString(), removeM, Protease.Cleavage.Semi);
    }

    @Test
    void testSemiP2KeepM() throws IOException
    {
        Path path = FileUtils.getResourcePath("p2.fasta");
        int miss = 3;
        Protease protease = new Protease(Enzyme.ofName("Trypsin/P"), miss, Protease.Cleavage.Semi).controller(new LengthDigestionController(7, 40));

        int dup0 = 0;
        Set<String> seqSet = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease, path.toString(), 7, 40, false);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                for (int i = 0; i < cleavageEvent.size(); i++) {
                    int startIndex = cleavageEvent.getStartIndex();
                    int endIndex = cleavageEvent.getEndIndex(i);
                    String pep = it.getPeptide(startIndex + 1, endIndex - 1);
                    boolean add = seqSet.add(pep);
                    if (!add) {
                        System.out.println("DP:" + startIndex + "\t" + endIndex + "\t" + pep);
                        dup0++;
                    }
                }
            } else if (event.isNoCleavageSeqEvent()) {
                NoCleavageSeqEvent event1 = event.asNoCleavageSeqEvent();
                for (int i = 0; i < event1.size(); i++) {
                    int startIndex = event1.getStartIndex();
                    int endIndex = event1.getEndIndex(i);
                    String pep = it.getPeptide(startIndex + 1, endIndex - 1);
//                    System.out.println(event1.getStartIndex() + "\t" + event1.getEndIndex(i));
                    boolean add = seqSet.add(pep);
                    if (!add) {
                        System.out.println("DP1:" + startIndex + "\t" + endIndex + "\t" + pep);
                        dup0++;
                    }
                }
            }
        }
        it.close();
        System.out.println("PEP COUNT=" + seqSet.size());

        int dup = 0;
        FastaReader reader = new FastaReader(path);
        Set<String> set2 = new HashSet<>();
        while (reader.hasNext()) {
            omics.util.protein.database.Protein protein = reader.next();
            List<Peptide> peptideList = protease.digest(new omics.util.protein.Protein(protein.getAccession(), protein.getSequence()));
            for (Peptide peptide : peptideList) {
                String s = peptide.toSymbolString();
                boolean add = set2.add(s);
                if (!add) {
                    dup++;
                }
                if (!seqSet.contains(s)) {
                    System.out.println(protein.getAccession() + "\t" + s);
                }
            }
        }
        reader.close();

        System.out.println(dup0 + "/" + dup);
    }

    @Test
    void testSemi() throws IOException
    {
        String database = "D:\\database\\uniprot\\protein101.fasta";
        int miss = 3;
        Protease protease = new Protease(Enzyme.ofName("Trypsin/P"), miss, Protease.Cleavage.Semi).controller(new LengthDigestionController(7, 40));

        Range<Integer> range = Range.closed(7, 40);
        int dup0 = 0;
        Set<String> seqSet = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease, database, 7, 40, false);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                int startIndex = cleavageEvent.getStartIndex();
                for (int i = 0; i < cleavageEvent.size(); i++) {
                    int endIndex = cleavageEvent.getEndIndex(i);
                    String pep = it.getPeptide(startIndex + 1, endIndex - 1);
                    if (!range.contains(pep.length())) {
                        System.out.println("LEN=" + startIndex + "\t" + endIndex + "\t" + pep);
                    }
//                    System.out.println(cleavageEvent.getStartIndex() + "\t" + cleavageEvent.getEndIndex(i));
                    boolean add = seqSet.add(pep);
                    if (!add) {
                        System.out.println("DP0:" + startIndex + "\t" + endIndex + "\t" + pep);
                        dup0++;
                    }
                }
            } else if (event.isNoCleavageSeqEvent()) {
                NoCleavageSeqEvent event1 = event.asNoCleavageSeqEvent();
                int startIndex = event1.getStartIndex();
                for (int i = 0; i < event1.size(); i++) {
                    int endIndex = event1.getEndIndex(i);
                    String pep = it.getPeptide(startIndex + 1, endIndex - 1);
                    if (!range.contains(pep.length())) {
                        System.out.println("LEN1=" + startIndex + "\t" + endIndex + "\t" + pep);
                    }
                    boolean add = seqSet.add(pep);
                    if (!add) {
                        System.out.println("DP1:" + startIndex + "\t" + endIndex + "\t" + pep);
                        dup0++;
                    }
                }
            }
        }
        it.close();
        System.out.println("PEP COUNT=" + seqSet.size());

        int dup = 0;
        FastaReader reader = new FastaReader(Paths.get(database));
        Set<String> set2 = new HashSet<>();
        while (reader.hasNext()) {
            omics.util.protein.database.Protein protein = reader.next();
            List<Peptide> peptideList = protease.digest(new omics.util.protein.Protein(protein.getAccession(), protein.getSequence()));
            for (Peptide peptide : peptideList) {
                String s = peptide.toSymbolString();
                boolean add = set2.add(s);
                if (!add) {
                    dup++;
                }
                if (!seqSet.contains(s)) {
                    System.out.println(protein.getAccession() + "\t" + s);
                }
            }
        }
        reader.close();

        System.out.println(dup0 + "/" + dup);
    }

}