package omics.util.protein.digest.suffix;

import omics.util.io.FileUtils;
import omics.util.protein.database.Protein;
import omics.util.protein.database.uniprot.FastaReader;
import omics.util.protein.digest.Enzyme;
import omics.util.protein.digest.Protease;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2019, 8:29 AM
 */
class UnspecificIteratorTest
{
    private final String protein1000 = "D:\\database\\uniprot\\proteins1000.fasta";

    @Test
    void testUnSpecificRemoveM() throws IOException
    {
        Protease protease = new Protease(Enzyme.ofName("unspecific cleavage"));
//        Path resourcePath = FileUtils.getResourcePath("protein_peptide.fasta");

        Set<String> pepSeq = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease, protein1000, 6, 40, true);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent startEvent = event.asCleavageEvent();
                for (int i = 0; i < startEvent.size(); i++) {
                    int index = startEvent.getEndIndex(i);
                    boolean add = pepSeq.add(it.getPeptide(startEvent.getStartIndex() + 1, index - 1));
                    assertTrue(add);
                }
            }
        }
        it.close();

        System.out.println("PEP=" + pepSeq.size());

        int dup = 0;
        Set<String> set2 = new HashSet<>();
        FastaReader reader = new FastaReader(Paths.get(protein1000));
        while (reader.hasNext()) {
            Protein protein = reader.next();
            String sequence = protein.getSequence();
            if (sequence.startsWith("M")) {
                sequence = sequence.substring(1);
            }
            for (int i = 0; i < sequence.length(); i++) {
                for (int len = 6; len <= 40; len++) {
                    int end = i + len;
                    if (end > sequence.length())
                        break;
                    String seq = sequence.substring(i, end);
                    boolean add = set2.add(seq);
                    if (!add) {
                        dup++;
                    }
                }
            }
        }
        reader.close();
        assertEquals(pepSeq, set2);
        System.out.println("Duplicates in remove M 3: " + dup);
    }

    @Test
    void testUnSpecificRemoveM100() throws IOException
    {
        Protease protease = new Protease(Enzyme.ofName("unspecific cleavage"));
        Path resourcePath = Paths.get("D:\\database\\uniprot\\protein101.fasta");

        Set<String> pepSeq = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease, resourcePath.toString(), 6, 40, true);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent startEvent = event.asCleavageEvent();
                for (int i = 0; i < startEvent.size(); i++) {
                    int index = startEvent.getEndIndex(i);
                    boolean add = pepSeq.add(it.getPeptide(startEvent.getStartIndex() + 1, index - 1));
                    assertTrue(add);
                }
            }
        }
        it.close();

        System.out.println("PEP=" + pepSeq.size());
        int dup = 0;
        Set<String> set2 = new HashSet<>();
        FastaReader reader = new FastaReader(resourcePath);
        while (reader.hasNext()) {
            Protein protein = reader.next();
            String sequence = protein.getSequence();
            if (sequence.startsWith("M")) {
                sequence = sequence.substring(1);
            }
            for (int i = 0; i < sequence.length(); i++) {
                for (int len = 6; len <= 40; len++) {
                    int end = i + len;
                    if (end > sequence.length())
                        break;
                    String seq = sequence.substring(i, end);
                    boolean add = set2.add(seq);
                    if (!add) {
                        dup++;
                    }
                }
            }
        }
        reader.close();
        assertEquals(pepSeq, set2);
        System.out.println("Duplicates in remove M 100: " + dup);
    }

    @Test
    void testUnSpecificKeepM() throws IOException
    {
        Protease protease = new Protease(Enzyme.ofName("unspecific cleavage"));
        Path resourcePath = FileUtils.getResourcePath("protein_peptide.fasta");

        int dup1 = 0;
        Set<String> pepSeq = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease, resourcePath.toString(), 6, 40, false);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                for (int i = 0; i < cleavageEvent.size(); i++) {
                    int index = cleavageEvent.getEndIndex(i);
                    boolean add = pepSeq.add(it.getPeptide(cleavageEvent.getStartIndex() + 1, index - 1));
                    if (!add)
                        dup1++;
                }
            }
        }
        it.close();

        System.out.println("PEP=" + pepSeq.size());

        Set<String> set2 = new HashSet<>();
        FastaReader reader = new FastaReader(resourcePath);
        int dup = 0;
        while (reader.hasNext()) {
            Protein protein = reader.next();
            String sequence = protein.getSequence();

            for (int i = 0; i < sequence.length(); i++) {
                for (int len = 6; len <= 40; len++) {
                    int end = i + len;
                    if (end > sequence.length())
                        break;
                    String seq = sequence.substring(i, end);
                    if (!pepSeq.contains(seq)) {
                        System.out.println(protein.getAccession() + "\t" + seq);
                    }
                    boolean add = set2.add(seq);
                    if (!add) {
                        dup++;
                    }
                }
            }
        }
        reader.close();
        assertEquals(pepSeq, set2);
        System.out.println("Duplicates in keep M 3: " + dup1 + "/" + dup);
    }

    @Test
    void testUnSpecificKeepMLarge() throws IOException
    {
        Protease protease = new Protease(Enzyme.ofName("unspecific cleavage"));
//        Path database = Paths.get("D:\\database\\uniprot\\human_reviewed_20190322.fasta");
        Path database = Paths.get("C:\\Users\\Chen\\IdeaProjects\\data\\homo_20191210.fasta");

        long count = 0;
        PeptideIterator it = PeptideIterator.getIterator(protease, database.toString(), 6, 40, false);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            int startIndex = event.getStartIndex();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                for (int i = 0; i < cleavageEvent.size(); i++) {
                    int index = cleavageEvent.getEndIndex(i);
                    count++;
                    System.out.println(it.getPeptide(startIndex, index));
                }
            }
        }
        it.close();
//
//        FastaReader reader = new FastaReader(database);
//        long dup = 0;
//        while (reader.hasNext()) {
//            Protein protein = reader.next();
//            String sequence = protein.getSequence();
//
//            for (int i = 0; i < sequence.length(); i++) {
//                for (int len = 6; len <= 40; len++) {
//                    int end = i + len;
//                    if (end > sequence.length())
//                        break;
//                    String seq = sequence.substring(i, end);
//                    dup++;
//                }
//            }
//        }
//        reader.close();
//        System.out.println("Duplicates in keep M 3: " + count + "/" + dup);
    }

    @Test
    void testUnSpecific100M() throws IOException
    {
        Protease protease = new Protease(Enzyme.ofName("unspecific cleavage"));
        Path resourcePath = Paths.get("D:\\database\\uniprot\\protein101.fasta");

        int dup1 = 0;
        Set<String> pepSeq = new HashSet<>();
        PeptideIterator it = PeptideIterator.getIterator(protease, resourcePath.toString(), 6, 40, false);
        while (it.hasNext()) {
            SuffixEvent event = it.next();
            if (event.isCleavageEvent()) {
                CleavageEvent cleavageEvent = event.asCleavageEvent();
                for (int i = 0; i < cleavageEvent.size(); i++) {
                    int index = cleavageEvent.getEndIndex(i);
                    boolean add = pepSeq.add(it.getPeptide(cleavageEvent.getStartIndex() + 1, index - 1));
                    if (!add) {
                        dup1++;
                    }
                }
            }
        }
        it.close();

        System.out.println("PEP=" + pepSeq.size());

        Set<String> set2 = new HashSet<>();
        FastaReader reader = new FastaReader(resourcePath);
        int dup = 0;
        while (reader.hasNext()) {
            Protein protein = reader.next();
            String sequence = protein.getSequence();

            for (int i = 0; i < sequence.length(); i++) {
                for (int len = 6; len <= 40; len++) {
                    int end = i + len;
                    if (end > sequence.length())
                        break;
                    String seq = sequence.substring(i, end);
                    if (!pepSeq.contains(seq)) {
                        System.out.println(protein.getAccession() + "\t" + seq);
                    }
                    boolean add = set2.add(seq);
                    if (!add) {
                        dup++;
                    }
                }
            }
        }
        reader.close();
        assertEquals(pepSeq, set2);
        System.out.println("Duplicates in keep M 100: " + dup1 + "/" + dup);
    }
}