package omics.util.protein.digest.util;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author JiaweiMao 2017.03.17
 * @since 1.0-SNAPSHOT
 */
class ForwardIntIntervalIteratorTest
{

    @Test
    void test()
    {
        Iterator<IntInterval> iterator = new ForwardIntIntervalIterator(0, 4);

        List<Integer> expectedFirsts = Arrays.asList(0, 0, 0, 0, 1, 1, 1, 2, 2, 3);
        List<Integer> expectedSeconds = Arrays.asList(1, 2, 3, 4, 2, 3, 4, 3, 4, 4);

        int count = 0;
        while (iterator.hasNext()) {
            IntInterval interval = iterator.next();
            assertEquals(expectedFirsts.get(count).intValue(), interval.getLowerBound());
            assertEquals(expectedSeconds.get(count).intValue(), interval.getUpperBound());

            count++;
        }

        assertEquals(10, count);
    }


    @Test
    void test2()
    {
        Iterator<IntInterval> iterator = new ForwardIntIntervalIterator(0, 4, 5);

        List<Integer> expectedFirsts = Arrays.asList(0, 0, 0, 0, 1, 1, 1, 2, 2, 3);
        List<Integer> expectedSeconds = Arrays.asList(1, 2, 3, 4, 2, 3, 4, 3, 4, 4);

        int count = 0;
        while (iterator.hasNext()) {

            IntInterval interval = iterator.next();

            assertEquals(expectedFirsts.get(count).intValue(), interval.getLowerBound());
            assertEquals(expectedSeconds.get(count).intValue(), interval.getUpperBound());

            count++;
        }

        assertEquals(10, count);
    }

    @Test
    void testNoPairConsumedInLoop()
    {
        Iterator<IntInterval> iterator = new ForwardIntIntervalIterator(0, 4);

        // consume first interval
        IntInterval interval = iterator.next();

        assertEquals(0, interval.getLowerBound());
        assertEquals(1, interval.getUpperBound());

        int count = 0;
        while (iterator.hasNext()) {

            count++;
            if (count == 15)
                break;
        }

        assertEquals(15, count);

        // consume second interval
        interval = iterator.next();

        assertEquals(0, interval.getLowerBound());
        assertEquals(2, interval.getUpperBound());

    }

    @Test
    void testNoHasNext()
    {
        Iterator<IntInterval> iterator = new ForwardIntIntervalIterator(0, 4);

        List<Integer> expectedFirsts = Arrays.asList(0, 0, 0, 0, 1, 1, 1, 2, 2, 3, -1, -1, -1, -1, -1);
        List<Integer> expectedSeconds = Arrays.asList(1, 2, 3, 4, 2, 3, 4, 3, 4, 4, -1, -1, -1, -1, -1);

        int count = 0;
        while (true) {

            IntInterval interval = iterator.next();

            assertEquals(expectedFirsts.get(count).intValue(), interval.getLowerBound());
            assertEquals(expectedSeconds.get(count).intValue(), interval.getUpperBound());

            count++;

            if (count == 15)
                break;
        }

        assertEquals(15, count);
    }

    @Test
    void testWithDistConstraint()
    {
        Iterator<IntInterval> iterator = new ForwardIntIntervalIterator(0, 4, 2);

        List<Integer> expectedFirsts = Arrays.asList(0, 0, 1, 1, 2, 2, 3);
        List<Integer> expectedSeconds = Arrays.asList(1, 2, 2, 3, 3, 4, 4);

        int count = 0;
        while (iterator.hasNext()) {

            IntInterval interval = iterator.next();

            assertEquals(expectedFirsts.get(count).intValue(), interval.getLowerBound());
            assertEquals(expectedSeconds.get(count).intValue(), interval.getUpperBound());

            count++;
        }

        assertEquals(7, count);
    }

    @Test
    void testBadConstructor()
    {
        assertThrows(IllegalArgumentException.class, () -> new ForwardIntIntervalIterator(1, 1));
    }

    @Test
    void testBadRangeConstraintConstructor()
    {
        assertThrows(IllegalArgumentException.class, () -> new ForwardIntIntervalIterator(1, 2, 0));
    }
}