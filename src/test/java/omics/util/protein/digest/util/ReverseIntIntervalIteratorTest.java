package omics.util.protein.digest.util;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao 2017.03.17
 * @since 1.0-SNAPSHOT
 */
class ReverseIntIntervalIteratorTest
{
    @Test
    void test()
    {
        Iterator<IntInterval> iterator = new ReverseIntIntervalIterator(0, 4);

        List<Integer> expectedFirsts = Arrays.asList(3, 2, 1, 0, 2, 1, 0, 1, 0, 0);
        List<Integer> expectedSeconds = Arrays.asList(4, 4, 4, 4, 3, 3, 3, 2, 2, 1);

        int count = 0;
        while (iterator.hasNext()) {

            IntInterval interval = iterator.next();

            assertEquals(expectedFirsts.get(count).intValue(), interval.getLowerBound());
            assertEquals(expectedSeconds.get(count).intValue(), interval.getUpperBound());

            count++;
        }

        assertEquals(10, count);
    }

    @Test
    void testNoPairConsumedInLoop()
    {
        Iterator<IntInterval> iterator = new ReverseIntIntervalIterator(0, 4);

        // consume first interval
        IntInterval interval = iterator.next();

        assertEquals(3, interval.getLowerBound());
        assertEquals(4, interval.getUpperBound());

        int count = 0;
        while (iterator.hasNext()) {

            count++;

            if (count == 15)
                break;
        }

        assertEquals(15, count);

        // consume second interval
        interval = iterator.next();

        assertEquals(2, interval.getLowerBound());
        assertEquals(4, interval.getUpperBound());

    }

    @Test
    void testNoHasNext()
    {
        Iterator<IntInterval> iterator = new ReverseIntIntervalIterator(0, 4);

        List<Integer> expectedFirsts = Arrays.asList(3, 2, 1, 0, 2, 1, 0, 1, 0, 0, -1, -1, -1, -1, -1);
        List<Integer> expectedSeconds = Arrays.asList(4, 4, 4, 4, 3, 3, 3, 2, 2, 1, -1, -1, -1, -1, -1);

        int count = 0;
        while (true) {

            IntInterval interval = iterator.next();

            assertEquals(expectedFirsts.get(count).intValue(), interval.getLowerBound());
            assertEquals(expectedSeconds.get(count).intValue(), interval.getUpperBound());

            count++;

            if (count == 15)
                break;
        }

        assertEquals(15, count);
    }

    @Test
    void testWithDistConstraint()
    {
        Iterator<IntInterval> iterator = new ReverseIntIntervalIterator(0, 4, 2);

        List<Integer> expectedFirsts = Arrays.asList(3, 2, 2, 1, 1, 0, 0);
        List<Integer> expectedSeconds = Arrays.asList(4, 4, 3, 3, 2, 2, 1);

        int count = 0;
        while (iterator.hasNext()) {

            IntInterval interval = iterator.next();

            assertEquals(expectedFirsts.get(count).intValue(), interval.getLowerBound());
            assertEquals(expectedSeconds.get(count).intValue(), interval.getUpperBound());

            count++;
        }

        assertEquals(7, count);
    }
}