package omics.util.protein.mod;

import omics.util.chem.AtomicSymbol;
import omics.util.chem.Composition;
import omics.util.chem.Mass;
import omics.util.chem.PeriodicTable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ModificationTest
{
    // There are two ways to build an instance of *Modification* class,
    // using the constructor with a label and a *Mass* (*NumericMass*,*Composition* and *NeutralLoss* are *Mass*es)
    @Test
    void newModifFromMass()
    {
        Modification mod = new Modification("phospho",
                new Composition.Builder().add(AtomicSymbol.H).add(AtomicSymbol.P).add(AtomicSymbol.O, 3).build());

        assertEquals(79.96633, mod.getMolecularMass(), 0.00001);
    }

    // or using the static factory method parseModification from a string with the following format: (label:)?formula
    @Test
    void newModifFromString1()
    {
        Modification mod = Modification.parseModification("phospho:HPO3");
        assertEquals(79.96633, mod.getMolecularMass(), 0.00001);
    }

    // or the same method without label (in this case the label is the formula)
    @Test
    void newModifFromString2()
    {
        Modification mod = Modification.parseModification("HPO3");
        assertEquals(79.96633, mod.getMolecularMass(), 0.00001);
    }

    @Test
    void getters()
    {
        Modification mod = Modification.parseModification("HPO3");
        String label = mod.getTitle();
        Mass mass = mod.getMass();
        double molecularMass = mod.getMolecularMass();
        assertEquals("HPO3", label);
    }

    @Test
    void test() throws Exception
    {
        Modification mod = new Modification("phospho", new Composition(
                new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.P).add(AtomicSymbol.O, 3).build()));
        assertEquals("phospho", mod.getTitle());
        assertEquals(79.966331, mod.getMolecularMass(), 0.000001);
        assertEquals(79.9799, ((Composition) mod.getMass()).getAverageMass(), 0.0001);
    }

    @Test
    void testParseModification() throws Exception
    {
        Modification modification = Modification.parseModification("H2H[2]PO4");

        Composition composition = (Composition) modification.getMass();
        assertEquals(2, composition.getCount(PeriodicTable.H));
        assertEquals(1, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        assertEquals(1, composition.getCount(PeriodicTable.P));
        assertEquals(4, composition.getCount(PeriodicTable.O));
        assertEquals("H2H[2]PO4", modification.getTitle());
    }

    @Test
    void testParseModificationWithLabel() throws Exception
    {
        Modification modification = Modification.parseModification("Phospho:H[2]PO3");

        Composition composition = (Composition) modification.getMass();
        assertEquals(0, composition.getCount(PeriodicTable.H));
        assertEquals(1, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        assertEquals(1, composition.getCount(PeriodicTable.P));
        assertEquals(3, composition.getCount(PeriodicTable.O));
        assertEquals("Phospho", modification.getTitle());
    }

    @Test
    void testParseModificationWithLabel2() throws Exception
    {
        Modification modification = Modification.parseModification("H[2]PO3:H[2]PO3");

        Composition composition = (Composition) modification.getMass();
        assertEquals(0, composition.getCount(PeriodicTable.H));
        assertEquals(1, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        assertEquals(1, composition.getCount(PeriodicTable.P));
        assertEquals(3, composition.getCount(PeriodicTable.O));
        assertEquals("H[2]PO3", modification.getTitle());
    }

    @Test
    void testParseModificationWithLabel3() throws Exception
    {
        Modification modification = Modification.parseModification("Phosphate loss:H[2]-3P-1O-4");

        Composition composition = (Composition) modification.getMass();
        assertEquals(0, composition.getCount(PeriodicTable.H));
        assertEquals(-3, composition.getCount(PeriodicTable.getInstance().getAtom(AtomicSymbol.H, 2)));
        assertEquals(-1, composition.getCount(PeriodicTable.P));
        assertEquals(-4, composition.getCount(PeriodicTable.O));
        assertEquals("Phosphate loss", modification.getTitle());
    }

    @Test
    void testParseModificationWithLabel4() throws Exception
    {
        Modification modification = Modification.parseModification("H3PO4 loss:H-3P-1O-4");

        Composition composition = (Composition) modification.getMass();
        assertEquals(-3, composition.getCount(PeriodicTable.H));
        assertEquals(-1, composition.getCount(PeriodicTable.P));
        assertEquals(-4, composition.getCount(PeriodicTable.O));
        assertEquals("H3PO4 loss", modification.getTitle());
    }

    @Test
    void testParseModificationWithLabel5() throws Exception
    {
        Modification modification = Modification.parseModification("Loss:H(3) P O(4):H-3P-1O-4");

        Composition composition = (Composition) modification.getMass();
        assertEquals(-3, composition.getCount(PeriodicTable.H));
        assertEquals(-1, composition.getCount(PeriodicTable.P));
        assertEquals(-4, composition.getCount(PeriodicTable.O));
        assertEquals("Loss:H(3) P O(4)", modification.getTitle());
    }

    @Test
    void testParseIllegalModification1() throws Exception
    {
        Modification modification = Modification.parseModification("H3PO4:loss:H-3P-1O-4");

        Composition composition = (Composition) modification.getMass();
        assertEquals(-3, composition.getCount(PeriodicTable.H));
        assertEquals(-1, composition.getCount(PeriodicTable.P));
        assertEquals(-4, composition.getCount(PeriodicTable.O));
        assertEquals("H3PO4:loss", modification.getTitle());
    }

    @Test
    void testParseIllegalModification2()
    {
        assertThrows(IllegalArgumentException.class, () -> Modification.parseModification("H[2]]3PO4"));
    }

    @Test
    void testParseIllegalModification3()
    {
        assertThrows(IllegalArgumentException.class, () -> Modification.parseModification("H3P-O4"));
    }

    @Test
    void testParseIllegalModification5()
    {
        assertThrows(IllegalArgumentException.class, () -> Modification.parseModification("H3P+O4"));
    }

    @Test
    void testParseIllegalModification6()
    {
        assertThrows(IllegalArgumentException.class, () -> Modification.parseModification("H3PO4;"));
    }

    @Test
    void testParseIllegalModification7()
    {
        assertThrows(IllegalArgumentException.class, () -> Modification.parseModification(";H3PO4"));
    }

    @Test
    void testParseRoundTrip() throws Exception
    {
        String string = "Phospho:HPO3";
        Modification modification = Modification.parseModification(string);

        Composition composition = (Composition) modification.getMass();
        assertEquals(1, composition.getCount(PeriodicTable.H));
        assertEquals(1, composition.getCount(PeriodicTable.P));
        assertEquals(3, composition.getCount(PeriodicTable.O));

        assertEquals(string, modification.toString());
    }

    @Test
    void testChargedModification() throws Exception
    {
        Modification mod = Modification.parseModification("C-1H-4S-1(+)");

        Composition composition = (Composition) mod.getMass();
        assertEquals(-1, composition.getCount(PeriodicTable.C));
        assertEquals(-4, composition.getCount(PeriodicTable.H));
        assertEquals(-1, composition.getCount(PeriodicTable.S));
        assertEquals(1, composition.getCharge());
    }

    @Test
    void testChargedModification2() throws Exception
    {
        Modification mod = Modification.parseModification("C-1H-4S-1(2+)");

        Composition composition = (Composition) mod.getMass();
        assertEquals(-1, composition.getCount(PeriodicTable.C));
        assertEquals(-4, composition.getCount(PeriodicTable.H));
        assertEquals(-1, composition.getCount(PeriodicTable.S));
        assertEquals(2, composition.getCharge());
    }

}