package omics.util.protein.mod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 04 Dec 2019, 3:39 PM
 */
class NeutralLossTest
{
    @Test
    void testEmpty()
    {
        System.out.println(NeutralLoss.EMPTY);
        System.out.println(NeutralLoss.EMPTY.getMolecularMass());
    }

    @Test
    void getFormattedSymbol()
    {
        String formattedSymbol = NeutralLoss.getFormattedSymbol(new NeutralLoss[]{NeutralLoss.H2O_LOSS, NeutralLoss.H2O_LOSS});
        assertEquals(formattedSymbol, "°°");
    }

    @Test
    void getFormattedName()
    {
        assertEquals(NeutralLoss.getFormattedName(new NeutralLoss[]{NeutralLoss.H2O_LOSS, NeutralLoss.H2O_LOSS}), "-2H2O");
        assertEquals(NeutralLoss.getFormattedName(new NeutralLoss[]{NeutralLoss.H2O_LOSS, NeutralLoss.H3PO4_LOSS}), "-H2O-H3PO4");
    }
}