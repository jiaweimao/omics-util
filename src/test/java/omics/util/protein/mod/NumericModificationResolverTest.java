package omics.util.protein.mod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao 2017.04.01
 * @since 1.0-SNAPSHOT
 */
class NumericModificationResolverTest
{

    @Test
    void testResolve() throws Exception
    {
        NumericModificationResolver modResolver = new NumericModificationResolver();

        Modification mod = modResolver.resolve("79.9");
        assertNotNull(mod);
        assertEquals(79.9, mod.getMolecularMass(), 0.0001);
        assertEquals("79.9", mod.getTitle());
    }

    @Test
    void testResolve2() throws Exception
    {
        NumericModificationResolver modResolver = new NumericModificationResolver();
        assertNull(modResolver.resolve("p"));
        assertNull(modResolver.resolve("H3PO4"));
        assertNull(modResolver.resolve("#!"));
    }
}