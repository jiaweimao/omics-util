package omics.util.protein.mod;

import omics.util.chem.Composition;
import omics.util.protein.AminoAcid;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 May 2018, 9:08 PM
 */
class PTMTest
{
    @Test
    void testGetSpecificityList()
    {
        PTM acetyl = PTM.Acetyl();
        assertEquals(acetyl.getMolecularMass(), 42.010565, 1e-6);
        assertEquals(acetyl.getFullName(), "Acetylation");
        assertEquals(acetyl.getAccession(), "UNIMOD:1");
        assertEquals(acetyl.getComposition(), Composition.parseComposition("H2C2O"));
        assertEquals(acetyl.getUserName(), "admin");
        assertNull(acetyl.getMiscNote());

        Set<Specificity> specificities = acetyl.getSpecificitySet();
        assertEquals(specificities.size(), 9);
        assertTrue(specificities.contains(new Specificity(AminoAcid.T, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.N_TERM, Pos.PROTEIN_N_TERM, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.S, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.C, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.N_TERM, Pos.ANY_N_TERM, "Multiple")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.K, Pos.ANY_WHERE, "Multiple")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.Y, Pos.ANY_WHERE, "Chemical derivative")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.H, Pos.ANY_WHERE, "Chemical derivative")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.R, Pos.ANY_WHERE, "Artefact")));
    }

    @Test
    void testSpecificityNL()
    {
        PTM phospho = PTM.Phospho();
        assertEquals(phospho.getFullName(), "Phosphorylation");
        assertEquals(phospho.getAccession(), "UNIMOD:21");
        assertEquals(phospho.getUserName(), "admin");
        assertEquals(phospho.getComposition(), Composition.parseComposition("HPO3"));
        assertEquals(phospho.getMolecularMass(), 79.966331, 1e-6);

        Set<Specificity> specificities = phospho.getSpecificitySet();
        assertEquals(specificities.size(), 9);

        assertTrue(specificities.contains(new Specificity(AminoAcid.E, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.R, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.K, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.H, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.C, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.D, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.Y, Pos.ANY_WHERE, "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.T, Pos.ANY_WHERE, NeutralLoss.of("H3PO4"), "Post-translational")));
        assertTrue(specificities.contains(new Specificity(AminoAcid.S, Pos.ANY_WHERE, NeutralLoss.of("H3PO4"), "Post-translational")));
    }
}