package omics.util.protein.mod.model;

import omics.util.chem.Composition;
import omics.util.chem.Mass;
import omics.util.protein.AminoAcid;
import omics.util.protein.mod.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


public class PTMFactoryTest
{
    private static final PTMFactory factory = PTMFactory.getInstance();
    private final double eps = 1E-6;
    private final double avgEPS = 1E-4;

    @Test
    void testSize()
    {
        int size = factory.size();
        assertTrue(size >= 1486);
    }

    @Test
    public void testMutation()
    {
        PTM ptm = factory.ofName("Gln->pyro-Glu");
        assertEquals(ptm.getAccession(), "UNIMOD:28");

        ptm = factory.ofName("Glu->pyro-Glu");
        assertEquals(ptm.getAccession(), "UNIMOD:27");
    }

    @Test
    public void testWrite() throws XMLStreamException, IOException
    {
        PTMFactory factory = PTMFactory.getInstance();
        factory.write("D:\\mods.xml");
    }

    @Test
    @Disabled
    public void addUserPTM()
    {
        Specificity ks = new Specificity(AminoAcid.K, Pos.ANY_WHERE);
        Specificity rs = new Specificity(AminoAcid.R, Pos.ANY_WHERE);
        Specificity cs = new Specificity(AminoAcid.C, Pos.ANY_WHERE);
        Specificity ds = new Specificity(AminoAcid.D, Pos.ANY_WHERE);
        Specificity es = new Specificity(AminoAcid.E, Pos.ANY_WHERE);
        Specificity hs = new Specificity(AminoAcid.H, Pos.ANY_WHERE);
        Specificity ns = new Specificity(AminoAcid.N, Pos.ANY_WHERE);
        Specificity qs = new Specificity(AminoAcid.Q, Pos.ANY_WHERE);

        List<Specificity> specificities = new ArrayList<>();
        specificities.add(ks);
        specificities.add(rs);
        specificities.add(cs);
        specificities.add(ds);
        specificities.add(es);
        specificities.add(hs);
        specificities.add(ns);
        specificities.add(qs);

        PTM ptm1 = new PTM("m_methylH_1", "light mono methyl", Composition.parseComposition("CH2"), specificities, "mjw");
        PTM ptm4 = new PTM("m_methylD_1", "heavy mono methyl", Composition.parseComposition("H[2]3CH-1"), specificities, "mjw");

        specificities = new ArrayList<>();
        specificities.add(ks);
        specificities.add(rs);
        PTM ptm2 = new PTM("m_methylH_2", "light di methyl", Composition.parseComposition("C2H4"), specificities, "mjw");
        PTM ptm5 = new PTM("m_methylD_2", "heavy di methyl", Composition.parseComposition("H[2]6C2H-2"), specificities, "mjw");

        specificities = new ArrayList<>();
        specificities.add(ks);
        PTM ptm3 = new PTM("m_methylH_3", "light tri methyl", Composition.parseComposition("C3H6"), specificities, "mjw");
        PTM ptm6 = new PTM("m_methylD_3", "heavy tri methyl", Composition.parseComposition("H[2]9C3H-3"), specificities, "mjw");

        specificities = new ArrayList<>();
        specificities.add(new Specificity(AminoAcid.M, Pos.ANY_WHERE));
        PTM ptm7 = new PTM("m_met_C13D", "heavy methionine", Composition.parseComposition("C[13]H[2]3C-1H-3"), specificities, "mjw");

        factory.add(ptm1);
        factory.add(ptm2);
        factory.add(ptm3);
        factory.add(ptm4);
        factory.add(ptm5);
        factory.add(ptm6);
        factory.add(ptm7);
        factory.write();
    }

    @Test
    public void methyl()
    {
        PTM ptm = factory.ofName("Methyl");
        assertNotNull(ptm);

        assertEquals("Methyl", ptm.getTitle());
        assertEquals(Composition.parseComposition("CH2"), ptm.getComposition());
        assertEquals("Methylation", ptm.getFullName());
        assertNull(ptm.getMiscNote());

        List<Specificity> ptmSites = ptm.getSpecificityList();
        assertEquals(15, ptmSites.size());

        assertTrue(ptmSites.contains(new Specificity(AminoAcid.E, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.D, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.C_TERM, Pos.ANY_C_TERM)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.N_TERM, Pos.PROTEIN_N_TERM)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.I, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.L, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.R, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.Q, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.N_TERM, Pos.ANY_N_TERM)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.N, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.K, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.H, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.C, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.S, Pos.ANY_WHERE)));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.T, Pos.ANY_WHERE)));
    }

    @Test
    public void neutralLoss()
    {
        PTMFactory factory = PTMFactory.getInstance();
        PTM ptm = factory.ofName("Hex(3)HexNAc(2)");
        assertNotNull(ptm);

        assertEquals("Hex3HexNAc2", ptm.getFullName());

        assertEquals(892.317216, ptm.getMolecularMass(), eps);
        assertEquals(892.8068, ptm.getComposition().getAverageMass(), avgEPS);

        List<Specificity> ptmSites = ptm.getSpecificityList();
        assertEquals(3, ptmSites.size());

        assertTrue(ptmSites.contains(new Specificity(AminoAcid.T, Pos.ANY_WHERE, NeutralLoss.of("H56C34N2O25"))));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.S, Pos.ANY_WHERE, NeutralLoss.of("H56C34N2O25"))));
        assertTrue(ptmSites.contains(new Specificity(AminoAcid.N, Pos.ANY_WHERE, NeutralLoss.of("H56C34N2O25"))));
    }

    @Test
    public void edt()
    {
        PTMFactory factory = PTMFactory.getInstance();
        PTM edt = factory.ofName("Ethanedithiol");
        assertNotNull(edt);

        assertEquals(75.980527, edt.getMolecularMass(), eps);

        Composition comp = edt.getComposition();
        assertNotNull(comp);

        assertEquals(76.1838, comp.getAverageMass(), avgEPS);
        assertEquals(Composition.parseComposition("H4C2O-1S2"), comp);
    }

    @Test
    void testTerminalSpecificity()
    {
        PTM acetyl = factory.ofName("Acetyl");
        for (Specificity specificity : acetyl.getSpecificitySet()) {
            System.out.println(specificity.getAminoAcid().getShortName() + "\t" + specificity);
        }

    }

    @Test
    void testMass()
    {
        PTM ptm = factory.ofName("2-dimethylsuccinyl");
        assertNotNull(ptm);

        assertEquals(144.042259, ptm.getMolecularMass(), eps);
        Composition composition = ptm.getComposition();
        assertEquals(144.1253, composition.getAverageMass(), avgEPS);
    }

    @Test
    void testGln()
    {
        PTM amidated = factory.ofName("Amidated");
        System.out.println(amidated.getMolecularMass());
    }

    @Test
    public void phospho()
    {
        PTM phospho = factory.ofName("Phospho");
        assertNotNull(phospho);

        Mass mass = phospho.getMass();
        assertEquals(79.966331, mass.getMolecularMass(), eps);
        assertEquals(79.9799, ((Composition) mass).getAverageMass(), avgEPS);

        assertEquals("admin", phospho.getUserName());
        assertEquals("Phosphorylation", phospho.getFullName());
        assertEquals(phospho.getAccession(), "UNIMOD:21");
        assertEquals("Phospho", phospho.getTitle());
        assertEquals(79.966331, phospho.getMolecularMass(), eps);
        assertEquals(Composition.parseComposition("HPO3"), phospho.getComposition());

        Set<Specificity> specificitySet = phospho.getSpecificitySet();
        assertEquals(specificitySet.size(), 9);

        assertTrue(specificitySet.contains(new Specificity(AminoAcid.E, Pos.ANY_WHERE)));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.R, Pos.ANY_WHERE)));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.K, Pos.ANY_WHERE)));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.H, Pos.ANY_WHERE)));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.C, Pos.ANY_WHERE)));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.D, Pos.ANY_WHERE)));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.Y, Pos.ANY_WHERE)));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.T, Pos.ANY_WHERE, NeutralLoss.of("H3PO4"))));
        assertTrue(specificitySet.contains(new Specificity(AminoAcid.S, Pos.ANY_WHERE, NeutralLoss.of("H3PO4"))));
    }
}