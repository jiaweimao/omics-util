package omics.util.protein.mod.model;

import omics.util.protein.AminoAcid;
import omics.util.protein.mod.Pos;
import omics.util.protein.mod.Specificity;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class SpecificityTest
{
    @Test
    void parse()
    {
        test("N-term", Specificity.AnyNTerm);
        test("C-term", Specificity.AnyCTerm);
        test("Protein N-term", Specificity.ProteinNTerm);
        test("Protein C-term", Specificity.ProteinCTerm);
        test("Protein N-term T", new Specificity(AminoAcid.T, Pos.PROTEIN_N_TERM));
        test("Protein C-term G", new Specificity(AminoAcid.G, Pos.PROTEIN_C_TERM));
        test("N-term C", new Specificity(AminoAcid.C, Pos.ANY_N_TERM));
        test("M", new Specificity(AminoAcid.M, Pos.ANY_WHERE));
    }

    private void test(String string, Specificity expect)
    {
        Specificity specificity = Specificity.parse(string);
        assertEquals(specificity, expect);
    }
}