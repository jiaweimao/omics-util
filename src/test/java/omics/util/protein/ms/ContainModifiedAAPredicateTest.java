package omics.util.protein.ms;

import com.google.common.collect.Sets;
import omics.util.protein.AminoAcid;
import omics.util.protein.PeptideFragment;
import omics.util.protein.mod.ModAttachment;
import omics.util.protein.mod.Modification;
import omics.util.protein.ms.ContainModifiedAAPredicate;
import omics.util.protein.ms.FragmentType;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jan 2018, 9:57 PM
 */
class ContainModifiedAAPredicateTest
{
    @Test
    void testTest1()
    {
        ContainModifiedAAPredicate predicate = new ContainModifiedAAPredicate(Sets.newHashSet(AminoAcid.S, AminoAcid.T),
                Collections.singleton(Modification.parseModification("HPO3")),
                EnumSet.of(ModAttachment.SIDE_CHAIN));

        PeptideFragment fragment = PeptideFragment.parse("G", FragmentType.FORWARD);
        assertFalse(predicate.test(fragment));
    }
}