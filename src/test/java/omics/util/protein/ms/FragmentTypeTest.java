package omics.util.protein.ms;


import omics.util.protein.ms.FragmentType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2019, 9:33 PM
 */
class FragmentTypeTest
{

    @Test
    void testOrdinal()
    {
        assertEquals(FragmentType.FORWARD.ordinal(), 0);
        assertEquals(FragmentType.REVERSE.ordinal(), 1);
    }

}