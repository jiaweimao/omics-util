package omics.util.protein.ms;

import omics.util.protein.AminoAcid;
import omics.util.protein.PeptideFragment;
import org.junit.jupiter.api.Test;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Sep 2020, 11:10 PM
 */
class ImmoniumAnnotationTest
{
    @Test
    void symbol()
    {
        ImmoniumAnnotation annotation = new ImmoniumAnnotation(1, new PeptideFragment(FragmentType.MONOMER, AminoAcid.S), 0);
        System.out.println(annotation.getSymbol());
    }
}