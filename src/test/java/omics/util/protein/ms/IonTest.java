package omics.util.protein.ms;


import omics.util.chem.AtomicSymbol;
import omics.util.chem.Composition;
import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.Peak;
import omics.util.protein.AminoAcid;
import omics.util.protein.mod.PTM;
import omics.util.protein.mod.PTMFactory;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Jul 2019, 3:56 PM
 */
class IonTest
{
    private static final double eps = 0.000001;

    @Test
    void getMz()
    {
        Ion im = Ion.im;
        double mz = im.getMz(AminoAcid.S.getMolecularMass(), 1);
        System.out.println(mz);
    }

    double createImmonium(AminoAcid a)
    {
        return Ion.im.getMz(a.getMolecularMass(), 1);
    }


    @Test
    void testImmoniumMz()
    {
        double eps = 0.01;

        double a = createImmonium(AminoAcid.A);
        assertEquals(a, 44.05, eps);

        a = createImmonium(AminoAcid.G);
        assertEquals(a, 30.03, eps);

        a = createImmonium(AminoAcid.S);
        assertEquals(a, 60.05, eps);

        a = createImmonium(AminoAcid.P);
        assertEquals(a, 70.07, eps);

        a = createImmonium(AminoAcid.V);
        assertEquals(a, 72.08, eps);

        a = createImmonium(AminoAcid.T);
        assertEquals(a, 74.06, eps);

        a = createImmonium(AminoAcid.C);
        assertEquals(a, 76.02, eps);

        a = createImmonium(AminoAcid.I);
        assertEquals(a, 86.10, eps);

        a = createImmonium(AminoAcid.L);
        assertEquals(a, 86.10, eps);

        a = createImmonium(AminoAcid.N);
        assertEquals(a, 87.06, eps);

        a = createImmonium(AminoAcid.D);
        assertEquals(a, 88.04, eps);

        a = createImmonium(AminoAcid.Q);
        assertEquals(a, 101.07, eps);

        a = createImmonium(AminoAcid.K);
        assertEquals(a, 101.11, eps);

        a = createImmonium(AminoAcid.E);
        assertEquals(a, 102.06, eps);

        a = createImmonium(AminoAcid.M);
        assertEquals(a, 104.05, eps);

        a = createImmonium(AminoAcid.H);
        assertEquals(a, 110.07, eps);

        a = createImmonium(AminoAcid.F);
        assertEquals(a, 120.08, eps);

        a = createImmonium(AminoAcid.R);
        assertEquals(a, 129.11, eps);

        a = createImmonium(AminoAcid.Y);
        assertEquals(a, 136.08, eps);

        a = createImmonium(AminoAcid.W);
        assertEquals(a, 159.09, eps);
    }

    @Test
    void testToZ1()
    {
        double mz = 242.6416311077;
        double v = Ion.toZ1(mz, 2);

        assertEquals(v, mz * 2 - Ion.CHARGE_PARTICLE_MASS, eps);
    }

    @Test
    void calcNeutralMass()
    {
        final double molecularMass = Ion.calcNeutralMass(new Peak(242.6416311077, 10, 2));
        assertEquals(483.26871, molecularMass, eps);
    }

    @Test
    void testRoundTripPositive()
    {
        double mw = 483.26871;
        int charge = 2;

        double mz = Ion.calcMz(mw, charge);
        assertEquals(242.6416311077, mz, eps);
        assertEquals(mw, Ion.calcNeutralMass(mz, charge), eps);
    }


    @Test
    void testGetMz()
    {
        double deltaY = Ion.y.getDeltaMass();

        // With electron weight
        assertEquals(18.010564, deltaY, eps);

        assertEquals(60.012558, Ion.calcMz(100 + deltaY, 2), eps);
        assertEquals(51.007276, Ion.calcMz(100 + Ion.b.getDeltaMass(), 2), eps);
    }


    @Test
    void testCalculateMzIllegalCharge()
    {
        assertThrows(IllegalArgumentException.class, () -> Ion.calcMz(845.9, -12));
    }

    @Test
    void getCharges()
    {
        int[] charges = Ion.getFragmentCharges(4);
        assertArrayEquals(charges, new int[]{1, 2, 3});

        charges = Ion.getFragmentCharges(2);
        assertArrayEquals(charges, new int[]{1, 2});

        charges = Ion.getFragmentCharges(1);
        assertArrayEquals(charges, new int[]{1});
    }

    @Test
    void testgetPrecursorCharges()
    {
        int[] precursorCharges = Ion.getPrecursorCharges(3);
        assertArrayEquals(precursorCharges, new int[]{1, 2, 3});
    }


    @Test
    void getFragmentCharges()
    {
        int[] fragmentCharges = Ion.getMascotCharges(3);
        assertEquals("[1, 2]", Arrays.toString(fragmentCharges));

        fragmentCharges = Ion.getMascotCharges(1);
        assertEquals("[1]", Arrays.toString(fragmentCharges));
    }

    @Test
    void testComposition()
    {
        assertEquals(Ion.a_H2O.getComposition(), Ion.a.getComposition().subtract(Composition.H2O));
        assertEquals(Ion.a_NH3.getComposition(), Ion.a.getComposition().subtract(Composition.NH3));

        assertEquals(Ion.b_H2O.getComposition(), Ion.b.getComposition().subtract(Composition.H2O));
        assertEquals(Ion.b_NH3.getComposition(), Ion.b.getComposition().subtract(Composition.NH3));

        assertEquals(Ion.c_H2O.getComposition(), Ion.c.getComposition().subtract(Composition.H2O));
        assertEquals(Ion.c_NH3.getComposition(), Ion.c.getComposition().subtract(Composition.NH3));

        assertEquals(Ion.x_H2O.getComposition(), Ion.x.getComposition().subtract(Composition.H2O));
        assertEquals(Ion.x_NH3.getComposition(), Ion.x.getComposition().subtract(Composition.NH3));

        assertEquals(Ion.y_H2O.getComposition(), Ion.y.getComposition().subtract(Composition.H2O));
        assertEquals(Ion.y_NH3.getComposition(), Ion.y.getComposition().subtract(Composition.NH3));

        assertEquals(Ion.z_H2O.getComposition(), Ion.z.getComposition().subtract(Composition.H2O));
        assertEquals(Ion.z_NH3.getComposition(), Ion.z.getComposition().subtract(Composition.NH3));
    }

    @Test
    void valueOf()
    {
        Ion a = Ion.valueOf("a");
        System.out.println(a);
    }

    @Test
    void testX()
    {
        Composition composition = Composition.parseComposition("C8H13NO5");
        Composition sumComp = new Composition(Ion.y.getComposition(), composition);
        System.out.println(sumComp.getFormula());
        System.out.println(sumComp.getMolecularMass());
        System.out.println(composition.getMolecularMass() + Ion.y.getDeltaMass());
    }

    @Test
    void testImmonium()
    {
        PTMFactory factory = PTMFactory.getInstance();

        PTM carb = factory.ofName("Carbamidomethyl");
        assertNotNull(carb);
        double carbamidomethyl = carb.getMolecularMass();

        double eps = 0.0001;
        double ammoniaDelta = Ion.im.getDeltaMass() + PeriodicTable.PROTON_MASS;

        double a = AminoAcid.A.getMolecularMass() + ammoniaDelta;
        assertEquals(44.0495, a, eps);

        double c = AminoAcid.C.getMolecularMass() + ammoniaDelta + carbamidomethyl;
        assertEquals(133.0430, c, eps);

        double d = AminoAcid.D.getMolecularMass() + ammoniaDelta;
        assertEquals(88.0393, d, eps);

        double e = AminoAcid.E.getMolecularMass() + ammoniaDelta;
        assertEquals(102.0550, e, eps);

        double f = AminoAcid.F.getMolecularMass() + ammoniaDelta;
        assertEquals(120.0808, f, eps);

        double g = AminoAcid.G.getMolecularMass() + ammoniaDelta;
        assertEquals(30.0338, g, eps);

        double h = AminoAcid.H.getMolecularMass() + ammoniaDelta;
        assertEquals(110.0713, h, eps);

        double i = AminoAcid.I.getMolecularMass() + ammoniaDelta;
        assertEquals(86.0964, i, eps);

        double k = AminoAcid.K.getMolecularMass() + ammoniaDelta;
        assertEquals(101.1073, k, eps);

        double l = AminoAcid.L.getMolecularMass() + ammoniaDelta;
        assertEquals(86.0964, l, eps);

        double m = AminoAcid.M.getMolecularMass() + ammoniaDelta;
        assertEquals(104.0528, m, eps);

        double n = AminoAcid.N.getMolecularMass() + ammoniaDelta;
        assertEquals(87.0553, n, eps);

        double p = AminoAcid.P.getMolecularMass() + ammoniaDelta;
        assertEquals(70.0651, p, eps);

        double q = AminoAcid.Q.getMolecularMass() + ammoniaDelta;
        assertEquals(101.0709, q, eps);

        double r = AminoAcid.R.getMolecularMass() + ammoniaDelta;
        assertEquals(129.1135, r, eps);

        double s = AminoAcid.S.getMolecularMass() + ammoniaDelta;
        assertEquals(60.0444, s, eps);

        double t = AminoAcid.T.getMolecularMass() + ammoniaDelta;
        assertEquals(74.0600, t, eps);

        double v = AminoAcid.V.getMolecularMass() + ammoniaDelta;
        assertEquals(v, 72.0808, eps);

        double w = AminoAcid.W.getMolecularMass() + ammoniaDelta;
        assertEquals(159.0917, w, eps);

        PTM phos = factory.ofName("Phospho");
        assertNotNull(phos);

        double y = AminoAcid.Y.getMolecularMass() + ammoniaDelta + phos.getMolecularMass();
        assertEquals(y, 216.0420, eps);
    }

    @Test
    void testGetName()
    {
        Ion ionType = Ion.ya;
        assertEquals(ionType.getName(), "ya");
    }


    @Test
    void testGlycineBIon()
    {
        // Composition of singly charged glycine b ion
        Composition bGlycineComposition = new Composition(new Composition.Builder(AtomicSymbol.C, 2)
                .add(AtomicSymbol.H, 4).add(AtomicSymbol.N).add(AtomicSymbol.O).charge(1).build());

        assertEquals(bGlycineComposition.getMolecularMass(),
                Ion.calcMz(AminoAcid.G.getComposition().getMolecularMass() + Ion.b.getDeltaMass(),
                        1), eps);
        assertEquals(bGlycineComposition.getMolecularMass(),
                AminoAcid.G.getComposition().getMolecularMass() + Ion.b.getDeltaMass() +
                        PeriodicTable.PROTON_MASS, eps);
    }

    @Test
    void testIonTypeDelta()
    {
        double yDelta = PeriodicTable.H_MASS * 2 + PeriodicTable.O_MASS; // H2O
        double bDelta = 0;
        double aDelta = -PeriodicTable.C_MASS - PeriodicTable.O_MASS; // -CO
        double intactDelta = PeriodicTable.H_MASS * 2 + PeriodicTable.O_MASS; // H2O
        double immoniumDelta = -PeriodicTable.C_MASS - PeriodicTable.O_MASS; // - C - O

        assertEquals(yDelta, Ion.y.getDeltaMass(), eps);
        assertEquals(bDelta, Ion.b.getDeltaMass(), eps);
        assertEquals(aDelta, Ion.a.getDeltaMass(), eps);
        assertEquals(intactDelta, Ion.p.getDeltaMass(), eps);
        assertEquals(immoniumDelta, Ion.im.getDeltaMass(), eps);
    }

    @Test
    void testGetDeltaMass()
    {
        assertEquals(-27.99491463, Ion.a.getDeltaMass(), eps);
        assertEquals(0.0, Ion.b.getDeltaMass(), eps);
        assertEquals(17.0265491015, Ion.c.getDeltaMass(), eps);
        assertEquals(43.989829, Ion.x.getDeltaMass(), eps);
        assertEquals(18.0105646942, Ion.y.getDeltaMass(), eps);
        assertEquals(0.9840155927000005, Ion.z.getDeltaMass(), eps);
        assertEquals(-27.99491463, Ion.im.getDeltaMass(), eps);
        assertEquals(18.010564686, Ion.p.getDeltaMass(), eps);

        double[] peptides = getPrefixArray("PEPTIDE");

        printArray(getSuffixArray("PEPTIDE"), Ion.z.getDeltaMass() + PeriodicTable.PROTON_MASS);

        System.out.println(Composition.parseComposition("HPO3").getMolecularMass());
        System.out.println(AminoAcid.T.getMolecularMass() + Composition.parseComposition("HPO3").getMolecularMass());
        System.out.println(AminoAcid.S.getMolecularMass() + Composition.parseComposition("HPO3").getMolecularMass());
    }

    private void printArray(double[] array, double delta)
    {
        for (double v : array) {
            System.out.println(v + delta);
        }
    }

    private double[] getSuffixArray(String pep)
    {
        double[] mass = new double[pep.length()];
        mass[pep.length() - 1] = AminoAcid.getAminoAcid(pep.charAt(pep.length() - 1)).getMolecularMass();
        for (int i = pep.length() - 2; i >= 0; i--) {
            AminoAcid aa = AminoAcid.getAminoAcid(pep.charAt(i));
            mass[i] = mass[i + 1] + aa.getMolecularMass();
        }

        return mass;
    }

    private double[] getPrefixArray(String pep)
    {
        double[] mass = new double[pep.length()];
        mass[0] = AminoAcid.getAminoAcid(pep.charAt(0)).getMolecularMass();
        for (int i = 1; i < pep.length(); i++) {
            AminoAcid aa = AminoAcid.getAminoAcid(pep.charAt(i));
            mass[i] = mass[i - 1] + aa.getMolecularMass();
        }

        return mass;
    }

    @Test
    void testGetDeltaComposition()
    {
        assertEquals(Composition.parseComposition("C-1O-1"), Ion.a.getComposition());
        assertEquals(Composition.parseComposition(""), Ion.b.getComposition());
        assertEquals(Composition.parseComposition("NH3"), Ion.c.getComposition());
        assertEquals(Composition.parseComposition("CO2"), Ion.x.getComposition());
        assertEquals(Composition.parseComposition("H2O"), Ion.y.getComposition());
        assertEquals(Composition.parseComposition("N-1OH-1"), Ion.z.getComposition());
        assertEquals(Composition.parseComposition("C-1O-1"), Ion.im.getComposition());
        assertEquals(Composition.parseComposition("H2O"), Ion.p.getComposition());
    }
}