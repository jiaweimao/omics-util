package omics.util.protein.ms;

import omics.util.protein.AminoAcid;
import omics.util.protein.Peptide;
import omics.util.protein.PeptideFragment;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 16 Aug 2020, 10:02 AM
 */
class PeptideFragmentGeneratorTest
{

    @Test
    void generateForward()
    {
    }

    @Test
    void generateReverse()
    {
    }

    @Test
    void generateImmonium()
    {
    }

    @Test
    void generateInternal()
    {
        List<PeptideFragment> fragmentList = PeptideFragmentGenerator.generateInternal(Peptide.parse("SAMPLER"));
        assertEquals(fragmentList.size(), 10);
        assertEquals(fragmentList.get(0), new PeptideFragment(FragmentType.INTERNAL, AminoAcid.A, AminoAcid.M));
        assertEquals(fragmentList.get(1), new PeptideFragment(FragmentType.INTERNAL, AminoAcid.A, AminoAcid.M));
        assertEquals(fragmentList.get(2), new PeptideFragment(FragmentType.INTERNAL, AminoAcid.A, AminoAcid.M));
    }
}