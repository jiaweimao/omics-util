package omics.util.protein.ms;

import omics.util.protein.Peptide;
import omics.util.protein.mod.PTM;
import omics.util.protein.mod.PTMFactory;
import omics.util.utils.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

/**
 * Theoretical spectrum is compare with http://db.systemsbiology.net:8080/proteomicsToolkit/
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 Feb 2018, 10:21 AM
 */
public class PeptideFragmenterTest
{
    private static double delta;
    private static PeptideFragmenter fragmenter2;

    @BeforeAll
    static void setUp()
    {
        fragmenter2 = new PeptideFragmenter();
        delta = 1E-3;
    }

    @Test
    void testX()
    {
        Peptide peptide = Peptide.parse("PEPTIDE");
        PeptideSpectrum spectrum = fragmenter2.fragment(peptide, 1, Collections.singletonList(Pair.create(PeptideIon.z(1), 100.0)));
        System.out.println(spectrum);
    }

    @Test
    void testInternal()
    {
        Peptide sampler = Peptide.parse("SAMPLER");
        PeptideFragmenter fragmenter = new PeptideFragmenter();
        fragmenter.setPeptideIonList(Arrays.asList(PeptideIon.ya(1), PeptideIon.yb(1)));
        PeptideSpectrum spectrum = fragmenter.fragment(sampler, 1);
        System.out.println(spectrum);
    }

    /**
     * pd theoretical m/z values
     */
    @Test
    void testFragment_Phospho()
    {
        Peptide peptide = new Peptide.Builder("EMEHNTVCAAGTSPVGEIGEEK")
                .addModification(7, PTMFactory.getInstance().ofName("Carbamidomethyl"))
                .addModification(12, PTMFactory.getInstance().ofName("Phospho")).build();

        //<editor-fold desc="b+">
        test(peptide, Ion.b, null, 1, new double[]{
                130.04987, 261.09035, 390.13295, 527.19186, 641.23479, 742.28246, 841.35088, 1001.38153,
                1072.41864, 1143.45575, 1200.47722, 1301.5249, 1468.52326, 1565.57602, 1664.64443, 1721.6659, 1850.70849, 1963.79255,
                2020.81402, 2149.85661, 2278.8992
        });
        //</editor-fold>

        //<editor-fold desc="b++">
        test(peptide, Ion.b, null, 2, new double[]{
                65.52857, 131.04882, 195.57011, 264.09957, 321.12103, 371.64487, 421.17908, 501.1944, 536.71296,
                572.23152, 600.74225, 651.26609, 734.76527, 783.29165, 832.82586, 861.33659, 925.85788, 982.39992, 1010.91065, 1075.43194, 1139.95324
        });
        //</editor-fold>

        //<editor-fold desc="b+++">
        test(peptide, Ion.b, null, 3, new double[]{
                44.02147, 87.70164, 130.71583, 176.40214, 214.41645, 248.09901, 281.12181, 334.46536, 358.1444, 381.82344,
                400.83059, 434.51315, 490.17927, 522.53019, 555.553, 574.56015, 617.57435, 655.26904, 674.27619, 717.29039, 760.30459
        });
        //</editor-fold>

        //<editor-fold desc="y+">
        test(peptide, Ion.y, null, 1, new double[]{
                147.1128,
                276.1554,
                405.19799,
                462.21945,
                575.30352,
                704.34611,
                761.36757,
                860.43599,
                957.48875,
                1124.48711,
                1225.53479,
                1282.55625,
                1353.59337,
                1424.63048,
                1584.66113,
                1683.72954,
                1784.77722,
                1898.82015,
                2035.87906,
                2164.92165,
                2295.96214
        });
        //</editor-fold>

        //<editor-fold desc="b-H3PO4+">
        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                1370.54636,
                1467.59912,
                1566.66754,
                1623.689,
                1752.7316,
                1865.81566,
                1922.83712,
                2051.87972,
                2180.92231
        });
        //</editor-fold>

        //<editor-fold desc="b-H3PO4++">
        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4(), 2, new double[]{
                685.77682,
                734.3032,
                783.83741,
                812.34814,
                876.86944,
                933.41147,
                961.9222,
                1026.4435,
                1090.96479
        });
        //</editor-fold>

        //<editor-fold desc="y-H3PO4">
        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                1026.51022,
                1127.5579,
                1184.57936,
                1255.61647,
                1326.65359,
                1486.68423,
                1585.75265,
                1686.80033,
                1800.84325,
                1937.90217,
                2066.94476,
                2197.98524
        });
        //</editor-fold>

        //<editor-fold desc="b-H2O">
        test(peptide, Ion.b, PeptideNeutralLoss.H2O(), 1, new double[]{
                112.0393,
                243.07979,
                372.12238,
                509.18129,
                623.22422,
                724.2719,
                823.34031,
                983.37096,
                1054.40808,
                1125.44519,
                1182.46665,
                1283.51433,
                1450.51269,
                1547.56546,
                1646.63387,
                1703.65533,
                1832.69793,
                1945.78199,
                2002.80345,
                2131.84605,
                2260.88864
        });
        //</editor-fold>

        //<editor-fold desc="b-NH3">
        test(peptide, Ion.b, PeptideNeutralLoss.NH3(), 1, new double[]{
                624.20824,
                725.25592,
                824.32433,
                984.35498,
                1055.39209,
                1126.42921,
                1183.45067,
                1284.49835,
                1451.49671,
                1548.54947,
                1647.61788,
                1704.63935,
                1833.68194,
                1946.76601,
                2003.78747,
                2132.83006,
                2261.87266
        });
        //</editor-fold>

        //<editor-fold desc="y-H2O">
        test(peptide, Ion.y, PeptideNeutralLoss.H2O(), 1, new double[]{
                258.14483,
                387.18743,
                444.20889,
                557.29295,
                686.33555,
                743.35701,
                842.42542,
                939.47819,
                1106.47655,
                1207.52423,
                1264.54569,
                1335.5828,
                1406.61992,
                1566.65057,
                1665.71898,
                1766.76666,
                1880.80958,
                2017.8685,
                2146.91109,
                2277.95157
        });
        //</editor-fold>

        //<editor-fold desc="y-NH3">
        test(peptide, Ion.y, PeptideNeutralLoss.NH3(), 1, new double[]{
                130.08626,
                259.12885,
                388.17144,
                445.1929,
                558.27697,
                687.31956,
                744.34103,
                843.40944,
                940.4622,
                1107.46056,
                1208.50824,
                1265.5297,
                1336.56682,
                1407.60393,
                1567.63458,
                1666.70299,
                1767.75067,
                1881.7936,
                2018.85251,
                2147.89511,
                2278.93559
        });
        //</editor-fold>

        //<editor-fold desc="b-H2O-H3PO4">
        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                1352.5358,
                1449.58856,
                1548.65697,
                1605.67844,
                1734.72103,
                1847.80509,
                1904.82656,
                2033.86915,
                2162.91174
        });
        //</editor-fold>

        //<editor-fold desc="b-NH3-H3PO4">
        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                1353.51981,
                1450.57258,
                1549.64099,
                1606.66245,
                1735.70505,
                1848.78911,
                1905.81057,
                2034.85317,
                2163.89576
        });
        //</editor-fold>

        //<editor-fold desc="y-H2O-H3PO4">
        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                1008.49965,
                1109.54733,
                1166.56879,
                1237.60591,
                1308.64302,
                1468.67367,
                1567.74208,
                1668.78976,
                1782.83269,
                1919.8916,
                2048.93419,
                2179.97468
        });
        //</editor-fold>

        //<editor-fold desc="y-NH3-H3PO4">
        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                1009.48367,
                1110.53135,
                1167.55281,
                1238.58992,
                1309.62704,
                1469.65769,
                1568.7261,
                1669.77378,
                1783.81671,
                1920.87562,
                2049.91821,
                2180.95869
        });
        //</editor-fold>

        // precursor+
        test(peptide, Ion.p, null, 1, new double[]{2425.00473});

        // precursor++
        test(peptide, Ion.p, null, 2, new double[]{1213.006});

        // precursor+++
        test(peptide, Ion.p, null, 3, new double[]{809.00643});

        // precursor-NH3
        test(peptide, Ion.p, PeptideNeutralLoss.NH3(), 1, new double[]{2407.97818});

        test(peptide, Ion.p, PeptideNeutralLoss.H2O(), 1, new double[]{2406.99417});
        test(peptide, Ion.p, PeptideNeutralLoss.NH3_H2O(), 1, new double[]{2389.96762});
    }

    @Test
    void testFragment2()
    {
        Peptide peptide = new Peptide.Builder("FGESEEVEMEVESDEEDDKQEK")
                .addModification(12, PTMFactory.getInstance().ofName("Phospho"))
                .build();

        //<editor-fold desc="b">
        test(peptide, Ion.b, null, 1, new double[]{
                148.07569,
                205.09715,
                334.13975,
                421.17178,
                550.21437,
                679.25696,
                778.32538,
                907.36797,
                1038.40845,
                1167.45105,
                1266.51946,
                1395.56205,
                1562.56041,
                1677.58736,
                1806.62995,
                1935.67254,
                2050.69948,
                2165.72643,
                2293.82139,
                2421.87997,
                2550.92256
        });
        //</editor-fold>

        //<editor-fold desc="y">
        test(peptide, Ion.y, null, 1, new double[]{
                147.1128,
                276.1554,
                404.21397,
                532.30894,
                647.33588,
                762.36282,
                891.40542,
                1020.44801,
                1135.47495,
                1302.47331,
                1431.5159,
                1530.58432,
                1659.62691,
                1790.6674,
                1919.70999,
                2018.7784,
                2147.821,
                2276.86359,
                2363.89562,
                2492.93821,
                2549.95967
        });
        //</editor-fold>

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                1464.58352,
                1579.61046,
                1708.65305,
                1837.69565,
                1952.72259,
                2067.74953,
                2195.8445,
                2323.90307,
                2452.94567
        });
        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                1204.49642,
                1333.53901,
                1432.60742,
                1561.65002,
                1692.6905,
                1821.73309,
                1920.80151,
                2049.8441,
                2178.88669,
                2265.91872,
                2394.96132,
                2451.98278
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H2O(), 1, new double[]{
                316.12918,
                403.16121,
                532.2038,
                661.2464,
                760.31481,
                889.3574,
                1020.39789,
                1149.44048,
                1248.5089,
                1377.55149,
                1544.54985,
                1659.57679,
                1788.61938,
                1917.66198,
                2032.68892,
                2147.71586,
                2275.81083,
                2403.8694,
                2532.912
        });

        test(peptide, Ion.b, PeptideNeutralLoss.NH3(), 1, new double[]{
                2276.79484,
                2404.85342,
                2533.89601
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H2O(), 1, new double[]{
                258.14483,
                386.20341,
                514.29837,
                629.32532,
                744.35226,
                873.39485,
                1002.43744,
                1117.46439,
                1284.46275,
                1413.50534,
                1512.57375,
                1641.61635,
                1772.65683,
                1901.69942,
                2000.76784,
                2129.81043,
                2258.85302,
                2345.88505,
                2474.92765,
                2531.94911
        });

        test(peptide, Ion.y, PeptideNeutralLoss.NH3(), 1, new double[]{
                130.08626,
                259.12885,
                387.18743,
                515.28239,
                630.30933,
                745.33627,
                874.37887,
                1003.42146,
                1118.4484,
                1285.44676,
                1414.48936,
                1513.55777,
                1642.60036,
                1773.64085,
                1902.68344,
                2001.75185,
                2130.79445,
                2259.83704,
                2346.86907,
                2475.91166,
                2532.93313
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                1446.57295,
                1561.5999,
                1690.64249,
                1819.68508,
                1934.71202,
                2049.73897,
                2177.83393,
                2305.89251,
                2434.9351
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                2178.81795,
                2306.87652,
                2435.91912
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                1186.48585,
                1315.52845,
                1414.59686,
                1543.63945,
                1674.67994,
                1803.72253,
                1902.79094,
                2031.83354,
                2160.87613,
                2247.90816,
                2376.95075,
                2433.97221
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                1187.46987,
                1316.51246,
                1415.58087,
                1544.62347,
                1675.66395,
                1804.70655,
                1903.77496,
                2032.81755,
                2161.86015,
                2248.89217,
                2377.93477,
                2434.95623
        });

        test(peptide, Ion.p, null, 1, new double[]{2697.02809});
        test(peptide, Ion.p, PeptideNeutralLoss.NH3(), 1, new double[]{2680.00154});
        test(peptide, Ion.p, PeptideNeutralLoss.H2O(), 1, new double[]{2679.01752});
        test(peptide, Ion.p, PeptideNeutralLoss.NH3_H2O(), 1, new double[]{2661.99097});
    }

    @Test
    void testFragment3()
    {
        Peptide peptide = new Peptide.Builder("TLDSDEERPRPAPPDWSHMR")
                .addModification(3, PTMFactory.getInstance().ofName("Phospho"))
                .build();

        test(peptide, Ion.b, null, 1, new double[]{
                102.05496,
                215.13902,
                330.16596,
                497.16432,
                612.19126,
                741.23386,
                870.27645,
                1026.37756,
                1123.43033,
                1279.53144,
                1376.5842,
                1447.62131,
                1544.67408,
                1641.72684,
                1756.75378,
                1942.8331,
                2029.86513,
                2166.92404,
                2297.96452
        });

        test(peptide, Ion.y, null, 1, new double[]{
                175.11895,
                306.15944,
                443.21835,
                530.25038,
                716.32969,
                831.35663,
                928.4094,
                1025.46216,
                1096.49927,
                1193.55204,
                1349.65315,
                1446.70591,
                1602.80702,
                1731.84962,
                1860.89221,
                1975.91915,
                2142.91751,
                2257.94446,
                2371.02852
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                399.18743,
                514.21437,
                643.25696,
                772.29956,
                928.40067,
                1025.45343,
                1181.55454,
                1278.6073,
                1349.64442,
                1446.69718,
                1543.74995,
                1658.77689,
                1844.8562,
                1931.88823,
                2068.94714,
                2199.98763
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                2044.94062,
                2159.96756,
                2273.05162
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H2O(), 1, new double[]{
                84.04439,
                197.12845,
                312.1554,
                479.15376,
                594.1807,
                723.22329,
                852.26589,
                1008.367,
                1105.41976,
                1261.52087,
                1358.57364,
                1429.61075,
                1526.66351,
                1623.71628,
                1738.74322,
                1924.82253,
                2011.85456,
                2148.91347,
                2279.95396
        });

        test(peptide, Ion.b, PeptideNeutralLoss.NH3(), 1, new double[]{
                1009.35101,
                1106.40378,
                1262.50489,
                1359.55765,
                1430.59476,
                1527.64753,
                1624.70029,
                1739.72724,
                1925.80655,
                2012.83858,
                2149.89749,
                2280.93797
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H2O(), 1, new double[]{
                512.23981,
                698.31913,
                813.34607,
                910.39883,
                1007.4516,
                1078.48871,
                1175.54147,
                1331.64258,
                1428.69535,
                1584.79646,
                1713.83905,
                1842.88165,
                1957.90859,
                2124.90695,
                2239.93389,
                2353.01795
        });

        test(peptide, Ion.y, PeptideNeutralLoss.NH3(), 1, new double[]{
                158.0924,
                289.13289,
                426.1918,
                513.22383,
                699.30314,
                814.33008,
                911.38285,
                1008.43561,
                1079.47273,
                1176.52549,
                1332.6266,
                1429.67936,
                1585.78047,
                1714.82307,
                1843.86566,
                1958.8926,
                2125.89096,
                2240.91791,
                2354.00197
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                381.17686,
                496.2038,
                625.2464,
                754.28899,
                910.3901,
                1007.44287,
                1163.54398,
                1260.59674,
                1331.63385,
                1428.68662,
                1525.73938,
                1640.76632,
                1826.84564,
                1913.87767,
                2050.93658,
                2181.97706
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                911.37412,
                1008.42688,
                1164.52799,
                1261.58076,
                1332.61787,
                1429.67063,
                1526.7234,
                1641.75034,
                1827.82965,
                1914.86168,
                2051.92059,
                2182.96108
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                2027.91407,
                2142.94101,
                2256.02507
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                2026.93005,
                2141.957,
                2255.04106
        });

        test(peptide, Ion.p, null, 1, new double[]{2472.0762});
        test(peptide, Ion.p, PeptideNeutralLoss.NH3(), 1, new double[]{2455.04965});
        test(peptide, Ion.p, PeptideNeutralLoss.H2O(), 1, new double[]{2454.06563});
        test(peptide, Ion.p, PeptideNeutralLoss.NH3_H2O(), 1, new double[]{2437.03908});
    }


    @Test
    public void testFragment4()
    {
        Peptide peptide = new Peptide.Builder("EGMNPSYDEYADSDEDQHDAYLER")
                .addModification(12, PTMFactory.getInstance().ofName("Phospho"))
                .build();

        test(peptide, Ion.b, null, 1, new double[]{
                130.04987,
                187.07133,
                318.11182,
                432.15475,
                529.20751,
                616.23954,
                779.30287,
                894.32981,
                1023.3724,
                1186.43573,
                1257.47284,
                1372.49979,
                1539.49815,
                1654.52509,
                1783.56768,
                1898.59463,
                2026.6532,
                2163.71211,
                2278.73906,
                2349.77617,
                2512.8395,
                2625.92356,
                2754.96616
        });

        test(peptide, Ion.y, null, 1, new double[]{
                175.11895,
                304.16155,
                417.24561,
                580.30894,
                651.34605,
                766.37299,
                903.43191,
                1031.49048,
                1146.51743,
                1275.56002,
                1390.58696,
                1557.58532,
                1672.61226,
                1743.64938,
                1906.71271,
                2035.7553,
                2150.78224,
                2313.84557,
                2400.8776,
                2497.93036,
                2611.97329,
                2743.01378,
                2800.03524
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                1441.52125,
                1556.54819,
                1685.59079,
                1800.61773,
                1928.67631,
                2065.73522,
                2180.76216,
                2251.79928,
                2414.8626,
                2527.94667,
                2656.98926
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4(), 1, new double[]{
                1459.60843,
                1574.63537,
                1645.67248,
                1808.73581,
                1937.77841,
                2052.80535,
                2215.86868,
                2302.9007,
                2399.95347,
                2513.9964,
                2645.03688,
                2702.05834
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H2O(), 1, new double[]{
                112.0393,
                169.06077,
                300.10125,
                414.14418,
                511.19694,
                598.22897,
                761.2923,
                876.31924,
                1005.36184,
                1168.42517,
                1239.46228,
                1354.48922,
                1521.48758,
                1636.51452,
                1765.55712,
                1880.58406,
                2008.64264,
                2145.70155,
                2260.72849,
                2331.76561,
                2494.82894,
                2607.913,
                2736.95559
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H2O(), 1, new double[]{
                286.15098,
                399.23504,
                562.29837,
                633.33549,
                748.36243,
                885.42134,
                1013.47992,
                1128.50686,
                1257.54945,
                1372.5764,
                1539.57476,
                1654.6017,
                1725.63881,
                1888.70214,
                2017.74474,
                2132.77168,
                2295.83501,
                2382.86704,
                2479.9198,
                2593.96273,
                2725.00321,
                2782.02467
        });

        test(peptide, Ion.b, PeptideNeutralLoss.NH3(), 1, new double[]{
                415.1282,
                512.18096,
                599.21299,
                762.27632,
                877.30326,
                1006.34585,
                1169.40918,
                1240.4463,
                1355.47324,
                1522.4716,
                1637.49854,
                1766.54113,
                1881.56808,
                2009.62665,
                2146.68557,
                2261.71251,
                2332.74962,
                2495.81295,
                2608.89701,
                2737.93961
        });

        test(peptide, Ion.y, PeptideNeutralLoss.NH3(), 1, new double[]{
                158.0924,
                287.135,
                400.21906,
                563.28239,
                634.3195,
                749.34645,
                886.40536,
                1014.46393,
                1129.49088,
                1258.53347,
                1373.56041,
                1540.55877,
                1655.58572,
                1726.62283,
                1889.68616,
                2018.72875,
                2133.75569,
                2296.81902,
                2383.85105,
                2480.90381,
                2594.94674,
                2725.98723,
                2783.00869
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                1423.51069,
                1538.53763,
                1667.58022,
                1782.60717,
                1910.66574,
                2047.72465,
                2162.7516,
                2233.78871,
                2396.85204,
                2509.9361,
                2638.9787,
        });
        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_H2O(), 1, new double[]{
                1441.59786,
                1556.62481,
                1627.66192,
                1790.72525,
                1919.76784,
                2034.79478,
                2197.85811,
                2284.89014,
                2381.9429,
                2495.98583,
                2627.02632,
                2684.04778
        });

        test(peptide, Ion.b, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                1424.4947,
                1539.52165,
                1668.56424,
                1783.59118,
                1911.64976,
                2048.70867,
                2163.73561,
                2234.77273,
                2397.83606,
                2510.92012,
                2639.96271
        });

        test(peptide, Ion.y, PeptideNeutralLoss.H3PO4_NH3(), 1, new double[]{
                1442.58188,
                1557.60882,
                1628.64593,
                1791.70926,
                1920.75186,
                2035.7788,
                2198.84213,
                2285.87416,
                2382.92692,
                2496.96985,
                2628.01033,
                2685.0318
        });

        test(peptide, Ion.p, null, 1, new double[]{2929.07783});
        test(peptide, Ion.p, null, 2, new double[]{1465.04255});
        test(peptide, Ion.p, PeptideNeutralLoss.H2O(), 1, new double[]{2911.06727});
        test(peptide, Ion.p, PeptideNeutralLoss.NH3(), 1, new double[]{2912.05128});
    }

    @Test
    void testFragment()
    {
        PTM phospho = PTMFactory.getInstance().ofName("Phospho");
        Peptide peptide = new Peptide.Builder("TPPTPSLPWISSLVVEGFVPSSPPSLSLSASSSSLPWVFF")
                .addModification(5, phospho)
                .addModification(10, phospho)
                .build();

        List<Pair<PeptideIon, Double>> ionTypes = new ArrayList<>();
//        ionTypes.add(new PeptidePeptideFragmentIon(PeptideFragmentIon.b, ArrayUtils.array(1), 100));
        ionTypes.add(Pair.create(PeptideIon.y(1), 100.));

        PeptideSpectrum spectrum = fragmenter2.fragment(peptide, 2, ionTypes);
//        printSpectrum(spectrum);
    }

    private void test(Peptide peptide, Ion ionType, PeptideNeutralLoss neutralLosses, int charge, double[] values)
    {
        PeptideSpectrum spectrum = fragmenter2.fragment(peptide, 2, Collections.singletonList(Pair.create(new PeptideIon(ionType, neutralLosses, charge), 50.0)));
        assertArrayEquals(spectrum.getXs(), values, delta);
    }

    private void printSpectrum(PeptideSpectrum spectrum)
    {
        for (int i = 0; i < spectrum.size(); i++) {
            System.out.println(spectrum.getX(i) + "," + spectrum.getAnnotationsText(i));
        }
    }
}