package omics.util.protein.ms;

import omics.util.chem.Composition;
import omics.util.ms.Dissociation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.protein.mod.NeutralLoss;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Jul 2019, 7:45 PM
 */
class PeptideIonTest
{
    @Test
    void getPeptideIons()
    {
        List<Ion> ionList = new ArrayList<>();
        ionList.add(Ion.a);
        ionList.add(Ion.b);
        ionList.add(Ion.x);
        ionList.add(Ion.y);

        List<PeptideNeutralLoss> neutralLossList = new ArrayList<>();
        neutralLossList.add(PeptideNeutralLoss.NH3());
        neutralLossList.add(PeptideNeutralLoss.H2O());
        neutralLossList.add(PeptideNeutralLoss.of(NeutralLoss.of("-2H2O", Composition.parseComposition("H4O2"))));
        neutralLossList.add(PeptideNeutralLoss.of(NeutralLoss.of("-H2O-NH3", Composition.parseComposition("H5O1N1"))));
        neutralLossList.add(PeptideNeutralLoss.of(NeutralLoss.of("-2NH3", Composition.parseComposition("N2H6"))));
        neutralLossList.add(PeptideNeutralLoss.of(NeutralLoss.of("[C13]", Composition.parseComposition("C[13]-1C"))));
        neutralLossList.add(PeptideNeutralLoss.of(NeutralLoss.of("[C14]", Composition.parseComposition("C[14]-1C"))));
        neutralLossList.add(PeptideNeutralLoss.of(NeutralLoss.of("-H", Composition.parseComposition("H"))));

        List<PeptideIon> peptideIons = PeptideIon.getPeptideIons(ionList, neutralLossList, 1, 1, Tolerance.abs(0.01));
        for (PeptideIon peptideIon : peptideIons) {
            System.out.println(peptideIon.getName()+"\t"+peptideIon.getDeltaMz());
        }

    }

    @Test
    void testgetPeptideIons()
    {
        ArrayList<PeptideIon> peptideIons = PeptideIon.getPeptideIons(Dissociation.HCD, 1, true, false);
        for (PeptideIon peptideIon : peptideIons) {
            System.out.println(peptideIon.getName() + "\t" + peptideIon.getDeltaMz());
        }
    }

    @Test
    void testgetAllPeptideIons()
    {
        ArrayList<PeptideIon> allPeptideIons = PeptideIon.getPeptideIons(1, false, 0.1, false);
        for (PeptideIon allPeptideIon : allPeptideIons) {
            System.out.println(allPeptideIon.getName() + "\t" + (allPeptideIon.getDeltaMz()));
        }
    }


    @Test
    void testNeutralLoss()
    {
        PeptideIon ion = new PeptideIon(Ion.x, PeptideNeutralLoss.of(NeutralLoss.of("-2H2O", Composition.parseComposition("H-4O-2"))), 1);
        System.out.println(ion.getDeltaMz());
    }

    @Test
    void testName()
    {
        assertEquals(PeptideIon.b(1).getName(), "b");
        assertEquals(PeptideIon.b_H2O(1).getName(), "b-H2O");
        assertEquals(PeptideIon.b_NH3(1).getName(), "b-NH3");

        System.out.println(PeptideIon.b(2).getName());

//        List<Pair<PeptideIon, Double>> ionTypes = new ArrayList<>();
//        ionTypes.add(Pair.create(PeptideIon.b(1), 100.0));
//        ionTypes.add(Pair.create(PeptideIon.b(2), 100.0));
//        ionTypes.add(Pair.create(PeptideIon.y(1), 100.0));
//        ionTypes.add(Pair.create(PeptideIon.y(2), 100.0));
////        ionTypes.add(Pair.create(PeptideIon.y_H2O(1), 100.0));
////        ionTypes.add(Pair.create(PeptideIon.y_H2O(2), 100.0));
////        ionTypes.add(Pair.create(PeptideIon.y_NH3(1), 100.0));
////        ionTypes.add(Pair.create(PeptideIon.y_NH3(2), 100.0));
//        ionTypes.add(Pair.create(PeptideIon.p(1), 50.0));
//        ionTypes.add(Pair.create(PeptideIon.p(2), 50.0));
//        ionTypes.add(Pair.create(PeptideIon.p(3), 50.0));
//        ionTypes.add(Pair.create(PeptideIon.p(4), 50.0));
    }
}