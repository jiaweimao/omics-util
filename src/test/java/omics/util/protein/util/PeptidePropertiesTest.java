package omics.util.protein.util;


import omics.util.OmicsException;
import omics.util.io.csv.CsvWriter;
import omics.util.protein.Peptide;
import omics.util.protein.PeptideProperties;
import omics.util.protein.database.Protein;
import omics.util.protein.database.ProteinDB;
import omics.util.utils.NumberFormatFactory;

import java.io.IOException;
import java.text.NumberFormat;

/**
 * @author JiaweiMao 2017-04-20
 * @since 1.0-SNAPSHOT
 */
public class PeptidePropertiesTest
{
    public static void main(String[] args) throws IOException, OmicsException
    {
        NumberFormat format = NumberFormatFactory.valueOf(4);
        NumberFormat massFormat = NumberFormatFactory.valueOf(2);

        CsvWriter writer = new CsvWriter(args[1]);
        writer.writeRecord(new String[]{"acc", "mass", "gravy"});

        ProteinDB protein = ProteinDB.read(args[0]);
        for (Protein en : protein) {

            String seq = Peptide.clearSequence(en.getSequence());

            Peptide peptide = Peptide.parse(seq);

            double gravy = PeptideProperties.calcGravy(en.getSequence());
            writer.write(en.getAccession());
            writer.write(massFormat.format(peptide.getMolecularMass()));
            writer.write(format.format(gravy));
            writer.endRecord();
        }

        writer.close();
        protein.clear();
    }


//    /**
//     * compare to http://www.bioinformatics.org/sms2/protein_gravy.html
//     */
//    @Test
//    void calcGravy()
//    {
//
//        String seq =
//                "MQKSPLEKASFISKLFFSWTTPILRKGYRHHLELSDIYQAPSADSADHLSEKLEREWDREQASKKNPQLIHALRRCFFWRFLFYGILLYLGEVTKAVQPVLLGRIIASYDPENKVERSIAIYLGIGLCLLFIVRTLLLHPAIFGLHRIGMQMRTAMFSLIYKKTLKLSSRVLDKISIGQLVSLLSNNLNKFDEGLALAHFIWIAPLQVTLLMGLLWDLLQFSAFCGLGLLIILVIFQAILGKMMVKYRDQRAAKINERLVIT";
//        double gravy = PeptideProperties.gravy(seq);
//        assertEquals(gravy, 0.308, 0.001);
//    }

}