package omics.util.string;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Jun 2019, 11:37 AM
 */
class AlphabetTest
{
    private Alphabet alphabet;

    @BeforeAll
    void setUp()
    {
        alphabet = Alphabet.PROTEIN;
    }

    @Test
    void testContains()
    {
        assertTrue(alphabet.contains('A'));
    }

    @Test
    void testGetRadix()
    {
        assertEquals(alphabet.getRadix(), 20);
        assertEquals(Alphabet.DNA.getRadix(), 4);
        assertEquals(Alphabet.UPPERCASE_26.getRadix(), 26);
    }

    @Test
    void testToIndex()
    {
        int id = alphabet.toIndex('A');
        assertEquals(id, 0);
        assertEquals(alphabet.toIndex('Y'), 19);
    }

    @Test
    void testGetAlphabet()
    {
        char[] alphabet = this.alphabet.getAlphabet();
        assertEquals(new String(alphabet), "ACDEFGHIKLMNPQRSTVWY");
    }

    @Test
    void testcountChars()
    {
        String fasta = "";
    }

    @Test
    void testProtein()
    {
        Alphabet alphabet = Alphabet.PROTEIN;
        assertEquals(alphabet.toIndex('_'), 1);
        assertEquals(alphabet.toIndex('?'), 0);

        assertEquals(alphabet.toChar(1), '_');
        assertEquals(alphabet.toChar(0), '?');

        assertEquals(alphabet.toIndex('!'), 0);

        byte[] bytes = alphabet.toByteIndices("_GALRTKKURK");
        String s = alphabet.toChars(bytes);
        assertEquals(s, "_GALRTKK?RK");
    }
}