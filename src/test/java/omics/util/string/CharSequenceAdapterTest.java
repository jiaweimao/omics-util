package omics.util.string;

import org.junit.jupiter.api.Test;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Jun 2019, 4:15 PM
 */
public class CharSequenceAdapterTest
{
    @Test
    public void testBuildSuffixArray()
    {
        CharSequenceAdapter adapter = new CharSequenceAdapter(new DivSufSort());
        int[] ints = adapter.buildSuffixArray("MMKSFFLVVTILALTLPFLGAQEQNQEQPIRCEKDERFFSDKIAKYIPIQYVLSRYPSYG" +
                "LNYYQQKPVALINNQFLPYPYYAKPAAVRSPAQILQWQVLSNTVPAKSCQAQPTTMARHP" +
                "HPHLSFMAIPPKKNQDKTEIPTINTIASGEPTSTPTTEAVESTVATLEDSPEVIESPPEI" +
                "NTVQVTSTAV");

    }
}