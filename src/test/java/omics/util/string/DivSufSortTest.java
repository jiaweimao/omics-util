package omics.util.string;

import omics.util.utils.IntPair;
import org.junit.jupiter.api.BeforeAll;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Jun 2019, 3:19 PM
 */
public class DivSufSortTest extends SuffixArrayBuilderTestBase
{
    private final int alphabetSize = 256;

    @BeforeAll
    public void setupForConstraints()
    {
        smallAlphabet = new IntPair(1, 10);
        largeAlphabet = new IntPair(1, alphabetSize - 1);
    }

    @Override
    protected ISuffixArrayBuilder getInstance()
    {
        return new DivSufSort(alphabetSize);
    }
}