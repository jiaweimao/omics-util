package omics.util.string;

import org.junit.jupiter.api.Test;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 26 Jun 2019, 10:20 AM
 */
public class SAISTest
{
    @Test
    public void testBuildSuffixArray()
    {
        String s = "MMKSFFLVVTILALTLPFLGAQEQNQEQPIRCEKD";
        int[] sa = new int[s.length()];
        SAIS.suffixsort(s, sa, s.length());
        for (int index : sa) {
            System.out.println(index + "=" + s.substring(index));
        }
//        System.out.println(Arrays.toString(sa));
    }
}