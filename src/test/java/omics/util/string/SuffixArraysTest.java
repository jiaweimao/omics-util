package omics.util.string;

import omics.util.utils.IntPair;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Jun 2019, 4:05 PM
 */
public class SuffixArraysTest
{
    @Test
    public void test()
    {
        String seq = "MKWVTFISLLLLFSSAYSRGVFRRDTHKSEIAHRFKDLGEEHFKGLVLIAFSQYLQQCPF";
        Alphabet alphabet = Alphabet.PROTEIN;
        byte[] bytes = alphabet.toByteIndices(seq);

    }

    /**
     * Verify that LCP array is indeed valid and correct with respect to the input.
     */
    @Test
    public void verifyLCP()
    {
        final Random rnd = new Random(0x11223344);
        final int size = 1000;
        final IntPair alphabet = new IntPair(1, 5);
        final int[] input = SuffixArrayBuilderTestBase.generateRandom(rnd, size, alphabet);

        final SuffixData data = SuffixArrays.createWithLCP(input, 0, input.length);
        final int[] lcp = data.getLCP();
        final int[] sa = data.getSuffixArray();

        assertEquals(-1, lcp[0]);
        for (int i = 1; i < lcp.length; i++) {
            assertEquals(lcp[i], prefixLength(input, sa[i], sa[i - 1]));
        }
    }

    private int prefixLength(int[] input, int i, int j)
    {
        int prefix = 0;
        while (i < input.length && j < input.length && input[i] == input[j]) {
            prefix++;
            i++;
            j++;
        }
        return prefix;
    }
}