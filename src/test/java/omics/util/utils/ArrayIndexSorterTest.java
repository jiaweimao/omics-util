package omics.util.utils;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ArrayIndexSorterTest
{
    @Test
    void sort()
    {
        Double[] values = new Double[]{2.0, 3.0, 1.0, 4.0, 6.1, 7.2, 4.6};

        Integer[] indexes = ArrayIndexSorter.sort(new DoubleComparator(values));
        assertEquals(indexes, new Integer[]{5, 4, 6, 3, 1, 0, 2});
    }

    static class DoubleComparator extends ArrayIndexComparator<Double>
    {
        public DoubleComparator(Double... array)
        {
            super(array);
        }

        @Override
        public int compare(Integer index1, Integer index2)
        {
            return array[index2].compareTo(array[index1]);
        }
    }

}