package omics.util.utils;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 Nov 2018, 4:47 PM
 */
public class ArrayUtilsTest
{

    @Test
    public void testIndexOf()
    {
        double[] array = new double[]{2.1, 3.4, 6.4, 7., 10};

        // for lower empty case
        int[] indexes = ArrayUtils.indexOf(array, 0, 1);
        assertEquals(indexes, new int[0]);

        indexes = ArrayUtils.indexOf(array, 0, 3);
        assertEquals(indexes, new int[]{0});

        indexes = ArrayUtils.indexOf(array, 3, 8);
        assertEquals(indexes, new int[]{1, 2, 3});

        // for edge case, edge is included
        indexes = ArrayUtils.indexOf(array, 10, 13);
        assertEquals(indexes, new int[]{4});

        // upper empty case
        indexes = ArrayUtils.indexOf(array, 11, 12);
        assertEquals(indexes, new int[0]);
    }

    @Test
    public void testSearch()
    {
        double[] array = new double[]{1, 2, 3};

        int key = Arrays.binarySearch(array, 1);
        if (key < 0)
            key = -key - 1;
        System.out.println(key);
    }
}