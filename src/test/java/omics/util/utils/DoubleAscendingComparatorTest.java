package omics.util.utils;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.impl.DoublePeakList;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 26 Jun 2018, 4:56 PM
 */
class DoubleAscendingComparatorTest
{
    @Test
    void testCompare()
    {
        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(1, 2.0); // 1
        peakList.add(3, 1.0); // 0
        peakList.add(4, 3.0); // 3
        peakList.add(5, 5.0); // 4
        peakList.add(6, 2.0); // 2
        peakList.add(8, 6.0); // 5

        double[] intensityValues = peakList.getYs();

        DoubleAscendingComparator comparator = new DoubleAscendingComparator();
        comparator.setArray(intensityValues);

        Integer[] indexes = ArrayIndexSorter.sort(comparator);
        assertEquals(indexes, new Integer[]{1, 0, 4, 2, 3, 5});
    }

    @Test
    void testSame()
    {
        List<Double> values = new ArrayList<>();
        values.add(1.);
        values.add(1.);
        values.add(1.);
        values.add(1.);

        DoubleAscendingComparator comparator = new DoubleAscendingComparator(values.toArray(new Double[0]));
        Integer[] sortIndexes = ArrayIndexSorter.sort(comparator);
        assertEquals(sortIndexes, new Integer[]{0, 1, 2, 3});

        comparator.setArray(new Double[]{1.0, 2.0, 2.0, 3.0});
        sortIndexes = ArrayIndexSorter.sort(comparator);
        assertEquals(sortIndexes, new Integer[]{0, 1, 2, 3});
    }
}