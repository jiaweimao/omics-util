package omics.util.utils;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 01 Jun 2018, 8:59 PM
 */
class DoubleDescendingComparatorTest
{
    @Test
    void testSame()
    {
        List<Double> values = new ArrayList<>();
        values.add(1.);
        values.add(1.);
        values.add(1.);
        values.add(1.);

        DoubleDescendingComparator comparator = new DoubleDescendingComparator(values.toArray(new Double[0]));
        Integer[] sortIndexes = ArrayIndexSorter.sort(comparator);
        assertEquals(sortIndexes, new Integer[]{0, 1, 2, 3});

        comparator.setArray(new Double[]{1.0, 2.0, 2.0, 3.0});
        sortIndexes = ArrayIndexSorter.sort(comparator);
        assertEquals(sortIndexes, new Integer[]{3, 1, 2, 0});
    }

    @Test
    void testCompare()
    {
        List<Double> array = new ArrayList<>();
        array.add(1.);
        array.add(2.);
        array.add(3.);
        array.add(6.);
        array.add(4.);
        array.add(5.);

        DoubleDescendingComparator comparator = new DoubleDescendingComparator(array.toArray(new Double[0]));
        Integer[] index = ArrayIndexSorter.sort(comparator);
        assertEquals(index, new Integer[]{3, 5, 4, 2, 1, 0});

        double[] values = new double[array.size()];
        for (int i = 0; i < array.size(); i++) {
            values[i] = array.get(index[i]);
        }

        assertArrayEquals(values, new double[]{6., 5., 4., 3., 2., 1.}, 0.01);
    }
}