package omics.util.utils;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 23 Jan 2018, 3:09 PM
 */
class IntArrayTest
{
    @Test
    void testIntArrayList()
    {
        IntList list = new IntArrayList();
        int[] value = list.toIntArray();
        assertNotNull(value);
    }

}
