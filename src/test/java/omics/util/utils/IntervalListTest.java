package omics.util.utils;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class IntervalListTest
{
    @Test
    void testPerformance()
    {
        IntervalList il = new IntervalList();

    }

    @Test
    void addInterval()
    {
        IntervalList il = new IntervalList();

        il.addInterval(400.0, 800);
        il.addInterval(750.0, 750.0);
        il.addInterval(900.0, 1000.0);
        il.addInterval(1168.39, 1210.209);
        il.addInterval(1168.39, 1210.209);
        il.addInterval(1188.39, 1210.209);
        il.addInterval(950.0, 750.0);

        assertTrue(il.contains(500.0));
        assertTrue(il.contains(800.0));
        assertTrue(il.contains(820.0));
        assertTrue(il.contains(855.0));
        assertTrue(il.contains(750.0));
        assertTrue(il.contains(400.00001));
        assertTrue(il.contains(1179.5));

        assertFalse(il.contains(399.99999));
        assertFalse(il.contains(1168.389));
        assertFalse(il.contains(10000.0));
        assertFalse(il.contains(-100.0));
    }

    @Test
    void contains()
    {
        IntervalList il = new IntervalList();

        il.addInterval(500.0, 800);
        il.addInterval(-100.0, 100000.0);

        assertTrue(il.contains(500.0));
        assertTrue(il.contains(820.0));
        assertTrue(il.contains(10900.00001));
        assertTrue(il.contains(1179.5));
        assertFalse(il.contains(-10000.0));
    }

    @Test
    void randomInterval()
    {
        IntervalList il = new IntervalList();

        for (int i = 0; i < 1000; i++) {
            double r1 = Math.random() * 2000;
            double r2 = Math.random() * 2000;
            il.addInterval(r1, r2);
        }

        il.contains(500.0);
        il.contains(820.0);
        il.contains(10900.00001);
        il.contains(1179.5);
        il.contains(-10000.0);
    }

    @Test
    void testSmall()
    {
        IntervalList il = new IntervalList();
        il.addInterval(0.2, 0.4);
        System.out.println(il.contains(0.1));
        System.out.println(il.contains(0.2));
        System.out.println(il.contains(0.3));
        System.out.println(il.contains(0.4));
        System.out.println(il.contains(0.5));
        System.out.println(il.contains(0.6));
    }

}