package omics.util.utils;


import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao 2017.03.20
 * @since 1.0-SNAPSHOT
 */
class NumberFormatFactoryTest
{

    @Test
    void testValueOfSN()
    {
        NumberFormat format = NumberFormatFactory.valueOfSN(2);
        assertEquals(format.format(0.04151515), "4.15E-2");
        assertEquals(format.format(0.04155515), "4.16E-2");
        assertEquals(format.format(9852522.554666), "9.85E6");
    }

    @Test
    void testShort()
    {
        DecimalFormat format = NumberFormatFactory.valueOf(1, true, 2, true, true);
        System.out.println(format.format(1.23));
        System.out.println(format.format(12.3456));
        System.out.println(format.format(0.123));
        System.out.println(format.format(0.1));

        DecimalFormat decimalFormat = NumberFormatFactory.valueOf("##0.###");
        System.out.println(decimalFormat.format(12.3));
        System.out.println(decimalFormat.format(0.123));
        System.out.println(decimalFormat.format(0.1234));
        System.out.println(decimalFormat.format(123456.1234));
    }

    @Test
    void testNewLocaleInstance()
    {
        NumberFormat df = DecimalFormat.getInstance(Locale.FRANCE);

        assertEquals("1,329", df.format(1.328984366538442783));
    }

    @Test
    void testNewLocaleInstanceWithPattern()
    {
        DecimalFormatSymbols frSymbols = new DecimalFormatSymbols(Locale.FRANCE);

        DecimalFormat df = new DecimalFormat("0.0000", frSymbols);

        assertEquals("1,3290", df.format(1.328984366538442783));
    }

    @Test
    void testDecimalFormatFactory()
    {
        NumberFormat df = NumberFormatFactory.getInstance();

        assertEquals("1.33", df.format(1.328984366538442783));
    }

    @Test
    void testDecimalFormatFactoryWithPrecision()
    {
        NumberFormat df = NumberFormatFactory.valueOf(4);

        assertEquals("1.3290", df.format(1.328984366538442783));
    }

    @Test
    void testDecimalFractionalPart()
    {
        NumberFormat df = NumberFormatFactory.valueOf(3);
        assertEquals("0.034", df.format(0.0342345678));
    }

    @Test
    void testDecimalIntegerPart()
    {
        NumberFormat df = NumberFormatFactory.valueOf(4, 0);

        assertEquals("1329", df.format(1328.984366538442783));
    }

    @Test
    void testDecimalIntegerAndFractionalPart()
    {
        NumberFormat df = NumberFormatFactory.valueOf(4, 4);

        assertEquals("1328.9844", df.format(1328.984366538442783));
    }

    @Test
    void testDecimal()
    {
        NumberFormat dec = NumberFormatFactory.valueOf(2);
        assertEquals("0.00", dec.format(0.001));
    }

    @Test
    void testInteger()
    {
        NumberFormat nf = NumberFormatFactory.INT_PRECISION;

        assertEquals("13", nf.format(12.984366538442783));
    }

    @Test
    void testInteger2()
    {
        NumberFormat nf = NumberFormatFactory.valueOf(1, 0);

        assertEquals("13", nf.format(12.984366538442783));
    }

    @Test
    void testInteger3()
    {
        NumberFormat nf = NumberFormatFactory.valueOf(4, 0);

        assertEquals("0013", nf.format(12.984366538442783));
    }

    @Test
    void testInteger4()
    {
        NumberFormat nf = NumberFormatFactory.INT_PRECISION;

        assertEquals("12984367", nf.format(12984366.538442783));
    }
}