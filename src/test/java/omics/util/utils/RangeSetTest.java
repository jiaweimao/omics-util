package omics.util.utils;

import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;
import org.checkerframework.dataflow.qual.TerminatesExecution;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RangeSetTest
{
    @Test
    void test()
    {
        int[] counts = new int[]{1, 3, 5, 7, 10, 50, 100, 1000, 10000};
        for (int count : counts) {
            System.out.println(count);
            testContains(count);
            System.out.println();
        }

    }

    //    @ParameterizedTest
//    @ValueSource(ints = {1, 10, 50, 100, 1000, 10000})
    void testContains(int arg)
    {
        double[] starts = new double[arg];
        double[] ends = new double[arg];
        double[] ins = new double[arg];
        double[] outs = new double[arg];
        Random random = new Random(1L);
        for (int i = 0; i < arg; i++) {
            starts[i] = i + 0.1;
            ends[i] = i + .9;

            double v = random.nextDouble();
            ins[i] = i + 0.1 + 0.8 * v;
            outs[i] = i + 0.91 + v * 0.1;
        }

//        System.out.println(Arrays.toString(starts));
//        System.out.println(Arrays.toString(ends));
//        System.out.println(Arrays.toString(ins));
//        System.out.println(Arrays.toString(outs));

        long s1 = System.nanoTime();
        RangeSet<Double> set = TreeRangeSet.create();
        for (int i = 0; i < arg; i++) {
            set.add(Range.closed(starts[i], ends[i]));
        }
        for (int i = 0; i < arg; i++) {
            assertTrue(set.contains(ins[i]));
            assertFalse(set.contains(outs[i]));
        }
        long s2 = System.nanoTime();
        System.out.println(s2 - s1);

        long s3 = System.nanoTime();
        IntervalList il = new IntervalList();
        for (int i = 0; i < arg; i++) {
            il.addInterval(starts[i], ends[i]);
        }
        for (int i = 0; i < arg; i++) {
            assertTrue(il.contains(ins[i]));
            assertFalse(il.contains(outs[i]));
        }
        long s4 = System.nanoTime();
        System.out.println(s4 - s3);

    }

}
