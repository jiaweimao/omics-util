package omics.util.utils;

import com.google.common.collect.Sets;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Mar 2020, 11:16 PM
 */
class SetUtilsTest
{

    @Test
    void venn()
    {
        Set<Integer> aSet = Sets.newHashSet(1, 2, 3);
        HashSet<Integer> bSet = Sets.newHashSet(2, 3, 4);
        HashSet<Integer> cSet = Sets.newHashSet(1, 3, 5);
        int[] venn = SetUtils.venn(aSet, bSet, cSet);
        System.out.println(Arrays.toString(venn));
    }
}