package omics.util.utils;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 08 Nov 2017, 3:42 PM
 */
class StringUtilsTest
{

    @Test
    void testIndexOf()
    {
        Pattern pattern = Pattern.compile("N[^P][ST]");
        int[] indexes = StringUtils.indexOf("YKNNSDISSTR", pattern);
        assertEquals(indexes.length, 1);
        assertEquals(indexes[0], 2);

        indexes = StringUtils.indexOf("SADGADOGIAOHGIWO", pattern);
        assertEquals(indexes.length, 0);

        indexes = StringUtils.indexOf("SOgNYTslkjoNRSgaiNPTSG", pattern);
        assertEquals(indexes[0], 3);
        assertEquals(indexes[1], 11);
    }

    @Test
    void testSplitter()
    {
        Splitter.on(CharMatcher.anyOf("(,)")).trimResults().omitEmptyStrings().split("(0.3, 0.5)").forEach(System.out::println);
    }

    @Test
    void testContainsIgnoreCase()
    {
        String s1 = "AbBaCca";
        String s2 = "bac";
        assertTrue(StringUtils.containsIgnoreCase(s1, s2));
    }

    private static Stream<Arguments> trimLeftData()
    {
        return Stream.of(
                Arguments.of(" O00713;", "O00713;"),
                Arguments.of("O00713; ", "O00713; ")
        );
    }

    @ParameterizedTest
    @MethodSource("trimLeftData")
    void testTrimLeft(String raw, String trim)
    {
        assertEquals(StringUtils.trimLeft(raw), trim);
    }

    @Test
    void testRepeat()
    {
        String a = StringUtils.repeat("a", 3);
        assertEquals(a, "aaa");
    }

    @Test
    void testSubString()
    {
        String value = "ABCDEFG";
        assertEquals(StringUtils.subString(value, 1, 3, 3, '_'), "__ABCDE");
        assertEquals(StringUtils.subString(value, 3, 3, 3, '_'), "ABCDEFG");
        assertEquals(StringUtils.subString(value, 5, 3, 3, '_'), "CDEFG__");
    }

}