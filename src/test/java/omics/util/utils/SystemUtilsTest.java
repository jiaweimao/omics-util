package omics.util.utils;


import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 May 2019, 10:51 AM
 */
class SystemUtilsTest
{

    @Test
    void test()
    {
        assertNotNull(SystemUtils.OS_VERSION);
    }

    @Test
    void testAMD()
    {
        System.out.println(SystemUtils.IS_OS_WINDOWS);
        System.out.println(SystemUtils.isAMD64());
        Properties properties = System.getProperties();
        for (Map.Entry<Object, Object> objectObjectEntry : properties.entrySet()) {
            System.out.println(objectObjectEntry);
        }

    }

    @Test
    void testUserName()
    {
        System.out.println(SystemUtils.USER_NAME);
    }

}